# Squark-gluino grid with all processes enabled
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# grid SM_SG_direct 2012
sumList_SM_SG   = [0,150,300,450,600,750,900,1050,1200,1350,1500,1650,1800,1950,2100,2250,2400,2550,2700,2850,3000,3150,3300,3450,3600,3750]
points_SM_SG = []
for sum in sumList_SM_SG:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        point = [msq,msq-diff]
        if msq>2000 or msq-diff<0 or msq-diff>1200: continue
        points_SM_SG += [ point ]

# Standard grid points
therun = runArgs.runNumber-162807
if therun>=0 and therun<len(points_SM_SG):
    evgenLog.info('Registered generation of grid two, direct SG point '+str(therun))
    for q in squarks: masses[q] = 0.96*points_SM_SG[therun][0]
    masses['1000021'] = points_SM_SG[therun][0]
    masses['1000022'] = points_SM_SG[therun][1]
    stringy = str(int(points_SM_SG[therun][0]))+'_'+str(int(points_SM_SG[therun][1]))
    gentype='ALL'
    decaytype='SGGrid'
    njets=1
    use_decays=False

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

