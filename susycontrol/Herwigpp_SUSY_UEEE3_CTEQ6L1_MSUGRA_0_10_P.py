## SUSY Herwig++ jobOptions for CMSSM/MSUGRA grid (tan beta = 10, A_0 = 0, mu>0)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.MSUGRA')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_MSUGRA_0_10_P_mc12points.py' )
try:
    mzero, mhalf = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_msugra_%s_%s_0_10_P_softsusy.slha' % (mzero, mhalf)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino', 'squarks', 'sbottoms', 'stops'], slha_file)

# define metadata
evgenConfig.description = 'MSUGRA grid generation with m_0 = %s, m_1/2 = %s, tan beta = 10, A_0 = 0, mu>0' % (mzero,mhalf)
evgenConfig.keywords = ['SUSY','MSUGRA','tanbeta10']
evgenConfig.contact  = ['teng.jian.khoo@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
