templatefile = 'susy_RPVMSSM_SM_gluino_template.slha'

evgenConfig.description = 'RPV gluino generation - simplified model g->3q (off shell ~q)'
evgenConfig.generators = ['Herwigpp']
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['lawrence.lee.jr@cern.ch']
evgenConfig.auxfiles += [templatefile, 'MSSM.model']

import os

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('gluino')
slha_file = templatefile
cmds = buildHerwigppCommands(sparticle_list, slha_file,'TwoParticleInclusive', earlyCopy=True)

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

if runArgs.runNumber==174653:
    Mgo = '1.000000e+02'
elif runArgs.runNumber==174654:
    Mgo = '2.000000e+02'
elif runArgs.runNumber==174655:
    Mgo = '3.000000e+02'
elif runArgs.runNumber==174656:
    Mgo = '4.000000e+02'
elif runArgs.runNumber==174657:
    Mgo = '5.000000e+02'
elif runArgs.runNumber==174658:
    Mgo = '6.000000e+02'
elif runArgs.runNumber==177968:
    Mgo = '7.000000e+02'
elif runArgs.runNumber==174659:
    Mgo = '8.000000e+02'
elif runArgs.runNumber==177969:
    Mgo = '9.000000e+02'
elif runArgs.runNumber==174660:
    Mgo = '10.000000e+02'
elif runArgs.runNumber==174661:
    Mgo = '12.000000e+02'
elif runArgs.runNumber==174662:
    Mgo = '14.000000e+02'
else:
    print 'ERROR - no Mgo specified'


if not os.access(templatefile,os.R_OK):
    print 'ERROR: Could not get slha file'
else:
    oldcard = open(templatefile,'r')
    newcard = open('spectrum.slha','w')
    for line in oldcard:
        if '   1000021     6.07713704E+02' in line:
            newcard.write('   1000021     %s   # ~g  \n'%(Mgo))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    os.rename('spectrum.slha',templatefile)


## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds




