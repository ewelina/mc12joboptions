# SUSY Herwig++ jobOptions for natural pMSSM
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_pMSSM_stL_M1_100_M2_1000_M3_2500_mc12points.py' )

try:
    (mu,mql3) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_pMSSM_stL_M1_100_M2_1000_M3_2500_mu_%s_mqL3_%s.slha' % (mu,mql3)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stops','sbottoms'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Natural pMSSM grid generation with mu %s mqL3 %s for t1->t+neut2 decays' % (mu,mql3) 
evgenConfig.keywords = ['SUSY','stop','sbottom','pMSSM']
evgenConfig.contact  = ['carolina.deluca.silberberg@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

