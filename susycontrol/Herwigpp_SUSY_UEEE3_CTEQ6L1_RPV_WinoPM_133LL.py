## SUSY Herwig++ jobOptions for WinoPM/Bino simplified model with lambda_133 R-parity violation.
## Author: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVSimplifiedModel')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_RPV_WinoPM_133LL_mc12points.py' )
try:
    parameters = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

if len(parameters) > 2:
    # Third parameter is the LSP lifetime
    slha_file = 'susy_RPV_%s_%s_%sps_WinoPM_133LL_simplified.slha' % (parameters[0], parameters[1], parameters[2])
else:
    slha_file = 'susy_RPV_%s_%s_WinoPM_133LL_simplified.slha' % (parameters[0], parameters[1])

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'WinoPM/Bino simplified model with lambda_133 R-parity violation. with m_WinoPM = %s, m_Bino = %s' % (parameters[0], parameters[1])
if len(parameters) > 2:
    evgenConfig.description += ', Bino lifetime = %s ps' % (parameters[2])
evgenConfig.keywords = ['SUSY', 'RPVLL', 'Lambda133', 'WinoPM']
evgenConfig.contact  = ['flowerde@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()


from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()
DecayLengthFilter = topAlg.DecayLengthFilter
DecayLengthFilter.McEventCollection = "GEN_EVENT"
# specify desired decay region:
DecayLengthFilter.Rmin = 0
DecayLengthFilter.Rmax = 300
DecayLengthFilter.Zmin = 0
DecayLengthFilter.Zmax = 300
DecayLengthFilter.particle_id = 1000022;
StreamEVGEN.RequireAlgs += [ "DecayLengthFilter" ]

# clean up
del cmds
