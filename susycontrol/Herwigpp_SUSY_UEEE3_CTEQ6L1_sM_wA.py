## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SMModeADG')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

## Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
if   ((runArgs.runNumber >= 179578 and runArgs.runNumber <= 179583) or
      (runArgs.runNumber >= 204026 and runArgs.runNumber <= 204055) or
      (runArgs.runNumber >= 205154 and runArgs.runNumber <= 205169) or
      (runArgs.runNumber >= 205176 and runArgs.runNumber <= 205186)):
    slha_file = 'susy_simplifiedModel_wA_sl_noWcascade_%s.slha' % (runArgs.runNumber)
elif ((runArgs.runNumber >= 205170 and runArgs.runNumber <= 205174)):
    slha_file = 'susy_simplifiedModel_wA_noslep_noWcascade_%s.slha' % (runArgs.runNumber)
elif ((runArgs.runNumber >= 179223 and runArgs.runNumber <= 179299) or 
      (runArgs.runNumber >= 204076 and runArgs.runNumber <= 204095)):
    slha_file = 'susy_simplifiedModel_wA_stau_noWcascade_%s.slha' % (runArgs.runNumber)
else:
    raise RuntimeError('Common file not defined for DSID %s. Aborting!' % runArgs.runNumber)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

# Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['~chi_1+','~chi_20'],slha_file,'Exclusive')

## Define metadata
evgenConfig.description = 'Simplified Model Mode A grid generation for direct gaugino search...'
evgenConfig.keywords    = ['SUSY','Direct Gaugino','Simplified Models','chargino','neutralino']
evgenConfig.contact     = ['alaettin.serhan.mete@cern.ch']

## Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter if used
if ((runArgs.runNumber >= 179223 and runArgs.runNumber <= 179299) or 
    (runArgs.runNumber >= 179578 and runArgs.runNumber <= 179583)):
    include ( 'MC12JobOptions/MultiElecMuTauFilter.py' )

    if (runArgs.runNumber >= 179223 and runArgs.runNumber <= 179299):
        MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
        MultiElecMuTauFilter.NLeptons  = 2
        MultiElecMuTauFilter.MinPt = 5000.
        MultiElecMuTauFilter.MaxEta = 2.7
        MultiElecMuTauFilter.MinVisPtHadTau = 1e10
        MultiElecMuTauFilter.IncludeHadTaus = 0
    elif (runArgs.runNumber >= 179578 and runArgs.runNumber <= 179583):
        MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
        MultiElecMuTauFilter.NLeptons  = 2
        MultiElecMuTauFilter.MinPt = 10000.
        MultiElecMuTauFilter.MaxEta = 2.7
        MultiElecMuTauFilter.MinVisPtHadTau = 1e10
        MultiElecMuTauFilter.IncludeHadTaus = 0

    StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

elif ((runArgs.runNumber >= 205154 and runArgs.runNumber <= 205169) or
      (runArgs.runNumber >= 205176 and runArgs.runNumber <= 205186) or 
      (runArgs.runNumber >= 205170 and runArgs.runNumber <= 205174)):
      include ( 'MC12JobOptions/MultiElecMuTauFilter.py' )

      MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
      MultiElecMuTauFilter.NLeptons  = 2
      MultiElecMuTauFilter.MinPt = 4000.
      MultiElecMuTauFilter.MaxEta = 2.7
      MultiElecMuTauFilter.MinVisPtHadTau = 1e10
      MultiElecMuTauFilter.IncludeHadTaus = 0

      StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

elif( runArgs.runNumber >= 204076 and runArgs.runNumber <= 204095 ):
    include ( 'MC12JobOptions/MultiElecMuTauFilter.py' )

    topAlg += MultiElecMuTauFilter(
        name = 'DileptonFilter',
        NLeptons  = 2,
        MinPt = 5000,
        MaxEta = 2.7,
        MinVisPtHadTau = 15000,
        IncludeHadTaus = 1
    )
    topAlg += MultiElecMuTauFilter(
        name = 'TauFilter',
        NLeptons  = 1,
        MinPt = 1e10,
        MaxEta = 2.7,
        MinVisPtHadTau = 15000,
        IncludeHadTaus = 1
    )

    StreamEVGEN.RequireAlgs = [ "DileptonFilter","TauFilter" ]

## Clean up
del cmds
