from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

import os

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model RPVMSSM_UFO
define p = g u c d s u~ c~ d~ s~
generate    p p > go go @1
output -f pythia8
""")
fcard.close()

process_dir = new_process()

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(6000))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   4000      = ebeam1   ! beam 1 energy in GeV \n')
        elif ' ebeam2 ' in line:
            newcard.write('   4000      = ebeam2   ! beam 1 energy in GeV \n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

#Map between run number and gluino mass
if runArgs.runNumber==173031:
    Mgo = '1.000000e+02'
elif runArgs.runNumber==173032:
    Mgo = '2.000000e+02'
elif runArgs.runNumber==173033:
    Mgo = '3.000000e+02'
elif runArgs.runNumber==173034:
    Mgo = '4.000000e+02'
elif runArgs.runNumber==173035:
    Mgo = '5.000000e+02'
elif runArgs.runNumber==173036:
    Mgo = '6.000000e+02'
elif runArgs.runNumber==173037:
    Mgo = '8.000000e+02'
elif runArgs.runNumber==173038:
    Mgo = '1.000000e+03'
elif runArgs.runNumber==173039:
    Mgo = '1.200000e+03'
elif runArgs.runNumber==173040:
    Mgo = '1.400000e+03'
else:
    print 'ERROR - no Mgo specified'

#Change MG param card to desired gluino mass
paramcard = subprocess.Popen(['get_files','-data','param_card.RPV_Gluino.dat'])
paramcard.wait()
if not os.access('param_card.RPV_Gluino.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('param_card.RPV_Gluino.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' Mgo' in line:
            newcard.write('  1000021 %s # Mgo \n'%(Mgo))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_RPVGluino'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

## Initialize gluino decay table to zero
topAlg.Pythia8.Commands += [
	"SLHA:file = events.lhe",
	"SLHA:readFrom = 0",
	"SLHA:useDecayTable = off",
	"1000021:0:bRatio = 0.0",
	"1000021:onMode = off",
]

## For each of the gluino->sq q decays, turn on and set to equal BR
gluinodecays = range(169,181) #decays with u or c
gluinodecays += range(181,187) #decays with tops

for mode in gluinodecays:
	topAlg.Pythia8.Commands += [
		"1000021:%d:bRatio = 1.0"%(mode), #pythia auto-renormalizes
		"1000021:%d:onMode = on" %(mode),
	]

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_RPVGluino_'+str(Mgo)
evgenConfig.keywords = ["RPV","Gluino"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
