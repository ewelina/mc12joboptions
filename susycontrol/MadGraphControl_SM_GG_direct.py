# Direct gluino decay to LSP (0-lepton, grid 1 last year)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2a = []
sumList   = [0,50,100,150,200,250,300,350,400,450,500,550,600,750,900,1050,1200,1350,1500,1650,1800,3950,2100,2250]
for sum in sumList:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        if msq>1200 or msq-diff<0: continue
        point = [msq,msq-diff]
        points2a += [ point ]

therun = runArgs.runNumber-144253-len(points2a)
if therun>=0 and therun<len(points2a):
    evgenLog.info('Registered generation of grid two, direct GG point '+str(therun))
    masses['1000021'] = points2a[therun][0]
    masses['1000022'] = points2a[therun][1]
    stringy = str(int(points2a[therun][0]))+'_'+str(int(points2a[therun][1]))
    gentype='GG'
    decaytype='direct'
    njets=1
    use_decays=False

# 2012 Extension
sumList_SM_GG   = [0,150,300,450,600,750,800,850,900,950,1000,1050,1100,1150,1200,1250,1300,1350,1400,1450,1500,1650,1800,1950,2100,2250,2400,2550,2700,2850,3000,3150,3300,3450,3600,3750]
points_SM_GG = []
for sum in sumList_SM_GG:
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*75 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        point = [msq,msq-diff]
        if msq>1800 or msq-diff<0 or msq-diff>1200: continue
        points_SM_GG += [ point ]

therun = therun-350
if therun>=0 and therun<len(points_SM_GG):
    evgenLog.info('Registered generation of grid two, direct GG point '+str(therun))
    masses['1000021'] = points_SM_GG[therun][0]
    masses['1000022'] = points_SM_GG[therun][1]
    stringy = str(int(points_SM_GG[therun][0]))+'_'+str(int(points_SM_GG[therun][1]))
    gentype='GG'
    decaytype='direct'
    njets=1
    use_decays=False

# mc12_14TeV samples for upgrade studies
points_SM_GG_14TeV = {186401 : "1650_1", 186402 : "1950_1", 186403 : "2250_1", 186404 : "2550_1", 186405 : "2850_1", 186406 : "3150_1", 186407 : "3450_1", 186408 : "3750_1", 186409 : "1800_300", 186410 : "2100_300", 186411 : "2400_300", 186412 : "2700_300", 186413 : "3000_300", 186414 : "3300_300", 186415 : "3600_300", 186416 : "3900_300", 186417 : "750_600", 186418 : "1050_600", 186419 : "1350_600", 186420 : "1650_600", 186421 : "1950_600", 186422 : "2250_600", 186423 : "2550_600", 186424 : "2850_600", 186425 : "3150_600", 186426 : "3450_600", 186427 : "3750_600", 186428 : "1200_900", 186429 : "1050_900", 186430 : "1500_900", 186431 : "1800_900", 186432 : "2100_900", 186433 : "2400_900", 186434 : "2700_900", 186435 : "3000_900", 186436 : "3300_900", 186437 : "3600_900", 186438 : "3900_900", 186439 : "1350_1200", 186440 : "1650_1200", 186441 : "1950_1200", 186442 : "2250_1200", 186443 : "2550_1200", 186444 : "2850_1200", 186445 : "3150_1200", 186446 : "3450_1200", 186447 : "3750_1200", 186448 : "1800_1500", 186449 : "1650_1500", 186450 : "2100_1500", 186451 : "2400_1500", 186452 : "2700_1500", 186453 : "3000_1500", 186454 : "3300_1500", 186455 : "3600_1500", 186456 : "3900_1500", 186457 : "1950_1800", 186458 : "2250_1800", 186459 : "2550_1800", 186460 : "2850_1800", 186461 : "3150_1800", 186462 : "3450_1800", 186463 : "3750_1800", 186464 : "2400_2100", 186465 : "2250_2100", 186466 : "2700_2100", 186467 : "3000_2100", 186468 : "3300_2100", 186469 : "3600_2100", 186470 : "3900_2100", 186471 : "2550_2400", 186472 : "2850_2400", 186473 : "3150_2400", 186474 : "3450_2400", 186475 : "3750_2400"}

# extension of the upgrade samples, append to dict
points_SM_GG_14TeV_ext = {186898 : "1800_150",  186899 : "2100_150",  186900 : "2400_150",  186901 : "2700_150",  186902 : "3000_150",  186903 : "1650_450",  186904 : "1950_450",  186905 : "2250_450",  186906 : "2550_450",  186907 : "2850_450",  186908 : "900_750",  186909 : "1200_750",  186910 : "1500_750",  186911 : "1800_750",  186912 : "2100_750",  186913 : "2400_750",  186914 : "2700_750",  186915 : "3000_750",  186916 : "1350_1050",  186917 : "1650_1050",  186918 : "1950_1050",  186919 : "2250_1050",  186920 : "2550_1050",  186921 : "2850_1050",  186922 : "1500_1350",  186923 : "1800_1350",  186924 : "2100_1350",  186925 : "2400_1350",  186926 : "2700_1350",  186927 : "3000_1350",  186928 : "625_600",  186929 : "675_600",  186930 : "725_700",  186931 : "775_700",  186932 : "825_800",  186933 : "875_800",  186934 : "925_900",  186935 : "975_900",  186936 : "1025_1000",  186937 : "1075_1000",  186938 : "1125_1100",  186939 : "1175_1100",  186940 : "1225_1200",  186941 : "1275_1200",  186942 : "1325_1300",  186943 : "1375_1300",  186944 : "1425_1400",  186945 : "1475_1400",  186946 : "1525_1500",  186947 : "1575_1500",  186948 : "1625_1600",  186949 : "1675_1600" }

points_SM_GG_14TeV.update(points_SM_GG_14TeV_ext)

# further extension of the upgrade samples, append to dict
points_SM_GG_14TeV_ext_v2 = {204872 : "75_1",  204873 : "375_1",  204874 : "675_1",  204875 : "975_1",  204876 : "1275_1",  204877 : "1575_1",  204878 : "225_150",  204879 : "525_150",  204880 : "825_150",  204881 : "1125_150",  204882 : "1425_150",  204883 : "375_300",  204884 : "675_300",  204885 : "975_300",  204886 : "1275_300",  204887 : "1575_300",  204888 : "525_450",  204889 : "825_450",  204890 : "1125_450",  204891 : "1425_450" } 

points_SM_GG_14TeV.update(points_SM_GG_14TeV_ext_v2)

therun = runArgs.runNumber
if therun in points_SM_GG_14TeV.keys():
    evgenLog.info('Registered generation of grid two, direct GG point '+str(therun))
    p = points_SM_GG_14TeV[therun].split("_")
    
    masses['1000021'] = p[0] 
    masses['1000022'] = p[1]
    stringy = points_SM_GG_14TeV[therun] 
    gentype='GG'
    decaytype='direct'
    njets=1
    use_decays=False

# 13TeV benchmark points for DC14
points_SM_GG_13TeV_benchmark = {204696:"1050_450", 204697:"1050_600", 204698:"1050_750", 204699:"1050_900", 204650:"1350_0", 204651:"1500_0", 204652:"1650_0", 204653:"1800_0", 204654:"1950_0"}

therun = runArgs.runNumber
if therun in points_SM_GG_13TeV_benchmark.keys():
    evgenLog.info('Registered generation of grid two, direct GG point '+str(therun))
    p = points_SM_GG_13TeV_benchmark[therun].split("_")    
    masses['1000021'] = p[0] 
    masses['1000022'] = p[1]
    stringy = points_SM_GG_13TeV_benchmark[therun] 
    gentype='GG'
    decaytype='direct'
    njets=1
    use_decays=False

    
# Systematics
therun = runArgs.runNumber-152687
if therun>=0 and therun<8*5:
    systpoints = [ [137,62], [137,112], [325,25], [325,175], [325,275], [600,0], [600,300], [625,575] ]
    evgenLog.info('Registered generation of gluino grid two systematic uncertainty point '+str(therun))
    masses['1000021'] = systpoints[therun%8][0]
    masses['1000022'] = systpoints[therun%8][1]
    syst_mod='qup'
    if therun>=8: syst_mod='qdown'
    if therun>=16: syst_mod='radhi'
    if therun>=24: syst_mod='radlo'
    if therun>=32: syst_mod='rad0'
    stringy = str(int(systpoints[therun%8][0]))+'_'+str(int(systpoints[therun%8][1]))+'_'+syst_mod
    gentype='GG'
    decaytype='direct'
    njets=1
    use_decays=False

if runArgs.runNumber in [152695,152696]:
    evt_multiplier = 4.0

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

