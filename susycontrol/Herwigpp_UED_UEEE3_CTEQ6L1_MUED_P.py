## SUSY Herwig++ jobOptions for MUED grid (mh0=125GeV)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.MUED')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/UED_MUED_P_mc12points.py' )
try:
    (InverseRadius, LambdaR, NLeptons) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_UEDConfig.py' )
cmds = buildHerwigppCommands(['KKgluon', 'KKquarks', 'KKbottoms', 'KKtops'], InverseRadius, LambdaR)

# define metadata
evgenConfig.description = 'MUED grid generation with 1/R = %s, LR = %s, mh0 = 125' % (InverseRadius, LambdaR)
evgenConfig.keywords = ['SUSY','MUED','mh125']
evgenConfig.contact  = ['Naoko.Kanaya@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
