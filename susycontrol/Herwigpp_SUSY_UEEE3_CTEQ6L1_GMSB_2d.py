
pointdict = {
    174854: ( 50, 2 ), 174855: ( 60, 2 ), 174856: ( 70, 2 ), 174857: ( 80, 2 ), 174858: ( 90, 2 ), 174859: ( 100, 2 ), 
    174860: ( 50,15 ), 174861: ( 60,15 ), 174862: ( 70,15 ), 174863: ( 80,15 ), 174864: ( 90,15 ), 174865: ( 100,15 ), 
    174866: ( 50,30 ), 174867: ( 60,30 ), 174868: ( 70,30 ), 174869: ( 80,30 ), 174870: ( 90,30 ), 174871: ( 100,30 )
    }

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GMSB')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

try:
    Lambda, tanbeta = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
    
# define spectrum file name
slha_file = 'gmsb.%s_250_3_%s_1_1.slha'  % (Lambda, tanbeta)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino', 'squarks', 'sbottoms', 'stops', 'gauginos', 'sleptons', 'staus', 'sneutrinos'], slha_file)

# define metadata
evgenConfig.description = 'SUSY GMSB 2d grid: Lambda=%s, tan beta=%s' % (Lambda, tanbeta)
evgenConfig.keywords = ['SUSY', 'GMSB', 'N_5=3']
evgenConfig.contact = [ 'wolfgang.ehrenfeld@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds, Lambda, tanbeta
