# SUSY Herwig++ jobOptions for natural pMSSM
# Use SUSYHIT and bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_3SQ_SLAC_10K_pMSSM_mc12points.py' )
try:
    pMSSM_ID = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_%s.slha' % pMSSM_ID

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop1','sbottom1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Natural pMSSM grid generation for model %s' % pMSSM_ID
evgenConfig.keywords = ['SUSY','stop','sbottom','pMSSM']
evgenConfig.contact  = ['Sara.Strandberg@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

