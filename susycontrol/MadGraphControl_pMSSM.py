# SUSY MadGraph+Pythia6 jobOptions for pMSSM simulation

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')
from MadGraphControl.MadGraphUtils import *

madGraphConfigs={
  'QCD': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define sstrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > sstrong sstrong QED=99 QCD=99 @1
add process p p > sstrong sstrong j QED=99 QCD=99 @2
output -f
""",
  'EW': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define sewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
generate p p > sewk sewk QED=99 QCD=99 @1
add process p p > sewk sewk j QED=99 QCD=99 @2
output -f
""",  
  'hEW': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define rewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n3 n4 x2- x2+ sve sve~ svm svm~ svt svt~
define sewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
generate p p > rewk sewk QED=99 QCD=99 @1
add process p p > rewk sewk j QED=99 QCD=99 @2
output -f
""",
  'wEW': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define rewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n2 n3 n4 x2- x2+ sve sve~ svm svm~ svt svt~
define sewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
generate p p > rewk sewk QED=99 QCD=99 @1
add process p p > rewk sewk j QED=99 QCD=99 @2
output -f
""",
  'Mix': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define sewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
define sstrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > sewk sstrong QED=99 QCD=99 @1
add process p p > sewk sstrong j QED=99 QCD=99 @2
output -f
""",
'LL': """
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define slep = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ sve sve~ svm svm~ svt svt~
generate p p > slep slep QED=99 QCD=99 @1
add process p p > slep slep j QED=99 QCD=99 @2
output -f
""",}

LongLivedWidth=2E-13 #anything smaller and we consider the sparticle long-lived

def checkLifetimes(slha):
  import shutil
  shutil.move(slha, slha+'.org')
  slhaInfo=open(slha+'.org').readlines()
  update=open(slha,'w')
  block=None
  skip=False
  n1Mass="0"
  n2Mass="0"
  c1Mass="0"
  c1Lifetime=None
  n2Lifetime=None
  for line in slhaInfo:
    if line.upper().startswith("BLOCK"):
      block=line.upper().split()[1]
    elif block=="MASS" and not line.startswith("#"):
      part,mass=line.split()[0:2]
      mass=str(abs(float(mass))) #mass can be negative in slha files
      if part=="1000022":
        n1Mass=mass
      if part== "1000023":
        n2Mass=mass
      if part=="1000024" in line:
        c1Mass=mass
    if line.upper().startswith('DECAY'):
      skip=False
      part,width=line.split()[1:3]
      if (part=="1000023" or part=="1000024") and float(width)<LongLivedWidth:
        skip=True
        update.write("DECAY %s 0 # decay disabled as width=%s\n" % (part,width))
        if float(width)==0:
          lifetime="100000"
        else:
          lifetime=str(6.582E-16/float(width)) #check numbers
        if part=="1000024":
          c1Lifetime=lifetime
        if part=="1000023":
          n2Lifetime=lifetime
    if skip:
      update.write("#")
    update.write(line)
  return c1Lifetime,n2Lifetime,c1Mass,n1Mass,n2Mass

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 3.0
num_events=5000

if not hasattr(runArgs,'jobConfig') or len(runArgs.jobConfig)==0:
  log.error('No jobConfig set - cannot figure out configuration')

#naming examples: MC12.210071.MadGraphPythia_AUET2B_CTEQ6L1_pMSSM_QCD_23851474.py
#                 MC12.210071.MadGraphPythia_AUET2B_CTEQ6L1_pMSSM_EW_23851474_DiLeptonFilter.py
jobConfig=runArgs.jobConfig[0]
parts=jobConfig.split('.')[2].split('_')
if len(parts)<6 or parts[3]!='pMSSM':
  log.fatal('Malformed jobConfig name - cannot figure out configuration')
generatorType=parts[4]
modelName=parts[5]
if not generatorType in ['LL','QCD','EW','Mix','hEW','wEW']:
    log.fatal('Unknown production type %s - cannot figure out configuration' % generatorType)

slha_file = 'pMSSM_%s.slha' % modelName
log.info("Using paramCard %s" % slha_file)
proccard = subprocess.Popen(['get_files','-data',slha_file])
proccard.wait()
if not os.access(slha_file,os.R_OK):
  log.fatal('Did not find slha file %s - giving up' % slha_file)

charginoLifetime,neutralinoLifetime,c1mass,n1mass,n2mass=checkLifetimes(slha_file)
log.info("Found  m(n1)=%s,m(c1+)=%s, t(c1+)=%s, m(n2)=%s, t(n2)=%s" % 
         (n1mass,c1mass,charginoLifetime,n2mass,neutralinoLifetime))


filterName=None
if len(parts)>6:
  filterName=parts[6]
  if filterName not in ['DiLepton','METFilter','EMuFilter']:
    log.fatal('Unknown filter type %s - cannot figure out configuration' % filterName)
  if filterName=='DiLepton':
    evt_multiplier=30 # can be overriden by command line if necessary
    runArgs.use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterPtCut = 5.
    runArgs.MultiElecMuTauFilterEtaCut = 6.
    runArgs.MultiElecMuTauFilterLepCut = 2
    runArgs.MultiElecMuTauFilterTauPtCut = 5.
    runArgs.MultiElecMuTauFilterIncludeTau = 1
  elif filterName=='EMuFilter':
    evt_multiplier=300 # it was 60 for pMSSM, 300 needed for DMpMSSM - can be overriden by command line if necessary
    runArgs.use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterPtCut = 15.
    runArgs.MultiElecMuTauFilterEtaCut = 6.
    runArgs.MultiElecMuTauFilterLepCut = 2
    runArgs.MultiElecMuTauFilterTauPtCut = 5.
    runArgs.MultiElecMuTauFilterIncludeTau = 0
  elif filterName=='METFilter':
    evt_multiplier=15 # can be overriden by command line if necessary
    runArgs.use_METfilter=True
    runArgs.METfilterCut=100.

if 'EventMultiplier' in dir(): evt_multiplier=EventMultiplier
if hasattr(runArgs,'EventMultiplier'): evt_multiplier=runArgs.EventMultiplier
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents>0: num_events=runArgs.maxEvents
nevts=num_events*evt_multiplier

ickkw=1
xqcut = 100 #see: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Matching#

fcard = open('proc_card_mg5.dat','w')
fcard.write(madGraphConfigs[generatorType])
  
name="pMSSM_"+generatorType
fcard.close()
iexcfile=0
print "proc card:"
print fcard

#--------------------------------------------------------------
# Beam energy
#--------------------------------------------------------------
### As suggested by MC generator experts
beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RunTimeError("No center of mass energy found.")


#Default Scales
isFixedRenScale="F"
isFixedFacScale="F"

scale=91.1880
renScale =scale
facScale1=scale
facScale2=scale

#--------------------------------------------------------------
# Choice of PDF set 
#--------------------------------------------------------------
doMSTW08PDFSet = None
pdfSet = 'lhapdf'   #default set to 'cteq6l1'
pdfNumber = 21000   # for lhaid

#--------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#--------------------------------------------------------------
# Grab the run card and move it into place
import subprocess
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    log.error('Could not get run card')
elif os.access('run_card.dat',os.R_OK):
    log.error('Old run card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' = ptj ' in line:
            newcard.write('   %f      = ptj ! minimum pt for the jets \n'%(xqcut))
        elif ' = ptb ' in line:
            newcard.write('   %f      = ptb ! minimum pt for the b \n'%(xqcut))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%int(nevts))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ickkw' in line:
            newcard.write('  %i         = ickkw   ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ' = pdlabel ' in line:
            if doMSTW08PDFSet:
                newcard.write('   \'%s\'      = pdlabel ! PDF set\n   %d      = lhaid       ! PDF number used ONLY for LHAPDF\n' \
                    % (str(pdfSet), int(pdfNumber)))
            else:
              newcard.write(line)
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


print "Modified run_card.dat:"
rcard = open('run_card.dat','r')
print rcard.read()
rcard.close()

process_dir = new_process()

generate(run_card_loc='run_card.dat',param_card_loc=slha_file,mode=0,njobs=1,run_name=generatorType,proc_dir=process_dir)

stringy = 'madgraph.'+str(name)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name=generatorType,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


#--------------------------------------------------------------
# Multi-core support
#--------------------------------------------------------------
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

#--------------------
# Pythia 6
#--------------------
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
runArgs.qcut = max(xqcut+5,xqcut*1.2)
runArgs.use_Tauola = True
if 'noTauola' in dir():
  runArgs.use_Tauola = False
if 'syst_mod' in dir():
    runArgs.syst_mod = syst_mod

include ("MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py")
evgenConfig.auxfiles += [slha_file]
if charginoLifetime:
  log.info("Using long-lived chargino")
  topAlg.Pythia.PythiaCommand += [ "pydat3 mdcy 312 1 0"]
if neutralinoLifetime:
  log.info("Using long-lived neutralino - setting to stable")
  topAlg.Pythia.PythiaCommand += [ "pydat3 mdcy 311 1 0"]

#--------------------------------------------------------------
# MC12 metadata
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph_'+str(name)+' for '+slha_file
if filterName:
  evgenConfig.description += ' with '+filterName
evgenConfig.keywords = ["pMSSM"]
evgenConfig.contact  = [ "Brian.Petersen@cern.ch" ]
if charginoLifetime:
  evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (c1mass,n1mass,charginoLifetime)
  evgenConfig.description += ' and long-lived chargino'
if neutralinoLifetime:
    evgenConfig.description += ' and long-lived neutralino2'

evgenConfig.inputfilecheck = stringy








