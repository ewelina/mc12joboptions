## SUSY Herwig++ jobOptions for simplified model GluinoStop_N60

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.sbotom_bneut2')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_sbottom_bneut2_mc12points.py' )
try:
    (msb,mchar,mneut) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_sbottom_bneut2_B%s_C%s_N%s.slha' % (msb, mchar,mneut)

m_sbottom = int(msb)
m_chargino = int(mchar)
m_neutralino = int(mneut)
diff = m_sbottom - m_chargino

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['sbottom1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Sbottom sb->t+chargino grid generation with m_sbottom = %s, m_chargino = %s, m_neutralino = %s GeV' % (msb,mchar,mneut)
evgenConfig.keywords = ['SUSY','sbottom']
evgenConfig.contact  = ['antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()


# clean up
del cmds
