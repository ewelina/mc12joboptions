
pointdict = {
    175821: (  40,  2), 175822: (  40,  5), 175823: (  40, 10), 175824: (  40, 15), 175825: (  40, 20), 175826: (  40, 30), 
    175827: (  40, 36), 175828: (  40, 37), 
    175829: (  50,  2), 175830: (  50,  5), 175831: (  50, 10), 175832: (  50, 15), 175833: (  50, 20), 175834: (  50, 30), 
    175835: (  50, 40), 175836: (  50, 43), 175837: (  50, 44), 
    175838: (  60,  2), 175839: (  60,  5), 175840: (  60, 10), 175841: (  60, 15), 175842: (  60, 20), 175843: (  60, 30), 
    175844: (  60, 40), 175845: (  60, 49), 175846: (  60, 50), 
    175847: (  70,  2), 175848: (  70,  5), 175849: (  70, 10), 175850: (  70, 15), 175851: (  70, 20), 175852: (  70, 30), 
    175853: (  70, 40), 175854: (  70, 50), 175855: (  70, 57), 175856: (  70, 58), 
    175857: (  80,  2), 175858: (  80,  5), 175859: (  80, 10), 175860: (  80, 15), 175861: (  80, 20), 175862: (  80, 30), 
    175863: (  80, 40), 175864: (  80, 50), 175865: (  80, 58), 175866: (  80, 59), 
    175867: (  90,  2), 175868: (  90,  5), 175869: (  90, 10), 175870: (  90, 15), 175871: (  90, 20), 175872: (  90, 30), 
    175873: (  90, 40), 175874: (  90, 50), 175875: (  90, 60), 175876: (  90, 61), 175877: (  90, 62), 
    175878: ( 100,  2), 175879: ( 100,  5), 175880: ( 100, 10), 175881: ( 100, 15), 175882: ( 100, 20), 175883: ( 100, 30), 
    175884: ( 100, 40), 175885: ( 100, 50), 175886: ( 100, 60), 175887: ( 100, 61), 175888: ( 100, 62), 
    175889: ( 110,  2), 175890: ( 110,  5), 175891: ( 110, 10), 175892: ( 110, 15), 175893: ( 110, 20), 175894: ( 110, 30), 
    175895: ( 110, 40), 175896: ( 110, 50), 175897: ( 110, 60), 175898: ( 110, 61), 175899: ( 110, 62), 
    175900: ( 120,  2), 175901: ( 120,  5), 175902: ( 120, 10), 175903: ( 120, 15), 175904: ( 120, 20), 175905: ( 120, 30), 
    175906: ( 120, 40), 175907: ( 120, 50), 175908: ( 120, 60), 175909: ( 120, 61), 175910: ( 120, 62), 
    }

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GMSB')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

try:
    Lambda, tanbeta = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
    
# define spectrum file name
slha_file = 'susy_gmsb_%s_250_3_%s_1_1_spheno.slha'  % (Lambda, tanbeta)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['all'], slha_file)

# define metadata
evgenConfig.description = 'SUSY GMSB 2d grid: Lambda=%s, tan beta=%s' % (Lambda, tanbeta)
evgenConfig.keywords = ['SUSY', 'GMSB', 'N_5=3']
evgenConfig.contact = [ 'wolfgang.ehrenfeld@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds, Lambda, tanbeta, slha_file
