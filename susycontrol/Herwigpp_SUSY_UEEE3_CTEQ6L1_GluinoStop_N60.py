## SUSY Herwig++ jobOptions for simplified model GluinoStop_N60

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.GluinoStop_N60')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_GluinoStop_N60_mc12points.py' )
try:
    (mgl,mst) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_GluinoStop_G%s_T%s_N60.slha' % (mgl, mst)

m_gluino = int(mgl)
m_stop = int(mst)
diff = m_gluino - m_stop

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
if (m_gluino == 700 or  diff == 200) : cmds = buildHerwigppCommands(['gluino','stop1'], slha_file)
else : cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GluinoStop grid generation with m_gluino = %s, m_stop = %s GeV' % (mgl,mst)
evgenConfig.keywords = ['SUSY','stop','gluino']
evgenConfig.contact  = ['antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
