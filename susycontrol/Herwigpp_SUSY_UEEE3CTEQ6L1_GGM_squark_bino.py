# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

masses = runArgs.jobConfig[0].split('.py')[0].split('_')[-2:]

# define spectrum file name
slha_file = 'susy_sq_bino_%s_%s.slha'%(masses[0],masses[1])

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks', 'sbottoms', 'stops'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM wino-bino grid generation with msq=%s mbino=%s'%(masses[0],masses[1])
evgenConfig.keywords = ['SUSY', 'GGM', 'squark', 'bino']
evgenConfig.contact = [ 'susan.fowler@cern.ch']

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
