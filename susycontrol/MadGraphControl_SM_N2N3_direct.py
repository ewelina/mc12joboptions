# Direct N2N3 grid (re-parameterised)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

sumList_SM_N2N3_v2A = [ 180, 230, 280, 330, 380, 430, 480, 530, 580, 630, 680, 780 ]
points_SM_N2N3 = []
massDiff = xrange(15,80,10);
for massN3 in sumList_SM_N2N3_v2A:
    for item in massDiff:
        #print 'adding point ' ,massN3 ,' and ' ,item
        massN1 = massN3 - 80
        massN2 = massN3 - 5
        masseR = massN3 - item
        point = [massN3, massN2, massN1, masseR]
        points_SM_N2N3 += [ point ]
sumList_SM_N2N3_v2B = [100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 700 ]
massDiffN2N1 = [20,35,50,65,80]
for massN1 in sumList_SM_N2N3_v2B:
   for item in massDiffN2N1:
        massN2 = massN1 + item
        massN3 = massN2
        masseR = 0.5*(massN2+massN1)
        point = [massN3, massN2, massN1, masseR]
        points_SM_N2N3 += [point]

therun = runArgs.runNumber-174679
if 174768<=runArgs.runNumber: therun = runArgs.runNumber-174679-7
if therun>=0 and therun<len(points_SM_N2N3):
    log.info('Registered generation of grid 25, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3[therun][0]
    masses['1000023'] = points_SM_N2N3[therun][1]
    masses['1000022'] = points_SM_N2N3[therun][2]
    masses['2000011'] = points_SM_N2N3[therun][3]
    masses['2000013'] = points_SM_N2N3[therun][3]
    log.info('Setting up grid with masses ' + str(points_SM_N2N3[therun][0]) + ' - ' + str(points_SM_N2N3[therun][1]) + ' - ' + str(points_SM_N2N3[therun][2]) + ' - ' + str(points_SM_N2N3[therun][3]))
    stringy = str(int(points_SM_N2N3[therun][0]))+'_'+str(int(points_SM_N2N3[therun][3]))
    gentype = 'N2N3'
    decaytype = 'direct'
    njets = 1
    use_decays = False

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

