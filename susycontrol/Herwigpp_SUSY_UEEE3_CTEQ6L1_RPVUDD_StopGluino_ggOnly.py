def slhaFileFromRunNumber(runNumber, offset):
  include('MC12JobOptions/SUSY_RPVUDD_StopGluino_points.py')
  return rpvUDDStopGluinoPoints[int(runNumber) - int(offset)]

# Start of the run number range 178206 (offset due to previous samples 27)
firstRunNumber = 178206 - 27

slhafile = slhaFileFromRunNumber(runArgs.runNumber, firstRunNumber)
print 'Using slha:', slhafile

evgenConfig.description = 'RPV UDD Stop-Gluino Model - gl-gl production only, slha file: {0}'.format(slhafile)
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['thomas.gillam@cern.ch','christopher.young@cern.ch']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['gluino'], slhafile, 'TwoParticleInclusive')

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Append our commands
topAlg.Herwigpp.Commands += cmds.splitlines()

## Clean up
del cmds
