# Stop pair-production with stop > charm LSP or stop > b W LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

therun = -1

points_TT_mixedBWC = []
if runArgs.runNumber in range(183973,183996+1):
    therun = runArgs.runNumber-183973
    points_TT_mixedBWC = [[110, 30],[110, 30],[110, 30],
                          [110,100],[110,100],[110,100],
                          [200,120],[200,120],[200,120],
                          [200,190],[200,190],[200,190],
                          [250,170],[250,170],[250,170],
                          [250,240],[250,240],[250,240],
                          [300,220],[300,220],[300,220],
                          [300,290],[300,290],[300,290]]
elif runArgs.runNumber in range(204686,204694+1):
    therun = runArgs.runNumber-204686
    points_TT_mixedBWC = [[150,70],[150,70],[150,70],
                          [150,140],[150,140],[150,140]]
if therun>=0 and therun<len(points_TT_mixedBWC):
    masses['1000006'] = points_TT_mixedBWC[therun][0]
    masses['1000022'] = points_TT_mixedBWC[therun][1]
    stringy = str(points_TT_mixedBWC[therun][0])+'_'+str(points_TT_mixedBWC[therun][1])
    gentype='TT'
    decaytype='mixedBWC'
    njets=1
    nevts=100000
    evgenLog.info('Registered generation of stop pair prodcution, stop to charm+LSP and b+W+LSP mixed; grid point '+str(therun)+' decoded into mass point '+str(points_TT_mixedBWC[therun])+', with '+str(njets)+' jets.')
    use_decays=False

evgenConfig.contact  = [ "takashi.yamanaka@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
