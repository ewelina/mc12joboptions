# One-step squark decay to LSP, x=1/2, WW (0-2-lepton, grid 16 last year)
# Also One-step squark decay to LSP, grid in X, WW (0-2-lepton)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# m_heavy / m_LSP planes, grids 4,5,8-15
points2b1 = []
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        msq = (sum+diff)/2.
        if msq>1200 or msq-diff<0 or msq<200: continue
        point = [msq,msq-diff]
        points2b1 += [ point ]

# Grids 6 and 7
def wantedPoint(dM1,dM2):
    dMStepsBoundaries=[10,25,50,100]
    dMStepBulk=100
    for s in dMStepsBoundaries:
        if dM1==s or dM2==s:
            return True
        pass
    if dM2 > dMStepsBoundaries[len(dMStepsBoundaries)-1]:
        if dM2%dMStepBulk==0:
            return True
        pass
    return False

mLSP=60
points2x = []
points2xextra = []
for mGluino in [200,300,400,500,600,700,800,900,1000,1100,1200]:
    for m in xrange(mLSP,mGluino):
        dM1=mGluino-m
        dM2=m-mLSP
        if not wantedPoint(dM1,dM2): continue
        point = [mGluino,float(dM2)/float(mGluino-mLSP)]
        points2x += [ point ]
for mGluino in [1300,1400,1500]:
    for m in xrange(mLSP,mGluino):
        dM1=mGluino-m
        dM2=m-mLSP
        if not wantedPoint(dM1,dM2): continue
        point = [mGluino,float(dM2)/float(mGluino-mLSP)]
        points2xextra += [ point ]

points2x2 = []
for mchar in [100,70,85]:
     point = [200,mchar]
     points2x2 += [ point ] 
point= [300,160]
points2x2 += [ point ]

# Standard grid points
therun = runArgs.runNumber-148001
if therun>=0 and therun<len(points2b1):
    evgenLog.info('Registered generation of one-step SS point '+str(therun))
    for q in squarksl: masses[q] = points2b1[therun][0]
    masses['1000022'] = points2b1[therun][1]
    masses['1000024'] = 0.5*(points2b1[therun][0]+points2b1[therun][1])
    stringy = str(int(points2b1[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(points2b1[therun][1]))
    gentype='SS'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False
# x-grid
therun = therun-2*len(points2b1)
if therun>=0 and therun<len(points2x):
    evgenLog.info('Registered generation of one-step SS in X point '+str(therun))
    for q in squarksl: masses[q] = points2x[therun][0]
    masses['1000022'] = mLSP
    masses['1000024'] = points2x[therun][1] * (points2x[therun][0]-mLSP) + mLSP
    stringy = str(int(points2x[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(mLSP))
    gentype='SS'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False

# Extensions
list_of_dsids = [ 186618, 186698, 186699, 186989]
therun=0
for dsid in list_of_dsids:
   therunis = runArgs.runNumber-dsid
   if therunis==0:
       evgenLog.info('Registered generation of one-step SS in X point with filter '+str(therun))
       for q in squarksl: masses[q] = points2x2[therun][0]
       masses['1000022'] = mLSP
       masses['1000024'] = points2x2[therun][1]
       stringy = str(int(points2x2[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(mLSP))
       gentype='SS'
       decaytype='onestepCC'
       njets=1
       use_decays=False
       use_Tauola=False
   therun+=1

therun = runArgs.runNumber - 179852
points2x = [[300,0.16],[300,0.3],[400,0.25],[400,0.45]]
if therun>=0 and therun<len(points2x):
    mLSP=60
    evgenLog.info('Registered generation of grid extension: one-step SS in X point '+str(therun))
    for q in squarksl: masses[q] = points2x[therun][0]
    masses['1000022'] = mLSP
    masses['1000024'] = points2x[therun][1] * (points2x[therun][0]-mLSP) + mLSP
    stringy = str(int(points2x[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(mLSP))
    gentype='SS'
    decaytype='onestepCC'
    njets=1
    use_decays=False
    use_Tauola=False
    evt_multiplier = 4.0

# Systematics
therun = runArgs.runNumber-152727
if therun>=0 and therun<5*5:
    evgenLog.info('Registered generation of grid four systematics, one-step SS point '+str(therun))
    systpoints = [ [300,250], [325,165], [335,95], [210,160], [205,45] ]
    for q in squarksl: masses[q] = systpoints[therun%5][0]
    masses['1000022'] = systpoints[therun%5][1]
    masses['1000024'] = 0.5*(systpoints[therun%5][0]+systpoints[therun%5][1])
    syst_mod='qup'
    if therun>=5: syst_mod='qdown'
    if therun>=10: syst_mod='radhi'
    if therun>=15: syst_mod='radlo'
    if therun>=20: syst_mod='rad0'
    stringy = str(int(systpoints[therun%5][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(systpoints[therun%5][1]))+'_'+syst_mod
    gentype='SS'
    decaytype='onestepCC'
    njets=1
    use_decays=False

# x-grid systematics
therun = runArgs.runNumber-152777
if therun>=0 and therun<5*8:
    evgenLog.info('Registered generation of grid six systematics, x-grid one-step SS point '+str(therun))
    systpoints = [ [400,390], [400,375], [400,350], [400,110], [400,70], [200,100], [200,110], [200,190] ]
    for q in squarksl: masses[q] = systpoints[therun%8][0]
    masses['1000024'] = systpoints[therun%8][1]
    masses['1000022'] = 60
    syst_mod='qup'
    if therun>=8: syst_mod='qdown'
    if therun>=16: syst_mod='radhi'
    if therun>=24: syst_mod='radlo'
    if therun>=32: syst_mod='rad0'
    stringy = str(int(systpoints[therun%8][0]))+'_'+str(int(systpoints[therun%8][1]))+'_60_'+syst_mod
    gentype='SS'
    decaytype='onestepCC'
    njets=1
    use_decays=False

if runArgs.runNumber in [152732, \
                 152733,152734,152735,152736,
                 152785,152786, \
                 152787,152788,152790,152791,152792]:
    evt_multiplier = 4.0


evgenConfig.contact  = [ "genest@lpsc.in2p3.fr" ]
evgenConfig.keywords += ['squark','one_step']
evgenConfig.description = 'squark production one step without slepton in simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

