# SUSY Herwig++ jobOptions for stop2 pair production grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop2')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_T2t2_hTone_mc12points.py' )
try:
    mT2, mT1, mN1 = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_T2t2_Ttwo%s_Tone%s_L%s_hTone.slha' % (mT2, mT1, mN1)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop2'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'stop2 simplified grid generation' 
evgenConfig.keywords = ['SUSY','stop2']
evgenConfig.contact  = ['Ximo.Poveda@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds


#==============================================================
#
# End of job options file
#
###############################################################
