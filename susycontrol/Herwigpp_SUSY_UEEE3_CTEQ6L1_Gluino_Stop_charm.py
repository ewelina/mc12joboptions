## SUSY Herwig++ jobOptions for simplified model Gtt

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Gluino_Stop_charm')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include( 'MC12JobOptions/SUSY_Gluino_Stop_charm_mc12points.py' )
try:
    mgl, mst = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_Gluino_Stop_charm_G%s_T%s.slha' % (mgl, mst)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Gluino_Stop_charm grid generation with m_gluino = %s, m_stop = %s GeV' % (mgl,mst)
evgenConfig.keywords = ['SUSY','charm','stop','gluino']
evgenConfig.contact  = ['scaron@nikhef.nl','antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
