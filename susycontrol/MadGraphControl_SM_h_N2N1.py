#
# This becomes MC12JobOptions/MadGraphControl_SM_h_N2N1.py
#

from MadGraphControl.MadGraphUtils import *
#from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

pcard = open('param_card2.dat','w')
pcard.write("""

##******************************************************************
##                      MadGraph/MadEvent                          *
##******************************************************************
## Les Houches friendly file for the (MS)SM parameters of MadGraph *
##      SM parameter set and decay widths produced by MSSMCalc     *
##******************************************************************
##*Please note the following IMPORTANT issues:                     *
##                                                                 *
##0. REFRAIN from editing this file by hand! Some of the parame-   *
##   ters are not independent. Always use a calculator.            *
##                                                                 *
##1. alpha_S(MZ) has been used in the calculation of the parameters*
##   This value is KEPT by madgraph when no pdf are used lpp(i)=0, *
##   but, for consistency, it will be reset by madgraph to the     *
##   value expected IF the pdfs for collisions with hadrons are    *
##   used.                                                         *
##                                                                 *
##2. Values of the charm and bottom kinematic (pole) masses are    *
##   those used in the matrix elements and phase space UNLESS they *
##   are set to ZERO from the start in the model (particles.dat)   *
##   This happens, for example,  when using 5-flavor QCD where     *
##   charm and bottom are treated as partons in the initial state  *
##   and a zero mass might be hardwired in the model definition.   *
##                                                                 *
##       The SUSY decays have calculated using SDECAY 1.1a         *
##                                                                 *
##******************************************************************
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.1a        # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SOFTSUSY                                          
     2   2.0.5                                             
#
BLOCK MODSEL  # Model selection
     1     1   sugra                                             
#
BLOCK SMINPUTS  # Standard Model inputs
     1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
     2     1.16637000E-05   # G_F [GeV^-2]
     3     1.18000000E-01   # alpha_S(M_Z)^MSbar
     4     9.11876000E+01   # M_Z pole mass
     6     1.75000000E+02   # mt pole mass
     7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
     1     1.00000000E+02   #  m0                 
     2     2.50000000E+02   #  m12                
     3     1.00000000E+01   #  tanb               
     4     1.00000000E+00   #  sign(mu)           
     5    -1.00000000E+02   #  A0                 
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
         5     4.88991651E+00   # b-quark pole mass
         6     1.75000000E+02   # t-quark pole mass (not read by ME)
        24     7.98290131E+01   # W+
        25     1.25000000E+02   # h
        35     3.99960116E+02   # H
        36     3.99583917E+02   # A
        37     4.07879012E+02   # H+
   1000001     5.68441109E+05   # ~d_L
   2000001     5.45228462E+05   # ~d_R
   1000002     5.61119014E+05   # ~u_L
   2000002     5.49259265E+05   # ~u_R
   1000003     5.68441109E+05   # ~s_L
   2000003     5.45228462E+05   # ~s_R
   1000004     5.61119014E+05   # ~c_L
   2000004     5.49259265E+05   # ~c_R
   1000005     5.13065179E+05   # ~b_1
   2000005     5.43726676E+05   # ~b_2
   1000006     3.99668493E+05   # ~t_1
   2000006     5.85785818E+05   # ~t_2
   1000011     2.02915690E+05   # ~e_L
   2000011     1.44102799E+05   # ~e_R
   1000012     1.85258326E+05   # ~nu_eL
   1000013     2.02915690E+05   # ~mu_L
   2000013     1.44102799E+05   # ~mu_R
   1000014     1.85258326E+05   # ~nu_muL
   1000015     1.34490864E+05   # ~tau_1
   2000015     2.06867805E+05   # ~tau_2
   1000016     1.84708464E+05   # ~nu_tauL
   1000021     6.07713704E+05   # ~g
   1000022     1.00000000E+02   # ~chi_10
   1000023     1.00000000E-01   # ~chi_20
   1000025    -3.63756027E+05   # ~chi_30
   1000035     3.81729382E+05   # ~chi_40
   1000024     1.74738888E+05   # ~chi_1+
   1000037     3.79939320E+05   # ~chi_2+
   1000039     1.00000000E-01   #gravitino ACH
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.86364430E-01   # N_11
  1  2    -5.31103553E-02   # N_12
  1  3     1.46433995E-01   # N_13
  1  4    -5.31186117E-02   # N_14
  2  1     9.93505358E-02   # N_21
  2  2     9.44949299E-01   # N_22
  2  3    -2.69846720E-01   # N_23
  2  4     1.56150698E-01   # N_24
  3  1    -6.03388002E-02   # N_31
  3  2     8.77004854E-02   # N_32
  3  3     6.95877493E-01   # N_33
  3  4     7.10226984E-01   # N_34
  4  1    -1.16507132E-01   # N_41
  4  2     3.10739017E-01   # N_42
  4  3     6.49225960E-01   # N_43
  4  4    -6.84377823E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     9.16834859E-01   # U_11
  1  2    -3.99266629E-01   # U_12
  2  1     3.99266629E-01   # U_21
  2  2     9.16834859E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     9.72557835E-01   # V_11
  1  2    -2.32661249E-01   # V_12
  2  1     2.32661249E-01   # V_21
  2  2     9.72557835E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     5.53644960E-01   # O_{11}
  1  2     8.32752820E-01   # O_{12}
  2  1     8.32752820E-01   # O_{21}
  2  2    -5.53644960E-01   # O_{22}
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.38737896E-01   # O_{11}
  1  2     3.44631925E-01   # O_{12}
  2  1    -3.44631925E-01   # O_{21}
  2  2     9.38737896E-01   # O_{22}
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     2.82487190E-01   # O_{11}
  1  2     9.59271071E-01   # O_{12}
  2  1     9.59271071E-01   # O_{21}
  2  2    -2.82487190E-01   # O_{22}
#
BLOCK ALPHA  # Higgs mixing
          -1.13825210E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.67034192E+02  # DRbar Higgs Parameters
     1     3.57680977E+02   #  mu(Q)MSSM DRbar    
     2     9.74862403E+00   #  tan beta(Q)MSSM DRb
     3     2.44894549E+02   #  higgs vev(Q)MSSM DR
     4     1.66439065E+05   #  mA^2(Q)MSSM DRbar  
#
BLOCK GAUGE Q=  4.67034192E+02  # The gauge couplings
     3     1.10178679E+00   # g3(Q) MSbar
#
BLOCK AU Q=  4.67034192E+02  # The trilinear couplings
  3  3    -4.98129778E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.67034192E+02  # The trilinear couplings
  3  3    -7.97274397E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.67034192E+02  # The trilinear couplings
  3  3    -2.51776873E+02   # A_tau(Q) DRbar
#
BLOCK YU Q=  4.67034192E+02  # The Yukawa couplings
  3  3     8.92844550E-01   # y_t(Q) DRbar
#
BLOCK YD Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.38840206E-01   # y_b(Q) DRbar
#
BLOCK YE Q=  4.67034192E+02  # The Yukawa couplings
  3  3     1.00890810E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.67034192E+02  # The soft SUSY breaking masses at the scale Q
     1     1.01396534E+02   #  M_1(Q)             
     2     1.91504241E+02   #  M_2(Q)             
     3     5.88263031E+02   #  M_3(Q)             
    21     3.23374943E+04   #  mH1^2(Q)           
    22    -1.28800134E+05   #  mH2^2(Q)           
    31     1.95334764E+02   #  meL(Q)             
    32     1.95334764E+02   #  mmuL(Q)            
    33     1.94495956E+02   #  mtauL(Q)           
    34     1.36494061E+02   #  meR(Q)             
    35     1.36494061E+02   #  mmuR(Q)            
    36     1.34043428E+02   #  mtauR(Q)           
    41     5.47573466E+02   #  mqL1(Q)            
    42     5.47573466E+02   #  mqL2(Q)            
    43     4.98763839E+02   #  mqL3(Q)            
    44     5.29511195E+02   #  muR(Q)             
    45     5.29511195E+02   #  mcR(Q)             
    46     4.23245877E+02   #  mtR(Q)             
    47     5.23148807E+02   #  mdR(Q)             
    48     5.23148807E+02   #  msR(Q)             
    49     5.19867261E+02   #  mbR(Q)             
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY        23     2.41178499E+00   # Z width (SM calculation)
DECAY        24     2.00313003E+00   # W width (SM calculation)
#
#         PDG            Width
DECAY         6     1.56194983E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
     0.00000000E+00    2           5        37   # BR(t ->  b    H+)
     0.00000000E+00    2     1000006   1000022   # BR(t -> ~t_1 ~chi_10)
     0.00000000E+00    2     1000006   1000023   # BR(t -> ~t_1 ~chi_20)
     0.00000000E+00    2     1000006   1000025   # BR(t -> ~t_1 ~chi_30)
     0.00000000E+00    2     1000006   1000035   # BR(t -> ~t_1 ~chi_40)
     0.00000000E+00    2     2000006   1000022   # BR(t -> ~t_2 ~chi_10)
     0.00000000E+00    2     2000006   1000023   # BR(t -> ~t_2 ~chi_20)
     0.00000000E+00    2     2000006   1000025   # BR(t -> ~t_2 ~chi_30)
     0.00000000E+00    2     2000006   1000035   # BR(t -> ~t_2 ~chi_40)
#
#         PDG            Width
DECAY        25     1.98610799E-03   # h decays
#          BR         NDA      ID1       ID2
     1.45642955E-01    2          15       -15   # BR(H1 -> tau- tau+)
     8.19070713E-01    2           5        -5   # BR(H1 -> b bb)
     3.36338173E-02    2          24       -24   # BR(H1 -> W+ W-)
     1.65251528E-03    2          23        23   # BR(H1 -> Z Z)
#
#         PDG            Width
DECAY        35     5.74801389E-01   # H decays
#          BR         NDA      ID1       ID2
     1.39072676E-01    2          15       -15   # BR(H -> tau- tau+)
     4.84110879E-02    2           6        -6   # BR(H -> t tb)
     7.89500067E-01    2           5        -5   # BR(H -> b bb)
     3.87681171E-03    2          24       -24   # BR(H -> W+ W-)
     1.80454752E-03    2          23        23   # BR(H -> Z Z)
     0.00000000E+00    2          24       -37   # BR(H -> W+ H-)
     0.00000000E+00    2         -24        37   # BR(H -> W- H+)
     0.00000000E+00    2          37       -37   # BR(H -> H+ H-)
     1.73348101E-02    2          25        25   # BR(H -> h h)
     0.00000000E+00    2          36        36   # BR(H -> A A)
#
#         PDG            Width
DECAY        36     6.32178488E-01   # A decays
#          BR         NDA      ID1       ID2
     1.26659725E-01    2          15       -15   # BR(A -> tau- tau+)
     1.51081526E-01    2           6        -6   # BR(A -> t tb)
     7.19406137E-01    2           5        -5   # BR(A -> b bb)
     2.85261228E-03    2          23        25   # BR(A -> Z h)
     0.00000000E+00    2          23        35   # BR(A -> Z H)
     0.00000000E+00    2          24       -37   # BR(A -> W+ H-)
     0.00000000E+00    2         -24        37   # BR(A -> W- H+)
#
#         PDG            Width
DECAY        37     5.46962813E-01   # H+ decays
#          BR         NDA      ID1       ID2
     1.49435135E-01    2         -15        16   # BR(H+ -> tau+ nu_tau)
     8.46811711E-01    2           6        -5   # BR(H+ -> t bb)
     3.75315387E-03    2          24        25   # BR(H+ -> W+ h)
     0.00000000E+00    2          24        35   # BR(H+ -> W+ H)
     0.00000000E+00    2          24        36   # BR(H+ -> W+ A)
#
#         PDG            Width
DECAY   1000021     9.78994772E+03   # gluino decays
#          BR         NDA      ID1       ID2
     1.17253546E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     1.17253546E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     2.85225117E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     2.85225117E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     1.63002919E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     1.63002919E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     2.51361771E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     2.51361771E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     1.17253546E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     1.17253546E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     2.85225117E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     2.85225117E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     1.63002919E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     1.63002919E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     2.51361771E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     2.51361771E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     6.18400233E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     6.18400233E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     2.98337100E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     2.98337100E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     2.41165450E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     2.41165450E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
     3.79214611E-03    2     2000006        -6   # BR(~g -> ~t_2  tb)
     3.79214611E-03    2    -2000006         6   # BR(~g -> ~t_2* t )
#
#         PDG            Width
DECAY   1000006     4.11138964E+03   # stop1 decays
#          BR         NDA      ID1       ID2
     1.78234141E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.56196203E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.49018763E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     4.17595572E-03    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     5.29067921E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     7.42390294E-03    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     0.00000000E+00    2     1000021         6   # BR(~t_1 -> ~g      t )
     0.00000000E+00    2     1000005        37   # BR(~t_1 -> ~b_1    H+)
     0.00000000E+00    2     2000005        37   # BR(~t_1 -> ~b_2    H+)
     0.00000000E+00    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
     0.00000000E+00    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006     3.18803816E+09   # stop2 decays
#          BR         NDA      ID1       ID2
     9.73960616E-08    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     3.74994758E-07    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     5.11173659E-07    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     5.84781466E-07    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     6.22753723E-07    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.64900925E-07    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     0.00000000E+00    2     1000021         6   # BR(~t_2 -> ~g      t )
     1.06190027E-13    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     2.93006514E-14    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     2.11098797E-13    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     7.19619999E-14    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
     2.77642739E-15    2     2000005        37   # BR(~t_2 -> ~b_2    H+)
     6.71592848E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     3.19360370E-01    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     9.04412661E-03    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     1.45539338E+09   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.22796118E-07    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.19519621E-06    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.33344213E-08    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.59771402E-08    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     2.60317096E-06    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.00772416E-06    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     0.00000000E+00    2     1000021         5   # BR(~b_1 -> ~g      b )
     4.30224066E-13    2     1000006       -37   # BR(~b_1 -> ~t_1    H-)
     0.00000000E+00    2     2000006       -37   # BR(~b_1 -> ~t_2    H-)
     9.99995032E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
     0.00000000E+00    2     2000006       -24   # BR(~b_1 -> ~t_2    W-)
#
#         PDG            Width
DECAY   2000005     3.80381083E+08   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.42739588E-07    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.75364039E-07    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.14021377E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.55410040E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     8.10120839E-07    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     9.92111133E-07    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     0.00000000E+00    2     1000021         5   # BR(~b_2 -> ~g      b )
     1.74866570E-14    2     1000005        25   # BR(~b_2 -> ~b_1    h )
     3.14723659E-14    2     1000005        35   # BR(~b_2 -> ~b_1    H )
     5.81653230E-14    2     1000005        36   # BR(~b_2 -> ~b_1    A )
     6.07465227E-13    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     0.00000000E+00    2     2000006       -37   # BR(~b_2 -> ~t_2    H-)
     1.91101031E-02    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
     9.80886807E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
     0.00000000E+00    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     6.79163069E+03   # sup_L decays
#          BR         NDA      ID1       ID2
     5.69827876E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.20609800E-01    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     6.81403592E-04    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.34569105E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     6.53694382E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.09704444E-02    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     0.00000000E+00    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     1.22939873E+03   # sup_R decays
#          BR         NDA      ID1       ID2
     9.85169505E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     9.99485679E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.16192757E-03    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     3.67371099E-03    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     0.00000000E+00    2     1000024         1   # BR(~u_R -> ~chi_1+ d)
     0.00000000E+00    2     1000037         1   # BR(~u_R -> ~chi_2+ d)
     0.00000000E+00    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.48862362E+03   # sdown_L decays
#          BR         NDA      ID1       ID2
     2.01721945E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.14638399E-01    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.24729426E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.21875983E-02    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     6.15996275E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.57582390E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     0.00000000E+00    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.05055123E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     9.85295576E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     9.99613582E-03    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.13528606E-03    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.57300238E-03    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     0.00000000E+00    2    -1000024         2   # BR(~d_R -> ~chi_1- u)
     0.00000000E+00    2    -1000037         2   # BR(~d_R -> ~chi_2- u)
     0.00000000E+00    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.79163069E+03   # scharm_L decays
#          BR         NDA      ID1       ID2
     5.69827876E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.20609800E-01    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     6.81403592E-04    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.34569105E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     6.53694382E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.09704444E-02    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     0.00000000E+00    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     1.22939873E+03   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.85169505E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     9.99485679E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.16192757E-03    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     3.67371099E-03    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     0.00000000E+00    2     1000024         3   # BR(~c_R -> ~chi_1+ s)
     0.00000000E+00    2     1000037         3   # BR(~c_R -> ~chi_2+ s)
     0.00000000E+00    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.48862362E+03   # sstrange_L decays
#          BR         NDA      ID1       ID2
     2.01721945E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.14638399E-01    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.24729426E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.21875983E-02    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     6.15996275E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.57582390E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     0.00000000E+00    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.05055123E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     9.85295576E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     9.99613582E-03    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.13528606E-03    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.57300238E-03    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     0.00000000E+00    2    -1000024         4   # BR(~s_R -> ~chi_1- c)
     0.00000000E+00    2    -1000037         4   # BR(~s_R -> ~chi_2- c)
     0.00000000E+00    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     2.48013293E+03   # selectron_L decays
#          BR         NDA      ID1       ID2
     8.26529806E-02    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.42058646E-01    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     0.00000000E+00    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     0.00000000E+00    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     5.75288373E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     0.00000000E+00    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     7.22211908E+02   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.89956579E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.00434211E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     0.00000000E+00    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     0.00000000E+00    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
     0.00000000E+00    2    -1000024        12   # BR(~e_R -> ~chi_1- nu_e)
     0.00000000E+00    2    -1000037        12   # BR(~e_R -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   1000013     2.48013293E+03   # smuon_L decays
#          BR         NDA      ID1       ID2
     8.26529806E-02    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.42058646E-01    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     0.00000000E+00    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     0.00000000E+00    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     5.75288373E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     0.00000000E+00    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.22211908E+02   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.89956579E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.00434211E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     0.00000000E+00    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     0.00000000E+00    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
     0.00000000E+00    2    -1000024        14   # BR(~mu_R -> ~chi_1- nu_mu)
     0.00000000E+00    2    -1000037        14   # BR(~mu_R -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   1000015     8.14697781E+02   # stau_1 decays
#          BR         NDA      ID1       ID2
     7.74911545E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     8.49320981E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.40156357E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
     0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
     0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
#
#         PDG            Width
DECAY   2000015     8.56208892E+07   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.10761701E-06    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     9.05102030E-06    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     0.00000000E+00    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     0.00000000E+00    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.50153118E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     0.00000000E+00    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
     1.67558726E-16    2     1000016       -37   # BR(~tau_2 -> ~nu_tauL H-)
     5.20531305E-01    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     3.26370475E-13    2     1000015        25   # BR(~tau_2 -> ~tau_1 h)
     1.04431590E-13    2     1000015        35   # BR(~tau_2 -> ~tau_1 H)
     1.51087800E-13    2     1000015        36   # BR(~tau_2 -> ~tau_1 A)
     4.79441521E-01    2     1000015        23   # BR(~tau_2 -> ~tau_1 Z)
#
#         PDG            Width
DECAY   1000012     2.35646482E+03   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.17461392E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     2.60510113E-01    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     0.00000000E+00    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     0.00000000E+00    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     6.22028495E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     0.00000000E+00    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     2.35646482E+03   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.17461392E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     2.60510113E-01    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     0.00000000E+00    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     0.00000000E+00    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     6.22028495E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     0.00000000E+00    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.42318655E+07   # snu_tauL decays
#          BR         NDA      ID1       ID2
     8.06184784E-06    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.78798570E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     0.00000000E+00    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     0.00000000E+00    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.28665044E-05    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     0.00000000E+00    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
     7.65489436E-13    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
     0.00000000E+00    2    -2000015       -37   # BR(~nu_tauL -> ~tau_2+ H-)
     9.99931192E-01    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
     0.00000000E+00    2    -2000015       -24   # BR(~nu_tauL -> ~tau_2+ W-)
#
#         PDG            Width
DECAY   1000024     2.90777493E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.38976218E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     0.00000000E+00    3     1000023         2        -1   # BR(~chi_1+ -> ~chi_20 u    db)
     0.00000000E+00    3     1000025         2        -1   # BR(~chi_1+ -> ~chi_30 u    db)
     0.00000000E+00    3     1000035         2        -1   # BR(~chi_1+ -> ~chi_40 u    db)
     3.38976218E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     0.00000000E+00    3     1000023         4        -3   # BR(~chi_1+ -> ~chi_20 c    sb)
     0.00000000E+00    3     1000025         4        -3   # BR(~chi_1+ -> ~chi_30 c    sb)
     0.00000000E+00    3     1000035         4        -3   # BR(~chi_1+ -> ~chi_40 c    sb)
     0.00000000E+00    3     1000022         6        -5   # BR(~chi_1+ -> ~chi_10 t    bb)
     0.00000000E+00    3     1000023         6        -5   # BR(~chi_1+ -> ~chi_20 t    bb)
     0.00000000E+00    3     1000025         6        -5   # BR(~chi_1+ -> ~chi_30 t    bb)
     0.00000000E+00    3     1000035         6        -5   # BR(~chi_1+ -> ~chi_40 t    bb)
     1.12992024E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     0.00000000E+00    3     1000023       -11        12   # BR(~chi_1+ -> ~chi_20 e+   nu_e)
     0.00000000E+00    3     1000025       -11        12   # BR(~chi_1+ -> ~chi_30 e+   nu_e)
     0.00000000E+00    3     1000035       -11        12   # BR(~chi_1+ -> ~chi_40 e+   nu_e)
     1.12992024E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     0.00000000E+00    3     1000023       -13        14   # BR(~chi_1+ -> ~chi_20 mu+  nu_mu)
     0.00000000E+00    3     1000025       -13        14   # BR(~chi_1+ -> ~chi_30 mu+  nu_mu)
     0.00000000E+00    3     1000035       -13        14   # BR(~chi_1+ -> ~chi_40 mu+  nu_mu)
     9.60635164E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
     0.00000000E+00    3     1000023       -15        16   # BR(~chi_1+ -> ~chi_20 tau+ nu_tau)
     0.00000000E+00    3     1000025       -15        16   # BR(~chi_1+ -> ~chi_30 tau+ nu_tau)
     0.00000000E+00    3     1000035       -15        16   # BR(~chi_1+ -> ~chi_40 tau+ nu_tau)
     0.00000000E+00    3     1000021         2        -1   # BR(~chi_1+ -> ~g      u    db)
     0.00000000E+00    3     1000021         4        -3   # BR(~chi_1+ -> ~g      c    sb)
     0.00000000E+00    3     1000021         6        -5   # BR(~chi_1+ -> ~g      t    bb)
#
#         PDG            Width
DECAY   1000037     3.82195474E+09   # chargino2+ decays
#          BR         NDA      ID1       ID2
     0.00000000E+00    2     1000002        -1   # BR(~chi_2+ -> ~u_L   db)
     0.00000000E+00    2     2000002        -1   # BR(~chi_2+ -> ~u_R   db)
     0.00000000E+00    2    -1000001         2   # BR(~chi_2+ -> ~d_L*  u )
     0.00000000E+00    2    -2000001         2   # BR(~chi_2+ -> ~d_R*  u )
     0.00000000E+00    2     1000004        -3   # BR(~chi_2+ -> ~c_L   sb)
     0.00000000E+00    2     2000004        -3   # BR(~chi_2+ -> ~c_R   sb)
     0.00000000E+00    2    -1000003         4   # BR(~chi_2+ -> ~s_L*  c )
     0.00000000E+00    2    -2000003         4   # BR(~chi_2+ -> ~s_R*  c )
     0.00000000E+00    2     1000006        -5   # BR(~chi_2+ -> ~t_1   bb)
     0.00000000E+00    2     2000006        -5   # BR(~chi_2+ -> ~t_2   bb)
     0.00000000E+00    2    -1000005         6   # BR(~chi_2+ -> ~b_1*  t )
     0.00000000E+00    2    -2000005         6   # BR(~chi_2+ -> ~b_2*  t )
     1.30767748E-08    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL  e+  )
     1.30767748E-08    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.80590931E-08    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     3.38621332E-08    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     0.00000000E+00    2    -2000011        12   # BR(~chi_2+ -> ~e_R+    nu_e)
     3.38621332E-08    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     0.00000000E+00    2    -2000013        14   # BR(~chi_2+ -> ~mu_R+   nu_mu)
     1.84053172E-10    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     3.68763239E-08    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     4.36068306E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     5.66990923E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     5.04746069E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.48562054E-03    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     0.00000000E+00    2     1000035        24   # BR(~chi_2+ -> ~chi_40  W+)
     1.59312702E-07    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.74809893E-07    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.60091318E-07    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     2.83010094E-08    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     2.37342026E-07    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     3.23395293E-09    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     0.00000000E+00    2     1000035        37   # BR(~chi_2+ -> ~chi_40  H+)
#
#         PDG            Width
DECAY   1000022     1.56194983E+00   # neutralino1 decays
     1.00000000E+00    2     1000039        22   # BR(~chi_1 -> gamma gravitino )

#         PDG            Width
DECAY   1000023     0.00000000E+00   # neutralino2 decays
#           BR         NDA      ID1       ID2       ID3

#
#         PDG            Width
DECAY   1000025     5.14942367E+09   # neutralino3 decays
#          BR         NDA      ID1       ID2
     5.97982354E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.73636131E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     3.33282570E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.33282570E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     0.00000000E+00    2     1000037       -24   # BR(~chi_30 -> ~chi_2+   W-)
     0.00000000E+00    2    -1000037        24   # BR(~chi_30 -> ~chi_2-   W+)
     2.16458280E-08    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     2.91780601E-08    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.83297988E-08    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     4.96983448E-08    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     6.37027983E-08    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
     3.95485612E-08    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
     1.26220317E-07    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
     1.26220317E-07    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
     0.00000000E+00    2     1000037       -37   # BR(~chi_30 -> ~chi_2+   H-)
     0.00000000E+00    2    -1000037        37   # BR(~chi_30 -> ~chi_2-   H+)
     0.00000000E+00    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     0.00000000E+00    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     0.00000000E+00    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     0.00000000E+00    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     0.00000000E+00    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
     0.00000000E+00    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
     0.00000000E+00    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
     0.00000000E+00    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
     0.00000000E+00    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     0.00000000E+00    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     0.00000000E+00    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     0.00000000E+00    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     0.00000000E+00    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
     0.00000000E+00    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
     0.00000000E+00    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
     0.00000000E+00    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
     0.00000000E+00    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     0.00000000E+00    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     0.00000000E+00    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
     0.00000000E+00    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
     0.00000000E+00    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
     0.00000000E+00    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
     0.00000000E+00    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
     0.00000000E+00    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
     2.07329223E-10    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     2.07329223E-10    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.66089577E-10    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
     4.66089577E-10    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
     2.07329223E-10    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     2.07329223E-10    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     4.66089577E-10    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
     4.66089577E-10    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
     1.95142017E-09    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.95142017E-09    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     2.47262553E-09    2     2000015       -15   # BR(~chi_30 -> ~tau_2-   tau+)
     2.47262553E-09    2    -2000015        15   # BR(~chi_30 -> ~tau_2+   tau-)
     1.18663153E-09    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.18663153E-09    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.18663153E-09    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.18663153E-09    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.19156323E-09    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.19156323E-09    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     3.53971449E+09   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.77781347E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.40737930E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.52611980E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     4.77305562E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.77305562E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     4.98467099E-06    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     4.98467099E-06    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     4.06505191E-08    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.85555971E-08    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.10364727E-08    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     8.15571984E-08    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.81275227E-08    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     6.33957144E-08    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     9.50649856E-13    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     5.89465526E-12    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     8.04915953E-09    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.99583437E-07    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.99583437E-07    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     8.17819049E-11    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     8.17819049E-11    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     0.00000000E+00    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     0.00000000E+00    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     0.00000000E+00    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     0.00000000E+00    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     0.00000000E+00    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     0.00000000E+00    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     0.00000000E+00    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     0.00000000E+00    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     0.00000000E+00    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     0.00000000E+00    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     0.00000000E+00    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     0.00000000E+00    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     0.00000000E+00    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     0.00000000E+00    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     0.00000000E+00    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     0.00000000E+00    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     0.00000000E+00    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     0.00000000E+00    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     0.00000000E+00    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
     0.00000000E+00    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
     0.00000000E+00    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     0.00000000E+00    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     0.00000000E+00    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     0.00000000E+00    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     7.04836629E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     7.04836629E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     2.74446989E-09    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.74446989E-09    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     7.04836629E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     7.04836629E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     2.74446989E-09    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.74446989E-09    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.97997040E-09    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.97997040E-09    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.19981476E-08    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.19981476E-08    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.85404785E-08    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.85404785E-08    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.85404785E-08    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.85404785E-08    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.86082569E-08    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.86082569E-08    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )

""")
pcard.close()

fcard = open('proc_card_mg5.dat','w')
fcard.write("""

import model mssm
define j = g u c d s u~ c~ d~ s~
define j = p
define vl~ = ve~ vm~ vt~
generate p p > h1 j j, h1 > n1 n2 QED=99 QCD=99
output madevent -f

""")
fcard.close()

rcard = open('run_card2.dat','w')
rcard.write("""

#*********************************************************************
#                       MadGraph/MadEvent                            *
#                  http://madgraph.hep.uiuc.edu                      *
#                                                                    *
#                        run_card.dat                                *
#                                                                    *
#  This file is used to set the parameters of the run.               *
#                                                                    *
#  Some notation/conventions:                                        *
#                                                                    *
#   Lines starting with a '# ' are info or comments                  *
#                                                                    *
#   mind the format:   value    = variable     ! comment             *
#*********************************************************************
#
#*******************                                                 
# Running parameters
#*******************                                                 
#                                                                    
#*********************************************************************
# Tag name for the run (one word)                                    *
#*********************************************************************
  tag_vbf3     = run_tag ! name of the run 
#*********************************************************************
# Run to generate the grid pack                                      *
#*********************************************************************
  .false.     = gridpack  !True = setting up the grid pack
#*********************************************************************
# Number of events and rnd seed                                      *
# Warning: Do not generate more than 1M events in a single run       *
# If you want to run Pythia, avoid more than 50k events in a run.    *
#*********************************************************************
  10000 = nevents ! Number of unweighted events requested 
      0       = iseed   ! rnd seed (0=assigned automatically=default))
#*********************************************************************
# Collider type and energy                                           *
# lpp: 0=No PDF, 1=proton, -1=antiproton, 2=photon from proton,      *
#                                         3=photon from electron     *
#*********************************************************************
        1     = lpp1    ! beam 1 type 
        1     = lpp2    ! beam 2 type
     4000     = ebeam1  ! beam 1 total energy in GeV
     4000     = ebeam2  ! beam 2 total energy in GeV
#*********************************************************************
# Beam polarization from -100 (left-handed) to 100 (right-handed)    *
#*********************************************************************
        0     = polbeam1 ! beam polarization for beam 1
        0     = polbeam2 ! beam polarization for beam 2
#*********************************************************************
# PDF CHOICE: this automatically fixes also alpha_s and its evol.    *
#*********************************************************************
 'cteq6l1'    = pdlabel     ! PDF set                                     
#*********************************************************************
# Renormalization and factorization scales                           *
#*********************************************************************
 F        = fixed_ren_scale  ! if .true. use fixed ren scale
 F        = fixed_fac_scale  ! if .true. use fixed fac scale
 91.1880  = scale            ! fixed ren scale
 91.1880  = dsqrt_q2fact1    ! fixed fact scale for pdf1
 91.1880  = dsqrt_q2fact2    ! fixed fact scale for pdf2
 1        = scalefact        ! scale factor for event-by-event scales
#*********************************************************************
# Matching - Warning! ickkw > 1 is still beta
#*********************************************************************
 0        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching
 1        = highestmult      ! for ickkw=2, highest mult group
 1        = ktscheme         ! for ickkw=1, 1 Durham kT, 2 Pythia pTE
 1        = alpsfact         ! scale factor for QCD emission vx
 F        = chcluster        ! cluster only according to channel diag
 T        = pdfwgt           ! for ickkw=1, perform pdf reweighting
 5        = asrwgtflavor     ! highest quark flavor for a_s reweight
#*********************************************************************
# Automatic ptj and mjj cuts if xqcut > 0
# (turn off for VBF and single top processes)
#**********************************************************
   F  = auto_ptj_mjj  ! Automatic setting of ptj and mjj
#**********************************************************
#                                                                    
#**********************************
# BW cutoff (M+/-bwcutoff*Gamma)
#**********************************
  15  = bwcutoff      ! (M+/-bwcutoff*Gamma)
#**********************************************************
# Apply pt/E/eta/dr/mij cuts on decay products or not
# (note that etmiss/ptll/ptheavy/ht/sorted cuts always apply)
#**********************************************************
   T  = cut_decays    ! Cut decay products 
#*************************************************************
# Number of helicities to sum per event (0 = all helicities)
# 0 gives more stable result, but longer run time (needed for
# long decay chains e.g.).
# Use >=2 if most helicities contribute, e.g. pure QCD.
#*************************************************************
   0  = nhel          ! Number of helicities used per event
#*******************                                                 
# Standard Cuts
#*******************                                                 
#                                                                    
#*********************************************************************
# Minimum and maximum pt's (for max, -1 means no cut)                *
#*********************************************************************
 20  = ptj       ! minimum pt for the jets 
  0  = ptb       ! minimum pt for the b 
  0  = pta       ! minimum pt for the photons 
  0  = ptl       ! minimum pt for the charged leptons 
  0  = misset    ! minimum missing Et (sum of neutrino's momenta)
  0  = ptheavy   ! minimum pt for one heavy final state
 1.0 = ptonium   ! minimum pt for the quarkonium states
 -1  = ptjmax    ! maximum pt for the jets
 -1  = ptbmax    ! maximum pt for the b
 -1  = ptamax    ! maximum pt for the photons
 -1  = ptlmax    ! maximum pt for the charged leptons
 -1  = missetmax ! maximum missing Et (sum of neutrino's momenta)
#*********************************************************************
# Minimum and maximum E's (in the lab frame)                         *
#*********************************************************************
  0  = ej     ! minimum E for the jets 
  0  = eb     ! minimum E for the b 
  0  = ea     ! minimum E for the photons 
  0  = el     ! minimum E for the charged leptons 
 -1   = ejmax ! maximum E for the jets
 -1   = ebmax ! maximum E for the b
 -1   = eamax ! maximum E for the photons
 -1   = elmax ! maximum E for the charged leptons
#*********************************************************************
# Maximum and minimum absolute rapidity (for max, -1 means no cut)   *
#*********************************************************************
 5.5  = etaj    ! max rap for the jets 
  -1  = etab    ! max rap for the b
 2.5  = etaa    ! max rap for the photons 
 2.5  = etal    ! max rap for the charged leptons 
 0.6  = etaonium ! max rap for the quarkonium states
   0  = etajmin ! min rap for the jets
   0  = etabmin ! min rap for the b
   0  = etaamin ! min rap for the photons
   0  = etalmin ! main rap for the charged leptons
#*********************************************************************
# Minimum and maximum DeltaR distance                                *
#*********************************************************************
 0.4 = drjj    ! min distance between jets 
 0   = drbb    ! min distance between b's 
 0.2 = drll    ! min distance between leptons 
 0.2 = draa    ! min distance between gammas 
 0   = drbj    ! min distance between b and jet 
 0.2 = draj    ! min distance between gamma and jet 
 0.2 = drjl    ! min distance between jet and lepton 
 0   = drab    ! min distance between gamma and b 
 0   = drbl    ! min distance between b and lepton 
 0.2 = dral    ! min distance between gamma and lepton 
 -1  = drjjmax ! max distance between jets
 -1  = drbbmax ! max distance between b's
 -1  = drllmax ! max distance between leptons
 -1  = draamax ! max distance between gammas
 -1  = drbjmax ! max distance between b and jet
 -1  = drajmax ! max distance between gamma and jet
 -1  = drjlmax ! max distance between jet and lepton
 -1  = drabmax ! max distance between gamma and b
 -1  = drblmax ! max distance between b and lepton
 -1  = dralmax ! maxdistance between gamma and lepton
#*********************************************************************
# Minimum and maximum invariant mass for pairs                       *
#*********************************************************************
 400   = mmjj    ! min invariant mass of a jet pair 
 0   = mmbb    ! min invariant mass of a b pair 
 0   = mmaa    ! min invariant mass of gamma gamma pair
 0   = mmll    ! min invariant mass of l+l- (same flavour) lepton pair
 -1  = mmjjmax ! max invariant mass of a jet pair
 -1  = mmbbmax ! max invariant mass of a b pair
 -1  = mmaamax ! max invariant mass of gamma gamma pair
 -1  = mmllmax ! max invariant mass of l+l- (same flavour) lepton pair
#*********************************************************************
# Minimum and maximum invariant mass for all letpons                 *
#*********************************************************************
 0   = mmnl    ! min invariant mass for all letpons (l+- and vl) 
 -1  = mmnlmax ! max invariant mass for all letpons (l+- and vl) 
#*********************************************************************
# Minimum and maximum pt for 4-momenta sum of leptons                *
#*********************************************************************
 0   = ptllmin  ! Minimum pt for 4-momenta sum of leptons(l and vl)
 -1  = ptllmax  ! Maximum pt for 4-momenta sum of leptons(l and vl)
#*********************************************************************
# Inclusive cuts                                                     *
#*********************************************************************
 0  = xptj ! minimum pt for at least one jet  
 0  = xptb ! minimum pt for at least one b 
 0  = xpta ! minimum pt for at least one photon 
 0  = xptl ! minimum pt for at least one charged lepton 
#*********************************************************************
# Control the pt's of the jets sorted by pt                          *
#*********************************************************************
 20   = ptj1min ! minimum pt for the leading jet in pt
 20   = ptj2min ! minimum pt for the second jet in pt
 0   = ptj3min ! minimum pt for the third jet in pt
 0   = ptj4min ! minimum pt for the fourth jet in pt
 -1  = ptj1max ! maximum pt for the leading jet in pt 
 -1  = ptj2max ! maximum pt for the second jet in pt
 -1  = ptj3max ! maximum pt for the third jet in pt
 -1  = ptj4max ! maximum pt for the fourth jet in pt
 0   = cutuse  ! reject event if fails any (0) / all (1) jet pt cuts
#*********************************************************************
# Control the pt's of leptons sorted by pt                           *
#*********************************************************************
 0   = ptl1min ! minimum pt for the leading lepton in pt
 0   = ptl2min ! minimum pt for the second lepton in pt
 0   = ptl3min ! minimum pt for the third lepton in pt
 0   = ptl4min ! minimum pt for the fourth lepton in pt
 -1  = ptl1max ! maximum pt for the leading lepton in pt 
 -1  = ptl2max ! maximum pt for the second lepton in pt
 -1  = ptl3max ! maximum pt for the third lepton in pt
 -1  = ptl4max ! maximum pt for the fourth lepton in pt
#*********************************************************************
# Control the Ht(k)=Sum of k leading jets                            *
#*********************************************************************
 0   = htjmin ! minimum jet HT=Sum(jet pt)
 -1  = htjmax ! maximum jet HT=Sum(jet pt)
 0   = ihtmin  !inclusive Ht for all partons (including b)
 -1  = ihtmax  !inclusive Ht for all partons (including b)
 0   = ht2min ! minimum Ht for the two leading jets
 0   = ht3min ! minimum Ht for the three leading jets
 0   = ht4min ! minimum Ht for the four leading jets
 -1  = ht2max ! maximum Ht for the two leading jets
 -1  = ht3max ! maximum Ht for the three leading jets
 -1  = ht4max ! maximum Ht for the four leading jets
#*********************************************************************
# WBF cuts                                                           *
#*********************************************************************
 0   = xetamin ! minimum rapidity for two jets in the WBF case  
 3.0   = deltaeta ! minimum rapidity for two jets in the WBF case 
#*********************************************************************
# maximal pdg code for quark to be considered as a light jet         *
# (otherwise b cuts are applied)                                     *
#*********************************************************************
 4 = maxjetflavor    ! Maximum jet pdg code
#*********************************************************************
# Jet measure cuts                                                   *
#*********************************************************************
 0   = xqcut   ! minimum kt jet measure between partons
#*********************************************************************

""")
rcard.close()

process_dir = new_process()

nmass=100.0 #default
nmasslow=0.1 #default
xqcut = 0 #default
jetmin = 0 #default

if runArgs.runNumber==1:
    nmass = 0
    xqcut = 15
    jetmin = 20
elif runArgs.runNumber==183941:
    nmass = 65.0
elif runArgs.runNumber==183942:
    nmass = 70.0
elif runArgs.runNumber==183943:
    nmass = 80.0
elif runArgs.runNumber==183944:
    nmass = 90.0
elif runArgs.runNumber==183945:
    nmass = 100.0
elif runArgs.runNumber==183946:
    nmass = 110.0
elif runArgs.runNumber==183947:
    nmass = 120.0
elif runArgs.runNumber==202853:
    nmass = 110.0
    nmasslow = 10.0
elif runArgs.runNumber==202854:
    nmass = 90.0
    nmasslow = 10.0
elif runArgs.runNumber==202855:
    nmass = 90.0
    nmasslow = 30.0
elif runArgs.runNumber==202856:
    nmass = 70.0
    nmasslow = 10.0
elif runArgs.runNumber==202857:
    nmass = 70.0
    nmasslow = 30.0
elif runArgs.runNumber==202858:
    nmass = 70.0
    nmasslow = 50.0
elif runArgs.runNumber==204590:
    nmass = 100.0
    nmasslow = 10.0 
elif runArgs.runNumber==204591:
    nmass = 80.0
    nmasslow = 10.0 
elif runArgs.runNumber==204592:
    nmass = 65.0
    nmasslow = 10.0 
elif runArgs.runNumber==204593:
    nmass = 100.0
    nmasslow = 20.0 
elif runArgs.runNumber==204594:
    nmass = 80.0
    nmasslow = 20.0 
elif runArgs.runNumber==204595:
    nmass = 70.0
    nmasslow = 20.0 
elif runArgs.runNumber==204596:
    nmass = 65.0
    nmasslow = 20.0 
elif runArgs.runNumber==204597:
    nmass = 80.0
    nmasslow = 30.0 
elif runArgs.runNumber==204598:
    nmass = 65.0
    nmasslow = 30.0 
elif runArgs.runNumber==204599:
    nmass = 80.0
    nmasslow = 40.0 
elif runArgs.runNumber==204600:
    nmass = 70.0
    nmasslow = 40.0 
elif runArgs.runNumber==204601:
    nmass = 65.0
    nmasslow = 40.0 
elif runArgs.runNumber==204621:
    nmass = 90.0
    nmasslow = 20.0 
else:
    print "ERROR: don't know what to do with runNumber ",runArgs.runNumber


# Grab the run card and move it into place
runcard = open('run_card2.dat');#subprocess.Popen(['get_files','-data','run_card.SM.dat'])
#runcard.wait()
if not os.access('run_card2.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card2.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(5100))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' xptj ' in line:
            newcard.write('   %i      = xptj ! minimum pt for at least one jet \n'%(jetmin))
        elif ' ickkw  ' in line:
            newcard.write('   0      = ickkw  ! 0 no matching, 1 MLM, 2 CKKW matching \n')
        elif ' xpta ' in line:
            newcard.write('   0      = xpta ! minimum pt for at least one photon \n')
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(runArgs.ecmEnergy/2)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(runArgs.ecmEnergy/2)))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

#fix up the param_card.dat
paramcard = open('param_card2.dat');
#paramcard.wait()
if not os.access('param_card2.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('param_card2.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if '1000022     1.00000000E+02   # ~chi_10' in line:
            newcard.write('1000022  %f   # ~chi_10\n'%(nmass))
        elif '1000023     1.00000000E-01   # ~chi_20' in line:
            newcard.write('1000023     %f   # ~chi_20\n'%(nmasslow))
        elif '1000039     1.00000000E-01   #gravitino ACH' in line:
            newcard.write('1000039     %f   #gravitino ACH\n'%(nmasslow))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

#########################
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_hn1n2jj_'+str(nmass)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")

#include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
#Pythia = topAlg.Pythia
#Pythia.PythiaCommand = ["pyinit user madgraph"]
#Pythia.PythiaCommand +=  [
#            "pystat 1 3 4 5",
#            "pyinit dumpr 1 5",
#            "pyinit pylistf 1",
#            ]
##include ( "MC12JobOptions/Pythia_Tauola.py" )
#include ( "MC12JobOptions/Pythia_Photos.py" )

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

#from MC12JobOptions.MadGraphEvgenConfig import evgenConfig
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_hn1n2jj_'+str(nmass)
evgenConfig.keywords = ["MSSM","hn1n2jj"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

