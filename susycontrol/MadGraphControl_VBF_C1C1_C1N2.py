from MadGraphControl.MadGraphUtils import *

###########################################
#Remove old *.dat files for new generation
###########################################
command='rm *.dat'
os.system(command)

#######################################
# Create the process Card
#######################################
fcard = open('proc_card_mg5.dat','w')


################################################
#Initialization of the variables in run_card.dat  
################################################
#nevents=-1
if hasattr(runArgs,'maxEvents'):
    nevents = runArgs.maxEvents
else:
    print 'Nevents: Using test value of 1000 events'
    nevents=1000

evt_multiplier = 1.1
    
bwcutoff=10000000
ickkw=0
xqcut=0
qcut=0
ptj=5
dRjj=0.4
mmjj=10
ptb=0
pta=0
ptl=0
misset=0
ptheavy=0
ptonium=0
etaa=-1
etal=-1
etaonium=-1
drll=0
draa=0
draj=0
drjl=0
dral=0
ptj1min=0
htjmin=0
deltaeta=0
nJobs=1
stringy=''        
beamEnergy=runArgs.ecmEnergy/2.0
lhapdfid=-1

prefix=''
suffix='.slha'

gridpackMode=False
cluster_type=None
cluster_queue=None



##################################################
# Preparing process card for VBF C1N2 production
##################################################
if runArgs.runNumber==204911 or (runArgs.runNumber>=204631 and runArgs.runNumber<=204638):
    fcard.write("""
    import model mssm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve
    define vl~ = ve~ vm~ vt~
    # Define SUSY particles
    define sll   = el+ mul+ ta1+ el- mul- ta1-
    define sll+  = el+
    define sll-  = el- mul- ta1-
    define slr+  = er+ mur+ ta2+
    define slr-  = er- mur- ta2-
    define svl   = sve svm svt
    define svl~  = sve~ svm~ svt~
    # Specify process(es) to run
    generate    p p > x1+ n2 j j / go ul ur dl dr cl cr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ t1~ t2~ b1~ b2~ QCD=0 @1
    add process p p > x1- n2 j j / go ul ur dl dr cl cr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ t1~ t2~ b1~ b2~ QCD=0 @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    bwcutoff=10000000
    ickkw=0
    xqcut=0
    qcut=0
    deltaeta=4.2
    gridpackMode=False
    prefix='susy_simplifiedModel_BinoWino_VBF_C1N2_'
    name="c1n2VBFBW"


#################################################
# Preparing process card for C1C1 OS VBF production 
#################################################
elif runArgs.runNumber==204910 or (runArgs.runNumber>=204623 and runArgs.runNumber<=204630):
    fcard.write("""
    import model mssm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define sll+  = el+ mul+ ta1+
    define sll-  = el- mul- ta1-
    define slr+  = er+ mur+ ta2+
    define slr-  = er- mur- ta2-
    define svl   = sve svm svt
    define svl~  = sve~ svm~ svt~
    generate    p p > x1+ x1- j j / go ul ur dl dr cl cr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ t1~ t2~ b1~ b2~ QCD=0 @1
    output -f
    """)
    fcard.close()
    iexcfile=0
    bwcutoff=10000000
    ickkw=0
    xqcut=0
    qcut=0
    deltaeta=4.2
    prefix='susy_simplifiedModel_BinoWino_VBF_C1C1_OS_'
    name="c1c1VBFBW"
    gridpackMode=False

#################################################
# Preparing process card for C1C1 SS VBF production 
#################################################
elif (runArgs.runNumber>=204517 and runArgs.runNumber<=204532) or runArgs.runNumber==204909:
    fcard.write("""
    import model mssm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define sll+  = el+ mul+ ta1+
    define sll-  = el- mul- ta1-
    define slr+  = er+ mur+ ta2+
    define slr-  = er- mur- ta2-
    define svl   = sve svm svt
    define svl~  = sve~ svm~ svt~
    generate    p p > x1- x1- j j / go ul ur dl dr cl cr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ t1~ t2~ b1~ b2~ QCD=0 @1
    add process p p > x1+ x1+ j j / go ul ur dl dr cl cr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ t1~ t2~ b1~ b2~ QCD=0 @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    bwcutoff=10000000
    ickkw=0
    xqcut=0
    qcut=0
    deltaeta=0.0
    gridpackMode=False
    if (runArgs.runNumber>=204517 and runArgs.runNumber<=204524) or runArgs.runNumber==204909:
        prefix='susy_simplifiedModel_BinoWino_VBF_C1C1_SS_'
        name="c1c1VBFBW"
    elif runArgs.runNumber>=204525 and runArgs.runNumber<=204532:
        name="c1c1VBFBH"
        prefix='susy_simplifiedModel_BinoHiggsino_VBF_C1C1_SS_'

else:
    print "ERROR: RunNumber is out of range!"
    

    
##########################################################
# Grab the run card and move it into place
# Change generator cuts according to the production request
##########################################################
if not gridpackMode:
    
    runcard = subprocess.Popen(['get_files','-data','run_card.bH.dat'])
    runcard.wait()
    if not os.access('run_card.bH.dat',os.R_OK):
        print 'ERROR: Could not get run card'
    elif os.access('run_card.dat',os.R_OK):
        print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
    else:
        oldcard = open('run_card.bH.dat','r')
        newcard = open('run_card.dat','w')
        for line in oldcard:
        
            if ' = ickkw ' in line:
                newcard.write('%i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))            
            
            elif ' = bwcutoff' in line:
                newcard.write('%i        = bwcutoff         !  \n'%(bwcutoff))

            elif ' = ptj ' in line:
                newcard.write('%i        = ptj         ! minimum pt for the jets  \n'%(ptj))

            elif ' = dRjj ' in line:
                newcard.write('%i        = dRjj         ! min distance between jets   \n'%(dRjj))

            elif ' = mmjj ' in line:
                newcard.write('%i        = mmjj         ! min distance between jets   \n'%(mmjj))

            elif ' = ptb ' in line:
                newcard.write('%i        = ptb         ! minimum pt for the b   \n'%(ptb))
            
            elif ' = pta ' in line:
                newcard.write('%i        = pta         ! minimum pt for the photons   \n'%(pta))
            
            elif ' = ptl ' in line:
                newcard.write('%i        = ptl         ! minimum pt for the charged leptons   \n'%(ptl))
            
            elif ' = misset ' in line:
                newcard.write('%i        = misset         ! minimum missing Et (sum of neutrinos momenta)   \n'%(misset))
            
            elif ' = ptheavy ' in line:
                newcard.write('%i        = ptheavy         ! minimum pt for one heavy final state   \n'%(ptheavy))
            
            elif ' = ptonium ' in line:
                newcard.write('%i        = ptonium         ! minimum pt for the quarkonium states   \n'%(ptonium))
            
            elif ' = etaa ' in line:
                newcard.write('%i        = etaa         ! max rap for the photons   \n'%(etaa))
            
            elif ' = etal ' in line:
                newcard.write('%i        = etal         ! max rap for the charged leptons    \n'%(etal))

            elif ' = etaonium ' in line:
                newcard.write('%i        = etaonium         ! max rap for the quarkonium states    \n'%(etaonium))
            
            elif ' = drll ' in line:
                newcard.write('%i        = drll         ! min distance between leptons   \n'%(drll))
            
            elif ' = draa ' in line:
                newcard.write('%i        = draa         ! min distance between gammas   \n'%(draa))
            
            elif ' = draj ' in line:
                newcard.write('%i        = draj         ! min distance between gamma and jet   \n'%(draj))
            
            elif ' = drjl ' in line:
                newcard.write('%i        = drjl         ! min distance between jet and lepton   \n'%(drjl))
            
            elif ' = dral ' in line:
                newcard.write('%i        = dral         ! min distance between gamma and lepton   \n'%(dral))

            elif ' = ptj1min ' in line:
                newcard.write('%i        = ptj1min         ! minimum pt for the leading jet in pt   \n'%(ptj1min))

            elif ' = htjmin ' in line:
                newcard.write('%i        = htjmin         ! minimum jet HT=Sum(jet pt)   \n'%(htjmin))        

            elif '= deltaeta ' in line:
                newcard.write('%f        = deltaeta         ! minimum rapidity for two jets in the WBF case    \n'%(deltaeta))        
            
            elif ickkw==1 and ' = xqcut' in line:
                newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
            
            elif ickkw==1 and ' xptj ' in line:
                newcard.write('   %f      = xptj ! minimum pt for at least one jet \n'%(xqcut))
            
            elif ickkw==0 and ' = xqcut' in line:
                newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
            
            elif ' nevents ' in line:
                newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents*evt_multiplier))
            
            elif ' iseed ' in line:
                newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
            
            elif ' ebeam1 ' in line:
                newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
            
            elif ' ebeam2 ' in line:
                newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
            
            elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
                newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
                newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
            
            else:
                newcard.write(line)
                

        oldcard.close()
        newcard.close()

###############################################
# Print out the process card, slha and run cards
###############################################
#procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
#procCard.wait()

datasetID=str(runArgs.runNumber)
slhaFile=prefix+datasetID+suffix

print "Name of the slha file: ", slhaFile
evgenConfig.auxfiles += [ slhaFile, 'MSSM.model' ]

from PyJobTransformsCore.trfutil import get_files
get_files( slhaFile, keepDir=True, errorIfNotFound=True )

#paramCard = subprocess.Popen(['cat',slhaFile])
#paramCard.wait()

#runCard = subprocess.Popen(['cat','run_card.dat'])
#runCard.wait()




runName=str(datasetID)
print 'Generating process and events for dataseID ', runName

#@@ Addition for gridpack
if not hasattr(runArgs, "inputGenConfFile"):
    ## Generate process ##
    if gridpackMode:
        print 'Generating process for gridpack'
    else:
        print 'Generating process and events'

    process_dir = new_process()
    if process_dir==1 :
        print "ERROR: THERE IS A PROBLEM WITH THE PROCESS DIRECTORY"
    else:
        print "process_dir = ", process_dir

    
    generate(run_card_loc='run_card.dat',param_card_loc=slhaFile,mode=1,njobs=nJobs,proc_dir=process_dir,run_name=runName,grid_pack=gridpackMode,cluster_type=cluster_type,cluster_queue=cluster_queue)

    if gridpackMode:
        if not os.access(process_dir+'/'+runName+'_gridpack.tar.gz',os.R_OK):
            print 'ERROR: No gridpack found at '+process_dir
        else: 
            print 'Gridpack generation complete - exiting Generate transform.'
            theApp.finalize()
            theApp.exit()


else:

    seed=runArgs.randomSeed        

    if gridpackMode and hasattr(runArgs, "inputGenConfFile"):
        ## Generate events from gridpack ##
        print 'Generating events using gripack mode'
        
        gridpack_dir='madevent/'
        generate_from_gridpack(run_name=runName,gridpack_dir=gridpack_dir,nevents=nevents*evt_multiplier,random_seed=seed)
        process_dir=gridpack_dir
    
#process_dir = new_process()

    
#########################################        
# Pass to Pythia ##
#########################################
print 'Pass generated events to Pythia'
stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

if process_dir!=1 :
    arrange_output(run_name=runName,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)
else :
    print "ERROR: PROBLEM WITH THE PROCESS_DIR...ARRANGE_OUTPUT IS NOT PERFORMED"

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
syst_mod=None
decaytype=None
gentype=None
use_Tauola=True
use_METfilter=False
use_MultiElecMuTauFilter=False
use_MultiLeptonFilter=False
outputDS=stringy+'._00001.events.tar.gz'

runArgs.qcut = qcut
runArgs.inputGeneratorFile = outputDS
if 'syst_mod' in dir():
    runArgs.syst_mod = syst_mod

runArgs.decaytype = decaytype
runArgs.gentype = gentype
runArgs.use_Tauola = use_Tauola
runArgs.use_METfilter = use_METfilter
runArgs.use_MultiElecMuTauFilter = use_MultiElecMuTauFilter
runArgs.use_MultiLeptonFilter = use_MultiLeptonFilter

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = 'MadGraph_SM_VBF'
evgenConfig.keywords = ["SM","C1N2","C1C1","BoostedGaugino","CompressedSpectra", "VBF"]
evgenConfig.contact = ["Andreas.Petridis@cern.ch"]
evgenConfig.inputfilecheck = stringy
#evgenConfig.minevents = 1000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'    


include( 'MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py' )



