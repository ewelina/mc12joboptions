# SUSY Herwig++ jobOptions for direct stop pair production grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_Stop_Stau_mc12points_allDecays.py' )
try:
    (stop, stau, mu) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_Stop%s_Stau%s_mu%s.slha' % (stop, stau, mu)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Stop pair production, with stop decaying into stau'
evgenConfig.keywords = ['SUSY','stop','stau']
evgenConfig.contact  = ['ilaria.besana@mi.infn.it','tommaso.lari@mi.infn.it','ewan.hill@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#============================================================
#
# Filter events
#
#============================================================
#dileptonic filter:
if (runArgs.runNumber >= 186700 and runArgs.runNumber <= 186701):
    include ( 'MC12JobOptions/TauFilter.py' )
    topAlg.TauFilter.Ntaus = 2
    topAlg.TauFilter.EtaMaxe = 2.8
    topAlg.TauFilter.EtaMaxmu = 2.8
    topAlg.TauFilter.EtaMaxhad = 2.8
    topAlg.TauFilter.Ptcute = 8000.
    topAlg.TauFilter.Ptcutmu = 8000.
    topAlg.TauFilter.Ptcuthad = 1000000000.

if (runArgs.runNumber >= 202778 and runArgs.runNumber <= 202780):
    include ( 'MC12JobOptions/TauFilter.py' )
    topAlg.TauFilter.Ntaus = 2
    topAlg.TauFilter.EtaMaxe = 2.8
    topAlg.TauFilter.EtaMaxmu = 2.8
    topAlg.TauFilter.EtaMaxhad = 2.8
    topAlg.TauFilter.Ptcute = 8000.
    topAlg.TauFilter.Ptcutmu = 8000.
    topAlg.TauFilter.Ptcuthad = 1000000000.

#============= lep-had filter :
if (runArgs.runNumber >= 202781 and runArgs.runNumber <= 202783):
    include ( 'MC12JobOptions/TauFilter.py' )
    topAlg.TauFilter.Ntaus = 1
    topAlg.TauFilter.EtaMaxe = 2.8
    topAlg.TauFilter.EtaMaxmu = 2.8
    topAlg.TauFilter.EtaMaxhad = 2.8
    topAlg.TauFilter.Ptcute = 1000000.
    topAlg.TauFilter.Ptcutmu = 1000000.
    topAlg.TauFilter.Ptcuthad = 12000.

    if not hasattr(topAlg, "LeptonFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
        topAlg += LeptonFilter()
    if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.RequireAlgs += ["LeptonFilter"]
    topAlg.LeptonFilter.Ptcut = 20000.
    topAlg.LeptonFilter.Etacut = 2.8

#============= MET filter (MET>150 GeV):
if (runArgs.runNumber >= 186702 and runArgs.runNumber <= 186717):
    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    if not hasattr(topAlg, "METFilter"):
        from GeneratorFilters.GeneratorFiltersConf import METFilter
        topAlg += METFilter()
    if "METFilter" not in StreamEVGEN.RequireAlgs:
        StreamEVGEN.RequireAlgs +=  [ "METFilter" ]
    topAlg.METFilter.MissingEtCut = 150*GeV
    topAlg.METFilter.MissingEtCalcOption = 1

#============= MET filter2 (60 GeV<MET<150 GeV):
if (runArgs.runNumber >= 186718 and runArgs.runNumber <= 186733):
    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    metveto = METFilter(name="metveto")
    metveto.MissingEtCut = 150*GeV
    metveto.MissingEtCalcOption = 1
    topAlg += metveto
    if "metveto" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.VetoAlgs +=  [ "metveto" ]

    from GeneratorFilters.GeneratorFiltersConf import METFilter
    metfilt = METFilter(name ="metfilt")
    metfilt.MissingEtCut = 60*GeV
    metfilt.MissingEtCalcOption = 1
    topAlg += metfilt
    if "metfilt" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.RequireAlgs +=  [ "metfilt" ]


#============= lepton&MET filter (1 lepton of 20 GeV & MET<60 GeV):
if (runArgs.runNumber >= 186734 and runArgs.runNumber <= 186749):
    if not hasattr(topAlg, "LeptonFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
        topAlg += LeptonFilter()
    if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.RequireAlgs += ["LeptonFilter"]
    topAlg.LeptonFilter.Ptcut = 20000.
    topAlg.LeptonFilter.Etacut = 2.8

    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    if not hasattr(topAlg, "METFilter"):
        from GeneratorFilters.GeneratorFiltersConf import METFilter
        topAlg += METFilter()
    if "METFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.VetoAlgs +=  [ "METFilter" ]
    topAlg.METFilter.MissingEtCut = 60*GeV
    topAlg.METFilter.MissingEtCalcOption = 1

#============= lepton or MET filter (1 lepton of 20 GeV OR MET>60 GeV):
if (runArgs.runNumber >= 186750 and runArgs.runNumber <= 186817):
    if not hasattr(topAlg, "LeptonFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
        topAlg += LeptonFilter()
    if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs += ["LeptonFilter"]
    topAlg.LeptonFilter.Ptcut = 20000.
    topAlg.LeptonFilter.Etacut = 2.8

    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    if not hasattr(topAlg, "METFilter"):
        from GeneratorFilters.GeneratorFiltersConf import METFilter
        topAlg += METFilter()
    if "METFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs +=  [ "METFilter" ]
    topAlg.METFilter.MissingEtCut = 60*GeV
    topAlg.METFilter.MissingEtCalcOption = 1

if (runArgs.runNumber >= 202784 and runArgs.runNumber <= 202825):
    if not hasattr(topAlg, "LeptonFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
        topAlg += LeptonFilter()
    if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs += ["LeptonFilter"]
    topAlg.LeptonFilter.Ptcut = 20000.
    topAlg.LeptonFilter.Etacut = 2.8

    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    if not hasattr(topAlg, "METFilter"):
        from GeneratorFilters.GeneratorFiltersConf import METFilter
        topAlg += METFilter()
    if "METFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs +=  [ "METFilter" ]
    topAlg.METFilter.MissingEtCut = 60*GeV
    topAlg.METFilter.MissingEtCalcOption = 1

if (runArgs.runNumber >= 205086 and runArgs.runNumber <= 205097):
    if not hasattr(topAlg, "LeptonFilter"):
        from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
        topAlg += LeptonFilter()
    if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs += ["LeptonFilter"]
    topAlg.LeptonFilter.Ptcut = 20000.
    topAlg.LeptonFilter.Etacut = 2.8

    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    if not hasattr(topAlg, "METFilter"):
        from GeneratorFilters.GeneratorFiltersConf import METFilter
        topAlg += METFilter()
    if "METFilter" not in StreamEVGEN.AcceptAlgs:
        StreamEVGEN.AcceptAlgs +=  [ "METFilter" ]
    topAlg.METFilter.MissingEtCut = 60*GeV
    topAlg.METFilter.MissingEtCalcOption = 1




#==============================================================
#
# End of job options file
#
###############################################################
