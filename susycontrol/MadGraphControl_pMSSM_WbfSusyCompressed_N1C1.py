# SUSY MadGraph+Pythia8 jobOptions for pMSSM simulation

from MadGraphControl.MadGraphUtils import *

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 1.0
num_events=40000

if 'EventMultiplier' in dir(): evt_multiplier=EventMultiplier
if hasattr(runArgs,'EventMultiplier'): evt_multiplier=runArgs.EventMultiplier
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents>0: num_events=runArgs.maxEvents
nevts=num_events*evt_multiplier

ickkw=0
xqcut = 20 #see: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Matching#

fcard = open('proc_card_mg5.dat','w')
fcard.write('import model mssm\n')
fcard.write('define p = g u c d s b u~ c~ d~ s~ b~\n')
fcard.write('define j = g u c d s b u~ c~ d~ s~ b~\n')
fcard.write('define susyExcluded = ul ur cl cr t1 t2 dl dr sl sr b1 b2 sve svm svt ul~ ur~ cl~ cr~ t1~ t2~ dl~ dr~ sl~ sr~ b1~ b2~ sve~ svm~ svt~ el- er- mul- mur- ta1- ta2- el+ er+ mul+ mur+ ta1+ ta2+ h2 h3 h- h+ go\n')
fcard.write('generate p p > n1 x1+ p p / susyExcluded QCD=10 QED=10 @1\n')
fcard.write('add process p p > n1 x1- p p / susyExcluded QCD=10 QED=10 @2\n')
fcard.write('output -f %s\n'%os.getcwd())

name="pMSSM_ewk"
fcard.close()
print "proc card:"
print fcard

#--------------------------------------------------------------
# Beam energy
#--------------------------------------------------------------
### As suggested by MC generator experts
beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#--------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#--------------------------------------------------------------
# Grab the run card and move it into place
import subprocess
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    evgenLog.error('Could not get run card')
elif os.access('run_card.dat',os.R_OK):
    evgenLog.error('Old run card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ptj ' in line:
            newcard.write('   %f      = ptj ! minimum pt for the jets \n'%(xqcut))
        elif ' = ptb ' in line:
            newcard.write('   %f      = ptb ! minimum pt for the b \n'%(xqcut))
        elif ' = xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%int(nevts))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ickkw' in line:
            newcard.write('  %i         = ickkw   ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(0))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


print "Modified run_card.dat:"
rcard = open('run_card.dat','r')
print rcard.read()
rcard.close()

process_dir = new_process()
slha_file = 'susy_'+str(runArgs.runNumber)+'_WbfSusyCompressedN1C1.slha'

evgenLog.info("Using paramCard %s" % slha_file)
proccard = subprocess.Popen(['get_files','-data',slha_file])
proccard.wait()

generate(run_card_loc='run_card.dat',param_card_loc=slha_file,mode=0,njobs=1,run_name='EWK',proc_dir=process_dir)

stringy = 'madgraph.'+str(name)

print "PROC_mssm_0", os.listdir("PROC_mssm_0")
print "Events", os.listdir("PROC_mssm_0/Events")

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='EWK',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------
# Pythia 8
#--------------------
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.auxfiles += [ slha_file ]
topAlg.Pythia8.Commands += ['SLHA:file = '+slha_file]

#--------------------------------------------------------------
# MC12 metadata
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords = ["pMSSM"]
evgenConfig.contact  = [ "Andrew.James.Nelson@cern.ch" ]
evgenConfig.inputfilecheck = stringy
