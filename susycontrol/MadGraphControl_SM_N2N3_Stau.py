#N2N3 stau grid settings 
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )
#grid before N2 mass bug
sumList_SM_N2N3_stau = [50, 100, 150, 200, 250, 300, 400  ]
points_SM_N2N3_stau = []
for massN2 in sumList_SM_N2N3_stau:
    sumList_SM_N1_stau = [0, 50, 100, 150, 200, 250, 300, 350 ]
    for item in sumList_SM_N1_stau:
        if item < massN2:
            if massN2>250 and item in [50,150]: continue
            if massN2>300 and item==250: continue
            if massN2>400 and item==350: continue
            massN1 = item
            massStau = (massN2+massN1)/2
            point = [massN2, massN1, massStau]
            points_SM_N2N3_stau += [ point ]

#new grid with negative N2 mass and tighter filter
points_SM_N2N3_stau_new = [[100,0,50],[100,50,75],[150,0,75],[150,50,100],[150,100,125],[200,0,100],[200,50,125],[200,100,150],[250,0,125],[250,50,150],[250,100,175],[300,0,150],[300,50,175],[300,100,200],[350,0,175]]


# Standard points
therun = runArgs.runNumber-178742
if therun>=0 and therun<len(points_SM_N2N3_stau):
    evgenLog.info('Registered generation of grid 27, direct N2 N3 stau point '+str(therun))
    masses['1000025'] = points_SM_N2N3_stau[therun][0]
    masses['1000023'] = -points_SM_N2N3_stau[therun][0]
    masses['1000022'] = points_SM_N2N3_stau[therun][1]
    masses['1000015'] = points_SM_N2N3_stau[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_stau[therun][0]) + ' - ' + str(points_SM_N2N3_stau[therun][1]) + ' - ' + str(points_SM_N2N3_stau[therun][2]))
    stringy = str(int(points_SM_N2N3_stau[therun][0]))+'_'+str(int(points_SM_N2N3_stau[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=3
    runArgs.MultiElecMuTauFilterPtCut=5
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=1
    gentype = 'N2N3'
    decaytype = 'Stau'
    njets = 1
    use_decays = False


# Negative N2 mass tighter filter points
therun = runArgs.runNumber-156979
if therun>=0 and therun<len(points_SM_N2N3_stau_new):
    evgenLog.info('Registered generation of grid 27, direct N2 N3 stau point '+str(therun))
    masses['1000025'] = points_SM_N2N3_stau_new[therun][0]
    masses['1000023'] = -points_SM_N2N3_stau_new[therun][0]
    masses['1000022'] = points_SM_N2N3_stau_new[therun][1]
    masses['1000015'] = points_SM_N2N3_stau_new[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_stau_new[therun][0]) + ' - ' + str(points_SM_N2N3_stau_new[therun][1]) + ' - ' + str(points_SM_N2N3_stau_new[therun][2]))
    stringy = str(int(points_SM_N2N3_stau_new[therun][0]))+'_'+str(int(points_SM_N2N3_stau_new[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=3
    runArgs.MultiElecMuTauFilterPtCut=5
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=1
    use_MultiLeptonFilter=True
    runArgs.MultiLeptonFilterLepCut=1
    runArgs.MultiLeptonFilterPtCut=5
    runArgs.MultiLeptonFilterEtaCut=10
    gentype = 'N2N3'
    decaytype = 'Stau'
    njets = 1
    use_decays = False

# Increase the number of MadGraph events and decrease the number of athena events if the filter is on
if use_MultiElecMuTauFilter:
    evt_multiplier = 10.0
    nevts=2000*evt_multiplier

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

