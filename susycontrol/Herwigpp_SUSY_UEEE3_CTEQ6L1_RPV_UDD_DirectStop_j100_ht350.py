templatefile = 'susy_RPVMSSM_SM_DirectStop_template.slha'

evgenConfig.description = 'RPV direct stop generation - simplified model stop->bs '
evgenConfig.generators  = ['Herwigpp']
evgenConfig.keywords    = ['SUSY','RPV','stop']
evgenConfig.contact     = ['David.W.Miller@cern.ch']
evgenConfig.auxfiles   += ['susy_RPVMSSM_SM_DirectStop_template.slha', 'MSSM.model'] 

import os

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('stop1')
slha_file = templatefile
cmds = buildHerwigppCommands(sparticle_list, slha_file,'TwoParticleInclusive', earlyCopy=True)

cmds += """
# Protect against A non-integrable singularity or other bad integrand behavior was found in the integration interval for offshell particles
set /Herwig/NewPhysics/NewModel:WhichOffshell Selected
"""

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

# define spectrum file name
include( 'MC12JobOptions/SUSY_RPV_UDD_DirectStop_mc12points.py' )
try:
    Mstop = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

if not os.access(templatefile,os.R_OK):
    print 'ERROR: Could not get slha file'
else:
    oldcard = open(templatefile,'r')
    newcard = open('spectrum.slha','w')
    for line in oldcard:
        if '   1000006     100              # ~t_1' in line:
            newcard.write('   1000006     %s              # ~t_1\n'%(Mstop))
            print '*** WRITING Mstop = %s' % (Mstop)
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    os.rename('spectrum.slha',templatefile)

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
#include("MC12JobOptions/ParentChildFilter.py")
#topAlg.ParentChildFilter.PDGParent  = [1000006]
#topAlg.ParentChildFilter.PtMinParent =  0.0 # MeV
#topAlg.ParentChildFilter.EtaRangeParent = 1000.
#topAlg.ParentChildFilter.PDGChild = [5]
#topAlg.ParentChildFilter.PtMinChild = 1000. # MeV
#topAlg.ParentChildFilter.EtaRangeChild = 1000.

## HT filter setup for anti-kT R=0.4 truth jets
# Make our standard truth jets
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth', disable=False).jetAlgorithmHandle()

# Grab the HT filter and add it to the job
from GeneratorFilters.GeneratorFiltersConf import HTFilter
if "HTFilter" not in topAlg:
     topAlg += HTFilter()
if "HTFilter" not in StreamEVGEN.RequireAlgs:
     StreamEVGEN.RequireAlgs += ["HTFilter"]

# Configure the HT filter
topAlg.HTFilter.MinJetPt = 20.*GeV # Min pT to consider jet in HT
topAlg.HTFilter.MaxJetEta = 3.2 # Max eta to consider jet in HT
topAlg.HTFilter.MinHT = 350.*GeV # Min HT to keep event
topAlg.HTFilter.MaxHT = 9000.*GeV # Max HT to keep event
topAlg.HTFilter.TruthJetContainer = "AntiKt4TruthJets" # Which jets to use for HT
topAlg.HTFilter.UseNeutrinos = False # Include neutrinos from the MC event in the HT
topAlg.HTFilter.UseLeptons = False # Include e/mu from the MC event in the HT

## Default truth jet filter setup
## The specified truth jet container must exist
from GeneratorFilters.GeneratorFiltersConf import QCDTruthJetFilter
if "QCDTruthJetFilter" not in topAlg:
      topAlg += QCDTruthJetFilter()
if "QCDTruthJetFilter" not in StreamEVGEN.RequireAlgs:
      StreamEVGEN.RequireAlgs += ["QCDTruthJetFilter"]

# Configure the pT filter
topAlg.QCDTruthJetFilter.MaxEta = 999.
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False
topAlg.QCDTruthJetFilter.MinPt = 100.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 8000.*GeV

del cmds
