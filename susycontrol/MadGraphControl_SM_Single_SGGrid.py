# Gluino-gluino grid including squarks
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2sg0 = []
massrange = range(1000,2000,200)+range(2000,4200,400)
for mGluino in sorted(range(400,1000,400)+massrange):
    for mSquark in sorted(range(400,1000,400)+massrange):
        points2sg0 += [[mSquark,mGluino]]

points2sg395 = []
for mGluino in sorted(range(400,1000,400)+massrange+[500,700]):
    for mSquark in sorted(range(400,1000,400)+massrange+[500,700]):
        points2sg395 += [[mSquark,mGluino]]

points2sg695 = []
for mGluino in sorted(massrange+[700,800,900]):
    for mSquark in sorted(massrange+[700,800,900]):
        points2sg695 += [[mSquark,mGluino]]

therun = runArgs.runNumber-153588
if therun>=0 and therun<4*len(points2sg0):
    arun = therun%len(points2sg0)
    gentype = 'GG'
    if therun>=len(points2sg0):   gentype='SS'
    if therun>=len(points2sg0)*2: gentype='SG'
    if therun>=len(points2sg0)*3: gentype='SSB'
    evgenLog.info('Registered generation of grid fifteen, squark-gluino LSP=0 grid point '+str(therun)+' decoded into mass point '+str(arun)+' with gen '+str(gentype))
    stringy = str(int(points2sg0[arun][0]))+'_'+str(int(points2sg0[arun][1]))+'_0'
    for q in squarks: masses[q] = points2sg0[arun][0]
    masses['1000021'] = points2sg0[arun][1]
    masses['1000022'] = 0
    decaytype='SGGrid'
    njets=1
    use_decays=False
    use_Tauola=False
therun = therun - 4*len(points2sg0)
if therun>=0 and therun<4*len(points2sg395):
    arun = therun%len(points2sg395)
    gentype = 'GG'
    if therun>=len(points2sg395):   gentype='SS'
    if therun>=len(points2sg395)*2: gentype='SG'
    if therun>=len(points2sg395)*3: gentype='SSB'
    evgenLog.info('Registered generation of grid sixteen, squark-gluino LSP=395 grid point '+str(therun)+' decoded into mass point '+str(arun)+' with gen '+str(gentype))
    stringy = str(int(points2sg395[arun][0]))+'_'+str(int(points2sg395[arun][1]))+'_395'
    for q in squarks: masses[q] = points2sg395[arun][0]
    masses['1000021'] = points2sg395[arun][1]
    masses['1000022'] = 395
    decaytype='SGGrid'
    njets=1
    use_decays=False
    use_Tauola=False
therun = therun - 4*len(points2sg395)
if therun>=0 and therun<4*len(points2sg695):
    arun = therun%len(points2sg695)
    gentype = 'GG'
    if therun>=len(points2sg695):   gentype='SS'
    if therun>=len(points2sg695)*2: gentype='SG'
    if therun>=len(points2sg695)*3: gentype='SSB'
    evgenLog.info('Registered generation of grid seventeen, squark-gluino LSP=695 grid point '+str(therun)+' decoded into mass point '+str(arun)+' with gen '+str(gentype))
    stringy = str(int(points2sg695[arun][0]))+'_'+str(int(points2sg695[arun][1]))+'_695'
    for q in squarks: masses[q] = points2sg695[arun][0]
    masses['1000021'] = points2sg695[arun][1]
    masses['1000022'] = 695
    decaytype='SGGrid'
    njets=1
    use_decays=False
    use_Tauola=False

# Increase the number of MadGraph events for SG grid samples that need it
# Configuration of if statements courtesy of TJ
if 'SGGrid' in decaytype:
    if 395==masses['1000022']:
        if gentype=='SS' and masses[squarks[0]]<=500: evt_multiplier = 4.0
        if gentype=='SS' and masses['1000021']==400: evt_multiplier = 4.0
        if gentype=='SG' and masses['1000021']==400: evt_multiplier = 4.0
        if gentype=='SS' or gentype=='SSB':
            if masses[squarks[0]]==2000 and masses['1000021']==400: evt_multiplier = 4.0
    elif 695==masses['1000022']:
        if gentype=='SS' and masses[squarks[0]]<=800: evt_multiplier = 4.0
        if gentype=='SS' and masses['1000021']==700: evt_multiplier = 4.0
        if gentype=='SG' and masses['1000021']==700: evt_multiplier = 4.0
        if gentype=='SS' or gentype=='SSB':
            if masses[squarks[0]]==2000 and masses['1000021']==700: evt_multiplier = 4.0

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

