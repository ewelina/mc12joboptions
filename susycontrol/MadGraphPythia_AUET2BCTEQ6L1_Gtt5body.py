import os,subprocess,math,shutil,gzip,random
import ROOT # need TLorentzVector, TVector3, TRotation

from AthenaCommon import Logging
logger = Logging.logging.getLogger('GttExtension_JobOptions')


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #
def LookupMasses(params):

    grid_points = (
        (800.,400.),(800.,445.),(1000.,600.),(1000.,645.), # these ones for validation
        ( 450.,185.),( 550.,285.),( 650.,385.),
        ( 700.,360.),( 700.,410.),( 750.,435.),( 750.,485.),
        ( 800.,460.),( 800.,510.),( 850.,535.),( 850.,585.),
        ( 900.,560.),( 900.,610.),( 950.,635.),( 950.,685.),
        (1000.,660.),(1000.,710.),(1050.,735.),(1050.,785.),
        (1100.,760.),(1100.,810.),
        (1150.,885.),(1250.,985.),(1350.,1085.))

    gridpoint = grid_points[params['RUNNUMBER']-params['BASE_RUNNUMBER']]

    masses = {}
    masses['MGLUINO'] = gridpoint[0]
    masses['MLSP'] = gridpoint[1]
    masses['MSTOP1'] = 2500.
    masses['MSTOP2'] = 3000.
    masses['STOP_MIXING'] = {'11':5.52988023E-01, '12':8.33189202E-01, '21':-8.33189202E-01, '22':5.52988023E-01}

    return masses


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #
def DefineParameters():
    
    params = {}
    params['RUNNUMBER'] = int(runArgs.runNumber)
    params['BASE_RUNNUMBER'] = 204145
    params['SEED'] = int(runArgs.randomSeed)
    params['NEVENTS_PYTHIA'] = 5000
    params['NEVENTS_MADGRAPH'] = int(2*params['NEVENTS_PYTHIA'])
    params['DO_W_DECAYS'] = True
    params['SOURCE_RUN_CARD'] = 'run_card.Gtt5body.template.dat'
    params['SOURCE_PARAM_CARD'] = 'param_card.Gtt5body.template.dat'
    params['PROCNAME_HARD'] = 'PP_GOGO_NLO'
    params['PROCNAME_DECAY'] = 'GO_5BODY'
    params['RUNNAME_HARD'] = 'run_hard'
    params['RUNNAME_DECAYWIDTH'] = 'run_decaywidth'
    params['RUNNAME_DECAY'] = 'run_decay'
    params['FILENAME_HARD'] = 'myevents_hard.lhe'
    params['FILENAME_DECAY'] = 'myevents_decay.lhe'
    params['FILENAME_COMBINED'] = 'myevents_combined.lhe'
    params['FORCE_MULTICORE'] = False
    masses = LookupMasses(params)
    params.update(masses)
    params['PYTHIA_VERSION'] = 'PYTHIA6'
    params['MATCHING_TYPE'] = 'MLM'
    params['ICKKW'] = 1
    params['CUTDECAYS'] = 'F'
    params['PTJET'] = 0.
    params['DRJETJET'] = 0.
    params['MJETJET'] = 0.
    params['XQCUT'] = 0.25*masses['MGLUINO']
    params['TEST_MERGING'] = False
    return params


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #
def GenerateMadGraphSamples(params):
  
    # First build processes
    generate_decay_processes = ''
    gluino_decay = 'go > w+ w- b b~ n1 / unused'
    if params['DO_W_DECAYS']:
        wplus_decays = ['u d~','c s~','e+ ve','mu+ vm','ta+ vt']
        wminus_decays = ['u~ d','c~ s','e- ve~','mu- vm~','ta- vt~']
        for wpd in wplus_decays:
            for wmd in wminus_decays:
                proc = '%s, w+ > %s / unused, w- > %s / unused'%(gluino_decay,wpd,wmd)
                if len(generate_decay_processes)==0: 
                    generate_decay_processes = 'generate '+proc
                else:
                    generate_decay_processes += '\n  add process '+proc
    else:
        generate_decay_processes = 'generate '+gluino_decay

    txt = """
      import model mssm
      define unused = t2 t2~ b1 b1~ b2 b2~ x1- x1+ x2- x2+ n2 n3 n4 ul ul~ ur ur~ dl dl~ dr dr~ sl sl~ sr sr~ cl cl~ cr cr~ h1 h2 h3 h+ h-

      generate p p > go go / unused
      add process p p > go go j / unused
      output %(procname_hard)s

      %(generate_decay_processes)s
      output %(procname_decay)s

    """%{
          'procname_hard': params['PROCNAME_HARD'],
          'generate_decay_processes': generate_decay_processes,
          'procname_decay': params['PROCNAME_DECAY']
        }
    with open('mgcmd.mad','w') as f:
        f.write(txt)

    subprocess.call('$MADPATH/bin/mg5 -f mgcmd.mad',shell=True)

    # Fix Makefile -- taken from Zach Marshall's MadGraphControl.MadGraphUtils module
    for path in (params['PROCNAME_HARD'],params['PROCNAME_DECAY']):
        makefile = path+'/Source/make_opts'
        new_makefile = makefile+'_new'
        with open(makefile,'r') as f:
            with open(new_makefile,'w') as fnew:
                for aline in f:
                    if 'FC=g' in aline:
                        fnew.write('  FC=gfortran\n')
                    elif 'FFLAGS+= -ffixed-line-length-132' in aline and 'i686' in os.environ['CMTCONFIG']:
                        fnew.write('FFLAGS+= -ffixed-line-length-132 -m32\n')
                    else: fnew.write(aline)
        shutil.move(new_makefile,makefile)

    # Retrieve template cards
    subprocess.call(['get_files','-data',params['SOURCE_PARAM_CARD'],params['SOURCE_RUN_CARD']]) # is there an alternative to subprocess?

    # Build param_card
    paramCardLoc = '/Cards/param_card.dat'
    if not params['SOURCE_PARAM_CARD'].endswith('.slha'):
        with open(params['SOURCE_PARAM_CARD'],'r') as f:
            param_card = f.read()
        param_card = param_card.replace('GLUINOMASS',str(params['MGLUINO']))
        param_card = param_card.replace('LSPMASS',str(params['MLSP']))
        param_card = param_card.replace('STOP1MASS',str(params['MSTOP1']))
        param_card = param_card.replace('STOP2MASS',str(params['MSTOP2']))
        param_card = param_card.replace('STOPMIXING11',str(params['STOP_MIXING']['11']))
        param_card = param_card.replace('STOPMIXING12',str(params['STOP_MIXING']['12']))
        param_card = param_card.replace('STOPMIXING21',str(params['STOP_MIXING']['21']))
        param_card = param_card.replace('STOPMIXING22',str(params['STOP_MIXING']['22']))
        destParamCard = params['PROCNAME_HARD']+paramCardLoc
        with open(destParamCard,'w') as f:
            f.write(param_card)
        shutil.copyfile(destParamCard,params['PROCNAME_DECAY']+paramCardLoc)
    else:
        shutil.copyfile(params['SOURCE_PARAM_CARD'],params['PROCNAME_HARD']+paramCardLoc)
        shutil.copyfile(params['SOURCE_PARAM_CARD'],params['PROCNAME_DECAY']+paramCardLoc)

    # Build run_card
    runCardLoc = '/Cards/run_card.dat'
    for i in (0,1): # slightly different parameters between generations of hard process or decay
        if i==0:
            destRunCard = params['PROCNAME_HARD']+runCardLoc
            nevents = params['NEVENTS_MADGRAPH']
            ickkw = params['ICKKW']
            xqcut = params['XQCUT']
        else:
            destRunCard = params['PROCNAME_DECAY']+runCardLoc
            nevents = int(2.7*params['NEVENTS_MADGRAPH']) # need 2 gluino decays for 1 hard process
            ickkw = 0  # disable ME/PS matching in decay
            xqcut = 0. # disable ME/PS matching in decay           

        with open(params['SOURCE_RUN_CARD'],'r') as f:
            run_card = f.read()
        run_card = run_card.replace('RNDSEED',str(params['SEED']))
        run_card = run_card.replace('NEVENTS',str(nevents))
        run_card = run_card.replace('ICKKW',str(ickkw))
        run_card = run_card.replace('CUTDECAYS',str(params['CUTDECAYS']))
        run_card = run_card.replace('PTJET',str(params['PTJET']))
        run_card = run_card.replace('DRJETJET',str(params['DRJETJET']))
        run_card = run_card.replace('MJETJET',str(params['MJETJET']))
        run_card = run_card.replace('XQCUT',str(xqcut))
        with open(destRunCard,'w') as f:
            f.write(run_card)


    # Set madevent running options
    if not params['FORCE_MULTICORE']:
        # Look if allowed -- also taken from Zach Marshall's MadGraphControl.MadGraphUtils module
        if 'ATHENA_PROC_NUMBER' in os.environ:
            ncores = int(os.environ['ATHENA_PROC_NUMBER'])
        else:
            ncores = 1
        if ncores>1:
            mg_options = '-f --multicore --nb_core=%d'%(ncores)
        else:
            mg_options = '-f --nb_core=1'
    else:
        mg_options = '-f --multicore --nb_core=8'

    # Generate sample for hard process
    shcmd = './%s/bin/generate_events %s %s ' % (params['PROCNAME_HARD'],params['RUNNAME_HARD'],mg_options)
    subprocess.call(shcmd,shell=True,stdin=subprocess.PIPE)
    src = '%s/Events/%s/unweighted_events.lhe.gz' % (params['PROCNAME_HARD'],params['RUNNAME_HARD'])
    fzipped = gzip.open(src,'rb')
    if fzipped:
        with open(params['FILENAME_HARD'],'wb') as f:
            f.write(fzipped.read())
        fzipped.close()

    # Generate sample for decay
    shcmd = './%s/bin/generate_events %s %s ' % (params['PROCNAME_DECAY'],params['RUNNAME_DECAY'],mg_options)
    subprocess.call(shcmd,shell=True,stdin=subprocess.PIPE)
    src = '%s/Events/%s/unweighted_events.lhe.gz' % (params['PROCNAME_DECAY'],params['RUNNAME_DECAY'])
    fzipped = gzip.open(src,'rb')
    if fzipped:
        with open(params['FILENAME_DECAY'],'wb') as f:
            f.write(fzipped.read())

    pass


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #
def CombineMadGraphSamples(params):

    #
    # First, helper stuff to read from input LHE files
    #

    class LheEventEntry:
        def __init__(self):
            self.header=''
            self.particles=[]
            self.comments=[]
            self.filled=False
            self.channel = ''
            pass
        def get_scale_tokens(self):
            tokens = []
            if len(self.comments)>0:
                 tokens = self.comments[-1].split()[1:]
            return tokens
    
    def read_event(filestream,fill_channel=False):

        event = LheEventEntry()
        mode='none'
        channel = ''
        for line in filestream:
            if line.startswith('<event>'):
                mode='event_tag'
            elif line.startswith('</event>'):
                event.filled=True
                mode='none'
                break
            elif mode=='event_tag':
                event.header=line.strip('\n')
                mode='event_core'
            elif mode=='event_core':
                if line.startswith('#'):
                    event.comments.append(line.strip('\n'))
                else:
                    event.particles.append(line.strip('\n'))
                    if fill_channel:
                        tokens = line.split()
                        pdg = abs(int(tokens[0]))
                        if pdg==2 or pdg==4: channel += 'h'
                        elif pdg==11 or pdg==13: channel += 'l'
                        elif pdg==15: channel += 't'
        if fill_channel:
            if len(channel)==2:
                event.channel = ''.join(sorted(channel))
            else:
                logger.warning('unable to determine gluino decay channel, channel is %s'%(channel))
        return event

    def read_event_with_channel(filestream,channel):

        if 'event_pool' not in read_event_with_channel.__dict__:
            read_event_with_channel.event_pool = {} # "static" variable
        if channel in read_event_with_channel.event_pool:
            pool = read_event_with_channel.event_pool[channel]
            if pool is not None and len(pool)>0:
                event = pool[0]
                del pool[0]
                #print 'remove event from pool %s, size is now %d'%(channel,len(pool))
                return event
        event = read_event(filestream,True)
        while event.filled and event.channel!=channel:
            if event.channel not in read_event_with_channel.event_pool:
                read_event_with_channel.event_pool[event.channel] = [event]
            else:
                pool = read_event_with_channel.event_pool[event.channel]
                if pool is None:
                    read_event_with_channel.event_pool[event.channel] = [event]
                elif len(pool)<50: # more leptons than needed, not necessary to store them all
                    pool.append(event)
            #print 'add event in pool since channel is %s and we want %s, pool size is now %d'%(event.channel,channel,len(read_event_with_channel.event_pool[event.channel]))
            event = read_event(filestream,True)
        return event

    def read_banner(filename):
        banner = '' 
        with open(filename,'r') as f:
            for line in f:
                if line.startswith('<event>'): break
                banner += line
        return banner
    
    #
    # Combination task performed now
    #

    logger.info('working on files %s and %s'%(params['FILENAME_HARD'],params['FILENAME_DECAY']))    

    banner = read_banner(params['FILENAME_HARD'])

    file_hard = open(params['FILENAME_HARD'],'r')
    file_decay = open(params['FILENAME_DECAY'],'r')
    file_output = open(params['FILENAME_COMBINED'],'w')
    file_output.write(banner)

    random.seed(params['SEED'])
    bH, bL, bT = (0.676, 2*0.108, 0.108) # W branching ratio in hadrons/light leptons/taus
    umax = {} # thresholds for random channel picking, based on gluino branching ratios
    umax['hh'] = bH*bH
    umax['hl'] =  2*bH*bL + umax['hh']
    umax['ht'] =  2*bH*bT + umax['hl']
    umax['ll'] =  bL*bL + umax['ht']
    umax['lt'] =  2*bL*bT + umax['ll']
    umax['tt'] = 1. # = bT*bT + umax['lt']

    banner = None

    nBuiltEvents=0

    while True:
        hard_event = read_event(file_hard)
        if not hard_event.filled: break
        decay_event = [None,None]
        if params['DO_W_DECAYS']:
            for i in (0,1):
                u = random.random() # choose a random gluino decay channel
                for key in ('hh','hl','ht','ll','lt','tt'):
                    if u<=umax[key]:
                        channel = key
                        break
                event = read_event_with_channel(file_decay,channel)
                if not event.filled: break
                decay_event[i] = event
        else:
            for i in (0,1):
                event = read_event(file_decay)
                if not event.filled: break
                decay_event[i] = event 
        if None in decay_event: break

        nGluinos=0
        GBoost = [None,None]
        GQuanta = [None,None] # ID, color 1, color 2, spin
        used_colors = []

        particles_tokens = {'1': [], '2': []}
        particles_scales = []
        particles_initial_indexes = {'1': [], '2': []}
        
        final_event = ''
        nParticlesInEvent=0
        nstatus1 = 0
        scale_tokens = hard_event.get_scale_tokens()
        for l in hard_event.particles:
            nParticlesInEvent += 1
            tokens = l.split()
            pid = int(tokens[0])
            status = int(tokens[1])
            color = int(tokens[4])
            if color!=0 and color not in used_colors: used_colors.append(color)
            color = int(tokens[5])
            if color!=0 and color not in used_colors: used_colors.append(color)
            if pid!=1000021 or status!=1:
                if status==1: # typically: ISR/FSR
                    particles_tokens['1'].append(tokens)
                    particles_initial_indexes['1'].append(nParticlesInEvent+1)
                    if len(scale_tokens)>nstatus1:
                        particles_scales.append(scale_tokens[nstatus1])
                else:
                    final_event += l+'\n'
            else:
                px = float(tokens[6])
                py = float(tokens[7])
                pz = float(tokens[8])
                e = float(tokens[9])
                v = ROOT.TLorentzVector()
                v.SetPxPyPzE(px,py,pz,e)
                GBoost[nGluinos] = v.BoostVector()
                GQuanta[nGluinos] = [nParticlesInEvent,tokens[4],tokens[5],tokens[-1]] # ID, color1, color2, spin
                nGluinos+=1
                tokens[1] = '2' # change status from final to intermediate
                new_l = "%9s%5s%5s%5s%5s%5s%19s%19s%19s%19s%19s%3s%4s"%tuple(tokens)
                final_event += new_l+'\n'
            if status==1:
                nstatus1+=1
            pass
        nParticlesInHard = nParticlesInEvent - len(particles_initial_indexes['1'])
        skip_event = False
        for b in [0,1]: # process each of the 2 gluinos
            mothID_offset = nParticlesInEvent
            rotMx = None # for proper spin handling
            color_map = {}
            nstatus1 = 0
            scale_tokens = decay_event[b].get_scale_tokens()
            for l in decay_event[b].particles:
                tokens = l.split()
                pid = int(tokens[0])
                if pid==1000021:
                    mothID_offset -= 1
                    # initial gluino spin is given wrt positive Z-axis, rotate to direction pointed by gluino momentum
                    dspin = int(float(tokens[-1]))
                    pspin = int(float(GQuanta[b][3]))
                    zAxis = ROOT.TVector3(0,0,1)
                    rotAxis = zAxis.Cross(GBoost[b])
                    rotAngle = GBoost[b].Angle(zAxis)
                    if pspin!= dspin:
                        rotAngle = (math.pi - rotAngle)
                    rotMx = ROOT.TRotation()
                    rotMx.Rotate(rotAngle,rotAxis)
                    color_map[int(tokens[4])] = GQuanta[b][1]
                    color_map[int(tokens[5])] = GQuanta[b][2]
                    continue # already recorded
                elif rotMx is None: # may happen if gluino is not the first particle stored, but I never saw this occur
                    logger.warning('unknown gluino frame, skipping event')
                    skip_event = True
                    break

                px = float(tokens[6])
                py = float(tokens[7])
                pz = float(tokens[8])
                e = float(tokens[9])
                v = ROOT.TLorentzVector()
                v.SetPxPyPzE(px,py,pz,e)
                v.Transform(rotMx)
                v.Boost(GBoost[b])
                tokens[6] = '%e'%(v.Px())
                tokens[7] = '%e'%(v.Py())
                tokens[8] = '%e'%(v.Pz())
                tokens[9] = '%e'%(v.E())
                for c in [4,5]: # update color flow
                    color = int(tokens[c])
                    if color!=0:
                        if color in color_map:
                            tokens[c] = str(color_map[color])
                        else: 
                            new_color = min(used_colors)
                            while new_color in used_colors: new_color += 1
                            used_colors.append(new_color)
                            color_map[color] = new_color
                            tokens[c] = str(new_color)
                    pass
                # spin information is lost in the boost (unless boost parallel to momentum, like for the gluino)
                # could build something more elaborate (random based on angle between initial spin and after-boost momentum), but...
                tokens[-1] = '0.'
                mothID = int(tokens[2])
                if mothID==1:
                    tokens[2] = str(GQuanta[b][0])
                else:
                    tokens[2] = str(mothID + mothID_offset)
                tokens[3] = tokens[2]
                particles_tokens[tokens[1]].append(tokens)
                nParticlesInEvent += 1
                particles_initial_indexes[tokens[1]].append(nParticlesInEvent)
                if int(tokens[1])==1: # status==1
                    if nstatus1<len(scale_tokens):
                        #particles_scales.append(scale_tokens[nstatus1])
                        pass
                    nstatus1 += 1
        if skip_event: continue

        # must write all particles with status 2 first -- otherwise pythia really not happy
        nParticlesStatus2 = len(particles_tokens['2'])
        for s in ['2', '1']:
            for tokens in particles_tokens[s]:
                for t in [2,3]:
                    # need to update mother ID when relevant
                    mID = int(tokens[t])
                    if mID in particles_initial_indexes['1']:
                        mID = nParticlesInHard + nParticlesStatus2 + particles_initial_indexes['1'].index(mID) +1
                    elif mID in particles_initial_indexes['2']:
                        mID = nParticlesInHard + particles_initial_indexes['2'].index(mID) + 1
                    tokens[t] = str(mID)
                new_l = "%9s%5s%5s%5s%5s%5s%19s%19s%19s%19s%19s%3s%4s"%tuple(tokens)
                final_event += new_l+'\n'
        # set the scale comment
        scale_comment = '# ' + '  '.join(particles_scales)
        for i in range(len(particles_tokens['1'])-len(particles_scales)):
            scale_comment += '  0.01000000E+04'
        
        # write events
        file_output.write('<event>\n')
        tokens = hard_event.header.split()
        tokens[0] = str(nParticlesInEvent)
        file_output.write(' '.join(tokens)+'\n')
        file_output.write(final_event)
        file_output.write(scale_comment+'\n')
        file_output.write('</event>\n')
        nBuiltEvents += 1

    file_hard.close()
    file_decay.close()
    file_output.close()
    logger.info('built %d events when combining files, stored in %s'%(nBuiltEvents,params['FILENAME_COMBINED']))

    pass


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #
def SetupPythia(params):
    if params['PYTHIA_VERSION']!='PYTHIA8':
        if params['MATCHING_TYPE']!='MLM':
            logger.fatal('matching not implemented')
   
        include('MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py')
        topAlg.Pythia.PythiaCommand = [
            'pyinit user madgraph',
            'pystat 1 3 4 5',
            'pyinit dumpr 1 5',
            'pyinit pylistf 1',
            'pydat1 parj 90 20000',
            'pydat3 mdcy 15 1 0'
        ]
        include("MC12JobOptions/Tauola_Fragment.py")
        include("MC12JobOptions/Photos_Fragment.py")
        evgenConfig.generators += ['MadGraph','Pythia']

        phojf=open('./pythia_card.dat','w')
        phojf.write("""
            !...Matching parameters...
            IEXCFILE=0
            showerkt=T
            MINJETS=0
            MAXJETS=1
            QCUT=""" + str(params["XQCUT"]) + """
            imss(21)=24
            imss(22)=24
        """)
        phojf.close()

    else:
        include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
        include('MC12JobOptions/Pythia8_MadGraph.py')
        topAlg.Pythia8.Commands += [
            "Init:showAllParticleData = off",
            "Beams:frameType = 4",
            "Next:numberShowLHA = 3",
            "Next:numberShowEvent =3"
            ]
        if params['MATCHING_TYPE']=='CutBased':
             topAlg.Pythia8.Commands += [
                "Merging:doCutBasedMerging= on",
                "Merging:QijMS = "+str(myMJETJET), #
                "Merging:pTiMS = "+str(myPTJET), #
                "Merging:dRijMS = "+str(myDRJETJET), #
                #"Merging:Process=pp>{go,1000021}{go,1000021}", #
                #"Merging:enforceCutOnLHE = off", #
                "Merging:Process = pp>t1t1~", #
                "Merging:nJetMax = 1"
            ]
        elif params['MATCHING_TYPE']=='MLM':
            topAlg.Pythia8.Commands += [
                "Merging:doCutBasedMerging= off",
                "JetMatching:merge = on", # main switch for MLM matching
                "JetMatching:setMad= on", # sets doMerge, qcut, maxjetflavor and alpsfact from LHEF
                "JetMatching:scheme = 1", # MLM a la MadGraph
                "JetMatching:jetAlgorithm = 2", # slowjet algo, no other choice
                "JetMatching:slowJetPower = 1", # kT jets, no other choice
                "JetMatching:exclusive = 2", # input file is a mix of several extra parton multiplicities (+0, +1)
                "JetMatching:nJet = -1", # ignored, in principle (determined event-by-event)
                "JetMatching:nJetMax = 1" # input file contains up to +1 extra parton
            ]
    pass


# # # # # # # # # # # # # # # 
 # # # # # # # # # # # # # #

params = DefineParameters()
GenerateMadGraphSamples(params)
CombineMadGraphSamples(params)
SetupPythia(params)

evgenConfig.description = 'SUSY Gtt 5-body decay with offshell top(s), mGluino = %.0f, mLSP = %.0f'%(params['MGLUINO'],params['MLSP'])
evgenConfig.keywords = ['SUSY','Gtt','gluino']
evgenConfig.contact = ['otilia.ducu@cern.ch','jmaurer@cern.ch']
evgenConfig.minevents = params['NEVENTS_PYTHIA']
evgenConfig.inputfilecheck = params['FILENAME_COMBINED']
runArgs.inputGeneratorFile = params['FILENAME_COMBINED']

try:
    os.remove(params['FILENAME_COMBINED']+'.symlink.events')
    os.remove('events.lhe')
except :
    pass
# dummy link for the input file check
os.symlink(params['FILENAME_COMBINED'],params['FILENAME_COMBINED']+'.symlink.events')


if params['TEST_MERGING']:
    from TestMerging.TestMergingConf import TestMergingAlg
    topAlg += [TestMergingAlg('TestMergingAlg_instance')]


