## SUSY Herwig++ jobOptions for Wino/Bino simplified model with lambda_121 R-parity violation.
## Author: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVSimplifiedModel')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py' )
JobFile = runArgs.jobConfig[0]
if "RPV" not in JobFile:
	raise RuntimeError("This File is made for smplified RPV models.")
RPVConfigs=JobFile.split("_")
for a in range(len(RPVConfigs)):
	if RPVConfigs[a]=="RPV" :
		NLSP=RPVConfigs[a+1]
		mNLSP=RPVConfigs[a+2]
		mLSP=RPVConfigs[a+3]
		if ".py" in RPVConfigs[a+4]:
			Lambda=RPVConfigs[a+4].replace(".py","")
		elif ".py" in RPVConfigs[a+5]:
			Lambda="%s_%s"%(RPVConfigs[a+4],RPVConfigs[a+5].replace(".py",""))
		else:
			raise RuntimeError("Could not determine the Lambda Couplings. Maximal two couplings are allowed. Please check the number of couplings in your model.")
				
print NLSP+" "+mNLSP+" "+mLSP+" "+Lambda

slha_file = 'susy_RPV_%s_%s_%s_%s_simplified.slha' % (mNLSP, mLSP,NLSP,Lambda)
# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
if   NLSP == 'WinoPM': 
	cmds = buildHerwigppCommands(['~chi_1+','~chi_1-'], slha_file, 'TwoParticleInclusive')
	evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda121','Lambda122', 'Gauginos']
elif NLSP == 'Wino'  : 
	cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')
	evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda121','Lambda122', 'Gauginos']
elif NLSP == 'Gluino': 
	cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')
	evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda121','Lambda122', 'Gluinos']
else: raise RuntimeError("The NLSP is unknown to the JobOptions file.")

# define metadata
evgenConfig.description = '%s/Bino simplified model with lambda_%s R-parity violation. with m_%s = %s, m_Bino = %s' % (NLSP,Lambda,NLSP,mNLSP,mLSP)
evgenConfig.contact  = ['flowerde@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
