# Stop pair-production with stop > b chargino, chargino > W LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points_TT_onestepBB = []
therun = -1
if runArgs.runNumber in range(166417,166500+1):
    # m(chargino)=2*m(neutralino) samples
    therun = runArgs.runNumber-166417
    points_TT_onestepBB = [
        [350.0,200.0,100.0],[125.0,100.0,50.0],[125.0,120.0,60.0],[150.0,100.0,50.0],
        [150.0,140.0,70.0],[180.0,100.0,50.0],[180.0,150.0,75.0],[200.0,100.0,50.0],
        [200.0,150.0,75.0],[200.0,190.0,95.0],[225.0,100.0,50.0],[225.0,150.0,75.0],
        [225.0,200.0,100.0],[250.0,100.0,50.0],[250.0,150.0,75.0],[250.0,200.0,100.0],
        [250.0,240.0,120.0],[275.0,100.0,50.0],[275.0,150.0,75.0],[275.0,200.0,100.0],
        [275.0,250.0,125.0],[300.0,100.0,50.0],[300.0,200.0,100.0],[300.0,290.0,145.0],
        [350.0,100.0,50.0],[350.0,200.0,100.0],[350.0,300.0,150.0],[350.0,340.0,170.0],
        [400.0,100.0,50.0],[400.0,200.0,100.0],[400.0,300.0,150.0],[400.0,390.0,195.0],
        [450.0,100.0,50.0],[450.0,200.0,100.0],[450.0,300.0,150.0],[450.0,400.0,200.0],
        [450.0,440.0,220.0],[500.0,100.0,50.0],[500.0,200.0,100.0],[500.0,300.0,150.0],
        [500.0,400.0,200.0],[500.0,490.0,245.0],[550.0,100.0,50.0],[550.0,200.0,100.0],
        [550.0,300.0,150.0],[550.0,400.0,200.0],[550.0,500.0,250.0],[550.0,540.0,270.0],
        [600.0,100.0,50.0],[600.0,200.0,100.0],[600.0,300.0,150.0],[600.0,400.0,200.0],
        [600.0,500.0,250.0],[600.0,590.0,295.0],[650.0,100.0,50.0],[650.0,200.0,100.0],
        [650.0,300.0,150.0],[650.0,400.0,200.0],[650.0,500.0,250.0],[650.0,600.0,300.0],
        [650.0,640.0,320.0],[700.0,100.0,50.0],[700.0,200.0,100.0],[700.0,300.0,150.0],
        [700.0,400.0,200.0],[700.0,500.0,250.0],[700.0,600.0,300.0],[700.0,690.0,345.0],
        [750.0,100.0,50.0],[750.0,200.0,100.0],[750.0,300.0,150.0],[750.0,400.0,200.0],
        [750.0,500.0,250.0],[750.0,600.0,300.0],[750.0,700.0,350.0],[750.0,740.0,370.0],
        [800.0,100.0,50.0],[800.0,200.0,100.0],[800.0,300.0,150.0],[800.0,400.0,200.0],
        [800.0,500.0,250.0],[800.0,600.0,300.0],[800.0,700.0,350.0],[800.0,790.0,395.0]
        ]
elif runArgs.runNumber in range(172648,172769+1):
    therun = runArgs.runNumber-172648
    points_TT_onestepBB = [
        [150.0,100.0,1.0],[150.0,120.0,1.0],[150.0,140.0,1.0],[200.0,100.0,1.0],
        [200.0,125.0,1.0],[200.0,145.0,1.0],[200.0,170.0,1.0],[200.0,190.0,1.0],
        [250.0,100.0,1.0],[250.0,150.0,1.0],[250.0,200.0,1.0],[250.0,240.0,1.0],
        [300.0,100.0,1.0],[300.0,150.0,1.0],[300.0,200.0,1.0],[300.0,250.0,1.0],
        [300.0,290.0,1.0],[350.0,100.0,1.0],[350.0,150.0,1.0],[350.0,200.0,1.0],
        [350.0,250.0,1.0],[350.0,300.0,1.0],[350.0,340.0,1.0],[400.0,100.0,1.0],
        [400.0,150.0,1.0],[400.0,200.0,1.0],[400.0,250.0,1.0],[400.0,300.0,1.0],
        [400.0,350.0,1.0],[400.0,390.0,1.0],[500.0,100.0,1.0],[500.0,200.0,1.0],
        [500.0,300.0,1.0],[500.0,400.0,1.0],[500.0,490.0,1.0],[600.0,100.0,1.0],
        [600.0,200.0,1.0],[600.0,300.0,1.0],[600.0,400.0,1.0],[600.0,500.0,1.0],
        [600.0,590.0,1.0],
        [150.0,140.0,25.0],[150.0,140.0,50.0],[150.0,140.0,100.0],[150.0,140.0,115.0],
        [200.0,190.0,25.0],[200.0,190.0,50.0],[200.0,190.0,75.0],[200.0,190.0,125.0],
        [200.0,190.0,150.0],[200.0,190.0,180.0],[250.0,240.0,60.0],[250.0,240.0,180.0],
        [250.0,240.0,200.0],[250.0,240.0,230.0],[300.0,290.0,50.0],[300.0,290.0,100.0],
        [300.0,290.0,200.0],[300.0,290.0,280.0],[350.0,340.0,60.0],[350.0,340.0,120.0],
        [350.0,340.0,240.0],[350.0,340.0,300.0],[400.0,390.0,50.0],[400.0,390.0,100.0],
        [400.0,390.0,150.0],[400.0,390.0,250.0],[400.0,390.0,300.0],[400.0,390.0,350.0],
        [500.0,490.0,100.0],[500.0,490.0,200.0],[500.0,490.0,300.0],[500.0,490.0,400.0],
        [500.0,490.0,480.0],[600.0,590.0,100.0],[600.0,590.0,200.0],[600.0,590.0,400.0],
        [600.0,590.0,500.0],
        [300.0,100.0,90.0],[300.0,150.0,50.0],[300.0,150.0,100.0],[300.0,150.0,140.0],
        [300.0,200.0,50.0],[300.0,200.0,150.0],[300.0,200.0,190.0],[300.0,250.0,50.0],
        [300.0,250.0,100.0],[300.0,250.0,150.0],[300.0,250.0,200.0],[300.0,250.0,240.0],
        [150.0,100.0,25.0],[150.0,100.0,75.0],[150.0,100.0,90.0],[150.0,120.0,25.0],
        [150.0,120.0,50.0],[150.0,120.0,75.0],[150.0,120.0,110.0],
        [160.0,150.0,25.0],[160.0,150.0,50.0],[160.0,150.0,100.0],[160.0,150.0,140.0],
        [200.0,150.0,25.0],[200.0,150.0,50.0],[200.0,150.0,100.0],[200.0,150.0,140.0],
        [250.0,150.0,25.0],[250.0,150.0,50.0],[250.0,150.0,100.0],[250.0,150.0,140.0],
        [300.0,150.0,25.0],[400.0,150.0,25.0],[400.0,150.0,50.0],[400.0,150.0,100.0],
        [400.0,150.0,140.0],[500.0,150.0,25.0],[500.0,150.0,50.0],[500.0,150.0,100.0],
        [500.0,150.0,140.0],[600.0,150.0,25.0],[600.0,150.0,50.0],[600.0,150.0,100.0],
        [600.0,150.0,140.0],
        ]
elif runArgs.runNumber in range(173278,173294+1):
    therun = runArgs.runNumber-173278
    points_TT_onestepBB = [
        [135,100,50],[135,120,60],[135,130,65],[150,120,60],
        [150,130,65],[165,130,65],[165,144,72],[165,160,80],
        [180,130,65],[180,160,80],[180,174,87],[190,140,70],
        [190,170,85],[200,130,65],[200,174,87],[215,130,65],
        [215,160,80],
        ]
elif runArgs.runNumber in range(173473,173485+1):
    therun = runArgs.runNumber-173473
    points_TT_onestepBB = [
        [215,194,97],[215,210,105],[225,170,85],[225,214,107],
        [235,130,65],[235,160,80],[235,180,90],[235,214,107],
        [235,230,115],[250,130,65],[250,180,90],[250,214,107],
        [250,230,115],
        ]
elif runArgs.runNumber in range(173486,173676+1):
    # m(chargino)-m(neutralino) fixed samples
    therun = runArgs.runNumber - 173486
    points_TT_onestepBB = [
        [150,105,100],[150,140,135],[200,105,100],[200,155,150],
        [200,190,185],[250,105,100],[250,155,150],[250,205,200],
        [250,240,235],[300,105,100],[300,155,150],[300,205,200],
        [300,255,250],[300,290,285],[350,155,150],[350,205,200],
        [350,255,250],[350,305,300],[350,340,335],[400,105,100],
        [400,205,200],[400,255,250],[400,305,300],[400,355,350],
        [400,390,385],[450,155,150],[450,255,250],[450,305,300],
        [450,355,350],[450,405,400],[450,440,435],[500,105,100],
        [500,205,200],[500,255,250],[500,305,300],[500,355,350],
        [500,405,400],[500,455,450],[500,490,485],[550,105,100],
        [550,155,150],[550,205,200],[550,255,250],[550,305,300],
        [550,355,350],[550,405,400],[550,455,450],[600,105,100],
        [600,155,150],[600,205,200],[600,255,250],[600,305,300],
        [600,355,350],[600,405,400],[600,455,450],[650,105,100],
        [650,155,150],[650,205,200],[650,255,250],[650,305,300],
        [650,355,350],[650,405,400],[650,455,450],[700,105,100],
        [700,155,150],[700,205,200],[700,255,250],[700,305,300],
        [700,355,350],[700,405,400],[700,455,450],[750,105,100],
        [750,155,150],[750,205,200],[750,255,250],[750,305,300],
        [750,355,350],[750,405,400],[750,455,450],[800,105,100],
        [800,155,150],[800,205,200],[800,255,250],[800,305,300],
        [800,355,350],[800,405,400],[800,455,450],
        [150,95,75],[150,140,120],[200,95,75],[200,120,100],
        [200,170,150],[200,190,170],[250,95,75],[250,120,100],
        [250,170,150],[250,220,200],[250,240,220],[300,120,100],
        [300,170,150],[300,220,200],[300,270,250],[300,290,270],
        [350,95,75],[350,170,150],[350,220,200],[350,270,250],
        [350,320,300],[350,340,320],[400,120,100],[400,220,200],
        [400,270,250],[400,320,300],[400,370,350],[400,390,370],
        [450,95,75],[450,170,150],[450,220,200],[450,270,250],
        [450,320,300],[450,370,350],[450,420,400],[450,440,420],
        [500,120,100],[500,220,200],[500,270,250],[500,320,300],
        [500,370,350],[500,420,400],[500,470,450],[500,490,470],
        [550,95,75],[550,120,100],[550,170,150],[550,220,200],
        [550,270,250],[550,320,300],[550,370,350],[550,420,400],
        [550,470,450],[600,95,75],[600,120,100],[600,170,150],
        [600,220,200],[600,270,250],[600,320,300],[600,370,350],
        [600,420,400],[600,470,450],[650,95,75],[650,120,100],
        [650,170,150],[650,220,200],[650,270,250],[650,320,300],
        [650,370,350],[650,420,400],[650,470,450],[700,95,75],
        [700,120,100],[700,170,150],[700,220,200],[700,270,250],
        [700,320,300],[700,370,350],[700,420,400],[700,470,450],
        [750,95,75],[750,120,100],[750,170,150],[750,220,200],
        [750,270,250],[750,320,300],[750,370,350],[750,420,400],
        [750,470,450],[800,95,75],[800,120,100],[800,170,150],
        [800,220,200],[800,270,250],[800,320,300],[800,370,350],
        [800,420,400],[800,470,450],
        [350,160,150],[400,210,200],[450,310,300],[450,360,350],
        [500,310,300],[600,310,300],
        ]
elif runArgs.runNumber in range(174849,174856+1):
    # additional m(chargino)=150GeV samples
    therun = runArgs.runNumber - 174849
    points_TT_onestepBB = [
        [160,150,1],[160,150,75],[350,150,50],[350,150,75],[350,150,100],
        [160,150,75],[350,150,75],[350,150,100]]
elif runArgs.runNumber in range(175708,175766+1):
    # m(chargino)=106GeV samples
    therun = runArgs.runNumber - 175708
    points_TT_onestepBB = [
        [130,106,95],[150,106,1],[150,106,60],[150,106,95],
        [170,106,1],[170,106,35],[170,106,60],[170,106,75],
        [170,106,95],[190,106,1],[190,106,35],[190,106,60],
        [190,106,75],[190,106,95],[210,106,1],[210,106,35],
        [210,106,60],[210,106,75],[210,106,95],[230,106,1],
        [230,106,35],[230,106,60],[230,106,75],[230,106,95],
        [250,106,1],[250,106,35],[250,106,60],[250,106,75],
        [250,106,95],[300,106,1],[300,106,35],[300,106,60],
        [300,106,75],[300,106,95],[350,106,1],[350,106,35],
        [350,106,60],[350,106,75],[350,106,95],[400,106,1],
        [400,106,35],[400,106,60],[400,106,75],[400,106,95],
        [500,106,1],[500,106,35],[500,106,60],[500,106,75],
        [500,106,95],[600,106,1],[600,106,35],[600,106,60],
        [600,106,75],[600,106,95],[700,106,1],[700,106,35],
        [700,106,60],[700,106,75],[700,106,95]
        ]
elif runArgs.runNumber in range(166997,166999+1):
    # benchmark points for no-lepton analysis
    therun = runArgs.runNumber - 166997
    points_TT_onestepBB = [
        [450,300,150],[450,100,50],[450,250,1]
        ]
elif runArgs.runNumber in range(177053,177089+1):
    # no lepton filter sample and extension of lepton filtered sample
    therun = runArgs.runNumber - 177053
    points_TT_onestepBB = [
        [300,100,1],[300,100,50],[300,200,100],[300,200,150],
        [300,250,150],[300,250,200],[300,290,200],[160,150,50],
        [200,150,50],[250,150,50],[300,150,50],[350,150,50],
        [400,150,50],[500,150,50],[600,150,50],[700,150,50],
        [800,150,50],[160,150,100],[200,150,100],[250,150,100],
        [300,150,100],[350,150,100],[400,150,100],[500,150,100],
        [600,150,100],[700,150,100],[800,150,100],[500,150,1],
        [600,150,1],[700,150,1],[700,150,25],[700,150,50],
        [700,150,100],[800,150,1],[800,150,25],[800,150,50],
        [800,150,100],
        ]
elif runArgs.runNumber in range(177822,177858+1):
    # extension of compressed chargino-neutralino grid
    therun = runArgs.runNumber - 177822
    points_TT_onestepBB = [
        [160,150,145],[175,130,125],[185,115,110],[185,175,170],
        [210,130,125],[210,200,195],[225,215,210],[275,230,225],
        [325,230,225],[575,230,225],[225,170,150],[250,195,175],
        [375,230,225],[175,165,160],[240,230,225],[150,120,100],
        [160,150,130],[175,95,75],[175,120,100],[175,145,125],
        [175,165,145],[185,175,155],[200,145,125],[210,200,180],
        [225,120,100],[225,145,125],[225,195,175],[225,215,195],
        [240,230,210],[250,145,125],[275,170,150],[275,195,175],
        [275,220,200],[275,245,225],[275,265,245],[300,195,175],
        [300,245,225],
        ]
elif runArgs.runNumber in range(178060,178103+1):
    # extension of compressed chargino-neutralino grid for high met filter
    therun = runArgs.runNumber - 178060
    points_TT_onestepBB = [
        [150,140,135],[175,165,160],[200,190,185],[225,215,210],
        [150,95,75],[150,120,100],[150,140,120],[160,150,130],
        [175,95,75],[175,120,100],[175,145,125],[175,165,145],
        [185,175,155],[200,95,75],[200,120,100],[200,145,125],
        [200,170,150],[200,190,170],[210,200,180],[225,120,100],
        [225,145,125],[225,170,150],[225,195,175],[225,215,195],
        [240,230,210],[250,95,75],[250,120,100],[250,145,125],
        [250,170,150],[250,195,175],[250,220,200],[250,240,220],
        [275,170,150],[275,195,175],[275,220,200],[275,245,225],
        [275,265,245],[300,120,100],[300,170,150],[300,195,175],
        [300,220,200],[300,245,225],[300,270,250],[300,290,270],
        ]
elif runArgs.runNumber in range(186990,186999+1):
    # extension of fixed chargino 150GeV grid for high met filter
    therun = runArgs.runNumber - 186990
    points_TT_onestepBB = [
        [160,150,100],[160,150,50],[200,150,130],[200,150,115],
        [200,150,100],[200,150,50],[250,150,130],[250,150,115],
        [250,150,100],[250,150,50]
        ]
elif runArgs.runNumber in range(176957,176958+1):
    therun = runArgs.runNumber - 176957
    points_TT_onestepBB = [
        [225,180,175],[450,120,100]
        ]
elif runArgs.runNumber in range(177986,177996+1):
    therun = runArgs.runNumber - 177986
    points_TT_onestepBB = [
        [300,125,25],[300,125,75],[300,175,25],[300,175,75],
        [200,150,115],[250,150,115],[300,150,115],[350,150,115],
        [400,150,115],[500,150,115],[600,150,115],
        ]
elif runArgs.runNumber in range(179000,179061+1):
    # for 0-lepton filtered sample
    therun = runArgs.runNumber - 179000
    points_TT_onestepBB = [
        [250,100,50],[300,100,50],[300,200,100],[350,100,50],
        [350,200,100],[350,300,150],[400,100,50],[400,200,100],
        [400,300,150],[450,100,50],[450,200,100],[450,300,150],
        [450,400,200],[500,100,50],[500,200,100],[500,300,150],
        [500,400,200],[550,100,50],[550,200,100],[550,300,150],
        [550,400,200],[550,500,250],[600,100,50],[600,200,100],
        [600,300,150],[600,400,200],[600,500,250],[650,100,50],
        [650,200,100],[650,300,150],[650,400,200],[650,500,250],
        [650,600,300],[700,100,50],[700,200,100],[700,300,150],
        [700,400,200],[700,500,250],[700,600,300],[750,100,50],
        [750,200,100],[750,300,150],[750,400,200],[750,500,250],
        [750,600,300],[750,700,350],[800,100,50],[800,200,100],
        [800,300,150],[800,400,200],[800,500,250],[800,600,300],
        [800,700,350],[850,100,50],[850,200,100],[850,300,150],
        [850,400,200],[850,500,250],[850,600,300],[850,700,350],
        [850,800,400],[850,840,420],
        ]
elif runArgs.runNumber in range(179300,179364+1):
    therun = runArgs.runNumber - 179300
    points_TT_onestepBB = [
        [160,150,145],[200,150,145],[250,150,145],[300,150,145],
        [350,150,145],[400,150,145],[500,150,145],[600,150,145],
        [700,150,145],[160,150,130],[200,150,130],[250,150,130],
        [300,150,130],[350,150,130],[400,150,130],[500,150,130],
        [600,150,130],[700,150,130],[130,106,95],[150,106,60],
        [150,106,95],[170,106,35],[170,106,60],[170,106,75],
        [170,106,95],[190,106,35],[190,106,60],[190,106,75],
        [190,106,95],[210,106,35],[210,106,60],[210,106,75],
        [210,106,95],[230,106,35],[230,106,60],[230,106,75],
        [230,106,95],[250,106,35],[250,106,60],[250,106,75],
        [250,106,95],[300,106,35],[300,106,60],[300,106,75],
        [300,106,95],[350,106,35],[350,106,60],[350,106,75],
        [350,106,95],[400,106,35],[400,106,60],[400,106,75],
        [400,106,95],[500,106,35],[500,106,60],[500,106,75],
        [500,106,95],[600,106,35],[600,106,60],[600,106,75],
        [600,106,95],[700,106,35],[700,106,60],[700,106,75],
        [700,106,95],
        ]
elif runArgs.runNumber in range(179856,179864+1):
    therun = runArgs.runNumber - 179856
    points_TT_onestepBB = [
        [185,105,85],[185,130,110],[185,155,135],[210,130,110],
        [210,155,135],[210,180,160],[240,130,110],[240,185,165],
        [240,210,190],
        ]
elif runArgs.runNumber in range(157990,157998+1):
    therun = runArgs.runNumber - 157990
    points_TT_onestepBB = [
        [185,105,85],[185,130,110],[185,155,135],[210,130,110],
        [210,155,135],[210,180,160],[240,130,110],[240,185,165],
        [240,210,190],
        ]
elif runArgs.runNumber in range(186580,186604+1):
 # 2l-filterered DM=10 grid
    therun = runArgs.runNumber - 186580
    points_TT_onestepBB = [
        [150,140,60],[150,140,90],
        [160,150,90],[160,150,110],[160,150,120],
        [170,160,50],[170,160,75],[170,160,80],[170,160,90],
        [170,160,100],[170,160,110],[170,160,120],
        [180,170,60],[180,170,85],[180,170,90],[180,170,100],
        [180,170,110],[180,170,120],
        [190,180,75],[190,180,95],[190,180,100],[190,180,110],[190,180,120],
        [200,190,105],[200,190,110],
        ]
elif runArgs.runNumber in range(157001,157006+1):
    therun = runArgs.runNumber - 157001
    points_TT_onestepBB = [
        [230,100,50],[275,100,50],[275,150,75],[275,190,95],
        [300,240,120],[350,320,160],
        ]
elif runArgs.runNumber in range(202888,202893+1):
    therun = runArgs.runNumber - 202888
    points_TT_onestepBB = [
        [230,100,50],[275,100,50],[275,150,75],[275,190,95],
        [300,240,120],[350,320,160],
        ]
elif runArgs.runNumber in range(204173,204196+1):
    therun = runArgs.runNumber - 204173
    points_TT_onestepBB = [
        [130,106,100],[150,106,100],[170,106,100],[190,106,100],
        [210,106,100],[230,106,100],[250,106,100],[300,106,100],
        [350,106,100],[400,106,100],[500,106,100],[600,106,100],
        [600,106,100],[170,106,105],[190,106,105],[210,106,105],
        [230,106,105],[250,106,105],[300,106,105],[350,106,105],
        [400,106,105],[500,106,105],[600,106,105],[600,106,105],
        ]
elif runArgs.runNumber in range(204284,204295+1):
    therun = runArgs.runNumber - 204284
    points_TT_onestepBB = [
        [275,100,95],[300,125,120],[350,175,170],[400,225,220],
        [450,275,270],[500,325,320],[275,115,95],[300,140,120],
        [350,190,170],[400,240,220],[450,290,270],[500,340,320],
        ]
elif runArgs.runNumber in range(204305,204314+1):
    therun = runArgs.runNumber - 204305
    points_TT_onestepBB = [
        [300,220,140],[300,220,170],[300,235,170],[300,235,200],[300,250,170],
        [300,250,215],[300,270,205],[300,270,235],[300,285,205],[300,285,235],
        ]
elif runArgs.runNumber in range(204940,204940+1):
    therun = runArgs.runNumber - 204940
    points_TT_onestepBB = [
        [600,300,150],
        ]    
elif runArgs.runNumber in range(205111,205114+1):
    therun = runArgs.runNumber - 205111
    points_TT_onestepBB = [
        [500,490,245],[600,100,50],[600,400,1],[500,490,1],
        ]
    use_Tauola = False
    use_Photos = False
if therun>=0 and therun<len(points_TT_onestepBB):
    masses['1000006'] = points_TT_onestepBB[therun][0]
    masses['1000024'] = points_TT_onestepBB[therun][1]
    masses['1000022'] = points_TT_onestepBB[therun][2]
    stringy = str(int(points_TT_onestepBB[therun][0]))+'_'+str(int(points_TT_onestepBB[therun][1]))+'_'+str(int(points_TT_onestepBB[therun][2]))
    gentype='TT'
    decaytype='onestepBB'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to b+chargino; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_onestepBB[therun]) + ', with ' + str(njets) + ' jets.')
    use_decays=True
    if masses['1000024']-masses['1000022']<81.0:
        use_decays=False
        decaytype+='.offshell'

therun = -1
if runArgs.runNumber in range(177375,177390+1):
    therun = runArgs.runNumber-177375
    points_TT_onestepBB = [
        [350,340,170],[450,440,220]
        ]
if therun>=0 and therun<(len(points_TT_onestepBB)*8):
    masses['1000006'] = points_TT_onestepBB[therun/8][0]
    masses['1000024'] = points_TT_onestepBB[therun/8][1]
    masses['1000022'] = points_TT_onestepBB[therun/8][2]
    stringy = str(int(points_TT_onestepBB[therun/8][0]))+'_'+str(int(points_TT_onestepBB[therun/8][1]))+'_'+str(int(points_TT_onestepBB[therun/8][2]))
    gentype='TT'
    decaytype='onestepBB'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to b+chargino; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_onestepBB[therun/8]) + ', with ' + str(njets) + ' jets.')
    use_decays=True
    if masses['1000024']-masses['1000022']<81.0:
        use_decays=False
        decaytype+='.offshell'
    syst_mod=dict_index_syst[therun%8]

# second batch of signal systematics samples (Jan 2014)
therun = -1
if runArgs.runNumber in range(186818,186853+1):
    therun = runArgs.runNumber-186818
    points_TT_onestepBB = [
        [125,120,60],[150,140,70],[180,174,87],[180,100,50],[250,100,50],
        [250,240,120]
        ]
#    points_TT_onestepBB = [
#        [125,120,060],[150,140,070],[180,174,087],[180,100,050],[250,100,050],[250,240,120]
#
if therun>=0 and therun<(len(points_TT_onestepBB)*6):
    masses['1000006'] = points_TT_onestepBB[therun/6][0]
    masses['1000024'] = points_TT_onestepBB[therun/6][1]
    masses['1000022'] = points_TT_onestepBB[therun/6][2]
    stringy = str(int(points_TT_onestepBB[therun/6][0]))+'_'+str(int(points_TT_onestepBB[therun/6][1]))+'_'+str(int(points_TT_onestepBB[therun/6][2]))
    gentype='TT'
    decaytype='onestepBB'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to b+chargino; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_onestepBB[therun/6]) + ', with ' + str(njets) + ' jets.')
    use_decays=True
    if masses['1000024']-masses['1000022']<81.0:
        use_decays=False
        decaytype+='.offshell'
    syst_mod=dict_index_syst[therun%6]

evt_multiplier = 10.0
if runArgs.runNumber in [202892,202893]:
    evt_multiplier = 13.0
if runArgs.runNumber in [202891]:
    evt_multiplier = 17.0
if runArgs.runNumber in [172698,172702,172706,172721,172744,172748,175708,177053] or runArgs.runNumber in range(177375,177390+1) or runArgs.runNumber in range(178060,178103+1) or runArgs.runNumber in range(179000,179061+1):
    evt_multiplier = 20.0
if runArgs.runNumber in [186580,186587,186588,186594,186595,186596,186600,186601,186602,186604]:
    evt_multiplier = 35.0
if runArgs.runNumber in [186581,186583,186584,186590,186591,186597]:
    evt_multiplier = 70.0
if runArgs.runNumber in [186582,186585,186586,186589,186592,186593,186598,186599,186603]:
    evt_multiplier = 100.0
evgenConfig.contact  = [ "Keisuke.Yoshihara@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

