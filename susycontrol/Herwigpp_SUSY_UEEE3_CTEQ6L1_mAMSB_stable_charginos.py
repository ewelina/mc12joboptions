## SUSY Herwig++ jobOptions for mAMSB electroweak production (long-lived chargino)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.mAMSB')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_mAMSB_stable_charginos_mc12points.py' )

try:
      mass, lifetime = pointdict[runArgs.runNumber]
except:
      raise RuntimeError('DSID %s not found in dictionary. Aborting!' % runArgs.runNumber)

slha_file = 'susy_mAMSB_stable_charginos_C%04d.slha' % mass

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['neutralinos','charginos','sleptons','staus'], slha_file, earlyCopy=True) 

# speciaConfig params
include ( 'MC12JobOptions/SUSYMetadata.py' )
(c1mass, n1mass) = mAMSB_mass_extract(slha_file, ['1000024', '1000022'])

# define metadata
evgenConfig.description = 'Chargino Long Lived mAMSB EW production, slha file: %s, lifetime: %s' % (slha_file,lifetime)
evgenConfig.keywords = ['SUSY','mAMSB','SMP','stableChargino']
evgenConfig.contact  = ['benitez@cern.ch', 'sascha.mehlhase@cern.ch']
evgenConfig.specialConfig = 'AMSBC1Mass=%s*GeV;AMSBN1Mass=%s*GeV;AMSBC1Lifetime=%s*ns;preInclude=SimulationJobOptions/preInclude.AMSB.py' % (c1mass,n1mass,lifetime)

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines() 

# clean up
del cmds
