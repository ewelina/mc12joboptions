# SUSY Herwig++ jobOptions for natural pMSSM
# Use SUSYHIT and bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_DM_pMSSM_tL_tb20_M2_2000_mc12points.py' )
try:
    (M1, mqL3) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_DM_pMSSM_tb_20_M2_2000_M1_%s_mq3_%s.slha' % (M1, mqL3)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stops','sbottoms'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Well-tempered DM pMSSM grid generation with M1 %s mqL3 %s' % (M1, mqL3)
evgenConfig.keywords = ['SUSY','stop','sbottom','pMSSM']
evgenConfig.contact  = ['Brian.Petersen@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

