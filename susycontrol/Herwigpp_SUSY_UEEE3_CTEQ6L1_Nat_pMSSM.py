# SUSY Herwig++ jobOptions for natural pMSSM
# Use SUSYHIT and bottom-up schema for SLHA files

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

include ( 'MC12JobOptions/SUSY_Natural_pMSSM_M1_3000_M2_3xmu_mc12points.py' )
try:
    (mu, mql3) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

# define spectrum file name
slha_file = 'susy_natural_pMSSM_M1_3000_M3_1700_mu_%s_mqL3_%s.slha' % (mu, mql3)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stops','sbottoms'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Natural pMSSM grid generation with mu %s mqL3 %s' % (mu, mql3)
evgenConfig.keywords = ['SUSY','stop','sbottom','pMSSM']
evgenConfig.contact  = ['iacopo.vivarelli@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

