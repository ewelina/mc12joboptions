# SUSY MadGraph+Pythia6 jobOptions for pMSSM simulation

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.pMSSM')
from MadGraphControl.MadGraphUtils import *

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 2.0
num_events=5000

if 'EventMultiplier' in dir(): evt_multiplier=EventMultiplier
if hasattr(runArgs,'EventMultiplier'): evt_multiplier=runArgs.EventMultiplier
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents>0: num_events=runArgs.maxEvents
nevts=num_events*evt_multiplier

ickkw=1
xqcut = 20 #see: https://cp3.irmp.ucl.ac.be/projects/madgraph/wiki/Matching#

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model mssm-full
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define sewk = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
define sstrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > sewk sstrong QED=99 QCD=99 @1
add process p p > sewk sstrong j QED=99 QCD=99 @2
output -f
""")
name="pMSSM_mix"
fcard.close()
iexcfile=0
print "proc card:"
print fcard

#--------------------------------------------------------------
# Beam energy
#--------------------------------------------------------------
### As suggested by MC generator experts
beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RunTimeError("No center of mass energy found.")


#Default Scales
isFixedRenScale="F"
isFixedFacScale="F"

scale=91.1880
renScale =scale
facScale1=scale
facScale2=scale

#--------------------------------------------------------------
# Choice of PDF set 
#--------------------------------------------------------------
doMSTW08PDFSet = None
pdfSet = 'lhapdf'   #default set to 'cteq6l1'
pdfNumber = 21000   # for lhaid

#--------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#--------------------------------------------------------------
# Grab the run card and move it into place
import subprocess
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    log.error('Could not get run card')
elif os.access('run_card.dat',os.R_OK):
    log.error('Old run card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' = ptj ' in line:
            newcard.write('   %f      = ptj ! minimum pt for the jets \n'%(xqcut))
        elif ' = ptb ' in line:
            newcard.write('   %f      = ptb ! minimum pt for the b \n'%(xqcut))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%int(nevts))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ickkw' in line:
            newcard.write('  %i         = ickkw   ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(0))
        elif ' = pdlabel ' in line:
            if doMSTW08PDFSet:
                newcard.write('   \'%s\'      = pdlabel ! PDF set\n   %d      = lhaid       ! PDF number used ONLY for LHAPDF\n' \
                    % (str(pdfSet), int(pdfNumber)))
            else:
              newcard.write(line)
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


print "Modified run_card.dat:"
rcard = open('run_card.dat','r')
print rcard.read()
rcard.close()

process_dir = new_process()

include ( 'MC12JobOptions/SUSY_pMSSM_Mix_mc12points.py' )

try:
  slha_file = pointdict[runArgs.runNumber]
except:
  raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)


log.info("Using paramCard %s" % slha_file)
proccard = subprocess.Popen(['get_files','-data',slha_file])
proccard.wait()

generate(run_card_loc='run_card.dat',param_card_loc=slha_file,mode=0,njobs=1,run_name='Mix',proc_dir=process_dir)

stringy = 'madgraph.'+str(name)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Mix',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


#--------------------------------------------------------------
# Multi-core support
#--------------------------------------------------------------
import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

#--------------------
# Pythia 6
#--------------------
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
runArgs.qcut = max(xqcut+5,xqcut*1.2)
runArgs.use_Tauola = True
if 'syst_mod' in dir():
    runArgs.syst_mod = syst_mod

include ("MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py")
topAlg.Pythia.PythiaCommand += [ "pydat1 mstj 22 1" ]
evgenConfig.generators+=["ParticleGun"] #hack to disable testhepmc
evgenConfig.auxfiles += [slha_file]

#--------------------------------------------------------------
# MC12 metadata
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords = ["pMSSM"]
evgenConfig.contact  = [ "Brian.Petersen@cern.ch" ]
evgenConfig.inputfilecheck = stringy








