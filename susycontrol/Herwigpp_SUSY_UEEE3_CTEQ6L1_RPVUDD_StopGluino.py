def slhaFileFromRunNumber(runNumber, offset):
  include('MC12JobOptions/SUSY_RPVUDD_StopGluino_points.py')
  return rpvUDDStopGluinoPoints[int(runNumber) - int(offset)]

# Start of the run number range
firstRunNumber = 174320

slhaFile = slhaFileFromRunNumber(runArgs.runNumber, firstRunNumber)
print 'Using slha:',slhaFile

evgenConfig.description = 'RPV UDD Stop-Gluino Model, slha file: {0}'.format(slhaFile)
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['thomas.gillam@cern.ch']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticleList = []
sparticleList.append('gluino')
sparticleList.append('stop1')
cmds = buildHerwigppCommands(sparticleList, slhaFile)

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Append our commands
topAlg.Herwigpp.Commands += cmds.splitlines()

## Clean up
del cmds
