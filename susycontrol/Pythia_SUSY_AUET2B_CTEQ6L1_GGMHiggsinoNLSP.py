# set up Pythia
include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

# define spectrum file name
include ( 'MC12JobOptions/SUSY_GGMHiggsinoNLSP_mc12points.py' )
try:
  m3, mu, tanb = pointdict[runArgs.runNumber]
except:
  raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

if tanb == 30:
  slha_file = 'susy_GGMHiggsinoNLSP_%s_%s_%s.slha' % (m3, mu, tanb)
else:
  slha_file = 'susy_GGMHiggsinoNLSP_%s_%s.slha' % (m3, mu)

topAlg.Pythia.SusyInputFile = slha_file
topAlg.Pythia.PythiaCommand += [
                           "pysubs msel 0",
                           "pymssm imss 1 11",
                           "pymssm imss 11 1",
                           "pymssm imss 21 50",
                           "pymssm imss 22 50",
                           "pysubs msub 243 1",
                           "pysubs msub 244 1",
                           "pysubs msub 216 1",
                           "pysubs msub 217 1",
                           "pysubs msub 220 1",
                           "pysubs msub 226 1",
                           "pysubs msub 229 1",
                           "pysubs msub 230 1"]

evgenConfig.contact = [ "Natalia.Panikashvili@cern.ch" ]
evgenConfig.description = "PYTHIA 6 Higgsino-like NLSP GGM with tan beta = 1.5" 
evgenConfig.process = "PYTHIA 6 used since PYTHIA 8.165 does not support decays to gravitinos" 
evgenConfig.keywords = ["SUSY", "GGM", "higgsino", "nlsp"] 
evgenConfig.auxfiles += [ slha_file ]

from GeneratorFilters.GeneratorFiltersConf import ZtoLeptonFilter
topAlg += ZtoLeptonFilter()
StreamEVGEN.RequireAlgs  = [ "ZtoLeptonFilter" ]
