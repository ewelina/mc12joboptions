## SUSY Herwig++ jobOptions for L slepton/Bino simplified model with lambda_133 R-parity violation.
## Author: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVSimplifiedModel')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_RPV_SlepL_133_mc12points.py' )
try:
    mSlepL, mBino = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_RPV_%s_%s_SlepL_133_simplified.slha' % (mSlepL, mBino)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['sleptons_L', 'stau1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'L slepton/Bino simplified model with lambda_133 R-parity violation. with m_SlepL = %s, m_Bino = %s' % (mSlepL,mBino)
evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda133', 'Slepton']
evgenConfig.contact  = ['flowerde@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
