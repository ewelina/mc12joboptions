## SUSY Herwig++ jobOptions for Wino/Bino simplified model with lambda_133 R-parity violation.
## Author: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVSimplifiedModel')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_RPV_Wino_133_mc12points.py' )
try:
    mWino, mBino = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_RPV_%s_%s_Wino_133_simplified.slha' % (mWino, mBino)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gauginos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Wino/Bino simplified model with lambda_133 R-parity violation. with m_Wino = %s, m_Bino = %s' % (mWino,mBino)
evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda133', 'Gauginos']
evgenConfig.contact  = ['flowerde@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
