## SUSY Herwig++ jobOptions for simplified model Gtb

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Gtb')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_Gtb_mc12points.py' )
try:
    (mgl,mchar,mlsp) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_Gtb_G%s_C%s_L%s.slha' % (mgl, mchar, mlsp)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Gtb grid generation with m_gluino = %s, m_chargino = %s GeV, m_lsp = %s GeV' % (mgl, mchar, mlsp)
evgenConfig.keywords = ['SUSY','Gtb','sbottom','stop','gluino']
evgenConfig.contact  = ['antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
