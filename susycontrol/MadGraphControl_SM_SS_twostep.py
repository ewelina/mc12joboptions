# Two-step squark decay to LSP, x1=1/2, x2=1/4, WWZZ (Multi-lepton)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

points2b2q_base = []
toremove = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
counter2=0
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter2 += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>1200 or msq-diff<0 or msq-diff>800 or msq<250: continue
        if msq in toremove: continue
        if msq<=575 and msq-diff<200: continue
        if msq-diff>600 and counter2%2==0: continue
        point = [msq,msq-diff]
        points2b2q_base += [ point ]
minDSID_base = 173865
        
# Grid extension
points2b2q_extension = [[205,45],[245,5],[225,145],[265,105],[305,65],[345,25],[325,165],[365,125],[405,85],[445,45],[485,5],[425,185],[465,145],[505,105],[545,65]]
minDSID_extension = 179831

# DC14
points2b2q_dc14 = [
  (650.,100.), (750.,100.), (850.,100.), (950.,100.),
  (650.,300.), (650.,400.), (650.,500.), (650.,600.)
]
minDSID_dc14 = 204965

# Selects appropriate list of masses depending on DSID
def get_points():
    therun = (runArgs.runNumber-minDSID_base)
    if 0 <= therun < len(points2b2q_base):
        loginfo = 'Registered generation of two-step SS point '+str(therun)
        return (points2b2q_base,therun,loginfo)
    therun = (runArgs.runNumber-minDSID_extension)
    if 0 <= therun < len(points2b2q_extension):
        loginfo = 'Registered generation of grid extension: SS two step with WWZZ '+str(therun)
        return (points2b2q_extension,therun,loginfo)
    therun = (runArgs.runNumber-minDSID_dc14)
    if 0 <= therun < len(points2b2q_dc14):
        global use_Photos # don't remove!
        use_Photos = False
        loginfo = 'Registered generation of two-step SS point for DC14 '+str(therun)
        return (points2b2q_dc14,therun,loginfo)
    raise BaseException("Unknown DSID %d"%(runArgs.runNumber))
    return ([],-1,'')
    
points2b2q,therun,loginfo = get_points()

# Grid points
if therun>=0 and therun<len(points2b2q):
    evgenLog.info(loginfo)
    for q in squarksl: masses[q] = points2b2q[therun][0] # changed to squarksl!
    masses['1000022'] = points2b2q[therun][1]
    masses['1000024'] = 0.5*(points2b2q[therun][0]+points2b2q[therun][1])
    masses['1000023'] = 0.5*(masses['1000024']+points2b2q[therun][1])
    stringy = str(int(points2b2q[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(points2b2q[therun][1]))
    gentype='SS'
    decaytype='twostep'
    njets=1
    use_decays=False
    use_Tauola=False

# Systematics
therun = runArgs.runNumber-152857
if therun>=0 and therun<5*5:
    evgenLog.info('Registered generation of grid ten systematics, two-step SS point '+str(therun))
    syspoints = [ [235,75], [205,45], [335,95], [325,165], [300,250] ]
    for q in squarks: masses[q] = syspoints[therun%5][0]
    masses['1000022'] = syspoints[therun%5][1]
    masses['1000024'] = 0.5*(syspoints[therun%5][0]+syspoints[therun%5][1])
    masses['1000023'] = 0.5*(masses['1000024']+syspoints[therun%5][1])
    syst_mod='qup'
    if therun>=5: syst_mod='qdown'
    if therun>=10: syst_mod='radhi'
    if therun>=15: syst_mod='radlo'
    if therun>=20: syst_mod='rad0'
    stringy = str(int(syspoints[therun%5][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(syspoints[therun%5][1]))+'_'+syst_mod
    gentype='SS'
    decaytype='twostep'
    njets=1
    use_decays=False

if runArgs.runNumber in [152862,152863,152864, \
                 152865]:
    evt_multiplier = 4.0

evgenConfig.contact  = [ "genest@lpsc.in2p3.fr" ]
evgenConfig.keywords += ['squark','two_step']
evgenConfig.description = 'squark production two step without slepton in simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )
