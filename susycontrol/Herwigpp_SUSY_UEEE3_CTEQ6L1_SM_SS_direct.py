from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SM_SS_direct')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_SM_SS_direct_mc12points.py' )
try:
    (m_sq, m_lsp) = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

slha_file = "SM_SS_direct_%d_%d.slha" % (m_sq, m_lsp)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['squarks'], slha_file, 'TwoParticleInclusive')
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H0\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H+\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H-\n"
cmds += "insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/A0\n"

# define metadata
evgenConfig.description = 'sq-pair production with m_sq = %d, m_lsp = %d GeV' % (m_sq, m_lsp)
evgenConfig.keywords = ['SUSY','strong', 'inclusive', 'squark', 'pair', 'pair production']
evgenConfig.contact  = ['scaron@nikhef.nl','ingrid.deigaard@cern.ch','geert-jan.besjes@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
