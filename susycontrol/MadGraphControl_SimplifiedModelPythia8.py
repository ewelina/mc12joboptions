#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
if hasattr(runArgs,'pythia8_tune'):
    if runArgs.pythia8_tune=='AU2_CT10':
        include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
    elif runArgs.pythia8_tune=='AZ_CTEQ6L1':
        include("MC12JobOptions/Pythia8_AZ_CTEQ6L1_Common.py")
    elif runArgs.pythia8_tune=='A14_CTEQ6L1':
        include("MC12JobOptions/Pythia8_A14_CTEQ6L1_Common.py")
    elif runArgs.pythia8_tune=='A14_NNPDF23LO':
        include("MC12JobOptions/Pythia8_A14_NNPDF23LO_Common.py")
    else:
        include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
else:
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_MadGraph.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            ]
if njets>0:
    topAlg.Pythia8.Commands += ["Merging:mayRemoveDecayProducts = on",
                                "Merging:nJetMax = "+str(njets),
                                ]
    if 'doKtMerge' in dir() and doKtMerge:
        if hasattr(runArgs,'qcut'): TMS = runArgs.qcut
        else: TMS = 30.
        topAlg.Pythia8.Commands += ["Merging:doKTMerging = on",
                                    "Merging:TMS = "+str(TMS),
                                    "Merging:ktType = 1",
                                    "Merging:Dparameter = 0.4",
                                    ]
    else:
        if not hasattr(runArgs,'mmjj') or not hasattr(runArgs,'ptj') or not hasattr(runArgs,'drjj'):
            evgenLog.info('Matching parameters were not sucessfully transferred from the fragment.')
            if not hasattr(runArgs,'ptj') : runArgs.ptj = 50
            if not hasattr(runArgs,'mmjj'): runArgs.mmjj = 25.
            if not hasattr(runArgs,'drjj'): runArgs.drjj=0.1
        topAlg.Pythia8.Commands += ["Merging:doCutBasedMerging = on",
                                    "Merging:QijMS = "+str(runArgs.mmjj),
                                    "Merging:pTiMS = "+str(runArgs.ptj),
                                    "Merging:dRijMS = "+str(runArgs.drjj),
                                    ]
    if hasattr(runArgs,'gentype'):
        if runArgs.gentype=='SS':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{ul,1000002}{ul~,-1000002}{ur,2000002}{ur~,-2000002}{dl,1000001}{dl~,-1000001}{dr,2000001}{dr~,-2000001}{sl,1000003}{sl~,-1000003}{sr,2000003}{sr~,-2000003}{cl,1000004}{cl~,-1000004}{cr,2000004}{cr~,-2000004}"]
        elif runArgs.gentype=='GG':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{go,1000021}{go,1000021}"]
        elif runArgs.gentype == 'BB':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{b1,1000005}{b1~,-1000005}"]
        elif runArgs.gentype == 'Scharm':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{cl,1000004}{cl~,-1000004}{cr,2000004}{cr~,-2000004}"]
        elif runArgs.gentype == 'TT':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]
        elif runArgs.gentype=='CC' or runArgs.gentype=='C1C1':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}"]
        elif runArgs.gentype=='C1N2':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}{n2,1000023}"]
        elif runArgs.gentype=='N2N3':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{n2,1000023}{n3,1000025}"]
        elif runArgs.gentype=='N1N1':
            topAlg.Pythia8.Commands += ["Merging:Process = pp>{n1,1000022}{n1,1000022}"]
        

# Special hook for stop 4-body decays
if runArgs.runNumber in range(178414,178473+1):
    evgenLog.error('No known special hook for stop 4-body decays - need equivalent Pythia8 command!!')

if hasattr(runArgs,'syst_mod') and runArgs.syst_mod is not None:
    evgenLog.error('No known equivalent for systematics in Pythia8!')

if hasattr(runArgs,'use_Photos'):
    usePhotos=runArgs.use_Photos
else:
    usePhotos=True

if hasattr(runArgs,'use_METfilter'):
    useMETfilter=runArgs.use_METfilter
    if useMETfilter:
        METfilterCut=runArgs.METfilterCut
else:
    useMETfilter=False

if not hasattr(runArgs,'useHiggsFilter'):
    runArgs.useHiggsFilter=False
if not hasattr(runArgs,'useParticleFilter'):
    runArgs.useParticleFilter=False

if hasattr(runArgs,'use_MultiElecMuTauFilter'):
    useMultiElecMuTauFilter=runArgs.use_MultiElecMuTauFilter
    if(useMultiElecMuTauFilter):
        MultiElecMuTauFilterPtCut=runArgs.MultiElecMuTauFilterPtCut
        MultiElecMuTauFilterEtaCut=runArgs.MultiElecMuTauFilterEtaCut
        MultiElecMuTauFilterLepCut=runArgs.MultiElecMuTauFilterLepCut
        MultiElecMuTauFilterTauPtCut=runArgs.MultiElecMuTauFilterTauPtCut
        MultiElecMuTauFilterIncludeTau=runArgs.MultiElecMuTauFilterIncludeTau
else:
    useMultiElecMuTauFilter=False
if hasattr(runArgs,'use_MultiLeptonFilter'):
    useMultiLeptonFilter=runArgs.use_MultiLeptonFilter
    if(useMultiLeptonFilter):
        MultiLeptonFilterPtCut=runArgs.MultiLeptonFilterPtCut
        MultiLeptonFilterEtaCut=runArgs.MultiLeptonFilterEtaCut
        MultiLeptonFilterLepCut=runArgs.MultiLeptonFilterLepCut
else:
    useMultiLeptonFilter=False


# ... Photos
if usePhotos:
    include ( "MC12JobOptions/Pythia8_Photos.py" )

if hasattr(runArgs,'decaytype') and runArgs.decaytype is not None:
    if hasattr(runArgs,'gentype') and runArgs.gentype is not None:
        if 'GG' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
                topAlg += ParticleFilter()
                ParticleFilter = topAlg.ParticleFilter
                ParticleFilter.Etacut= 10.
                ParticleFilter.Energycut = 100000000.0
                ParticleFilter.PDG = 1000024
                ParticleFilter.StatusReq = -1
                ParticleFilter.Ptcut = 0.
                from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
                topAlg += ParentChildFilter()
                ParentChildFilter = topAlg.ParentChildFilter
                ParentChildFilter.PDGParent = [1000021]
                ParentChildFilter.PDGChild = [1000023]
                StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]
                StreamEVGEN.RequireAlgs +=  [ "ParentChildFilter" ]

        if 'SS' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
                squarks = [1000001, -1000001, 2000001, -2000001, 1000002, -1000002, 2000002, -2000002, 1000003, -1000003, 2000003, -2000003, 1000004, -1000004, 2000004, -2000004]
                topAlg += ParticleFilter()
                ParticleFilter = topAlg.ParticleFilter
                ParticleFilter.Etacut= 10.
                ParticleFilter.Energycut = 100000000.0
                ParticleFilter.PDG = 1000024
                ParticleFilter.StatusReq = -1
                ParticleFilter.Ptcut = 0.
                from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
                topAlg += ParentChildFilter()
                ParentChildFilter = topAlg.ParentChildFilter
                ParentChildFilter.PDGParent = squarks
                ParentChildFilter.PDGChild = [1000023]
                StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]
                StreamEVGEN.RequireAlgs +=  [ "ParentChildFilter" ]


# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph", "Pythia8"]
if hasattr(runArgs,'decaytype') and runArgs.decaytype is not None:
    if hasattr(runArgs,'gentype') and runArgs.gentype is not None:
        if 'GG' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                evgenConfig.efficiency = 0.5

if not hasattr(runArgs,'inputGeneratorFile'):
    print 'ERROR: something wasnt write in file name transfer from the fragment.'
    runArgs.inputGeneratorFile='madgraph.*._events.tar.gz'
evgenConfig.description = 'SUSY Simplified Model generation with MadGraph/Pythia8 in MC12'
evgenConfig.keywords = ["SUSY"]
evgenConfig.inputfilecheck = runArgs.inputGeneratorFile.split('._0')[0]

if useMETfilter:
    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()
    
    METFilter = topAlg.METFilter
    METFilter.MissingEtCut = METfilterCut*GeV
    evgenLog.info("Using a MET filter lower cut at " + str(METFilter.MissingEtCut) + " MeV.")
    try:
        METFilter.MissingEtUpperCut = runArgs.METfilterUpperCut*GeV
        evgenLog.info("Using a MET filter upper cut at " + str(METFilter.MissingEtUpperCut) + " MeV.")
    except:
        evgenLog.info("Not using a MET filter upper cut.")
    METFilter.MissingEtCalcOption = 1
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "METFilter" ]

if runArgs.useHiggsFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiHiggsFilter
    topAlg += MultiHiggsFilter()
    
    MultiHiggsFilter = topAlg.MultiHiggsFilter
    MultiHiggsFilter.Exclusive = runArgs.ExclusiveNHiggs
    MultiHiggsFilter.NHiggs = runArgs.NHiggs
    MultiHiggsFilter.UseStatus = True
    MultiHiggsFilter.Status = 3
    if MultiHiggsFilter.Exclusive:
        evgenLog.info("Using a Higgs filter requiring exactly " + str(MultiHiggsFilter.NHiggs) + " Higgs(es).")
    else:
        evgenLog.info("Using a Higgs filter requiring at least " + str(MultiHiggsFilter.NHiggs) + " Higgs(es).")
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "MultiHiggsFilter" ]

if useMultiElecMuTauFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    topAlg += MultiElecMuTauFilter()

    MultiElecMuTauFilter.MinPt = MultiElecMuTauFilterPtCut*GeV
    MultiElecMuTauFilter.MinVisPtHadTau = MultiElecMuTauFilterTauPtCut*GeV
    MultiElecMuTauFilter.MaxEta = MultiElecMuTauFilterEtaCut
    MultiElecMuTauFilter.NLeptons = MultiElecMuTauFilterLepCut
    MultiElecMuTauFilter.IncludeHadTaus = MultiElecMuTauFilterIncludeTau

    StreamEVGEN.RequireAlgs +=  [ "MultiElecMuTauFilter" ]

if useMultiLeptonFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter()
    MultiLeptonFilter.MinPt = MultiLeptonFilterPtCut*GeV
    MultiLeptonFilter.MaxEta = MultiLeptonFilterEtaCut
    MultiLeptonFilter.NLeptons = MultiLeptonFilterLepCut

    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]


if runArgs.useParticleFilter:
    from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
    topAlg += ParticleFilter()
    
    ParticleFilter = topAlg.ParticleFilter
    ParticleFilter.Exclusive = runArgs.ExclusiveNParticles
    ParticleFilter.MinParts = runArgs.MinParts
    ParticleFilter.StatusReq = 3
    ParticleFilter.Ptcut = 0.0
    ParticleFilter.Etacut= 10.
    ParticleFilter.Energycut = 100000000.0
    ParticleFilter.PDG = runArgs.RequiredPDGID
    
    if ParticleFilter.Exclusive:
        evgenLog.info("Using a Particle filter requiring exactly " + str(ParticleFilter.MinParts) + " particle(s) with PDGID " + str(ParticleFilter.PDG) + ".")
    else:
        evgenLog.info("Using a Particle filter requiring at least " + str(ParticleFilter.MinParts) + " particle(s) with PDGID "+ str(ParticleFilter.PDG) + ".")
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]

# Special hook for sbottom -> b+neut2/neut1 mixed decays
if runArgs.runNumber in range(179062,179222+1):
    from TruthExamples.TruthExamplesConf import TestHepMC
    topAlg += TestHepMC(CmEnergy=runArgs.ecmEnergy*Units.GeV,MaxTransVtxDisp=300.,MaxTransVtxDispLoose=300.)
    StreamEVGEN.RequireAlgs += ["TestHepMC"]

