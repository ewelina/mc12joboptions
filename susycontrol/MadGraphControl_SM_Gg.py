import MadGraphControl.MadGraphUtils as mgutils

# SUSY Simplified Model process:
# Gluino pair production with up to one extra jet in MadGraph,
# 1-loop decay of the gluino into a gluon + a neutralino1 in PYTHIA.
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model mssm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define sq = ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > go go / sq @0
add process p p > go go j / sq @1
output -f
""")
fcard.close()

# Define parameters caracterizing this simplified model
mgo = float(runArgs.jobConfig[0].split('.')[2].split('_')[3][1:])
mn1 = float(runArgs.jobConfig[0].split('.')[2].split('_')[4][1:])
masses = {'1000021':mgo,'1000022':mn1}
decays = {'1000021':"""DECAY   1000021     4.85367509E-11   # gluino decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        21   # BR(~g -> ~chi_10 g)
#
#         PDG            Width
"""}
runArgs.qcut = 500. # default MLM matching scale
if masses['1000021'] < runArgs.qcut * 4.: runArgs.qcut = masses['1000021'] * 0.25
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RunTimeError("No center of mass energy found.")

if (mgo <= 450):
    evt_multiplier = 4
else:
    evt_multiplier = 2
nevts = runArgs.maxEvents if runArgs.maxEvents > 0 else 5000 

# Build MadGraph param and run cards
param_card_name = mgutils.build_param_card(param_card_old='param_card.SM.GG.direct.dat',
                                           masses=masses,decays=decays)
run_card_name = mgutils.build_run_card(run_card_old='run_card.SM.dat',xqcut=runArgs.qcut,
                                       nevts=nevts*evt_multiplier,
                                       rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy)

# Generate the new process
process_dir = mgutils.new_process(card_loc='proc_card_mg5.dat')
mgutils.generate(run_card_loc=run_card_name,param_card_loc=param_card_name,mode=0,njobs=1,
                 run_name='Test',proc_dir=process_dir)

# Arrange the output
runArgs.inputGeneratorFile = 'madgraph.%i.MadGraph_SM_Gg_G%d_N%d._00001.events.tar.gz' % (
    runArgs.runNumber,int(masses['1000021']),int(masses['1000022']))
skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events = runArgs.skipEvents
mgutils.arrange_output(run_name='Test',proc_dir=process_dir,
                       outputDS=runArgs.inputGeneratorFile,
                       skip_events=skip_events)

# Interface to PYTHIA6
include('MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py')

# Information about dataset
evgenConfig.contact = ["renaud.bruneliere@cern.ch"]
evgenConfig.keywords += ['SUSY']
evgenConfig.description = "Direct gluino production with gluino to gluon + neutralino1 decay"
