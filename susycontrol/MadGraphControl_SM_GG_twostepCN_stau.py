# Gluino pair production, two-step decay through chargino1/neutralino2 then stau/tau sneutrino
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

Mgluino = [665,825,985,1145,1305,1105,1265,905,1065,1225,1025,1185,1345,287,825,985,1145,1305,317,1105,1265,745,905,1065,1225,397,1025,1185,825,985,1145,477,945,1105,905,1065,1225,557,1025,1185,985,1145,637,185,345,505,265,425,345,505,465,425,585,545,505,665,625,785,585,745,705,865,665,825,945,745]

Mlsp = [105,105,105,105,105,145,145,185,185,185,225,225,225,262,265,265,265,265,292,305,305,345,345,345,345,372,385,385,425,425,425,452,465,465,505,505,505,532,545,545,585,585,612,105,105,105,185,185,265,265,305,345,345,385,425,425,465,465,505,505,545,545,585,585,625,665]

therun = runArgs.runNumber-204764
if therun>=0 and therun<len(Mgluino):
	evgenLog.info('Registered generation of gluino 2-step grid point with intermediate stau '+str(therun))
	masses['1000021'] = Mgluino[therun]
	masses['1000022'] = Mlsp[therun]
	masses['1000023'] = 0.5*(Mgluino[therun]+Mlsp[therun])
	masses['1000024'] = 0.5*(Mgluino[therun]+Mlsp[therun])
	masses['1000015'] = 0.25*(3.*Mlsp[therun]+Mgluino[therun])
	masses['1000016'] = 0.25*(3.*Mlsp[therun]+Mgluino[therun])
	stringy = str(int(Mgluino[therun]))+'_'+str(int(masses['1000024']))+'_'+str(int(masses['1000016']))+'_'+str(int(Mlsp[therun]))
	gentype='GG'
	decaytype='twostepCN_stau'
	njets=1
	use_decays=False
	use_Tauola=False

evgenConfig.contact  = [ "martindl@cern.ch" ]
evgenConfig.keywords += ['gluino','two_step','stau']
evgenConfig.description = 'simplified model of gluino pair production with two-step decay via stau'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

