include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

mcpoints = {204325: [160, 350],
            204326: [160, 650],
            204327: [160, 950],
            204328: [310, 450],
            204329: [310, 650],
            204330: [310, 950],
            204331: [460, 650],
            204332: [460, 800],
            204333: [460, 950],
            204755: [210, 950],
            204756: [460, 850],
            204757: [510, 850],
            204758: [510, 900],
            204759: [510, 950],
            204760: [560, 800],
            204761: [560, 850],
            204762: [560, 900],
            204763: [560, 950],
            }

try:
    mu,mq3L = mcpoints[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)

slha_file = "susy_bRPV_mu%i_mq3L%i.slha"%(mu,mq3L)
topAlg.Pythia.SusyInputFile = slha_file

topAlg.Pythia.PythiaCommand += ["pysubs msel 39", "pymssm imss 1 11"]
topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
topAlg.Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
topAlg.Pythia.PythiaCommand += ["pypars mstp 95 0"]
topAlg.Pythia.PythiaCommand += ["pydat1 mstj 22 3"]
topAlg.Pythia.PythiaCommand += ["pydat1 parj 72 100000."]

evgenConfig.contact = [ "Andreas.Redelbach@physik.uni-wuerzburg.de" ]
evgenConfig.description = "SUSY model with bilinear R-parity violation"
evgenConfig.process = "add process"
evgenConfig.keywords = ["SUSY", "addkw2"]
evgenConfig.auxfiles += [ slha_file ]
