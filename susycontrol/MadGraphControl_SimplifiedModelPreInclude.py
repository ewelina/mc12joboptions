# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation

# Simple variable setups
njets = -1
stringy = ''
masses = {}

# Event multipliers for getting more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 2.0

# Useful definitions
squarks = []
squarksl = []
for anum in [1,2,3,4]:
    squarks += [str(1000000+anum),str(-1000000-anum),str(2000000+anum),str(-2000000-anum)]
    squarksl += [str(1000000+anum),str(-1000000-anum)]
dict_index_syst = {0:'scalefactup',
                   1:'scalefactdown',
                   2:'alpsfactup',
                   3:'alpsfactdown',
                   4:'moreFSR',
                   5:'lessFSR',
                   6:'qup',
                   7:'qdown'}

# Basic settings for production and filters
SLHAonly = False
syst_mod=None
use_Tauola=True
use_Photos=True
use_METfilter=False
use_MultiElecMuTauFilter=False
use_MultiLeptonFilter=False

# Set up skip events - should almost never be used
skip_events=0

# Set random seed
rand_seed=1234
if hasattr(runArgs, "randomSeed"):
    rand_seed=runArgs.randomSeed

# Set beam energy - default to 8 TeV c.o.m.
beamEnergy = 4000.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.

if 'EventMultiplier' in dir(): evt_multiplier=EventMultiplier
if hasattr(runArgs,'EventMultiplier'): evt_multiplier=runArgs.EventMultiplier
nevts=5000*evt_multiplier
evt_multiplier=-1

# Set pythia6 or pythia8
usePythia6 = True
pythia_tune = 'AUET2B_CTEQ6L1'
pythia8_tune = 'AU2_CTEQ6L1'

# in case someone needs to be able to keep the output directory for testing
keepOutput = False
