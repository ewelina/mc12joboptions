from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > j j @1
add process p p > j j j @2
add process p p > j j j j @3
output -f
""") 
# Only four jets possible if we use gcc 4.3
# Only five jets possible in MadGraph as of 1.5.2
fcard.close()

process_dir = new_process()

if runArgs.runNumber==147569:
    jetmin = 15 
    xqcut = 10 
    slice = 1
    nevents = 50000 #50000 That should be OK for 5000 events.
elif runArgs.runNumber==147570: 
    jetmin = 50
    xqcut = 20
    slice = 2
    nevents = 1000000 #1000000 That should be OK for 5000 events.
elif runArgs.runNumber==147571: 
    jetmin = 120 
    xqcut = 30 
    slice = 3
    nevents = 800000 #800000 That should be OK for 50 events. 


if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 4000.

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' xptj ' in line:
            newcard.write('   %i      = xptj ! minimum pt for at least one jet \n'%(jetmin))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_MultiJet_'+str(jetmin)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

runArgs.use_Tauola=True
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
runArgs.qcut=20 # Not used in the end
include('MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py')

# Replacement configuration where necessary
#--------------------------------------------------------------
evgenConfig.keywords = ["SM","MultiJet"]
evgenConfig.inputfilecheck = stringy
# evgenConfig.minevents = 25
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0
      showerkt=T
      imss(21)=24
      imss(22)=24  
    """

phojf.write(phojinp)
phojf.close()

if slice < 3:
    filterString = 'MC12JobOptions/JetFilter_JZ'+str(slice)+'.py'
elif slice == 3:
    filterString = 'MC12JobOptions/JetFilter_JZ'+str(slice)+'i.py'
else:
    sys.exit("Exiting ==> Slice > 3 wasn't supposed to be here.")

include(filterString)
