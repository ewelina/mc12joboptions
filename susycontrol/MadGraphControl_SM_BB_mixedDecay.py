# Sbottom pair-production with mixed decays
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

therun = runArgs.runNumber-179062

points_BB_mixed = []
l_m_sb = [200,300,400,500,600,700,800,900,1000]
#add grid with m_chi2 = m_chi1*2
for m_sb in l_m_sb:
    for m_neut2 in range(100,1001,100):
        if m_neut2 < m_sb:
            points_BB_mixed.append((m_sb,m_neut2,int(m_neut2/2)))
    for m_neut2 in [260]:
        if m_neut2 < m_sb:
            points_BB_mixed.append((m_sb,m_neut2,int(m_neut2/2)))
    m_neut2 = m_sb-5
    points_BB_mixed.append((m_sb,m_neut2,int(m_neut2/2)))
    m_neut2 = m_sb-25
    points_BB_mixed.append((m_sb,m_neut2,int(m_neut2/2)))
#add grid with m_chi1 = 60 GeV
for m_sb in l_m_sb:
    for m_neut2 in range(100,1001,100):
        if m_neut2 < m_sb:
            points_BB_mixed.append((m_sb,m_neut2,60))
    for m_neut2 in [155,180,130]:
        if m_neut2 < m_sb:
            points_BB_mixed.append((m_sb,m_neut2,60))
    points_BB_mixed.append((m_sb,m_sb-5,60))
    points_BB_mixed.append((m_sb,m_sb-25,60))
points_BB_mixed = sorted(points_BB_mixed)

if therun>=0 and therun<(len(points_BB_mixed)):
    m_sb = points_BB_mixed[therun][0]
    m_chi2 = points_BB_mixed[therun][1]
    m_LSP = points_BB_mixed[therun][2]
    masses['1000005'] = m_sb
    masses['1000023'] = m_chi2
    masses['1000022'] = m_LSP
    gentype='BB'
    decaytype='mixed.3body'
    SLHAexactCopy = False
    if (m_chi2-m_LSP > 90) and (m_chi2-m_LSP < 125):
        decaytype='mixed.Z'
        SLHAexactCopy = False
    elif (m_chi2-m_LSP > 125):
        decaytype='mixed.Higgs'
        SLHAexactCopy = False
    njets=1
    str_info_METfilter = "noFilter"
    str_stringy_METfilter = "noFilter"
    stringy = str(int(points_BB_mixed[therun][0]))+'_'+str(int(points_BB_mixed[therun][1]))+'_'+str(int(points_BB_mixed[therun][2]))+'_'+str_stringy_METfilter
    evgenLog.info('Registered generation of sbottom pair production, sbottom to b+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_BB_mixed[therun]) + ', with ' + str(njets) + ' jets. Using MET filter ' +  str_info_METfilter + '.')
    use_decays=False
    use_Tauola=False
    use_METfilter=False

    runArgs.useParticleFilter=True
    runArgs.ExclusiveNParticles=True
    runArgs.MinParts=1
    runArgs.RequiredPDGID=1000023

    evt_multiplier = 5.0

evgenConfig.contact  = [ "alexandru.dafinca@cern.ch" ]
evgenConfig.keywords += ['sbottom', 'cascade_decays']
evgenConfig.description = 'sbottom direct pair production in simplified models; one leg: sb->b+neut2, neut2->H/Z/ff+neut1; other leg sb->b+neut1.'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

