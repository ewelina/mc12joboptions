#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
if hasattr(runArgs,'pythia_une') and runArgs.pythia_tune=='Perugia2012':
    include ( "MC12JobOptions/Pythia_Perugia2012_Common.py" )
else:
    include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
Pythia.PythiaCommand = ["pyinit user madgraph"]
#--------------------------------------------------------------
Pythia.PythiaCommand +=  [
            "pystat 1 3 4 5",
            "pyinit dumpr 1 5",
            "pyinit pylistf 1",
            ]

# Special hook for stop 4-body decays
if 'do4bodydecay' not in dir():
    do4bodydecay = False
if runArgs.runNumber in range(178414,178473+1) or runArgs.runNumber in range(183973,183996+1) or runArgs.runNumber in range(204686,204691+1) or runArgs.runNumber in range(204674,204685+1) or do4bodydecay:
    Pythia.PythiaCommand += ["pysubs ckin 45 0", "pysubs ckin 47 0"]

if hasattr(runArgs,'syst_mod') and runArgs.syst_mod is not None:
    if 'radhi' in runArgs.syst_mod: Pythia.Tune_Name='PYTUNE_351'
    elif 'radlo' in runArgs.syst_mod: Pythia.Tune_Name='PYTUNE_352'
    elif 'rad' in runArgs.syst_mod: Pythia.Tune_Name='PYTUNE_350'
    elif 'alpsfactup' in runArgs.syst_mod: Pythia.PythiaCommand +=  ["pypars parp 64 4.0"]
    elif 'alpsfactdown' in runArgs.syst_mod: Pythia.PythiaCommand +=  ["pypars parp 64 0.25"]
    elif 'moreFSR' in runArgs.syst_mod:
        PythiaParameterList = [ "PARP(72)=0.7905", "PARJ(82)=0.5"]
        topAlg.Pythia.PygiveCommand += PythiaParameterList
    elif 'lessFSR' in runArgs.syst_mod:
        PythiaParameterList = [ "PARP(72)=0.2635", "PARJ(82)=1.66"]
        topAlg.Pythia.PygiveCommand += PythiaParameterList

if hasattr(runArgs,'use_Tauola'):
    useTauola=runArgs.use_Tauola
else:
    useTauola=True

if hasattr(runArgs,'use_Photos'):
    usePhotos=runArgs.use_Photos
else:
    usePhotos=True

if hasattr(runArgs,'use_METfilter'):
    useMETfilter=runArgs.use_METfilter
    if useMETfilter:
        METfilterCut=runArgs.METfilterCut
else:
    useMETfilter=False

if not hasattr(runArgs,'useHiggsFilter'):
    runArgs.useHiggsFilter=False
if not hasattr(runArgs,'useParticleFilter'):
    runArgs.useParticleFilter=False

if hasattr(runArgs,'use_MultiElecMuTauFilter'):
    useMultiElecMuTauFilter=runArgs.use_MultiElecMuTauFilter
    if(useMultiElecMuTauFilter):
        MultiElecMuTauFilterPtCut=runArgs.MultiElecMuTauFilterPtCut
        MultiElecMuTauFilterEtaCut=runArgs.MultiElecMuTauFilterEtaCut
        MultiElecMuTauFilterLepCut=runArgs.MultiElecMuTauFilterLepCut
        MultiElecMuTauFilterTauPtCut=runArgs.MultiElecMuTauFilterTauPtCut
        MultiElecMuTauFilterIncludeTau=runArgs.MultiElecMuTauFilterIncludeTau
else:
    useMultiElecMuTauFilter=False
if hasattr(runArgs,'use_MultiLeptonFilter'):
    useMultiLeptonFilter=runArgs.use_MultiLeptonFilter
    if(useMultiLeptonFilter):
        MultiLeptonFilterPtCut=runArgs.MultiLeptonFilterPtCut
        MultiLeptonFilterEtaCut=runArgs.MultiLeptonFilterEtaCut
        MultiLeptonFilterLepCut=runArgs.MultiLeptonFilterLepCut
else:
    useMultiLeptonFilter=False


# ... Tauola
if useTauola:
    include ( "MC12JobOptions/Pythia_Tauola.py" )
# ... Photos
if usePhotos:
    include ( "MC12JobOptions/Pythia_Photos.py" )

if hasattr(runArgs,'decaytype') and runArgs.decaytype is not None:
    if hasattr(runArgs,'gentype') and runArgs.gentype is not None:
        if 'GG' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
                topAlg += ParticleFilter()
                ParticleFilter = topAlg.ParticleFilter
                ParticleFilter.Etacut= 10.
                ParticleFilter.Energycut = 100000000.0
                ParticleFilter.PDG = 1000024
                ParticleFilter.StatusReq = -1
                ParticleFilter.Ptcut = 0.
                from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
                topAlg += ParentChildFilter()
                ParentChildFilter = topAlg.ParentChildFilter
                ParentChildFilter.PDGParent = [1000021]
                ParentChildFilter.PDGChild = [1000023]
                StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]
                StreamEVGEN.RequireAlgs +=  [ "ParentChildFilter" ]

        if 'SS' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
                squarks = [1000001, -1000001, 2000001, -2000001, 1000002, -1000002, 2000002, -2000002, 1000003, -1000003, 2000003, -2000003, 1000004, -1000004, 2000004, -2000004]
                topAlg += ParticleFilter()
                ParticleFilter = topAlg.ParticleFilter
                ParticleFilter.Etacut= 10.
                ParticleFilter.Energycut = 100000000.0
                ParticleFilter.PDG = 1000024
                ParticleFilter.StatusReq = -1
                ParticleFilter.Ptcut = 0.
                from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
                topAlg += ParentChildFilter()
                ParentChildFilter = topAlg.ParentChildFilter
                ParentChildFilter.PDGParent = squarks
                ParentChildFilter.PDGChild = [1000023]
                StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]
                StreamEVGEN.RequireAlgs +=  [ "ParentChildFilter" ]


# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph", "Pythia"]
if hasattr(runArgs,'decaytype') and runArgs.decaytype is not None:
    if hasattr(runArgs,'gentype') and runArgs.gentype is not None:
        if 'GG' in runArgs.gentype:
            if ('onestep' in runArgs.decaytype and 'CN' in runArgs.decaytype):
                evgenConfig.efficiency = 0.5

if not hasattr(runArgs,'inputGeneratorFile'):
    evgenLog.error('Something wasnt write in file name transfer from the fragment.')
    runArgs.inputGeneratorFile='madgraph.*._events.tar.gz'
evgenConfig.description = 'SUSY Simplified Model generation with MadGraph/Pythia6 in MC12'
evgenConfig.keywords = ["SUSY"]
evgenConfig.inputfilecheck = runArgs.inputGeneratorFile.split('._0')[0]


if not hasattr(runArgs,'qcut'):
    evgenLog.error(' qcut was not sucessfully transferred from the fragment.')
    runArgs.qcut=50

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(runArgs.qcut)

phojf.write(phojinp)
phojf.close()

if useMETfilter:
    include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
    METPartTruth.TruthCollectionName="GEN_EVENT"
    topAlg.METAlg+=METPartTruth
    
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()
    
    METFilter = topAlg.METFilter
    METFilter.MissingEtCut = METfilterCut*GeV
    evgenLog.info("Using a MET filter lower cut at " + str(METFilter.MissingEtCut) + " MeV.")
    try:
        METFilter.MissingEtUpperCut = runArgs.METfilterUpperCut*GeV
        evgenLog.info("Using a MET filter upper cut at " + str(METFilter.MissingEtUpperCut) + " MeV.")
    except:
        evgenLog.info("Not using a MET filter upper cut.")
    METFilter.MissingEtCalcOption = 1
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "METFilter" ]

if runArgs.useHiggsFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiHiggsFilter
    topAlg += MultiHiggsFilter()
    
    MultiHiggsFilter = topAlg.MultiHiggsFilter
    MultiHiggsFilter.Exclusive = runArgs.ExclusiveNHiggs
    MultiHiggsFilter.NHiggs = runArgs.NHiggs
    MultiHiggsFilter.UseStatus = True
    MultiHiggsFilter.Status = 3
    if MultiHiggsFilter.Exclusive:
        evgenLog.info("Using a Higgs filter requiring exactly " + str(MultiHiggsFilter.NHiggs) + " Higgs(es).")
    else:
        evgenLog.info("Using a Higgs filter requiring at least " + str(MultiHiggsFilter.NHiggs) + " Higgs(es).")
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "MultiHiggsFilter" ]

if useMultiElecMuTauFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
    topAlg += MultiElecMuTauFilter()

    MultiElecMuTauFilter.MinPt = MultiElecMuTauFilterPtCut*GeV
    MultiElecMuTauFilter.MinVisPtHadTau = MultiElecMuTauFilterTauPtCut*GeV
    MultiElecMuTauFilter.MaxEta = MultiElecMuTauFilterEtaCut
    MultiElecMuTauFilter.NLeptons = MultiElecMuTauFilterLepCut
    MultiElecMuTauFilter.IncludeHadTaus = MultiElecMuTauFilterIncludeTau

    StreamEVGEN.RequireAlgs +=  [ "MultiElecMuTauFilter" ]

if useMultiLeptonFilter:
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter()
    MultiLeptonFilter.MinPt = MultiLeptonFilterPtCut*GeV
    MultiLeptonFilter.MaxEta = MultiLeptonFilterEtaCut
    MultiLeptonFilter.NLeptons = MultiLeptonFilterLepCut

    StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]

if runArgs.useParticleFilter:
    from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
    topAlg += ParticleFilter()
    
    ParticleFilter = topAlg.ParticleFilter
    ParticleFilter.Exclusive = runArgs.ExclusiveNParticles
    ParticleFilter.MinParts = runArgs.MinParts
    ParticleFilter.StatusReq = 3
    ParticleFilter.Ptcut = 0.0
    ParticleFilter.Etacut= 10.
    ParticleFilter.Energycut = 100000000.0
    ParticleFilter.PDG = runArgs.RequiredPDGID
    
    if ParticleFilter.Exclusive:
        evgenLog.info("Using a Particle filter requiring exactly " + str(ParticleFilter.MinParts) + " particle(s) with PDGID " + str(ParticleFilter.PDG) + ".")
    else:
        evgenLog.info("Using a Particle filter requiring at least " + str(ParticleFilter.MinParts) + " particle(s) with PDGID "+ str(ParticleFilter.PDG) + ".")
    
    #---------------------------------------------------------------
    # POOL / Root output
    #---------------------------------------------------------------
    
    StreamEVGEN.RequireAlgs +=  [ "ParticleFilter" ]

# Special hook for sbottom -> b+neut2/neut1 mixed decays
if runArgs.runNumber in range(179062,179222+1):
    from TruthExamples.TruthExamplesConf import TestHepMC
    topAlg += TestHepMC(CmEnergy=runArgs.ecmEnergy*Units.GeV,MaxTransVtxDisp=300.,MaxTransVtxDispLoose=300.)
    StreamEVGEN.RequireAlgs += ["TestHepMC"]

