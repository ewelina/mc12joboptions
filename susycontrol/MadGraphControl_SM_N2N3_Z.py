# N2N3 Z grid settings
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

sumList_SM_N2N3_Z = [50, 100, 112.5, 125, 137.5, 150, 162.5, 175, 187.5, 200, 250, 300, 350, 400, 450, 500 ]
points_SM_N2N3_Z = []
points_SM_N2N3_Z_2lep = []
for massN2 in sumList_SM_N2N3_Z:

    sumList_SM_N1_Slep = [0, 25, 37.5, 50, 62.5, 75, 87.5, 100, 112.5, 125, 137.5, 150, 162.5, 175, 187.5, 200, 250, 300, 350, 400, 450 ]
    for item in sumList_SM_N1_Slep:
        if item < massN2:
            if (massN2==50 and item!=0):
                continue
            elif (massN2==100 and (item==37.5 or item==62.5)):
                continue
            elif (massN2==112.5 and item!=50):
                continue
            elif (massN2==125 and (item==37.5 or item==62.5 or item==87.5)):
                continue
            elif (massN2==137.5 and item!=50):
                continue
            elif (massN2==150 and (item==87.5 or item==112.5)):
                continue
            elif (massN2==162.5 and item!=50):
                continue
            elif (massN2>150 and (item==37.5 or item==62.5 or item==87.5 or item==112.5 or item==137.5)):
                continue
            elif (massN2>175 and item==162.5):
                continue
            elif (massN2==187.5 and item!=50):
                continue
            elif (massN2>200 and (item==25 or item==75 or item==125 or item==175 or item==187.5)):
                 continue
            elif (massN2>450 and (item==50 or item==150 or item==250 or item==350)):
                continue
            else:
                massN1 = item
                point = [massN2, massN1]
                points_SM_N2N3_Z += [ point ]
                if ((massN2==100 or massN2==125 or massN2==150 or massN2==175 or massN2==200) and (item==0)):
                    massN1 = item
                    point= [massN2, massN1]
                    points_SM_N2N3_Z_2lep += [ point ]

# N2N3 grid points
therun = runArgs.runNumber- 204387
if therun>=0 and therun<len(points_SM_N2N3_Z_2lep):
    evgenLog.info('Registered generation of grid, direct N2 N3 Z point '+str(therun))
    masses['1000025'] = points_SM_N2N3_Z_2lep[therun][0]
    masses['1000023'] = -points_SM_N2N3_Z_2lep[therun][0]
    masses['1000022'] = points_SM_N2N3_Z_2lep[therun][1]
    log.info('Setting up grid with masses ' + str(points_SM_N2N3_Z_2lep[therun][0]) + ' - ' + str(points_SM_N2N3_Z_2lep[therun][1]))
    stringy = str(int(points_SM_N2N3_Z_2lep[therun][0]))+'_'+str(int(points_SM_N2N3_Z_2lep[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=2
    runArgs.MultiElecMuTauFilterPtCut=5
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=0
    gentype = 'N2N3'
    decaytype = 'Zall'
    njets = 1
    use_decays = False
therun = runArgs.runNumber-186200
if therun>=0 and therun<len(points_SM_N2N3_Z):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 Z point '+str(therun))
    masses['1000025'] = points_SM_N2N3_Z[therun][0]
    masses['1000023'] = -points_SM_N2N3_Z[therun][0]
    masses['1000022'] = points_SM_N2N3_Z[therun][1]
    log.info('Setting up grid with masses ' + str(points_SM_N2N3_Z[therun][0]) + ' - ' + str(points_SM_N2N3_Z[therun][1]))
    stringy = str(int(points_SM_N2N3_Z[therun][0]))+'_'+str(int(points_SM_N2N3_Z[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=3
    runArgs.MultiElecMuTauFilterPtCut=5
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=1
    gentype = 'N2N3'
    decaytype = 'Z'
    njets = 1
    use_decays = False

# Increase the number of MadGraph events and decrease the number of athena events if the filter is on
if use_MultiElecMuTauFilter:
    evt_multiplier = 20.0
    nevts=100*evt_multiplier

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

