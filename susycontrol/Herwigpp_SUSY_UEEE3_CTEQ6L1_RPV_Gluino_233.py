## SUSY Herwig++ jobOptions for Gluino/Bino simplified model with lambda_233 R-parity violation.
## Author: Mike Flowerdew (flowerde@cern.ch)

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.RPVSimplifiedModel')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# Set up Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
include ( 'MC12JobOptions/SUSY_RPV_Gluino_233_mc12points.py' )
try:
    mGluino, mBino = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)
slha_file = 'susy_RPV_%s_%s_Gluino_233_simplified.slha' % (mGluino, mBino)

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Gluino/Bino simplified model with lambda_233 R-parity violation. with m_Gluino = %s, m_Bino = %s' % (mGluino,mBino)
evgenConfig.keywords = ['SUSY', 'RPV', 'Lambda233', 'Gluino']
evgenConfig.contact  = ['flowerde@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
