# Two-step gluino decay to LSP, x1=1/2, x2=1/4, WWZZ (Multi-lepton)
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# new two step wwzz
points2b2 = []
sumList   = [250,310,370,430,490,550,610,770,930,1090,1250,1410,1570,1730,1890,2050,2210,2370,2530,2690,2850]
toremove  = [210,240,270,300,330,410,490,570,650,730,810,890,970,1050,1130,1197.5]
counter=0
counter2=0
for sum in ([0,50,100,150,200]+sumList):
    diffI = xrange(1,int(sum/75)+1)
    diffL = [25,50]
    for item in diffI: diffL += [ item*80 ]
    for diff in diffL:
        counter += 1
        msq = (sum+diff)/2.
        #remove regions already excluded or unreachable
        if msq>1500 or msq-diff<0 or msq-diff>800 or msq<250: continue
        if msq in toremove: continue
        if msq<=850 and msq-diff<250: continue
        if msq<910  and msq-diff<100: continue
        if msq-diff>600 and counter%2==0: continue
        if msq-diff>600 and msq>1300: continue
        point = [msq,msq-diff]
        points2b2 += [ point ]

# points for DC14
points2b2_dc14 = [
  (1100.,100.), (1300.,100.), (1500.,100.), (1700.,100.), (1900.,100.), (2100.,100.), (2300.,100.), 
  (1000.,400.), (1000.,500.), (1000.,600.), (1000.,700.), (1000.,800.), (1000.,900.)
]
therun = runArgs.runNumber - 204952

if 0 <= therun < len(points2b2_dc14):
    use_Photos=False
    points2b2 = points2b2_dc14
    moreinfo = ' for DC14'
else:
    therun = runArgs.runNumber - 173951
    moreinfo = ''

if therun>=0 and therun<len(points2b2):
    evgenLog.info('Registered generation of two-step GG point '+str(therun)+moreinfo)
    masses['1000021'] = points2b2[therun][0]
    masses['1000022'] = points2b2[therun][1]
    masses['1000024'] = 0.5*(points2b2[therun][0]+points2b2[therun][1])
    masses['1000023'] = 0.5*(masses['1000024']+points2b2[therun][1])
    stringy = str(int(points2b2[therun][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(points2b2[therun][1]))
    gentype='GG'
    decaytype='twostep'
    njets=1
    use_decays=False
    use_Tauola=False

# Systematics
therun = runArgs.runNumber-152882
if therun>=0 and therun<5*5:
    evgenLog.info('Registered generation of grid eleven systematics, two-step GG point '+str(therun))
    syspoints = [ [235,75], [205,45], [335,95], [325,165], [300,250] ]
    masses['1000021'] = syspoints[therun%5][0]
    masses['1000022'] = syspoints[therun%5][1]
    masses['1000024'] = 0.5*(syspoints[therun%5][0]+syspoints[therun%5][1])
    masses['1000023'] = 0.5*(masses['1000024']+syspoints[therun%5][1])
    syst_mod='qup'
    if therun>=5: syst_mod='qdown'
    if therun>=10: syst_mod='radhi'
    if therun>=15: syst_mod='radlo'
    if therun>=20: syst_mod='rad0'
    stringy = str(int(syspoints[therun%5][0]))+'_'+str(int(masses['1000024']))+'_'+str(int(syspoints[therun%5][1]))+'_'+syst_mod
    gentype='GG'
    decaytype='twostep'
    njets=1
    use_decays=False

if runArgs.runNumber in [152887,152888,152889,152890, \
                 152891,152893,]:
    evt_multiplier = 4.0

evgenConfig.contact  = [ "genest@lpsc.in2p3.fr" ]
evgenConfig.keywords += ['gluino','two_step']
evgenConfig.description = 'gluino production two step without slepton in simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

