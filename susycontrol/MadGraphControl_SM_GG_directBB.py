# Direct gluino decays to bbar
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

l_SUSY_Gbb_madgraph_mc12points = []
for heavy in xrange(200,1601,100):
    for light in xrange(0,1100,100):
        if light==0:                                             l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , 1 ] ]
        elif light==heavy+100 and heavy<1100 and heavy>=200:     l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , heavy-25 ] ]
        elif light==heavy     and heavy<1100 and heavy>200:      l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , heavy-75 ] ]
        elif heavy==light+100 and heavy<1100 and heavy>200:      l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , heavy-125 ] ]
        elif heavy>1000:                                         l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , light ] ]
        elif heavy<1100 and light <= 700 and light <= heavy-200: l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , light ] ]
l_SUSY_Gbb_madgraph_mc12points += [ [200,100],[1000,700],[1000,800],[1000,975],[1000,1],[1000,800], ]

therun = runArgs.runNumber- 175925
if therun>=0 and therun<140:
    evgenLog.info('Registered generation of grid twelve, direct (to bbbar) GG point '+str(therun))
    masses['1000021'] = l_SUSY_Gbb_madgraph_mc12points[therun][0]
    masses['1000022'] = l_SUSY_Gbb_madgraph_mc12points[therun][1]
    stringy = str(int(l_SUSY_Gbb_madgraph_mc12points[therun][0]))+'_'+str(int(l_SUSY_Gbb_madgraph_mc12points[therun][1]))
    if therun in [0,1,5,6,12,19,27,36,46,134,137,138]:
        stringy += '_noMETfilter'
        use_METfilter = False
    else:
        stringy += '_METfilter50GeV'
        use_METfilter=True
        runArgs.METfilterCut = 50.0
    gentype='GG'
    decaytype='directBB'
    njets=1
    use_decays=False
    use_Tauola=False


for heavy in xrange(1000,2501,300):
    for light in xrange(0,1100,200):
        if heavy!=1300 and light!=200: continue
        elif light==0:                 l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , 1 ] ]
        else:                          l_SUSY_Gbb_madgraph_mc12points += [ [ heavy , light ] ]


therun = runArgs.runNumber - 204898 + 140
if therun>=140 and therun<151:
    evgenLog.info('Registered generation of grid twelve, direct (to bbbar) GG point '+str(therun))
    masses['1000021'] = l_SUSY_Gbb_madgraph_mc12points[therun][0]
    masses['1000022'] = l_SUSY_Gbb_madgraph_mc12points[therun][1]
    stringy = str(int(l_SUSY_Gbb_madgraph_mc12points[therun][0]))+'_'+str(int(l_SUSY_Gbb_madgraph_mc12points[therun][1]))
    gentype='GG'
    decaytype='directBB'
    njets=1
    use_decays=False
    use_Tauola=False

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

