
evgenConfig.description = 'RPV gluino generation - simplified model q->qqchi->5q '
evgenConfig.keywords = ['SUSY','RPV']
evgenConfig.contact  = ['niteshehep@gmail.com']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('squarks')
slha_file = 'susy_RPVMSSM_SM_squarktochi_template.slha'
cmds = buildHerwigppCommands(sparticle_list, slha_file,'TwoParticleInclusive', earlyCopy=True)

# define spectrum file name
include( 'MC12JobOptions/SUSY_RPV_UDD_SquarkToChi_mc12points.py' )

try:
    Msq, Mchi = pointdict[runArgs.runNumber]
except:
    raise RuntimeError('DSID %s not found in grid point dictionary. Aborting!' % runArgs.runNumber)


# modify parameters in slha file
newcard = open('spectrum.slha.tmp','w')
for line in open(slha_file,'r'):
    if '1000001     5.0E+03' in line:
        newcard.write('   1000001     %s  \n'%(Msq))
    elif '2000001     5.0E+03' in line:
        newcard.write('   2000001     %s  \n'%(Msq))
    elif '1000002     5.0E+03' in line:
        newcard.write('   1000002     %s  \n'%(Msq))
    elif '2000002     5.0E+03' in line:
        newcard.write('   2000002     %s  \n'%(Msq))
    elif '1000003     5.0E+03' in line:
        newcard.write('   1000003     %s  \n'%(Msq))
    elif '2000003     5.0E+03' in line:
        newcard.write('   2000003     %s  \n'%(Msq))
    elif '1000004     5.0E+03' in line:
        newcard.write('   1000004     %s  \n'%(Msq))
    elif '2000004     5.0E+03' in line:
        newcard.write('   2000004     %s  \n'%(Msq))
    elif '   1000022     11111' in line:
        newcard.write('   1000022     %i \n'%(Mchi) )
    else:
        newcard.write(line)

newcard.close()

import os
os.rename('spectrum.slha.tmp',slha_file)

## Introduce the decay length filter
from GeneratorFilters.GeneratorFiltersConf import DecayLengthFilter
topAlg += DecayLengthFilter()

DecayLengthFilter = topAlg.DecayLengthFilter
DecayLengthFilter.McEventCollection = "GEN_EVENT"
# specify desired decay region:
DecayLengthFilter.Rmin = 0
DecayLengthFilter.Rmax = 300
DecayLengthFilter.Zmin = 0
DecayLengthFilter.Zmax = 300
DecayLengthFilter.particle_id = 1000022;

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs += [ "DecayLengthFilter" ]

from TruthExamples.TruthExamplesConf import TestHepMC
topAlg+=TestHepMC()
topAlg.TestHepMC.MaxVtxDisp=1000000.

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

del cmds
