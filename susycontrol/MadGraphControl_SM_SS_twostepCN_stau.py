# Squark pair production, two-step decay through chargino1/neutralino2 then stau/tau sneutrino
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

Msquark = [745,905,1065,545,705,865,1025,1185,665,825,625,785,585,745,705,865,257,287,665,825,317,785,745,397,205,305,265,225,465,325,425,285,385,545,315,345,505,465,625,425,585,545]

Mlsp = [25,25,25,65,65,65,65,65,105,105,145,145,185,185,225,225,232,262,265,265,292,305,345,372,45,65,105,145,145,165,185,205,225,225,235,265,265,305,305,345,345,385]

therun = runArgs.runNumber-204830
if therun>=0 and therun<len(Msquark):
	evgenLog.info('Registered generation of squark 2-step grid with intermediate stau, point '+str(therun))
	masses['1000001'] = Msquark[therun]
	masses['1000002'] = Msquark[therun]
	masses['1000003'] = Msquark[therun]
	masses['1000004'] = Msquark[therun]
	masses['1000022'] = Mlsp[therun]
	masses['1000023'] = 0.5*(Msquark[therun]+Mlsp[therun])
	masses['1000024'] = 0.5*(Msquark[therun]+Mlsp[therun])
	masses['1000015'] = 0.25*(3.*Mlsp[therun]+Msquark[therun])
	masses['1000016'] = 0.25*(3.*Mlsp[therun]+Msquark[therun])
	gentype='SS'
	decaytype='twostepCN_stau'
	njets=1
	use_decays=False
	use_Tauola=False

evgenConfig.contact  = [ "martindl@cern.ch" ]
evgenConfig.keywords += ['squark','two_step','stau']
evgenConfig.description = 'simplified model of squark pair production with two-step decay via stau'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

