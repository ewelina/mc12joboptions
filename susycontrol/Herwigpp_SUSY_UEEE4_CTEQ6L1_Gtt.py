## SUSY Herwig++ jobOptions for simplified model Gtt

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Gtt')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py' )

# define spectrum file name
key_string = runArgs.jobConfig[0].split('CTEQ6L1_')[1].split('.')[0]
slha_file = 'susy_'+key_string+'.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
mgl = key_string.split('Gtt_G')[1].split('_T')[0]
mlsp = key_string.split('_L')[1]
evgenConfig.description = 'Gtt grid generation with m_gluino = %s, m_LSP = %s, m_stop = 5000 GeV' % (mgl,mlsp)
evgenConfig.keywords = ['SUSY','gluino']
evgenConfig.contact  = ['Jean-Francois.Arguin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds
