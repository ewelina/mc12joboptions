# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

masses = runArgs.jobConfig[0].split('.py')[0].split('_')[-2:]

# define spectrum file name
slha_file = 'susy_wino_bino_%s_%s.slha'%(masses[0],masses[1])

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

sparticles['neutralinos'] = ['~chi_20','~chi_30','~chi_40']
cmds = buildHerwigppCommands(['charginos','neutralinos'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM wino-bino grid generation with mwino=%s mbino=%s'%(masses[0],masses[1])
evgenConfig.keywords = ['SUSY', 'GGM', 'wino', 'bino']
evgenConfig.contact = [ 'susan.fowler@cern.ch']

#ParticleFilters

# Direct photon filter - filtering here for two 65 GeV photons
if runArgs.runNumber<202745:
    include("MC12JobOptions/DirectPhotonFilter.py")
    topAlg.DirectPhotonFilter.Ptcut = 65000.
    topAlg.DirectPhotonFilter.Etacut =  2.7
    topAlg.DirectPhotonFilter.NPhotons = 2

# Bino Filter - filtering for events with two binos only 

from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
topAlg += ParticleFilter()
ParticleFilter = topAlg.ParticleFilter
ParticleFilter.Ptcut = 0.0
ParticleFilter.Etacut = 10.0
ParticleFilter.PDG = 1000022
ParticleFilter.MinParts = 2
ParticleFilter.StatusReq = 11
StreamEVGEN.RequireAlgs += [ "ParticleFilter" ]

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
