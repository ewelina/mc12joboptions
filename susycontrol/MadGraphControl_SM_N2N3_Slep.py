# N2N3 slepton grid settings
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# N2N3_slepton 2012 (re-parameterised)
sumList_SM_N2N3_Slep = [50, 100, 150, 200, 250, 300, 400, 500, 600, 700, 800 ]
points_SM_N2N3_Slep = []
points_SM_N2N3_ext = []
points_SM_N2N3_extNew = []
points_SM_N2N3_LM = []
for massN2 in sumList_SM_N2N3_Slep:
    sumList_SM_N1_Slep = [0, 50, 80, 100, 130, 150, 180, 200, 230, 250, 280, 300, 350, 380, 400, 450, 500, 550, 600, 650, 700, 750 ]
    for item in sumList_SM_N1_Slep:
        if item < massN2:
            if (massN2!=100 and (item==80)):
                continue
            elif (massN2!=150 and (item==130)):
                continue
            elif (massN2!=200 and (item==180)):
                continue
            elif (massN2!=250 and (item==230)):
                continue
            elif (massN2!=300 and (item==280)):
                continue
            elif (massN2!=400 and (item==380)):
                continue
            elif (massN2>250 and (item==50 or item==150)):
                continue
            elif (massN2>300 and (item==250)):
                continue
            elif (massN2>400 and (item==350)):
                continue
            elif (massN2>500 and (item==450)):
                continue
            elif (massN2>600 and (item==550)):
                continue
            elif (massN2>700 and (item==650)):
                continue
            else:
                massN1 = item
                masseR = (massN2+massN1)/2
                point = [massN2, massN1, masseR]
                if(massN1==80 or massN1==130 or massN1==180 or massN1==230 or massN1==280 or massN1==380):
                    points_SM_N2N3_LM += [ point ]
                elif(massN2 < 600):
                    points_SM_N2N3_Slep += [ point ]
                elif(massN2 > 500 and massN2 < 800):
                    points_SM_N2N3_ext += [ point ]
                else:
                    points_SM_N2N3_extNew += [ point ]

#negative mass points with C1N2 layout
points_SM_N2N3_New = [[110,90,100],[112.5,12.5,62.5],[112.5,47.5,80],[117.5,82.5,100],[185,165,175],
                      [192.5,157.5,175],[207.5,142.5,175],[250,0,125],[260,240,250],[267.5,232.5,250],
                      [282.5,217.5,250],[350,0,175],[375,125,250],[392.5,357.5,375],[407.5,342.5,375],
                      [450,300,375],[455,420,437.5],[470,405,437.5],[500,0,250],[512.5,362.5,437.5],
                      [532.5,467.5,500],[550,200,375],[575,425,500],[612.5,262.5,437.5],[625,0,312.5],
                      [625,125,375],[675,325,500],[675,575,625],[687.5,62.5,375],[687.5,187.5,437.5],
                      [700,550,625],[800,450,625],[812.5,62.5,437.5],[812.5,187.5,500] ]

#extension to negative mass points with varying slepton mass contribution - 5%,25%,50%(current),75%,95%
points_SM_N2N3_New_SlepVar = [[100,0,50],[150,0,75],[700,0,350],
                              [100,0,5],[150,0,7.5],[250,0,12.5],[350,0,17.5],[500,0,25],[625,0,31.25],[700,0,35],
                              [100,0,25],[150,0,37.5],[250,0,62.5],[350,0,87.5],[500,0,125],[625,0,156.25],[700,0,175],
                              [100,0,75],[150,0,112.5],[250,0,187.5],[350,0,262.5],[500,0,375],[625,0,468.75],[700,0,525],
                              [100,0,95],[150,0,142.5],[250,0,237.5],[350,0,332.5],[500,0,475],[625,0,593.75],[700,0,665] ]

                    
# Original grid points
therun = runArgs.runNumber-178712
if therun>=0 and therun<len(points_SM_N2N3_Slep):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_Slep[therun][0]
    masses['1000023'] = -points_SM_N2N3_Slep[therun][0]
    masses['1000022'] = points_SM_N2N3_Slep[therun][1]
    masses['2000011'] = points_SM_N2N3_Slep[therun][2]
    masses['2000013'] = points_SM_N2N3_Slep[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_Slep[therun][0]) + ' - ' + str(points_SM_N2N3_Slep[therun][1]) + ' - ' + str(points_SM_N2N3_Slep[therun][2]))
    stringy = str(int(points_SM_N2N3_Slep[therun][0]))+'_'+str(int(points_SM_N2N3_Slep[therun][1]))
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1 
    use_decays = False
    evt_multiplier = 5.0

# Extension
therun = runArgs.runNumber-179976
if therun>=0 and therun<len(points_SM_N2N3_ext):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_ext[therun][0]
    masses['1000023'] = -points_SM_N2N3_ext[therun][0]
    masses['1000022'] = points_SM_N2N3_ext[therun][1]
    masses['2000011'] = points_SM_N2N3_ext[therun][2]
    masses['2000013'] = points_SM_N2N3_ext[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_ext[therun][0]) + ' - ' + str(points_SM_N2N3_ext[therun][1]) + ' - ' + str(points_SM_N2N3_ext[therun][2]))
    stringy = str(int(points_SM_N2N3_ext[therun][0]))+'_'+str(int(points_SM_N2N3_ext[therun][1]))
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1
    use_decays = False
    evt_multiplier = 5.0

#Additional Extension
therun = runArgs.runNumber-202239
if therun>=0 and therun<len(points_SM_N2N3_ext):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_extNew[therun][0]
    masses['1000023'] = -points_SM_N2N3_extNew[therun][0]
    masses['1000022'] = points_SM_N2N3_extNew[therun][1]
    masses['2000011'] = points_SM_N2N3_extNew[therun][2]
    masses['2000013'] = points_SM_N2N3_extNew[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_extNew[therun][0]) + ' - ' + str(points_SM_N2N3_extNew[therun][1]) + ' - ' + str(points_SM_N2N3_extNew[therun][2]))
    stringy = str(int(points_SM_N2N3_extNew[therun][0]))+'_'+str(int(points_SM_N2N3_extNew[therun][1]))
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1
    use_decays = False
    evt_multiplier = 5.0

# Extension lowmass
therun = runArgs.runNumber-202233
if therun>=0 and therun<len(points_SM_N2N3_LM):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_LM[therun][0]
    masses['1000023'] = -points_SM_N2N3_LM[therun][0]
    masses['1000022'] = points_SM_N2N3_LM[therun][1]
    masses['2000011'] = points_SM_N2N3_LM[therun][2]
    masses['2000013'] = points_SM_N2N3_LM[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_LM[therun][0]) + ' - ' + str(points_SM_N2N3_LM[therun][1]) + ' - ' + str(points_SM_N2N3_LM[therun][2]))
    stringy = str(int(points_SM_N2N3_LM[therun][0]))+'_'+str(int(points_SM_N2N3_LM[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=4
    runArgs.MultiElecMuTauFilterPtCut=7
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=0
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1
    use_decays = False
    evt_multiplier = 10.0

# Correct negative N2 mass C1N2 like points
therun = runArgs.runNumber-156945
if therun>=0 and therun<len(points_SM_N2N3_New):
    evgenLog.info('Registered generation of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_New[therun][0]
    masses['1000023'] = -points_SM_N2N3_New[therun][0]
    masses['1000022'] = points_SM_N2N3_New[therun][1]
    masses['2000011'] = points_SM_N2N3_New[therun][2]
    masses['2000013'] = points_SM_N2N3_New[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_New[therun][0]) + ' - ' + str(points_SM_N2N3_New[therun][1]) + ' - ' + str(points_SM_N2N3_New[therun][2]))
    stringy = str(int(points_SM_N2N3_New[therun][0]))+'_'+str(int(points_SM_N2N3_New[therun][1]))
    use_MultiElecMuTauFilter=True
    runArgs.MultiElecMuTauFilterLepCut=3
    runArgs.MultiElecMuTauFilterPtCut=7
    runArgs.MultiElecMuTauFilterEtaCut=10
    runArgs.MultiElecMuTauFilterTauPtCut=15
    runArgs.MultiElecMuTauFilterIncludeTau=0
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1
    use_decays = False
    evt_multiplier = 5.0
    
#extension to negative mass points to do scan of slepton mass
therun = runArgs.runNumber-202958
if therun>=0 and therun<len(points_SM_N2N3_New_SlepVar):
    evgenLog.info('Registered extension of grid 26, direct N2 N3 point '+str(therun))
    masses['1000025'] = points_SM_N2N3_New_SlepVar[therun][0]
    masses['1000023'] = -points_SM_N2N3_New_SlepVar[therun][0]
    masses['1000022'] = points_SM_N2N3_New_SlepVar[therun][1]
    masses['2000011'] = points_SM_N2N3_New_SlepVar[therun][2]
    masses['2000013'] = points_SM_N2N3_New_SlepVar[therun][2]
    evgenLog.info('Setting up grid with masses ' + str(points_SM_N2N3_New_SlepVar[therun][0]) + ' - ' + str(points_SM_N2N3_New_SlepVar[therun][1]) + ' - ' + str(points_SM_N2N3_New_SlepVar[therun][2]))
    stringy = str(int(points_SM_N2N3_New_SlepVar[therun][0]))+'_'+str(int(points_SM_N2N3_New_SlepVar[therun][1]))
    gentype = 'N2N3'
    decaytype = 'Slep'
    njets = 1 
    use_decays = False
    evt_multiplier = 5.0

nevts=5000*evt_multiplier

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

