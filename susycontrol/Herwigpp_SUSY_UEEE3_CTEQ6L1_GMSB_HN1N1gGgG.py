# Define metadata
evgenConfig.keywords    = ['SUSY','GMSB']
evgenConfig.contact     = ['mike.hance@cern.ch']

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SMModeADG')

# Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_GMSB_HN1N1gGgG_template.slha'

# figure out production mode
prodMode=""
if runArgs.runNumber >= 183740 and runArgs.runNumber < 183750:
    prodMode="MEHiggs"
elif runArgs.runNumber >= 183750 and runArgs.runNumber < 183760:
    prodMode="MEPP2HiggsVBF"
elif runArgs.runNumber >= 183760 and runArgs.runNumber < 183770:
    prodMode="MEPP2WH"
elif runArgs.runNumber >= 183770 and runArgs.runNumber < 183780:
    prodMode="MEPP2ZH"
elif runArgs.runNumber >= 183780 and runArgs.runNumber < 183790:
    prodMode="MEPP2ttbarH"
else:
    raise RuntimeError('Invalid run number (%d) for this grid.' % runArgs.runNumber)

# Build the Herwig++ commands
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands([], slha_file, earlyCopy=True)

cmds += """
set /Herwig/Particles/h0:NominalMass 126*GeV

insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H0
insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/A0
insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H+
insert /Herwig/NewPhysics/HPConstructor:Excluded 0 /Herwig/Particles/H-

insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]  /Herwig/MatrixElements/%s

set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:ShapeScheme  FixedBreitWigner

insert HPConstructor:Outgoing 0 /Herwig/Particles/h0

insert NewModel:DecayParticles 0 /Herwig/Particles/h0
insert NewModel:DecayParticles 1 /Herwig/Particles/~chi_10
insert NewModel:DecayParticles 2 /Herwig/Particles/~gravitino
insert NewModel:DecayParticles 3 /Herwig/Particles/gamma
""" % prodMode


# figure out the mass for the N1 in this sample
mchi=0
masspoint=(runArgs.runNumber % 10)
if masspoint==0:   mchi=1
elif masspoint==1: mchi=2
elif masspoint==2: mchi=5
elif masspoint==3: mchi=10
elif masspoint==4: mchi=20
elif masspoint==5: mchi=30
elif masspoint==6: mchi=40
elif masspoint==7: mchi=50
elif masspoint==8: mchi=60
elif masspoint==9: mchi=62
else:
    raise RuntimeError('Invalid run (%d) number for this grid.' % runArgs.runNumber)

# more metadata
evgenConfig.description = "GMSB H phi phi, m_phi="+str(mchi)+" GeV, with the MSTW tune"

# fix the SLHA file
if not os.access(slha_file,os.R_OK):
    raise RuntimeError('Could not get slha file')
else:
    oldcard = open(slha_file,'r')
    newcard = open('spectrum.slha','w')
    allslha=""
    for line in oldcard:
        if '1000022     5.00000000E+01' in line:
            newcard.write('   1000022     %i \n'%(mchi) )
            allslha+='   1000022     %i \n'%(mchi)
        else:
            newcard.write(line)
            allslha+=line
    newcard.write('DECAY   1000022   1.00000000E-06        # ~chi_10\n')
    allslha+='DECAY   1000022   1.00000000E-06        # ~chi_10\n'
    newcard.write('     1.00000000E+00    2   1000039        22\n')
    allslha+='     1.00000000E+00    2   1000039        22\n'
    oldcard.close()
    newcard.close()
    os.rename('spectrum.slha',slha_file)


# # Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')
log.info('*** Begin SLHA file ***')
log.info(allslha)
log.info('*** End SLHA file ***')

# # Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# # Clean up
del cmds

## Filter for two photons with minimal cuts...
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
if not hasattr(topAlg, "PhotonFilter"):
    topAlg += PhotonFilter()
    topAlg.PhotonFilter.Ptcut = 20000.
    topAlg.PhotonFilter.Etacut =  2.7
    topAlg.PhotonFilter.NPhotons = 2
    if "PhotonFilter" not in StreamEVGEN.RequireAlgs:
        StreamEVGEN.RequireAlgs +=  ["PhotonFilter"]
