from MadGraphControl.MadGraphUtils import *

if not hasattr(runArgs,'runNumber'):
  raise RunTimeError("No run number found.")

#--------------------------------------------------------------
# MG5 Proc card
#--------------------------------------------------------------
# SUSY GGM EWK:
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
    import model mssm-full
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define sq = ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul ur dl dr cl cr sl sr t1 t2 b1 b2
    define accessibleweak = n1 n2 n3 x1- x1+ 
    generate p p > accessibleweak accessibleweak $ sq go QED=99 QCD=99  @1
    add process p p > accessibleweak accessibleweak j $ sq go QED=99 QCD=99 @2
    output -f
    """)
fcard.close()

run_name=runArgs.jobConfig[0][runArgs.jobConfig[0].find("GGM"):runArgs.jobConfig[0].find("tanb")+8]

if(runArgs.jobConfig[0].find("tanb_5")>0):
  run_name=runArgs.jobConfig[0][runArgs.jobConfig[0].find("GGM"):runArgs.jobConfig[0].find("tanb")+7]

slha_file="susy.{0}.".format(runArgs.runNumber)+run_name+"slha"
log.info("Using paramCard %s" % slha_file)

xqcut = 100
runArgs.qcut = 100. # default MLM matching scale
ickkw=1

beamEnergy=4000

if hasattr(runArgs,'ecmEnergy'): 
  beamEnergy = runArgs.ecmEnergy / 2.
else:
  raise RunTimeError("No center of mass energy found.")

evt_multiplier = 4
if (run_name.find("tanb_20")>0 and (run_name.find("300_M2_150")>0 or run_name.find("150_M2_400")>0 or run_name.find("_500_")>0)):
  evt_multiplier = 15
if (run_name.find("tanb_20")>0 and (run_name.find("_600_")>0 or run_name.find("_700_")>0 or run_name.find("_800_")>0)):
  evt_multiplier = 20
if (run_name.find("tanb_5")>0 and (run_name.find("150_M2_400")>0 or run_name.find("150_M2_500")>0 or run_name.find("150_M2_600")>0 or run_name.find("150_M2_700")>0 or run_name.find("150_M2_800")>0)):            
  evt_multiplier = 8                                                                                                                                              
if (run_name.find("400_M2_150")>0):                                                                                                      
  evt_multiplier = 15                                                                                                                                             
if (run_name.find("tanb_5")>0 and (run_name.find("500_M2_150")>0 or run_name.find("600_M2_150")>0 or run_name.find("700_M2_150")>0 or run_name.find("800_M2_150")>0 or run_name.find("300_M2_150")>0)): 
  evt_multiplier = 20      

nevts = 5000
nevts = runArgs.maxEvents*evt_multiplier if runArgs.maxEvents > 0 else nevts*evt_multiplier

run_card_name = build_run_card(run_card_old='run_card.SM.dat',xqcut=runArgs.qcut,nevts=nevts,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy)

proccard = subprocess.Popen(['get_files','-data',slha_file])
proccard.wait()
if not os.access(slha_file,os.R_OK):
  log.fatal('Did not find slha file %s - giving up' % slha_file)

process_dir = new_process(card_loc='proc_card_mg5.dat')
generate(run_card_loc=run_card_name,param_card_loc=slha_file,mode=0,njobs=1,run_name=run_name,proc_dir=process_dir)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name=run_name,proc_dir=process_dir,outputDS=run_name+'._00001.events.tar.gz')

#--------------------------------------------------------------
# Pythia6
#--------------------------------------------------------------
runArgs.inputGeneratorFile=run_name+'._00001.events.tar.gz'

include ("MC12JobOptions/MadGraphControl_SimplifiedModelPythia6.py")
topAlg.Pythia.PythiaCommand += ["pymssm imss 11 1"] # Allow 2-body decays to gravitinos
evgenConfig.auxfiles += [slha_file]

#OR of two filters to select events which contain at least 2 leptons or 2 photons
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter

topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.MinPt = 5000.0
MultiElecMuTauFilter.MaxEta = 10.0
MultiElecMuTauFilter.NLeptons = 2

topAlg+= DirectPhotonFilter()

DirectPhotonFilter = topAlg.DirectPhotonFilter
DirectPhotonFilter.Ptmin = 10000.0
DirectPhotonFilter.Etacut = 10.0
DirectPhotonFilter.NPhotons = 2
DirectPhotonFilter.AllowSUSYDecay = 1
StreamEVGEN.AcceptAlgs+=["MultiElecMuTauFilter"]
StreamEVGEN.AcceptAlgs+=["DirectPhotonFilter"]

#--------------------------------------------------------------
# MC12 metadata
#--------------------------------------------------------------
evgenConfig.contact = ["zara.jane.grout@cern.ch"]
evgenConfig.description = "Higgsino/Wino GGM test model with weak production, tanb=%s GeV, M2 = %s GeV, mu = %s GeV"%(runArgs.jobConfig[0].split('_')[7],runArgs.jobConfig[0].split('_')[5], runArgs.jobConfig[0].split('_')[3])
evgenConfig.keywords = ["SUSY","GGM"]
evgenConfig.process = "weak"
