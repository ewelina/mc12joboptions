from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

fcard = open('proc_card_mg5.dat', 'w')

xqcut = 100
nevents = 60000
beamenergy = 4000
param_card_loc_in = ''

if runArgs.runNumber == 176519:
    ### SS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_600_0p8'
elif runArgs.runNumber == 176520:
    ### goS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > go r @1 QED=0 QCD=99
        add process p p > go r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_600_0p8'
elif runArgs.runNumber == 176521:
    ### gogo process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        generate p p > go go @1 QED=0 QCD=99
        add process p p > go go j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_600_0p8'
elif runArgs.runNumber == 176522:
    ### stsb process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r t1 t1~ t2 t2~ b1 b1~ b2 b2~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_600_0p8'
elif runArgs.runNumber == 176523:
    ### SS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_800_0p6'
elif runArgs.runNumber == 176524:
    ### goS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > go r @1 QED=0 QCD=99
        add process p p > go r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_800_0p6'
elif runArgs.runNumber == 176525:
    ### gogo process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        generate p p > go go @1 QED=0 QCD=99
        add process p p > go go j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_800_0p6'
elif runArgs.runNumber == 176526:
    ### stsb process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r t1 t1~ t2 t2~ b1 b1~ b2 b2~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_800_0p6'
elif runArgs.runNumber == 176527:
    ### SS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_1200_0p5'
elif runArgs.runNumber == 176528:
    ### goS process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r ul dl sl cl ur dr sr cr ul~ dl~ sl~ cl~ ur~ dr~ sr~ cr~
        generate p p > go r @1 QED=0 QCD=99
        add process p p > go r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_1200_0p5'
elif runArgs.runNumber == 176529:
    ### gogo process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        generate p p > go go @1 QED=0 QCD=99
        add process p p > go go j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_1200_0p5'
elif runArgs.runNumber == 176530:
    ### stsb process_card
    fcard.write("""
        import model mssm
        define p g u u~ d d~
        define j g u u~ d d~ s s~ c c~
        define r t1 t1~ t2 t2~ b1 b1~ b2 b2~
        generate p p > r r @1 QED=0 QCD=99
        add process p p > r r j @2 QED=0 QCD=99
        output -f
        """)
    param_card_loc_in = 'lecompte_martin_compsusy_1200_0p5'
else:
    raise RunTimeError("Unknown runNumber!")

fcard.close()

runcard = subprocess.Popen(['get_files', '-data', 'run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat', os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n' % (xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n ' % (nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n' % (runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('%i     = ebeam1  ! beam 1 energy in GeV \n' % (beamenergy))
        elif ' ebeam2 ' in line:
            newcard.write('%i     = ebeam2  ! beam 2 energy in GeV \n' % (beamenergy))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

process_dir = new_process()
generate(run_card_loc = 'run_card.dat', param_card_loc = 'susy_' + param_card_loc_in + '.slha', run_name = 'Test', proc_dir = process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber) + '.MadGraph_Pythia_' + param_card_loc_in

skip_events=0
if hasattr(runArgs, 'skipEvents'): skip_events = runArgs.skipEvents
arrange_output(run_name = 'Test', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
# topAlg.Pythia.Tune_Name="350"
topAlg.Pythia.PythiaCommand += [
                                "pyinit user madgraph",
                                "pystat 1 3 4 5",
                                "pyinit dumpr 1 5",       # printout of first 5 events
                                "pyinit pylistf 1",
                                "pydat1 mstj 1 1",        # string fragmentation on
                                "pypars mstp 61 1",       # initial-state radiation on
                                "pypars mstp 71 1",       # final-state radiation on
                                "pypars mstp 81 1",       # Multiple interactions on
                                "pypars mstp 128 1",      # fix junk output for documentary particles
                                "pydat1 mstu 21 1"        # prevent Pythia from exiting when it exceeds its errors limit
                                ]

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = "compressed SUSY model generation"
evgenConfig.keywords = ["SUSY", "Compressed"]
evgenConfig.minevents = 5000
evgenConfig.auxfiles += ['susy_' + param_card_loc_in + '.slha']
evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile = stringy + '._00001.events.tar.gz'

# -- Additional bit for ME/PS matching
phojf = open("./pythia_card.dat", "w")
phojinp = """
    ! exclusive or inclusive matching
    ! IEXCFILE=0
    ! showerkt=F
    ! qcut=0
    IMSS(21)=24
    IMSS(11)=1
    IMSS(22)=24
    """
phojf.write(phojinp)
phojf.close()
