# Stop pair-production with stop > top LSP
include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPreInclude.py' )

# Assigned run numbers the stop->top+LSP DS 164644,164645
therun = runArgs.runNumber-164644
points_TT_directTT = [
    [275.0,75.0],
    [500.0,150.0]]
if therun>=0 and therun<len(points_TT_directTT):
    masses['1000006'] = points_TT_directTT[therun][0]
    masses['1000022'] = points_TT_directTT[therun][1]
    stringy = str(int(points_TT_directTT[therun][0]))+'_'+str(int(points_TT_directTT[therun][1]))
    gentype='TT'
    decaytype='directTT'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to top+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directTT[therun]) + ', with ' + str(njets) + ' jets.')
    use_decays=False

# Assigned run numbers the stop->top+LSP
points_TT_directTT = []
if runArgs.runNumber in range(166987,166989+1):
    therun = runArgs.runNumber-166987
    points_TT_directTT = [[180.0,1.0],[350.0,170.0],[400.0,100.0]]
if therun>=0 and therun<len(points_TT_directTT):
    masses['1000006'] = points_TT_directTT[therun][0]
    masses['1000022'] = points_TT_directTT[therun][1]
    stringy = str(int(points_TT_directTT[therun][0]))+'_'+str(int(points_TT_directTT[therun][1]))
    gentype='TT'
    decaytype='directTT'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to top+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directTT[therun]) + ', with ' + str(njets) + ' jets.')
    use_decays=True

# ISR/FSR systematic sample for the stop->top+LSP
if runArgs.runNumber in range(177359,177374+1):
    therun = runArgs.runNumber-177359
    points_TT_directTT = [[180,1],[350,170]]
if therun>=0 and therun<(len(points_TT_directTT)*8):
    masses['1000006'] = points_TT_directTT[therun/8][0]
    masses['1000022'] = points_TT_directTT[therun/8][1]
    stringy = str(int(points_TT_directTT[therun/8][0]))+'_'+str(int(points_TT_directTT[therun/8][1]))
    gentype='TT'
    decaytype='directTT'
    njets=1
    evgenLog.info('Registered generation of stop pair production, stop to top+LSP; grid point '+str(therun)+' decoded into mass point ' + str(points_TT_directTT[therun/8]) + ', with ' + str(njets) + ' jets.')
    use_decays=True
    syst_mod=dict_index_syst[therun%8]

evt_multiplier = 5.0
if runArgs.runNumber in [177374]: evt_multiplier = 10.0

evgenConfig.contact  = [ "takashi.yamanaka@cern.ch" ]
evgenConfig.keywords += ['stop']
evgenConfig.description = 'stop direct pair production with simplified model'

include ( 'MC12JobOptions/MadGraphControl_SimplifiedModelPostInclude.py' )

