include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W -> tau v + at least 4 jets (up to 5)."
evgenConfig.keywords = ["W", "4jets"]
evgenConfig.contact  = ["frank.siegert@cern.ch", "christian.schillo@cern.ch"]
evgenConfig.minevents = 500

evgenConfig.process="""
(processes){
  Process 93 93 -> 15 -16 93 93 93 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;

  Process 93 93 -> -15 16 93 93 93 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 15 -16 1.7 E_CMS
  Mass -15 16 1.7 E_CMS
}(selector)

(run){
  MASSIVE[15]=1
  SOFT_SPIN_CORRELATIONS=1
}(run)
"""


