## MC@NLO+Herwig++ ttbar

# TODO: Leptonic, semi-leptonic, hadronic decay mode?
evgenConfig.description = "MC@NLO+Herwig++ ttbar production"
evgenConfig.keywords = ["top", "ttbar"]
evgenConfig.inputfilecheck = "McAtNlo.*tt"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_NLOME_LHEF_Common.py")
