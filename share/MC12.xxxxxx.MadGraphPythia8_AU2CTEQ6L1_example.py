evgenConfig.description = "MadGraph+Pythia8 production JO example with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["add", "some", "physics", "keywords", "here"]
evgenConfig.inputfilecheck = "MadGraph_CTEQ6L1.*MyProcess"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")
