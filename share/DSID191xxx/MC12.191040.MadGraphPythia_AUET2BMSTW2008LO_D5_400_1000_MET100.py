from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model EffDM -modelname

generate p p > chi chi~ D1=0 D2=0 D3=0 D4=0 D5=1 D6=0 D7=0 D8=0 D9=0 D10=0 D11=0 D12=0 D13=0 D14=0
add process p p > chi chi~ j D1=0 D2=0 D3=0 D4=0 D5=1 D6=0 D7=0 D8=0 D9=0 D10=0 D11=0 D12=0 D13=0 D14=0
add process p p > chi chi~ j j D1=0 D2=0 D3=0 D4=0 D5=1 D6=0 D7=0 D8=0 D9=0 D10=0 D11=0 D12=0 D13=0 D14=0

output -f
""")
fcard.close()

process_dir = new_process()

ebeam1 = 6500.
ebeam2 = 6500.
xqcut = 30
mDM = 400.
Mstar = 1000.

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','MadGraph_EffDM_runcard.dat'])
runcard.wait()
if not os.access('MadGraph_EffDM_runcard.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_EffDM_runcard.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' ebeam1 ' in line:
            newcard.write('%f   = ebeam1  ! beam 1 energy in GeV \n'%(ebeam1))
        elif ' ebeam2 ' in line:
            newcard.write('%f   = ebeam2  ! beam 2 energy in GeV \n'%(ebeam2))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(100000))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ickkw ' in line:
            newcard.write('   1     = ickkw ! 0 no matching, 1 MLM, 2 CKKW matching \n')
        elif ' xqcut ' in line:
            newcard.write('   %i     = xqcut ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' dokt ' in line:
            newcard.write('   F     = dokt ! apply ktdurham cuts \n')
        elif ' pdlabel ' in line: 
            newcard.write(" 'lhapdf'     = pdlabel     ! PDF set\n 21000 = lhaid\n")
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

paramcard = subprocess.Popen(['get_files','-data','MadGraph_EffDM_paramcard.dat'])
paramcard.wait()
if not os.access('MadGraph_EffDM_paramcard.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_EffDM_paramcard.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'mDM' in line:
            newcard.write('  1000022 %e # mDM\n'%(mDM))
        elif 'Mstar' in line:
            newcard.write('    1 %e # Mstar\n'%(Mstar))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_EffDM'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1",     # Changes listing of particles in log file
                               	 "pyinit pylistf 1",      # Changes listing of particles in log file
                                 "pyinit dumpr 1 2",
                                 "pypars mstp 51 21000",  # MSTW2008 LO
                                 "pypars mstp 53 21000",
                                 "pypars mstp 55 21000",
				]            


# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0 
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24
    """%(xqcut)

phojf.write(phojinp)
phojf.close()

evgenConfig.description = "Dirac fermion DM pair production via vector EFT contact interaction, mDM=400 GeV, Mstar=1000 GeV"
evgenConfig.contact = ["David Salek <david.salek@cern.ch>"]
evgenConfig.keywords = ["WIMP","monojet","contactInteraction"]
evgenConfig.generators += ["MadGraph"]
evgenConfig.inputfilecheck = stringy
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
runArgs.maxEvents = 5000
runArgs.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

include("MC12JobOptions/METFilter.py")
topAlg.METFilter.MissingEtCut = 100.*GeV
