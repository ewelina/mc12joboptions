include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "SVD_Mchi_50_Mmed_100"
evgenConfig.keywords = ["Z", "lepton"]
evgenConfig.inputfilecheck = 'SVD_Mchi_50_Mmed_100'
evgenConfig.contact = ["Shawn McKee <smckee@umich.edu>"]
