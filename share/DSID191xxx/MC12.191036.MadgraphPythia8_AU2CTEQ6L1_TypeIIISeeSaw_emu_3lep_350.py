
evgenConfig.inputfilecheck = 'typeIII'
evgenConfig.description = "Type III SeeSaw Model Mass: 350 GeV Vu=0.063 Ve=0.055"
evgenConfig.keywords = ["typeIIIss"] 
evgenConfig.contact = ["liv.antje.mari.wiik@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

include('MC12JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 4000
MultiLeptonFilter.Etacut      = 3
MultiLeptonFilter.NLeptons = 3
