evgenConfig.description = "Vector-like Tbj production, singlet, M=500 GeV, mixing=0.10, with Protos+Pythia6, tune Perugia2011C"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","VLQ"]
evgenConfig.contact  = ["takuya.tashiro@cern.ch"]
evgenConfig.inputfilecheck = "protos"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/LeptonFilter.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")
