## POWHEG+Pythia8 Wp->munu

include("MC12JobOptions/PowhegControl_preInclude.py")
postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Wp_munu"

evgenConfig.description = "POWHEG+Pythia8 Wp->munu production with lepton pT cut 150<pT<250GeV, inv.mass cut <120GeV and AZNLO_CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "W", "leptonic", "mu"] 
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>", "Nikos Tsirintanis <Nikolaos.Tsirintanis@cern.ch>"]
evgenConfig.generators = ["Powheg", "Pythia8"]

def powheg_override():
# needed for AZNLO tune
   PowhegConfig.ptsqmin = 4.0
   PowhegConfig.mass_high = 120.

evt_multiplier = 2000.

evgenConfig.minevents = 200.

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 150000.
topAlg.LeptonFilter.PtcutMax = 250000.
topAlg.LeptonFilter.Etacut = 10.0

