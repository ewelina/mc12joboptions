include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "ZZxxScalarD6_Mx_50"
evgenConfig.keywords = ["Z", "lepton"]
evgenConfig.inputfilecheck = 'ZZxxScalarD6_Mx_50'
evgenConfig.contact = ["Shawn McKee <smckee@umich.edu>"]
