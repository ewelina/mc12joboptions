LQmass = 2000

evgenConfig.description = "Pair produced scalar leptoquarks (M = "+str(LQmass)+" GeV) to di-muon jet channel, AU2 tune and CTEQ6L1 PDF"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = [ "ilias.panagoulias@cern.ch" ]


include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")


topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 0.01",                            
                            "42:0:products = 4 13",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]
