quirk_mass=500e3
quirk_charge=1
quirk_pdgid=10000100
quirk_stringforce=1e4
quirk_mergingscale=60

import os
if os.path.isfile('PDGTABLE.MeV'):
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f = open('PDGTABLE.MeV', 'a')
f.write("M%8d                          %.8E +0.0E+00 -0.0E+00 Quirk               +\n" % (quirk_pdgid, quirk_mass))
f.write("W%8d                          0.E+00         +0.0E+00 -0.0E+00 Quirk               +\n" % quirk_pdgid)
f.close()

evgenConfig.description = "pp -> quirk pair + jet(s)"
evgenConfig.contact = ["jblack@cern.ch"]
evgenConfig.keywords = ["quirks"]
evgenConfig.inputfilecheck = "Quirks"
evgenConfig.specialConfig="MASS=%e;CHARGE=%e;PDGID=%d;STRINGFORCE=%e;preInclude=SimulationJobOptions/preInclude.Quirks.py;" % (quirk_mass, quirk_charge, quirk_pdgid, quirk_stringforce)

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
topAlg.Pythia8.Commands += [
    "%d:new = qi+ qi- 1 3 0 %e" % (quirk_pdgid, quirk_mass/1000), # set spin to zero to work around merging bug
    "Merging:doKTMerging = on",
    "Merging:ktType = 1",
    "Merging:Dparameter = 0.4",
    "Merging:nJetMax = 2",
    "Merging:Process = pp>{qi+,%d}{qi-,%d}" % (quirk_pdgid, -quirk_pdgid),
    "Merging:TMS = %f" % quirk_mergingscale
]

del quirk_mass, quirk_charge, quirk_pdgid, quirk_stringforce
