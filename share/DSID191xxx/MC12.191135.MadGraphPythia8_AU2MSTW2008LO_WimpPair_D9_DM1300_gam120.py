from MadGraphControl.MadGraphUtils import *

mDM=1300.
Mstar = 1000.

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model EffDM -modelname

generate p p > chi chi~ a QED=1 D1=0 D2=0 D3=0 D4=0 D5=0 D6=0 D7=0 D8=0 D9=1 D10=0 D11=0 D12=0 D13=0 D14=0

output -f
""")
fcard.close()

if os.access('run_card.dat',os.R_OK):
	bashCommand = "rm run_card.dat"
	subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
runcard = subprocess.Popen(['get_files','-data','MadGraph_MonoPhot_runcard.dat'])
runcard.wait()
if not os.access('MadGraph_MonoPhot_runcard.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('MadGraph_MonoPhot_runcard.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' nevents ' in line:
            newcard.write('   %i      = nevents ! Number of unweighted events requested) \n'%(runArgs.maxEvents*2))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 total energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 total energy in GeV \n'%(runArgs.ecmEnergy/2))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


paramcard = subprocess.Popen(['get_files','-data','MadGraph_EffDM_paramcard.dat'])
paramcard.wait()
if not os.access('MadGraph_EffDM_paramcard.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_EffDM_paramcard.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'mDM' in line:
            newcard.write('  1000022 %e # mDM\n'%(mDM))
        elif 'Mstar' in line:
            newcard.write('    1 %e # Mstar\n'%(Mstar))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_EFT'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

bashCommand = "cp " + stringy + "._00001.events tmp_lhe"
subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)

if os.access('tmp_lhe2',os.R_OK):
	bashCommand = "rm tmp_lhe2"
	subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
lhefile = subprocess.Popen(['get_files','-data','tmp_lhe'])
lhefile.wait()
if not os.access('tmp_lhe',os.R_OK):
    print 'ERROR: Could not get temporary LHE file'
else:
    oldcard = open('tmp_lhe','r')
    newcard = open('tmp_lhe2','w')
    for line in oldcard:
        if 'LesHouchesEvents version' in line:
            newcard.write('<LesHouchesEvents version="1.0">\n')
        elif ' lhe_version ' in line:
            newcard.write('2.0      = lhe_version       ! Change the way clustering information pass to shower.\n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

bashCommand = "cp tmp_lhe2 " + stringy + "._00001.events"
subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
bashCommand = "tar -czf " + stringy + "._00001.events.tar.gz " + stringy + "._00001.events"
subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)

evgenConfig.description = "Wimp pair monophoton with EFT, D9, M_WIMP=1300 GeV, M_star=1000 GeV, pta>120 GeV"
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["WIMP","EFT"]
evgenConfig.contact = ["genest@lpsc.in2p3.fr"]

include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.inputfilecheck = stringy
#evgenConfig.minevents = 5000
#runArgs.maxEvents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
topAlg.Pythia8.Commands +=["1000022:all = chid chid~ 2 0 0 1300. 0"]
