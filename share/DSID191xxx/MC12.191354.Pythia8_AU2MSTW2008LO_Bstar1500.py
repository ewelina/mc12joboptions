evgenConfig.description = "Excited quark with Pythia8, AU2 tune and MSTW2008LO PDF, m=1500 GeV"
evgenConfig.keywords = ["excitedquark"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact  = [ "ning.zhou@cern.ch" ]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
topAlg.Pythia8.Commands += [
                           "ExcitedFermion:bg2bStar = on",
                           "4000005:m0 = 1500", # b* mass
                           "ExcitedFermion:Lambda =  1500",
                           "ExcitedFermion:coupF = 1.0", # SU(2) coupling
                           "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
                           "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
                           "4000005:mayDecay = on"] # b* -> b g, b gam, b Z0, t W-



  
