from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model dmV -modelname

generate p p > chi chi~ A QED=3 QCD=0

output -f
""")
fcard.close()

if os.access('run_card.dat',os.R_OK):
	bashCommand = "rm run_card.dat"
	subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
runcard = subprocess.Popen(['get_files','-data','MadGraph_MonoPhot_runcard.dat'])
runcard.wait()
if not os.access('MadGraph_MonoPhot_runcard.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('MadGraph_MonoPhot_runcard.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' nevents ' in line:
            newcard.write('   %i      = nevents ! Number of unweighted events requested) \n'%(runArgs.maxEvents*2))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 total energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 total energy in GeV \n'%(runArgs.ecmEnergy/2))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

Mxi = 50.
Wxi = 16.666667
Mchi = 400.
if os.access('param_card.dat',os.R_OK):
	bashCommand = "rm param_card.dat"
	subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
runcard = subprocess.Popen(['get_files','-data','MadGraph_dmV_paramcard.dat'])
runcard.wait()
if not os.access('MadGraph_dmV_paramcard.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('MadGraph_dmV_paramcard.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' Mxi ' in line:
            newcard.write('  101 %e # Mxi \n'%(Mxi))
        elif ' Wxi ' in line:
            newcard.write('DECAY 101 %e # Wxi  \n'%(Wxi))
        elif ' Mchi ' in line:
            newcard.write('  1000022 %e # Mchi \n'%(Mchi))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_dmV'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

evgenConfig.description = "Wimp pair monophoton with light mediator, dmV, M_WIMP=50 GeV, M_M=300 GeV, width(M)=M/8pi, pta>120 GeV"
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["WIMP","light","mediator"]
evgenConfig.contact = ["genest@lpsc.in2p3.fr","fuquan.wang@cern.ch"]

include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.inputfilecheck = stringy
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
topAlg.Pythia8.Commands +=["1000022:all = chi chi~ 2 0 0 50. 0"]
