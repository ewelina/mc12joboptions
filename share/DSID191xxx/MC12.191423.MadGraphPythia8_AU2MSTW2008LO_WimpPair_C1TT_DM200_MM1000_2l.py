from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model EffDMS_UFO -modelname

generate p p > t t~ chi chi~ C1=1 C2=0 C3=0 C4=0 C5=0 C6=0

output -f
""")
fcard.close()

process_dir = new_process()

ebeam1 = 6500.
ebeam2 = 6500.
xqcut = 0
mDM = 200
Mstar = 1000.

runcard = subprocess.Popen(['get_files','-data','MadGraph_EffDM_runcard.dat'])
runcard.wait()
if not os.access('MadGraph_EffDM_runcard.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_EffDM_runcard.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' ebeam1 ' in line:
            newcard.write('%f   = ebeam1  ! beam 1 energy in GeV \n'%(ebeam1))
        elif ' ebeam2 ' in line:
            newcard.write('%f   = ebeam2  ! beam 2 energy in GeV \n'%(ebeam2))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(runArgs.maxEvents*6))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ickkw ' in line:
            newcard.write('   0     = ickkw ! 0 no matching, 1 MLM, 2 CKKW matching \n')
        elif ' xqcut ' in line:
            newcard.write('   %i     = xqcut ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' dokt ' in line:
            newcard.write('   F     = dokt ! apply ktdurham cuts \n')
        elif ' pdlabel ' in line: 
            newcard.write(" 'lhapdf'     = pdlabel     ! PDF set\n 21000 = lhaid\n")
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

paramcard = subprocess.Popen(['get_files','-data','MadGraph_EffDM_paramcard.dat'])
paramcard.wait()
if not os.access('MadGraph_EffDM_paramcard.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('MadGraph_EffDM_paramcard.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'mDM' in line:
            newcard.write('  1000022 %e # mDM\n'%(mDM))
        elif 'Mstar' in line:
            newcard.write('    1 %e # Mstar\n'%(Mstar))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'WimpPair_C1'+str(runArgs.runNumber)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include ("MC12JobOptions/Pythia8_LHEF.py")

include ( 'MC12JobOptions/MultiLeptonFilter.py' )
topAlg.MultiLeptonFilter.Ptcut = 8000.

evgenConfig.generators += ["MadGraph", "Pythia8"]

topAlg.Pythia8.Commands += [
    '1000022:all = X Xbar 2 0 0 10.0 0.0 0.0 0.0 0.0',
    '1000022:isVisible = false'
]

evgenConfig.description = "Scalar DM pair production via vector EFT contact interaction"
evgenConfig.contact = ["Johanna Gramling <jgramlin@cern.ch>"]
evgenConfig.keywords = ["WIMP","DMHF","contactInteraction"]
evgenConfig.generators += ["MadGraph"]
evgenConfig.inputfilecheck = stringy

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
