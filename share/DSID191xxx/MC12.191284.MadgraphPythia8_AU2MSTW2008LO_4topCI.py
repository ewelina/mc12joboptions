include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "Four top production, Contact interaction, with MadGraph5+Pythia8, tune: AU2, pdf: MSTW2008LO"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["top","CI","exotics"]
evgenConfig.contact  = ["romain.madar@cern.ch"]
evgenConfig.inputfilecheck = '4topCI'
