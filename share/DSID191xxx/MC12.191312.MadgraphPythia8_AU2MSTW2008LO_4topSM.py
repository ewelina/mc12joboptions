### Generation of pp->tttt samples with MadGraph+Pythia ###

from MadGraphControl.MadGraphUtils import *

#####################################
###### HARD PROCESS GENERATION ######
#####################################

### Set generation card
procname='4top_SM'
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > t t~ t t~
output -f
""")
fcard.close()

### Read the proc_card and generate the directory (containing diagramms)
process_dir = new_process()

### Modified parameters w.r.t default run_card.SM.dat
nevents = runArgs.maxEvents

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' nevents ' in line:
            newcard.write('   %i     = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i     = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i     = ebeam1  ! beam 1 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('   %i     = ebeam2  ! beam 2 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' pdlabel ' in line:
            newcard.write('     \'lhapdf\'   = pdlabel     ! PDF set \n')
            newcard.write('      21000     = lhaid       ! if pdlabel=lhapdf, this is the lhapdf number\n')
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
            
# Run the hard process generation (ie ./generate_event of MG)
# --> To use a specific param card, write it and set the param_card_loc
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,run_name='Test',proc_dir=process_dir)
            


#######################################
###### SHOWERING + HADRONIZATION ######
#######################################

### Set the output name
prefix_name = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+procname

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=prefix_name+'._00001.events.tar.gz',skip_events=skip_events)

### PYTHIA 8 
runArgs.inputGeneratorFile=prefix_name+'._00001.events.tar.gz'
evgenConfig.description = "MadGraph5+Pythia8 for 4top SM"
evgenConfig.keywords = ["4top", "top", "SM"]
evgenConfig.contact  = ["romain.madar@cern.ch"]
evgenConfig.inputfilecheck = "4top"
evgenConfig.generators = ["MadGraph", "Pythia8"]

### MRST2008LO
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

