
evgenConfig.description = "ADD Graviton"
evgenConfig.keywords = ["Exotics", "Graviton"]
evgenConfig.contact = ["Olof Lundberg"]

include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )

topAlg.Pythia8.Commands += [ 'ExtraDimensionsLED:qqbar2Gg = on',       # Process type.
                             'ExtraDimensionsLED:qg2Gq = on',
                             'ExtraDimensionsLED:gg2Gg = on',  
                             'ExtraDimensionsLED:n = 2',               # Number of extra dimensions.
                             'ExtraDimensionsLED:MD = 4000',           # Choice of the scale, MD.
                             'ExtraDimensionsLED:CutOffmode = 0',      # Treatment of the effective theory (0: all the events. 1 : truncate events with s_hat>MD^2, with a weight of MD^2/s_hat^4).
                             'PhaseSpace:pTHatMin = 150.',              # pT Cut at the generator level.
                             '5000039:m0 = 200.',                      # Central value of the Breit-Wigner mass resonance
                             '5000039:mWidth = 100.',                  # Resonance width
                             '5000039:mMin = 1.',                      # Minimum mass of the Breit-Wigner distribution.
                             '5000039:mMax = 13990.']                  # Maximum mass of the Breit-Wigner distribution.
