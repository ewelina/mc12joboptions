
###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Vector triplet Vz -> WW -> lvjj"
evgenConfig.keywords = ["Exotics", "Vector", "Triplet"]
evgenConfig.contact = ["lei.li@cern.ch"]
evgenConfig.generators = ["MadGraph"]
evgenConfig.inputfilecheck = 'Agv1_VzWW_lvqq_m1800'

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
