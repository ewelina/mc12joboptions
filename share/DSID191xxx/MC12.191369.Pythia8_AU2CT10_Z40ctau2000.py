evgenConfig.description = "Long lived Z boson + q/g jet"
evgenConfig.keywords = ["Exotics", "LongLived"]
evgenConfig.contact = ["Michael Werner (mdwerner@iastate.edu)"]


include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")


topAlg.Pythia8.Commands += \
    ["HardQCD:all = off",
     'PhaseSpace:pTHatMin = 250.', 'PhaseSpace:mHatMin = 250.']

topAlg.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2', 
                             'WeakBosonAndParton:qqbar2gmZg = on',      
                             'WeakBosonAndParton:qg2gmZq = on',       
                             '23:onMode = off',      
                             '23:m0 = 40',            
                             '23:tau0 = 2000',         
                             '23:onIfAll = -1 1',      
                             '23:onIfAll = -2 2',      
                             '23:onIfAll = -3 3',      
                             '23:onIfAll = -4 4',      
                             '23:onIfAll = -5 5',    
                             '23:onIfAll = -6 6']   


from TruthExamples.TruthExamplesConf import TestHepMC
testHepMC = TestHepMC()
testHepMC.MaxTransVtxDisp = 1000000 
testHepMC.MaxVtxDisp      = 1000000000 
topAlg += testHepMC
