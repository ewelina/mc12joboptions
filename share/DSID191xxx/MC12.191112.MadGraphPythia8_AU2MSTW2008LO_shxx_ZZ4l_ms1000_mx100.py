include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "Higgs+XX, scalar model"
evgenConfig.contact = ["lashkar.kashif@cern.ch"]
evgenConfig.keywords = ["SMhiggs","WIMP","ZZ4l"]
evgenConfig.inputfilecheck = 'ZZ4l_ms1000_mx100'
