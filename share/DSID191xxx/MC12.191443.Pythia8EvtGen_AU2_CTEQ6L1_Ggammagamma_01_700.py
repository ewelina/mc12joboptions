######################################################################
# MC12 Graviton to gam gam decay with Pythia 8
######################################################################

evgenConfig.description = "RS Graviton, m = 700 GeV, kappaMG = 0.542."
evgenConfig.keywords = ["exotic", "graviton","diphoton","RandallSundrum","BSM"]
evgenConfig.contact = ["simone.mazza@mi.infn.it","jan.stark@cern.ch"]
evgenConfig.process = "RS Graviton -> gam gam"

include ("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include ("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands +=["PDF:pSet= 8",
                           "ExtraDimensionsG*:gg2G* = on",
                           "ExtraDimensionsG*:ffbar2G* = on",
                           "5100039:m0 = 700.",
                           "5100039:onMode = off",
                           "5100039:onIfAny = 22",
                           "ExtraDimensionsG*:kappaMG = 0.542"
                           ]
