from MadGraphControl.MadGraphUtils import *
import re


fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model Bprime
define p = p b b~
define l- = l- ta-
define l+ = l+ ta+
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define j = j b b~
generate p p > t w- QAD=2 QCD=0 QED=0, t > b l+ vl, w- > j j
add process p p > t w- QAD=2 QCD=0 QED=0, t > b j j, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > b l+ vl, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > b j j, w- > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > b~ j j, w+ > l+ vl
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > b~ j j, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > b~ l- vl~, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > b~ l- vl~, w+ > l+ vl
add process p p > t w- QAD=2 QCD=0 QED=0, t > s l+ vl, w- > j j
add process p p > t w- QAD=2 QCD=0 QED=0, t > s j j, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > s l+ vl, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > s j j, w- > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > s~ j j, w+ > l+ vl
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > s~ j j, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > s~ l- vl~, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > s~ l- vl~, w+ > l+ vl
add process p p > t w- QAD=2 QCD=0 QED=0, t > d l+ vl, w- > j j
add process p p > t w- QAD=2 QCD=0 QED=0, t > d j j, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > d l+ vl, w- > l- vl~
add process p p > t w- QAD=2 QCD=0 QED=0, t > d j j, w- > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > d~ j j, w+ > l+ vl
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > d~ j j, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > d~ l- vl~, w+ > j j
add process p p > t~ w+ QAD=2 QCD=0 QED=0, t~ > d~ l- vl~, w+ > l+ vl
output -f
""")
fcard.flush()


process_dir = new_process()
print "process_dir"
print "process_dir"
print "process_dir"
print "process_dir"
print process_dir

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 6500.

#Bprime Mass [GeV]  Width [GeV] 
bprimemasswidth = [
[ 300 , 9.52617 ],
[ 350 , 12.4645 ],
[ 400 , 15.3614 ],
[ 450 , 18.214 ],
[ 500 , 21.0219 ],
[ 550 , 23.7891 ],
[ 600 , 26.5207 ],
[ 650 , 29.2219 ],
[ 700 , 31.8972 ],
[ 750 , 34.5506 ],
[ 800 , 37.1852 ],
[ 850 , 39.8038 ],
[ 900 , 42.4087 ],
[ 950 , 45.0017 ],
[ 1000 , 47.5845 ],
[ 1050 , 50.1582 ],
[ 1100 , 52.7241 ],
[ 1150 , 55.2831 ],
[ 1200 , 57.8359 ],
[ 1250 , 60.3833 ],
[ 1300 , 62.9258 ],
[ 1350 , 65.4639 ],
[ 1400 , 67.9982 ],
[ 1450 , 70.5289 ],
[ 1500 , 73.0564 ],
[ 1550 , 75.581 ],
[ 1600 , 78.103 ],
[ 1650 , 80.6225 ],
[ 1700 , 83.1399 ],
[ 1750 , 85.6553 ],
[ 1800 , 88.1688 ],
[ 1850 , 90.6805 ],
[ 1900 , 93.1907 ],
[ 1950 , 95.6995 ],
[ 2000 , 98.2068 ],
[ 2050 , 100.713 ],
[ 2100 , 103.218 ],
[ 2150 , 105.722 ],
[ 2200 , 108.225 ],
[ 2250 , 110.727 ],
]

width=0.0
mass=int(re.findall(r'\d+',re.findall(r'bstarWt\d+GeV',runArgs.jobConfig[0])[0])[0])
LH=False
if re.findall(r'GeVRH',runArgs.jobConfig[0]) == []:
    LH=True

for [x,y] in bprimemasswidth:
    if(x==mass):
        width=y

print "Detected mass: " + str(mass)
print "Corresponding bstar width: " + str(width)
print "Is LH? " + str(LH)

shutil.copy(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat','run_card.SM.dat')
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(runArgs.maxEvents * 1.1))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('     %i     = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('     %i     = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' pdlabel ' in line:
            newcard.write(" 'lhapdf'    = pdlabel     ! PDF set\n")
        elif ' lhaid ' in line:
            newcard.write(' 21000       = lhaid     ! if pdlabel=lhapdf, this is the lhapdf number\n')
        elif ' fixed_ren_scale ' in line:
            newcard.write(" T        = fixed_ren_scale  ! if .true. use fixed ren scale\n")
        elif ' fixed_fac_scale ' in line:
            newcard.write(" T        = fixed_fac_scale  ! if .true. use fixed fac scale\n")
        elif ' fixed ren scale' in line:
            newcard.write(' %i  = scale            ! fixed ren scale\n'%(mass))
        elif ' dsqrt_q2fact1 ' in line:
            newcard.write(' %i  = dsqrt_q2fact1    ! fixed fact scale for pdf1\n'%(mass))
        elif ' dsqrt_q2fact2 ' in line:
            newcard.write(' %i  = dsqrt_q2fact2    ! fixed fact scale for pdf2\n'%(mass))
        elif ' lhe_version ' in line:
            newcard.write(' 2.0      = lhe_version      ! Change the way clustering information pass to shower.\n')
        elif ' bwcutoff ' in line:
            newcard.write('  10000  = bwcutoff      ! (M+/-bwcutoff*Gamma)\n')
        elif ' cut_decays ' in line:
            newcard.write('   F  = cut_decays    ! Cut decay products\n')

        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


if not os.access(process_dir+'/Cards/param_card.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open(process_dir+'/Cards/param_card.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if 'MBP' in line:
            newcard.write('  1005 %i # MBP \n'%(mass))  ## is set at top
        elif 'DECAY 1005' in line:
            newcard.write('DECAY 1005 %f  # WBP \n'%(width))  ## got from table if mass is known and in table
        elif 'fnewL' in line:
            newcard.write('    1 %fe+00 # fnewL \n'%(LH))  ## is set at top
        elif 'fnewR' in line:
            newcard.write('    2 %fe+00 # fnewR \n'%(not LH))  ## is set at top as well
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='./run_card.dat',param_card_loc='./param_card.dat',njobs=1,run_name=str(runArgs.runNumber),proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph'
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name=str(runArgs.runNumber),proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)




include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")



topAlg.Pythia8.LHEFile = "./events.lhe"

print "ecmEnergy"
print int(runArgs.ecmEnergy)
topAlg.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)


evgenConfig.description = "MadGraph+Pythia8 production JO with the AU2 MSTW2008LO tune for 1200 GeV bstar (LH)"

evgenConfig.keywords = ["Bprime"]

evgenConfig.generators = ["MadGraph", "Pythia8"]

evgenConfig.contact =  ['dsperlic@cern.ch']

evgenConfig.minevents = 5000

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

