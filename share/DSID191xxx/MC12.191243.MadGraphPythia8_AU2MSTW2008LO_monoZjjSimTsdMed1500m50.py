include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "WZ+XX, ISR EFT model"
evgenConfig.keywords = ["WIMP","boosted"]
evgenConfig.inputfilecheck = 'monoZjjSimTsdMed1500m50'
evgenConfig.contact = ["andrew.james.nelson@cern.ch"]
