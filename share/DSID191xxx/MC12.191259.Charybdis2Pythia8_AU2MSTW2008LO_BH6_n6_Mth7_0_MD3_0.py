#--------------------------------------------------------------
# MC12 common configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

#--------------------------------------------------------------
# Configuration for Charybdis2
#--------------------------------------------------------------
evgenConfig.description = "Charybdis2 + Pythia8 with the AU2 MSTW2008LO tune, BH6_n6_Mth7_0_MD3_0"
evgenConfig.keywords = ["charybdis2"]
evgenConfig.generators += ["Charybdis2"]
evgenConfig.inputfilecheck = "Charybdis2"
evgenConfig.contact = ['james.frost@cern.ch']	
