include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

evgenConfig.description = 'AlpGen+Herwig, gamma + 3 jets, phPt > 280 GeV'
evgenConfig.keywords = ["gamma"]
evgenConfig.contact = ['renaud.bruneliere@cern.ch']
evgenConfig.process = 'alpgen_herwig_jimmy_ph3j_nunu_8tev_ph280_param0'
evgenConfig.inputfilecheck = 'Gam'
evgenConfig.minevents = 5000
