include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

evgenConfig.description = 'AlpGen+Herwig, Zvv + 5 jets, met > 500 GeV, 1-jet filter'
evgenConfig.keywords = ["Z","nu"]
evgenConfig.contact = ['renaud.bruneliere@cern.ch']
evgenConfig.process = 'alpgen_herwig_jimmy_z5j_nunu_8tev_met500_param0'
evgenConfig.inputfilecheck = 'Znunu'
evgenConfig.minevents = 500

# Truth jet filter
include ( "MC12JobOptions/JetFilterAkt4.py" )
topAlg.QCDTruthJetFilter.MinPt = 30.*GeV
topAlg.QCDTruthJetFilter.MaxEta = 5
