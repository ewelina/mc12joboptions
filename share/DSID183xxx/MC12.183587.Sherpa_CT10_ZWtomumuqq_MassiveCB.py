include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ZW decaying semileptonically to mumuqq, merged with up to three additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.contact  = ["Takashi.Yamanaka@cern.ch"]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  MASSIVE[4]=1
  MASSIVE[5]=1
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  9923[a] 24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  9923[a] 24[b] 
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  9923[a] 24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  9923[a] 24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay 24[b] -> 94 4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  9923[a] -24[b] 93{3}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  9923[a] -24[b] 
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  9923[a] -24[b] 93{1}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  9923[a] -24[b] 93{2}
  Decay 9923[a] -> 13 -13
  Decay -24[b] -> 94 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

}(processes)

(selector){
  DecayMass 9923 7.0 E_CMS
}(selector)
"""
