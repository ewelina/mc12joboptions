include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WZ decaying semileptonically to enuqq, merged with up to one additional QCD jet in ME+PS."
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.contact  = ["frank.siegert@cern.ch"]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  MASSIVE[4]=1
  MASSIVE[5]=1
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;




  Process 93 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  24[a] 9923[b] 93{3}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  24[a] 9923[b] 
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  24[a] 9923[b] 93{1}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  24[a] 9923[b] 93{2}
  Decay 24[a] -> -11 12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;


  Process 93 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 4 -4
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;




  Process 93 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}  
  Cut_Core 1
  End process;

  Process -4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 ->  -24[a] 9923[b] 93{3}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 -4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -4 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 -5 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 -4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -4 -5 -5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 -5 -> -5 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 4 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 5 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -4 -5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process -5 93 -> -5 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process -5 93 -> -5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 4 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 5 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 5 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;
  
  Process 4 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 4 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> -4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 4 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 5 -> 5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> -5 5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 4 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 5 93 -> 5  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 -5 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4 4  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4 4  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 4 5  -24[a] 9923[b] 
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> -5 5  -24[a] 9923[b] 93{1}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

  Process 93 93 -> 4  -24[a] 9923[b] 93{2}
  Decay -24[a] -> 11 -12
  Decay 9923[b] -> 5 -5
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05 {5,6,7} 
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Cut_Core 1
  End process;

}(processes)

(selector){
  DecayMass 9923 7.0 E_CMS
}(selector)
"""
