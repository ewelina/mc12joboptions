## Pythia8 Z'(750)->tautau
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Zprime(750)->tautau production with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "Zprime", "tau"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", # create Z bosons
                            "PhaseSpace:mHatMin = 375.", # lower invariant mass
                            "32:m0 = 750", # set Z mass
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny = 15"] # switch on Z->tautau decays
