## Pythia8 W->tau nu with alternative PDF set
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "W->tau nu production with the AU2 MSTW2008LO tune"
evgenConfig.keywords = ["EW", "W", "tau"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15"] # switch on W->tau,nu decays
