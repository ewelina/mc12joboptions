## Pythia8 Z'(500)->tautau with alternative PDF set
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Zprime(500)->tautau production with the AU2 MSTW2008LO tune"
evgenConfig.keywords = ["EW", "Zprime", "tau"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", # create Z bosons
                            "PhaseSpace:mHatMin = 250.", # lower invariant mass
                            "32:m0 = 500", # set Z mass
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny = 15"] # switch on Z->tautau decays
