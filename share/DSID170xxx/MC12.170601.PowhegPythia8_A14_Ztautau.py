include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="A14_CTEQ6L1"
process="Z_tautau"

def powheg_override():
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production without lepton filter and A14 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "tau"]


include("MC12JobOptions/PowhegControl_postInclude.py")

include('MC12JobOptions/Pythia8_Powheg_Main31.py')


topAlg.Pythia8.UserModes += ['Main31:NFinal = 1',
                             'Main31:pTHard = 0',
                             'Main31:pTdef = 2'
                             ]
