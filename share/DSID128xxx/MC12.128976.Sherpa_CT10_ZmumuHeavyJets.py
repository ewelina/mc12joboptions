# Prepared by Frank Siegert
evgenConfig.description = "Zmumu + Heavy Flavour jets, 4 additional jets"
evgenConfig.keywords = [ "Z", "Heavy Flavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>","frank.siegert@cern.ch"]
evgenConfig.process="Zmumu+HF jets"

include( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(run){
MASSIVE[4]=1
MASSIVE[5]=1
}(run)

(processes){
Process 93 93 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 5 -5 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93 5 -> 13 -13 5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93 -5 -> 13 -13 -5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;


Process 93 93 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93  4 -> 13 -13 4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93 -4 -> 13 -13 -4 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process  5 -5 -> 13 -13 4 -4 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process  4 -4 -> 13 -13 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93  5 -> 13 -13 5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93  4 -> 13 -13 4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93 -5 -> 13 -13 -5 4 -4 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process 93 -4 -> 13 -13 -4 5 -5 93{1}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process  4  5 -> 13 -13 4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process -4  5 -> 13 -13 -4 5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process  4 -5 -> 13 -13 4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;

Process -4 -5 -> 13 -13 -4 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Cut_Core 1;
End process;
}(processes)

(selector){
Mass 13 -13 40 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""

