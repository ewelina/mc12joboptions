evgenConfig.description = "ALPGEN+Pythia GamGam(mgg>50GeV)+2jets(ex) process with PythiaPerugia2011C tune"
evgenConfig.keywords = [ "gammagamma" ]
evgenConfig.inputfilecheck = "gamgamNp2_mgg50"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
