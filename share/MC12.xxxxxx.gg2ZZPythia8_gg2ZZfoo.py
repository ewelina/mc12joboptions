evgenConfig.description = "gg2ZZ+Pythia8 production of gg->ZZ with CTEQ6L1 PDF"
evgenConfig.keywords = ["electroweak", "ZZ"]
evgenConfig.inputfilecheck = "gg2ZZ_LHEF.*"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")
