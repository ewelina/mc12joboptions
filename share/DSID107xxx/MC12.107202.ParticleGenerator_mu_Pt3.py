evgenConfig.description = "Single muons with constant pt 3 GeV"
evgenConfig.keywords = ["mu","singleparticle"]
include("MC12JobOptions/ParticleGenerator_Common.py")

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 3

ParticleGenerator.orders = [
 "PDGcode: sequence -13 13",
 "pt: constant 3000",
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
