evgenConfig.description = "Single muons with flat pt [4-100] GeV and flat vertex x, y -20 to 20 mm"
evgenConfig.keywords = ["mu","singleparticle"]
include("MC12JobOptions/ParticleGenerator_Common.py")

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 3

ParticleGenerator.orders = [
 "PDGcode: sequence -13 13",
 "pt: flat 4000 100000",
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159",
 "vertx: flat -20 20",
 "verty: flat -20 20"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
