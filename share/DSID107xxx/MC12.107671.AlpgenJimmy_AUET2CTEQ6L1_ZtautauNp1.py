evgenConfig.description = "ALPGEN+Jimmy Z(->tautau)+1jet process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW", "Z", "tau"]
evgenConfig.inputfilecheck = "ZtautauNp1"
evgenConfig.minevents = 5000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
