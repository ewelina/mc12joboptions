#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'leptons-nu'
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AU2 CT10 tune
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZllnunu_ee production mll>4GeV without filter using CT10 pdf and AU2 CT10 tune'
evgenConfig.keywords    = [ 'electroweak', 'ZZ', 'lepton', 'neutrino' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'oldrich.kepka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents   = 5000
evgenConfig.inputconfcheck = "ZZllnunu_ee_mll4"