include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Wp_enu"

evgenConfig.description = "POWHEG+Pythia8 Wplus->enu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]

include("MC12JobOptions/PowhegControl_postInclude.py")
