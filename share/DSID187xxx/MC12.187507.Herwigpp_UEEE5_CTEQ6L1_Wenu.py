# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py' )

evgenConfig.description = "Herwig++ Wenu sample with CTEQ6L1 PDF and UE-EE5 tune"
evgenConfig.keywords = ["electron", "W"]
evgenConfig.contact = ["Orel Gueta"]

# Configure Herwig
cmds = """\
## Set up qq -> W+- -> e+- nu process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2W2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Electron
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

# Set up lepton filter
include("MC12JobOptions/LeptonFilter.py")
evgenConfig.minevents = 5000

