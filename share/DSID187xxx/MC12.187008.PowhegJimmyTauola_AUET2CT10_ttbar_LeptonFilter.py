evgenConfig.description = "POWHEG+fHerwig/Jimmy ttbar production with a 1 MeV lepton filter and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar' 

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

## Generator = Powheg
evgenConfig.generators += [ "Powheg"]

# use Tauola for validation samples
include("MC12JobOptions/Jimmy_Tauola.py")

include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
