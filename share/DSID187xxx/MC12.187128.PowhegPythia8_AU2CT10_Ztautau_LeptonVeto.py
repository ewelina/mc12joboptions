include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_tautau"

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production with central lepton veto pt<15 GeV and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "ptlepton<15 GeV", "|etalepton|<2.7"]

# compensate filter efficiency
evt_multiplier = 1.5

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonVeto.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
