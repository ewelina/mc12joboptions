######################################################################
#
# gg2VV 3.1.6/Powheg/Pythia8 gg -> (H) -> WpWm, with WpWm -> munutaunu

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '24:onMode = off',#decay of W
                            '23:onMode = off',#decay of Z
                            'SpaceShower:pTmaxMatch = 1'#no power shower, just wimpy showers
                            ]

evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.description = 'gg2VV, (H->)WpWm-> munutaunu using CT10 PDF, PowhegPythia8 with AU2 CT10 tune, m(l+l-)>4GeV without filter, Higgs removed from LHE file to avoid ME matched Pythia shower since mainly background, interference kept'
evgenConfig.keywords = ['diboson', 'leptonic', 'EW']
evgenConfig.contact = ['jochen.meyer@cern.ch']
evgenConfig.inputfilecheck = 'gg2VV0316.187406.WpWmmunutaunu'
