include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Wm_munu"

evgenConfig.description = "POWHEG+Pythia8 Wmin->munu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu"]

include("MC12JobOptions/PowhegControl_postInclude.py")
