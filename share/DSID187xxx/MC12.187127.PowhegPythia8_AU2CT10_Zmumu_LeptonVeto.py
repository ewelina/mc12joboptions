include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_mumu"

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with central lepton veto pt<15 GeV and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "ptlepton<15 GeV", "|etalepton|<2.7"]

# compensate filter efficiency
evt_multiplier = 10

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonVeto.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
