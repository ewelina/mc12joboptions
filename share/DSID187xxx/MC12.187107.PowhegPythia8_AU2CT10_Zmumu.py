include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_mumu"

evgenConfig.description = "POWHEG+Pythia8 Z->mumu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu"]

include("MC12JobOptions/PowhegControl_postInclude.py")
