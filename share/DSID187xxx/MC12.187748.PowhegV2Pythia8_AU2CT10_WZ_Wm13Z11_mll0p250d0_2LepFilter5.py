#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.decay_mode = 'WpZmuvee'
PowhegConfig.withdamp = 1
PowhegConfig.mllmin     = 0.25
# take into account the filter efficiency of 29%
PowhegConfig.nEvents *= 3.5
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AU2 CT10 tune
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
include('MC12JobOptions/MultiLeptonFilter.py')
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut   = 2.7
topAlg.MultiLeptonFilter.Ptcut    = 5.*GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WpZmuvee production and AU2 CT10 tune, cut on all SFOS pair mll>2*m(l from Z)+250MeV, dilepton filter pt>5GeV, eta<2.7'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'lepton' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'alexander.oh@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents   = 5000
