include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Wp_taunu"

evgenConfig.description = "POWHEG+Pythia8 Wplus->taunu production with central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau", "ptlepton>15 GeV", "|etalepton|<2.7"]

# compensate filter efficiency
evt_multiplier = 15

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
