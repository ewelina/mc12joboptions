# General Description
evgenConfig.description = "Herwig++ gamma+jet performace sample with CTEQ6L1 EE4 tune (jets, gamma+jet) with 80 < photon pT < 150 GeV"
evgenConfig.generators=["Herwigpp"]
evgenConfig.keywords = ["egamma", "performance", "jets"]
evgenConfig.contact = ["Orel Gueta"]

# Include Herwig Info
include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py")

# Configure Herwig
cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEGammaJet
set /Herwig/Cuts/JetKtCut:MinKT 40.*GeV
set /Herwig/Cuts/PhotonKtCut:MinKT 40.*GeV
set /Herwig/Cuts/PhotonKtCut:MinEta -100.
set /Herwig/Cuts/PhotonKtCut:MaxEta 100.
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

# Set up photon filter
include("MC12JobOptions/LeadingPhotonFilter.py")
topAlg.LeadingPhotonFilter.PtMin = 80.*GeV
topAlg.LeadingPhotonFilter.PtMax = 150.*GeV
