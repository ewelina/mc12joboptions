#--------------------------------------------------------------
# Powheg Dijet setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Dijet_Common.py')
PowhegConfig.bornktmin = 900.0
PowhegConfig.nEvents   = 30000
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with main31 and CT10
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include('MC12JobOptions/Pythia8_Powheg_Main31.py')

#--------------------------------------------------------------
# Include jet filter
#--------------------------------------------------------------
include('MC12JobOptions/JetFilter_JZ6.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 dijet production with bornktmin = {0} GeV with the AU2 CT10 tune'.format( PowhegConfig.bornktmin )
evgenConfig.keywords    = [ 'QCD', 'dijet', 'jets', 'JZ6' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' , 'ZLMarshall@lbl.gov' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents   = 500
evgenConfig.weighting   = 0 # to avoid failure with high weights - TODO: remove this if fix is implemented
evgenConfig.inputconfcheck = "jetjet_JZ6"
