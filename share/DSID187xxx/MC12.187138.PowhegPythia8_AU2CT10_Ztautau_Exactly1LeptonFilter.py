include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_tautau"

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production with exactly one central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "exactly one ptlepton>15 GeV |etalepton|<2.7"]

# compensate filter efficiency
evt_multiplier = 6

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7

include("MC12JobOptions/MultiLeptonVeto.py")
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
