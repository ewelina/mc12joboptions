evgenConfig.description = "POWHEG+Pythia6 ttbar production with a 1 MeV lepton filter and PythiaPerugia2011C tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["Gia.Khoriauli@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

#use Tauola for validation samples!
include("MC12JobOptions/Pythia_Tauola.py")

include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/Pythia_EvtGen.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
