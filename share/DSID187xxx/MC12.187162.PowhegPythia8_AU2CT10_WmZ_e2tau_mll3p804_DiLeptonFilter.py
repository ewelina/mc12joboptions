include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="WmZ_enutautau"

evgenConfig.description = "POWHEG+Pythia8 WZ production and AU2 CT10 tune, cut on all SFOS pair mll>2*m(l from Z)+3804MeV, dilepton filter pt>5GeV, eta<2.7"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic" ]
evgenConfig.contact = ["Alexander Oh <alexander.oh@cern.ch>"]

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.mllmin = 3.804

# compensate filter efficiency
evt_multiplier = 6.5

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV
