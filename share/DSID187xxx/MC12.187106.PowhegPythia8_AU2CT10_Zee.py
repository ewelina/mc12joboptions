## POWHEG+Pythia8 Z->ee

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Z_ee"

evgenConfig.description = "POWHEG+Pythia8 Z->ee production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e"]

include("MC12JobOptions/PowhegControl_postInclude.py")
