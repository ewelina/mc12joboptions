#--------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ZZ_Common.py')
PowhegConfig.decay_mode = 'ZZeeee'
PowhegConfig.withdamp = 1
PowhegConfig.mllmin     = 4.0
# take into account the filter efficiency of 90%
PowhegConfig.nEvents *= 1.1
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AU2 CT10 tune
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
include('MC12JobOptions/MultiLeptonFilter.py')
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut   = 10
topAlg.MultiLeptonFilter.Ptcut    = 5.*GeV

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ_4e production mll>4GeV with dimuon filter pt>5GeV using CT10 pdf and AU2 CT10 tune'
evgenConfig.keywords    = [ 'electroweak', 'ZZ', 'leptons' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'oldrich.kepka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.minevents   = 5000