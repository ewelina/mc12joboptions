include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
process="Wp_munu"

evgenConfig.description = "POWHEG+Pythia8 Wplus->munu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu"]

include("MC12JobOptions/PowhegControl_postInclude.py")
