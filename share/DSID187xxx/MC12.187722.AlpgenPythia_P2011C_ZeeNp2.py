evgenConfig.description = "ALPGEN+Pythia Z(->ee)+2jets process with PythiaPerugia2011C tune, HPC"
evgenConfig.keywords = ["EW", "Z", "e"]
evgenConfig.inputfilecheck = "ZeeNp2"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

