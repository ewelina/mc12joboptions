## Herwig++ config for the CTEQ6L1 UE-EE-5 tune series with NLO events read from an LHEF file
include("MC12JobOptions/Herwigpp_UEEE5_MRSTMCal_CT10ME_LHEF_Common.py")

evgenConfig.generators += ["Powheg","Herwigpp"]
evgenConfig.description = "POWHEG+Herwigpp ttbar production with a 1 MeV lepton filter and Herwigpp UEEE4 tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["Orel.Gueta@cern.ch", "Thorsten.Kuhl@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/TTbarWToLeptonFilter.py")
