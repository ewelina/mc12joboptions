evgenConfig.description = "Single pions with constant pt 5 15 50 100 GeV"
evgenConfig.keywords = ["pions","singleparticle"]
include("MC12JobOptions/ParticleGenerator_Common.py")

# For VERBOSE output from ParticleGenerator.
ParticleGenerator.OutputLevel = 3

ParticleGenerator.orders = [
 "PDGcode: sequence -211 211 -211 211 211 -211 211 -211",
 "pt: sequence 5000 15000 50000 100000",
 "eta: flat -4.0 4.0",
 "phi: flat -3.14159 3.14159"
 ]

#==============================================================
#
# End of job options file
#
###############################################################
