evgenConfig.description = "Z->tautau, tau- ->mugamma production, NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Z ", "tau", "leptons"]
evgenConfig.contact = ['Olga.Igonkina_@_cern.ch']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z/gamma* bosons, fill interference
                            "PhaseSpace:mHatMin = 20.", # lower invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15", # switch on Z->tau tau decays
                            "15:1:onMode = 2",
                            "15:2:onMode = 2",
                            "15:3:onMode = 2",
                            "15:4:onMode = 2",
                            "15:5:onMode = 2",
                            "15:6:onMode = 2",
                            "15:7:onMode = 2",
                            "15:8:onMode = 2",
                            "15:9:onMode = 2",
                            "15:10:onMode = 2",
                            "15:11:onMode = 2",
                            "15:12:onMode = 2",
                            "15:13:onMode = 2",
                            "15:14:onMode = 2",
                            "15:15:onMode = 2",
                            "15:16:onMode = 2",
                            "15:17:onMode = 2",
                            "15:18:onMode = 2",
                            "15:19:onMode = 2",
                            "15:20:onMode = 2",
                            "15:21:onMode = 2",
                            "15:22:onMode = 2",
                            "15:23:onMode = 2",
                            "15:24:onMode = 2",
                            "15:25:onMode = 2",
                            "15:26:onMode = 2",
                            "15:27:onMode = 2",
                            "15:28:onMode = 2",
                            "15:29:onMode = 2",
                            "15:30:onMode = 2",
                            "15:31:onMode = 2",
                            "15:32:onMode = 2",
                            "15:33:onMode = 2",
                            "15:34:onMode = 2",
                            "15:35:onMode = 2",
                            "15:36:onMode = 2",
                            "15:37:onMode = 2",
                            "15:addChannel = 3 1.0 0 13 22"   # tau- decay

]

