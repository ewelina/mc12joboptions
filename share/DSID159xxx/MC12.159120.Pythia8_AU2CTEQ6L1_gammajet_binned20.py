evgenConfig.description = "Pythia8 gamma+jet performance sample with 20 < photon pT < 40 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets"]
evgenConfig.contact = ["Matt Relich"]

## Configure Pythia
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 10"]

# Set up photon filter
include("MC12JobOptions/LeadingPhotonFilter.py")
topAlg.LeadingPhotonFilter.PtMin = 20000.
topAlg.LeadingPhotonFilter.PtMax = 40000.
