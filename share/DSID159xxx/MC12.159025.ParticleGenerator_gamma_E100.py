evgenConfig.description = "Single photons with ET=100 GeV"
evgenConfig.keywords = ["singleparticle", "gamma"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 22",
 "et: constant 100000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
