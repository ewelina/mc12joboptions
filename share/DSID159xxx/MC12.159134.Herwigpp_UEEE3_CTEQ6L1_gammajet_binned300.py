# General Description
evgenConfig.description = "Herwig++ gamma+jet performace sample with CTEQ6L1 EE3 tune (jets, gamma+jet) with 300 GeV < photon pT "
evgenConfig.generators=["Herwigpp"]
evgenConfig.keywords = ["egamma", "performance", "jets"]

# Include Herwig Info
include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")

# Configure Herwig
topAlg.Herwigpp.Commands += ["insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEGammaJet",
                             "set /Herwig/Cuts/JetKtCut:MinKT 250.*GeV",
                             "set /Herwig/Cuts/PhotonKtCut:MinKT 250.*GeV",
                             "set /Herwig/Cuts/PhotonKtCut:MinEta -100.",
                             "set /Herwig/Cuts/PhotonKtCut:MaxEta 100."
                             ]

# Set up photon filter
include("MC12JobOptions/LeadingPhotonFilter.py")
topAlg.LeadingPhotonFilter.PtMin = 300000.

