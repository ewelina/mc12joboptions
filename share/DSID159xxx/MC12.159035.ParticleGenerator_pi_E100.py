evgenConfig.description = "Single pi+/- with ET=100 GeV"
evgenConfig.keywords = ["singleparticle", "pi"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -211 211",
 "et: constant 100000",
 "eta: flat -3.2 3.2",
 "phi: flat -3.14159 3.14159"
 ]
