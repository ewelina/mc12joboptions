## Single-tau, pt=10-500 GeV
## author: Martin Flechl, Apr 3, 2012

evgenConfig.description = "Single-particle tau, hadronic decays forced"
evgenConfig.keywords = ["tau", "hadronic"]

include("MC12JobOptions/ParticleGenerator_Common.py")
include("MC12JobOptions/Photos_Fragment.py")
include("MC12JobOptions/Tauola_HadronicDecay_Fragment.py")

topAlg.ParticleGenerator.orders = [
             "PDGcode: sequence  15 -15",
             "pt: flat 10000 500000",
             "eta: flat -2.7 2.7",
             "phi: flat -3.14159 3.14159"
]
