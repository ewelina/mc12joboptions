include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.description = "BlackMax + Pythia8 with the AU2 MSTW2008LO tune, BH11: n=6, Mp=2.5 TeV, Mth=5TeV"
evgenConfig.keywords = ["blackhole"]
evgenConfig.generators += ["Lhef","BlackMax"]
evgenConfig.inputfilecheck = "BlackMax"
