#
#  Control file to generate LQ production in Pythia, M(LQ) = 700 GeV, decay: LQ -> c mu
#
#  Prepared by V.Savinov ( vps3@pitt.edu, http://www.phyast.pitt.edu/~savinov ), 01/18/07
#
#  Sept.4,2008:  Modified and (re)validated with 14.2.20.2 by R.Yoosoofmiya and V.Savinov
#
# Version Jan 19 2011 By Jahred Adelman changing coupling to 0.01
# Version Feb 8 2011 Jahred Adelman. Also change to use D6 tune, and remove single LQ production
#
# For MC12, Sep 26, 2012  Giovanni Siragusa. Using regular Pythia6

evgenConfig.description = "Pythia6 Leptoquark (M = 700 GeV) to di-muon jet channel"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0"]
topAlg.Pythia.PythiaCommand += ["pysubs msub 163 1"]
topAlg.Pythia.PythiaCommand += ["pysubs msub 164 1"]
topAlg.Pythia.PythiaCommand += ["pydat2 pmas 42 1 700."]
topAlg.Pythia.PythiaCommand += ["pysubs ckin 41 500.0",
                                "pysubs ckin 42 900.0",
                                "pysubs ckin 43 500.0",
                                "pysubs ckin 44 900.0",]
topAlg.Pythia.PythiaCommand += ["pydat3 brat 539 1."]
topAlg.Pythia.PythiaCommand += ["pydat3 kfdp 539 1 4",
                                "pydat3 kfdp 539 2 13",]
topAlg.Pythia.PythiaCommand += ["pydat1 paru 151 0.01"]
topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
topAlg.Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
topAlg.Pythia.PythiaCommand += ["pyinit dumpr 1 20"]

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
