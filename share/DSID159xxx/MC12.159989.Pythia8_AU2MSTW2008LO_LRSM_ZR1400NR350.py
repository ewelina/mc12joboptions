evgenConfig.keywords = [ "LRSM","heavy neutrino","ZR" ]
evgenConfig.contact  = [ "kirill.skovpen@cern.ch" ]
evgenConfig.description = "pp->ZR(1400)->NN(350)->lljjjj; l=e,mu,tau; no N mixing"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += [
                            "LeftRightSymmmetry:ffbar2ZR = on",
                            "9900023:m0 = 1400",
                            "9900012:m0 = 350",
                            "9900014:m0 = 350",
                            "9900016:m0 = 350",
                            "9900023:onMode = off",
                            "9900023:8:products = 9900012 9900012",
                            "9900023:11:products = 9900014 9900014",
                            "9900023:14:products = 9900016 9900016",
                            "9900023:8:onMode = on",
                            "9900023:11:onMode = on",
                            "9900023:14:onMode = on"
                            ]
