evgenConfig.description = "Single K^0_L with Pt=0.2-30GeV Eta=2.2-2.3"
evgenConfig.keywords = ["singleparticle", "K0L"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 130",
 "pt: flat 200.0 30000.",
 "eta: flat 2.2 2.3",
 "phi: flat -3.14159 3.14159"
 ]
