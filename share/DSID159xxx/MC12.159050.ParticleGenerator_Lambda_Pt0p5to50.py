evgenConfig.description = "Single Lambda with flat Pt in [0.5-50] GeV"
evgenConfig.keywords = ["singleparticle", "lambda"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -3122 3122",
 "pt:  flat 500 50000",
 "eta: flat -2.7 2.7",
 "phi: flat -3.14159 3.14159"
 ]
