#--------------------------------------------------------------
# MC12 common configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

#--------------------------------------------------------------
# Configuration for Charybdis2
#--------------------------------------------------------------
evgenConfig.description = "Charybdis2 + Pythia8 with the AU2 MSTW2008LO tune, BH1_n2_Mth5_5_MD2_0"
evgenConfig.keywords = ["charybdis2"]
evgenConfig.generators += ["Lhef","Charybdis2"]
evgenConfig.inputfilecheck = "Charybdis2"
