###############################################################
#
# Job options file
# Pythia8 Z'->emu
# October 2012
#===============================================================

evgenConfig.description = "Zprime(1600)->emu production with the AU2 MSTW2008LO tune"
evgenConfig.contact = [ "skyler.kasko@cern.ch" ]
evgenConfig.keywords = [ "e", "mu", "exotics", "Zprime" ]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",
				"Zprime:gmZmode = 3",
				"32:m0 = 1600.0", # set Z' mass
				"32:8:products = 11 -13",
				"32:10:products = 13 -11",
				"32:0:onMode = 0",
				"32:1:onMode = 0",
				"32:2:onMode = 0",
				"32:3:onMode = 0",
				"32:4:onMode = 0",
				"32:5:onMode = 0",
				"32:6:onMode = 0",
				"32:7:onMode = 0",
				"32:9:onMode = 0",
				"32:11:onMode = 0",
				"32:12:onMode = 0"]


#==============================================================
#
# End of job options file
#
###############################################################
