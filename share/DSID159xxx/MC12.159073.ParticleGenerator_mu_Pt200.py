evgenConfig.description = "Single electrons with PT=200 GeV"
evgenConfig.keywords = ["singleparticle", "mu"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -13 13",
 "pt: constant 200000",
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159"
 ]
