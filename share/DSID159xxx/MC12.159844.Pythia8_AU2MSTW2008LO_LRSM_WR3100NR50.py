evgenConfig.keywords = [ "LRSM","heavy neutrino","WR" ]
evgenConfig.contact  = [ "kirill.skovpen@cern.ch" ]
evgenConfig.description = "pp->WR(3100)->lN(50)->lljj; l=e,mu,tau; no N mixing"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += [
                            "LeftRightSymmmetry:ffbar2WR = on",
                            "9900024:m0 = 3100",
                            "9900012:m0 = 50",
                            "9900014:m0 = 50",
                            "9900016:m0 = 50",
                            "9900024:onMode = off",
                            "9900024:9:products = -11 9900012",
                            "9900024:10:products = -13 9900014",
                            "9900024:11:products = -15 9900016",
                            "9900024:9:onMode = on",
                            "9900024:10:onMode = on",
                            "9900024:11:onMode = on"
                            ]
