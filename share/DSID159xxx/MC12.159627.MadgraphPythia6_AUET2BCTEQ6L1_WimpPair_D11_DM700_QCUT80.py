evgenConfig.description = "WIMP Model generation with MadGraph/Pythia6 in MC12"
evgenConfig.keywords = ["WimpPair"]
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.contact = ["sschramm@cern.ch", "david.berge@cern.ch"]

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1",     # Changes listing of particles in log file
                          	 "pyinit pylistf 1",      # Changes listing of particles in log file
                                 "pyinit dumpr 1 2",
				]            


evgenConfig.inputfilecheck = 'WimpPair_D11_DM700_QCUT80'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0 
      showerkt=T
      qcut=80
      imss(21)=24
      imss(22)=24  
    """
    
phojf.write(phojinp)
phojf.close()
