#-----------------------------------------------
# Pythia photon+jet w/ p_T(photon) > 140 GeV
# Prepared by M. Relich March 2012
#-----------------------------------------------

# General Description
evgenConfig.description = "Pythia8 gamma+jet performance sample with photon pT > 140 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets"]

# Include Pythia 8 information
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

# Configure Pythia
topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 135"]


# Setup filter
include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 140000.
