include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.description = "Charybdis2 + Pythia8 with the AU2 MSTW2008LO tune, BH4_n2_Mth4_5_MD4_0"
evgenConfig.keywords = ["charybdis2"]
evgenConfig.generators += ["Lhef","Charybdis2"]
evgenConfig.inputfilecheck = "Charybdis2"
