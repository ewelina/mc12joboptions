evgenConfig.description = "Single muons with 10<p<120 GeV and eta<1.7"
evgenConfig.keywords = ["singleparticle", "mu"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -13 13",
 "p: flat 10000 120000",
 "eta: flat -1.7 1.7",
 "phi: flat -3.14159 3.14159"
 ]
