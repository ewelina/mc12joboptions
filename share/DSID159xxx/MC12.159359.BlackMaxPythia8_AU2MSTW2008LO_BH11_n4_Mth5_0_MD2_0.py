#--------------------------------------------------------------
# MC12 common configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

#--------------------------------------------------------------
# Configuration for BlackMax
#--------------------------------------------------------------
evgenConfig.description = "BlackMax + Pythia8 with the AU2 MSTW2008LO tune, BH11: n=4, Mp=2.0 TeV, Mth=5.0TeV"
evgenConfig.keywords = ["blackhole"]
evgenConfig.generators += ["Lhef","BlackMax"]
evgenConfig.inputfilecheck = "BlackMax"
