
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.description = "charybdis2 test"
evgenConfig.keywords = ["charybdis2"]
evgenConfig.generators += ["Lhef","Charybdis2"]
evgenConfig.inputfilecheck = "Charybdis2"
