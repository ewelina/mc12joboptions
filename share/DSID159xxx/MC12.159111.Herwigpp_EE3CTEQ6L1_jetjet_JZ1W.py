## Job options file for Herwig++, QCD jet slice production
## Andy Buckley, April '10
## Lily Asquith, June '12

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ1W with EE3 tune"
evgenConfig.keywords = ["QCD", "jets"]

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 10*GeV
"""

#cmds = """\
#insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
#set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV
#set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
#set /Herwig/Cuts/QCDCuts:X1Min 0.01
#set /Herwig/Cuts/QCDCuts:X2Min 0.01
#set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
#"""

topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/JetFilter_JZ1W.py")
evgenConfig.minevents = 1000
