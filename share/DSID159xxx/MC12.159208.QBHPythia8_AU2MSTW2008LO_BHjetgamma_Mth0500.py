evgenConfig.description = "Quantum black holes (M_th = 0500 GeV) decaying to one photon and one jet."
evgenConfig.contact = ["gingrich@ualberta.ca", "areinsch@uoregon.edu"]
evgenConfig.keywords = ["exotics", "blackholes", "gamma", "jets"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py" )
