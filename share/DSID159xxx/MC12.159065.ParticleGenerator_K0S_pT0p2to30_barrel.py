evgenConfig.description = "Single K^0_S with Pt=0.2-30GeV Eta=0.2-0.3"
evgenConfig.keywords = ["singleparticle", "K0S"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 310",
 "pt: flat 200.0 30000.",
 "eta: flat 0.2 0.3",
 "phi: flat -3.14159 3.14159"
 ]
