################################################################
# Job options file
# Pythia8 Leptoquark -> cmu cmu (700 GeV)
# Contact: Giovanni Siragusa (siragusa@cern.ch)
#==============================================================
evgenConfig.description = "Leptoquark (M = 700 GeV) to di-muon jet channel"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:qqbar2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:kCoup = 0.01"]
topAlg.Pythia8.Commands += ["42:0:products = 4 13"]
topAlg.Pythia8.Commands += ["42:m0 = 700"]
topAlg.Pythia8.Commands += ["42:mMin = 500"]
topAlg.Pythia8.Commands += ["42:mMax = 900"]
