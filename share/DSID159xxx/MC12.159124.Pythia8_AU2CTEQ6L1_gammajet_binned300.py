evgenConfig.description = "Pythia8 gamma+jet performance sample with photon pT > 300 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets"]
evgenConfig.contact = ["Matt Relich"]

## Configure Pythia
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PhaseSpace:pTHatMin = 250"]

# Set up photon filter
include("MC12JobOptions/LeadingPhotonFilter.py")
topAlg.LeadingPhotonFilter.PtMin = 300000.
