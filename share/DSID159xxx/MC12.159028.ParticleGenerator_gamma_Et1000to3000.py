evgenConfig.description = "Single photons with flat ET in [1-3] TeV"
evgenConfig.keywords = ["singleparticle", "gamma"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE 
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 22",
 "et: flat 1000000 3000000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
