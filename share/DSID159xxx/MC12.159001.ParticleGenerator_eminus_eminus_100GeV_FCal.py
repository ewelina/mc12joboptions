evgenConfig.description = "Electrons into the FCal"
evgenConfig.keywords = ["two_singleparticle", "e"]
evgenConfig.contact  = ["ZLMarshall@lbl.gov"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = INFO
topAlg.ParticleGenerator.orders = [
 "pdgcode[0]: constant 11",
 "e[0]: 100000",
 "eta[0]: flat 3.2 4.9",
 "phi[0]: flat -3.1415926 3.1415926",
 "pdgcode[1]: constant 11",
 "e[1]: 100000",
 "eta[1]: flat -4.9 -3.2",
 "phi[1]: flat -3.1415926 3.1415926",
 ]

