evgenConfig.description = "Single electrons with flat ET in [7-80] GeV and flat x, y vertex [-20..20] mm"
evgenConfig.keywords = ["singleparticle", "e"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "et: flat 7000 80000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159",
 "vertx: flat -20 20",
 "verty: flat -20 20"
 ]

