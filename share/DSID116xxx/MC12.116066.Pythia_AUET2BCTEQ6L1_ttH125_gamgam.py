evgenConfig.description = "POWHEG+PYTHIA ttH H->gg with AUET2B,CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ttH", "gamma"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

Pythia.PythiaCommand += [
     "pysubs msel 0",
     "pypars mstp 7 6",
     "pysubs msub 121 1",
     "pysubs msub 122 1",
     "pydat2 pmas 25 1 125",
     "pydat2 pmas 25 2 0.00407",
     "pydat3 mdme 210 1 0",
     "pydat3 mdme 211 1 0",
     "pydat3 mdme 212 1 0",
     "pydat3 mdme 213 1 0",
     "pydat3 mdme 214 1 0",
     "pydat3 mdme 215 1 0",
     "pydat3 mdme 216 1 -1",
     "pydat3 mdme 217 1 -1",
     "pydat3 mdme 218 1 0",
     "pydat3 mdme 219 1 0",
     "pydat3 mdme 220 1 0",
     "pydat3 mdme 221 1 -1",
     "pydat3 mdme 222 1 0",
     "pydat3 mdme 223 1 1", #H-> GamGam
     "pydat3 mdme 224 1 0",
     "pydat3 mdme 225 1 0",
     "pydat3 mdme 226 1 0",
     "pydat1 parj 90 20000",
     "pydat3 mdcy 15 1 0" ]

