evgenConfig.description = "POWHEG+PYTHIA6 ggH H->gg with AUET2B,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "gamma"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
evgenConfig.generators += ["Lhef"]

Pythia.PythiaCommand += [   "pydat3 mdme 210 1 0", # Higgs decay
                            "pydat3 mdme 211 1 0",
                            "pydat3 mdme 212 1 0",
                            "pydat3 mdme 213 1 0",
                            "pydat3 mdme 214 1 0",
                            "pydat3 mdme 215 1 0",
                            "pydat3 mdme 218 1 0",
                            "pydat3 mdme 219 1 0",
                            "pydat3 mdme 220 1 0",
                            "pydat3 mdme 222 1 0",
                            "pydat3 mdme 223 1 1",
                            "pydat3 mdme 224 1 0",
                            "pydat3 mdme 225 1 0",
                            "pydat3 mdme 226 1 0" ]
