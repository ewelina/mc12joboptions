evgenConfig.description = "ALPGEN+Jimmy ttbb inclusive process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW","jets","ttbar","bottom"]
evgenConfig.inputfilecheck = "ttbbinclNp0"
evgenConfig.minevents = 500

include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
