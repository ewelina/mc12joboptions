# NUHM with neutralino LSP model
# Paper: arXiv:hep-ph/0210205v2
# Title: Exploration of the MSSM with Non-Universal Higgs Masses
# Authors: J. Ellis , T. Falk, K.A. Olive, Y. Santoso

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

slha_file = "susy_NUHM_700_400_10_2000.slha"
topAlg.Pythia.SusyInputFile = slha_file

topAlg.Pythia.PythiaCommand += ["pysubs msel 39",     # Include all production channels
								"pymssm imss 1 11",   # Read mass spectrum from LesHouches file
								"pymssm imss 11 0"]   # Neutralino LSP

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

evgenConfig.contact = [ "Judita.Mamuzic@cern.ch" ]
evgenConfig.description = "PYTHIA 6 generation of Non universal Higgs Masses model with neutralino LSP"
evgenConfig.process = "PYTHIA 6 supprots three body decays using all production channels and no generator filters"
evgenConfig.keywords = ["SUSY", "NUHM", "neutralino", "lsp"]
evgenConfig.auxfiles += [ slha_file ]

