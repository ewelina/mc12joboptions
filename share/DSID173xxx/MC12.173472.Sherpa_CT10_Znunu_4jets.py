include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> vv + 4 to 5 jets using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = [ "Z", "nunu", "4jets" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "christopher.young@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 91 91 93 93 93 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 91 91 1.7 E_CMS
}(selector)

"""

topAlg.Sherpa_i.ResetWeight = 0
