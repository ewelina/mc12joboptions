# SUSY Herwig++ jobOptions for stop2 pair production grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop2')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_T2t2_Ttwo800_Tone450_L270.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['stop2'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'stop2 simplified grid generation' 
evgenConfig.keywords = ['SUSY','stop2']
evgenConfig.contact  = ['Joaquin.Poveda@cern.ch','Joao.Costa@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

# standard dilepton filter with pT >10 and |eta| < 2.8
include('MC12JobOptions/MultiLeptonFilter.py')

#==============================================================
#
# End of job options file
#
###############################################################
