include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W -> ev + at least 3 jets (up to 4) using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = [ "W", "e" ]
evgenConfig.inputconfcheck = "Wenu"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(processes){
  Process 93 93 -> 90 91 93 93 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 90 91 1.7 E_CMS
}(selector)

(run){
  MASSIVE[13]=1
  MASSIVE[15]=1
}(run)
"""

