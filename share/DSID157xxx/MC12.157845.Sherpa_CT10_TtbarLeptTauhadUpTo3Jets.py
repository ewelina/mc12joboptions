include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbar + up to 3 jets in ME+PS. Decay modes: lept tau(had)."
evgenConfig.keywords = [ "ttbar","up3jets" ]
evgenConfig.contact  = [ "iacopo.vivarelli@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  # l tau
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> 90 91
  Decay -24[d] -> 15 -16
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process
  # tau l
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> -15 16
  Decay -24[d] -> 90 91
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10};
  End process
}(processes)

(selector){
  DecayMass 24 40.0 120.0
  DecayMass -24 40.0 120.0
  DecayMass 6 150.0 200.0
  DecayMass -6 150.0 200.0
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.Parameters += [ "DECAYFILE=HadronDecaysTauH.dat" ]
topAlg.Sherpa_i.CrossSectionScaleFactor=0.648

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
