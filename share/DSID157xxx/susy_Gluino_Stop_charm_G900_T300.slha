#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.46589425E+03
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.34        # version number                      
#
BLOCK MODSEL  # Model selection
     1     1   # mSUGRA                                            
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72500000E+02   # mt pole mass
         7     1.77710000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     1.00000000E+02   # m0                  
         2     2.50000000E+02   # m1%2                
         3     1.00000000E+01   # tanbeta             
         4     1.00000000E+00   # sign(mu)            
         5    -1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     4.65894250E+02   # EWSB_scale          
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04847306E+01   # W+
        25     1.09944814E+02   # h
        35     3.95061603E+02   # H
        36     3.94652125E+02   # A
        37     4.03076945E+02   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.17765846E+03   # ~d_L
   2000001     5.15619556E+03   # ~d_R
   1000002     5.12260526E+03   # ~u_L
   2000002     5.15890743E+03   # ~u_R
   1000003     5.17765846E+03   # ~s_L
   2000003     5.15619556E+03   # ~s_R
   1000004     5.12260526E+03   # ~c_L
   2000004     5.15890743E+03   # ~c_R
   1000005     5.16907880E+03   # ~b_1
   2000005     5.16235964E+03   # ~b_2
   1000006     3.00000000E+02   # ~t_1
   2000006     5.16526704E+03   # ~t_2
   1000011     5.00783618E+03   # ~e_L
   2000011     5.12821157E+03   # ~e_R
   1000012     5.14863960E+03   # ~nu_eL
   1000013     5.00783618E+03   # ~mu_L
   2000013     5.12821157E+03   # ~mu_R
   1000014     5.14863960E+03   # ~nu_muL
   1000015     4.33337347E+03   # ~tau_1
   2000015     5.04807298E+03   # ~tau_2
   1000016     4.83975742E+03   # ~nu_tauL
   1000021     9.00000000E+02   # ~g
   1000022     2.80000000E+02   # ~chi_10
   1000023     4.80301328E+03   # ~chi_20
   1000025    -5.10584586E+03   # ~chi_30
   1000035     5.17904756E+03   # ~chi_40
   1000024     4.79676634E+03   # ~chi_1+
   1000037     5.18107884E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.85365149E-01   # N_11
  1  2    -5.63708172E-02   # N_12
  1  3     1.50968191E-01   # N_13
  1  4    -5.55559078E-02   # N_14
  2  1     1.06011099E-01   # N_21
  2  2     9.39731843E-01   # N_22
  2  3    -2.80737798E-01   # N_23
  2  4     1.63865793E-01   # N_24
  3  1     6.12646703E-02   # N_31
  3  2    -9.07084513E-02   # N_32
  3  3    -6.95185707E-01   # N_33
  3  4    -7.10447359E-01   # N_34
  4  1     1.18590934E-01   # N_41
  4  2    -3.24805127E-01   # N_42
  4  3    -6.44291647E-01   # N_43
  4  4     6.82148146E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.11526811E-01   # U_11
  1  2     4.11240652E-01   # U_12
  2  1     4.11240652E-01   # U_21
  2  2     9.11526811E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.70474216E-01   # V_11
  1  2     2.41204883E-01   # V_12
  2  1     2.41204883E-01   # V_21
  2  2     9.70474216E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     5.52971576E-01   # cos(theta_t)
  1  2     8.33200118E-01   # sin(theta_t)
  2  1    -8.33200118E-01   # -sin(theta_t)
  2  2     5.52971576E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.30133242E-01   # cos(theta_b)
  1  2     3.67222211E-01   # sin(theta_b)
  2  1    -3.67222211E-01   # -sin(theta_b)
  2  2     9.30133242E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     2.84520650E-01   # cos(theta_tau)
  1  2     9.58669912E-01   # sin(theta_tau)
  2  1    -9.58669912E-01   # -sin(theta_tau)
  2  2     2.84520650E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.14169459E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  4.65894250E+02  # DRbar Higgs Parameters
         1     3.52307595E+02   # mu(Q)               
         2     9.75127163E+00   # tanbeta(Q)          
         3     2.45019343E+02   # vev(Q)              
         4     1.62474709E+05   # MA^2(Q)             
#
BLOCK GAUGE Q=  4.65894250E+02  # The gauge couplings
     1     3.60962223E-01   # gprime(Q) DRbar
     2     6.46342415E-01   # g(Q) DRbar
     3     1.09643637E+00   # g3(Q) DRbar
#
BLOCK AU Q=  4.65894250E+02  # The trilinear couplings
  1  1    -6.83371102E+02   # A_u(Q) DRbar
  2  2    -6.83371102E+02   # A_c(Q) DRbar
  3  3    -5.06244568E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  4.65894250E+02  # The trilinear couplings
  1  1    -8.59260696E+02   # A_d(Q) DRbar
  2  2    -8.59260696E+02   # A_s(Q) DRbar
  3  3    -7.96839768E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  4.65894250E+02  # The trilinear couplings
  1  1    -2.53317942E+02   # A_e(Q) DRbar
  2  2    -2.53317942E+02   # A_mu(Q) DRbar
  3  3    -2.51561628E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.79078600E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.39555961E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  4.65894250E+02  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.01153117E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  4.65894250E+02  # The soft SUSY breaking masses at the scale Q
         1     1.01473112E+02   # M_1                 
         2     1.91554089E+02   # M_2                 
         3     5.86419847E+02   # M_3                 
        21     3.23204645E+04   # M^2_Hd              
        22    -1.25091441E+05   # M^2_Hu              
        31     1.95452732E+02   # M_eL                
        32     1.95452732E+02   # M_muL               
        33     1.94612848E+02   # M_tauL              
        34     1.35952517E+02   # M_eR                
        35     1.35952517E+02   # M_muR               
        36     1.33481225E+02   # M_tauR              
        41     5.45693259E+02   # M_q1L               
        42     5.45693259E+02   # M_q2L               
        43     4.97700412E+02   # M_q3L               
        44     5.27681504E+02   # M_uR                
        45     5.27681504E+02   # M_cR                
        46     4.23537258E+02   # M_tR                
        47     5.25587342E+02   # M_dR                
        48     5.25587342E+02   # M_sR                
        49     5.22280285E+02   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.44629811E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     1.12722419E+01   # gluino decays
#          BR         NDA      ID1       ID2
     5.00000000E-01    2     1000006        -6   # BR(~g -> ~t_1  tb)
     5.00000000E-01    2    -1000006         6   # BR(~g -> ~t_1* t )
#
#         PDG            Width
DECAY   1000006     5.74639546E-10   # stop1 decays
#          BR         NDA      ID1       ID2
     1.00000000E-00    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
#     9.91278492E-01    2     1000022         4   # BR(~t_1 -> ~chi_10 c )
#     7.64245643E-03    2     1000022         2   # BR(~t_1 -> ~chi_10 u )
#           BR         NDA      ID1       ID2       ID3       ID4
#     1.07905140E-03    4     1000022         5   3000001   4000001   # BR(~t_1 -> chi_10 b f fbarprime)
#
#         PDG            Width
DECAY   2000006     1.02439294E+04   # stop2 decays
#          BR         NDA      ID1       ID2
     2.44792447E-04    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.13996500E-05    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.43860140E-05    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.91620452E-02    2     1000021         6   # BR(~t_2 -> ~g      t )
     5.30731377E-06    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.91852173E-06    2     1000006        35   # BR(~t_2 -> ~t_1    H )
     1.05455822E-05    2     1000006        36   # BR(~t_2 -> ~t_1    A )
     9.70529605E-01    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
#
#         PDG            Width
DECAY   1000005     2.56603393E+04   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.87851552E-05    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.26084328E-05    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.72489951E-08    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.85141544E-05    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     1.19645800E-02    2     1000021         5   # BR(~b_1 -> ~g      b )
     4.82603943E-06    2     1000006       -37   # BR(~b_1 -> ~t_1    H-)
     9.87930669E-01    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
#
#         PDG            Width
DECAY   2000005     4.24581191E+03   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.35432482E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.98960155E-06    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.42891439E-07    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     7.81446019E-06    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.22100218E-02    2     1000021         5   # BR(~b_2 -> ~g      b )
     9.06593815E-06    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
     9.27231533E-01    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
#
#         PDG            Width
DECAY   1000002     3.05849175E+02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.08178464E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.54529395E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.53506744E-07    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     3.25601534E-03    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.94116653E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.17593960E+02   # sup_R decays
#          BR         NDA      ID1       ID2
     3.49882125E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.18205308E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.96630395E-07    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     9.64999670E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     3.10583040E+02   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.83572871E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.77275197E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.19517188E-06    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     3.57008927E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.90819235E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.09074212E+02   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.98339128E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.00214175E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.15908541E-08    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.91013535E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.05849175E+02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.08178464E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.54529395E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.53506744E-07    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     3.25601534E-03    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.94116653E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.17593960E+02   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.49882125E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.18205308E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.96630395E-07    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     9.64999670E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     3.10583040E+02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.83572871E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.77275197E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.19517188E-06    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     3.57008927E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.90819235E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.09074212E+02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.98339128E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.00214175E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.15908541E-08    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.91013535E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     5.41400619E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     9.31878727E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.46264459E-02    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     4.34948272E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     2.56641626E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.99824130E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.75575355E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     2.94537112E-07    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     5.41400619E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     9.31878727E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.46264459E-02    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     4.34948272E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.56641626E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.99824130E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.75575355E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     2.94537112E-07    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.04499189E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
#
#         PDG            Width
DECAY   2000015     9.41435628E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     6.91641085E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.78170994E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.07464891E-03    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.33794268E-01    2     1000016       -24   # BR(~tau_2 -> ~nu_tauL W-)
     5.39289477E-06    2     1000015        25   # BR(~tau_2 -> ~tau_1   h)
     1.47266250E-06    2     1000015        35   # BR(~tau_2 -> ~tau_1   H)
     2.15573810E-06    2     1000015        36   # BR(~tau_2 -> ~tau_1   A)
     5.92176244E-01    2     1000015        23   # BR(~tau_2 -> ~tau_1   Z)
#
#         PDG            Width
DECAY   1000012     8.80964309E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     8.88561137E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.17015132E-02    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.03863231E-05    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     7.97269640E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     8.80964309E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     8.88561137E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.17015132E-02    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.03863231E-05    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     7.97269640E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     5.15485825E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.42633180E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.92156138E-05    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.29455293E-04    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     4.98029162E-06    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
     8.57063168E-01    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
#
#         PDG            Width
DECAY   1000024     9.65563920E+02   # chargino1+ decays
#          BR         NDA      ID1       ID2
     4.05071074E-02    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
     7.19434832E-05    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)
     9.57842683E-01    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
     1.57826576E-03    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000037     6.23794217E+02   # chargino2+ decays
#          BR         NDA      ID1       ID2
     1.42223270E-05    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     1.91460366E-06    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     1.42223270E-05    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     1.91460366E-06    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     9.01363380E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     4.33608842E-06    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     3.12906090E-07    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     3.12906090E-07    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.37961210E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     2.52395240E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     2.52395240E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     1.21567215E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     1.67643917E-05    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.92492453E-02    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     8.64228790E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.07273390E-02    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.31775859E-03    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     2.19103781E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     7.98643731E+01   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.98618577E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     4.61081387E-03    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
     1.11342638E-02    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
     1.09237932E-02    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
     2.36806790E-01    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
     2.36806790E-01    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
     5.49485738E-04    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     5.49485738E-04    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
#
#         PDG            Width
DECAY   1000025     1.03507266E+03   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.71256425E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     5.41272867E-03    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     7.07980991E-03    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     7.07980991E-03    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.38135279E-03    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     1.82628396E-03    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
     1.42545833E-03    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
     1.11412991E-06    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     5.22523232E-02    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
     5.22523232E-02    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
     4.72915100E-08    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
     4.72915100E-08    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
     4.72915100E-08    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
     4.72915100E-08    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
     1.42431460E-05    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
     1.42431460E-05    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
     1.99619838E-07    2     2000015       -15   # BR(~chi_30 -> ~tau_2-   tau+)
     1.99619838E-07    2    -2000015        15   # BR(~chi_30 -> ~tau_2+   tau-)
     1.64823728E-06    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.64823728E-06    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     2.66537952E+02   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.97012870E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     2.42351190E-03    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.66261212E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     4.66261212E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     8.16148105E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.59269271E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.87319507E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.87234498E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.50298196E-05    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.50298196E-05    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     1.26242447E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     1.26242447E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.27613251E-06    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.27613251E-06    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.60469516E-07    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.60469516E-07    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.50298196E-05    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.50298196E-05    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     1.26242447E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     1.26242447E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.27613251E-06    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.27613251E-06    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.60469516E-07    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.60469516E-07    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.41674817E-01    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.41674817E-01    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     1.38374163E-07    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.38374163E-07    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     6.41943766E-07    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     6.41943766E-07    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     1.14132754E-05    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.14132754E-05    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     2.70319163E-07    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     2.70319163E-07    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.14132754E-05    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.14132754E-05    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     2.70319163E-07    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     2.70319163E-07    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.14997829E-05    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.14997829E-05    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.12999059E-05    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.12999059E-05    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     8.46247819E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     8.46247819E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     8.46247819E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     8.46247819E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     9.91496459E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     9.91496459E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
