evgenConfig.description = "Herwig WW in taunuqq final state"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson", "tau"]

include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12800",
                                "modbos 1 1",
                                "modbos 2 4"] # note that 10000 is added to the herwig process number

# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

# Add the filters:



