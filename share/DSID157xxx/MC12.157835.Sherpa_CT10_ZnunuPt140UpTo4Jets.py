include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z -> vv + up to 4 jets with pT_Z>140 GeV."
evgenConfig.keywords = ["Z","nu"]
evgenConfig.inputconfcheck = "Znunu"
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 91 91 93 93{3}
  Order_EW 2
  CKKW sqr(30/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  #Enhance_Factor 4.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 91 91 1.7 E_CMS
  PT2  91 91 140.0 E_CMS
}(selector)
"""
