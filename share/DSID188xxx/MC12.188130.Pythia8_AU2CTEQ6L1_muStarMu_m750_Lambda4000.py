
evgenConfig.description = "Excited   muStarMu  mStar=750 Laambda=4000, with the AU2 CTEQ6L1 tune"
evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000013

# Excited lepton Mass (in GeV)
M_ExNu = 750.

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 4000.

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNue.py")

