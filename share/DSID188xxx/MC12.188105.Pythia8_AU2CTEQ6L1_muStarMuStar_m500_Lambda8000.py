
evgenConfig.description = "Excited   muStarMuStar  mStar=500 Laambda=8000, with the AU2 CTEQ6L1 tune"
evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000013

# Excited lepton Mass (in GeV)
M_ExNu = 500.

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 8000.

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNueNue.py")

