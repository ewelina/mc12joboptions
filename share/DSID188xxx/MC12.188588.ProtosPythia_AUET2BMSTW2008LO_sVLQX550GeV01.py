evgenConfig.description = "Vector-like single T5/3 production, mass 550GeV, mixing 0.1, XT doublet, with Protos+Pythia6, tune AUET2B MSTW2008LO, DSID188588"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","heavyquark","4thgen","VLQ"]
evgenConfig.contact  = ["takuya.tashiro@cern.ch"]
evgenConfig.inputfilecheck = "protos"

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

