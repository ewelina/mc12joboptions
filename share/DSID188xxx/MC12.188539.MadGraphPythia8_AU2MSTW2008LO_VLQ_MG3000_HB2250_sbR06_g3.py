#  choose the pdf
# ---------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.contact = ["Frederick.Dallaire@cern.ch"]
evgenConfig.description = "VLQ G* -> (B->((h->bb)+b)+b, M(G*)=3000 GeV, M(B)=2250 GeV, LHEF produced with MadGraph5 v1.5.10"
evgenConfig.keywords = ["VLQ single production"]
evgenConfig.inputfilecheck = 'VLQ'
