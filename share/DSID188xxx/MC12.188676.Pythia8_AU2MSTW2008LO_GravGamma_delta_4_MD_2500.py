evgenConfig.description = "photon + ADD Graviton"
evgenConfig.keywords = ["Exotics", "Graviton"]
evgenConfig.contact = ["Ning Zhou"]

include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )

topAlg.Pythia8.Commands += [ 'ExtraDimensionsLED:ffbar2Ggamma = on',    # Process type.
                             'ExtraDimensionsLED:n = 4',               # Number of extra dimensions.
                             'ExtraDimensionsLED:MD = 2500',           # Choice of the scale, MD.
                             'ExtraDimensionsLED:CutOffmode = 0',      # Treatment of the effective theory (0: all the events. 1 : truncate events with s_hat>MD^2, with a weight of MD^2/s_hat^4).
                             'PhaseSpace:pTHatMin = 80.',              # pT Cut at the generator level.
                             'SigmaProcess:factorScale2 = 3',          # Choice of factorization scale (3: the arithmetic mean of the squared transverse masses of the two outgoing particles, default in pythia6)
                             'SigmaProcess:renormScale2 = 3',          # Choice of renormalization scale (3: the arithmetic mean of the squared transverse masses of the two outgoing particles, default in pythia6)
                             '5000039:m0 = 2000.',                     # Central value of the Breit-Wigner mass resonance
                             '5000039:mWidth = 1000.',                 # Resonance width
                             '5000039:mMin = 1.',                      # Minimum mass of the Breit-Wigner distribution.
                             '5000039:mMax = 13990.',                  # Maximum mass of the Breit-Wigner distribution.
                             'PDF:pSet = 5' ]                          # MSTW2008LO
                             
