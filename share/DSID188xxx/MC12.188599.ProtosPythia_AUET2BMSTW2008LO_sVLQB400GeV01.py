evgenConfig.description = "Vector-like single B production, singlet, M=400GeV, with Protos22+Pythia6, tune AUET2B MSTW2008LO, with mixing 0.1"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","heavyquark","4thgen","VLQ"]
evgenConfig.contact  = ["xiedebao@googlemail.com"]
evgenConfig.inputfilecheck = "sVLQB400GeV01"

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
