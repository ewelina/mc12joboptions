evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]
evgenConfig.description = "Z'->ee production"
evgenConfig.keywords = ["HeavyBoson", "Zprime", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# Zprime resonance mass (in GeV)
ZprimeMass = 3000
ckin1 = 1000
ckin2 = 1500

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",    # create Z' bosons
                            "Zprime:gmZmode = 0",                    # Z',Z,g with interference
                            "Zprime:vd = 0.0",
                            "Zprime:ad = 0.505525029603",
                            "Zprime:vu = 0.0",
                            "Zprime:au = 0.505525029603",
                            "Zprime:ve = 0.0",
                            "Zprime:ae = 0.505525029603",
                            "Zprime:vnue = 0.252762514802",
                            "Zprime:anue = 0.252762514802",
                            "PhaseSpace:mHatMin = "+str(ckin1), # lower invariant mass
                            "PhaseSpace:mHatMax = "+str(ckin2), # upper invariant mass
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny  = 11", # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]
