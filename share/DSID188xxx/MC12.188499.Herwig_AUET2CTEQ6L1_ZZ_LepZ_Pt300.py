evgenConfig.description = "Herwig ZZ (Z->all, Z->ee/mumu) with pT(Z) > 300 GeV"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson","ZZ","semi-leptonic"]
evgenConfig.contact  = ["koji.terashi@cern.ch"]
#
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")

topAlg.Herwig.HerwigCommand += ["iproc 12810",  # note that 10000 is added to the herwig process number
                                "modbos 1 5",   # Z->all, Z->ee/mumu
                                "ptmin  300",
                                "effmin 1e-8"]

# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )

# Add the filters:
# Electron or Muon filter
include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8
