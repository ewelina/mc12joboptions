include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "T5/3 single prod at 8TeV MadGraph5+PYTHIA8 for M=600 GeV and lambda=0.5"
evgenConfig.contact = ["romain.kukla@cern.ch"]
evgenConfig.keywords = ["T5/3", "exotics", "top"]
evgenConfig.generators = ["MadGraph","Pythia8"]
evgenConfig.inputfilecheck = "T53_SP_600_05"
