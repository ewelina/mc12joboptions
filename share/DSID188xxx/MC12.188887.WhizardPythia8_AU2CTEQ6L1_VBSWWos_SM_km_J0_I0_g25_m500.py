############################################################################################################
# Whizard lhef showered with Pythia8
# Ulrike Schnoor <ulrike.schnoor@cern.ch>
# Generators/MC12JobOptions/tags/MC12JobOptions-00-05-70/share/MC12.xxxxxx.LHEFPythia8_AU2CTEQ6L1_example.py
############################################################################################################                                                                       
evgenConfig.description = "Whizard+Pythia8  VBSos qq-->lvlvjj spin0 isospin0 g2.5 mass500"
evgenConfig.keywords = ["EW"]
evgenConfig.inputfilecheck = "VBSWWos_SM_km_J0_I0_g25_m500"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.contact = ["Jiaming Yu <jiamingy@umich.edu>"]

evgenConfig.generators = ["Whizard", "Pythia8" ]
