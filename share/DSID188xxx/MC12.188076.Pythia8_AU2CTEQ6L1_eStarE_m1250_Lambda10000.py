
evgenConfig.description = "Excited   eStarE  mStar=1250 Laambda=10000, with the AU2 CTEQ6L1 tune"
evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000011

# Excited lepton Mass (in GeV)
M_ExNu = 1250.

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 10000.

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNue.py")

