evgenConfig.description = "Herwig WZ (W->all, Z->ee/mumu) with pT(W/Z) > 150 GeV"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson","WZ","semi-leptonic"]
evgenConfig.contact  = ["koji.terashi@cern.ch"]
#
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")

topAlg.Herwig.HerwigCommand += ["iproc 12820",  # note that 10000 is added to the herwig process number
                                "modbos 2 5",   # W->all, Z->ee/mumu
                                "ptmin  150",
                                "effmin 1e-8"]

# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )

# Add the filters:
# Electron or Muon filter
include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8
