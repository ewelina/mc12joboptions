evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.description = "Stealth SUSY singlino to singlet, mg1200"
evgenConfig.keywords = ["stealth","SUSY","singlino"]
evgenConfig.inputfilecheck = "stealth"
evgenConfig.contact = ["hrussell@cern.ch"]
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
from TruthExamples.TruthExamplesConf import TestHepMC
topAlg += TestHepMC()
TestHepMC.MaxTransVtxDisp = 200000 #in mm
TestHepMC.MaxTransVtxDispLoose = 300000 #in mm
TestHepMC.MaxVtxDisp = 500000 #in mm
topAlg.Pythia8.Commands += ["SLHA:useDecayTable = on", 
                            "SLHA:keepSM =off",
                            "SLHA:minMassSM = 0."]

if not hasattr(topAlg, "DecayPositionFilter"):
    from GeneratorFilters.GeneratorFiltersConf import DecayPositionFilter
    topAlg += DecayPositionFilter()

if "DecayPositionFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["DecayPositionFilter"]

topAlg.DecayPositionFilter.RCut = 400.
topAlg.DecayPositionFilter.PDG = 3000001
topAlg.DecayPositionFilter.MinPass = 2
