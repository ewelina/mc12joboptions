LQmass = 900

evgenConfig.description = "Leptoquark (M = "+str(LQmass)+" GeV) to muon(minus)+MET+2jets channel, AU2 tune and CTEQ6L1 PDF"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["ilias.panagoulias@cern.ch"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on",
                            "LeptoQuark:qqbar2LQLQbar = on",
                            "LeptoQuark:kCoup = 0.01",
                            "42:0:all = 2 0.5 0 4 13",
                            "42:addChannel = 3 0.5 0 3 14",
                            "42:m0 = "+ str(LQmass),
                            "42:mMin ="+ str(LQmass-200),
                            "42:mMax ="+ str(LQmass+200)]
