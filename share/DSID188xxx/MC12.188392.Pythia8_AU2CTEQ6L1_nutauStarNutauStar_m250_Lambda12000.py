
evgenConfig.description = "Excited   nutauStarNutauStar  mStar=250 Laambda=12000, with the AU2 CTEQ6L1 tune"
evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000016

# Excited lepton Mass (in GeV)
M_ExNu = 250.

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 12000.

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNueNue.py")

