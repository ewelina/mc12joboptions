
MASS=400
MASSX=200
CASE='stop'
DECAY='false'
MODEL='generic'
STOPPED = 'true'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic stop 400GeV 200GeV stopped"
evgenConfig.specialConfig = "CASE={case};CHARGE=999;DECAY={decay};MASS={mass};MASSX={massx};MODEL={model};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(case=CASE, decay=DECAY, mass=MASS, massx=MASSX, model=MODEL)

