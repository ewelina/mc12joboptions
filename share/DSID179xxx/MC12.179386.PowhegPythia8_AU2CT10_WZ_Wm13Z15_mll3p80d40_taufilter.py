## POWHEG+Pythia8 WZ
evgenConfig.description = "POWHEG+Pythia8 WZ production and AU2 CT10 tune, cut on all SFOS pair mll>2*m(l from Z)+250MeV, dilepton filter pt>5GeV, eta<2.7"
evgenConfig.keywords = ["EW", "diboson", "leptonic"]
evgenConfig.inputfilecheck = "Powheg_CT10.*.WZ_Wm13Z15_mll3p804d0"
evgenConfig.minevents  = 2000
evgenConfig.contact = ["Alexander Oh <alexander.oh@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# 2-lepton veto
if not hasattr(topAlg, "MultiLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter()
    
if "MultiLeptonFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs += ["MultiLeptonFilter"]
    
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

# 2-lepton with tau
include("MC12JobOptions/MultiElecMuTauFilter.py")
topAlg.MultiElecMuTauFilter.MinPt = 5.*GeV
topAlg.MultiElecMuTauFilter.MaxEta = 2.7
topAlg.MultiElecMuTauFilter.NLeptons = 2
topAlg.MultiElecMuTauFilter.MinVisPtHadTau = 15.*GeV
