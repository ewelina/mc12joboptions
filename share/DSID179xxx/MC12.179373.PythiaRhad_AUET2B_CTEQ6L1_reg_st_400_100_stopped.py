
MASS=400
MASSX=100
CASE='stop'
DECAY='false'
MODEL='regge'
STOPPED = 'true'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " regge stop 400GeV 100GeV stopped"
evgenConfig.specialConfig = "CASE={case};CHARGE=999;DECAY={decay};MASS={mass};MASSX={massx};MODEL={model};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(case=CASE, decay=DECAY, mass=MASS, massx=MASSX, model=MODEL)

