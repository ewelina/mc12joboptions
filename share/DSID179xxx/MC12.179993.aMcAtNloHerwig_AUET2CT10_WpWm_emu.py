evgenConfig.description = "aMcAtNlo+Herwig WW emu production"
evgenConfig.keywords = ["WW", "diboson", "leptonic"]
evgenConfig.contact  = ["Antoine Marzin <antoine.marzin@cern.ch>"]
evgenConfig.inputfilecheck = 'WpWm_emu'


include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
evgenConfig.generators += [ "aMcAtNlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
 
