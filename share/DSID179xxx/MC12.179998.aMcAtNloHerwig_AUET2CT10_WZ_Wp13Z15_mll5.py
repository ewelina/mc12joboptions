evgenConfig.description = "aMcAtNlo+Herwig WZ munu+tautau productionn m(ll) > 5 GeV"
evgenConfig.keywords = ["WZ", "diboson", "leptonic"]
evgenConfig.contact  = ["Antoine Marzin <antoine.marzin@cern.ch>"]
evgenConfig.inputfilecheck = 'WZ_Wp13Z15_mll5'


include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
evgenConfig.generators += [ "aMcAtNlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
 
