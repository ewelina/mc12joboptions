include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar radLo allhad with P2012 tune with ktfac=2.0 and 2 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "allhad"]
evgenConfig.inputconfcheck = "201442.ktfac2.0_CTEQ6L1_ttbar" 
evgenConfig.minevents = 2000
