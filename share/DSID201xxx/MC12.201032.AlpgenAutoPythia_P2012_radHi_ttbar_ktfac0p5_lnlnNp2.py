include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar radHi dileptonic with P2012 tune with ktfac=0.5 and 2 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.inputconfcheck = "201032.ktfac0.5_CTEQ6L1_ttbar"
evgenConfig.minevents = 2000
