include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar allhad with P2012 tune with 3 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "allhad"]
evgenConfig.inputconfcheck = "201423.CTEQ6L1_ttbar"
evgenConfig.minevents = 2000
