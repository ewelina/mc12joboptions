include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar radLo single lepton with P2012 tune with ktfac=2.0 and 4inc additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "singlelepton"]
evgenConfig.inputconfcheck = "201244.ktfac2.0_CTEQ6L1_ttbar"
evgenConfig.minevents = 500
