include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar radLo dileptonic with P2012 tune with ktfac=2.0 and 2 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic"]
evgenConfig.inputconfcheck = "201042.ktfac2.0_CTEQ6L1_ttbar"
evgenConfig.minevents = 2000
