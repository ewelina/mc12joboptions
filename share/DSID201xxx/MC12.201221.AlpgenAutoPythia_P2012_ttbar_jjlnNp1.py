include('MC12JobOptions/AlpgenControl_ttbar.py')
evgenConfig.description = "Alpgen+Pythia ttbar single lepton with P2012 tune with 1 additional jets"
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.keywords = ["top", "ttbar", "singlelepton"]
