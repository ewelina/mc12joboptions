evgenConfig.description = "POWHEG+fHerwig/Jimmy ttbar production with a 1 MeV lepton filter and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar' 

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

##To fix in MC13: "Lhef" -> "Powheg"; don't use Tauola.
evgenConfig.generators += [ "Lhef"]
include("MC12JobOptions/Jimmy_Tauola.py")

include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
#topAlg.TTbarWToLeptonFilter.NumLeptons=-1 #require at least one lepton (not yet released)
