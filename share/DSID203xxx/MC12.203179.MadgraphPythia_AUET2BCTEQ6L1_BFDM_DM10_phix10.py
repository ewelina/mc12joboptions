evgenConfig.description = "DM + b Model generation with MadGraph/Pythia6 in MC12"
evgenConfig.keywords = ["bFlavDM","BFDM"]
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.contact = ["emmalr@bu.edu","steven.schramm@cern.ch"]

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1",     # Changes listing of particles in log file
                          	 "pyinit pylistf 1",      # Changes listing of particles in log file
                                 "pyinit dumpr 1 2",
				]            

evgenConfig.inputfilecheck = 'BFDM_DM10_phix10'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0
      showerkt=T
      qcut=80
      imss(21)=24
      imss(22)=24
    """

phojf.write(phojinp)
phojf.close()

