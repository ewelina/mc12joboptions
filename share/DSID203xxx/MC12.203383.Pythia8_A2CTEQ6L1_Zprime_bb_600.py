#Pythia8 Z'(600 GeV)->bb with A2 CTEQ6L1 tune

evgenConfig.description = "Pythia8 Z'(600 GeV)->bb with A2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "Zprime", "bb"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ["Francesco.Guescini@cern.ch"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", #create Z' bosons
"PhaseSpace:mHatMin = 200.", #lower invariant mass
"Zprime:gmZmode = 3",
"32:m0 = 600", #set Z' mass [GeV]
"32:onMode = off", #switch off all Z' decays
"32:onIfAny = 5"] #switch on Z'->bb decay
