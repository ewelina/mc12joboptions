evgenConfig.contact = ["Angel Campoverde","angel.campoverde@cern.ch"]
evgenConfig.description = "Wprime 2500 GeV, with the AU2MSTW2008LO tune"
evgenConfig.keywords = ["Wprime"]

#Tune
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",  #Specify process
                          "34:m0 = 2500",    #Set the mass
                          "Wprime:coup2WZ = 1.", #Set coupling to WZ
                          "34:onMode = off",    #Turn off all W' decays
                          "34:onIfMatch = 24 23", #Turn on W' decays only to W/Z bosons
                          "24:onMode = off",      #Turn off W boson decays
                          "24:onIfAny = 1 2 3 4 5 6", #Turn on W boson decays to quarks
                          "23:onMode = off",      #Turn of Z boson decays
                          "23:onIfAny = 1 2 3 4 5 6", #Turn on Z boson decays to quarks
                          ]
