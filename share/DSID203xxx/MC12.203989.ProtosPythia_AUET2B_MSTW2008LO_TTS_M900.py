evgenConfig.description = "Vector-like TT production, singlet, M=900GeV, with Protos22+Pythia6, tune AUET2B MSTW2008LO"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","VLQ"]
evgenConfig.contact  = ["mark.stephen.cooke@cern.ch"]
evgenConfig.inputfilecheck = "protos"

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")   
