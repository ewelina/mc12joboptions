## POWHEG+Pythia8 DY->mumu

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_mumu"

def powheg_override():
    # nmumuded for AZNLO tune
    PowhegConfig.ptsqmin = 4.0
    PowhegConfig.mass_low = 180.
    PowhegConfig.mass_high = 250.

evgenConfig.description = "POWHEG+Pythia8 DY->mumu production with mass cut 180<M<250GeV without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "mumu"] 
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
