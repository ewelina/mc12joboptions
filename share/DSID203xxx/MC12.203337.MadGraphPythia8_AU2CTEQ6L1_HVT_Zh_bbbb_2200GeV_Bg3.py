include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.description = "HVT V0->Zh->bbbb (Model B, gV=3.0) with AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["Exotics", "HVT"]
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = 'HVT_Zh_bbbb_2200GeV_Bg3'

evgenConfig.contact = ["Massimiliano Bellomo"]
