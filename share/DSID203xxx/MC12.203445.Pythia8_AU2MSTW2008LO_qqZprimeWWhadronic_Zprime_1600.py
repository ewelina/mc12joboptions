evgenConfig.contact = ["Angel Campoverde","angel.campoverde@cern.ch"]
evgenConfig.description = "Zprime 1600 GeV, with the AU2MSTW2008LO tune"
evgenConfig.keywords = ["Zprime"]

#Tune
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",  #Specify process
                          "Zprime:gmZmode = 3", #Allow production of only Zprimes, no photons or Zs
                          "32:m0 = 1600",    #Set the mass
                          "Zprime:coup2WW = 1.",#Set coupling to WW
                          "32:onMode = off",    #Turn off all Z' decays
                          "32:onIfMatch = 24 -24", #Turn on Z' decays only to W bosons
                          "24:onMode = off",      #Turn of Z boson decays
                          "24:onIfAny = 1 2 3 4 5 6", #Turn on Z boson decays to quarks
                          ]
