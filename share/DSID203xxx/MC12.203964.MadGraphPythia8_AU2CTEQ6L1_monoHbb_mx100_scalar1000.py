include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")





evgenConfig.description = "Higgs+XX, scalar1000 model"
evgenConfig.keywords = ["SMhiggs","WIMP","bb"]
evgenConfig.inputfilecheck = 'mx100_scalar1000'
evgenConfig.contact = ["nwhallon@cern.ch"]
