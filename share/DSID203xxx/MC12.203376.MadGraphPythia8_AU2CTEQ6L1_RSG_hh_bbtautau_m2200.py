include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]


evgenConfig.description = "Bulk RS Graviton->HH->bbtautau with AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = 'SMRS_G2200_hh_bbtautau'

