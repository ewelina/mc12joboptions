## POWHEG+Pythia8 Wm->munu

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Wm_munu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0
    PowhegConfig.mass_low = 2750.
    PowhegConfig.mass_high = 3000.

evgenConfig.description = "POWHEG+Pythia8 Wm->munu production with mass cut 2750<M<3000GeV without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "W", "leptonic", "mu"] 
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>", "Nikos Tsirintanis <Nikolaos.Tsirintanis@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
