include ("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += [
    '1000022:all = X Xbar 2 0 0 20.0 0.0 0.0 0.0 0.0',
    '1000022:isVisible = false'
]
	
evgenConfig.description = "Higgs+Z,ZpA0_2HDM model with MadGraph/Pythia8 in MC12"
evgenConfig.keywords = ["2HDM","Z+H","bbar"]
evgenConfig.inputfilecheck = 'dmH_ZpZh'
evgenConfig.contact = ["yangyang.cheng@cern.ch"]
