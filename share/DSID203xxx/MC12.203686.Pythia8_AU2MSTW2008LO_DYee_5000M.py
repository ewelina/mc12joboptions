
evgenConfig.description = "DY->ee production, NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Z", "DY", "leptons"]
evgenConfig.contact = ['Daniel Hayden <daniel.hayden@cern.ch>']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                    # turn off all decays modes
                            "23:onIfAny = 11",                    # turn on the e+e- decay mode
                            "PhaseSpace:mHatMin = 5000."]         # lower invariant mass

