###############################################################
#
# Job options file
#
#===============================================================
evgenConfig.description = "CI dijet"
evgenConfig.keywords = ["Exotics", "CI dijet"]
evgenConfig.contact = ["Ning Zhou"]

include ( "MC12JobOptions/Pythia8_AU2_CT10_Common.py" )

topAlg.Pythia8.Commands += [
    #get the gluons through QCD
    #and the qq/qqbar from CI 
    "HardQCD:gg2gg = on",
    "HardQCD:gg2qqbar = on",
    "HardQCD:qg2qg = on",
    "HardQCD:qqbar2gg = on",
    'ContactInteractions:QCqq2qq = on',
    'ContactInteractions:QCqqbar2qqbar  = on',
    'ContactInteractions:Lambda = 10000.',
    'ContactInteractions:etaLL = -1',
    'PhaseSpace:mHatMin = 1000.',
    'PhaseSpace:pTHatMin = 600.',
    ]

include("MC12JobOptions/JetFilter_JZ5.py")
evgenConfig.minevents = 1000
#==============================================================
#
# End of job options file
#
###############################################################
