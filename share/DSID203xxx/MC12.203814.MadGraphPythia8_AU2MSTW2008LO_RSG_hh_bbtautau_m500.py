###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")


evgenConfig.description = "Bulk RS Graviton->HH->bbtautau with MSTW2008LO PDF"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.inputfilecheck = 'SMRS_G500_hh_bbtautau'
evgenConfig.contact = ["viviana.cavaliere@cern.ch"]

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]



