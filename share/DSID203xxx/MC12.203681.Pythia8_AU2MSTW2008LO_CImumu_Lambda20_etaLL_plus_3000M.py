evgenConfig.description = "CI->mumu production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CI", "mumu"]
evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["ContactInteractions:QCffbar2mumubar = on"] # process
topAlg.Pythia8.Commands += ["ContactInteractions:Lambda = 20000"] # mass scale
topAlg.Pythia8.Commands += ["ContactInteractions:etaLL = 1"] # interference
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMin = 3000"] # min invariant mass
