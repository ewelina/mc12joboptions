
###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Heavy vector triplet Vc -> WZ -> lljj with MSTW2008LO PDF and 2 lepton filter"
evgenConfig.keywords = ["Exotics", "Vector", "Triplet"]
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["MadGraph"]
evgenConfig.inputfilecheck = 'VcWZ_llqq_m3000'

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
