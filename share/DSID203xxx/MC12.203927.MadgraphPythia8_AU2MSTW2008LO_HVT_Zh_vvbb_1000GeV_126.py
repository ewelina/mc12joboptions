###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Heavy vector triplet V0 -> ZH -> vvbb with MSTW2008LO PDF"
evgenConfig.keywords = ["exotic", "resonance"]
evgenConfig.contact = ["frederick.dallaire@cern.ch"]
evgenConfig.generators = ["MadGraph"]
evgenConfig.inputfilecheck = 'HVT_Zh_vvbb'

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
