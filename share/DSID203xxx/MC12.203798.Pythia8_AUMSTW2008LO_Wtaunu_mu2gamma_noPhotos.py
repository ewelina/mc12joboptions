evgenConfig.description = "W->taumu, tau->mu2gamma production, with muon+diphoton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "W", "tau", "leptons", "photon", "mu2gamma", "LFV"]
evgenConfig.contact = ['Olga.Igonkina_cern.ch']
evgenConfig.generators += ["Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15", # switch on W->taunu decays
                            "15:oneChannel = true 1.0 0 13 22 22",   # tau- decays to mu- + 2gamma
                            "-15:oneChannel = true 1.0 0 -13 22 22"  # tau+ decays to mu+ + 2gamma
]

from GeneratorFilters.GeneratorFiltersConf import DiPhotonFilter
topAlg += DiPhotonFilter()
topAlg.DiPhotonFilter.PtCut1st = 4000.
topAlg.DiPhotonFilter.PtCut2nd = 4000.
topAlg.DiPhotonFilter.EtaCut1st = 2.7
topAlg.DiPhotonFilter.EtaCut2nd = 2.7
topAlg.DiPhotonFilter.MassCutFrom = -1
topAlg.DiPhotonFilter.MassCutTo = 1800.  #safety precoation

if "DiPhotonFilter" not in StreamEVGEN.RequireAlgs:
   StreamEVGEN.RequireAlgs += ["DiPhotonFilter"]

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 3800.
topAlg.LeptonFilter.Etacut = 2.7

