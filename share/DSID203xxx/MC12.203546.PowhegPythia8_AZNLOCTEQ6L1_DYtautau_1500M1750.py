## POWHEG+Pythia8 DY->tautau

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_tautau"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0
    PowhegConfig.mass_low = 1500.
    PowhegConfig.mass_high = 1750.

evgenConfig.description = "POWHEG+Pythia8 DY->tautau production with mass cut 1500<M<1750GeV without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "tau"] 
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
