evgenConfig.description = "Vector-like single B production, TB singlet, M=900GeV, Vmix=1.0, with Protos22+Pythia6, tune AUET2B MSTW2008LO"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","heavyquark","VLQ"]
evgenConfig.contact  = ["youzhou@email.arizona.edu"]
evgenConfig.inputfilecheck = "protos"

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

