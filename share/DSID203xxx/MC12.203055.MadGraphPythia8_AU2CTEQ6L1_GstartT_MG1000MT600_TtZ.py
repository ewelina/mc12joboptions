evgenConfig.description = "MadGraph5+Pythia8 for composite Higgs heavy gluon (mg=1000 GeV) to->tT, T->tZ (mT=600 GeV)"
evgenConfig.contact = ["james.ferrando@glasgow.ac.uk","shoaleh@lps.umontreal.ca"] 
evgenConfig.keywords = ["KKgluon","tT",]
evgenConfig.inputfilecheck = "GstartTMG1000MT600TtZ"
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
