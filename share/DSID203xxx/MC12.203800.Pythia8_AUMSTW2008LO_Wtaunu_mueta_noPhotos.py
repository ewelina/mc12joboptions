evgenConfig.description = "W->taumu, tau->mueta production, with lepton and eta filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "W", "tau", "leptons", "mueta", "LFV"]
evgenConfig.contact = ['Olya_Igonkina']
evgenConfig.generators += ["Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15", # switch on W->taunu decays
                            "15:oneChannel = true 1.0 0 13 221",   # tau- decays to mu- + 2gamma
                            "-15:oneChannel = true 1.0 0 -13 221"  # tau+ decays to mu+ + 2gamma
]

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 3800.
topAlg.LeptonFilter.Etacut = 2.7


if not hasattr(topAlg, "ParentChildFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
    topAlg += ParentChildFilter()

## Add this filter to the algs required to be successful for streaming
if "ParentChildFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ParentChildFilter"]

## Default cut params
topAlg.ParentChildFilter.PDGParent  = [15]
topAlg.ParentChildFilter.PtMinParent =  20000
topAlg.ParentChildFilter.EtaRangeParent = 2.7
topAlg.ParentChildFilter.PDGChild = [221]
topAlg.ParentChildFilter.PtMinChild = 5000.
topAlg.ParentChildFilter.EtaRangeChild = 2.7

