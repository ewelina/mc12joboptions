## POWHEG+Pythia8 Wp->munu

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Wp_munu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0
    PowhegConfig.mass_low = 1750.
    PowhegConfig.mass_high = 2000.

evgenConfig.description = "POWHEG+Pythia8 Wp->munu production with mass cut 1750<M<2000GeV without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "W", "leptonic", "mu"] 
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>", "Nikos Tsirintanis <Nikolaos.Tsirintanis@cern.ch>"]

include("MC12JobOptions/PowhegControl_postInclude.py")
