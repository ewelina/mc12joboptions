include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")





evgenConfig.description = "Higgs+XX, xdxhDh model"
evgenConfig.keywords = ["SMhiggs","WIMP","bb"]
evgenConfig.inputfilecheck = 'mx65_xdxhDh'
evgenConfig.contact = ["nwhallon@cern.ch"]
