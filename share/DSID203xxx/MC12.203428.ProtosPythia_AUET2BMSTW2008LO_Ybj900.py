evgenConfig.description = "Protos+Pythia6 for Ybj production with AUET2B tune and MSTW2008LO pdf"
evgenConfig.keywords = ["Ybj", "single production"]
evgenConfig.inputfilecheck = "group.phys-gener.Protos22.203428.Ybj900"
evgenConfig.contact  = ["biedermd@physik.hu-berlin.de"]
evgenConfig.generators = ["Protos", "Pythia"]
# ... Pythia

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
################################################################

