## Pythia8 Wprime(4000 GeV)->emutau with alternative PDF set

m_wprime=4000.0 #in GeV

evgenConfig.description = "SSM W prime ("+str(m_wprime)+") to emutau"
evgenConfig.keywords = ["Exotics", "W'"]
evgenConfig.contact = ["Daniel Hayden <daniel.hayden@cern.ch>", "Nikos Tsirintanis <Nikolaos.Tsirintanis@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# turn on the W' process
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]

# set mass and disable all decay modes except W' -> emutau
topAlg.Pythia8.Commands += ["34:m0 ="+str(m_wprime) ]
topAlg.Pythia8.Commands += ["34:onMode = off"]
topAlg.Pythia8.Commands += ["34:onIfAny = 11,12"]
topAlg.Pythia8.Commands += ["34:onIfAny = 13,14"]
topAlg.Pythia8.Commands += ["34:onIfAny = 15,16"]
