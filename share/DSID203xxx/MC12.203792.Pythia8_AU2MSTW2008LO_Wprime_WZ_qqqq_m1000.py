# W' -> WZ -> qqqq

# Wprime Mass (in GeV)
M_Wprime = 1000.0 

evgenConfig.contact = ["lei.li@cern.ch"]
evgenConfig.description = "Wprime->WZ->qqqq "+str(M_Wprime)+" GeV with MSTW2008LO PDF and AU2 tunee"
evgenConfig.keywords = ["Exotics", "EGM", "SSM", "Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                            "Wprime:coup2WZ = 1.",    # Wprime Coupling to WZ
                            "34:m0 = "+str(M_Wprime), # Wprime mass
                            "34:onMode = off",
                            "34:onIfAll = 23 24",
                            "24:onMode = off",
                            "24:onIfAny = 1 2 3 4 5",
                            "23:onMode = off",
                            "23:onIfAny = 1 2 3 4 5",
                            "Init:showAllParticleData = on",
                            "Next:numberShowEvent = 5"]
