
include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_LHEF_Common.py")
evgenConfig.description = "Madgraph+Herwigpp ZZ_mumu production without filter using CTEQ6L1 pdf and CTEQ6L1-UE-EE-3 tune"
evgenConfig.keywords = ["MPI", "EW", "diboson", "dimuons" ]
evgenConfig.minevents = 5000
evgenConfig.contact = ["Miroslav Myska <MyskaM@fzu.cz>"]
evgenConfig.generators += ["Lhef"]

cmds = """
set /Herwig/Cuts/EECuts:MHatMin 0.0*GeV
set /Herwig/Cuts/PhotonKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/PhotonKtCut:MinEta -10.
set /Herwig/Cuts/PhotonKtCut:MaxEta 10.
set /Herwig/Cuts/WBosonKtCut:MinKT 0.0*GeV
set /Herwig/Cuts/MassCut:MinM 0.*GeV
set /Herwig/Cuts/MassCut:MaxM 14000.*GeV
set /Herwig/Cuts/QCDCuts:MHatMin 0.0*GeV
set /Herwig/Cuts/JetKtCut:MinKT 0.001*GeV
set /Herwig/Cuts/LeptonKtCut:MinKT 0.001*GeV
set /Herwig/Cuts/LeptonKtCut:MaxEta 10.
set /Herwig/Cuts/LeptonKtCut:MinEta -10.
"""
topAlg.Herwigpp.Commands += cmds.splitlines()

if runArgs.ecmEnergy == 14000.0:
    evgenConfig.inputfilecheck = "MadGraph.*126567"
else:
    print "ERROR: invalid ecmEnergy:", runArgs.ecmEnergy



