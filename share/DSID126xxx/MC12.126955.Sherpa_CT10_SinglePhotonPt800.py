include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Gamma + up to 5 jets with ME+PS. Slice in photon pT>800.0 GeV."
evgenConfig.keywords = [ "EW", "gamma" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  QCUT:=30.0
}(run)

(processes){
  Process 93 93 -> 22 93 93{4}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Integration_Error 0.1 {5,6};
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process
}(processes)

(selector){
  PT  22  800.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""
