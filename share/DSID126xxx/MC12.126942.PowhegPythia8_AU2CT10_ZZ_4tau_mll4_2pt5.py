evgenConfig.description = "POWHEG+Pythia8 ZZ_4tau production mll>4GeV with dimuon filter pt>5GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.minevents = 5000
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

TMPBASEIF = 'ZZ_4tau'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV
