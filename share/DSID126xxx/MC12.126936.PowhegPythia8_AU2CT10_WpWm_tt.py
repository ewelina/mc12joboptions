
evgenConfig.description = "POWHEG+Pythia8 WpWm_tt production without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"WW", "leptons" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*126936"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]


include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
