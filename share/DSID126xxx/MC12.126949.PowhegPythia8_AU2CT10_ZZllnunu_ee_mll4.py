
evgenConfig.description = "POWHEG+Pythia8 ZZllnunu_ee production mll>4GeV without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "lepton", "neutrino" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*126949"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]


include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
