evgenConfig.description = "PYTHIA8 ZZ->4l (3 lepton filter) with AU2 CTEQ6L1"
evgenConfig.keywords = ["EW", "diboson","Z","leptonic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            'WeakDoubleBoson:ffbar2gmZgmZ = on',
                            '23:mMin = 7.0',
                            '23:onMode = off',
                            '23:onIfAny = 11 13 15'
                            ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import  MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 3
MultiLeptonFilter.Etacut = 10.0
MultiLeptonFilter.Ptcut = 5000.0

StreamEVGEN.RequireAlgs +=  [ "MultiLeptonFilter" ]
