evgenConfig.description = "Zee + Light Flavour jets (complementary sample to 128977)"
evgenConfig.keywords = [ "Z", "Heavy Flavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.process="Zee+LF jets"

include( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1

  YUKAWA_TAU=0
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process 93  93 -> 15 -15 93{4}
  Order_EW 2
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;

  Process 5  -5 -> 15 -15 93{4}
  Order_EW 2
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;

  Process 4 -4 -> 15 -15 93{4}
  Order_EW 2
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  End process;
}(processes)

(selector){
  Mass 15 -15 40 E_CMS
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""
