include("MC12JobOptions/Herwigpp_UEEE3_MRSTMCal_CT10ME_Common.py")

from Herwigpp_i import config as hw

cmds = """
create ThePEG::ParticleData H+
setup H+ 37 H+ 600.0 0.0 0.0 0.0 3 0 1 0
create ThePEG::ParticleData H-
setup H- -37 H- 600.0 0.0 0.0 0.0 -3 0 1 0
makeanti H+ H-

decaymode H+->t,bbar; 1.0 1 /Herwig/Decays/Mambo
"""

topAlg.Herwigpp.Commands += cmds.splitlines()
topAlg.Herwigpp.Commands += hw.lhef_cmds(filename="events.lhe", nlo=True, usespin=False).splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators = ["McAtNlo", "Herwigpp"]
evgenConfig.keywords = ["nonSMhiggs"]
evgenConfig.description = "McAtNlo+Herwig++ tH+ production with heavy H+(600GeV) -> t,bbar - CT10 PDF hard scatter and LO** with EE3 tune for shower+MPI"
evgenConfig.inputfilecheck = "group.phys-gener.mcatnlo46.146532.tHplus_tb"
evgenConfig.contact = ["jie.yu@cern.ch"]
