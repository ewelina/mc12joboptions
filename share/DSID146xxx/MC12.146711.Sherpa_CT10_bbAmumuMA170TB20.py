include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "b bbar -> Higgs [-> muon muon] production with up to 3 jets in ME+PS."
evgenConfig.keywords = [ "nonSMhiggs", "ggF", "mu", "leptonic", "bbA"]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch" ]

evgenConfig.process="""
(run){
  MASS[25]=170
  WIDTH[25]=1.13531
  MASSIVE[13]=1
  MASSIVE[5]=1
  YUKAWA_MU=0.105
}(run)

(processes){
  Process : 93 93 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 93 93 -> 25[a] 5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 93 -5 -> 25[a] -5 93{2}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 93 -5 -> 25[a] -5 5 -5
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 5 93 -> 25[a] 5 93{2}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 5 93 -> 25[a] 5 5 -5
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 5 5 -> 25[a] 5 5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : -5 -5 -> 25[a] -5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 5 -5 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
  Process : 5 -5 -> 25[a] 5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 13 -13
  Order_EW : 2
  End process
  
}(processes)
"""
