evgenConfig.description = "PYTHIA8 WZ->inclusive with AU2 CTEQ6L1"
evgenConfig.keywords = ["EW", "diboson","W","Z","inclusive"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            'WeakDoubleBoson:ffbar2ZW = on',
                            '23:mMin = 7.0',
                            '23:onMode = off',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                            '24:mMin = 7.0',
                            '24:onMode = off',
                            '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]
