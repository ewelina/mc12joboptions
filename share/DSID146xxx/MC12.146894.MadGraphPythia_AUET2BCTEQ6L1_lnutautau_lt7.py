include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2"
                                 ]

evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'WGammaStar using MadGraph/Pythia'
evgenConfig.keywords =["W","gammastar","leptonic"]
evgenConfig.inputfilecheck = 'madgraph.146894.lnutautau_lt7'
