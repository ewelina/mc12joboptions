evgenConfig.description = "McAtNlo+fHerwig/Jimmy dileptonic ttbar with 4 lepton mass filter - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "leptonic", "dileptonic"]
evgenConfig.contact  = ["tatsuya.masubuchi@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"
evgenConfig.minevents = 200

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import FourLeptonMassFilter
topAlg += FourLeptonMassFilter()
FourLeptonMassFilter = topAlg.FourLeptonMassFilter
FourLeptonMassFilter.MinPt = 5000.
FourLeptonMassFilter.MaxEta = 3.
FourLeptonMassFilter.MinMass1 = 40000.
FourLeptonMassFilter.MaxMass1 = 14000000.
FourLeptonMassFilter.MinMass2 = 12000.
FourLeptonMassFilter.MaxMass2 = 14000000.
FourLeptonMassFilter.AllowElecMu = True
FourLeptonMassFilter.AllowSameCharge = True

StreamEVGEN.RequireAlgs = [ "FourLeptonMassFilter" ]
