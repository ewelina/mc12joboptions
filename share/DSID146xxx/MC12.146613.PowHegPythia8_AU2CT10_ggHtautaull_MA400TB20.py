evgenConfig.description = "POWHEG+PYTHIA8 MSSM ggH H->tautau->ll with AU2,CT10"
evgenConfig.keywords = ["nonSMhiggs", "ggF", "tau","leptonic"]
evgenConfig.inputfilecheck = "ggH_MA400TB20"
evgenConfig.minevents = 2000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 15 15'
                           ]

# ... Filter H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13]
topAlg.XtoVVDecayFilter.PDGChild2 = [11,13]
