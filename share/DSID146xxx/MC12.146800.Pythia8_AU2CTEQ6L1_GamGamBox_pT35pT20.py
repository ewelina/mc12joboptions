## Pythia8 gg->gamgam(BOX)

evgenConfig.description = "gamgam with box diagram production with two photon filter and AU2 CTEQ6L1 tune"
evgenConfig.keywords = [ "gammagamma", "box" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["PromptPhoton:gg2gammagamma = on", # gg->gamgam
                            "PhaseSpace:pTHatMin = 15" ]

###
from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
if not hasattr(topAlg, "DirectPhotonFilter1"):
    topAlg += DirectPhotonFilter("DirectPhotonFilter1")
if not hasattr(topAlg, "DirectPhotonFilter2"):
    topAlg += DirectPhotonFilter("DirectPhotonFilter2")
topAlg.DirectPhotonFilter1.Ptcut = 20000.
topAlg.DirectPhotonFilter1.Etacut =  2.7
topAlg.DirectPhotonFilter1.NPhotons = 2
topAlg.DirectPhotonFilter2.Ptcut = 35000.
topAlg.DirectPhotonFilter2.Etacut =  2.7
topAlg.DirectPhotonFilter2.NPhotons = 1
if "DirectPhotonFilter1" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["DirectPhotonFilter1"]
if "DirectPhotonFilter2" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["DirectPhotonFilter2"]
