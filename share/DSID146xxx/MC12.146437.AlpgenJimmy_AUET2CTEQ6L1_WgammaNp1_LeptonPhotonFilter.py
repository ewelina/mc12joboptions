evgenConfig.description = 'ALPGEN+Herwig Wgamma 1p with 8,18 GeV photon lepton filter and AUET2 CTEQ6L1 tune'
evgenConfig.keywords = ['EW', 'W', 'gamma','leptonic']
evgenConfig.inputfilecheck = 'alpgen.*WgammaNp1_pt20'
evgenConfig.contact = ['zhijun.liang@cern.ch','biagio.di.micco@cern.ch']
evgenConfig.process = 'alpgen_process'
evgenConfig.minevents = 5000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/MultiObjectsFilter.py")
topAlg.MultiObjectsFilter.PtCut = 8000.
topAlg.MultiObjectsFilter.EtaCut = 3.0
topAlg.MultiObjectsFilter.UseEle = True
topAlg.MultiObjectsFilter.UseMuo = True
topAlg.MultiObjectsFilter.UseJet = False
topAlg.MultiObjectsFilter.UsePho = True
topAlg.MultiObjectsFilter.UseSumPt = False
topAlg.MultiObjectsFilter.PtCutEach = [18000.,8000.]

