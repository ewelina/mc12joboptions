#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.22127835E+17
#              |                                                   |
#              -----------------------------------------------------
#
#
# WARNING: For the gauge couplings g1 and g2 the  DRbar values at the scale Q are not given.
# They are calculated instead from alpha_ew_MSbar at M_Z.
# The decay widths and the QCD corrections cannot be calculated consistently.
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   ISASUGRA                                          
     2   7.81                                              
#
BLOCK MODSEL  # Model selection
     1     1   # #                                                 
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.25778332E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16570000E-05   # G_F [GeV^-2]
         3     1.17200002E-01   # alpha_S(M_Z)^MSbar
         4     8.93764874E+01   # M_Z pole mass
         5     4.19999981E+00   # mb(mb)^MSbar
         6     1.54540821E+02   # mt pole mass
         7     1.94850223E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     6.00000000E+02   # m_0                 
         2     3.00000000E+02   # m_{1                
         3     1.00000000E+01   # tan(beta)           
         4     1.00000000E+00   # sign(mu)            
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     2.21278347E+16   # Input               
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     7.71761851E+01   # W+
        25     1.11844620E+02   # h
        35     7.49062561E+02   # H
        36     7.43967712E+02   # A
        37     7.53755432E+02   # H+
         5     4.81948767E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     7.50000000E+02   # ~d_L
   2000001     7.50000000E+02   # ~d_R
   1000002     7.50000000E+02   # ~u_L
   2000002     7.50000000E+02   # ~u_R
   1000003     7.50000000E+02   # ~s_L
   2000003     7.50000000E+02   # ~s_R
   1000004     7.50000000E+02   # ~c_L
   2000004     7.50000000E+02   # ~c_R
   1000005     4.50000000E+03   # ~b_1
   2000005     4.50000000E+03   # ~b_2
   1000006     4.50000000E+03   # ~t_1
   2000006     4.50000000E+03   # ~t_2
   1000011     4.50000000E+03   # ~e_L
   2000011     4.50000000E+03   # ~e_R
   1000012     4.50000000E+03   # ~nu_eL
   1000013     4.50000000E+03   # ~mu_L
   2000013     4.50000000E+03   # ~mu_R
   1000014     4.50000000E+03   # ~nu_muL
   1000015     4.50000000E+03   # ~tau_1
   2000015     4.50000000E+03   # ~tau_2
   1000016     4.50000000E+03   # ~nu_tauL
   1000021     4.50000000E+03   # ~g
   1000022     6.00000000E+02   # ~chi_10
   1000023     4.50000000E+03   # ~chi_20
   1000025     4.50000000E+03   # ~chi_30
   1000035     4.50000000E+03   # ~chi_40
   1000024     4.50000000E+03   # ~chi_1+
   1000037     4.50000000E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.00000000E+00   # N_11
  1  2     0.00000000E+00   # N_12
  1  3     0.00000000E+00   # N_13
  1  4     0.00000000E+00   # N_14
  2  1     0.00000000E+00   # N_21
  2  2     1.00000000E+00   # N_22
  2  3     0.00000000E+00   # N_23
  2  4     0.00000000E+00   # N_24
  3  1     0.00000000E+00   # N_31
  3  2     0.00000000E+00   # N_32
  3  3     1.00000000E+00   # N_33
  3  4     0.00000000E+00   # N_34
  4  1     0.00000000E+00   # N_41
  4  2     0.00000000E+00   # N_42
  4  3     0.00000000E+00   # N_43
  4  4     1.00000000E+00   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1     1.00000000E+00   # U_11
  1  2     0.00000000E+00   # U_12
  2  1     0.00000000E+00   # U_21
  2  2     1.00000000E+00   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1     1.00000000E+00   # V_11
  1  2     0.00000000E+00   # V_12
  2  1     0.00000000E+00   # V_21
  2  2     1.00000000E+00   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.00000000E+00   # cos(theta_t)
  1  2     0.00000000E+00   # sin(theta_t)
  2  1     0.00000000E+00   # -sin(theta_t)
  2  2     1.00000000E+00   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     1.00000000E+00   # cos(theta_b)
  1  2     0.00000000E+00   # sin(theta_b)
  2  1     0.00000000E+00   # -sin(theta_b)
  2  2     1.00000000E+00   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.00000000E+00   # cos(theta_tau)
  1  2     0.00000000E+00   # sin(theta_tau)
  2  1     0.00000000E+00   # -sin(theta_tau)
  2  2     1.00000000E+00   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.02914833E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  2.21278347E+16  # DRbar Higgs Parameters
         1     4.12454407E+02   # mu(Q)               
         2     9.36003455E+00   # tanb                
         3     2.50607727E+02   # Higgs               
         4     5.53487938E+05   # m_A^2(Q)            
#
BLOCK GAUGE Q=  2.21278347E+16  # The gauge couplings
     3     1.07381373E+00   # g3(Q) DRbar
#
BLOCK AU Q=  2.21278347E+16  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3    -5.32061523E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  2.21278347E+16  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3    -8.07902039E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  2.21278347E+16  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3    -1.81115051E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  2.21278347E+16  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.85841429E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  2.21278347E+16  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     1.36232540E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  2.21278347E+16  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.01981103E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  2.21278347E+16  # The soft SUSY breaking masses at the scale Q
         1     1.24019547E+02   # M_1(Q)              
         2     2.32185043E+02   # M_2(Q)              
         3     6.86750671E+02   # M_3(Q)              
        31     6.29402649E+02   # MeL(Q)              
        32     6.29402649E+02   # MmuL(Q)             
        33     6.26662476E+02   # MtauL(Q)            
        34     6.08800842E+02   # MeR(Q)              
        35     6.08800842E+02   # MmuR(Q)             
        36     6.03154236E+02   # MtauR(Q)            
        41     8.48326294E+02   # MqL1(Q)             
        42     8.48326294E+02   # MqL2(Q)             
        43     7.40788147E+02   # MqL3(Q)             
        44     8.34092896E+02   # MuR(Q)              
        45     8.34092896E+02   # McR(Q)              
        46     5.90198242E+02   # MtR(Q)              
        47     8.32408752E+02   # MdR(Q)              
        48     8.32408752E+02   # MsR(Q)              
        49     8.31454102E+02   # MbR(Q)              
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.02218095E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     3.36104348E+03   # gluino decays
#          BR         NDA      ID1       ID2
     6.25000000E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     6.25000000E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     6.25000000E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     6.25000000E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     6.25000000E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     6.25000000E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     6.25000000E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     6.25000000E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     6.25000000E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     6.25000000E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     6.25000000E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     6.25000000E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     6.25000000E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     6.25000000E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     6.25000000E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     6.25000000E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006                NAN   # stop1 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
                NAN    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
                NAN    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
                NAN    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
                NAN    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
                NAN    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
                NAN    2     1000021         6   # BR(~t_1 -> ~g      t )
                NAN    2     1000005        37   # BR(~t_1 -> ~b_1    H+)
                NAN    2     2000005        37   # BR(~t_1 -> ~b_2    H+)
                NAN    2     1000005        24   # BR(~t_1 -> ~b_1    W+)
                NAN    2     2000005        24   # BR(~t_1 -> ~b_2    W+)
#
#         PDG            Width
DECAY   2000006                NAN   # stop2 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
                NAN    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
                NAN    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
                NAN    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
                NAN    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
                NAN    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
                NAN    2     1000021         6   # BR(~t_2 -> ~g      t )
                NAN    2     1000006        25   # BR(~t_2 -> ~t_1    h )
                NAN    2     1000006        35   # BR(~t_2 -> ~t_1    H )
                NAN    2     1000006        36   # BR(~t_2 -> ~t_1    A )
                NAN    2     1000005        37   # BR(~t_2 -> ~b_1    H+)
                NAN    2     2000005        37   # BR(~t_2 -> ~b_2    H+)
                NAN    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
                NAN    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
                NAN    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005                NAN   # sbottom1 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
                NAN    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
                NAN    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
                NAN    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
                NAN    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
                NAN    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
                NAN    2     1000021         5   # BR(~b_1 -> ~g      b )
                NAN    2     1000006       -37   # BR(~b_1 -> ~t_1    H-)
                NAN    2     2000006       -37   # BR(~b_1 -> ~t_2    H-)
                NAN    2     1000006       -24   # BR(~b_1 -> ~t_1    W-)
                NAN    2     2000006       -24   # BR(~b_1 -> ~t_2    W-)
#
#         PDG            Width
DECAY   2000005                NAN   # sbottom2 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
                NAN    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
                NAN    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
                NAN    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
                NAN    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
                NAN    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
                NAN    2     1000021         5   # BR(~b_2 -> ~g      b )
                NAN    2     1000005        25   # BR(~b_2 -> ~b_1    h )
                NAN    2     1000005        35   # BR(~b_2 -> ~b_1    H )
                NAN    2     1000005        36   # BR(~b_2 -> ~b_1    A )
                NAN    2     1000006       -37   # BR(~b_2 -> ~t_1    H-)
                NAN    2     2000006       -37   # BR(~b_2 -> ~t_2    H-)
                NAN    2     1000005        23   # BR(~b_2 -> ~b_1    Z )
                NAN    2     1000006       -24   # BR(~b_2 -> ~t_1    W-)
                NAN    2     2000006       -24   # BR(~b_2 -> ~t_2    W-)
#
#         PDG            Width
DECAY   1000002     1.82085241E-02   # sup_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_L -> ~chi_10 u)
#
#         PDG            Width
DECAY   2000002     2.91336386E-01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> ~chi_10 u)
#
#         PDG            Width
DECAY   1000001     1.82085241E-02   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_L -> ~chi_10 d)
#
#         PDG            Width
DECAY   2000001     7.28340964E-02   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         1   # BR(~d_R -> ~chi_10 d)
#
#         PDG            Width
DECAY   1000004     1.82085241E-02   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_L -> ~chi_10 c)
#
#         PDG            Width
DECAY   2000004     2.91336386E-01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> ~chi_10 c)
#
#         PDG            Width
DECAY   1000003     1.82085241E-02   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_L -> ~chi_10 s)
#
#         PDG            Width
DECAY   2000003     7.28340964E-02   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022         3   # BR(~s_R -> ~chi_10 s)
#
#         PDG            Width
DECAY   1000011     5.78650267E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
#
#         PDG            Width
DECAY   2000011     2.31460107E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
#
#         PDG            Width
DECAY   1000013     5.78650267E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   2000013     2.31460107E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
#
#         PDG            Width
DECAY   1000015     5.78650042E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
#
#         PDG            Width
DECAY   2000015     2.31460017E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
#
#         PDG            Width
DECAY   1000012     5.78650267E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
#
#         PDG            Width
DECAY   1000014     5.78650267E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
#
#         PDG            Width
DECAY   1000016     5.78650267E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
#
#         PDG            Width
DECAY   1000024                NAN   # chargino1+ decays
#          BR         NDA      ID1       ID2
                NAN    2     1000002        -1   # BR(~chi_1+ -> ~u_L     db)
                NAN    2     2000002        -1   # BR(~chi_1+ -> ~u_R     db)
                NAN    2    -1000001         2   # BR(~chi_1+ -> ~d_L*    u )
                NAN    2    -2000001         2   # BR(~chi_1+ -> ~d_R*    u )
                NAN    2     1000004        -3   # BR(~chi_1+ -> ~c_L     sb)
                NAN    2     2000004        -3   # BR(~chi_1+ -> ~c_R     sb)
                NAN    2    -1000003         4   # BR(~chi_1+ -> ~s_L*    c )
                NAN    2    -2000003         4   # BR(~chi_1+ -> ~s_R*    c )
                NAN    2     1000006        -5   # BR(~chi_1+ -> ~t_1     bb)
                NAN    2     2000006        -5   # BR(~chi_1+ -> ~t_2     bb)
                NAN    2    -1000005         6   # BR(~chi_1+ -> ~b_1*    t )
                NAN    2    -2000005         6   # BR(~chi_1+ -> ~b_2*    t )
                NAN    2     1000012       -11   # BR(~chi_1+ -> ~nu_eL   e+  )
                NAN    2     1000014       -13   # BR(~chi_1+ -> ~nu_muL  mu+ )
                NAN    2     1000016       -15   # BR(~chi_1+ -> ~nu_tau1 tau+)
                NAN    2    -1000011        12   # BR(~chi_1+ -> ~e_L+    nu_e)
                NAN    2    -2000011        12   # BR(~chi_1+ -> ~e_R+    nu_e)
                NAN    2    -1000013        14   # BR(~chi_1+ -> ~mu_L+   nu_mu)
                NAN    2    -2000013        14   # BR(~chi_1+ -> ~mu_R+   nu_mu)
                NAN    2    -1000015        16   # BR(~chi_1+ -> ~tau_1+  nu_tau)
                NAN    2    -2000015        16   # BR(~chi_1+ -> ~tau_2+  nu_tau)
                NAN    2     1000022        24   # BR(~chi_1+ -> ~chi_10  W+)
                NAN    2     1000023        24   # BR(~chi_1+ -> ~chi_20  W+)
                NAN    2     1000025        24   # BR(~chi_1+ -> ~chi_30  W+)
                NAN    2     1000035        24   # BR(~chi_1+ -> ~chi_40  W+)
                NAN    2     1000022        37   # BR(~chi_1+ -> ~chi_10  H+)
                NAN    2     1000023        37   # BR(~chi_1+ -> ~chi_20  H+)
                NAN    2     1000025        37   # BR(~chi_1+ -> ~chi_30  H+)
                NAN    2     1000035        37   # BR(~chi_1+ -> ~chi_40  H+)
#
#         PDG            Width
DECAY   1000037                NAN   # chargino2+ decays
#          BR         NDA      ID1       ID2
                NAN    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
                NAN    2     2000002        -1   # BR(~chi_2+ -> ~u_R     db)
                NAN    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
                NAN    2    -2000001         2   # BR(~chi_2+ -> ~d_R*    u )
                NAN    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
                NAN    2     2000004        -3   # BR(~chi_2+ -> ~c_R     sb)
                NAN    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
                NAN    2    -2000003         4   # BR(~chi_2+ -> ~s_R*    c )
                NAN    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
                NAN    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
                NAN    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
                NAN    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
                NAN    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
                NAN    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
                NAN    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
                NAN    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
                NAN    2    -2000011        12   # BR(~chi_2+ -> ~e_R+    nu_e)
                NAN    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
                NAN    2    -2000013        14   # BR(~chi_2+ -> ~mu_R+   nu_mu)
                NAN    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
                NAN    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
                NAN    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
                NAN    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
                NAN    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
                NAN    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
                NAN    2     1000035        24   # BR(~chi_2+ -> ~chi_40  W+)
                NAN    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
                NAN    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
                NAN    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
                NAN    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
                NAN    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
                NAN    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
                NAN    2     1000035        37   # BR(~chi_2+ -> ~chi_40  H+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023                NAN   # neutralino2 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
                NAN    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
                NAN    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
                NAN    2     1000037       -24   # BR(~chi_20 -> ~chi_2+   W-)
                NAN    2    -1000037        24   # BR(~chi_20 -> ~chi_2-   W+)
                NAN    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
                NAN    2     1000022        35   # BR(~chi_20 -> ~chi_10   H )
                NAN    2     1000022        36   # BR(~chi_20 -> ~chi_10   A )
                NAN    2     1000024       -37   # BR(~chi_20 -> ~chi_1+   H-)
                NAN    2    -1000024        37   # BR(~chi_20 -> ~chi_1-   H+)
                NAN    2     1000037       -37   # BR(~chi_20 -> ~chi_2+   H-)
                NAN    2    -1000037        37   # BR(~chi_20 -> ~chi_2-   H+)
                NAN    2     1000002        -2   # BR(~chi_20 -> ~u_L      ub)
                NAN    2    -1000002         2   # BR(~chi_20 -> ~u_L*     u )
                NAN    2     2000002        -2   # BR(~chi_20 -> ~u_R      ub)
                NAN    2    -2000002         2   # BR(~chi_20 -> ~u_R*     u )
                NAN    2     1000001        -1   # BR(~chi_20 -> ~d_L      db)
                NAN    2    -1000001         1   # BR(~chi_20 -> ~d_L*     d )
                NAN    2     2000001        -1   # BR(~chi_20 -> ~d_R      db)
                NAN    2    -2000001         1   # BR(~chi_20 -> ~d_R*     d )
                NAN    2     1000004        -4   # BR(~chi_20 -> ~c_L      cb)
                NAN    2    -1000004         4   # BR(~chi_20 -> ~c_L*     c )
                NAN    2     2000004        -4   # BR(~chi_20 -> ~c_R      cb)
                NAN    2    -2000004         4   # BR(~chi_20 -> ~c_R*     c )
                NAN    2     1000003        -3   # BR(~chi_20 -> ~s_L      sb)
                NAN    2    -1000003         3   # BR(~chi_20 -> ~s_L*     s )
                NAN    2     2000003        -3   # BR(~chi_20 -> ~s_R      sb)
                NAN    2    -2000003         3   # BR(~chi_20 -> ~s_R*     s )
                NAN    2     1000006        -6   # BR(~chi_20 -> ~t_1      tb)
                NAN    2    -1000006         6   # BR(~chi_20 -> ~t_1*     t )
                NAN    2     2000006        -6   # BR(~chi_20 -> ~t_2      tb)
                NAN    2    -2000006         6   # BR(~chi_20 -> ~t_2*     t )
                NAN    2     1000005        -5   # BR(~chi_20 -> ~b_1      bb)
                NAN    2    -1000005         5   # BR(~chi_20 -> ~b_1*     b )
                NAN    2     2000005        -5   # BR(~chi_20 -> ~b_2      bb)
                NAN    2    -2000005         5   # BR(~chi_20 -> ~b_2*     b )
                NAN    2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
                NAN    2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
                NAN    2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
                NAN    2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
                NAN    2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
                NAN    2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
                NAN    2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
                NAN    2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
                NAN    2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
                NAN    2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
                NAN    2     2000015       -15   # BR(~chi_20 -> ~tau_2-   tau+)
                NAN    2    -2000015        15   # BR(~chi_20 -> ~tau_2+   tau-)
                NAN    2     1000012       -12   # BR(~chi_20 -> ~nu_eL    nu_eb)
                NAN    2    -1000012        12   # BR(~chi_20 -> ~nu_eL*   nu_e )
                NAN    2     1000014       -14   # BR(~chi_20 -> ~nu_muL   nu_mub)
                NAN    2    -1000014        14   # BR(~chi_20 -> ~nu_muL*  nu_mu )
                NAN    2     1000016       -16   # BR(~chi_20 -> ~nu_tau1  nu_taub)
                NAN    2    -1000016        16   # BR(~chi_20 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000025                NAN   # neutralino3 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
                NAN    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
                NAN    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
                NAN    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
                NAN    2     1000037       -24   # BR(~chi_30 -> ~chi_2+   W-)
                NAN    2    -1000037        24   # BR(~chi_30 -> ~chi_2-   W+)
                NAN    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
                NAN    2     1000022        35   # BR(~chi_30 -> ~chi_10   H )
                NAN    2     1000022        36   # BR(~chi_30 -> ~chi_10   A )
                NAN    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
                NAN    2     1000023        35   # BR(~chi_30 -> ~chi_20   H )
                NAN    2     1000023        36   # BR(~chi_30 -> ~chi_20   A )
                NAN    2     1000024       -37   # BR(~chi_30 -> ~chi_1+   H-)
                NAN    2    -1000024        37   # BR(~chi_30 -> ~chi_1-   H+)
                NAN    2     1000037       -37   # BR(~chi_30 -> ~chi_2+   H-)
                NAN    2    -1000037        37   # BR(~chi_30 -> ~chi_2-   H+)
                NAN    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
                NAN    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
                NAN    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
                NAN    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
                NAN    2     1000001        -1   # BR(~chi_30 -> ~d_L      db)
                NAN    2    -1000001         1   # BR(~chi_30 -> ~d_L*     d )
                NAN    2     2000001        -1   # BR(~chi_30 -> ~d_R      db)
                NAN    2    -2000001         1   # BR(~chi_30 -> ~d_R*     d )
                NAN    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
                NAN    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
                NAN    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
                NAN    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
                NAN    2     1000003        -3   # BR(~chi_30 -> ~s_L      sb)
                NAN    2    -1000003         3   # BR(~chi_30 -> ~s_L*     s )
                NAN    2     2000003        -3   # BR(~chi_30 -> ~s_R      sb)
                NAN    2    -2000003         3   # BR(~chi_30 -> ~s_R*     s )
                NAN    2     1000006        -6   # BR(~chi_30 -> ~t_1      tb)
                NAN    2    -1000006         6   # BR(~chi_30 -> ~t_1*     t )
                NAN    2     2000006        -6   # BR(~chi_30 -> ~t_2      tb)
                NAN    2    -2000006         6   # BR(~chi_30 -> ~t_2*     t )
                NAN    2     1000005        -5   # BR(~chi_30 -> ~b_1      bb)
                NAN    2    -1000005         5   # BR(~chi_30 -> ~b_1*     b )
                NAN    2     2000005        -5   # BR(~chi_30 -> ~b_2      bb)
                NAN    2    -2000005         5   # BR(~chi_30 -> ~b_2*     b )
                NAN    2     1000011       -11   # BR(~chi_30 -> ~e_L-     e+)
                NAN    2    -1000011        11   # BR(~chi_30 -> ~e_L+     e-)
                NAN    2     2000011       -11   # BR(~chi_30 -> ~e_R-     e+)
                NAN    2    -2000011        11   # BR(~chi_30 -> ~e_R+     e-)
                NAN    2     1000013       -13   # BR(~chi_30 -> ~mu_L-    mu+)
                NAN    2    -1000013        13   # BR(~chi_30 -> ~mu_L+    mu-)
                NAN    2     2000013       -13   # BR(~chi_30 -> ~mu_R-    mu+)
                NAN    2    -2000013        13   # BR(~chi_30 -> ~mu_R+    mu-)
                NAN    2     1000015       -15   # BR(~chi_30 -> ~tau_1-   tau+)
                NAN    2    -1000015        15   # BR(~chi_30 -> ~tau_1+   tau-)
                NAN    2     2000015       -15   # BR(~chi_30 -> ~tau_2-   tau+)
                NAN    2    -2000015        15   # BR(~chi_30 -> ~tau_2+   tau-)
                NAN    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
                NAN    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
                NAN    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
                NAN    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
                NAN    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
                NAN    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035                NAN   # neutralino4 decays
#          BR         NDA      ID1       ID2
                NAN    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
                NAN    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
                NAN    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
                NAN    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
                NAN    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
                NAN    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
                NAN    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
                NAN    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
                NAN    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
                NAN    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
                NAN    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
                NAN    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
                NAN    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
                NAN    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
                NAN    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
                NAN    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
                NAN    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
                NAN    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
                NAN    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
                NAN    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
                NAN    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
                NAN    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
                NAN    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
                NAN    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
                NAN    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
                NAN    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
                NAN    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
                NAN    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
                NAN    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
                NAN    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
                NAN    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
                NAN    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
                NAN    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
                NAN    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
                NAN    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
                NAN    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
                NAN    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
                NAN    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
                NAN    2     2000006        -6   # BR(~chi_40 -> ~t_2      tb)
                NAN    2    -2000006         6   # BR(~chi_40 -> ~t_2*     t )
                NAN    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
                NAN    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
                NAN    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
                NAN    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
                NAN    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
                NAN    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
                NAN    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
                NAN    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
                NAN    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
                NAN    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
                NAN    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
                NAN    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
                NAN    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
                NAN    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
                NAN    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
                NAN    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
                NAN    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
                NAN    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
                NAN    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
                NAN    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
                NAN    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
                NAN    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
