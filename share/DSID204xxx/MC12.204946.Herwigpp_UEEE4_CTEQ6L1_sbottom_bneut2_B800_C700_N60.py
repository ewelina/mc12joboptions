from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.sbotom_bneut2')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py' )

slha_file = 'susy_sbottom_bneut2_B800_C700_N60.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['sbottom1'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'Sbottom sb->t+chargino grid generation with m_sbottom = 800, m_chargino = 700, m_neutralino = 60 GeV'
evgenConfig.keywords = ['SUSY','sbottom']
evgenConfig.contact  = ['antoine.marzin@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()


# clean up
del cmds
