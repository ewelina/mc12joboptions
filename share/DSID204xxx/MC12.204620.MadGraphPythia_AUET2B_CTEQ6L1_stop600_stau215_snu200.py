include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pystat 1 3 4 5",
                                 "pyinit dumpr 1 5",
                                 "pyinit pylistf 1",
                                 ]

evgenConfig.inputfilecheck = 'madgraph.204620.stop600_stau215_snu200'
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = 'MRSSM stop'
evgenConfig.keywords = ["SUSY", "stop", "MRSSM", "stau"]                           
evgenConfig.contact = ["Takash Yamanaka"]

phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0
      imss(21)=24
      imss(22)=24 
    """%(runArgs)

phojf.write(phojinp)
phojf.close()
