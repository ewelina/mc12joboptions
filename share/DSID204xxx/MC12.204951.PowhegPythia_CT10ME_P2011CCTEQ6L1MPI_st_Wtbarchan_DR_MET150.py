#--------------------------------------------------------------
# Powheg Wt with diagram removal setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Wt_DR_Common.py')

PowhegConfig.ttype =-1 # tbar
PowhegConfig.nEvents *=100
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 showering
#--------------------------------------------------------------
include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 W+tbar with 150 GeV Etmiss filter'
evgenConfig.keywords    = [ 'W', 'top' ]
evgenConfig.contact     = [ 'stefania.stucci@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia' ]

## Include METFilter
include ( 'MC12JobOptions/METFilter.py' )
topAlg.METFilter.MissingEtCut = 150*GeV
