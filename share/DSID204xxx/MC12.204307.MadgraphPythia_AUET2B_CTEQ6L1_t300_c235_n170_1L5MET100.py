include('MC12JobOptions/MadGraphControl_SM_TT_onestepBB.py')
include('MC12JobOptions/METFilter.py')
topAlg.METFilter.MissingEtCut = 100*GeV
include('MC12JobOptions/LeptonFilter.py')
topAlg.LeptonFilter.Ptcut = 5000.
