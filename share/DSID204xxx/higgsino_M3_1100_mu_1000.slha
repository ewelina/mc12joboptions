#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.00000000E+03   # M_1                 
         2     2.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.00000000E+03   # mu(EWSB)            
        25     2.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.00000000E+03   # M_eL                
        32     2.00000000E+03   # M_muL               
        33     2.00000000E+03   # M_tauL              
        34     2.00000000E+03   # M_eR                
        35     2.00000000E+03   # M_muR               
        36     2.00000000E+03   # M_tauR              
        41     2.00000000E+03   # M_q1L               
        42     2.00000000E+03   # M_q2L               
        43     2.00000000E+03   # M_q3L               
        44     2.00000000E+03   # M_uR                
        45     2.00000000E+03   # M_cR                
        46     2.00000000E+03   # M_tR                
        47     2.00000000E+03   # M_dR                
        48     2.00000000E+03   # M_sR                
        49     2.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05304642E+01   # W+
        25     1.20000000E+02   # h
        35     2.00273434E+03   # H
        36     2.00000000E+03   # A
        37     2.00203925E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.00052385E+03   # ~d_L
   2000001     2.00009925E+03   # ~d_R
   1000002     1.99957530E+03   # ~u_L
   2000002     1.99980148E+03   # ~u_R
   1000003     2.00052385E+03   # ~s_L
   2000003     2.00009925E+03   # ~s_R
   1000004     1.99957530E+03   # ~c_L
   2000004     1.99980148E+03   # ~c_R
   1000005     1.99893950E+03   # ~b_1
   2000005     2.00168636E+03   # ~b_2
   1000006     1.98611421E+03   # ~t_1
   2000006     2.02559018E+03   # ~t_2
   1000011     2.00032537E+03   # ~e_L
   2000011     2.00029775E+03   # ~e_R
   1000012     1.99937673E+03   # ~nu_eL
   1000013     2.00032537E+03   # ~mu_L
   2000013     2.00029775E+03   # ~mu_R
   1000014     1.99937673E+03   # ~nu_muL
   1000015     1.99942219E+03   # ~tau_1
   2000015     2.00120212E+03   # ~tau_2
   1000016     1.99937673E+03   # ~nu_tauL
   1000021     1.10000000E+03   # ~g
   1000022     9.92580088E+02   # ~chi_10
   1000023    -1.00027729E+03   # ~chi_20
   1000025     2.00000076E+03   # ~chi_30
   1000035     2.00769744E+03   # ~chi_40
   1000024     9.94551375E+02   # ~chi_1+
   1000037     2.00587018E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.17862640E-02   # N_11
  1  2    -7.45808587E-02   # N_12
  1  3     7.05392079E-01   # N_13
  1  4    -7.03643104E-01   # N_14
  2  1     4.70171057E-03   # N_21
  2  2    -8.39169016E-03   # N_22
  2  3    -7.06779955E-01   # N_23
  2  4    -7.07368057E-01   # N_24
  3  1     8.72386690E-01   # N_31
  3  2     4.88816391E-01   # N_32
  3  3     2.57463299E-06   # N_33
  3  4    -3.94310261E-06   # N_34
  4  1     4.87026622E-01   # N_41
  4  2    -8.69139889E-01   # N_42
  4  3    -5.37039067E-02   # N_43
  4  4     6.72072411E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.64461813E-02   # U_11
  1  2     9.97790010E-01   # U_12
  2  1     9.97790010E-01   # U_21
  2  2     6.64461813E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -8.31622390E-02   # V_11
  1  2     9.96536021E-01   # V_12
  2  1     9.96536021E-01   # V_21
  2  2     8.31622390E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.09123369E-01   # cos(theta_t)
  1  2     7.05084426E-01   # sin(theta_t)
  2  1    -7.05084426E-01   # -sin(theta_t)
  2  2     7.09123369E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.50164255E-01   # cos(theta_b)
  1  2     7.59793684E-01   # sin(theta_b)
  2  1    -7.59793684E-01   # -sin(theta_b)
  2  2     6.50164255E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.01598277E-01   # cos(theta_tau)
  1  2     7.12572703E-01   # sin(theta_tau)
  2  1    -7.12572703E-01   # -sin(theta_tau)
  2  2     7.01598277E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -4.65494977E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.00000000E+03   # mu(Q)               
         2     1.99999975E+00   # tanbeta(Q)          
         3     2.51967321E+02   # vev(Q)              
         4     3.74537251E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53656715E-01   # gprime(Q) DRbar
     2     6.31212096E-01   # g(Q) DRbar
     3     1.10524108E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.93757528E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.40651154E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.23395823E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.00000000E+03   # M_1                 
         2     2.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.22214079E+06   # M^2_Hd              
        22    -7.79230147E+05   # M^2_Hu              
        31     2.00000000E+03   # M_eL                
        32     2.00000000E+03   # M_muL               
        33     2.00000000E+03   # M_tauL              
        34     2.00000000E+03   # M_eR                
        35     2.00000000E+03   # M_muR               
        36     2.00000000E+03   # M_tauR              
        41     2.00000000E+03   # M_q1L               
        42     2.00000000E+03   # M_q2L               
        43     2.00000000E+03   # M_q3L               
        44     2.00000000E+03   # M_uR                
        45     2.00000000E+03   # M_cR                
        46     2.00000000E+03   # M_tR                
        47     2.00000000E+03   # M_dR                
        48     2.00000000E+03   # M_sR                
        49     2.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.56637637E-06   # gluino decays
#          BR         NDA      ID1       ID2
     5.52335772E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     4.43966463E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
#           BR         NDA      ID1       ID2       ID3
     3.20333236E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     9.51986320E-07    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.48169358E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     7.37532125E-07    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     3.20333236E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     9.51986320E-07    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.48169358E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     7.37532125E-07    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     4.96069775E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.83627021E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.69420895E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     4.69420895E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     4.69420895E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     4.69420895E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
#
#         PDG            Width
DECAY   1000006     1.02239040E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     9.69722925E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.16662895E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.17455324E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     6.68909489E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     8.64127220E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.33931817E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.12904212E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     0.00000000E+00    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.19046241E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     4.28242661E-05    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     6.34074906E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     7.22327590E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     6.90562533E-04    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     1.45929239E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.24167027E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     8.74996481E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     7.53933886E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.01838853E-04    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.09493154E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.64589808E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     8.35098860E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     6.31025033E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.18028166E-04    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     3.98544129E-06    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     9.83934934E-04    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.98694051E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     6.30440384E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     6.95805200E-05    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     8.71967853E-07    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     9.99929548E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     6.31725245E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.84063851E-04    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.06623448E-06    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.64432587E-09    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.28129306E-04    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     9.98881737E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     6.30663822E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.73949417E-05    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.17990484E-07    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.29441318E-10    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.99982387E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     6.31025033E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.18028166E-04    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     3.98544129E-06    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     9.83934934E-04    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.98694051E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     6.30440384E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     6.95805200E-05    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     8.71967853E-07    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     9.99929548E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     6.31725245E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.84063851E-04    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.06623448E-06    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.64432587E-09    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.28129306E-04    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     9.98881737E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     6.30663822E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.73949417E-05    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.17990484E-07    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.29441318E-10    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.99982387E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     5.16136000E-02   # selectron_L decays
#          BR         NDA      ID1       ID2
     2.28497899E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.86350053E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.54603976E-05    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     7.68623140E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.00000123E-02   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.87557273E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.23759350E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.67922384E-05    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     5.16136000E-02   # smuon_L decays
#          BR         NDA      ID1       ID2
     2.28497899E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.86350053E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.54603976E-05    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     7.68623140E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.00000123E-02   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.87557273E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.23759350E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.67922384E-05    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     8.21727478E-02   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.88763533E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     4.83961931E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     5.62840274E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.29200364E-02   # stau_2 decays
#          BR         NDA      ID1       ID2
     6.84314846E-02    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.76875883E-01    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.54692632E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     1.05822594E-01   # snu_eL decays
#          BR         NDA      ID1       ID2
     4.08293307E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.11660774E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     5.86590085E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     1.05822594E-01   # snu_muL decays
#          BR         NDA      ID1       ID2
     4.08293307E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.11660774E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     5.86590085E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     1.16953245E-01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.69435297E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     4.62965096E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.25935052E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.04080406E-11   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.74918397E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.74918397E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24972938E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24972938E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.17330278E-04    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     2.37475384E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.91658475E-05    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     2.83362328E-05    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.91658475E-05    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     2.83362328E-05    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     1.40875451E-04    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.38905455E-05    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     1.38905455E-05    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     1.33447047E-05    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     1.01587352E-05    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     1.01587352E-05    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     6.72689357E-06    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     3.67452205E-06    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     2.54364739E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.47376222E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.47505621E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.50405694E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     6.23026478E-08   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.40814885E-01    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     4.82900448E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     6.24852677E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     4.82900448E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     6.24852677E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.42514740E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.42514740E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     9.94685148E-03    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.84472272E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.84472272E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.84472272E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.71636588E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.71636588E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.71636588E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.71636588E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.23879003E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.23879003E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.23879003E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.23879003E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     7.81838627E-03    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     7.81838627E-03    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.11949704E+01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     8.89508739E-11    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.98194107E-09    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     4.99999800E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.99999800E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     5.92935888E-10    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     7.37904661E-12    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
     8.16446855E-08    2     1000002        -2   # BR(~chi_30 -> ~u_L      ub)
     8.16446855E-08    2    -1000002         2   # BR(~chi_30 -> ~u_L*     u )
     1.79119058E-08    2     2000002        -2   # BR(~chi_30 -> ~u_R      ub)
     1.79119058E-08    2    -2000002         2   # BR(~chi_30 -> ~u_R*     u )
     8.16446855E-08    2     1000004        -4   # BR(~chi_30 -> ~c_L      cb)
     8.16446855E-08    2    -1000004         4   # BR(~chi_30 -> ~c_L*     c )
     1.79119058E-08    2     2000004        -4   # BR(~chi_30 -> ~c_R      cb)
     1.79119058E-08    2    -2000004         4   # BR(~chi_30 -> ~c_R*     c )
     1.58523472E-16    2     1000012       -12   # BR(~chi_30 -> ~nu_eL    nu_eb)
     1.58523472E-16    2    -1000012        12   # BR(~chi_30 -> ~nu_eL*   nu_e )
     1.58523472E-16    2     1000014       -14   # BR(~chi_30 -> ~nu_muL   nu_mub)
     1.58523472E-16    2    -1000014        14   # BR(~chi_30 -> ~nu_muL*  nu_mu )
     1.58523472E-16    2     1000016       -16   # BR(~chi_30 -> ~nu_tau1  nu_taub)
     1.58523472E-16    2    -1000016        16   # BR(~chi_30 -> ~nu_tau1* nu_tau )
#
#         PDG            Width
DECAY   1000035     1.99250905E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.90692897E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     3.90681793E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.07427092E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.07427092E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.84339036E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     4.78998221E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     2.36509500E-05    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.36509500E-05    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.88652034E-06    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.88652034E-06    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.80966090E-05    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.80966090E-05    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.13139610E-06    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.13139610E-06    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.36509500E-05    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.36509500E-05    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.88652034E-06    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.88652034E-06    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.80966090E-05    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.80966090E-05    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.13139610E-06    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.13139610E-06    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.87904136E-05    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     1.87904136E-05    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     5.08928940E-06    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     5.08928940E-06    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     3.81458677E-06    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     3.81458677E-06    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     3.21947978E-06    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     3.21947978E-06    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     3.81458677E-06    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     3.81458677E-06    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     3.21947978E-06    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     3.21947978E-06    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.18094834E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.18094834E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.92549275E-06    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.92549275E-06    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     1.78173299E-05    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.78173299E-05    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.78173299E-05    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.78173299E-05    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.78173299E-05    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.78173299E-05    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
