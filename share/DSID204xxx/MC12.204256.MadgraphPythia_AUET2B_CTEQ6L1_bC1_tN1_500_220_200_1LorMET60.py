include( 'MC12JobOptions/MadGraphControl_SM_TT_mixedBT.py' )
include( 'MC12JobOptions/PtmissAndOrLeptonFilter.py' )
topAlg.PtmissAndOrLeptonFilter.PtminElectron = 20*GeV
topAlg.PtmissAndOrLeptonFilter.PtminMuon = 20*GeV
topAlg.PtmissAndOrLeptonFilter.PtminMissing = 60*GeV
evgenConfig.inputconfcheck = "bC1_tN1_500_220_200"
