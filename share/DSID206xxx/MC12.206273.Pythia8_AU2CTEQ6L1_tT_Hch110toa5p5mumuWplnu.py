# Responsible: Chris Potter
# 23 Jan. 2014
# W+ -> e/mu/tau nu, mH+=110 GeV, ma1=5.5 GeV, tan beta=1

evgenConfig.description = "NMSSM H+->a1W from ttbar, t->bH+"
evgenConfig.keywords = ["Higgs","NMSSM","BSM"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["Top:gg2ttbar=on",
                            "Higgs:useBSM=on",
                            "37:m0 = 110.",
                            "25:m0 = 5.5",
                            "25:mMin = 4.",
                            "25:mMax = 10.",
                            "HiggsH1:parity=2",
                            "HiggsHchg:tanBeta=1.",
                            "6:onMode=3",
                            "6:onIfAny=37",
                            "24:onMode=3", # W+ ->e/mu/tau nu 
                            "24:onIfAny=11 13 15",
                            "37:onMode=off",
                            "37:onIfAny=25",
                            "25:onMode=off",
                            "25:onIfAny=13"]                           

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

topAlg.MultiLeptonFilter.Ptcut = 2000.
topAlg.MultiLeptonFilter.Etacut = 3.0
topAlg.MultiLeptonFilter.NLeptons = 3
    
StreamEVGEN.RequireAlgs += ["MultiLeptonFilter"]
