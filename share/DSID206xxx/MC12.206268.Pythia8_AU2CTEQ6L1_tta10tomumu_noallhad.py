# Responsible: Ben Allen
# 14 Aug. 2014
# pp->tta1, a1->mu+mu-,W+ ->e/mu/tau nu, W- -> hadronic, ma1=10 GeV,

evgenConfig.description = "NMSSM pp->tta1, a1->mumu"
evgenConfig.keywords = ["Higgs","NMSSM","BSM"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["Higgs:useBSM=on",
		            "HiggsBSM:gg2A3ttbar=on",
                            "36:m0 = 10.",
                            "6:onMode=0",
                            "6:onIfAny=5",
                            "24:onMode=3", # W+ ->e/mu/tau nu 
                            "24:onPosIfAny=11 13 15",
                            "36:onMode=0",
                            "36:onIfAny=13"]                       


