evgenConfig.description = 'MadGraph High mass charged Higgs NLO4FS'
evgenConfig.keywords+=['Higgs','MSSM','BSMHiggs','chargedHiggs']
evgenConfig.contact = ['Haleh.Hadavand@cern.ch']
evgenConfig.inputfilecheck = "Hplus4FS_H275"
evgenConfig.generators += ["MadGraph","Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["Higgs:useBSM = on",
                             "37:onMode = off",                   # turn off all mhc decays
                             "37:onIfMatch = 5 6"]                # switch on H+ to tb

include('MC12JobOptions/TTbarWToLeptonFilter.py')                # lep filter   
topAlg.TTbarWToLeptonFilter.NumLeptons = -1 
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
