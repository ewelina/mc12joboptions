evgenConfig.description = "POWHEG+PYTHIA8 ggH H->bb with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "bottom"]
evgenConfig.inputfilecheck = "ggH_SM_M125"
evgenConfig.minevents=1000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',
                            '25:onIfMatch = 5 5'
                            ]

from GeneratorFilters.GeneratorFiltersConf import HiggsFilter

topAlg += HiggsFilter("HiggsPtFilter")                                                                                                                                                                                                  

HiggsPtFilter = topAlg.HiggsPtFilter
HiggsPtFilter.OutputLevel = INFO;
HiggsPtFilter.Ptcut = 120.*GeV
HiggsPtFilter.Ptmaxcut = 200.*GeV

StreamEVGEN.RequireAlgs += [ "HiggsPtFilter" ]

