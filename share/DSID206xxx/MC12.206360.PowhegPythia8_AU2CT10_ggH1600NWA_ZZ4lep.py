#--------------------------------------------------------------
# Powheg ggH_quark_mass_effects setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_ggH_quark_mass_effects_Common.py')

# Set Powheg variables, overriding defaults
PowhegConfig.mass_H  = 1600.
PowhegConfig.width_H = 0.00407

# Turn on the heavy quark effect
PowhegConfig.use_massive_b = True
PowhegConfig.use_massive_c = True

# Complex pole scheme or not (1 for NWA and 3(CPS) for SM)
PowhegConfig.bwshape = 1

# Dynamical scale (sqrt(pT(H)^2+mH^2) real emission contributions)
# PowhegConfig.runningscale = 2 #r2330 does not support this option.

# EW correction
if PowhegConfig.mass_H <= 1000.:
  PowhegConfig.ew = 1
else:
  PowhegConfig.ew = 0

# Set scaling and masswindow parameters
hfact_scale    = 1.2
masswindow_max = 30.

# Calculate an appropriate masswindow and hfact
masswindow = masswindow_max
if PowhegConfig.mass_H <= 700.:
  masswindow = min( (1799.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
else:
  masswindow = min( (1999.9 - PowhegConfig.mass_H) / PowhegConfig.width_H, masswindow )
PowhegConfig.masswindow = masswindow
PowhegConfig.hfact = PowhegConfig.mass_H / hfact_scale

# Increase number of events requested to compensate for filter efficiency
PowhegConfig.nEvents *= 2.

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfMatch = 11 11',
                            '23:onIfMatch = 13 13',
                            '23:onIfMatch = 15 15'
                            ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8 using NWA, ggH H->ZZ->4l with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "Z","leptonic"]
evgenConfig.contact     = [ 'Junichi.Tanaka@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
