include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "bbjj QCD production + upto 3 jets at NLO"
evgenConfig.keywords = [ "QCD", "multi-jets", "bbjj" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","edson.carquin.lopez@cern.ch" ]
evgenConfig.minevents = 5000

evgenConfig.process="""
(processes){
  Process 93 93 -> 5 -5 93 93 93{1};
  Order_EW 0
  RESULT_DIRECTORY = Results/
  CKKW sqr(30/E_CMS)
  Integration_Error 0.1 {3,4,5}
  Max_N_Quarks 5 #Should change to 6 to include eventually ttbb ?
  End process
}(processes)

(selector){
  PT 5 25 E_CMS
  PT -5 25 E_CMS
  PT 93 25 E_CMS

  DeltaR 5 -5 0.4 99
  DeltaR 5 5 0.4 99
  DeltaR -5 -5 0.4 99

  DeltaR 93 93 0.4 99
  DeltaR 93 5 0.4 99
  DeltaR 93 -5 0.4 99

  PseudoRapidity 5 -2.6 2.6
  PseudoRapidity -5 -2.6 2.6
  PseudoRapidity 93 -5 5

  Mass 5 -5 60 160

  "Calc(sqrt(PPerp2(p[0]+p[1])))" 5,-5 100,E_CMS

}(selector)
"""
# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
