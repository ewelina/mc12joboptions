include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')

evgenConfig.inputfilecheck = "group.phys-gener.madgraph5.206563.H1000_hh_13TeV"
evgenConfig.description = "diHiggs production through 1000GeV Heavy Higgs resonance, decay to yybb, with Madgraph."
evgenConfig.keywords =["BSM", "BSMHiggs", "resonance", "diphoton", "bottom"]
evgenConfig.contact = ["Stefan von Buddenbrock"]

#Pythia8 Commands
topAlg.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ", #bb decay
                            "25:addChannel = on 0.5 100 22 22 "] #gammagamma decay

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 1560
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [5]
topAlg.XtoVVDecayFilter.PDGChild2 = [22]
StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]

