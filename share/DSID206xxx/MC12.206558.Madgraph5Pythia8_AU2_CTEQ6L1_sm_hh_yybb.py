include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')

evgenConfig.inputfilecheck = "group.phys-gener.madgraph5.206558.sm_hh_13TeV"
evgenConfig.description = "SM diHiggs production, decay to yybb, with Madgraph, inclusive of box diagram."
evgenConfig.keywords =["SM", "SMHiggs", "nonResonant", "diphoton", "bottom"]
evgenConfig.contact = ["Stefan von Buddenbrock"]

#Pythia8 Commands
topAlg.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ", #bb decay
                            "25:addChannel = on 0.5 100 22 22 "] #gammagamma decay

#Generator Filters
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
topAlg += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
topAlg += ParentChildFilter("hyyFilter", PDGParent = [25], PDGChild = [22])
StreamEVGEN.RequireAlgs = ["hbbFilter", "hyyFilter"]

