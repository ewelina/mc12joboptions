evgenConfig.description = "Standard model lvqqyy2 background sample for hh->wwyy analysis"
evgenConfig.keywords = ["lvqqyy2" , "semileptonic"]
evgenConfig.contact = ["Xiauho Sun <xiaohu.sun@cern.ch>"]

include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')
