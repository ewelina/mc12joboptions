evgenConfig.description = 'POWHEG+Pythia8 H+Z+jet production with AU2 CT10 tune'
evgenConfig.keywords    = [ 'Higgs' ]
evgenConfig.contact     = [ 'carlo.enrico.pandini@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.inputfilecheck = 'ZH125J_MINLO_mumubb_VpT_Weighted'

include('MC12JobOptions/Pythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Powheg_Main31.py')

topAlg.Pythia8.Commands += [ '25:onMode = off',
                             '25:onIfMatch = 5 5' # H->bb
                           ]
topAlg.Pythia8.UserModes += ['Main31:NFinal = 3']
