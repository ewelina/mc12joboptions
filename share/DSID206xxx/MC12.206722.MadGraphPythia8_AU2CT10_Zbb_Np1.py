from MadGraphControl.MadGraphUtils import *


# Nevents
minevents=5000
nevents=20000
lhe_version=2


# Parameters to override MG5 defaults
ptjcut=20
ptbcut=20
mllcut=0
mmjjcut=0
dRjjcut=0.
dRllcut=0.
dRjlcut=0.4
pTlcut=0.
etalcut=10
etabcut=6
etajcut=6

maxjetflavor=5
asrwgtflavor=5


# Matching/merging options
ickkw=0
cut_decays='F'

nJetMax=-1
ktdurham=-1
dparameter=-1


#DSIDs                
Zbb_Np0   = [206721]
Zbb_Np1   = [206722]
Zbb_Np2   = [206723]
Zbb_Np3   = [206724]





    
#Internal PDF options below - but should always use LHAPDF where possible
PDF='CT10'
lhapdfid=10800
pdflabel='ct10'
                      
fcard = open('proc_card_mg5.dat','w')

#Zbb Np0 Pythia8 KTMerging
if runArgs.runNumber in Zbb_Np0:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > z @0
    output -f
    """)
    fcard.close()
  
    name="Zbb_Np0_smnobmass_Pythia8"

#    mllcut=40
    
    process="pp>z"
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','b quarks','jets']

#Zbb Np1 Pythia8 KTMerging
elif runArgs.runNumber in Zbb_Np1:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > z j @1
    output -f
    """)
    fcard.close()
  
    name="Zbb_Np1_smnobmass_Pythia8"

#    mllcut=40
    
    process="pp>z"
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','b quarks','jets']

#Zbb Np2 Pythia8 KTMerging
elif runArgs.runNumber in Zbb_Np2:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > z j j @2
    output -f
    """)
    fcard.close()
  
    name="Zbb_Np2_smnobmass_Pythia8"

#    mllcut=40
    
    process="pp>z"
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','b quarks','jets']

#Zbb Np3 Pythia8 KTMerging
elif runArgs.runNumber in Zbb_Np3:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > z j j j @3
    output -f
    """)
    fcard.close()
  
    name="Zbb_Np3_smnobmass_Pythia8"

    mllcut=40
    
    process="pp>z"
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','b quarks','jets']

else:
    raise RuntimeError("DSID %i not recognised."%(int(runArgs.runNumber)))



    
beamEnergy=0
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")



# Grab the run card and move it into place
if os.access(os.environ['MADPATH']+'/Template/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/run_card.dat','run_card.SM.dat')
elif os.access(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat','run_card.SM.dat')
else:
    raise RuntimeError('Cannot find Template run_card.dat!')
    

    
if not os.access('run_card.SM.dat',os.R_OK):
    raise RuntimeError('Cannot read fetched run_card.dat!')
elif os.access('run_card.dat',os.R_OK):
    raise RuntimeError('Old run card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw' in line and ickkw!=0:
            newcard.write(' %i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ' = xqcut ' in line and ickkw==1 and xqcut>0:
                newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))               
        elif ' = ptj ' in line and ptjcut!=20:
            newcard.write(' %f  = ptj       ! minimum pt for the jets \n'%(ptjcut))
        elif ' = mmjj ' in line and mmjjcut>0:
            newcard.write(' %f = mmjj    ! min invariant mass of a jet pair\n'%(mmjjcut))
        elif ' = drjj ' in line and dRjjcut!=0.4:
            newcard.write(' %f = drjj    ! min distance between jets \n'%(dRjjcut))
        elif ' = ptb ' in line and ptbcut>0.:
            newcard.write(' %f  = ptb       ! minimum pt for the b \n'%(ptbcut))
        elif ' = maxjetflavor' in line and maxjetflavor!=4:
            newcard.write(' %i = maxjetflavor \n'%(maxjetflavor))
        elif ' = asrwgtflavor ' in line and asrwgtflavor!=5: 
            newcard.write(' %i        = asrwgtflavor     ! highest quark flavor for a_s reweight\n'%(asrwgtflavor))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = cut_decays' in line and cut_decays!='T':
            newcard.write(' %s = cut_decays \n'%(cut_decays))
        elif ' = mmll ' in line and mllcut>0:
            newcard.write(' %f = mmll    ! min invariant mass of l+l- (same flavour) lepton pair \n'%(mllcut))
        elif ' = drll ' in line and dRllcut!=0.4:
            newcard.write(' %f = drll    ! min distance between leptons \n'%(dRllcut))
        elif ' = ptl ' in line and pTlcut!=10:
            newcard.write(' %f = ptl       ! minimum pt for the charged leptons \n'%(pTlcut))
        elif ' = etal ' in line and etalcut!=2.5:
            newcard.write(' %f = etal    ! max rap for the charged leptons \n'%(etalcut))
        elif ' = drjl ' in line and dRjlcut!=0.4:
            newcard.write(' %f = drjl    ! min distance between jet and lepton \n'%(dRjlcut))
        elif ' = etab ' in line and etabcut!=-1:
            newcard.write(' %f = etab    ! max rap for the b \n'%(etabcut))
        elif ' = etaj ' in line and etajcut!=5:
           newcard.write(' %f = etaj    ! max rap for the jets \n'%(etajcut))
        elif ' =  ktdurham' in line and ktdurham!=-1:
            newcard.write(' %i =  ktdurham\n'%(ktdurham))
        elif ' =  dparameter' in line and dparameter!=0.4:
            newcard.write(' %f =  dparameter\n'%(dparameter))
        elif ' = lhe_version' in line:
            newcard.write(' %i.0      = lhe_version       ! Change the way clustering information pass to shower.\n'%lhe_version)
        elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
        elif lhapdfid>0 and '= lhaid ' in line:
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
        elif lhapdfid==-1 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'%s\'    = pdlabel     ! PDF set\n'%(pdflabel))

        else:
            newcard.write(line)
            

    oldcard.close()
    newcard.close()


   
stringy=''
   
#Print out cards for debugging
procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
procCard.wait()

runCard = subprocess.Popen(['cat','run_card.dat'])
runCard.wait()



## Generate process 
process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc=None,mode=1,njobs=1,proc_dir=process_dir)

## Pass generated events to Pythia8 (if not generating gridpack)
print 'Pass generated events to Pythia8'

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz')



include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += [
    "PDF:useLHAPDF = off"]
    

include ("MC12JobOptions/Pythia8_MadGraph.py")



topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "Beams:frameType = 4"]


topAlg.Pythia8.Commands += ["23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 5"] # switch on Z->bb decays   ]  


topAlg.Pythia8.Commands += ["Merging:doKTMerging = on",
                            "Merging:ktType = 1",
                            "Merging:nJetMax = %i"%nJetMax,
                            "Merging:Process = %s"%process,
                            "Merging:TMS = %f"%ktdurham,
                            "Merging:Dparameter = %f"%dparameter,
                            "Merging:nQuarksMerge = %i"%maxjetflavor]
    


evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords += ["SM","validation"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = minevents
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
    

