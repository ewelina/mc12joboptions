evgenConfig.description = "POWHEG+PYTHIA8 using NWA, VBF H->hh->bbtautau with AU2,CT10 "
evgenConfig.keywords = ["nonSMhiggs", "VBF", "di-higgs"]
evgenConfig.contact  = [ "Xiaohu.Sun@cern.ch"]

evgenConfig.inputfilecheck = "VBFH35_NW_M500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            'Higgs:useBSM on',
                            '35:onMode = off',#decay of Higgs
                            '35:onIfMatch = 25 25',
                            '25:onMode = off',#decay of Higgs
                            "25:oneChannel = on 0.5 100 15 -15",
                            "25:addChannel = on 0.5 100 5 -5 "]

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 35
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [15]
topAlg.XtoVVDecayFilter.PDGChild2 = [5]

StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]
