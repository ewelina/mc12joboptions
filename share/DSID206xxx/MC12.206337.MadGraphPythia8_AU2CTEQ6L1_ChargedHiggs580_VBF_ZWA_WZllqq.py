evgenConfig.description = "MadGraph5+Pythia8 for VBF > ChargedHiggs > WZ"
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.inputfilecheck = "Hplus_VBF_ZWA"
evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += [
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',                           
                            '24:onIfAny = 1 2 3 4 5 6',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfAny = 11 13 15'
                            ]
