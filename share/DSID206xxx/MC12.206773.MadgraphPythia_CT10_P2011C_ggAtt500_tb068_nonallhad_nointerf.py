include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "AFII MadGraph(CT10) Pythia Perugia 2011c gg->A->ttbar no interference, singlelepton+dilepton, m(A)=500GeV, 2HDM Type2, tan(beta)=0.68, sin(beta-alpha)=1.0, 8TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["ggAtt", "ttbar", "singlelepton", "dilepton" ]
evgenConfig.inputfilecheck = "madgraph.206773.ggAtt"
evgenConfig.contact  = ["madalina.stanescu.bellu@cern.ch"]
evgenConfig.minevents=5000

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


