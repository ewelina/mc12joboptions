include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')

evgenConfig.description = "bbH->hh with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["H","2HDM","bbH","Hhh"]
evgenConfig.contact  = [ "Xiaohu.Sun@cern.ch"]

topAlg.Pythia8.Commands += ["Higgs:useBSM on",
                            "HiggsBSM:gg2H2bbbar on",
                            "25:m0 125.0",
                            "35:m0 500.0", #H mass
                            "35:onMode = off", #turn off all H decays
                            "35:onIfMatch = 25 25", 
                            "SLHA:file = narrowWidth.slha",
                            "25:onMode = off", #turn off all H decays
                            "25:oneChannel = on 0.5 100 15 -15",
                            "25:addChannel = on 0.5 100 5 -5"
                            ] 

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 35
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [15]
topAlg.XtoVVDecayFilter.PDGChild2 = [5]

StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]

# set width for heavy Higgs
nwf=open('./narrowWidth.slha', 'w')
nwfinput = """#           PDG      WIDTH
DECAY   35  0.01
#          BR         NDA          ID1       ID2       ID3       ID4
1       2       25      25
"""
nwf.write(nwfinput)
nwf.close()

