evgenConfig.description = "mcfm 6.8, gg->WW->e+nu e-nu (sig+bkg with SM Width) using CT10nnlo PDF and PowhegPythia8 with AU2 CT10 tune"
evgenConfig.keywords    = ["ggWW", "mcfm", "W","leptonic"]
evgenConfig.contact     = ["Yanyan Gao <Yanyan.Gao@cern.ch>"]

TMPBASEIF = "ggH125p5_gg_WW_sigbkg_ee_2pt3"
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '24:onMode = off',#decay of W
                           ]
