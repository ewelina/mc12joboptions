include ("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "25:onMode = off",
                            "25:onIfMatch = 5 5" ]

## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "Standard Model Madgraph VBF Higgs -> b b jet jet gamma"
evgenConfig.keywords = ["Hbbjja"]
evgenConfig.inputfilecheck = 'VBFH125photon'
