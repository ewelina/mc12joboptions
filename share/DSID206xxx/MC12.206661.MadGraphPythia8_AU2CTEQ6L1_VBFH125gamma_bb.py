include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',
                            '25:onIfMatch = 5 5'
                            ]



evgenConfig.description = "VBFHiggs+gamma"
evgenConfig.keywords = ["SMhiggs","VBF","bb","photon"]
evgenConfig.inputfilecheck = 'VBFHgamma_SM_M125'
evgenConfig.contact = ["prose@ucsc.edu"]
