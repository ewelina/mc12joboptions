# Responsible: Chris Potter
# 12 Sept. 2014
# W- -> e/mu/tau nu, mH+=130 GeV, ma1=7 GeV, tan beta=1, a1->tautau->mumu

evgenConfig.description = "NMSSM H+->a1W from ttbar, t->bH+"
evgenConfig.keywords = ["Higgs","NMSSM","BSM"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["Top:gg2ttbar=on",
                            "Higgs:useBSM=on",
                            "37:m0 = 130.",
                            "25:m0 = 7.",
                            "25:mMin = 4.",
                            "25:mMax = 10.",
                            "HiggsH1:parity=2",
                            "HiggsHchg:tanBeta=1.",
                            "6:onMode=3",
                            "6:onIfAny=37",
                            "24:onMode=2", # W- ->e/mu nu 
                            "24:onIfAny=11 13",
                            "37:onMode=off",
                            "37:onIfAny=25",
                            "25:onMode=off",
                            "25:onIfMatch=15 15",
                            "15:onMode=off",
                            "15:onIfMatch=13 14 16"]

include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [13]
topAlg.XtoVVDecayFilter.PDGChild2 = [13]

