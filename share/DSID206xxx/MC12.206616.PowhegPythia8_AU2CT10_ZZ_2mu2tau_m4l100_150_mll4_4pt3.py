include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="ZZ_mumutautau"

evgenConfig.inputconfcheck = "ZZ_2mu2tau"
evgenConfig.minevents = 2000

evgenConfig.description = "POWHEG+Pythia8 ZZ -> 2mu2tau production mll>4GeV with mass filter m4l>100GeV and m4l<150GeV and lepton filter pt>3GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic"]
evgenConfig.contact = ["Antonio Salvucci <antonio.salvucci@cern.ch>"]

# compensate filter efficiency
evt_multiplier = 25

include("MC12JobOptions/PowhegControl_postInclude.py")

#forcing tau decay mode into leptons
topAlg.Pythia8.Commands += ["15:onMode = off",
                            "15:onIfAny=11 13"]

#topAlg.Pythia8.CrossSectionScaleFactor = 0.127700

include("MC12JobOptions/FourLeptonInvMassFilter.py")
topAlg.FourLeptonInvMassFilter.MinPt = 3.*GeV
topAlg.FourLeptonInvMassFilter.MaxEta = 5.
topAlg.FourLeptonInvMassFilter.MinMass = 100.*GeV
topAlg.FourLeptonInvMassFilter.MaxMass = 150.*GeV
