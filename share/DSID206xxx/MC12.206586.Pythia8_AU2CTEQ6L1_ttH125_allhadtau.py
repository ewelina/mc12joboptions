evgenConfig.description = "PYTHIA8 ttH all hadronic H->tautau with AU2 CTEQ6"
evgenConfig.keywords = ["SMhiggs", "ttH", "tau","allhadronic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            '25:mWidth = 0.00407',
                            '25:doForceWidth = true',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            '25:onMode = off',#decay of Higgs
                            '25:addChannel = 1 6.32e-2 100 15 -15', # H->tautau
                            '24:onMode = off',#decay of W
                            '24:onIfAny = 1 2 3 4 5 6'
                            ]

include("MC12JobOptions/BoostedHiggsFilter.py")
topAlg.HiggsFilter.Ptcut=0.
topAlg.HiggsFilter.Ptmaxcut=100000000.
topAlg.HiggsFilter.bbDecay=0
