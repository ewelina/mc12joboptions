include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "MadGraph(CT10) Pythia Perugia 2011c gg->A->ttbar no interference, singlelepton+dilepton, m(A)=750GeV, 2HDM Type2, tanb=0.70, sba=1, width(A)=10%, 8TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["ggAtt", "ttbar", "singlelepton", "dilepton"]
evgenConfig.inputfilecheck = "madgraph.206753.ggAtt"
evgenConfig.contact  = ["madalina.stanescu.bellu@cern.ch"]
evgenConfig.minevents=5000

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


