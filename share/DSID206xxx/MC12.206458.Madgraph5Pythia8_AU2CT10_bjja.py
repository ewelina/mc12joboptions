include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "Photon + dijet + b suitable for hh->bbaa searches"
evgenConfig.process = "Part of suite of samples with diphoton and dijet mass cuts loosely consistent with hh production"
evgenConfig.keywords = ["jets","multijet","photon","heavyFlavor","bottom"]
evgenConfig.inputfilecheck = 'madgraph5.206458.bjja'
evgenConfig.contact = ['Jahred Adelman']
