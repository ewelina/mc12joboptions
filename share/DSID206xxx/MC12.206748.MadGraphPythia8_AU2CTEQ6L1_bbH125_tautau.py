import os
from MadGraphControl.MadGraphUtils import *

ptb = 15
pttau = 15
mmbb = 60
mmbbmax = 180
xqcut = 0
lhef = 2

fcard = open('proc_card_mg5.dat','w')
proccard = """

import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
import model heft
generate p p > b b~ H
output -f
"""

#generate p p > b b~ H, H > ta+ ta-

fcard.write(proccard)
fcard.close()

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 4000.

# Grab the run card and move it into place
if os.access(os.environ['MADPATH']+'/Template/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/run_card.dat','run_card.SM.dat')
else:
    raise RuntimeError('Cannot find Template run_card.dat!')

if os.access(os.environ['MADPATH']+'/Template/Cards/param_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/param_card.dat','param_card.SM.dat')
else:
    raise RuntimeError('Cannot find Template param_card.dat!')

if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:

        if 'if xqcut' in line:
            newcard.write(line)                   
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(30000))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(rand_seed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' ptb ' in line:
            newcard.write('%f   = ptb   ! minimum pt for the b \n'%(ptb))
        elif ' ptl ' in line:
            newcard.write('%f   = ptl   ! minimum pt for the leptons \n'%(pttau))
        elif ' mmbb ' in line:
            newcard.write('%f   = mmbb   ! min invariant mass of bjet bjet pair \n'%(mmbb))
        elif ' mmbbmax ' in line:
            newcard.write('%f   = mmbbmax   ! max invariant mass of bjet bjet pair \n'%(mmbbmax))
        elif 'ktdurham' in line:
            newcard.write('-1   = ktdurham \n')
        elif ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif '= ickkw ' in line:
            newcard.write('0   = ickkw    ! 0 no matching, 1 MLM, 2 CKKW matching \n')
        elif ' lhe_version ' in line:
            newcard.write('%f  = lhe_version       ! Change the way clustering information pass to shower.\n'%(lhef))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

if not os.access('param_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get param card'
elif os.access('param_card.dat',os.R_OK):
    print 'ERROR: Old param card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('param_card.SM.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if '  25     1.20000000E+02' in line:
            newcard.write('    25 1.250000E+02  \n')
        elif '# BR' in line:
            newcard.write('\n')
        else:
            newcard.write(line)
    newcard.write('     4.31720144E-02    2          15       -15  # BR( H -> tau- tau+) \n')
    oldcard.close()
    newcard.close()

process_dir = new_process()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_BBxx'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia8 = topAlg.Pythia8

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                     "Beams:frameType = 4",
                     "Next:numberShowLHA = 10",
                     "Next:numberShowEvent = 10",
                     "25:onMode = off",
                     "25:onIfMatch = 15 -15"
                    ]


evgenConfig.description = "MadGraph+Pythia8 production JO with the AU2 CTEQ6L1 tune for continua with b-jets"
evgenConfig.keywords = ["bbH","tautau"]
evgenConfig.inputfilecheck = stringy

evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

