evgenConfig.description = "MadGraph+Pythia8 tHch Hch->tau nu with AU2,CTEQ6L1"
evgenConfig.keywords = ["nonSMhiggs", "Hch","top","tau"]
evgenConfig.inputfilecheck = "tHpl_2HDM_M350"
if runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = "tHpl_4FS_2HDM_M350"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 15 16'
                             ]
