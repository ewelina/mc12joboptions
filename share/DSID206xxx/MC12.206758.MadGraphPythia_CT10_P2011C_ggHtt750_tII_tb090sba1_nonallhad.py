include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "MadGraph(CT10) Pythia Perugia 2011c gg->H->ttbar + interference to SM ttbar, singlelepton+dilepton, m(H)=750GeV, 2HDM Type2, tanb=0.90, sba=1, width(A)=5%, 8TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["ggHtt", "ttbar", "singlelepton", "dilepton", "interference" ]
evgenConfig.inputfilecheck = "madgraph.206758.ggHtt"
evgenConfig.contact  = ["madalina.stanescu.bellu@cern.ch"]
evgenConfig.minevents=5000

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


