#
# Job options file for MadGraph tH showering with Pythia8
# by Giuseppe Salamanna (QMUL) / Andrey Loginov (Yale)
#
evgenConfig.description = "Generic tH production"
evgenConfig.keywords = ["tH", "generic", "yukawa", "top", "coupling", "Higgs", "HSG1", "HSG8"]
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
evgenConfig.inputfilecheck = 'thjb_hyy_null_ct10_scale75GeV'
#
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
#
topAlg.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
    ]
#
# to make pythia8 is more tolerant to imperfections in lhe 4-vectors
# (low precision in storing the kinematics in the LHEF):
topAlg.Pythia8.Commands += [
    'check:eptolerr = 0.01'
]
#
evgenConfig.generators += [ "MadGraph", "Pythia8" ]#for aMC@NLO? seems to work for MadGraph..

# OUTPUT PRINTOUT LEVEL
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
# you can override this for individual modules if necessary
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel               = 5
