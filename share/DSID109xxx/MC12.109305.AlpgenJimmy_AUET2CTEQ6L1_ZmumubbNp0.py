evgenConfig.description = "ALPGEN+Jimmy Z(->mumu)+bb process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW", "Z"]
evgenConfig.inputfilecheck = "ZmumubbNp0"
evgenConfig.minevents = 5000

include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
