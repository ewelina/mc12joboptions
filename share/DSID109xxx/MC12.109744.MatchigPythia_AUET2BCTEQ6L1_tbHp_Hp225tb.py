#
#
# Matchig / Pythia gg/gb->t[b]H+ with H+->tb
# H+ Mass 225GeV
#
# Responsible person(s)
#   Jun 3, 2009 : Martin Flechl (Martin.Flechl@cern.ch)
#

# ... Pythia
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
topAlg.Pythia.PythiaCommand += [
                            "pyinit user matchig",    #matchig user process
                            "pysubs msel 0",
                            "pysubs msub 161 1",      #f + g -> f' + H+/-
                            "pysubs msub 401 1",      #g + g -> t + b + H+/-
                            "pysubs msub 402 1",      #q + qbar -> t + b + H+/-
                            "pypars mstp 129 1000",   #number of attempts to find highest xsec with 2->3 variables
                        ]

#H+ parameters
topAlg.Pythia.PythiaCommand += [
                            "pydat1 paru 141 30.",   #tan beta
                            "pydat2 pmas 37 1 225."  #H+ mass
                        ]

#top decay
topAlg.Pythia.PythiaCommand += [
                            "pydat3 mdme 41 1 0",
                            "pydat3 mdme 42 1 0",
                            "pydat3 mdme 43 1 0",
                            "pydat3 mdme 44 1 0",
                            "pydat3 mdme 45 1 0",
                            "pydat3 mdme 46 1 1", # t->Wb
                            "pydat3 mdme 48 1 0",
                            "pydat3 mdme 49 1 0", # t->H+b
                            "pydat3 mdme 50 1 -1",
                            "pydat3 mdme 51 1 -1",
                            "pydat3 mdme 52 1 -1",
                            "pydat3 mdme 53 1 -1",
                            "pydat3 mdme 54 1 -1",
                            "pydat3 mdme 55 1 -1"
                        ]

#H+ decay
topAlg.Pythia.PythiaCommand += [
                            "pydat3 mdme 503 1 0",
                            "pydat3 mdme 504 1 0",
                            "pydat3 mdme 505 1 1", # H+ -> t b
                            "pydat3 mdme 507 1 0",
                            "pydat3 mdme 508 1 0",
                            "pydat3 mdme 509 1 0", # H+ -> tau nu
                            "pydat3 mdme 511 1 0", # H+ -> W h0
                            "pydat3 mdme 512 1 0", # H+ -> ~chi_10 ~chi_1+
                            "pydat3 mdme 513 1 0", # H+ -> ~chi_10 ~chi_2+
                            "pydat3 mdme 514 1 0", # H+ -> ~chi_20 ~chi_1+
                            "pydat3 mdme 515 1 0", # H+ -> ~chi_20 ~chi_2+
                            "pydat3 mdme 516 1 0", # H+ -> ~chi_30 ~chi_1+
                            "pydat3 mdme 517 1 0", # H+ -> ~chi_30 ~chi_2+
                            "pydat3 mdme 518 1 0", # H+ -> ~chi_40 ~chi_1+
                            "pydat3 mdme 519 1 0", # H+ -> ~chi_40 ~chi_2+
                            "pydat3 mdme 520 1 0", # below: non-chi SUSY decays
                            "pydat3 mdme 521 1 0",
                            "pydat3 mdme 522 1 0",
                            "pydat3 mdme 523 1 0",
                            "pydat3 mdme 524 1 0",
                            "pydat3 mdme 525 1 0",
                            "pydat3 mdme 526 1 0",
                            "pydat3 mdme 527 1 0",
                            "pydat3 mdme 528 1 0",
                            "pydat3 mdme 529 1 0"
                        ]

# ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

# ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )


evgenConfig.description = "Pythia H+ production in gg/gb fusion"
evgenConfig.keywords = [ "nonSMhiggs" ]
evgenConfig.contact  = [ "martin.flechl@cern.ch" ]
