evgenConfig.description = "JHU+PYTHIA8 ggH H->WW->lvlv with AU2,CTEQ6L1 SpinCP 2m"
evgenConfig.keywords = ["SMhiggs", "ggF", "W","leptonic", "SpinCP"]
evgenConfig.inputfilecheck = "JHU.*gg_qq_H125_WW_2lep_Spin2m_CTEQ6L1"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_JHU.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            ]
