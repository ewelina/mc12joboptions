include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "tau+- nu gamma gamma production with up to two jets in ME+PS."
evgenConfig.keywords = [ "EW","gamma","tau","neutrino" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "Junichi.Tanaka@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  16 -15 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;

  Process 93 93 ->  -16 15 22 22 93{2}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 7000
  Mass 91 91 1.5 7000
  PT 22  25 7000
  PT 90   8 7000
  DeltaR 22 93 0.1 1000
  DeltaR 90 22 0.5  1000
}(selector)
"""
evgenConfig.inputconfcheck = "167679.Sherpa_CT10_taunugammagammaPt25GeV"
evgenConfig.minevents=2000
