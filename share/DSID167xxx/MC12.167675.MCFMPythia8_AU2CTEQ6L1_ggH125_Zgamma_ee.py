evgenConfig.description = "125GeV Higgs decay to Z(ee)Gamma from MCFM interfaced to Pythia8 in MC12"
evgenConfig.keywords = ["SMhiggs","ggF","Z","gamma","leptonic","e"]
evgenConfig.contact = ["dartyin@cern.ch"]
evgenConfig.inputfilecheck = 'MCFM63.167675.ggH125Zeegamma'

include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
include("MC12JobOptions/Pythia8_MCFM.py")
include ( "MC12JobOptions/Pythia8_Photos.py" ) 

topAlg.Pythia8.Commands += [ "Init:showAllParticleData = on",
                             "Next:numberShowLHA = 10",
                             "Next:numberShowEvent = 10"
                             ]
