evgenConfig.description = "POWHEG+Pythia8 Wmtaunubb production without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["QCD","W","bottom","leptonic","tau"]
evgenConfig.inputfilecheck = "Powheg.167005.Wmtaunubb*"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Giacinto Piacquadio <giacinto.piacquadio@cern.ch>"]


include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

