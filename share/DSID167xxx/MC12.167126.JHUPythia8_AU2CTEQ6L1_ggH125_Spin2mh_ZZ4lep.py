evgenConfig.description = "JHU+PYTHIA8 ggH H->ZZ->4l (l=e,mu) with AU2,CTEQ6L1 SpinCP 2m higher dimensions"
evgenConfig.keywords = ["SMhiggs", "ggF", "Z","leptonic", "SpinCP"]
evgenConfig.contact = ["Rostislav Konoplich <rk60@nyu.edu>",
                       "Antonio Salvucci <antonio.salvucci@cern.ch>"]
evgenConfig.inputfilecheck = "JHU*.*167126"
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_JHU.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            ]
