evgenConfig.description = "JHU+PYTHIA8 ggH H->W+W-->lvtauv with AU2,CTEQ6L1 SpinCP 0m"
evgenConfig.keywords = ["SMhiggs", "ggF", "W","leptonic", "SpinCP"]
evgenConfig.inputfilecheck = "JHU.*gg_H125_WplepWmtau_Spin0m"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_JHU.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            ]
