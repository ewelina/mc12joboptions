include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
## ... Tauola
#include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )


topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000", # Turn off FSR.
                                 #"pydat3 mdcy 15 1 0"   # Turn off tau decays.
                                 ]

evgenConfig.inputfilecheck = 'madgraph.167008.ZZZStar_nunullll'
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'ZZZStar using MadGraph/Pythia'
evgenConfig.keywords =["EW","Z","leptonic"]
