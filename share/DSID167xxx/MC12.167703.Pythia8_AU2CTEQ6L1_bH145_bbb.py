evgenConfig.description = "PYTHIA8 bH H->bb with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "bH", "bottom"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 145',
                            '25:mWidth = 0.0115',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 5 5',
                            'HiggsSM:qg2Hq = on'
                            ]

