evgenConfig.description = "POWHEG+PYTHIA8 ggH H->Zg Z->ll with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "gamma","Z","leptonic"]
evgenConfig.inputfilecheck = "ggH_SM_M115"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 23',
                            '23:onMode = off',
                            '23:mMin = 2.0',
                            '23:onIfAny = 11 13 15'
                            ]
