include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py") 
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2"
                            ]

evgenConfig.description = 'WH(125)->lvbb in spin/cp 2+'
evgenConfig.keywords = ["nonSMhiggs","WH","leptonic","bottom"]
evgenConfig.inputfilecheck = 'Madgraph.167721.WH125_lvbb_Spin2p'

evgenConfig.minevents = 5000
