evgenConfig.description = "PYTHIA8 ZH H->Zgam Z->ll with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "gamma","Z","leptonic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 120',
                            '25:mWidth = 0.00350',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 22 23',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:mMin = 2.0',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]

if not hasattr(topAlg, "ParentChildwStatusFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentChildwStatusFilter
    topAlg += ParentChildwStatusFilter()

## Add this filter to the algs required to be successful for streaming
if "ParentChildwStatus" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ParentChildwStatusFilter"]

#include("MC12JobOptions/ParentChildwStatusFilter.py")
topAlg.ParentChildwStatusFilter.PDGParent  = [23]
topAlg.ParentChildwStatusFilter.StatusParent = [22,52]
topAlg.ParentChildwStatusFilter.PtMinParent =  -1e12
topAlg.ParentChildwStatusFilter.PtMaxParent = 1e12
topAlg.ParentChildwStatusFilter.EtaRangeParent = 1e12
topAlg.ParentChildwStatusFilter.MassMinParent = -1e12
topAlg.ParentChildwStatusFilter.MassMaxParent = 1e12
topAlg.ParentChildwStatusFilter.PDGChild = [11,13,15]
topAlg.ParentChildwStatusFilter.PtMinChild = -1e12
topAlg.ParentChildwStatusFilter.EtaRangeChild = 1e12
