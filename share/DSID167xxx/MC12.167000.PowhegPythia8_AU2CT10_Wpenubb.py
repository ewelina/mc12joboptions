evgenConfig.description = "POWHEG+Pythia8 Wpenubb production without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["QCD","W","bottom","leptonic","e"]
evgenConfig.inputfilecheck = "Powheg.167000.Wpenubb*"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Giacinto Piacquadio <giacinto.piacquadio@cern.ch>"]


include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")


  
