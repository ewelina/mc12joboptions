include( "MC12JobOptions/Sherpa_CT10_Common.py" )

evgenConfig.description = "Zee + 0,1,2,3,4 jets, treating b-quarks as massive. ME level cut pT(Z)>140."
evgenConfig.keywords = [ "Z", "Heavy Flavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.inputconfcheck = "ZeePt140"
evgenConfig.minevents = 1000
evgenConfig.process="""
(run){
MASSIVE[5]=1
}(run)

(processes){
Process 5 -5 -> 11 -11 93 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;
End process;

Process 93 93 -> 11 -11 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;
End process;

Process 5 -5 -> 11 -11 5 -5 93{2}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;
End process;

Process 93 5 -> 11 -11 5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;
End process;

Process 93 -5 -> 11 -11 -5 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;

Process 93 93 -> 11 -11 93 93{3}
Order_EW 2; Max_N_Quarks 6
CKKW sqr(20.0/E_CMS)
Integration_Error 0.05 {5,6,7,8}
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5,6,7,8}
Cut_Core 1;
End process;

}(processes)

(selector){
  Mass 11 -11 40.0  E_CMS
  PT2  11 -11 140.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.RequestBottom=True
HeavyFlavorHadronFilter.RequestCharm=True
HeavyFlavorHadronFilter.Request_cQuark=False
HeavyFlavorHadronFilter.Request_bQuark=False
HeavyFlavorHadronFilter.RequestSpecificPDGID=False
HeavyFlavorHadronFilter.RequireTruthJet=False
HeavyFlavorHadronFilter.BottomPtMin=0*GeV
HeavyFlavorHadronFilter.BottomEtaMax=4.
HeavyFlavorHadronFilter.CharmPtMin=0*GeV
HeavyFlavorHadronFilter.CharmEtaMax=4.
StreamEVGEN.VetoAlgs += [ "HeavyFlavorHadronFilter" ]
