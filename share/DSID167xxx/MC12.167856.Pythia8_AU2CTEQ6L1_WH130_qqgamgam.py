evgenConfig.description = "PYTHIA8 WH W->qq,H->gamgam with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "WH","gamma","hadronic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 130',
                            '25:mWidth = 0.00491',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 22 22',
                            'HiggsSM:ffbar2HW = on',
                            '24:onMode = off',
                            '24:onIfAny = 1 2 3 4 5 6'
                            ]
