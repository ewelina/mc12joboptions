evgenConfig.description = "PYTHIA8 ttH H->Zgam Z->ll with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "gamma","Z","leptonic"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 145',
                            '25:mWidth = 0.0115',
                            '25:doForceWidth = true',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 23',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            '24:onMode = off',
                            '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16',
                            '23:onMode = off',
                            '23:mMin = 2.0',
                            '23:onIfAny = 11 13 15'                           
                            ]
