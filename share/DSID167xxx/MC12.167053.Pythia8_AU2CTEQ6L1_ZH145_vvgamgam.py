evgenConfig.description = "PYTHIA8 ZH H->gamgam with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "gamma"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 145',
                            '25:mWidth = 0.0115',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 22 22',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:onIfAny = 12 14 16'
                            ]
