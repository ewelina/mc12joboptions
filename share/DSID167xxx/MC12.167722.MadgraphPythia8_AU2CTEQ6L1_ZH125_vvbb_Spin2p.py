include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py") 
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2"
                            ]

evgenConfig.description = 'ZH(125)->vvbb in spin/cp 2+'
evgenConfig.keywords = ["nonSMhiggs","ZH","neutrino","bottom"]
evgenConfig.inputfilecheck = 'Madgraph.167722.ZH125_vvbb_Spin2p'

evgenConfig.minevents = 5000
