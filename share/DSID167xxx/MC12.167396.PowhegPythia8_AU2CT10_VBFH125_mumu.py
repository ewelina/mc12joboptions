evgenConfig.description = "POWHEG+PYTHIA8 VBFH H->mumu with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "mu"]
evgenConfig.inputfilecheck = "VBFH_.*_SM_M125"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 13 13'
                            ]
