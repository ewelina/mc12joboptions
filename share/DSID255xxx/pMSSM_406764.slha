#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50017322E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.3.10       # version number                    
#
BLOCK MODSEL  # Model selection
     1     0   #  ewkh                                             
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27944000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637000E-05   # G_F [GeV^-2]
         3     1.18500002E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.17999983E+00   # mb(mb)^MSbar
         6     1.73480209E+02   # mt pole mass
         7     1.77699000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     5.00000000E+03   # m0                  
         2    -3.49034570E+03   # m12                 
         3     2.58011093E+01   # tanb                
         5     1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00173217E+03   # MX                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03871437E+01   # W+
        25     1.25181342E+02   # h
        35     1.97329423E+03   # H
        36     1.97327075E+03   # A
        37     1.97532715E+03   # H+
         5     4.82046057E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.13095000E+03   # ~d_L
   2000001     5.12300123E+03   # ~d_R
   1000002     5.13046665E+03   # ~u_L
   2000002     5.12526176E+03   # ~u_R
   1000003     5.13095000E+03   # ~s_L
   2000003     5.12300123E+03   # ~s_R
   1000004     5.13046665E+03   # ~c_L
   2000004     5.12526176E+03   # ~c_R
   1000005     5.09364896E+03   # ~b_1
   2000005     5.11191386E+03   # ~b_2
   1000006     5.06375095E+03   # ~t_1
   2000006     5.09631284E+03   # ~t_2
   1000011     5.01144305E+03   # ~e_L
   2000011     5.01144131E+03   # ~e_R
   1000012     5.01047023E+03   # ~nu_eL
   1000013     5.01144305E+03   # ~mu_L
   2000013     5.01144131E+03   # ~mu_R
   1000014     5.01047023E+03   # ~nu_muL
   1000015     5.00421035E+03   # ~tau_1
   2000015     5.00808138E+03   # ~tau_2
   1000016     5.00693440E+03   # ~nu_tauL
   1000021     4.35904419E+03   # ~g
   1000022     1.21505629E+02   # ~chi_10
   1000023    -1.88891225E+02   # ~chi_20
   1000025     2.37295071E+02   # ~chi_30
   1000035    -3.46169641E+03   # ~chi_40
   1000024     1.24719850E+02   # ~chi_1+
   1000037     2.42495019E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -5.09029732E-03   # N_11
  1  2    -7.07649961E-01   # N_12
  1  3     5.81149404E-01   # N_13
  1  4    -4.01834532E-01   # N_14
  2  1     8.99643726E-03   # N_21
  2  2     1.53999233E-01   # N_22
  2  3     6.81948363E-01   # N_23
  2  4     7.14947363E-01   # N_24
  3  1    -6.83701617E-03   # N_31
  3  2     6.89576460E-01   # N_32
  3  3     4.44085331E-01   # N_33
  3  4    -5.72036520E-01   # N_34
  4  1     9.99923201E-01   # N_41
  4  2    -2.72968684E-04   # N_42
  4  3    -1.40674620E-04   # N_43
  4  4    -1.23894108E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -6.32438013E-01   # U_11
  1  2     7.74610973E-01   # U_12
  2  1    -7.74610973E-01   # U_21
  2  2    -6.32438013E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -8.34167391E-01   # V_11
  1  2     5.51511344E-01   # V_12
  2  1    -5.51511344E-01   # V_21
  2  2    -8.34167391E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.19698525E-01   # cos(theta_t)
  1  2     9.92810286E-01   # sin(theta_t)
  2  1    -9.92810286E-01   # -sin(theta_t)
  2  2     1.19698525E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99032340E-01   # cos(theta_b)
  1  2     4.39816284E-02   # sin(theta_b)
  2  1    -4.39816284E-02   # -sin(theta_b)
  2  2     9.99032340E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.93974104E-01   # cos(theta_tau)
  1  2     9.81006650E-01   # sin(theta_tau)
  2  1    -9.81006650E-01   # -sin(theta_tau)
  2  2     1.93974104E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -4.07672415E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00173217E+03  # DRbar Higgs Parameters
         1     1.74896648E+02   # mu(Q)MSSM           
         2     2.46736583E+01   # tan                 
         3     2.39798790E+02   # higgs               
         4     3.87157390E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  5.00173217E+03  # The gauge couplings
     1     3.66321888E-01   # gprime(Q) DRbar
     2     6.43789818E-01   # g(Q) DRbar
     3     9.90198204E-01   # g3(Q) DRbar
#
BLOCK AU Q=  5.00173217E+03  # The trilinear couplings
  1  1     1.00000025E+02   # A_u(Q) DRbar
  2  2     1.00000025E+02   # A_c(Q) DRbar
  3  3     1.00000044E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00173217E+03  # The trilinear couplings
  1  1     1.00000008E+02   # A_d(Q) DRbar
  2  2     1.00000008E+02   # A_s(Q) DRbar
  3  3     1.00000017E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00173217E+03  # The trilinear couplings
  1  1     1.00000003E+02   # A_e(Q) DRbar
  2  2     1.00000003E+02   # A_mu(Q) DRbar
  3  3     1.00000004E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00173217E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.19839066E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00173217E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     3.26621321E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00173217E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.54838420E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00173217E+03  # The soft SUSY breaking masses at the scale Q
         1    -3.49034570E+03   # M_1(Q)              
         2     1.54466232E+02   # M_2(Q)              
         3     4.00000000E+03   # M_3(Q)              
        21     3.99662250E+06   # M^2_Hd              
        22     6.19273947E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.99999998E+03   # M_q1L               
        42     4.99999998E+03   # M_q2L               
        43     4.99999997E+03   # M_q3L               
        44     4.99999998E+03   # M_uR                
        45     4.99999998E+03   # M_cR                
        46     4.99999996E+03   # M_tR                
        47     4.99999998E+03   # M_dR                
        48     4.99999998E+03   # M_sR                
        49     4.99999998E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.46808018E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.61209253E-01   # gluino decays
#          BR         NDA      ID1       ID2
     1.90248151E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.62958651E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.74849202E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
     1.17938051E-08    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     1.24651573E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     5.22383395E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.21472652E-02    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.08995191E-05    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     1.25414401E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.47183947E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.20642126E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     7.08624005E-05    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     1.24651573E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     5.22383395E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.21472652E-02    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.08995191E-05    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     1.25414401E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.47183947E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.20642126E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.08624005E-05    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     2.56105250E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.78481914E-02    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.01024863E-02    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.16147993E-05    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     4.36385583E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.16435622E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     9.67695449E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     5.82220855E-05    3     1000035         6        -6   # BR(~g -> ~chi_40 t  tb)
     2.73410633E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.73410633E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.29433903E-02    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     2.29433903E-02    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     2.73410633E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.73410633E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.29433903E-02    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     2.29433903E-02    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     6.73450435E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     6.73450435E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     1.20850228E-01    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     1.20850228E-01    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000006     1.60225078E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.09442887E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.24543876E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     8.19555370E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.86793134E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.21220700E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.83046361E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     1.61111326E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.58904143E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.62904142E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.99870839E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.47824698E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.45345792E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.17298318E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.05423757E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.40838516E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.61704845E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     8.74247205E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     3.20309006E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     6.84822366E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.27619611E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     2.28771386E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.24446933E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.57567627E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     4.87079371E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     6.05571570E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.03787642E-01    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.29509211E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.69841299E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.09238729E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.12114150E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     5.44367271E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.81787744E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.14635425E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.52779014E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.07866572E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.47671804E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     3.17698018E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.38491632E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     3.13303844E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.07991857E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     9.73817258E-06    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.03792126E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     1.75218956E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.13284405E-01    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.86657956E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.81785105E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     1.14022740E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.28865510E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.08691775E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     2.49203687E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.82636153E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     2.73227816E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     3.13640823E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.80414921E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.67277046E-06    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     8.33796635E-06    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.80910362E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     3.10470112E-02    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.68937169E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.81787744E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.14635425E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.52779014E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.07866572E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.47671804E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     3.17698018E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.38491632E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     3.13303844E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.07991857E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     9.73817258E-06    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.03792126E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     1.75218956E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.13284405E-01    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.86657956E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.81785105E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     1.14022740E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.28865510E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.08691775E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     2.49203687E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.82636153E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     2.73227816E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     3.13640823E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.80414921E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.67277046E-06    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     8.33796635E-06    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.80910362E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     3.10470112E-02    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.68937169E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.36234096E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.63760189E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     8.19859647E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.51997107E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     2.87109219E-02    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.59454976E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     3.87878209E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     7.31784844E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     9.46325765E-05    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.95101459E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.70156558E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.99440109E-01    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.36234096E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.63760189E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     8.19859647E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.51997107E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     2.87109219E-02    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.59454976E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     3.87878209E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     7.31784844E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     9.46325765E-05    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.95101459E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.70156558E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.99440109E-01    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.20543955E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.98202533E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.16752487E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     1.29819465E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.20037026E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     3.35101092E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.69249153E-02    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     6.81400700E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.52675063E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.79323318E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.74928048E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.97651523E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.90456180E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.94243224E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.36418049E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.61023008E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.17402353E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.55390793E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.87317994E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     4.51151514E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.96528862E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.36418049E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.61023008E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.17402353E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.55390793E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.87317994E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     4.51151514E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.96528862E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.00439539E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.46201714E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.51367822E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.41087252E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.60199580E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.64966384E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.15211014E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     8.58888723E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.65155786E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.65155786E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.21719570E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.21719570E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.62492880E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.28294166E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.54482353E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.45517647E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     2.00496273E-03   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.25813489E-05    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.40687331E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     4.41885114E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.40687331E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     4.41885114E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.20110053E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.01342117E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.01342117E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.00647095E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.02615355E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.02615355E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.02615373E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.18437474E-01    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.18437474E-01    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.18437474E-01    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.18437474E-01    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.94791342E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.94791342E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.94791342E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.94791342E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.93388750E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.93388750E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.62031939E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.79071798E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.98104641E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.98104641E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
#
#         PDG            Width
DECAY   1000035     1.26273453E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.74996602E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     8.20462820E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     5.43205016E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.34581253E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.34581253E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.05946840E-01    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     1.05946840E-01    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     3.29513203E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.37035428E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.28137893E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     9.63678887E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     4.87194071E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.96671595E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     5.20567441E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.17365339E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.16678503E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.00362861E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.00362861E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     3.37834090E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     3.37834090E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
#
#         PDG            Width
DECAY        25     4.03217776E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01949516E-01    2           5        -5   # BR(h -> b       bb     )
     6.50905606E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.30398395E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.52307539E-04    2           3        -3   # BR(h -> s       sb     )
     1.94767265E-02    2           4        -4   # BR(h -> c       cb     )
     7.01520235E-02    2          21        21   # BR(h -> g       g      )
     2.39769147E-03    2          22        22   # BR(h -> gam     gam    )
     1.57426703E-03    2          22        23   # BR(h -> Z       gam    )
     2.12127512E-01    2          24       -24   # BR(h -> W+      W-     )
     2.65489967E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.10434475E+01   # H decays
#          BR         NDA      ID1       ID2
     3.42927932E-01    2           5        -5   # BR(H -> b       bb     )
     6.06576028E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.14449654E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.56440588E-04    2           3        -3   # BR(H -> s       sb     )
     3.14140429E-08    2           4        -4   # BR(H -> c       cb     )
     3.37716353E-03    2           6        -6   # BR(H -> t       tb     )
     7.37995783E-06    2          21        21   # BR(H -> g       g      )
     1.40426374E-07    2          22        22   # BR(H -> gam     gam    )
     4.67852155E-09    2          23        22   # BR(H -> Z       gam    )
     4.12316200E-06    2          24       -24   # BR(H -> W+      W-     )
     2.05570298E-06    2          23        23   # BR(H -> Z       Z      )
     1.35347026E-05    2          25        25   # BR(H -> h       h      )
    -9.21650872E-24    2          36        36   # BR(H -> A       A      )
    -2.72709020E-20    2          23        36   # BR(H -> Z       A      )
     1.58701961E-01    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     3.85521989E-02    2     1000037  -1000037   # BR(H -> ~chi_2+ ~chi_2-)
     9.91181014E-02    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     9.91181014E-02    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     6.30660703E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     4.33232233E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     3.16768636E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     3.49011563E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     1.74067610E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     6.13316897E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.10455503E+01   # A decays
#          BR         NDA      ID1       ID2
     3.42931771E-01    2           5        -5   # BR(A -> b       bb     )
     6.06552546E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.14440659E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.56458421E-04    2           3        -3   # BR(A -> s       sb     )
     3.13015945E-08    2           4        -4   # BR(A -> c       cb     )
     3.40571971E-03    2           6        -6   # BR(A -> t       tb     )
     2.88103594E-05    2          21        21   # BR(A -> g       g      )
     2.41581695E-08    2          22        22   # BR(A -> gam     gam    )
     2.12266976E-08    2          23        22   # BR(A -> Z       gam    )
     4.08736887E-06    2          23        25   # BR(A -> Z       h      )
     1.76074377E-01    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.54906132E-02    2     1000037  -1000037   # BR(A -> ~chi_2+ ~chi_2-)
     8.19540785E-02    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     8.19540785E-02    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     7.16589620E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.79169945E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     4.14651067E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.69936670E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.43114256E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     5.16896564E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     4.06078955E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.41555768E-04    2           4        -5   # BR(H+ -> c       bb     )
     6.13728634E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.16977693E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.46594733E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.23963257E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.54992486E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.40553776E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.15050446E-06    2          24        25   # BR(H+ -> W+      h      )
     1.19057229E-12    2          24        36   # BR(H+ -> W+      A      )
     1.90357147E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.08055264E-01    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.71674522E-01    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     2.34283423E-01    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     8.22962233E-02    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     5.40033581E-04    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
