#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50017274E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.3.10       # version number                    
#
BLOCK MODSEL  # Model selection
     1     0   #  ewkh                                             
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27944000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637000E-05   # G_F [GeV^-2]
         3     1.18500002E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.17999983E+00   # mb(mb)^MSbar
         6     1.73370377E+02   # mt pole mass
         7     1.77699000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     5.00000000E+03   # m0                  
         2    -3.62321381E+02   # m12                 
         3     3.36900063E+01   # tanb                
         5     1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00172739E+03   # MX                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03871488E+01   # W+
        25     1.25370317E+02   # h
        35     1.95823351E+03   # H
        36     1.95821399E+03   # A
        37     1.96021245E+03   # H+
         5     4.82046057E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.13078252E+03   # ~d_L
   2000001     5.12233083E+03   # ~d_R
   1000002     5.13029947E+03   # ~u_L
   2000002     5.12259180E+03   # ~u_R
   1000003     5.13078252E+03   # ~s_L
   2000003     5.12233083E+03   # ~s_R
   1000004     5.13029947E+03   # ~c_L
   2000004     5.12259180E+03   # ~c_R
   1000005     5.08924808E+03   # ~b_1
   2000005     5.10293962E+03   # ~b_2
   1000006     5.06108431E+03   # ~t_1
   2000006     5.09203404E+03   # ~t_2
   1000011     5.00991255E+03   # ~e_L
   2000011     5.00530365E+03   # ~e_R
   1000012     5.00893858E+03   # ~nu_eL
   1000013     5.00991255E+03   # ~mu_L
   2000013     5.00530365E+03   # ~mu_R
   1000014     5.00893858E+03   # ~nu_muL
   1000015     4.99290551E+03   # ~tau_1
   2000015     5.00388035E+03   # ~tau_2
   1000016     5.00279398E+03   # ~nu_tauL
   1000021     4.35903347E+03   # ~g
   1000022     1.16467219E+02   # ~chi_10
   1000023    -1.71066688E+02   # ~chi_20
   1000025     2.31692571E+02   # ~chi_30
   1000035    -3.65809582E+02   # ~chi_40
   1000024     1.19674059E+02   # ~chi_1+
   1000037     2.35750074E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -4.10473572E-02   # N_11
  1  2    -6.64210259E-01   # N_12
  1  3     6.08350842E-01   # N_13
  1  4    -4.32491732E-01   # N_14
  2  1     1.55639991E-01   # N_21
  2  2     1.59098329E-01   # N_22
  2  3     6.80515040E-01   # N_23
  2  4     6.98114028E-01   # N_24
  3  1    -4.12980329E-02   # N_31
  3  2     7.30082421E-01   # N_32
  3  3     4.03184197E-01   # N_33
  3  4    -5.50196905E-01   # N_34
  4  1     9.86096233E-01   # N_41
  4  2    -2.21836061E-02   # N_42
  4  3    -6.51999715E-02   # N_43
  4  4    -1.51231846E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.76032402E-01   # U_11
  1  2     8.17426860E-01   # U_12
  2  1    -8.17426860E-01   # U_21
  2  2    -5.76032402E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -8.01171314E-01   # V_11
  1  2     5.98435064E-01   # V_12
  2  1    -5.98435064E-01   # V_21
  2  2    -8.01171314E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.28804673E-01   # cos(theta_t)
  1  2     9.91669984E-01   # sin(theta_t)
  2  1    -9.91669984E-01   # -sin(theta_t)
  2  2     1.28804673E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.97393166E-01   # cos(theta_b)
  1  2     7.21586614E-02   # sin(theta_b)
  2  1    -7.21586614E-02   # -sin(theta_b)
  2  2     9.97393166E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     8.20595697E-02   # cos(theta_tau)
  1  2     9.96627426E-01   # sin(theta_tau)
  2  1    -9.96627426E-01   # -sin(theta_tau)
  2  2     8.20595697E-02   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -3.09774203E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00172739E+03  # DRbar Higgs Parameters
         1     1.61552889E+02   # mu(Q)MSSM           
         2     3.24845219E+01   # tan                 
         3     2.39584951E+02   # higgs               
         4     3.86183891E+06   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  5.00172739E+03  # The gauge couplings
     1     3.66338976E-01   # gprime(Q) DRbar
     2     6.43854526E-01   # g(Q) DRbar
     3     9.90183864E-01   # g3(Q) DRbar
#
BLOCK AU Q=  5.00172739E+03  # The trilinear couplings
  1  1     1.00000025E+02   # A_u(Q) DRbar
  2  2     1.00000025E+02   # A_c(Q) DRbar
  3  3     1.00000045E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00172739E+03  # The trilinear couplings
  1  1     1.00000010E+02   # A_d(Q) DRbar
  2  2     1.00000010E+02   # A_s(Q) DRbar
  3  3     1.00000021E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00172739E+03  # The trilinear couplings
  1  1     1.00000006E+02   # A_e(Q) DRbar
  2  2     1.00000006E+02   # A_mu(Q) DRbar
  3  3     1.00000006E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00172739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.19263899E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00172739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.30263965E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00172739E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     3.35867414E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00172739E+03  # The soft SUSY breaking masses at the scale Q
         1    -3.62321381E+02   # M_1(Q)              
         2     1.55539551E+02   # M_2(Q)              
         3     4.00000000E+03   # M_3(Q)              
        21     4.08912998E+06   # M^2_Hd              
        22     6.36678631E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.99999998E+03   # M_q1L               
        42     4.99999998E+03   # M_q2L               
        43     4.99999997E+03   # M_q3L               
        44     4.99999998E+03   # M_uR                
        45     4.99999998E+03   # M_cR                
        46     4.99999996E+03   # M_tR                
        47     4.99999998E+03   # M_dR                
        48     4.99999998E+03   # M_sR                
        49     4.99999998E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.46512995E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     5.11305642E-01   # gluino decays
#          BR         NDA      ID1       ID2
     1.93831638E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     5.82011105E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.09264641E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
     2.54621199E-05    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     9.69970292E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.15813210E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.25015787E-02    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     3.46224404E-03    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     1.01878844E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     1.01708460E-03    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.20011384E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.10817261E-02    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     9.69970292E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.15813210E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.25015787E-02    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     3.46224404E-03    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     1.01878844E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     1.01708460E-03    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.20011384E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.10817261E-02    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     3.19794823E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.76612471E-02    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.27571111E-02    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     3.83119313E-03    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     4.39087554E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.02258607E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.40366640E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     1.44289411E-02    3     1000035         6        -6   # BR(~g -> ~chi_40 t  tb)
     2.18872888E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.18872888E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.34959817E-02    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     2.34959817E-02    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     2.18872888E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.18872888E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.34959817E-02    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     2.34959817E-02    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     7.30250365E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     7.30250365E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     1.09854964E-01    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     1.09854964E-01    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000006     1.68298936E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.15392260E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     1.98537861E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.39342655E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     8.20703513E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.40702791E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.46048655E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     1.53313816E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.66393710E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     8.50703949E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     1.89835686E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.36954791E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.15312512E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     1.24852937E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.26979299E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.32153768E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.69074301E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     9.78696535E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.08748200E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     7.53636494E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     4.92214685E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     2.29271153E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     3.92465925E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.49232652E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     6.60399381E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.42076818E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     1.31371008E-01    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     6.09513552E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.47385359E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.54541639E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     1.30757697E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     3.93432084E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.86768053E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.02538492E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.07064476E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.18150442E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.12041950E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.91485434E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.62207044E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     3.11427524E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.86257136E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     5.04737678E-04    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     7.24984642E-03    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     5.09631438E-04    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     2.88955310E-01    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     7.02780474E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.86763164E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.78518037E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.80947605E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.23343733E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     9.85950800E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.50696684E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.02675655E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     3.11763139E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.99994264E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.62460172E-04    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.33351148E-03    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     1.64035281E-04    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     9.30061083E-02    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.04333885E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.86768053E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.02538492E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.07064476E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.18150442E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.12041950E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.91485434E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.62207044E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     3.11427524E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.86257136E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.04737678E-04    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     7.24984642E-03    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     5.09631438E-04    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     2.88955310E-01    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     7.02780474E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.86763164E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.78518037E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.80947605E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.23343733E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     9.85950800E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.50696684E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.02675655E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     3.11763139E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.99994264E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.62460172E-04    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.33351148E-03    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     1.64035281E-04    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     9.30061083E-02    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.04333885E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.84053555E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.42617903E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.84796281E-02    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.50135889E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     8.67684648E-02    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.00190632E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.01807483E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     2.64487575E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.70079331E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.44218622E-02    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.71611802E-03    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.72161227E-01    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.84053555E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.42617903E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.84796281E-02    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.50135889E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     8.67684648E-02    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.00190632E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.01807483E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     2.64487575E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.70079331E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.44218622E-02    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.71611802E-03    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.72161227E-01    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     4.89570088E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.06623795E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.16562292E-01    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.40203914E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.20210629E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     1.87359219E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     4.52236730E-02    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     7.93118133E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.62150463E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.27860999E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.60884915E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     7.82022392E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.51047670E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     3.64928613E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.84211221E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.23845803E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.49872043E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.70698448E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     1.01600625E-01    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.87093596E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.15262807E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.84211221E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.23845803E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.49872043E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.70698448E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     1.01600625E-01    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.87093596E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.15262807E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     7.95388994E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.06403929E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.28764357E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.46656907E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     8.72895912E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.26781490E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.31580439E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     7.78297630E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.65231395E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.65231395E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.21744602E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.21744602E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     2.60480068E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.07644275E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.34627650E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.65372350E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     6.24530966E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     1.11828574E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.76201337E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     4.87941800E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.76201337E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     4.87941800E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.52748681E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.11896126E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.11896126E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.10878641E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.23717802E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.23717802E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.23717817E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.13442450E-01    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.13442450E-01    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.13442450E-01    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.13442450E-01    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.78123806E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.78123806E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.78123806E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.78123806E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.75882324E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.75882324E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.41526925E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     2.56067549E-04    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.99871966E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.99871966E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
#
#         PDG            Width
DECAY   1000035     6.83568876E-01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.59744577E-01    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     5.10907893E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.46818412E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     2.20476047E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     2.20476047E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     3.59510105E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     3.59510105E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     3.69648764E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.14482803E-01    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.80997452E-04    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
#
#         PDG            Width
DECAY        25     4.06204167E-03   # h decays
#          BR         NDA      ID1       ID2
     5.99395447E-01    2           5        -5   # BR(h -> b       bb     )
     6.47327390E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.29130997E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.49683033E-04    2           3        -3   # BR(h -> s       sb     )
     1.93570877E-02    2           4        -4   # BR(h -> c       cb     )
     6.99509313E-02    2          21        21   # BR(h -> g       g      )
     2.39085542E-03    2          22        22   # BR(h -> gam     gam    )
     1.58914944E-03    2          22        23   # BR(h -> Z       gam    )
     2.14924391E-01    2          24       -24   # BR(h -> W+      W-     )
     2.69805852E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     5.49385373E+01   # H decays
#          BR         NDA      ID1       ID2
     4.40529470E-01    2           5        -5   # BR(H -> b       bb     )
     7.79496541E-02    2         -15        15   # BR(H -> tau+    tau-   )
     2.75584211E-04    2         -13        13   # BR(H -> mu+     mu-    )
     3.29903783E-04    2           3        -3   # BR(H -> s       sb     )
     1.34552834E-08    2           4        -4   # BR(H -> c       cb     )
     1.44393370E-03    2           6        -6   # BR(H -> t       tb     )
     1.21976001E-05    2          21        21   # BR(H -> g       g      )
     8.43317958E-08    2          22        22   # BR(H -> gam     gam    )
     6.57501207E-09    2          23        22   # BR(H -> Z       gam    )
     1.83424038E-06    2          24       -24   # BR(H -> W+      W-     )
     9.14464963E-07    2          23        23   # BR(H -> Z       Z      )
     5.84270401E-06    2          25        25   # BR(H -> h       h      )
     7.35624353E-24    2          36        36   # BR(H -> A       A      )
    -9.27819692E-21    2          23        36   # BR(H -> Z       A      )
     1.22558718E-01    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.90674248E-02    2     1000037  -1000037   # BR(H -> ~chi_2+ ~chi_2-)
     7.10144792E-02    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     7.10144792E-02    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     4.31659986E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.15726396E-04    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     2.33716063E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     3.56479347E-04    2     1000035   1000035   # BR(H -> ~chi_40 ~chi_40)
     2.45805366E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     5.72008600E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     1.13972608E-02    2     1000022   1000035   # BR(H -> ~chi_10 ~chi_40)
     4.58549312E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
     2.05010252E-02    2     1000023   1000035   # BR(H -> ~chi_20 ~chi_40)
     1.00318091E-02    2     1000025   1000035   # BR(H -> ~chi_30 ~chi_40)
#
#         PDG            Width
DECAY        36     5.49404212E+01   # A decays
#          BR         NDA      ID1       ID2
     4.40530329E-01    2           5        -5   # BR(A -> b       bb     )
     7.79474396E-02    2         -15        15   # BR(A -> tau+    tau-   )
     2.75575477E-04    2         -13        13   # BR(A -> mu+     mu-    )
     3.29914692E-04    2           3        -3   # BR(A -> s       sb     )
     1.34026016E-08    2           4        -4   # BR(A -> c       cb     )
     1.45612821E-03    2           6        -6   # BR(A -> t       tb     )
     2.84849388E-05    2          21        21   # BR(A -> g       g      )
     1.88565085E-08    2          22        22   # BR(A -> gam     gam    )
     1.89775473E-08    2          23        22   # BR(A -> Z       gam    )
     1.81804526E-06    2          23        25   # BR(A -> Z       h      )
     1.32774004E-01    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     3.90300217E-02    2     1000037  -1000037   # BR(A -> ~chi_2+ ~chi_2-)
     6.09201921E-02    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     6.09201921E-02    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     4.78045877E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     7.41208093E-04    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.93070244E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     3.10940902E-04    2     1000035   1000035   # BR(A -> ~chi_40 ~chi_40)
     2.06728443E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     5.78443756E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.25352784E-02    2     1000022   1000035   # BR(A -> ~chi_10 ~chi_40)
     3.93721814E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
     1.92940085E-02    2     1000023   1000035   # BR(A -> ~chi_20 ~chi_40)
     9.96333743E-03    2     1000025   1000035   # BR(A -> ~chi_30 ~chi_40)
#
#         PDG            Width
DECAY        37     5.42316846E+01   # H+ decays
#          BR         NDA      ID1       ID2
     6.96117573E-04    2           4        -5   # BR(H+ -> c       bb     )
     7.90466991E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.79461800E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     4.45514024E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.59828351E-05    2           2        -3   # BR(H+ -> u       sb     )
     3.28740931E-04    2           4        -3   # BR(H+ -> c       sb     )
     4.34835962E-01    2           6        -5   # BR(H+ -> t       bb     )
     1.85023448E-06    2          24        25   # BR(H+ -> W+      h      )
     7.72783261E-13    2          24        36   # BR(H+ -> W+      A      )
     6.44705087E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     8.48809333E-02    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.21880310E-01    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     1.83780673E-02    2     1000024   1000035   # BR(H+ -> ~chi_1+ ~chi_40)
     1.78459828E-01    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     6.14865465E-02    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     4.90611362E-04    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
     1.85697290E-02    2     1000037   1000035   # BR(H+ -> ~chi_2+ ~chi_40)
