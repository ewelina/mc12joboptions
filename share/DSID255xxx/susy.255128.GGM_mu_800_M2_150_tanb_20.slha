#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     8.00000000E+02   # mu(EWSB)            
        25     2.00000000E+01   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04927751E+01   # W+
        25     1.25000000E+02   # h
        35     2.00003420E+03   # H
        36     2.00000000E+03   # A
        37     2.00196297E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00058040E+03   # ~d_L
   2000001     3.00010844E+03   # ~d_R
   1000002     2.99952795E+03   # ~u_L
   2000002     2.99978310E+03   # ~u_R
   1000003     3.00058040E+03   # ~s_L
   2000003     3.00010844E+03   # ~s_R
   1000004     2.99952795E+03   # ~c_L
   2000004     2.99978310E+03   # ~c_R
   1000005     2.99344517E+03   # ~b_1
   2000005     3.00723009E+03   # ~b_2
   1000006     3.00274894E+03   # ~t_1
   2000006     3.00486688E+03   # ~t_2
   1000011     3.00036354E+03   # ~e_L
   2000011     3.00032532E+03   # ~e_R
   1000012     2.99931102E+03   # ~nu_eL
   1000013     3.00036354E+03   # ~mu_L
   2000013     3.00032532E+03   # ~mu_R
   1000014     2.99931102E+03   # ~nu_muL
   1000015     2.99559468E+03   # ~tau_1
   2000015     3.00508773E+03   # ~tau_2
   1000016     2.99931102E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.47664233E+02   # ~chi_10
   1000023    -8.03236990E+02   # ~chi_20
   1000025     8.04849638E+02   # ~chi_30
   1000035     3.00072312E+03   # ~chi_40
   1000024     1.47672358E+02   # ~chi_1+
   1000037     8.08322823E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.52725400E-04   # N_11
  1  2    -9.94354348E-01   # N_12
  1  3     1.03354305E-01   # N_13
  1  4    -2.40231732E-02   # N_14
  2  1     7.82012927E-03   # N_21
  2  2    -5.61179349E-02   # N_22
  2  3    -7.04393656E-01   # N_23
  2  4    -7.07544486E-01   # N_24
  3  1     1.49366252E-02   # N_31
  3  2     9.00555352E-02   # N_32
  3  3     7.02226591E-01   # N_33
  3  4    -7.06076988E-01   # N_34
  4  1     9.99857759E-01   # N_41
  4  2    -4.56171686E-04   # N_42
  4  3    -5.02795221E-03   # N_43
  4  4     1.60926616E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.89371491E-01   # U_11
  1  2     1.45409950E-01   # U_12
  2  1     1.45409950E-01   # U_21
  2  2     9.89371491E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.99426251E-01   # V_11
  1  2     3.38698944E-02   # V_12
  2  1     3.38698944E-02   # V_21
  2  2     9.99426251E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.48432524E-01   # cos(theta_t)
  1  2     6.63210945E-01   # sin(theta_t)
  2  1    -6.63210945E-01   # -sin(theta_t)
  2  2     7.48432524E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.94896695E-01   # cos(theta_b)
  1  2     7.19109577E-01   # sin(theta_b)
  2  1    -7.19109577E-01   # -sin(theta_b)
  2  2     6.94896695E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.05681957E-01   # cos(theta_tau)
  1  2     7.08528740E-01   # sin(theta_tau)
  2  1    -7.08528740E-01   # -sin(theta_tau)
  2  2     7.05681957E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.02707565E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     8.00000000E+02   # mu(Q)               
         2     1.99999983E+01   # tanbeta(Q)          
         3     2.50700052E+02   # vev(Q)              
         4     3.73424685E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53350695E-01   # gprime(Q) DRbar
     2     6.35537524E-01   # g(Q) DRbar
     3     1.08193246E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.91789133E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.91831886E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01087739E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.09339655E+06   # M^2_Hd              
        22    -2.20479568E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     2.44283029E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.41596838E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     1.41596838E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     2.98974119E-03    2     2000002        -2   # BR(~g -> ~u_R  ub)
     2.98974119E-03    2    -2000002         2   # BR(~g -> ~u_R* u )
     1.41596838E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     1.41596838E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     2.98974119E-03    2     2000004        -4   # BR(~g -> ~c_R  cb)
     2.98974119E-03    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.65701150E-01    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.65701150E-01    2    -1000005         5   # BR(~g -> ~b_1* b )
#
#         PDG            Width
DECAY   1000006     8.16470854E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.80323108E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.68725443E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.26447321E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.79438490E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.37356436E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     8.09685703E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     5.74469702E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.32796563E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.74031782E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.17344690E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.18379995E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     4.33643521E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.51448529E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.37975178E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     4.15873819E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.97024105E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.66142467E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     4.62925630E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.16665002E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.36551896E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     5.61008099E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     4.48026147E-07    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     2.28681713E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.44874459E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     2.23785999E-05    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     3.59586787E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.29710823E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     8.63128178E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     2.48683542E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.66277906E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.61307841E-04    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.62300295E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     8.32381770E-04    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.15063349E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     7.84104269E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.59033840E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.30447113E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     9.58996987E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.20343162E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.54176392E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.22132891E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.76585569E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     4.06781983E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     8.30362493E-04    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.14548587E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     7.82227603E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.39344707E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.59586787E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.29710823E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     8.63128178E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     2.48683542E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.66277906E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.61307841E-04    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.62300295E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     8.32381770E-04    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.15063349E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     7.84104269E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.59033840E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.30447113E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     9.58996987E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.20343162E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.54176392E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.22132891E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.76585569E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     4.06781983E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     8.30362493E-04    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.14548587E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     7.82227603E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.39344707E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59074750E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.30162387E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     7.75412937E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     2.79733442E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.54054127E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.22107388E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.65262113E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     8.32336643E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.15063318E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     7.84104346E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.59074750E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.30162387E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     7.75412937E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     2.79733442E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.54054127E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.22107388E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.65262113E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     8.32336643E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.15063318E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     7.84104346E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.12120510E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.05053425E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.99237643E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.80875846E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     6.03077121E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.38581051E-02    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.09481187E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.59400818E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     5.95076437E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     6.42945746E-02    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     2.31039968E-06    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     5.12704827E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     1.04089826E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59509253E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.29980504E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.05601798E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.92915582E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.66372935E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.61387642E-04    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59509253E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.29980504E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.05601798E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.92915582E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.66372935E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.61387642E-04    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.80328674E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.11917172E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     9.98210914E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.82355267E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.31226326E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.40347387E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
#DECAY   1000024     1.46375655E-22   # chargino1+ decays
##           BR         NDA      ID1       ID2       ID3
#     3.74999990E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
#     3.74999990E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
#     1.25000010E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
#     1.25000010E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
##
##         PDG            Width
DECAY   1000037     4.80960933E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.40143607E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.36674873E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.23181520E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     4.84923620E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.42025620E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.34053351E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.34053351E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     8.98676788E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     4.85971457E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.60676562E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.36025391E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.36025391E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     2.31881562E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     9.09160622E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.65265425E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.67295867E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     4.61647577E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.69819774E-05    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.69819774E-05    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.09382217E-01    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     2.09382217E-01    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.23607077E-12    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     6.60988689E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.58229670E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     4.55457279E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.37987952E-03    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.49264382E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.68503177E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.45914864E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     3.32096194E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.10032042E-03    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.10032042E-03    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     3.70581540E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     3.70581540E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     4.31098156E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.31098156E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.28833595E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.28833595E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     6.21033951E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     6.21033951E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     4.58458342E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     4.58458342E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.31098156E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.31098156E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.28833595E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.28833595E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     6.21033951E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     6.21033951E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     4.58458342E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     4.58458342E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     4.53462087E-06    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     4.53462087E-06    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.17483780E-08    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.17483780E-08    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     5.76081487E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     5.76081487E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.17483780E-08    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.17483780E-08    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     5.76081487E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     5.76081487E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     4.05104661E-06    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     4.05104661E-06    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.81715057E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.81715057E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.81715057E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.81715057E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.81715057E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.81715057E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
