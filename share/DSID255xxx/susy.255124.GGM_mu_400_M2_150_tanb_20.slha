#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     4.00000000E+02   # mu(EWSB)            
        25     2.00000000E+01   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04932647E+01   # W+
        25     1.25000000E+02   # h
        35     2.00003423E+03   # H
        36     2.00000000E+03   # A
        37     2.00204997E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00057894E+03   # ~d_L
   2000001     3.00010800E+03   # ~d_R
   1000002     2.99952897E+03   # ~u_L
   2000002     2.99978399E+03   # ~u_R
   1000003     3.00057894E+03   # ~s_L
   2000003     3.00010800E+03   # ~s_R
   1000004     2.99952897E+03   # ~c_L
   2000004     2.99978399E+03   # ~c_R
   1000005     2.99680992E+03   # ~b_1
   2000005     3.00387521E+03   # ~b_2
   1000006     3.00326477E+03   # ~t_1
   2000006     3.00434601E+03   # ~t_2
   1000011     3.00036297E+03   # ~e_L
   2000011     3.00032399E+03   # ~e_R
   1000012     2.99931292E+03   # ~nu_eL
   1000013     3.00036297E+03   # ~mu_L
   2000013     3.00032399E+03   # ~mu_R
   1000014     2.99931292E+03   # ~nu_muL
   1000015     2.99797102E+03   # ~tau_1
   2000015     3.00271512E+03   # ~tau_2
   1000016     2.99931292E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.41770806E+02   # ~chi_10
   1000023    -4.05433729E+02   # ~chi_20
   1000025     4.12990694E+02   # ~chi_30
   1000035     3.00067223E+03   # ~chi_40
   1000024     1.41907437E+02   # ~chi_1+
   1000037     4.18360233E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.55195089E-03   # N_11
  1  2    -9.70355686E-01   # N_12
  1  3     2.24592776E-01   # N_13
  1  4    -8.92497607E-02   # N_14
  2  1     8.73857175E-03   # N_21
  2  2    -9.64481408E-02   # N_22
  2  3    -6.98515969E-01   # N_23
  2  4    -7.09011167E-01   # N_24
  3  1     1.25141895E-02   # N_31
  3  2     2.21601939E-01   # N_32
  3  3     6.79430628E-01   # N_33
  3  4    -6.99363995E-01   # N_34
  4  1     9.99882305E-01   # N_41
  4  2    -4.24455240E-04   # N_42
  4  3    -2.74737202E-03   # N_43
  4  4     1.50880055E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.50366345E-01   # U_11
  1  2     3.11133106E-01   # U_12
  2  1     3.11133106E-01   # U_21
  2  2     9.50366345E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.92242181E-01   # V_11
  1  2     1.24319971E-01   # V_12
  2  1     1.24319971E-01   # V_21
  2  2     9.92242181E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.85979875E-01   # cos(theta_t)
  1  2     6.18252081E-01   # sin(theta_t)
  2  1    -6.18252081E-01   # -sin(theta_t)
  2  2     7.85979875E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.83134263E-01   # cos(theta_b)
  1  2     7.30292803E-01   # sin(theta_b)
  2  1    -7.30292803E-01   # -sin(theta_b)
  2  2     6.83134263E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.04196176E-01   # cos(theta_tau)
  1  2     7.10005455E-01   # sin(theta_tau)
  2  1    -7.10005455E-01   # -sin(theta_tau)
  2  2     7.04196176E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.02750710E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     4.00000000E+02   # mu(Q)               
         2     1.99999983E+01   # tanbeta(Q)          
         3     2.50098407E+02   # vev(Q)              
         4     3.90122566E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53475447E-01   # gprime(Q) DRbar
     2     6.36315455E-01   # g(Q) DRbar
     3     1.08193494E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.93545297E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.99378356E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.01463399E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.59736602E+06   # M^2_Hd              
        22    -1.74126780E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.66736825E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.06556371E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.06556371E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.34436287E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.34436287E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.06556371E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.06556371E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.34436287E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.34436287E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     8.90877944E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     1.04540476E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.80759708E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.13557088E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     2.16654323E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.84488405E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     8.94202088E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.98723985E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.35496220E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.93882313E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     6.33119663E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     3.77437103E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     4.58753016E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.58934784E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     4.40190443E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     3.29032132E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.04331435E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.59811523E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     5.12141667E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     8.93611471E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.10251473E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.43512793E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     1.72055523E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     6.03206903E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.60464260E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.13977157E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.90477236E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.61731410E-02    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.56984358E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     9.96057128E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.50271769E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     1.05760248E-02    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.24621052E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.64802923E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.59890931E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.14961292E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     3.11253283E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.55411663E-02    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.03874128E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.25101093E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.70847586E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.76688799E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.05488347E-02    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.23788767E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.63098662E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.56373682E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.60464260E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.13977157E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.90477236E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.61731410E-02    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.56984358E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     9.96057128E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.50271769E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.05760248E-02    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.24621052E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.64802923E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.59890931E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.14961292E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     3.11253283E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.55411663E-02    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.03874128E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.25101093E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.70847586E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.76688799E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.05488347E-02    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.23788767E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.63098662E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.56373682E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59935888E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.14153686E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     2.71468540E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.68793282E-02    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.03754855E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.24974463E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.38176947E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.05758979E-02    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.24620981E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.64803121E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.59935888E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.14153686E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     2.71468540E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.68793282E-02    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.03754855E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.24974463E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.38176947E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.05758979E-02    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.24620981E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.64803121E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     2.14724216E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.21262887E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     3.81297792E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     2.88168206E-02    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     6.11758258E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     3.22557200E-05    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     2.15367383E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.15936309E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     7.23963307E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     9.96133850E-02    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.19016215E-07    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     4.09776528E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.02277328E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.60381554E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.14771045E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     3.31532551E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.48641696E-02    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.57087377E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     9.96208265E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.60381554E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.14771045E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     3.31532551E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.48641696E-02    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.57087377E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     9.96208265E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.83746618E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.95605675E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     3.11346629E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.39591394E-02    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.23154161E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.41675581E-02    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.88394301E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     9.98829591E-01    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
     5.85205000E-04    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     5.852050000E-04    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
#
#         PDG            Width
DECAY   1000037     2.10944766E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.55046861E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.65054220E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.79898919E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
DECAY   1000022     1.00000000E-10   # neutralino1 decays
     6.99311629E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     3.00673173E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     1.51974691E-05    2     1000039        25   # BR(~chi_10 -> ~G        h)
##         PDG            Width
DECAY   1000023     2.17228085E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.73982387E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.42859679E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.42859679E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     4.02982552E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     2.26090165E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.10874206E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.55788526E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.55788526E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     2.27335527E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     9.61919692E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.70752161E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.33971459E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     6.34554278E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.49099279E-03    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.49099279E-03    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.92717141E-01    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     1.92717141E-01    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.15454241E-03    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.25995424E-03    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     2.45278234E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     6.37599583E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.60700802E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     3.64647146E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.33968600E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     3.45372897E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.49836136E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.44771651E-03    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.44771651E-03    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     4.80675487E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     4.80675487E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     3.73249089E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.73249089E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.62176156E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.62176156E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.50907399E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.50907399E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.65391515E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.65391515E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.73249089E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.73249089E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.62176156E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.62176156E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.50907399E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.50907399E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.65391515E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.65391515E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     8.22105908E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     8.22105908E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.17595622E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.17595622E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     8.22105908E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     8.22105908E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.17595622E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.17595622E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     5.63990513E-07    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     5.63990513E-07    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.59254903E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.59254903E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.59254903E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.59254903E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.59254903E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.59254903E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
