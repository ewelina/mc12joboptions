#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.50017324E+04
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.3.10       # version number                    
#
BLOCK MODSEL  # Model selection
     1     0   #  ewkh                                             
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27944000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637000E-05   # G_F [GeV^-2]
         3     1.18500002E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.17999983E+00   # mb(mb)^MSbar
         6     1.73417770E+02   # mt pole mass
         7     1.77699000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
         1     5.00000000E+03   # m0                  
         2    -2.74525684E+03   # m12                 
         3     2.24136505E+01   # tanb                
         5     1.00000000E+02   # A0                  
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     5.00173242E+03   # MX                  
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03853180E+01   # W+
        25     1.24998887E+02   # h
        35     7.80601825E+02   # H
        36     7.80538757E+02   # A
        37     7.85111409E+02   # H+
         5     4.82046057E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     5.13076840E+03   # ~d_L
   2000001     5.12249280E+03   # ~d_R
   1000002     5.13028731E+03   # ~u_L
   2000002     5.12457746E+03   # ~u_R
   1000003     5.13076840E+03   # ~s_L
   2000003     5.12249280E+03   # ~s_R
   1000004     5.13028731E+03   # ~c_L
   2000004     5.12457746E+03   # ~c_R
   1000005     5.09526234E+03   # ~b_1
   2000005     5.11491207E+03   # ~b_2
   1000006     5.06306902E+03   # ~t_1
   2000006     5.09787466E+03   # ~t_2
   1000011     5.01113424E+03   # ~e_L
   2000011     5.00812389E+03   # ~e_R
   1000012     5.01016332E+03   # ~nu_eL
   1000013     5.01113424E+03   # ~mu_L
   2000013     5.00812389E+03   # ~mu_R
   1000014     5.01016332E+03   # ~nu_muL
   1000015     5.00322548E+03   # ~tau_1
   2000015     5.00883665E+03   # ~tau_2
   1000016     5.00776067E+03   # ~nu_tauL
   1000021     4.35905303E+03   # ~g
   1000022     1.35918667E+02   # ~chi_10
   1000023    -1.93502640E+02   # ~chi_20
   1000025     2.53363607E+02   # ~chi_30
   1000035    -2.72378440E+03   # ~chi_40
   1000024     1.39482701E+02   # ~chi_1+
   1000037     2.57382590E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1    -7.27486189E-03   # N_11
  1  2    -6.49772057E-01   # N_12
  1  3     6.08417613E-01   # N_13
  1  4    -4.55599999E-01   # N_14
  2  1     1.15933007E-02   # N_21
  2  2     1.40722454E-01   # N_22
  2  3     6.85369660E-01   # N_23
  2  4     7.14374703E-01   # N_24
  3  1    -7.93702525E-03   # N_31
  3  2     7.46989478E-01   # N_32
  3  3     4.00120382E-01   # N_33
  3  4    -5.30893024E-01   # N_34
  4  1     9.99874830E-01   # N_41
  4  2    -4.29619128E-04   # N_42
  4  3    -3.43819938E-04   # N_43
  4  4    -1.58120784E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -5.74032632E-01   # U_11
  1  2     8.18832423E-01   # U_12
  2  1    -8.18832423E-01   # U_21
  2  2    -5.74032632E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -7.75671992E-01   # V_11
  1  2     6.31136246E-01   # V_12
  2  1    -6.31136246E-01   # V_21
  2  2    -7.75671992E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     1.13075221E-01   # cos(theta_t)
  1  2     9.93586430E-01   # sin(theta_t)
  2  1    -9.93586430E-01   # -sin(theta_t)
  2  2     1.13075221E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     9.99317087E-01   # cos(theta_b)
  1  2     3.69507731E-02   # sin(theta_b)
  2  1    -3.69507731E-02   # -sin(theta_b)
  2  2     9.99317087E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     1.17240583E-01   # cos(theta_tau)
  1  2     9.93103542E-01   # sin(theta_tau)
  2  1    -9.93103542E-01   # -sin(theta_tau)
  2  2     1.17240583E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -4.86889603E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  5.00173242E+03  # DRbar Higgs Parameters
         1     1.80498542E+02   # mu(Q)MSSM           
         2     2.13758441E+01   # tan                 
         3     2.39846167E+02   # higgs               
         4     6.11268935E+05   # mA^2(Q)MSSM         
#
BLOCK GAUGE Q=  5.00173242E+03  # The gauge couplings
     1     3.66361745E-01   # gprime(Q) DRbar
     2     6.43580360E-01   # g(Q) DRbar
     3     9.90210361E-01   # g3(Q) DRbar
#
BLOCK AU Q=  5.00173242E+03  # The trilinear couplings
  1  1     1.00000025E+02   # A_u(Q) DRbar
  2  2     1.00000025E+02   # A_c(Q) DRbar
  3  3     1.00000044E+02   # A_t(Q) DRbar
#
BLOCK AD Q=  5.00173242E+03  # The trilinear couplings
  1  1     1.00000008E+02   # A_d(Q) DRbar
  2  2     1.00000008E+02   # A_s(Q) DRbar
  3  3     1.00000016E+02   # A_b(Q) DRbar
#
BLOCK AE Q=  5.00173242E+03  # The trilinear couplings
  1  1     1.00000003E+02   # A_e(Q) DRbar
  2  2     1.00000003E+02   # A_mu(Q) DRbar
  3  3     1.00000003E+02   # A_tau(Q) DRbar
#
BLOCK Yu Q=  5.00173242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     8.19905177E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  5.00173242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.85090739E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  5.00173242E+03  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     2.20833972E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  5.00173242E+03  # The soft SUSY breaking masses at the scale Q
         1    -2.74525684E+03   # M_1(Q)              
         2     1.78698776E+02   # M_2(Q)              
         3     4.00000000E+03   # M_3(Q)              
        21     6.67762975E+05   # M^2_Hd              
        22     6.18961072E+05   # M^2_Hu              
        31     5.00000000E+03   # M_eL                
        32     5.00000000E+03   # M_muL               
        33     5.00000000E+03   # M_tauL              
        34     5.00000000E+03   # M_eR                
        35     5.00000000E+03   # M_muR               
        36     5.00000000E+03   # M_tauR              
        41     4.99999998E+03   # M_q1L               
        42     4.99999998E+03   # M_q2L               
        43     4.99999997E+03   # M_q3L               
        44     4.99999998E+03   # M_uR                
        45     4.99999998E+03   # M_cR                
        46     4.99999996E+03   # M_tR                
        47     4.99999998E+03   # M_dR                
        48     4.99999998E+03   # M_sR                
        49     4.99999998E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
#
#         PDG            Width
DECAY         6     1.46536428E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     4.54736669E-01   # gluino decays
#          BR         NDA      ID1       ID2
     2.49894406E-04    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.66330248E-04    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.27785004E-04    2     1000025        21   # BR(~g -> ~chi_30 g)
     6.53661344E-08    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     1.06713261E-02    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     4.37530519E-04    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     1.44820520E-02    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     2.31208286E-04    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     1.07696329E-02    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.67363132E-04    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.43752827E-02    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     7.84111531E-04    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     1.06713261E-02    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     4.37530519E-04    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     1.44820520E-02    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     2.31208286E-04    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     1.07696329E-02    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.67363132E-04    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.43752827E-02    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     7.84111531E-04    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     2.17390278E-02    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.39149460E-02    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     1.97701758E-02    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     2.36746458E-04    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     5.28578508E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.17951581E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.95280851E-02    3     1000025         6        -6   # BR(~g -> ~chi_30 t  tb)
     7.62564393E-04    3     1000035         6        -6   # BR(~g -> ~chi_40 t  tb)
     2.36176238E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     2.36176238E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     2.75805026E-02    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     2.75805026E-02    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     2.36176238E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     2.36176238E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     2.75805026E-02    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     2.75805026E-02    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     7.31430988E-02    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     7.31430988E-02    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     1.13239615E-01    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     1.13239615E-01    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000006     1.62256306E+02   # stop1 decays
#          BR         NDA      ID1       ID2
     1.24908928E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.19091446E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     6.55937071E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     3.35339853E-02    2     1000035         6   # BR(~t_1 -> ~chi_40 t )
     2.52577496E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     1.46365823E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
     1.57928614E-01    2     1000021         6   # BR(~t_1 -> ~g      t )
#
#         PDG            Width
DECAY   2000006     1.57029973E+02   # stop2 decays
#          BR         NDA      ID1       ID2
     9.86102279E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.03862524E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.38961248E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     3.85933839E-03    2     1000035         6   # BR(~t_2 -> ~chi_40 t )
     9.27990068E-02    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.17844866E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
     1.44062789E-01    2     1000021         6   # BR(~t_2 -> ~g      t )
#
#         PDG            Width
DECAY   1000005     1.59506398E+02   # sbottom1 decays
#          BR         NDA      ID1       ID2
     7.39729644E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.52512168E-02    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     7.56216932E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     2.28450687E-03    2     1000035         5   # BR(~b_1 -> ~chi_40 b )
     2.51421596E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     4.11133089E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
     1.60314934E-01    2     1000021         5   # BR(~b_1 -> ~g      b )
#
#         PDG            Width
DECAY   2000005     4.44272224E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     5.70170366E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     8.72736737E-02    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     3.72538038E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     3.27436927E-02    2     1000035         5   # BR(~b_2 -> ~chi_40 b )
     1.05221717E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     7.96666152E-02    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
     6.00823461E-01    2     1000021         5   # BR(~b_2 -> ~g      b )
#
#         PDG            Width
DECAY   1000002     8.82509056E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     9.66309688E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     4.64986579E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     1.26284422E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     4.26394563E-03    2     1000035         2   # BR(~u_L -> ~chi_40 u)
     2.74228134E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.81004744E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
     3.12937920E-01    2     1000021         2   # BR(~u_L -> ~g      u)
#
#         PDG            Width
DECAY   2000002     3.33015899E+01   # sup_R decays
#          BR         NDA      ID1       ID2
     1.83931845E-05    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     4.66562432E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     2.18294408E-05    2     1000025         2   # BR(~u_R -> ~chi_30 u)
     1.81097852E-01    2     1000035         2   # BR(~u_R -> ~chi_40 u)
     8.18815269E-01    2     1000021         2   # BR(~u_R -> ~g      u)
#
#         PDG            Width
DECAY   1000001     8.82498405E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     9.58235487E-02    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     4.36845932E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.27320699E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.30382902E-03    2     1000035         1   # BR(~d_L -> ~chi_40 d)
     1.50201910E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     3.04705682E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     3.13275871E-01    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     2.86475881E+01   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.34311958E-06    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.35533687E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.34131614E-06    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     5.25746035E-02    2     1000035         1   # BR(~d_R -> ~chi_40 d)
     9.47400159E-01    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     8.82509056E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     9.66309688E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     4.64986579E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     1.26284422E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     4.26394563E-03    2     1000035         4   # BR(~c_L -> ~chi_40 c)
     2.74228134E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.81004744E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
     3.12937920E-01    2     1000021         4   # BR(~c_L -> ~g      c)
#
#         PDG            Width
DECAY   2000004     3.33015899E+01   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.83931845E-05    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     4.66562432E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     2.18294408E-05    2     1000025         4   # BR(~c_R -> ~chi_30 c)
     1.81097852E-01    2     1000035         4   # BR(~c_R -> ~chi_40 c)
     8.18815269E-01    2     1000021         4   # BR(~c_R -> ~g      c)
#
#         PDG            Width
DECAY   1000003     8.82498405E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     9.58235487E-02    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     4.36845932E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.27320699E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.30382902E-03    2     1000035         3   # BR(~s_L -> ~chi_40 s)
     1.50201910E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     3.04705682E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     3.13275871E-01    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     2.86475881E+01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.34311958E-06    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.35533687E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.34131614E-06    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     5.25746035E-02    2     1000035         3   # BR(~s_R -> ~chi_40 s)
     9.47400159E-01    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     6.50265171E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.35566499E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     6.87053251E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     1.74135508E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.09839975E-02    2     1000035        11   # BR(~e_L -> ~chi_40 e-)
     2.08920571E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     4.23522891E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     1.32666010E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.06538181E-04    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.70154566E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     1.26352949E-04    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
     9.99496954E-01    2     1000035        11   # BR(~e_R -> ~chi_40 e-)
#
#         PDG            Width
DECAY   1000013     6.50265171E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.35566499E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     6.87053251E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     1.74135508E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.09839975E-02    2     1000035        13   # BR(~mu_L -> ~chi_40 mu-)
     2.08920571E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     4.23522891E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.32666010E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.06538181E-04    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.70154566E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     1.26352949E-04    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
     9.99496954E-01    2     1000035        13   # BR(~mu_R -> ~chi_40 mu-)
#
#         PDG            Width
DECAY   1000015     2.35650432E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.20102910E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     8.78216976E-02    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     9.90276450E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     5.55385277E-01    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
     2.09519801E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.72675506E-02    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     6.91877178E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.38343537E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     4.23487932E-02    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.82611826E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     4.98517862E-02    2     1000035        15   # BR(~tau_2 -> ~chi_40  tau-)
     1.71843551E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     4.15000507E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     6.50435246E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     1.32093829E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     5.69197269E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     1.78318444E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     5.10982684E-02    2     1000035        12   # BR(~nu_eL -> ~chi_40 nu_e)
     3.81299533E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     2.51497952E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     6.50435246E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     1.32093829E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     5.69197269E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     1.78318444E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     5.10982684E-02    2     1000035        14   # BR(~nu_muL -> ~chi_40 nu_mu)
     3.81299533E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     2.51497952E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     6.98545108E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     1.22937169E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     5.29740108E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     1.65956942E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     4.75179426E-02    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
     4.01424380E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     2.56866166E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     1.30251187E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.61674743E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.61674743E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.20559103E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.20559103E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     3.55323080E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.47158721E-01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.58839702E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     6.41160298E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
#
#         PDG            Width
DECAY   1000022     0.00000000E+00   # neutralino1 decays
#
#         PDG            Width
DECAY   1000023     9.07508057E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     5.85848615E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     3.88599702E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     5.04075232E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     3.88599702E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     5.04075232E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.70371122E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     1.15624419E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     1.15624419E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.14561385E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     2.31171618E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     2.31171618E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     2.31171629E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.11720714E-01    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.11720714E-01    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.11720714E-01    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.11720714E-01    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.72401679E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.72401679E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.72401679E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.72401679E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.70330082E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.70330082E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     4.83754322E-01   # neutralino3 decays
#          BR         NDA      ID1       ID2
     1.24702617E-05    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.99993765E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.99993765E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
#
#         PDG            Width
DECAY   1000035     1.27959187E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.82466789E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     6.24438688E-02    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     3.67169601E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     5.45594275E-02    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     5.45594275E-02    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     6.94198641E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     6.94198641E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     3.22122796E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     3.64627153E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     5.26243072E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     7.56473456E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     7.15497255E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     4.29553235E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     3.23067000E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.34670889E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.58263301E-02    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     8.04332410E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     8.04332410E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     4.03578057E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     4.03578057E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
#
#         PDG            Width
DECAY        25     4.19401061E-03   # h decays
#          BR         NDA      ID1       ID2
     6.18073566E-01    2           5        -5   # BR(h -> b       bb     )
     6.69183169E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.36868864E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.64725919E-04    2           3        -3   # BR(h -> s       sb     )
     1.87002146E-02    2           4        -4   # BR(h -> c       cb     )
     6.68937051E-02    2          21        21   # BR(h -> g       g      )
     2.28681364E-03    2          22        22   # BR(h -> gam     gam    )
     1.48897351E-03    2          22        23   # BR(h -> Z       gam    )
     1.99987455E-01    2          24       -24   # BR(h -> W+      W-     )
     2.49493607E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     1.33065470E+01   # H decays
#          BR         NDA      ID1       ID2
     3.60857033E-01    2           5        -5   # BR(H -> b       bb     )
     5.55390130E-02    2         -15        15   # BR(H -> tau+    tau-   )
     1.96358459E-04    2         -13        13   # BR(H -> mu+     mu-    )
     2.69669141E-04    2           3        -3   # BR(H -> s       sb     )
     6.27820085E-08    2           4        -4   # BR(H -> c       cb     )
     5.55189327E-03    2           6        -6   # BR(H -> t       tb     )
     5.10298164E-05    2          21        21   # BR(H -> g       g      )
     7.68578491E-07    2          22        22   # BR(H -> gam     gam    )
     2.44519022E-08    2          23        22   # BR(H -> Z       gam    )
     4.14810015E-05    2          24       -24   # BR(H -> W+      W-     )
     2.03658745E-05    2          23        23   # BR(H -> Z       Z      )
     1.42556423E-04    2          25        25   # BR(H -> h       h      )
    -2.55325711E-24    2          36        36   # BR(H -> A       A      )
     3.37814551E-19    2          23        36   # BR(H -> Z       A      )
     1.54198164E-01    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     2.31531260E-02    2     1000037  -1000037   # BR(H -> ~chi_2+ ~chi_2-)
     1.05443553E-01    2     1000024  -1000037   # BR(H -> ~chi_1+ ~chi_2-)
     1.05443553E-01    2     1000037  -1000024   # BR(H -> ~chi_2+ ~chi_1-)
     5.86174325E-02    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     3.05123816E-03    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     1.72697798E-02    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     3.33033738E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     6.30689108E-03    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     7.05426330E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     1.32964197E+01   # A decays
#          BR         NDA      ID1       ID2
     3.61210438E-01    2           5        -5   # BR(A -> b       bb     )
     5.55882816E-02    2         -15        15   # BR(A -> tau+    tau-   )
     1.96528589E-04    2         -13        13   # BR(A -> mu+     mu-    )
     2.69946639E-04    2           3        -3   # BR(A -> s       sb     )
     5.84549885E-08    2           4        -4   # BR(A -> c       cb     )
     6.08031698E-03    2           6        -6   # BR(A -> t       tb     )
     9.75961847E-05    2          21        21   # BR(A -> g       g      )
     2.49470798E-07    2          22        22   # BR(A -> gam     gam    )
     4.29045376E-08    2          23        22   # BR(A -> Z       gam    )
     3.91273985E-05    2          23        25   # BR(A -> Z       h      )
     1.97310352E-01    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     5.73902396E-02    2     1000037  -1000037   # BR(A -> ~chi_2+ ~chi_2-)
     6.65067307E-02    2     1000024  -1000037   # BR(A -> ~chi_1+ ~chi_2-)
     6.65067307E-02    2     1000037  -1000024   # BR(A -> ~chi_2+ ~chi_1-)
     7.70436202E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.31817335E-03    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     3.85138930E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.08852783E-02    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     8.20647741E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     4.08359176E-02    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     1.28648455E+01   # H+ decays
#          BR         NDA      ID1       ID2
     5.84000970E-04    2           4        -5   # BR(H+ -> c       bb     )
     5.77896756E-02    2         -15        16   # BR(H+ -> tau+    nu_tau )
     2.04311443E-04    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     3.73754766E-06    2           2        -5   # BR(H+ -> u       bb     )
     1.33478667E-05    2           2        -3   # BR(H+ -> u       sb     )
     2.74590826E-04    2           4        -3   # BR(H+ -> c       sb     )
     3.58340069E-01    2           6        -5   # BR(H+ -> t       bb     )
     4.16249267E-05    2          24        25   # BR(H+ -> W+      h      )
     2.03134298E-10    2          24        36   # BR(H+ -> W+      A      )
     3.86413069E-04    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     9.80943141E-02    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     1.69235469E-01    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
     2.24733149E-01    2     1000037   1000022   # BR(H+ -> ~chi_2+ ~chi_10)
     9.00989321E-02    2     1000037   1000023   # BR(H+ -> ~chi_2+ ~chi_20)
     2.00363409E-04    2     1000037   1000025   # BR(H+ -> ~chi_2+ ~chi_30)
