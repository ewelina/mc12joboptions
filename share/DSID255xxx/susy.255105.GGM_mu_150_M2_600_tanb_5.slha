#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04984979E+01   # W+
        25     1.25000000E+02   # h
        35     2.00054521E+03   # H
        36     2.00000000E+03   # A
        37     2.00186566E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00053583E+03   # ~d_L
   2000001     3.00010061E+03   # ~d_R
   1000002     2.99956470E+03   # ~u_L
   2000002     2.99979876E+03   # ~u_R
   1000003     3.00053583E+03   # ~s_L
   2000003     3.00010061E+03   # ~s_R
   1000004     2.99956470E+03   # ~c_L
   2000004     2.99979876E+03   # ~c_R
   1000005     2.99991783E+03   # ~b_1
   2000005     3.00072101E+03   # ~b_2
   1000006     3.00303720E+03   # ~t_1
   2000006     3.00463135E+03   # ~t_2
   1000011     3.00033463E+03   # ~e_L
   2000011     3.00030183E+03   # ~e_R
   1000012     2.99936344E+03   # ~nu_eL
   1000013     3.00033463E+03   # ~mu_L
   2000013     3.00030183E+03   # ~mu_R
   1000014     2.99936344E+03   # ~nu_muL
   1000015     3.00009597E+03   # ~tau_1
   2000015     3.00054153E+03   # ~tau_2
   1000016     2.99936344E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.40171355E+02   # ~chi_10
   1000023    -1.52829741E+02   # ~chi_20
   1000025     6.11989695E+02   # ~chi_30
   1000035     3.00066869E+03   # ~chi_40
   1000024     1.43070359E+02   # ~chi_1+
   1000037     6.12091339E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.25636800E-02   # N_11
  1  2    -1.40156031E-01   # N_12
  1  3     7.15696891E-01   # N_13
  1  4    -6.84088007E-01   # N_14
  2  1     7.94167435E-03   # N_21
  2  2    -5.96577351E-02   # N_22
  2  3    -6.95863464E-01   # N_23
  2  4    -7.15647905E-01   # N_24
  3  1     2.76645213E-03   # N_31
  3  2     9.88330406E-01   # N_32
  3  3     5.94879843E-02   # N_33
  3  4    -1.40201763E-01   # N_34
  4  1     9.99885709E-01   # N_41
  4  2    -4.99568031E-04   # N_42
  4  3    -3.63045126E-03   # N_43
  4  4     1.46676433E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -8.42903326E-02   # U_11
  1  2     9.96441238E-01   # U_12
  2  1     9.96441238E-01   # U_21
  2  2     8.42903326E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -2.00019518E-01   # V_11
  1  2     9.79791913E-01   # V_12
  2  1     9.79791913E-01   # V_21
  2  2     2.00019518E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.57173053E-01   # cos(theta_t)
  1  2     6.53214335E-01   # sin(theta_t)
  2  1    -6.53214335E-01   # -sin(theta_t)
  2  2     7.57173053E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.78608989E-01   # cos(theta_b)
  1  2     8.78028152E-01   # sin(theta_b)
  2  1    -8.78028152E-01   # -sin(theta_b)
  2  2     4.78608989E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.80579722E-01   # cos(theta_tau)
  1  2     7.32674035E-01   # sin(theta_tau)
  2  1    -7.32674035E-01   # -sin(theta_tau)
  2  2     6.80579722E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.98528012E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     4.99999949E+00   # tanbeta(Q)          
         3     2.50488388E+02   # vev(Q)              
         4     4.04590822E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53664148E-01   # gprime(Q) DRbar
     2     6.34369029E-01   # g(Q) DRbar
     3     1.08193255E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.09096018E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.77420854E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.11740123E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     6.00000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.83407392E+06   # M^2_Hd              
        22    -1.51114871E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.42803588E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.05974974E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.05974974E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.40250263E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.40250263E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.05974974E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.05974974E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.40250263E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.40250263E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     8.98819520E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.80807944E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.92111733E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     4.51289077E-02    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     3.06346414E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     7.56050003E-02    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     9.02526494E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.29027679E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.63506129E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     9.51847530E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     2.32405629E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     1.79875810E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     1.93906369E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     2.18222370E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     5.49130479E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.22341878E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     5.78329232E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     2.72015348E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     6.35549167E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     2.95429586E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     4.49737732E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.32099495E-01    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     5.68027085E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     2.92421747E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.31813195E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     6.84356257E-03    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.21861525E-03    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.24993085E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     2.88233945E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     6.38121343E-01    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.50571956E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     6.92536873E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.76486840E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     3.09762872E-02    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.31305016E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.32876763E-03    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.34790169E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     3.24938466E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     5.12818180E-03    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     6.61255966E-01    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     7.17313336E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     3.77306401E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     6.90998190E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.75872586E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     3.09079768E-02    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     2.22124766E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.31813195E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     6.84356257E-03    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.21861525E-03    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.24993085E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     2.88233945E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     6.38121343E-01    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.50571956E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     6.92536873E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.76486840E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     3.09762872E-02    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.31305016E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.32876763E-03    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.34790169E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     3.24938466E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     5.12818180E-03    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     6.61255966E-01    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     7.17313336E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     3.77306401E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     6.90998190E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.75872586E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     3.09079768E-02    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     2.22124766E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.31337506E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     6.39850951E-03    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.09996777E-03    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.26235103E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.12733197E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     6.61139088E-01    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     3.38844530E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     6.92536226E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.76486659E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     3.09771150E-02    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.31337506E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     6.39850951E-03    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.09996777E-03    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.26235103E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.12733197E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     6.61139088E-01    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     3.38844530E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     6.92536226E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.76486659E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     3.09771150E-02    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.56080549E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     2.06578467E-02    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     2.05416494E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.16069924E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     2.07054461E-02    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     6.40512618E-01    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.79956654E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.85196288E-03    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     8.66434031E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     3.26596481E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.60635202E-05    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     6.61821153E-01    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.31743544E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     7.80356810E-03    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.47866216E-03    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.23685225E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.88274940E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     6.38205050E-01    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.31743544E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     7.80356810E-03    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.47866216E-03    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.23685225E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.88274940E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     6.38205050E-01    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.33298360E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     7.76716495E-03    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.47176430E-03    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.22175254E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     3.33256194E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     6.35260197E-01    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
DECAY   1000024     2.17470686E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.68426815E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.68426815E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.22809023E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.22809023E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.75283226E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     5.25883092E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     2.57269269E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     2.48403790E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     2.60959310E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     2.33367630E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     4.26062830E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.49610956E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.63433889E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.86980872E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.63433889E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.86980872E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.60646733E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.24760128E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.24760128E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.00074923E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.48340198E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.48340198E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.48340198E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     7.30185799E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     7.30185799E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     7.30185799E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     7.30185799E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.43395570E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.43395570E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.43395570E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.43395570E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.07310701E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.07310701E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     5.26374027E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.42894319E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     2.18700912E-01    2     1000023        23   # BR(~chi_30 -> ~chi_20   Z )
     2.63375167E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     2.63375167E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     1.87700377E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
     3.25589447E-02    2     1000023        25   # BR(~chi_30 -> ~chi_20   h )
#
#         PDG            Width
DECAY   1000035     9.76803117E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     4.79490406E-02    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.46825219E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.60559593E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.82540321E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.82540321E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.06500491E-02    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     1.06500491E-02    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.36115583E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     2.19929562E-02    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     3.40368298E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     5.42392616E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.24889301E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.04344108E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     8.20029595E-03    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     8.50718362E-05    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.49539419E-04    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     5.40123004E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     5.40123004E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.34589653E-04    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     2.34589653E-04    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     3.42845696E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     3.42845696E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     3.42475323E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     3.42475323E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     5.02074276E-10    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     5.02074276E-10    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     3.65143327E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     3.65143327E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     3.42845696E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     3.42845696E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     3.42475323E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     3.42475323E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     5.02074276E-10    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     5.02074276E-10    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     3.65143327E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     3.65143327E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     9.45387157E-09    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     9.45387157E-09    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     4.56872979E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     4.56872979E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     9.45387157E-09    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     9.45387157E-09    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     4.56872979E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     4.56872979E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.44798609E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.44798609E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.44798609E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.44798609E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.44798609E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.44798609E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
