#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     8.00000000E+02   # mu(EWSB)            
        25     5.00000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04965629E+01   # W+
        25     1.25000000E+02   # h
        35     2.00054240E+03   # H
        36     2.00000000E+03   # A
        37     2.00196845E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     3.00053877E+03   # ~d_L
   2000001     3.00010064E+03   # ~d_R
   1000002     2.99956179E+03   # ~u_L
   2000002     2.99979871E+03   # ~u_R
   1000003     3.00053877E+03   # ~s_L
   2000003     3.00010064E+03   # ~s_R
   1000004     2.99956179E+03   # ~c_L
   2000004     2.99979871E+03   # ~c_R
   1000005     2.99852411E+03   # ~b_1
   2000005     3.00211662E+03   # ~b_2
   1000006     2.99962200E+03   # ~t_1
   2000006     3.00803324E+03   # ~t_2
   1000011     3.00033752E+03   # ~e_L
   2000011     3.00030191E+03   # ~e_R
   1000012     2.99936047E+03   # ~nu_eL
   1000013     3.00033752E+03   # ~mu_L
   2000013     3.00030191E+03   # ~mu_R
   1000014     2.99936047E+03   # ~nu_muL
   1000015     2.99913491E+03   # ~tau_1
   2000015     3.00150511E+03   # ~tau_2
   1000016     2.99936047E+03   # ~nu_tauL
   1000021     3.00000000E+03   # ~g
   1000022     1.45348844E+02   # ~chi_10
   1000023    -8.02217746E+02   # ~chi_20
   1000025     8.06091987E+02   # ~chi_30
   1000035     3.00077692E+03   # ~chi_40
   1000024     1.45357531E+02   # ~chi_1+
   1000037     8.08747273E+02   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.00164732E-04   # N_11
  1  2    -9.93828430E-01   # N_12
  1  3     1.04089216E-01   # N_13
  1  4    -3.83363673E-02   # N_14
  2  1     6.47585936E-03   # N_21
  2  2    -4.65152560E-02   # N_22
  2  3    -7.04858515E-01   # N_23
  2  4    -7.07791543E-01   # N_24
  3  1     1.67414270E-02   # N_31
  3  2     1.00703241E-01   # N_32
  3  3     7.01631641E-01   # N_33
  3  4    -7.05189068E-01   # N_34
  4  1     9.99838476E-01   # N_41
  4  2    -4.90159578E-04   # N_42
  4  3    -7.27662309E-03   # N_43
  4  4     1.64265921E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -9.89211258E-01   # U_11
  1  2     1.46496032E-01   # U_12
  2  1     1.46496032E-01   # U_21
  2  2     9.89211258E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -9.98529841E-01   # V_11
  1  2     5.42047676E-02   # V_12
  2  1     5.42047676E-02   # V_21
  2  2     9.98529841E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.16982622E-01   # cos(theta_t)
  1  2     6.97091041E-01   # sin(theta_t)
  2  1    -6.97091041E-01   # -sin(theta_t)
  2  2     7.16982622E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.62587087E-01   # cos(theta_b)
  1  2     7.48984881E-01   # sin(theta_b)
  2  1    -7.48984881E-01   # -sin(theta_b)
  2  2     6.62587087E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.01775234E-01   # cos(theta_tau)
  1  2     7.12398429E-01   # sin(theta_tau)
  2  1    -7.12398429E-01   # -sin(theta_tau)
  2  2     7.01775234E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -1.98528000E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     8.00000000E+02   # mu(Q)               
         2     4.99999949E+00   # tanbeta(Q)          
         3     2.50750770E+02   # vev(Q)              
         4     3.98996641E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53340771E-01   # gprime(Q) DRbar
     2     6.35610475E-01   # g(Q) DRbar
     3     1.08193385E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.07888415E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     7.69150287E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     5.11214702E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     3.00000000E+03   # M_1                 
         2     1.50000000E+02   # M_2                 
         3     3.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.16244488E+06   # M^2_Hd              
        22    -2.11293993E+06   # M^2_Hu              
        31     3.00000000E+03   # M_eL                
        32     3.00000000E+03   # M_muL               
        33     3.00000000E+03   # M_tauL              
        34     3.00000000E+03   # M_eR                
        35     3.00000000E+03   # M_muR               
        36     3.00000000E+03   # M_tauR              
        41     3.00000000E+03   # M_q1L               
        42     3.00000000E+03   # M_q2L               
        43     3.00000000E+03   # M_q3L               
        44     3.00000000E+03   # M_uR                
        45     3.00000000E+03   # M_cR                
        46     3.00000000E+03   # M_tR                
        47     3.00000000E+03   # M_dR                
        48     3.00000000E+03   # M_sR                
        49     3.00000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.44394644E-05   # gluino decays
#          BR         NDA      ID1       ID2
     2.06438334E-01    2     1000002        -2   # BR(~g -> ~u_L  ub)
     2.06438334E-01    2    -1000002         2   # BR(~g -> ~u_L* u )
     4.35616657E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     4.35616657E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     2.06438334E-01    2     1000004        -4   # BR(~g -> ~c_L  cb)
     2.06438334E-01    2    -1000004         4   # BR(~g -> ~c_L* c )
     4.35616657E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     4.35616657E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
#
#         PDG            Width
DECAY   1000006     8.18376790E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     8.59143258E-02    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     2.75680889E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.31514186E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.73846690E-01    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
     2.33043909E-01    2     1000037         5   # BR(~t_1 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   2000006     8.11583259E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     6.01405077E-02    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.43207364E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     2.84835269E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     1.21702498E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
     2.90114360E-01    2     1000037         5   # BR(~t_2 -> ~chi_2+ b )
#
#         PDG            Width
DECAY   1000005     3.46734033E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
     1.56228083E-01    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     2.86430681E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     2.31059178E-03    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     3.08785631E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
     5.29811388E-01    2    -1000037         6   # BR(~b_1 -> ~chi_2- t )
#
#         PDG            Width
DECAY   2000005     4.42597464E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.45690464E-01    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     5.12638528E-03    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     7.20400349E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     2.88080457E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
     5.53898690E-01    2    -1000037         6   # BR(~b_2 -> ~chi_2- t )
#
#         PDG            Width
DECAY   1000002     3.59650445E+01   # sup_L decays
#          BR         NDA      ID1       ID2
     3.29378008E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     5.93317532E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     3.10878404E-03    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     6.65226298E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
     1.69359188E-03    2     1000037         1   # BR(~u_L -> ~chi_2+ d)
#
#         PDG            Width
DECAY   2000002     1.84266154E-03   # sup_R decays
#          BR         NDA      ID1       ID2
     2.89877221E-03    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.29943866E-01    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.67157362E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.59137665E+01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.30178335E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     6.59048262E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     2.75336512E-03    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     6.54015170E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
     1.23934130E-02    2    -1000037         2   # BR(~d_L -> ~chi_2- u)
     6.69004307E-07    2     1000021         1   # BR(~d_L -> ~g      d)
#
#         PDG            Width
DECAY   2000001     4.61564718E-04   # sdown_R decays
#          BR         NDA      ID1       ID2
     2.89341806E-03    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.29707753E-01    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.65581982E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
     1.81684779E-03    2     1000021         1   # BR(~d_R -> ~g      d)
#
#         PDG            Width
DECAY   1000004     3.59650445E+01   # scharm_L decays
#          BR         NDA      ID1       ID2
     3.29378008E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     5.93317532E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     3.10878404E-03    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     6.65226298E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
     1.69359188E-03    2     1000037         3   # BR(~c_L -> ~chi_2+ s)
#
#         PDG            Width
DECAY   2000004     1.84266154E-03   # scharm_R decays
#          BR         NDA      ID1       ID2
     2.89877221E-03    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.29943866E-01    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.67157362E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.59137665E+01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.30178335E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     6.59048262E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     2.75336512E-03    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     6.54015170E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
     1.23934130E-02    2    -1000037         4   # BR(~s_L -> ~chi_2- c)
     6.69004307E-07    2     1000021         3   # BR(~s_L -> ~g      s)
#
#         PDG            Width
DECAY   2000003     4.61564718E-04   # sstrange_R decays
#          BR         NDA      ID1       ID2
     2.89341806E-03    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.29707753E-01    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.65581982E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
     1.81684779E-03    2     1000021         3   # BR(~s_R -> ~g      s)
#
#         PDG            Width
DECAY   1000011     3.59176048E+01   # selectron_L decays
#          BR         NDA      ID1       ID2
     3.29677667E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.33018307E-04    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     3.49731166E-03    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     6.53901006E-01    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
     1.23909970E-02    2    -1000037        12   # BR(~e_L -> ~chi_2- nu_e)
#
#         PDG            Width
DECAY   2000011     4.14689953E-03   # selectron_R decays
#          BR         NDA      ID1       ID2
     2.89862609E-03    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.29943824E-01    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     8.67157550E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.59176048E+01   # smuon_L decays
#          BR         NDA      ID1       ID2
     3.29677667E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.33018307E-04    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     3.49731166E-03    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     6.53901006E-01    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
     1.23909970E-02    2    -1000037        14   # BR(~mu_L -> ~chi_2- nu_mu)
#
#         PDG            Width
DECAY   2000013     4.14689953E-03   # smuon_R decays
#          BR         NDA      ID1       ID2
     2.89862609E-03    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.29943824E-01    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     8.67157550E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     1.79485937E+01   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.32738629E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     1.93601217E-03    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     3.01295007E-03    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     6.59855255E-01    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.45715319E-03    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   2000015     1.83770069E+01   # stau_2 decays
#          BR         NDA      ID1       ID2
     3.19545072E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     6.45271401E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     1.12900328E-02    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     6.33737333E-01    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
     2.89748483E-02    2    -1000037        16   # BR(~tau_2 -> ~chi_2-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.59581453E+01   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.29861423E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.25743124E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     2.41020949E-03    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     6.65308857E-01    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
     1.69376748E-03    2     1000037        11   # BR(~nu_eL -> ~chi_2+ e-)
#
#         PDG            Width
DECAY   1000014     3.59581453E+01   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.29861423E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.25743124E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     2.41020949E-03    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     6.65308857E-01    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
     1.69376748E-03    2     1000037        13   # BR(~nu_muL -> ~chi_2+ mu-)
#
#         PDG            Width
DECAY   1000016     3.60927020E+01   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.28631672E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.23037492E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     2.40122403E-03    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     6.62919451E-01    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
     5.32461554E-03    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#
#         PDG            Width
#DECAY   1000024     2.04342333E-22   # chargino1+ decays
##           BR         NDA      ID1       ID2       ID3
#     3.74999991E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
#     3.74999991E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
#     1.25000009E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
#     1.25000009E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
##
##         PDG            Width
DECAY   1000037     5.27129570E+00   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.40736959E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     3.36691285E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     3.22571756E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
#
#         PDG            Width
#DECAY   1000022     0.00000000E+00   # neutralino1 decays
##
##         PDG            Width
DECAY   1000023     5.30279414E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.77106301E-01    2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     3.32914734E-01    2     1000024       -24   # BR(~chi_20 -> ~chi_1+   W-)
     3.32914734E-01    2    -1000024        24   # BR(~chi_20 -> ~chi_1-   W+)
     5.70642318E-02    2     1000022        25   # BR(~chi_20 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000025     5.30825771E+00   # neutralino3 decays
#          BR         NDA      ID1       ID2
     6.06455983E-02    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     3.37194017E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     3.37194017E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     2.64966368E-01    2     1000022        25   # BR(~chi_30 -> ~chi_10   h )
#
#         PDG            Width
DECAY   1000035     9.76539016E+00   # neutralino4 decays
#          BR         NDA      ID1       ID2
     1.41730836E-05    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.96385857E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     2.93929071E-02    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.56668104E-04    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.56668104E-04    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     2.21195122E-01    2     1000037       -24   # BR(~chi_40 -> ~chi_2+   W-)
     2.21195122E-01    2    -1000037        24   # BR(~chi_40 -> ~chi_2-   W+)
     1.49627806E-04    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     5.26582341E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     4.01847334E-04    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     2.90081220E-02    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     3.99041398E-03    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     2.22930588E-02    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     1.97199328E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.18116497E-02    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     3.86226504E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     9.13846352E-04    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     9.13846352E-04    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.52146972E-02    2     1000037       -37   # BR(~chi_40 -> ~chi_2+   H-)
     2.52146972E-02    2    -1000037        37   # BR(~chi_40 -> ~chi_2-   H+)
     4.14668825E-08    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     4.14668825E-08    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     4.32290179E-07    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     4.32290179E-07    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.61025403E-09    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.61025403E-09    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     5.16592352E-08    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     5.16592352E-08    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     4.14668825E-08    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     4.14668825E-08    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     4.32290179E-07    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     4.32290179E-07    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.61025403E-09    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.61025403E-09    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     5.16592352E-08    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     5.16592352E-08    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.63284221E-08    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     1.63284221E-08    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.64629020E-08    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.64629020E-08    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     1.63284221E-08    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     1.63284221E-08    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.64629020E-08    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.64629020E-08    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.70222573E-07    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     1.70222573E-07    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     1.70222573E-07    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     1.70222573E-07    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     1.70222573E-07    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     1.70222573E-07    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
