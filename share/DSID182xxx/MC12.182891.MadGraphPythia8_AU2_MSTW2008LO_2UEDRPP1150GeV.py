evgenConfig.description = "MadGraph5+Pythia8 for 2UED/RPP 4top with Mkk=1150 GeV"

evgenConfig.keywords = ["4top"]

evgenConfig.inputfilecheck = "4top"

evgenConfig.contact = ["Emmanuel Busato"]

evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")

include("MC12JobOptions/Pythia8_Photos.py")

