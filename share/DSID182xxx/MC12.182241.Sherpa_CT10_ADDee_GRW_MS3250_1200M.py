include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Sherpa" ]

evgenConfig.description = "ADD->ee production"
evgenConfig.keywords = [ "ADD" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> e+ e-
  #
  Process 93 93 -> 11 -11 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 1200.0 10000.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3          
  M_S   = 3250      
  M_CUT = 3250
  KK_CONVENTION = 5   

  MASS[39] = 100. 
  MASS[40] = 100.   

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   


evgenConfig.inputconfcheck = "Sherpa_CT10_ADD"
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
