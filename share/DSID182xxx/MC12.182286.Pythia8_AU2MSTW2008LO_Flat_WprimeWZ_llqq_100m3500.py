# W' -> WZ -> qq+ll JO: Jul. 2013 - K. Terashi

evgenConfig.description = "Flat EGM Wprime->WZ->qq+ll with AU2 MSTW2008LO tune"
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.keywords = ["Exotics", "EGM", "SSM", "Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# Turn on the W' process
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on"]
# Set W' -> WZ -> jj ll
topAlg.Pythia8.Commands += ["34:onMode = off"]
topAlg.Pythia8.Commands += ["34:onIfAll = 23 24"]
topAlg.Pythia8.Commands += ["23:onMode = off",
                            "23:onIfAny = 11 13",
                            "24:onMode = off",
                            "24:onIfAny = 1 2 3 4 5",
                            "Init:showAllParticleData = on",
                            "Next:numberShowEvent = 5"]
# W' coupling to WZ
topAlg.Pythia8.Commands += ["Wprime:coup2WZ=1."]

topAlg.Pythia8.Commands += ["34:mMin = 100.",
                            "34:mMax = 3500.",
                            "PhaseSpace:mHatMin = 100.",
                            "PhaseSpace:mHatMax = 3500."]

topAlg.Pythia8.UserHook = "WprimeWZFlat"
