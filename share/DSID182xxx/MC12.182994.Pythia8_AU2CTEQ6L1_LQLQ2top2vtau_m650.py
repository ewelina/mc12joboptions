
evgenConfig.generators = ["Pythia8"] 
evgenConfig.contact = ["Katharine Leney (at SPAMNOTcern.ch)"] 
evgenConfig.description = "LQ3 ->top + nu_tau, M_LQ = 650 GeV, Pythia 8"
evgenConfig.keywords    = [ "exotics", "leptoquark" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

# Turns on LQ decays to top
topAlg.Pythia8.UserResonances = "LQ:42"

topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:qqbar2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:kCoup = 0.01"]
topAlg.Pythia8.Commands += ["42:0:products = 6 16"]
topAlg.Pythia8.Commands += ["42:m0 = 650"]
topAlg.Pythia8.Commands += ["42:mMin = 550"]
topAlg.Pythia8.Commands += ["42:mMax = 750"]


