
include ( "MC12JobOptions/Pythia8_AU2_CT10_Common.py" )
include ( "MC12JobOptions/Pythia8_LHEF.py" )

evgenConfig.description = "BlackMax + Pythia8 with the AU2 tune and CT10 PDF, LM1: n=6, Mp=2 TeV, Mth=2 TeV"
evgenConfig.keywords = ["blackhole"]
evgenConfig.generators += ["BlackMax"]
evgenConfig.inputfilecheck = "BlackMax"
  