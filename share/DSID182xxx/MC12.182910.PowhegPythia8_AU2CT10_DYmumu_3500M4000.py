## POWHEG+Pythia8 DYmumu_3500M4000

evgenConfig.description = "POWHEG+Pythia8 Z/gamma*mu with mass cut DYmumu_3500M4000GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_3500M4000"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
