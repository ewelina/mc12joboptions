evgenConfig.contact = ["Olya Igonkina"]

#Excited Lepton ID
leptID = 4000015

# Excited lepton Mass (in GeV)
M_ExNu = 1000.0

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 4000.0

# this is double excited lepton production
include("MC12JobOptions/Pythia8_AU2CTEQ6L1_ExcitedNueNue.py")
evgenConfig.description = "Double Excited tauStar production, with the CTEQ6L1 tune"

