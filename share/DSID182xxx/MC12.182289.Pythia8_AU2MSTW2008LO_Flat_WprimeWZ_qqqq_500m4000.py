# W' -> WZ -> qq+qq JO: Jul. 2013 - K. Terashi

evgenConfig.description = "Flat EGM Wprime->WZ (all-hadronic) with AU2 MSTW2008LO tune"
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.keywords = ["Exotics", "EGM", "SSM", "Wprime"]

#Tune
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                            "Wprime:coup2WZ = 1.", #Wprime Coupling to WZ
                            "34:onMode = off", #turn off all Wprime decays
                            "34:onIfMatch = 24 23", #Wprime->WZ
                            "24:onMode = off", #turn off all W decays
                            "24:onIfAny = 1 2 3 4 5 6 ", #W->qq
                            "23:onMode = off", #turn off all Z decays
                            "23:onIfAny = 1 2 3 4 5 6" #Z->qq
                           ]
                           
topAlg.Pythia8.Commands += ["34:mMin = 500.",
                            "34:mMax = 4000.",
                            "PhaseSpace:mHatMin = 500.",
                            "PhaseSpace:mHatMax = 4000."]

topAlg.Pythia8.UserHook = "WprimeWZFlat"
