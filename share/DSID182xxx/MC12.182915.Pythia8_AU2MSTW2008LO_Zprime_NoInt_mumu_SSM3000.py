evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]
evgenConfig.description = "Z'->mm production"
evgenConfig.keywords = ["HeavyBoson", "Zprime", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# Zprime resonance mass (in GeV)
ZprimeMass = 3000

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",    # create Z' bosons
                            "Zprime:gmZmode = 3",                    # Z',Z,g with interference
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny  = 13", # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]
