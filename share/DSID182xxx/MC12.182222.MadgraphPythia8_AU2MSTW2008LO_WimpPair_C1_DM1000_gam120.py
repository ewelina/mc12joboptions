include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_LHEF.py")
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2",
                            "1000022:all = chid chid~ 2 0 0 1000. 0"
                            ]

evgenConfig.description = "mono photon pT > 120GeV : C1 DM 1000 GeV"
evgenConfig.keywords = ["mono photon"]
evgenConfig.inputfilecheck = 'gam'
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["ning.zhou@cern.ch"]
