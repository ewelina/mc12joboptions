evgenConfig.description = "MadGraph UFO Z->3photons"
evgenConfig.keywords = ["SMZ", "rare Z decay", "Z", "3photons", "3gamma", "multiphoton"]
evgenConfig.inputfilecheck = "182999.SM_Z_3photon"
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact  = [ "j.beacham@cern.ch" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
