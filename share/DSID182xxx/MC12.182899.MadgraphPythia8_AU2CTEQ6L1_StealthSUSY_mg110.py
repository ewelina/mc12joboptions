evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.description = "Stealth SUSY singlino to singlet, mg110"
evgenConfig.keywords = ["stealth","SUSY","singlino"]
evgenConfig.inputfilecheck = "stealth"
evgenConfig.contact = ["hrussell@cern.ch"]
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
topAlg.Pythia8.Commands += ["SLHA:useDecayTable = on", 
                            "SLHA:keepSM =off",
                            "SLHA:minMassSM = 0."]
