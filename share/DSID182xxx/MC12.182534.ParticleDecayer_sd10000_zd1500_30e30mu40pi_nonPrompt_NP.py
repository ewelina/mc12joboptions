evgenConfig.description = "Long-lived particle subgroup; prompt and non-prompt leptonjet samples"
evgenConfig.keywords = ["leptonjets"]

include("MC12JobOptions/ParticleGenerator_Common.py")
topAlg.ParticleGenerator.OutputLevel = FATAL
topAlg.ParticleGenerator.orders = [
  "pdgcode[0]:  constant 999",
  "et[0]: flat 15000. 100000.",
  "eta[0]: flat -2.5 2.5",
  "phi[0]: flat -3.14159 3.14159"
]

include("MC12JobOptions/ParticleDecayer_Common.py")
topAlg.ParticleDecayer.OutputLevel = FATAL
topAlg.ParticleDecayer.LJType = 2 
topAlg.ParticleDecayer.ScalarMass = 10000 
topAlg.ParticleDecayer.ScalarPDGID = 700021 
topAlg.ParticleDecayer.ParticleID = 999 
topAlg.ParticleDecayer.ParticleMass = 1500 
topAlg.ParticleDecayer.ParticlePolarization = 0 
topAlg.ParticleDecayer.ParticlePDGID = 700022 
topAlg.ParticleDecayer.DecayBRElectrons = 0.30 
topAlg.ParticleDecayer.DecayBRMuons     = 0.30 
topAlg.ParticleDecayer.DecayBRPions     = 0.40 
topAlg.ParticleDecayer.ParticleLifeTime = 0 
topAlg.ParticleDecayer.DoUniformDecay               = False  
topAlg.ParticleDecayer.DoExponentialDecay           = True   
topAlg.ParticleDecayer.ExpDecayDoVariableLifetime   = True   
topAlg.ParticleDecayer.ExpDecayPercentageToKeep     = 0.8
topAlg.ParticleDecayer.ExpDecayDoTruncateLongDecays = True
topAlg.ParticleDecayer.BarrelRadius         = 8.e3 
topAlg.ParticleDecayer.EndCapDistance       = 11.e3 
topAlg.ParticleDecayer.ThetaEndCapBarrel    = 0.628796286 

