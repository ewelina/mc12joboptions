evgenConfig.description = "Protos+Pythia6 for same sign tops"

evgenConfig.keywords = ["tt", "top"]

evgenConfig.inputfilecheck = "ttLL_LessPS"

evgenConfig.generators = ["Protos", "Pythia"]

# ... Pythia
include("MC12JobOptions/Pythia_CTEQ6L1_Perugia2011C_LessPS_Common.py")

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

#
###############################################################
