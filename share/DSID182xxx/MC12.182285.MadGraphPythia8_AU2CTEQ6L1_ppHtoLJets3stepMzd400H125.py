evgenConfig.description    = "Madgraph+Pythia8, Higgs production with a Higgs boson decaying to lepton-jets, ppH 3step-model mzd=400MeV mH=125GeV"
evgenConfig.keywords       = ["nonSMhiggs","lepton jets","dark photon", "exotics"]
evgenConfig.generators     = ['MadGraph','Pythia8']
evgenConfig.contact        = ["andrii.tykhonov@cern.ch","Maksym.Deliyergiyev@cern.ch"]
evgenConfig.inputfilecheck = 'HtoLJets'

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

