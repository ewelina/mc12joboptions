# W' -> WH -> qq+all

evgenConfig.description = "Wprime(->WH->qq+all) 2000 GeV in default LRSM"
evgenConfig.contact = ["koji.terashi@cern.ch"]
evgenConfig.generators = ["Pythia"]
evgenConfig.keywords = ["Exotics", "LRSM", "Wprime", "WH"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                                
                                # Wprime production                                
                                "pysubs msub 142 1", 

                                # W' mass
                                "pydat2 pmas 34 1 2000",

                                # W' decay
                                "pydat3 mdme 311 1 0",   # d ubar
                                "pydat3 mdme 312 1 0",   # d cbar
                                "pydat3 mdme 313 1 0",   # d tbar
                                "pydat3 mdme 315 1 0",   # s ubar
                                "pydat3 mdme 316 1 0",   # s cbar
                                "pydat3 mdme 317 1 0",   # s tbar
                                "pydat3 mdme 319 1 0",   # b ubar
                                "pydat3 mdme 320 1 0",   # b cbar
                                "pydat3 mdme 321 1 0",   # b tbar
                                "pydat3 mdme 327 1 0",   # e nue
                                "pydat3 mdme 328 1 0",   # mu numu
                                "pydat3 mdme 329 1 0",   # tau nutau
                                "pydat3 mdme 331 1 0",   # W Z
                                "pydat3 mdme 332 1 0",   # W gamma

                                # W' decay to WH
                                "pydat3 mdme 333 1 1",   # W H

                                # W decay to qq
                                "pydat3 mdme 190 1 1",   # dbar u
                                "pydat3 mdme 191 1 1",   # dbar c
                                "pydat3 mdme 192 1 1",   # dbar t
                                "pydat3 mdme 194 1 1",   # sbar u
                                "pydat3 mdme 195 1 1",   # sbar c
                                "pydat3 mdme 196 1 1",   # sbar t
                                "pydat3 mdme 198 1 1",   # bbar u
                                "pydat3 mdme 199 1 1",   # bbar c
                                "pydat3 mdme 200 1 1",   # bbar t
                                "pydat3 mdme 206 1 0",   # e nu_e
                                "pydat3 mdme 207 1 0",   # mu nu_mu
                                "pydat3 mdme 208 1 0",   # tau nu_tau

                                # H mass
                                "pydat2 pmas 25 1 125",

                                # H decay to all
                                "pydat3 mdme 210 1 1",   # d dbar
                                "pydat3 mdme 211 1 1",   # u ubar
                                "pydat3 mdme 212 1 1",   # s sbar
                                "pydat3 mdme 213 1 1",   # c cbar
                                "pydat3 mdme 214 1 1",   # b bbar
                                "pydat3 mdme 215 1 1",   # t tbar
                                "pydat3 mdme 218 1 1",   # e- e+
                                "pydat3 mdme 219 1 1",   # mu- mu+
                                "pydat3 mdme 220 1 1",   # tau- tau+
                                "pydat3 mdme 222 1 1",   # g g
                                "pydat3 mdme 223 1 1",   # gamma gamma
                                "pydat3 mdme 224 1 1",   # gamma Z
                                "pydat3 mdme 225 1 1",   # Z Z
                                "pydat3 mdme 226 1 1",   # W W

                                # Turn off FSR for Photos
                                "pydat1 parj 90 20000",

                                # Turn off tau decays
                                "pydat3 mdcy 15 1 0",

                                "pystat 1 3 4 5",
                                "pyinit dumpr 1 5",
                                "pyinit pylistf 1",
                                "pyinit pylisti 12"]

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )
