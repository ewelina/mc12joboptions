include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
#include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            "Merging:doKTMerging = on",
                            "Merging:nJetMax = 2",
                            "Merging:Process = pp>LEPTONS,NEUTRINOS",
                            "Merging:TMS = 30"]

evgenConfig.generators += ["MadGraph","Pythia8"]
evgenConfig.contact = ["Frederick.Dallaire@cern.ch or Merlin.Davies@cern.ch"]
evgenConfig.description = "pp -> (l-v) + (l+l-) production (electrons, muons and taus included), higgs mass was set to 120 GeV"
evgenConfig.keywords = ["WZ", "Exotics", "Dibosons", "Multileptons"]
evgenConfig.inputfilecheck = 'WminusZ'
