evgenConfig.generators  = ["Pythia"]
evgenConfig.tune        = "Perugia2012"
evgenConfig.contact     = ["Katharine Leney (at SPAMNOTcern.ch)"] 
evgenConfig.description = "LQ3 ->b + nu_tau, M_LQ = 700 GeV, Pythia6, Efficiency ~96%"
evgenConfig.keywords    = [ "exotics", "leptoquark" ]

from Pythia_i.Pythia_iConf import Pythia
topAlg += Pythia()

topAlg.Pythia.Tune_Name = "PYTUNE_370"

topAlg.Pythia.PythiaCommand +=  [# Initialisations
                                 "pyinit pylisti 12",
                                 "pyinit pylistf 1",
                                 "pystat 1 3 4 5",
                                 "pyinit dumpr 1 20",
                                 # Weak mixing angle
                                 "pydat1 paru 102 0.23113",
                                 # Masses
                                 "pydat2 pmas 6 1 172.5",     # Top mass
                                 "pydat2 pmas 24 1 80.399",   # PDG2010 W mass
                                 "pydat2 pmas 23 1 91.1876",  # PDG2010 Z0 mass
                                 # Leptoquark stuff
                                 "pysubs msel 0"          ,
                                 "pysubs msub 163 1"      ,     # gg    -> LQLQ
                                 "pysubs msub 164 1"      ,     # qqbar -> LQLQ
                                 "pydat2 pmas 42 1 700."  ,
                                 "pysubs ckin 41 600.0"     ,
                                 "pysubs ckin 42 800.0"   ,
                                 "pysubs ckin 43 600.0"     ,
                                 "pysubs ckin 44 800.0"   ,
                                 "pydat3 brat 539 1."     ,
                                 "pydat3 kfdp 539 1 5"    ,
                                 "pydat3 kfdp 539 2 16"   ,
                                 "pydat1 paru 151 0.01"
                                 ]

# MET filter
include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 80*GeV
topAlg.METFilter.MissingEtCalcOption = 1
