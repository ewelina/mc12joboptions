include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa diboson bbbb, inclusive ZZ, up to 1 jets with ME+PS."
evgenConfig.keywords = ["diboson", "bbbb", "ZZ"]
evgenConfig.contact  = ["luke.lambourne@cern.ch"]
evgenConfig.minevents = 200

evgenConfig.process="""
(run){
  ACTIVE[25]=1
  ERROR=0.02
}(run)

(processes){
  Process 93 93 -> 5 -5 5 -5 93{1};
  Integration_Error 0.05 {5}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Print_Graphs FeynGraphs
  End process;
}(processes)

(selector){
  PT 5 25 E_CMS
  PseudoRapidity 5 -2.8 2.8
  PT -5 25 E_CMS
  PseudoRapidity -5 -2.8 2.8
  DeltaR 5 -5 0.1 10

}(selector)
"""

include("MC12JobOptions/Boosted2Dijet_160_FilterFragment.py")
