evgenConfig.contact = [ "daniel.hayden@cern.ch" ]
evgenConfig.generators = [ "Pythia8" ]
evgenConfig.description = "Z'->ee production"
evgenConfig.keywords = ["HeavyBoson", "Zprime", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# Zprime resonance mass (in GeV)
ZprimeMass = 5000

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",    # create Z' bosons
                            "Zprime:gmZmode = 3",                    # Z',Z,g with interference
                            "Zprime:vd = 0.783156008298",
                            "Zprime:ad = -0.391578004149",
                            "Zprime:vu = 0.0",
                            "Zprime:au = 0.391578004149",
                            "Zprime:ve = -0.783156008298",
                            "Zprime:ae = -0.391578004149",
                            "Zprime:vnue = -0.587367006224",
                            "Zprime:anue = -0.587367006224",
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny  = 11", # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]
