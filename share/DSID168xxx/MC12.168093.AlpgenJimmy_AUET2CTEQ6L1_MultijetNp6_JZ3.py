include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np6_JZ3'
evgenConfig.description = 'pp -> 6 or more light jets, for leading jet pT range = 200-500 GeV'
evgenConfig.minevents = 50
