include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np6_JZ4'
evgenConfig.minevents = 50
evgenConfig.description = 'pp -> 6 or more light jets, for leading jet pT range = 500-1000 GeV'
