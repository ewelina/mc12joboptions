include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np5_JZ3'
evgenConfig.minevents = 100
evgenConfig.description = 'pp -> 5 light jets, for leading jet pT range = 200-500 GeV'
