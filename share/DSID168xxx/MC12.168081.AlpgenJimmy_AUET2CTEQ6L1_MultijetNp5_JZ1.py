include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np5_JZ1'
evgenConfig.description = 'pp -> 5 light jets, for leading jet pT range = 20-80 GeV'
if runArgs.ecmEnergy == 7000.0:
  evgenConfig.minevents = 500
elif runArgs.ecmEnergy == 8000.0:
  evgenConfig.minevents = 100
else:
  print "ERROR: Invalid ecmEnergy:", runArgs.ecmEnergy
