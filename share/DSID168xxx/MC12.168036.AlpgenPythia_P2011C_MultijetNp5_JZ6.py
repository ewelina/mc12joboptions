include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np5_JZ6'
evgenConfig.minevents = 50
evgenConfig.description = 'pp -> 5 light jets, for leading jet pT range = 1500-2000 GeV'
