include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np4_JZ3'
evgenConfig.minevents = 200
evgenConfig.description = 'pp -> 4 light jets, for leading jet pT range = 200-500 GeV'
