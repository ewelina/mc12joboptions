evgenConfig.description = "Single photons with E=100 GeV"
evgenConfig.keywords = ["singleparticle", "gamma"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: constant 22",
 "e: constant 100000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
