evgenConfig.description = "Single electrons with E=25 GeV"
evgenConfig.keywords = ["singleparticle", "e"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "e: constant 25000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
