evgenConfig.description = "Pi+ and Pi- close by"
evgenConfig.keywords = ["two_singleparticle", "piplus_piminus"]
evgenConfig.contact  = ["christopher.young@cern.ch"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = INFO
topAlg.ParticleGenerator.orders = [
 "pdgcode[0]: constant 211",
 "pt[0]: 7000",
 "eta[0]: flat 0.0 2.5",
 "phi[0]: flat 0.0 3.14",
 "pdgcode[1]: constant -211",
 "pt[1]: 15000",
 "eta[1]: flat -0.2 2.7",
 "phi[1]: flat 0.0 3.14",
 ]
