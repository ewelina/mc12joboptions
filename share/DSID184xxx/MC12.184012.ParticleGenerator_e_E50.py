evgenConfig.description = "Single electrons with E=50 GeV"
evgenConfig.keywords = ["singleparticle", "e"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = VERBOSE
topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -11 11",
 "e: constant 50000",
 "eta: flat -2.5 2.5",
 "phi: flat -3.14159 3.14159"
 ]
