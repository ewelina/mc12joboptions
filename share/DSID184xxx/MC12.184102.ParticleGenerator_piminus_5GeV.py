evgenConfig.description = "Single Pi- with E 5 GeV"
evgenConfig.keywords = ["singleparticle", "piminus"]

include("MC12JobOptions/ParticleGenerator_Common.py")

topAlg.ParticleGenerator.OutputLevel = INFO
topAlg.ParticleGenerator.orders = [
    "PDGcode: constant -211",
    "e: constant 5000.",
    "eta: flat -3.0 3.0",
    "phi: flat -3.14159 3.14159"
 ]

