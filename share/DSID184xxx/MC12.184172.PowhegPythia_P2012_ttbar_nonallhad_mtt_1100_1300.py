include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia6 ttbar production with Perugia 2012 tune and LeptonFilter mtt slice 1100-1300 GeV"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["lily.asquith@cern.ch"]
evgenConfig.minevents      = 2000   

postGenerator="Pythia6TauolaPhotos"
postGeneratorTune ="Perugia2012"
process="tt_all"

# compensate filter efficiency
evt_multiplier = 1e3

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarMassFilter.py')
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1100000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1300000.

include('MC12JobOptions/TTbarWToLeptonFilter.py')