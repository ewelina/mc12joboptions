
evgenConfig.description = "DY->mumu production, NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Z", "DY", "leptons"]
evgenConfig.contact = ['Noam.Hod@cern.ch']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on",
                            "23:onMode = off",                    # turn off all decays modes
                            "23:onIfAny = 13",                    # turn on the mu+mu- decay mode
                            "PhaseSpace:mHatMin = 1750.",         # lower invariant mass
                            "PhaseSpace:mHatMax = 2000."]         # upper invariant mass

