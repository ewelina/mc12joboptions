########################
### Job Options File ###
## By: Daniel Hayden ###
#### RS G* -> mumu #####
########################

evgenConfig.description = "G*->mumu production with no lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "G*", "RS", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["PDF:pSet= 5",
                            "ExtraDimensionsG*:gg2G* = on", # create G* bosons (gg production)
                            "ExtraDimensionsG*:ffbar2G* = on", # create G* bosons (qq production)
                            "ExtraDimensionsG*:SMinBulk = off", # SM on the TeV brane
                            "ExtraDimensionsG*:VLVL = on", # standard Z/W boson couplings
                            "ExtraDimensionsG*:kappaMG = 0.054",
                            "5100039:onMode = off", # switch off all Z decays
                            "5100039:onIfAny = 13", # switch on Z->ee decays
                            "5100039:m0 = 1000.",    # new mass
                            "5100039:mMin = 50.",                     # lower allowed mass
#                            "5100039:mMax = x",                    # upper allowed mass
                            "PhaseSpace:mHatMin = 50."]          # lower invariant mass
#                            "PhaseSpace:mHatMax = x"]         # upper invariant mass


#from TruthExamples.TruthExamplesConf import DumpMC
#topAlg += DumpMC()
