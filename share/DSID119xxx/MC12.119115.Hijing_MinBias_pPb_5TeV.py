###############################################################
#
# Job options file for Hijing generation of
# p + Pb collisions at 5023 GeV CMS (4.0 TeV p, 1.577 TeV Pb/n)
#
# Andrzej Olszewski
#
# October 2012
#==============================================================

# use common fragment
include("MC12JobOptions/Hijing_Common.py")

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["p-Pb", "MinBias"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 5023.", "frame CMS", "proj A", "targ P",
                     "iap 208", "izp 82", "iat 1", "izt 1",
# simulation of minimum-bias events
                     "bmin 0", "bmax 10",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1",
# turning OFF string radiation
                     "ihpr2 1 0",
# Nov 10,11, set minimum pt for hard scatterings to default, 2 GeV
                     "hipr1 8 2",
# Nov 10,11, turn off diffractive scatterings
                     "ihpr2 13 0"
                     ]


from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
BoostEvent=topAlg.BoostEvent
BoostEvent.BetaZ=-0.43448
BoostEvent.McInputKey="HIJING_EVENT"
BoostEvent.McOutputKey="GEN_EVENT"
