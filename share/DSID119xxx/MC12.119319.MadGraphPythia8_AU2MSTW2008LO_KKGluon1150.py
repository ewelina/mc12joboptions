include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")


evgenConfig.description = "MadGraph5+Pythia8 mstw2008LO  -  KKGluonTTBar  -  pp -> o1 -> ttbar ALLchannels  -  where o1 is the colour octet state in the topBSM model  -  Couplings according to Randall & Lillie  -  GAMMA/M=15.3%  -  M=1150 GeV"
evgenConfig.keywords = ["KKGluon"]
evgenConfig.inputfilecheck = "KKGluonTTbar1150"
