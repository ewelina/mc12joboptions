## POWHEG+Pythia8 Z->tautau neutron-proton beam central lepton filter

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production for neutron-proton beam with central lepton pt>10 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "ptlepton>10 GeV", "|etalepton|<2.7", "neutron-proton beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Ztautau_np"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.7
