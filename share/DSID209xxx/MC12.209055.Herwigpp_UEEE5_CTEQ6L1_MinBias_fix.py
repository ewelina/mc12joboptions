## Job options file for Herwig++, min bias production

evgenConfig.description = "Herwig++ MB  with the CTEQ6L1 EE5 tune, fix for 2.7.1"
evgenConfig.generators = ["Herwigpp"]
evgenConfig.keywords = ["minBias"]

include ("MC12JobOptions/Herwigpp_UEEE5_CTEQ6L1_Common.py")

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEMinBias
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0

## The cuts are set by creating a new Cuts objects.
## This is done due to an issue in 2.7.1, once it will be fixed in a new version, we can go back to the usual setup (as in DSID 187503 in MC12).
cd /Herwig/Cuts
create ThePEG::Cuts MinBiasCuts
set MinBiasCuts:ScaleMin 2.0*GeV
set MinBiasCuts:X1Min 0.01
set MinBiasCuts:X2Min 0.01
set MinBiasCuts:MHatMin 0.0*GeV
set /Herwig/Generators/LHCGenerator:EventHandler:Cuts /Herwig/Cuts/MinBiasCuts
"""

topAlg.Herwigpp.Commands += cmds.splitlines()
