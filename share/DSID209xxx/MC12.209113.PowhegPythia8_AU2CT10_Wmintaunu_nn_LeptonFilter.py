## POWHEG+Pythia8 Wmin->taunu for neutron-neutron beam central lepton filter

evgenConfig.description = "POWHEG+Pythia8 Wmin->taunu production for neutron-neutron beam with central lepton pt>10 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau", "ptlepton>10 GeV", "|etalepton|<2.7", "neutron-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wmintaunu_nn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# force leptonic tau decays
topAlg.Pythia8.Commands += [
    '15:onMode = off',
    '15:onIfAny = 11 12 13 14'
    ]

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.7
