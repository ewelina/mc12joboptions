## POWHEG+Pythia8 Zmumu_nn

evgenConfig.description = "POWHEG+Pythia8 Zmumu for neutron-neutron beam, m>60GeV, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "neutron-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu_nn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
