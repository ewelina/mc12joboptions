## Job options file for Herwig++, QCD jet slice production
## A.A.,A.O., Feb '14

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")

evgenConfig.description = "QCD dijet production JZ2R04 with EE3 tune"
evgenConfig.keywords = ["QCD", "jets"]


cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 40*GeV
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/JetFilter_JZ2R04.py")
evgenConfig.minevents = 1000 
