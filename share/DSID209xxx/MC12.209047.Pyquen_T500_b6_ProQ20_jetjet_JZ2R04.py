evgenConfig.description = "Dijet truth jet slice JZ2, with Pythia 6 Pro-Q20 plus Pyquen jet quenching and R=0.4"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pyquen_T500_b6_ProQ20_Common.py")

topAlg.Pyquench.PythiaCommand += [
                                  "pysubs msel 0",
                                  "pysubs ckin 3 40.",
                                  "pysubs msub 11 1",
                                  "pysubs msub 12 1",
                                  "pysubs msub 13 1",
                                  "pysubs msub 68 1",
                                  "pysubs msub 28 1",
                                  "pysubs msub 53 1"]


include("MC12JobOptions/JetFilter_JZ2R04.py")
evgenConfig.minevents = 2000

