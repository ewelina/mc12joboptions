#######################################################################
# Job options fragment for pp->X Upsilon(3S)->mu0mu0, eta < 3.1
#######################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Up(3S)->(mu0mu0)"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

topAlg.Pythia8B.Commands += ['100553:m0 = 10.355']     #Convert 2S into a 3S
topAlg.Pythia8B.Commands += ['100553:mMin = 10.3547']  #Convert 2S into a 3S
topAlg.Pythia8B.Commands += ['100553:mMax = 10.3553']  #Convert 2S into a 3S
topAlg.Pythia8B.Commands += ['100553:mWidth = 0.00002']


topAlg.Pythia8B.Commands += ['9900553:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9900553:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9900553:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9900553:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9900551:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9900551:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9900551:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9900551:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9910551:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9910551:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9910551:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9910551:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['100553:onMode = off']
topAlg.Pythia8B.Commands += ['100553:1:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100553,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [0.0]
topAlg.Pythia8B.TriggerStateEtaCut = 3.1
topAlg.Pythia8B.MinimumCountPerCut = [2]
