evgenConfig.description = "Dijet truth jet slice JZ1, with Pythia 6 AUET2B and R=0.4 with b filter"
evgenConfig.keywords = ["QCD", "jets", "b filter"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

# use min bias ND for this slice
topAlg.Pythia.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           ]


include("MC12JobOptions/JetFilter_JZ1R04.py")
evgenConfig.minevents = 2000
include("PutAlgsInSequence.py")

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
if 'HeavyFlavorHadronFilter' not in topAlg:
    topAlg += HeavyFlavorHadronFilter()
if 'HeavyFlavorHadronFilter' not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  [ "HeavyFlavorHadronFilter" ]


topAlg.HeavyFlavorHadronFilter.RequestCharm=False
topAlg.HeavyFlavorHadronFilter.RequestBottom=True
topAlg.HeavyFlavorHadronFilter.BottomPtMin=2*GeV
topAlg.HeavyFlavorHadronFilter.Request_cQuark=False
topAlg.HeavyFlavorHadronFilter.Request_bQuark=False
