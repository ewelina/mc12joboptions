## POWHEG+Pythia8 Z->tautau proton-neutron beam central lepton filter

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production for proton-neutron beam with central lepton pt>10 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "ptlepton>10 GeV", "|etalepton|<2.7", "proton-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Ztautau_pn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.7
