evgenConfig.description = "Dijet truth jet slice JZ2, with Pythia 6 AUET2B and R=0.4 with c filter"
evgenConfig.keywords = ["QCD", "jets", "c filter"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += [
                                  "pysubs msel 0",
                                  "pysubs ckin 3 40.",
                                  "pysubs msub 11 1",
                                  "pysubs msub 12 1",
                                  "pysubs msub 13 1",
                                  "pysubs msub 68 1",
                                  "pysubs msub 28 1",
                                  "pysubs msub 53 1"]


include("MC12JobOptions/JetFilter_JZ2R04.py")
evgenConfig.minevents = 2000
include("PutAlgsInSequence.py")

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
if 'HeavyFlavorHadronFilter' not in topAlg:
    topAlg += HeavyFlavorHadronFilter()
if 'HeavyFlavorHadronFilter' not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  [ "HeavyFlavorHadronFilter" ]

topAlg.HeavyFlavorHadronFilter.RequestCharm=True
topAlg.HeavyFlavorHadronFilter.RequestBottom=False
topAlg.HeavyFlavorHadronFilter.CharmPtMin=2*GeV
topAlg.HeavyFlavorHadronFilter.Request_cQuark=False
topAlg.HeavyFlavorHadronFilter.Request_bQuark=False
