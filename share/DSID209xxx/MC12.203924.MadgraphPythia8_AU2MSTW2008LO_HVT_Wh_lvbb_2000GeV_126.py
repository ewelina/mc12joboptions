###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Heavy vector triplet Vc -> WH -> lvbb with MSTW2008LO PDF and 1 lepton filter"
evgenConfig.keywords = ["exotic", "resonance"]
evgenConfig.contact = ["frederick.dallaire@cern.ch"]
evgenConfig.generators = ["MadGraph"]
evgenConfig.inputfilecheck = 'HVT_Wh_lvbb'

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 1
