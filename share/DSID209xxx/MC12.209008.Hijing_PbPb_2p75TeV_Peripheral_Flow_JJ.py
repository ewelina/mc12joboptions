###############################################################
#
# Job options file for Hijing generation of
# Pb + Pb collisions at 2750 GeV/(colliding nucleon pair)
# Peripheral sample (10-20) fm with 2010 JJ flow
#
# Andrzej Olszewski
#
# February 2014
#==============================================================

# use common fragment
include("MC12JobOptions/Hijing_Common.py")

from FlowAfterburner.FlowAfterburnerConf import AddFlowByShifting
topAlg += AddFlowByShifting()

evgenConfig.description = "Hijing"
evgenConfig.keywords = ["PbPb", "Peripheral", "Flow"]
evgenConfig.contact = ["Andrzej.Olszewski@ifj.edu.pl"]

#----------------------
# Hijing Parameters
#----------------------
Hijing = topAlg.Hijing
Hijing.McEventKey = "HIJING_EVENT"
Hijing.Initialize = ["efrm 2750.", "frame CMS", "proj A", "targ A",
                     "iap 208", "izp 82", "iat 208", "izt 82",
# simulation of minimum-bias events
                     "bmin 10", "bmax 20",
# turns OFF jet quenching:
                     "ihpr2 4 0",
# Jan24,06 turns ON decays charm and  bottom but not pi0, lambda, ...
                     "ihpr2 12 2",
# turns ON retaining of particle history - truth information:
                     "ihpr2 21 1"]


AddFlowByShifting = topAlg.AddFlowByShifting
AddFlowByShifting.McTruthKey    = "HIJING_EVENT"
AddFlowByShifting.McFlowKey     = "GEN_EVENT"

#----------------------
# Flow Parameters
#----------------------
AddFlowByShifting.FlowFunctionName = "jjia_minbias_new"
AddFlowByShifting.FlowImplementation="exact" # "approximate" , "exact"
