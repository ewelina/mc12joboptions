evgenConfig.description = "Inclusive bb->mu2mu2 production, eta < 3.1"
evgenConfig.keywords = ["inclusive","bbbar","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 2.0
topAlg.Pythia8B.AntiQuarkPtCut = 2.0
topAlg.Pythia8B.QuarkEtaCut = 4.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False

topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.NHadronizationLoops = 15

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.0]
topAlg.Pythia8B.TriggerStateEtaCut = 3.1
topAlg.Pythia8B.MinimumCountPerCut = [2]

