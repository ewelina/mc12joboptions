evgenConfig.description = "Inelastic minimum bias, with the A2 CTEQ6L1 tune"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += \
    ["SoftQCD:nonDiffractive = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]
