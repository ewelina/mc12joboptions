# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

evgenConfig.description = "Herwig++ Zee sample with CTEQ6L1 EE3 tune"
evgenConfig.keywords = ["electron", "Z"]
evgenConfig.contact = ["Orel Gueta"]

# Configure Herwig
cmds = """\
## Set up qq -> Z -> e+ e- process
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEqq2gZ2ff
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:Process Electron
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

# Set up lepton filter
include("MC12JobOptions/LeptonFilter.py")
evgenConfig.minevents = 5000

