include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Low mass VBF-Z production, with Z/gamma* -> mm."
evgenConfig.keywords = [ "Sherpa", "VBF" ]
evgenConfig.contact  = [ "kiran.joshi@cern.ch", "jvannieu@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(processes){
  Process 93 93 -> 13 -13 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  Mass 13 -13 7 40
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '181348.Sherpa_CT10_Zmm2jEWM7to40'
