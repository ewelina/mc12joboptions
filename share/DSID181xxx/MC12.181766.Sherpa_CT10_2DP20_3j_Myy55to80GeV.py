include("MC12JobOptions/Sherpa_CT10_Common.py")
	
evgenConfig.description = "Diphoton + jets production using ME+PS with up to three jets from the matrix element. Apply filter requiring two prompt photons and 55 < Mgamgam < 80 GeV."
evgenConfig.keywords = ["jets", "photon", "diphoton"]
evgenConfig.contact  = ["tatsuya.masubuchi@cern.ch", "junichi.tanaka@cern.ch","frank.siegert@cern.ch"]
evgenConfig.minevents = 2000

evgenConfig.process = """
(run){
  SCALES VAR{Abs2(p[2]+p[3])/4.0}
  ME_QED = Off
  QCUT:=7.0
}(run)
	
(processes){
  Process 21 21 -> 22 22
  Loop_Generator gg_yy
  End process;

  Process 93 93 -> 22 22 93{3}
  Order_EW 2
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/(Abs2(p[2]+p[3])/4.0));
  Integration_Error 0.1;
  End process
}(processes)
	
(selector){
  PT      22      14.0 E_CMS
  Mass    22  22  55.0 80.0
  DeltaR  22  93  0.3  100.0
}(selector)
"""

evgenConfig.inputconfcheck ="Sherpa_CT10_2DP20_3j_Myy55to80GeV"

## Filter
include("MC12JobOptions/DirectPhotonFilter.py")
topAlg.DirectPhotonFilter.Ptcut = 20000.
topAlg.DirectPhotonFilter.Etacut =  2.7
topAlg.DirectPhotonFilter.NPhotons = 2
