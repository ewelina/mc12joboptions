## POWHEG+Pythia8 DYmumu_100M160

evgenConfig.description = "POWHEG+Pythia8 DYmumu with mass cut 100<M<160GeV and di-lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_100M160"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 20000.
topAlg.LeptonFilter.Etacut = 2.7
