evgenConfig.description = "POWHEG+Pythia8 Z->nunu H->bb production and AU2 CT10 tune"
evgenConfig.keywords = ["ZH" ,"NLO", "Powheg", "pythia8", "bottom","neutrino"]
evgenConfig.inputfilecheck = 'powheg.181451.ZnutaunutauH_SM_M125' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',
                            '25:onIfMatch = 5 5'
                            ]
