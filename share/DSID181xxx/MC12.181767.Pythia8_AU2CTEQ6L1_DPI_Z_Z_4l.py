evgenConfig.description = "Double Parton Interaction production of Z/Gm*->ll and Z/Gm*->ll. 4lepton filter of pT>3 GeV."
evgenConfig.keywords = ["electroweak", "Z", "leptons", "l", "DPI"]
evgenConfig.contact = ["Roberto Di Nardo <Roberto.Di.Nardo@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")




topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate Z bosons
                            "SecondHard:generate = on",       # second hard process
                            "SecondHard:SingleGmZ = on",      # another Z
                            "23:mMin = 8",                    # Z/Gm* mass down to 8 GeV
                            "23:onMode = off",                # switch off all Z decays
                            "23:onIfAny = 11",                # switch on Z->ee decays
                            "23:onIfAny = 13"]                # switch on Z->uu decays


# 4-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut    = 3.*GeV
topAlg.MultiLeptonFilter.Etacut   = 5.
topAlg.MultiLeptonFilter.NLeptons = 4
