evgenConfig.description = "Prophecy4f/POWHEG+PYTHIA8 ggH H->2e2mu with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "Z","leptonic"]
evgenConfig.inputfilecheck = "ggH2e2mu_SM_M125"
evgenConfig.contact  = [ "daniela.rebuzzi@cern.ch" ]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '23:onMode = off' #decay of Z
                            ]
