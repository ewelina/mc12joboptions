include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma*(->mumu) + Z/gamma*(->tautau) with m_mumu > 4 GeV and m_tautau < 4 GeV"
evgenConfig.keywords    = ["diboson", "Zgamma*", "Zgammastar"]
evgenConfig.contact     = ["david.hall@cern.ch"]
evgenConfig.inputconfcheck = "181478.Sherpa_CT10_tautaumumu_1p_lt4gt4"

evgenConfig.process = """
(run){
  INT_MINSIJ_FACTOR=1.e-14
  ERROR=0.01
  ACTIVE[25]=0
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 13 -13 15 -15 93{1};
  Order_EW 4
  CKKW sqr(25.0/E_CMS);
  Integration_Error 0.02 {5,6}
  End process;
}(processes)

(selector){
  "m" 13,-13 4,E_CMS
  "m" 15,-15 3.55,4
  "PT" 991 5.0,E_CMS:5.0,E_CMS:1.0,E_CMS [PT_UP]
  "Eta" 991 -1000,1000:-1000,1000:-3.0,3.0:-3.0,3.0 [ETA_UP]
}(selector)
"""
