evgenConfig.generators += ["MadGraph"]
evgenConfig.description = "H(150)->Z'Z'->4l m_Z'=50"
evgenConfig.keywords = ["nonSMhiggs","ggF","Zprime","leptonic"]
evgenConfig.inputfilecheck= 'ggH150_ZpZp50_4l'
evgenConfig.minevents = 5000

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )
## ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += ["pyinit user madgraph",
				"pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2"
                               ] # Turn off tau decays.
                         

