evgenConfig.description = "POWHEG+Pythia8 using PowHel ttH H->inclusive (leptonic top decays) with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "leptonic"]
evgenConfig.contact = ["maria.moreno.llacer@cern.ch"]
evgenConfig.inputfilecheck = "PowHel-ttH_135"
evgenConfig.minevents = 500

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
include("MC12JobOptions/Pythia8_H135_HXSWG_InclusiveBRs.py")

# ... Filter (efficiency ~10%)
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0
