evgenConfig.description = "POWHEG+PYTHIA8 VBFH H->inclusive with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "inclusive"]
evgenConfig.inputfilecheck = "VBFH_SM_M105"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )
include("MC12JobOptions/Pythia8_H125_HXSWG_InclusiveBRs.py")

