evgenConfig.description = "POWHEG+PYTHIA8 using NWA, ggH H->WW->lvqq with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "W","semileptonic"]
evgenConfig.inputfilecheck = "ggH_NW_SM_M480"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',                           
                            '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]

# ... Filter
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 24
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [11,12,13,14,15,16]
topAlg.XtoVVDecayFilter.PDGChild2 = [1,2,3,4,5,6]
