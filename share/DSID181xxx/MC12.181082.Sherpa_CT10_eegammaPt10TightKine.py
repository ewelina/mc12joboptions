include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e e gamma production with up to three jets in ME+PS and pT_gamma>10 GeV."
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]
evgenConfig.inputconfcheck = "eegammaPt10"

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  11 -11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 E_CMS
  PT 22  10 E_CMS
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000

  Mass 11 -11 75.0 E_CMS
  "m" 11,-11,22 90.,E_CMS
  PT  11   8. E_CMS
  PT -11   8. E_CMS
  PseudoRapidity  11 -3. 3.
  PseudoRapidity -11 -3. 3.
  PseudoRapidity  22 -3. 3.
}(selector)
"""
