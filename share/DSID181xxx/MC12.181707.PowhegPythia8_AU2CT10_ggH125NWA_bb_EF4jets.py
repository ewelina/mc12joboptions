evgenConfig.description = "POWHEG+PYTHIA8 ggH H->bb with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "bottom"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 5 5'
                            ]

# TruthJet filters
from JetRec.JetGetters import *
akt4=make_StandardJetGetter('AntiKt',0.4,'Truth')
akt4alg = akt4.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter

topAlg += TruthJetFilter("FourJetsFilter")
FourJetsFilter = topAlg.FourJetsFilter
FourJetsFilter.OutputLevel=INFO;
FourJetsFilter.Njet=4;
FourJetsFilter.NjetMinPt=20.*GeV;
FourJetsFilter.NjetMaxEta=5.0;
FourJetsFilter.jet_pt1=20.*GeV;
FourJetsFilter.applyDeltaPhiCut=False;
FourJetsFilter.TruthJetContainer="AntiKt4TruthJets";

StreamEVGEN.RequireAlgs += [ "FourJetsFilter" ]
