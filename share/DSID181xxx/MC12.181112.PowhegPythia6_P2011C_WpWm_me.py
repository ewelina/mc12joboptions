evgenConfig.description = "POWHEG+Pythia6 WW production with P2011C tune"
evgenConfig.keywords = ["WW", "diboson", "leptonic"]
evgenConfig.contact  = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]
evgenConfig.inputfilecheck = 'Powheg_CT10.*.WpWm'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

## to fix in MC13: "Lhef" -> "Powheg"; don't use Tauola.

evgenConfig.generators += [ "Lhef"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

