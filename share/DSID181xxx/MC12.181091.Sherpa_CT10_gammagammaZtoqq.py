include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Zgamma+gammagamma decaying hadronically to qqgammagamma, merged with up to two additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.inputconfcheck = "gammagammaZtoqq"
evgenConfig.minevents = 1000
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "junichi.tanaka@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 -> 22 22 9923[a] 93{2}
  Decay 9923[a] -> 94 94
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;
}(processes)

(selector){
  PT 22 25.0 E_CMS
  DeltaR 22 22 0.2 1000.0
  DeltaR 22 93 0.2 1000.0
  DecayMass 9923 15.0 E_CMS
}(selector)
"""
