# Responsible: Chris Potter
# 6 November 2013
# W- -> e/mu/tau nu, mH+=95 GeV, ma1=10 GeV, tan beta=1

evgenConfig.description = "NMSSM H+->a1W from ttbar, t->bW"
evgenConfig.keywords = ["Higgs","NMSSM","BSM"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["Top:gg2ttbar=on",
                            "Higgs:useBSM=on",
                            "37:m0 = 95.",
                            "25:m0 = 10.",
                            "HiggsH1:parity=2",
                            "HiggsHchg:tanBeta=1.",
                            "6:onMode=3",
                            "6:onIfAny=37",
                            "24:onMode=2", # W- ->e/mu/tau nu
                            "24:onIfAny=11 13 15",
                            "37:onMode=off",
                            "37:onIfAny=25",
                            "25:onMode=off",
                            "25:onIfAny=13"]                           

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

topAlg.MultiLeptonFilter.Ptcut = 4000.
topAlg.MultiLeptonFilter.Etacut = 3.0
topAlg.MultiLeptonFilter.NLeptons = 3
    
StreamEVGEN.RequireAlgs += ["MultiLeptonFilter"]

