include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-Z production, with Z -> nunu."
evgenConfig.keywords = [ "Sherpa", "VBF" ]
evgenConfig.contact  = [ "biagio.di.micco@cern.ch" ]
evgenConfig.minevents = 500 

evgenConfig.process="""
(processes){
  Process 93 93 -> 91 91 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = 'Ztonunu2JetsEW1JetQCD15GeV'
