evgenConfig.description = "Z->tautau dilepton filter Alpgen+Pythia Perugia tune - 0 parton"
evgenConfig.keywords    = ["Z","leptonic"]
evgenConfig.contact     = ["Biagio Di Micco <biagio.di.micco@cern.ch>"]

include('MC12JobOptions/AlpgenControl_ZDY.py') 
evgenConfig.minevents = 5000 
