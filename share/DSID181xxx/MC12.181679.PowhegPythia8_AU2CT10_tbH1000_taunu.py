evgenConfig.description = "POWHEG+PYTHIA8 tHch Hch->tau nu with AU2,CT10"
evgenConfig.keywords = ["nonSMhiggs", "Hch","top","tau"]
evgenConfig.inputfilecheck = "tHpl_2HDM_M1000"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [ 'Higgs:useBSM = on',
                             '37:onMode = off',
                             '37:onIfMatch = 15 16'
                             ]
