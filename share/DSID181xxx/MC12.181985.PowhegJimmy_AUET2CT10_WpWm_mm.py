evgenConfig.description = "POWHEG+fHerwig/Jimmy WW production and AUET2 CT10 tune"
evgenConfig.keywords = ["WW", "diboson", "leptonic"]
evgenConfig.contact  = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]
evgenConfig.inputfilecheck = 'Powheg_CT10.*.WpWm_mm'

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

evgenConfig.generators += [ "Powheg"]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
