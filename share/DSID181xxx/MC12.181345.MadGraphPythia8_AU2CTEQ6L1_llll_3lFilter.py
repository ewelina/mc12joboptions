###llll production (4leptons, e mu, tau) with a 3leptons filter : pt cut at 7 GeV and DeltaR > 0.05 between the 3leptons. No cut on the 4th lepton.
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            '24:mMin = 2.0',
                            '24:onMode = off',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16',]
## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "llll >=3l"
evgenConfig.keywords = ["llll"]
evgenConfig.inputfilecheck = 'llll'
