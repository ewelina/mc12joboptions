include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')
include('MC12JobOptions/Pythia8_Photos.py')

evgenConfig.inputfilecheck = 'sm_hh_yybb'
evgenConfig.description = 'standard model dihiggs production, to yybb, with Madgraph HEFT2. includes the box'
evgenConfig.keywords =["SM", "hh", "non-resonant", "gamgam bb", "yybb"]
evgenConfig.contact = ["james.saxon@cern.ch"]
