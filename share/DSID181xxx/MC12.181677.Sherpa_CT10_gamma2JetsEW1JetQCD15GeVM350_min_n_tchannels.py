include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-gamma production Min_N_Tchannels option enabled"
evgenConfig.keywords = [ "Sherpa", "VBF", "Min_N_Tchannels" ]
evgenConfig.contact  = [ "rsv@cern.ch" ]

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 22 93 93 93{1}
  Order_EW 3
  Order_QCD 0
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  End process;
}(processes)

(selector){
  PT 22 10 E_CMS
  Mass 93 93 350 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '181677.Sherpa_CT10_gamma2JetsEW1JetQCD15GeVM350_min_n_tchannels'
