evgenConfig.description = "Madgraph tHWWj top->leptonic (C_F=-1) using AU2, CTEQ6L1"
evgenConfig.keywords = ["nonSMHiggs","W","top","leptonic"]
evgenConfig.inputfilecheck = "madgraph.181455.tHWW_tlep_cfm1"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

