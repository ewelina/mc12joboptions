evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'gg2WW WW->lnulnu using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['diboson', 'leptonic', 'EW']
evgenConfig.contact = ['Tiesheng.Dai@cern.ch']
evgenConfig.inputfilecheck = "gg2WW0240.106014.WpWm_munumunu"

include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ["iproc mcatnlo",
                                "modbos 1 3", "modbos 2 3",
                                "maxpr 10"]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')
