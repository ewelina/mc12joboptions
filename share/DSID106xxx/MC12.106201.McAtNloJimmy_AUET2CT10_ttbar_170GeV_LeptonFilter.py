evgenConfig.description = "McAtNlo+fHerwig/Jimmy ttbar production with m_top=170GeV TTbarWToLeptonFilter - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "leptonic", "mass"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
topAlg.Herwig.HerwigCommand+=[ "rmass 6 170.0" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
include("MC12JobOptions/TTbarWToLeptonFilter.py")
#topAlg.TTbarWToLeptonFilter.NumLeptons=-1 #require at least one lepton (not yet released)

