include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia6 ttbar production with Perugia 2011c tune and LeptonVeto mtt slice 1500-1700 GeV"
evgenConfig.keywords = ["top", "ttbar", "hadronic"]
evgenConfig.contact  = ["Christoph.Wasicki@cern.ch"]
evgenConfig.minevents      = 200   

postGenerator="Pythia6TauolaPhotos"
process="tt_all"

# compensate filter efficiency
evt_multiplier = 10e3

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarMassFilter.py')
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1500000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1700000.

include('MC12JobOptions/TTbarWToLeptonVeto.py')
