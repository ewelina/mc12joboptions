evgenConfig.description = "ttbar+gamma in the ljets+ll modes, scale=345GeV and pt(gamma) = 10 GeV"
evgenConfig.generators += ["MadGraph", "Jimmy", "Lhef" ]
evgenConfig.keywords = ["EW","ttbar","photon","ljets","leptonic"]
evgenConfig.contact  = ["andrey.loginov@yale.edu", "liza.mijovic@cern.ch" ]
evgenConfig.inputfilecheck = 'ttbarphoton'

include("MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

