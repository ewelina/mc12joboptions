evgenConfig.description = "ALPGEN+Pythia Z(->mumu)+2jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "mu"]
evgenConfig.inputfilecheck = "ZmumuNp2"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

