evgenConfig.description = "ALPGEN+Pythia Z(->ee) process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "e"]
evgenConfig.inputfilecheck = "ZeeNp0"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

