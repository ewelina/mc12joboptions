evgenConfig.description = "ALPGEN+Pythia W(->munu) process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W", "mu"]
evgenConfig.inputfilecheck = "WmunuNp0"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

