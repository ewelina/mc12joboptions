evgenConfig.description = "MEtop+Pythia strong FCNC LO direct single-top production with the AUET2B_CTEQ6L1 tune and MorePS param. settings"
evgenConfig.keywords = ["singletop","FCNC","leptonic","PS"]
evgenConfig.contact  = ["conrad.friedrich@cern.ch"]
evgenConfig.inputfilecheck = "MEtop10.117012.st_strongFCNC_cgt"

include ( "MC12JobOptions/MEtopPythia_AUET2B_CTEQ6L1_MorePS_Common.py" )

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

