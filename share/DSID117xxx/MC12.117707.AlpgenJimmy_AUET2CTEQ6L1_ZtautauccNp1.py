evgenConfig.description = "ALPGEN+Jimmy Z(->tautau)+cc+1jet process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW", "Z"]
evgenConfig.inputfilecheck = "ZtautauccNp1"
evgenConfig.minevents = 5000

include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
