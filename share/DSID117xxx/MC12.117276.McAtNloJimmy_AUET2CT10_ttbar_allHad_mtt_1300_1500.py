evgenConfig.description = "McAtNlo+fHerwig/Jimmy ttbar production with TTbarWToLeptonFilter veto and TTbarMassFilter for 1300 < mtt < 1500 GeV - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "hadronic", "mtt"]
evgenConfig.contact  = ["sebastian.schaetzel@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"
evgenConfig.minevents=500

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/TTbarMassFilter.py")
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1300000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1500000.

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=0
