evgenConfig.description = "McAtNlo+fHerwig/Jimmy singtop Wt dilepton channel production with - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "singletop","WtChan","dileptonic"]
evgenConfig.contact  = ["tatsuya.masubuchi@cern.ch"]
evgenConfig.inputfilecheck = "SingleTopWtChanIncl"
evgenConfig.minevents=1000

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/MultiObjectsFilter.py")
topAlg.MultiObjectsFilter.PtCut = 5000.
topAlg.MultiObjectsFilter.EtaCut = 10.0 
topAlg.MultiObjectsFilter.UseEle = True
topAlg.MultiObjectsFilter.UseMuo = True
topAlg.MultiObjectsFilter.UseJet = False
topAlg.MultiObjectsFilter.UsePho = False 
topAlg.MultiObjectsFilter.UseSumPt = False
topAlg.MultiObjectsFilter.PtCutEach = [5000.,5000.]
