evgenConfig.description = "AcerMCPythia6 singletop-schan-e production with AUET2B CTEQ6L1 tune and LessPS param. settings"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "PS", "schan", "e"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop"

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_LessPS_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
