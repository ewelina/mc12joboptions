evgenConfig.description = "ALPGEN+Pythia Z(->ee)+3jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "e"]
evgenConfig.inputfilecheck = "ZeeNp3"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

