evgenConfig.description = "ALPGEN+Pythia Z(->tautau)+1jet process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z", "tau"]
evgenConfig.inputfilecheck = "ZtautauNp1"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

