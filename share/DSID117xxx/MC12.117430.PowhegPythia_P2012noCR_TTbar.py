evgenConfig.description = "POWHEG+Pythia6 ttbar 172.5 GeV production with a 1 MeV lepton filter, CT10 + CTEQ61L PDFs, Perugia2012noCR tune"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["gia.khoriauli@cern.ch", "alexander.grohsjean@desy.de"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/PowhegPythia_Perugia2012noCR_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/TTbarWToLeptonFilter.py")
