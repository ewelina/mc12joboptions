evgenConfig.description = "McAtNlo+fHerwig/Jimmy dileptonic ttbar production without spin correlation - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "dileptonic","spin"]
evgenConfig.contact  = ["Simon.Head@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"
evgenConfig.minevents = 1000

include("MC12JobOptions/McAtNloJimmy_AUET2_CT10_Common.py")

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

# dilepton configuration
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=2 # require exactly two leptons
