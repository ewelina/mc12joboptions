evgenConfig.description = "POWHEG+Pythia6 ttbar 172.5 GeV production with a 1 MeV lepton veto, TTbarWToLeptonFilter veto, CT10 + CTEQ61L PDFs, PythiaPerugia2011C tune"  
evgenConfig.keywords = ["top", "ttbar", "allhadronic"]
evgenConfig.contact  = ["gia.khoriauli@cern.ch", "liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/TTbarWToLeptonVeto.py")


