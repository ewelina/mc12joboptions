evgenConfig.description = "MadGraph+Pythia6 ttbar up to Np3incl. via HepMC ASCII reading"
evgenConfig.keywords = ["ttbar", "leptonic" ]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar_lept_7TeV'
include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "Z2"
