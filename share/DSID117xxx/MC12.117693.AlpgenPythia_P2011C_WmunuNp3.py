evgenConfig.description = "ALPGEN+Pythia W(->munu)+3jets process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W", "mu"]
evgenConfig.inputfilecheck = "WmunuNp3"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

