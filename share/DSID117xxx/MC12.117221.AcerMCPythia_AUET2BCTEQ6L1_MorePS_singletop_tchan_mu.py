evgenConfig.description = "AcerMCPythia6 singletop-tchan-mu production with AUET2B CTEQ6L1 tune and MorePS param. settings"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "PS", "tchan", "mu"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop"

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_MorePS_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
