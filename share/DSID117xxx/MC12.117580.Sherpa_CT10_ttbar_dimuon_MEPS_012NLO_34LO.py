include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa ttbar production with tt+0,1,2j@NLO+3,4j@LO"
evgenConfig.keywords = ["top", "ttbar", "2lepton"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 50
evgenConfig.inputconfcheck = "Sherpa_CT10_ttbar_dilepton_MEPS_012NLO_34LO" # universal for all decay channels

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{sqr(172.5)+0.5*(PPerp2(p[2])+PPerp2(p[3]))};
  EXCLUSIVE_CLUSTER_MODE 1;

  %tags for process setup
  NJET:=4; LJET:=2,3,4; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  CDXS_VSOPT=5;
  LOOPGEN:=OpenLoops;
  ERROR=0.05;

  %decay settings
  HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
  #HDH_ONLY_DECAY {24,12,-11}|{24,14,-13}|{24,16,-15}|{-24,-12,11}|{-24,-14,13}|{-24,-16,15}
  # di-muon only, to allow systematic studies with higher statistics
  HDH_ONLY_DECAY {24,14,-13}|{-24,-14,13}
  STABLE[24] 0; STABLE[6] 0; WIDTH[6] 0;
}(run)

(processes){
  Process : 93 93 ->  6 -6 93{NJET};
  NLO_QCD_Mode 3 {LJET}; CKKW sqr(QCUT/E_CMS);
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN;
  Max_Epsilon 0.1 {4,5,6,7,8};
  Order_EW 0;
  End process
}(processes)
"""
