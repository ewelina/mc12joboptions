evgenConfig.description = "MEtop+Pythia strong FCNC LO direct single-top production with the AUET2B_CTEQ6L1 tune"
evgenConfig.keywords = ["singletop","FCNC","leptonic"]
evgenConfig.contact  = ["conrad.friedrich@cern.ch"]
evgenConfig.inputfilecheck = "MEtop10.117011.st_strongFCNC_ugt"

include ( "MC12JobOptions/MEtopPythia_AUET2B_CTEQ6L1_Common.py" )

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

