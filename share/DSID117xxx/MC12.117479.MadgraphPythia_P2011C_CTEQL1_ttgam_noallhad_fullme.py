evgenConfig.description = "ttbar+gamma in the ljets+ll modes using MadGraph+PythiaPerugia2011C full ME calculations, showered by Pythia with scale=172.5GeV and pt(gamma) = 10 GeV"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["EW","ttbar","photon","ljets","leptonic"]
evgenConfig.contact  = ["loginov@fnal.gov", "liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = 'ttgamma'

include ( "MC12JobOptions/Pythia_Perugia2011C_Common.py" )

## ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )

## ... Photos -- don't use it as we have photons generated by MadGraph
#include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                 "pyinit pylisti -1","pyinit pylistf 1","pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000",  # Turn off FSR (have photons generated in MadGraph)
                                 ]
