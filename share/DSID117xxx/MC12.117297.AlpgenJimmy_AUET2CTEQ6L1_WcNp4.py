evgenConfig.description = "ALPGEN+Jimmy W(->lnu)+c+4jets process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.keywords = ["EW", "W"]
evgenConfig.inputfilecheck = "WcNp4"
evgenConfig.minevents = 5000

include( 'MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py' )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

