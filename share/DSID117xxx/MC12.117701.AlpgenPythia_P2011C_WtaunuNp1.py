evgenConfig.description = "ALPGEN+Pythia W(->taunu)+1jet process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "W", "tau"]
evgenConfig.inputfilecheck = "WtaunuNp1"

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

