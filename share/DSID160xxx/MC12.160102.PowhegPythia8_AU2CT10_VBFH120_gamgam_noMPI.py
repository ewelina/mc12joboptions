evgenConfig.description = "POWHEG+PYTHIA8 VBFH H->gg with AU2,CT10 no MPI"
evgenConfig.keywords = ["SMhiggs", "VBF", "gamma"]
evgenConfig.inputfilecheck = "VBFH_SM_M120"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            'PartonLevel:MPI = off',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22'
                            ]
