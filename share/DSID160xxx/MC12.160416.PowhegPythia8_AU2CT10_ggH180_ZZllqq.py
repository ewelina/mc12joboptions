evgenConfig.description = "POWHEG+PYTHIA8 ggH H->ZZ->llqq with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "Z","leptonic","hadronic"]
evgenConfig.inputfilecheck = "ggH_SM_M180"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfAny = 1 2 3 4 5 6 11 13 15'
                            ]

include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 23
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13,15]
topAlg.XtoVVDecayFilter.PDGChild2 = [1,2,3,4,5,6]

