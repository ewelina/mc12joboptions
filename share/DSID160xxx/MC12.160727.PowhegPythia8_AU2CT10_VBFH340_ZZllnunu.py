evgenConfig.description = "POWHEG+PYTHIA8 VBFH H->ZZ->llnunu with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "Z","nu","leptonic"]
evgenConfig.inputfilecheck = "VBFH_SM_M340"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfAny = 11 12 13 14 15 16'
                            ]

include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 23
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13,15]
topAlg.XtoVVDecayFilter.PDGChild2 = [12,14,16]

