evgenConfig.description = "Herwig++ gamma+jet with the CTEQ6L1 EE3 tune"
evgenConfig.generators = ["Herwigpp"]
evgenConfig.keywords = ["gamma", "jets"]

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
topAlg.Herwigpp.Commands += ["insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEGammaJet"]
