MODEL = 'generic'
CASE = 'gluino'
MASS = 600
MASSX = 500
DECAY = 'false'
LIFETIME = '1.0'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 600GeV g/qq 500GeV 1.0ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
