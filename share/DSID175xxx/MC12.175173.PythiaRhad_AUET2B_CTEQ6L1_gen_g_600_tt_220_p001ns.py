MODEL = 'generic'
CASE = 'gluino'
MASS = 600
MASSX = 220
DECAY = 'false'
LIFETIME = '0.001'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 600GeV tt 220GeV 0.001ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;NOGLUINOGLUONDECAY=True;NOGLUINOLIGHTSQUARKDECAY=True;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
