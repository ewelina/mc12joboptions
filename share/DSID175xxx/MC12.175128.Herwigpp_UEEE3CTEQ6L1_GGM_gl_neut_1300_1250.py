# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_gl_neut_1300_1250.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['gluino'], slha_file, 'TwoParticleInclusive')

# define metadata
evgenConfig.description = 'GGM photon + b + MET gluino-neutralino grid generation with mgl=1300 mneut=1250'
evgenConfig.keywords = ['SUSY', 'GGM', 'gluino', 'bino', 'higgsino']
evgenConfig.contact = [ 'andrew.kuhl@cern.ch']

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Clean up
del cmds
