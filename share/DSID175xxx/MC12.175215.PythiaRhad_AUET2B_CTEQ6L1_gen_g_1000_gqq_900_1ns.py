MODEL = 'generic'
CASE = 'gluino'
MASS = 1000
MASSX = 900
DECAY = 'false'
LIFETIME = '1.0'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 1000GeV g/qq 900GeV 1.0ns"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};LIFETIME={lifetime};MASSX={massx};CHARGE=999;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, lifetime = LIFETIME, massx=MASSX)
