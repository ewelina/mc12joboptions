MODEL = 'generic'
CASE = 'gluino'
MASS = 400
MASSX = 300
DECAY = 'false'
STOPPED = 'true'

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " generic gluino 400GeV g/qq 300GeV stopped"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};DECAY={decay};MASSX={massx};CHARGE=999;preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS, decay = DECAY, massx=MASSX)
