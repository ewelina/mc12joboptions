
evgenConfig.description = "gammagamma -> tautau production with Pythia8+MRST2004QED single dissociative setup, 20<M<60GeV, pt>8 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "taus"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOtautau_7TeV_20M60'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOtautau_8TeV_20M60'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
