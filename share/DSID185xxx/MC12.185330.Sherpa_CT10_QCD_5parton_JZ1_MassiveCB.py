include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "QCD: up to 5 jets in ME (no electro-weak processes included). C and B quarks are treated as massive and all processes are included"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","christopher.young@cern.ch" ]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  ACTIVE[25]=0

  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)

(processes){

Process 93 93 -> 93 93 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 93 -> 5 -5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 93 -> 4 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 93 -> 5 5 -5 -5 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 93 -> 5 -5 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 93 -> 4 4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 5 -> 93 5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 5 -> 5 5 -5 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 5 -> 5 4 -4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 5 -> 5 5 5 -5 -5  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 5 -> 5 5 -5 4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 5 -> 5 4 4 -4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -5 -> 93 -5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 -5 -> 5 -5 -5 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -5 -> -5 4 -4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -5 -> 5 5 -5 -5 -5  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -5 -> 5 -5 -5 4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -5 -> -5 4 4 -4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 4 -> 93 4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 4 -> 5 -5 4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 4 -> 4 4 -4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 4 -> 5 5 -5 -5 4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 4 -> 5 -5 4 4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 4 -> 4 4 4 -4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -4 -> 93 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 93 -4 -> 5 -5 -4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -4 -> 4 -4 -4 93{2}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -4 -> 5 5 -5 -5 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -4 -> 5 -5 4 -4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 93 -4 -> 4 4 -4 -4 -4  
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 5 -> 5 5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.08 {4};
Integration_Error 0.10 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 5 -> 5 5 5 -5 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 5 -> 5 5 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 -5 -> 93 93 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 -5 -> 5 -5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 -5 -> 4 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 -5 -> 5 5 -5 -5 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 -5 -> 5 -5 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 -5 -> 4 4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 4 -> 5 4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 4 -> 5 5 -5 4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 4 -> 5 4 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 -4 -> 5 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.08 {4};
Integration_Error 0.08 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 5 -4 -> 5 5 -5 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 5 -4 -> 5 4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 -5 -> -5 -5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.08 {4};
Integration_Error 0.10 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process -5 -5 -> 5 -5 -5 -5 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 -5 -> -5 -5 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 4 -> -5 4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process -5 4 -> 5 -5 -5 4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 4 -> -5 4 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 -4 -> -5 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process -5 -4 -> 5 -5 -5 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -5 -4 -> -5 4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 4 4 -> 4 4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 4 4 -> 5 -5 4 4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 4 4 -> 4 4 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 4 -4 -> 93 93 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 4 -4 -> 5 -5 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 4 -4 -> 4 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process 4 -4 -> 5 5 -5 -5 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 4 -4 -> 5 -5 4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process 4 -4 -> 4 4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -4 -4 -> -4 -4 93{3}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
End process;

Process -4 -4 -> 5 -5 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

Process -4 -4 -> 4 -4 -4 -4 93{1}
Order_EW 0;
Enhance_Factor 2.0 {3}
Enhance_Factor 5.0 {4,5}
CKKW sqr(30/E_CMS)
Integration_Error 0.02 {2,3};
Integration_Error 0.04 {4};
Integration_Error 0.06 {5};
Selector_File *|(coresel){|}(coresel) {2};
Cut_Core 1
End process;

}(processes)

(coresel){
  NJetFinder  2  10.0  0.0  0.4  -1  999.0  10.0
  NJetFinder  1  10.0  0.0  0.4  -1  999.0  10.0
}(coresel)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]

include("MC12JobOptions/JetFilter_JZ1.py")
