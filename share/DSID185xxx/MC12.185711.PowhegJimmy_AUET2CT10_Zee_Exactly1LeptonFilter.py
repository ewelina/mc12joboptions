evgenConfig.description    = "POWHEG+fHerwig/Jimmy Z->ee production with exactly one central lepton pt>15 GeV filter and AUET2 CT10 tune"
evgenConfig.keywords       = [ "electroweak", "Z", "leptons", "electron"]
evgenConfig.contact        = [ "oleg.fedin@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*.Zee*"
	
## Herwig/Jimmy Showering
include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Photos.py")


## Exactly1LeptonFilter 
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("DiLeptonVeto")
StreamEVGEN.VetoAlgs += ["DiLeptonVeto"]
topAlg.DiLeptonVeto.Ptcut = 15000.
topAlg.DiLeptonVeto.Etacut = 2.7
topAlg.DiLeptonVeto.NLeptons = 2


evgenConfig.minevents = 2000
