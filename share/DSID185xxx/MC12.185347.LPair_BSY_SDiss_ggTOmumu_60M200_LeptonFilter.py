
evgenConfig.description = "gammagamma -> mumu production with LPAIR SingleDiss, 60<M<200GeV, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "muons", "60<M<200GeV", "lepton filter"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'SDiss_ggTOmumu_7TeV_60M200'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'SDiss_ggTOmumu_8TeV_60M200'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
