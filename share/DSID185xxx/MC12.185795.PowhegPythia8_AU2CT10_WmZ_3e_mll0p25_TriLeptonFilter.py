include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="WmZ_enuee"

evgenConfig.description = "POWHEG+Pythia8 WZ production and AU2 CT10 tune, cut on all SFOS pair mll>2*m(l from Z)+250MeV, trilepton filter pt>7GeV, eta<2.7"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic" ]
evgenConfig.contact = ["Alex Long <b.long@cern.ch>"]

def powheg_override():
    # update mll cut
    PowhegConfig.mllmin = 0.250

# compensate filter efficiency
evt_multiplier = 20

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 3
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.Ptcut = 7.*GeV
