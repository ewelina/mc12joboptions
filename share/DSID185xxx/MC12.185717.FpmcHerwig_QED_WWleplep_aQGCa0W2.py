evgenConfig.description = "aQGC a0W=2e-4 500GeVcutoff in exclusive gamgam->WW->lnulnu with no hadronic tau"          
evgenConfig.keywords = ["QED", "diphoton", "WW", "dileptonic" ]                                                                                   

evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]                                                                              
evgenConfig.contact += ["Chav Chhiv Chau <chav.chhiv.chau@cern.ch>"]
evgenConfig.generators += ['FPMC'] 

evgenConfig.inputfilecheck = 'FpmcHerwig'
include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
