## POWHEG+Pythia8 Wplus->munu with lepton filter and EvtGen Forced D*-->D0pi, D0-->Kpi and Charm filter

evgenConfig.description = "POWHEG+Pythia8 Wplus->munu withlepton filter, AU2 CT10 tune and EvtGen"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "charm"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusmunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_EvtGen.py")

include("MC12JobOptions/CharmFilter.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 20000.

