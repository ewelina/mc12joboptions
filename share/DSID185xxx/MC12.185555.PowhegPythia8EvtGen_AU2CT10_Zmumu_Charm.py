evgenConfig.description = "POWHEG+Pythia8 Z->mumu production with AU2 CT10 tune and forced EvtGen decay"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu","EvtGen"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"
evgenConfig.minevents = 200

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/Pythia8_EvtGen.py")

include("MC12JobOptions/CharmFilter.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 20000.0

