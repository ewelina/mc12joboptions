evgenConfig.description = "Dijet, W emission with truth jet 500 GeV filter (AU2CT10)"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 250.",
     "TimeShower:weakShower = on",
     #"SpaceShower:weakShower = on"
     ]

#include("MC12JobOptions/JetFilter_JZ4W.py")
include("MC12JobOptions/JetFilter_JZ4.py")
#topAlg.QCDTruthJetFilter.MinPt = 500.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 8000.*GeV


from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
if "ParticleFilter" not in topAlg:
  topAlg += ParticleFilter()
if "ParticleFilter" not in StreamEVGEN.RequireAlgs:
  StreamEVGEN.RequireAlgs += ["ParticleFilter"]
topAlg.ParticleFilter.Ptcut = 0.
topAlg.ParticleFilter.Etacut = 999.
topAlg.ParticleFilter.PDG = 24
topAlg.ParticleFilter.StatusReq = -1

evgenConfig.minevents = 50
