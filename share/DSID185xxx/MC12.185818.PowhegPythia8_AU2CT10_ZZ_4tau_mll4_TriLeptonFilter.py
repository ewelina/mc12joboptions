include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune='AU2_CT10'
process="ZZ_tautautautau"

evgenConfig.description = "POWHEG+Pythia8 ZZ -> 4tau production mll>4GeV with trilepton filter pt>7GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["EW"  ,"diboson", "leptonic"]
evgenConfig.contact = ["Alex Long <b.long@cern.ch>"]
evgenConfig.minevents = 1000

# compensate filter efficiency
evt_multiplier = 30

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 3
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.Ptcut = 7.*GeV
