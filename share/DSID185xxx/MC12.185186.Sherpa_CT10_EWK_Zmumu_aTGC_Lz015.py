include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "VBF-Z production with aTGC"
evgenConfig.keywords = [ "VBF", "EW"]
evgenConfig.contact  = [ "kjoshi@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1  
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  End process;
}(processes)

(selector){
  Mass 13 -13 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

(me){ 
  ME_SIGNAL_GENERATOR = Amegic
}(me)

(model){ 
  ACTIVE[6]=0 
  MODEL=SM+AGC

  G1_Z=1.0
  KAPPA_Z=1.0
  LAMBDA_Z=0.15

  G1_GAMMA=1.0 
  KAPPA_GAMMA=1.0
  LAMBDA_GAMMA=0.0
 
  UNITARIZATION_SCALE=100000 
  UNITARIZATION_N=2
}(model)

"""

topAlg.Sherpa_i.ResetWeight = 0
evgenConfig.inputconfcheck = 'Sherpa_CT10_EWK_Zmumu_aTGC_Lz015'

