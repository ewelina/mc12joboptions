## POWHEG+Pythia8 DYmumu_12M20

evgenConfig.description = "POWHEG+Pythia8 Z/gamma*->mumu with mass cut 12<M<20GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_12M20"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
