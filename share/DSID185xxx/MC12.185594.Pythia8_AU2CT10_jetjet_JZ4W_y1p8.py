evgenConfig.description = "Dijet truth jet slice JZ4W, with the AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 250."]

include("MC12JobOptions/JetFilter_JZ4W.py")

## Add a min eta cut to the truth jet filter
assert hasattr(topAlg, "QCDTruthJetFilter")
topAlg.QCDTruthJetFilter.MinEta = 1.8
topAlg.QCDTruthJetFilter.SymEta = True

evgenConfig.minevents = 5



