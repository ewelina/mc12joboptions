include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "mu mu gamma production with up to three jets in ME+PS and pT_gamma>10 GeV with VBF filter"
evgenConfig.keywords = [ "VBF","2muon","gamma","jet"]
evgenConfig.contact  = [ "zhijun.liang@cern.ch" ]
evgenConfig.minevents = 100
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  13 -13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 7000
  PT 22  10 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass


from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO
topAlg.VBFMjjIntervalFilter.NoJetProbability=0.0001
topAlg.VBFMjjIntervalFilter.OneJetProbability=0.0005
topAlg.VBFMjjIntervalFilter.LowMjjProbability=0.01
topAlg.VBFMjjIntervalFilter.LowMjj=150.0*1000
topAlg.VBFMjjIntervalFilter.HighMjj=400.0*1000
topAlg.VBFMjjIntervalFilter.RemovePhotonJetOverlap=1


StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]
