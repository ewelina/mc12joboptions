evgenConfig.description = "VBFNLO+Pythia8 production W+W+jj -> lvlvjj (l=e,mu,tau) with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["VBS", "electroweak", "WW","2jet"]
evgenConfig.inputfilecheck = "vbfnlo.*WpWpjj_lvlvjj_VBS"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Lulu Liu<Lulu.Liu@cern.ch>"]
evgenConfig.generators += ["VBFNLO","Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["ParticleDecays:sophisticatedTau = 1"]
