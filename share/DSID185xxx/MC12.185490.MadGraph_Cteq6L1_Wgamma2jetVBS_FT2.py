include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            '24:mMin = 2.0',
                            '24:onMode = off',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16',]
## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "Madgraph W gamma +2jet vector boson scattering (LO EWK  contribution only) with FT2=1.0e-10 TeV^-4"
evgenConfig.keywords = ["WAdijetFT2"]
evgenConfig.inputfilecheck = 'WAdijet'
