evgenConfig.description = "Inclusive minimum bias, with Monash tune"
evgenConfig.keywords = ["QCD", "minBias"]

include("MC12JobOptions/Pythia8_Monash_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:all = on"]


