############################################
###  MC12.185846.PowhegHerwig_AU2CT10_ZZllnunu_mm_mll50.py
###  Description: Job Options for full-sim sample for parton shower systematic
###  of PowHegBox + (f)Herwig+Jimmy
###  based on: atlasoff/Generators/MC12JobOptions/trunk/share/DSID126xxx/MC12.126950.PowhegPythia8_AU2CT10_ZZllnunu_mm_mll4.py
###            but lower mass bound during generation is moved to 50 GeV (mll4 changed to mll50 in name)
###            Replace the call to Pythia/Photos to (Herwig+Jimmy)/Photos for showering systematic
###  created: 29 Aug 2014
###  Steven Kaneti <steven.kaneti@cern.ch>
############################################


evgenConfig.description    = "POWHEG+Herwig/Jimmy ZZllnunu_mm production mll>50GeV without filter using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords       = ["electroweak"  ,"ZZ", "lepton", "neutrino" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "ZZllnunu_mm_mll50.*CT10"
evgenConfig.minevents      = 5000
evgenConfig.contact        = ["Steven Kaneti <steven.kaneti@cern.ch>"]


include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Photos.py")
