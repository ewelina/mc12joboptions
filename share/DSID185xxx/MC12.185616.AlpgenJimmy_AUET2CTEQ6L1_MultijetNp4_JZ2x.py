include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np4_JZ2x'
evgenConfig.description = 'pp -> 4 light jets, for leading jet pT range = 80-14000 GeV'

# Modify the normal leading pT cut of the JZ2 filter
topAlg.QCDTruthJetFilter.MaxPt = 14000.*GeV
