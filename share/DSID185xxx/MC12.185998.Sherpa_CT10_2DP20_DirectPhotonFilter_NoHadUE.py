include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Diphoton + jets production using ME+PS with up to two jets from the matrix element. Apply filter requiring two prompt photons."
evgenConfig.keywords = ["jets", "photon", "diphoton"]
evgenConfig.contact  = ["lroos@in2p3.fr", "frank.siegert@cern.ch"]

evgenConfig.process = """
(run){
  SCALES VAR{Abs2(p[2]+p[3])/4.0}
  ME_QED = Off
  QCUT:=7.0
}(run)

(processes){
  Process 21 21 -> 22 22
  Loop_Generator gg_yy
  End process;

  Process 93 93 -> 22 22 93{2}
  Order_EW 2
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/(Abs2(p[2]+p[3])/4.0));
  Integration_Error 0.1;
  End process
}(processes)

(selector){
  PT      22      14.0 E_CMS
  DeltaR  22  93  0.3  100.0
}(selector)
"""

topAlg.Sherpa_i.Parameters += [
           "MI_HANDLER=None",   # (multiparton interactions)
           "FRAGMENTATION=Off", # (hadronisation)
           ]

## Filter
from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
if not hasattr(topAlg, "DirectPhotonFilter"):
    topAlg += DirectPhotonFilter()

## topAlg.DirectPhotonFilter.Ptcut = 10000.		// old-fashion property
##                                                      // now use Ptmin and Ptmax instead

#topAlg.DirectPhotonFilter.Ptmin = 10000.		
#topAlg.DirectPhotonFilter.Ptmax = 100000000.		
#topAlg.DirectPhotonFilter.Etacut =  2.7
#topAlg.DirectPhotonFilter.NPhotons = 1
if "DirectPhotonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["DirectPhotonFilter"]

topAlg.DirectPhotonFilter.Ptcut = 20000.
topAlg.DirectPhotonFilter.Etacut =  2.7
topAlg.DirectPhotonFilter.NPhotons = 2
