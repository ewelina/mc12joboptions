include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np2_JZ2x'
evgenConfig.description = 'pp -> 2 light jets, four-jet filter with p_{T}^{1} - 80-14000 GeV and p_{T}^{2-4} > 15 GeV and Double Parton Scatter filter'

# Modify the normal leading pT cut of the JZ2 filter
topAlg.QCDTruthJetFilter.MaxPt = 14000.*GeV

# Include the four jet filter and set the pT cuts here
include("MC12JobOptions/FourJetFilter_JZ2.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 80.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 14000.*GeV
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 15.*GeV

include('MC12JobOptions/MultiParticleFilter_DPS_Jimmy.py')
