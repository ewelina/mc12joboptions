include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "ttbar gamma production (e nu jj channel)"
evgenConfig.contact  = [ "zhijun.liang@cern.ch"]
evgenConfig.keywords = [ "EW", "ttbar", "gamma" ]
#  11 -12 -11 12 5 -5 22 93{1}
evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.20
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  6[a] -6[b] 22
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> 93 93
  Decay -24[d] ->11 -12
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;

  Process 93 93 ->  6[a] -6[b] 22
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> -11 12
  Decay -24[d] ->93 93
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;


  Process 93 93 ->  6[a] -6[b] 
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> 93 93 22
  Decay -24[d] ->11 -12
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;


  Process 93 93 ->  6[a] -6[b] 
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> 93 93 
  Decay -24[d] ->11 -12 22
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;



  Process 93 93 ->  6[a] -6[b] 
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> -11 12 22
  Decay -24[d] ->93 93
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;


  Process 93 93 ->  6[a] -6[b] 
  Decay 6[a] -> 5 24[c]
  Decay -6[b] -> -5 -24[d]
  Decay 24[c] -> -11 12 
  Decay -24[d] ->93 93 22
  CKKW sqr(20/E_CMS)
  Max_Order_EW 5
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.1
  Print_Graphs;
  End process;


  
}(processes)
(selector){
  PT 22 10 E_CMS
  PT 90 8 E_CMS
  DeltaR 22 22 0.1 1000
  DeltaR 22 93 0.1 1000
  DeltaR 22 90 0.1 1000
  DeltaR 93 90 0.1 1000
}(selector)
"""
