include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WWgamma -> mu+nu tau- nu gamma  production with up to 1 jets in ME+PS"
evgenConfig.contact  = [ "zhijun.liang@cern.ch"]
evgenConfig.keywords = [ "EW", "Diboson", "gamma", "muon" ]


evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.20
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  13 -14 -15 16 22 93{1}
  CKKW sqr(20/E_CMS)
  Max_Order_EW 6
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.07
  Print_Graphs
  End process;
}(processes)
(selector){
  Mass 13 -13 15 E_CMS
  PT 22 10 E_CMS
  PT 90 8 E_CMS
  DeltaR 22 22 0.1 1000
  DeltaR 22 93 0.1 1000
  DeltaR 22 90 0.1 1000
  DeltaR 93 90 0.1 1000
}(selector)
"""
