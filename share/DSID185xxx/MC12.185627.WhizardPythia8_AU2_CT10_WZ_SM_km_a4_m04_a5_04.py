############################################################################################################
# Whizard lhef showered with Pythia8
# with anomalous quartic gauge couplings
# Anja Vest <anja.vest@cern.ch>
# Generators/MC12JobOptions/tags/MC12JobOptions-00-05-70/share/MC12.xxxxxx.LHEFPythia8_AU2CTEQ6L1_example.py
############################################################################################################ 
evgenConfig.description = "Whizard+Pythia8 production JO with aQGC and the AU2 CT10 tune"
evgenConfig.keywords = ["EW"]
evgenConfig.inputfilecheck = "whizard.185627"
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.minevents = 5000
evgenConfig.contact = ["Anja Vest <anja.vest@physik.tu-dresden.de>"]

evgenConfig.generators = ["Whizard", "Pythia8" ]
