evgenConfig.description = "DPI production of WZ->lvll and two jets. Dilepton filter of pT>10 GeV. Dijet filter of pT>20 GeV, select events with pT cut on the MPI-Emission of 25 GeV."
evgenConfig.keywords = ["electroweak", "WZ", "leptons", "l", "TwoJets", "DPI"]
evgenConfig.contact = ["Pieter van der Deijl <Pieter.van.der.deijl@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakDoubleBoson:ffbar2ZW = on", # WZ
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 11", # switch on W->e,nu decays
                            "24:onIfAny = 13", # switch on W->u,nu decays
                            "24:onIfAny = 15", # switch on W->t,nu decays
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 11", # switch on Z->e,e decays
                            "23:onIfAny = 13", # switch on Z->u,u decays
                            "23:onIfAny = 15"] # switch on Z->t,t decays
topAlg.Pythia8.UserHook = "EnhanceMPI"

topAlg.Pythia8.UserParams += ["EnhanceMPI:PTCut=25"]

# 2-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10.*GeV
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

# 2-jet filter
include("MC12JobOptions/VBFForwardJetsFilter.py")
topAlg.VBFForwardJetsFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.VBFForwardJetsFilter.NJets = 2
topAlg.VBFForwardJetsFilter.JetMinPt = 20.*GeV
topAlg.VBFForwardJetsFilter.JetMaxEta = 10.
topAlg.VBFForwardJetsFilter.Jet1MinPt = 20.*GeV
topAlg.VBFForwardJetsFilter.Jet1MaxEta = 10.
topAlg.VBFForwardJetsFilter.Jet2MinPt = 20.*GeV
topAlg.VBFForwardJetsFilter.Jet2MaxEta = 10.
topAlg.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2 = 0
topAlg.VBFForwardJetsFilter.MassJJ = 0.
topAlg.VBFForwardJetsFilter.DeltaEtaJJ = 0
topAlg.VBFForwardJetsFilter.UseLeadingJJ = 0
