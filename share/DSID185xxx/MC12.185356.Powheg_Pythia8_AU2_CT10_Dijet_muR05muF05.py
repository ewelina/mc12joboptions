###############################################################
#
# Job options file for POWHEG with Pythia8
# J. Robinson, 14.04.2013 <james.robinson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = 'POWHEG+Pythia8 dijet production with bornktmin = 5 GeV, bornsuppfact = 250 GeV, muR=muF=0.5 and AU2 CT10 tune: alpha_s matched for ISR only.'
evgenConfig.keywords       = [ 'QCD', 'dijet', 'jets' ]
evgenConfig.contact        = [ 'james.robinson@cern.ch' ]
evgenConfig.inputfilecheck = 'Powheg_Dijet_r2169_muR05muF05'
evgenConfig.weighting      = 0      # to avoid failure with high weights - TODO: remove this if fix is implemented
evgenConfig.minevents      = 48000  # allow some of the 50k events to fail TestHepMC 

## Pythia8 Showering
include('MC12JobOptions/Pythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_Powheg.py')

## New, main31-style shower
topAlg.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2'
                           , 'TimeShower:pTmaxMatch = 2'
                           , 'SpaceShower:alphaSvalue = 0.118'
                           ]
topAlg.Pythia8.UserHook = 'Main31'
