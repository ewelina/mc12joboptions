evgenConfig.description = "VBFNLO+Pythia8 production W-W+W- with the AU2 CTEQ6L1 tune aQGC"
evgenConfig.keywords = ["triboson", "EW", "WWW"]
evgenConfig.inputfilecheck = "vbfnlo.185406"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Yanwen Liu<Yanwen.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")
