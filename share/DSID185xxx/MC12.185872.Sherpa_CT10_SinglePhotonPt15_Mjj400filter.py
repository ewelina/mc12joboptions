include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Gamma + up to 4 jets with ME+PS. Slice in photon pT>15.0 GeV."
evgenConfig.keywords = ["photon","jets" ]
evgenConfig.contact  = [ "zhijun.liang@cern.ch" ]
evgenConfig.minevents = 10

evgenConfig.process="""
(run){
  QCUT:=30.0
}(run)

(processes){
  Process 93 93 -> 22 93 93{4}
  Order_EW 1
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
  Integration_Error 0.1 {5,6};
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process
}(processes)

(selector){
  PT  22  15.0  E_CMS
  DeltaR  22  93  0.3  20.0
}(selector)
"""
try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass


from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO
topAlg.VBFMjjIntervalFilter.NoJetProbability=0.00003
topAlg.VBFMjjIntervalFilter.OneJetProbability=0.0001
topAlg.VBFMjjIntervalFilter.LowMjjProbability=0.002
topAlg.VBFMjjIntervalFilter.LowMjj=150.0*1000
topAlg.VBFMjjIntervalFilter.HighMjj=400.0*1000
topAlg.VBFMjjIntervalFilter.RemovePhotonJetOverlap=1



StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]
