evgenConfig.description    = "POWHEG+fHerwig/Jimmy Z->mumu production with  AUET2 CT10 tune"
evgenConfig.keywords       = [ "electroweak", "Z", "leptons", "muon"]
evgenConfig.contact        = [ "oleg.fedin@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*.Zmumu*"
	
## Herwig/Jimmy Showering
include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Photos.py")


