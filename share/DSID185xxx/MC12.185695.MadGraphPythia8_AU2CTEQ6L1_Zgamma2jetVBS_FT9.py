include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfMatch = 11',
                            '23:onIfMatch = 13',
                            '23:onIfMatch = 15',]
## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "Madgraph Z gamma +2jet vector boson scattering (LO EWK  contribution only) with FT9=20 TeV^-4"
evgenConfig.keywords = ["Z","dijet","AQGC"]
evgenConfig.inputfilecheck = 'ZAdijet_LT8_LT9_FT9'

evgenConfig.minevents = 4000
