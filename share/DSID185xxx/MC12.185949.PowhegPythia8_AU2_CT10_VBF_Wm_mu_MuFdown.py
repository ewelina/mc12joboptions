###############################################################
#
# Job options file for POWHEG with Pythia8
# C. Johnson, 15.08.2014 <christian.johnson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+Pythia8 VBF_W mu nu production, muR*1 muF*0.5 and AU2 CT10 tune at 7TeV"
evgenConfig.keywords       = [ "electroweak","W","2jet","VBF","tChannel","muon","muFdown" ]
evgenConfig.contact        = [ "christian.johnson@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Pythia8" ]
evgenConfig.inputfilecheck = "Wminus_munu"
evgenConfig.minevents      = 5000

## Pythia8 Showering
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")