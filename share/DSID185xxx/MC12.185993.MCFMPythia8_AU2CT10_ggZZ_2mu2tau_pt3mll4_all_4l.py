evgenConfig.description = "MCFM+Pythia8 gg->(H)->ZZ->2mu2tau ptl>3GeV & mll>4GeV with CT10 pdf and AU2 CT10 tune, tau->e/mu+neutrinos"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.inputfilecheck = "mcfm68.185993"
evgenConfig.contact = ["Tiesheng Dai <Tiesheng.Dai@cern.ch>"]
evgenConfig.generators += ["MCFM"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '23:onMode = off',#decay of Z
                            '15:onMode = off',#decay of tau
                            '15:onIfAny = 11 12 13 14' #tau->e/mu+neutrinos
                           ]
