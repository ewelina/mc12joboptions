
evgenConfig.description = "gammagamma -> tautau production with Pythia8+MRST2004QED single dissociative setup, M>200GeV, pt>8 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "taus"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOtautau_7TeV_200M'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOtautau_8TeV_200M'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
