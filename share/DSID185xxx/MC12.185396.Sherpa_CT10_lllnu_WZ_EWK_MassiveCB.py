include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WZjj->lllnjj Order_EW=6"
evgenConfig.keywords = ["VBS", "Vector-Boson Scattering", "Diboson"]
evgenConfig.contact  = ["philipp.anger@cern.ch"]
evgenConfig.minevents = 100

evgenConfig.process = """
(run){
  MASSIVE[4] = 1;
  MASSIVE[5] = 1;
  ACTIVE[25] = 1;
  MASS[25]   = 126.0;
  WIDTH[25]  = 0.00418;
  ERROR=0.05;
}(run)

(model){
  EW_SCHEME 3;
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 4000;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 4000;
}(beam)

(processes){
  Process 4 93 -> 90 90 90 91 93 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 -5 -> -5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 93 -> 4 4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 5 93 -> -4 5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 93 93 -> 4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 93 -> -5 4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 93 -> -5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 93 -> -4 -5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 93 93 -> 90 90 90 91 93 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 93 -> -5 5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 4 -> -5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 93 -> -4 4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 5 -> 4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 5 -> -4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 93 -> -4 -4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 5 93 -> 5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 93 93 -> -4 4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 5 -> 5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 4 -> 4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 93 -> -4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 93 -> -5 5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 4 -> -4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 5 93 -> 4 5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 5 -> 5 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 4 -> 4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -5 5 -> 90 90 90 91 93 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 -4 -> -4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 93 93 -> -5 5 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 93 -> 90 90 90 91 93 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 93 -> 4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 93 93 -> -4 90 90 90 91 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process -4 4 -> 90 90 90 91 93 93;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
  Process 4 93 -> -4 4 90 90 90 91;
  Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  End process;
 
}(processes)

(selector){
  Mass 11 -11 0.1 E_CMS;
  Mass 13 -13 0.1 E_CMS;
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP];
  NJetFinder 2 15. 0. 0.4 -1 100 10;
}(selector)
"""
