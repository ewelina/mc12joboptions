############################################
###  MC12.185842.PowhegHerwig_AU2CT10_ZZ_4e_mll50_2pt5.py
###  Description: Job Options for full-sim sample for parton shower systematic
###  of PowHegBox + (f)Herwig+Jimmy
###  based on: atlasoff/Generators/MC12JobOptions/trunk/share/DSID126xxx/MC12.126937.PowhegPythia8_AU2CT10_ZZ_4e_mll4_2pt5.py
###  replace the call to Pythia/Photos to (Herwig+Jimmy)/Photos
###  created: 29 Aug 2014
###  Steven Kaneti <steven.kaneti@cern.ch>
############################################


evgenConfig.description    = "POWHEG+Herwig/Jimmy ZZ_4e production mll>50GeV with dimuon filter pt>5GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords       = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "ZZ_4e_mll50_2pt5.*CT10"

evgenConfig.minevents      = 5000
evgenConfig.contact        = ["Steven Kaneti <steven.kaneti@cern.ch>"]


include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.Ptcut = 5.
