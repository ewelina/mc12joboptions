evgenConfig.description = "VBFNLO+Pythia8 production W-W-W+ -> lvlvqq (l=e,mu,tau) with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "EW", "WWW", "1jet","inclusive"]
evgenConfig.inputfilecheck = "vbfnlo.*WmWmWp_lvlvqq_1jInclusive"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Lulu Liu<Lulu.Liu@cern.ch>"]
evgenConfig.generators += ["VBFNLO"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["ParticleDecays:sophisticatedTau = 1"]

topAlg.Pythia8.Commands += ["Merging:doCutBasedMerging = on",
                            "Merging:QijMS = 0",
                            "Merging:pTiMS = 10",
                            "Merging:dRijMS = 0",
                            "Merging:nJetMax = 1",
                            "Merging:Process = pp>(w+>e+ve)(w->jj)(w+>mu+vm)"]

