## POWHEG+Pythia8 Wplus->munu with lepton filter and EvtGen Forced D*-->D0pi, D0-->K3pi and D* filter

evgenConfig.description = "POWHEG+Pythia8 Wplus->munu withlepton filter, AU2 CT10 tune and EvtGen"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu","charm"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusmunu"
evgenConfig.auxfiles += ['DstarP2D0PiP_D02K3piPlusAnti.DEC']
evgenConfig.minevents = 200

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/Pythia8_EvtGen.py")
topAlg.EvtInclusiveDecay.userDecayFile = "DstarP2D0PiP_D02K3piPlusAnti.DEC"

include("MC12JobOptions/DstarFilter.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 20000.

