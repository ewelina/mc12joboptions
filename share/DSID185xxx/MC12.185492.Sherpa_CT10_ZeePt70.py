include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z -> ee + up to 5 jets with pT_Z>70 GeV."
evgenConfig.keywords = ["Z","e"]
evgenConfig.inputconfcheck = "Zee"
evgenConfig.contact  = [ "james.henderson@cern.ch" ]

evgenConfig.process="""
(run){
  MASSIVE[15]=1
}(run)

(processes){
  Process 93 93 -> 11 -11 93 93{4}
  Order_EW 2
  CKKW sqr(30/E_CMS)
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  !Enhance_Factor 4.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 11 -11 40.0 E_CMS
  PT2  11 -11 70.0 E_CMS
}(selector)
"""
