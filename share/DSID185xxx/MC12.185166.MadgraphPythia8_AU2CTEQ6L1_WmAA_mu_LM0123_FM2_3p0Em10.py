evgenConfig.description = "MadGraph+Pythia8 production for AQGC Wm + 2 photons with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "EW", "gamma", "leptonic", "AQGC"]
evgenConfig.inputfilecheck = "MadGraph.185166.WmAA_mu_LM0123_FM2_3p0Em10"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Veit Scharf <veit.scharf@kip.uni-heidelberg.de>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")
