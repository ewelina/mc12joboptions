include('MC12JobOptions/AlpgenControl_Multijets.py')
evgenConfig.minevents = 50
evgenConfig.inputconfcheck = 'Alpgen_CTEQ6L1_Multijet_Np6_JZ2x'
evgenConfig.description = 'pp -> 6 or more light jets, for leading jet pT range = 80-14000 GeV'

# Modify the normal leading pT cut of the JZ2 filter
topAlg.QCDTruthJetFilter.MaxPt = 14000.*GeV
