###############################################################
#
# Job options file for POWHEG with Pythia8
# C. Johnson, 19.05.2014 <christian.johnson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+Pythia8 W+2jets production with bornsuppfact (pT1 80GeV, pT2 60GeV, Mjj 500GeV), muR*0.5 muF*0.5 and AU2 CT10 tune at 7TeV"
evgenConfig.keywords       = [ "QCD","W","2jet","muon","SM","NLO","muRdown","muFdown" ]
evgenConfig.contact        = [ "christian.johnson@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Pythia8" ]
evgenConfig.inputfilecheck = "Wplus_munu"
evgenConfig.minevents      = 10000

## Pythia8 Showering
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Powheg.py")