
evgenConfig.description = "gammagamma -> mumu production with Pythia8+MRST2004QED single dissociative setup, 60<M<200GeV, pt>8 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "dissociation", "leptons", "muons"]
evgenConfig.contact = ["Mateusz Dyndal <mateusz.dyndal@cern.ch>"]

evgenConfig.minevents = 5000

if runArgs.ecmEnergy == 7000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOmumu_7TeV_60M200'
elif runArgs.ecmEnergy == 8000:
   evgenConfig.inputfilecheck = 'MRST2004QED_SDiss_ggTOmumu_8TeV_60M200'
else:
   raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
