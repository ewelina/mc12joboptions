evgenConfig.description    = "POWHEG+fHerwig/Jimmy Z->ee production with two central lepton pt>15 GeV filter and AUET2 CT10 tune"
evgenConfig.keywords       = [ "electroweak", "Z", "leptons", "electron"]
evgenConfig.contact        = [ "oleg.fedin@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*.Zee*"
	
## Herwig/Jimmy Showering
include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Photos.py")


## DiLeptonFilter 
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

