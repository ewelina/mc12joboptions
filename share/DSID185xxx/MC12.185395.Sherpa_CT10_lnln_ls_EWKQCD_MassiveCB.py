include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "lnlnjj(j) like-sign Max_Order_EW=6 with massive c,b"
evgenConfig.keywords = ["VBS", "Vector-Boson Scattering", "Diboson"]
evgenConfig.contact  = ["philipp.anger@cern.ch"]
evgenConfig.minevents = 100

evgenConfig.process = """
(run){
  MASSIVE[4] = 1;
  MASSIVE[5] = 1;
  ACTIVE[25] = 1;
  MASS[25]   = 126.0;
  WIDTH[25]  = 0.00418;
  PARTICLE_CONTAINER 901 leptons 11 13 15;
  PARTICLE_CONTAINER 902 antileptons -11 -13 -15;
  PARTICLE_CONTAINER 903 antineutrinos -12 -14 -16;
  PARTICLE_CONTAINER 904 neutrinos 12 14 16;
  ERROR=0.05;
}(run)

(model){
  EW_SCHEME 3;
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 4000;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 4000;
}(beam)

(processes){
  Process 93 93 -> 4 901 901 903 903 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process -4 -4 -> 901 901 903 903 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process -4 93 -> 4 901 901 903 903 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process -4 93 -> 901 901 903 903 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process 93 93 -> 4 4 901 901 903 903 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process 93 93 -> 901 901 903 903 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;

  Process 4 93 -> 902 902 904 904 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
  Process 93 93 -> -4 -4 902 902 904 904 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
  Process 4 93 -> -4 902 902 904 904 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
  Process 4 4 -> 902 902 904 904 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
  Process 93 93 -> -4 902 902 904 904 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
  Process 93 93 -> 902 902 904 904 93 93 93{1};
  CKKW sqr(20/E_CMS);
  Max_Order_EW 6;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  End process;
 
}(processes)

(selector){
  PT 90 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1 100 10;
}(selector)
"""
