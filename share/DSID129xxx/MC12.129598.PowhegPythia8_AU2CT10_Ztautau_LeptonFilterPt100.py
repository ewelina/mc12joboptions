## POWHEG+Pythia8 Z->tautau, 100<ptlepton<150 GeV

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production with high 100<pt<150 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "100<ptlepton<150GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Ztautau"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 100000.
topAlg.LeptonFilter.PtcutMax = 150000.
topAlg.LeptonFilter.Etacut = 10.0
evgenConfig.minevents = 200
