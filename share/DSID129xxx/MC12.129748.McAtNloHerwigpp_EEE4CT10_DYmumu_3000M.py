# MC@NLO + Herwig++ Z/gamma* -> mumu
evgenConfig.description = "MC@NLO + Herwig++ Z/gamma*->mumu, M>3000 GeV, without lepton filter, CT10 and EEE4 tune"
evgenConfig.contact = ["Rob Hickling", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu"]

evgenConfig.inputfilecheck = "McAtNlo409.*DYmumu_3000M"

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]

