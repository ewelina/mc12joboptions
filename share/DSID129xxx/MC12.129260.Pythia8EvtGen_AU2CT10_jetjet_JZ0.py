evgenConfig.description = "Dijet truth jet slice JZ0, with the AU2 CT10 tune and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

ver =  os.popen("cmt show versions External/Pythia8").read()
if 'Pythia8-01' in ver:
    include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
else:
    include("MC12JobOptions/Pythia82_AU2_CT10_Common.py")

## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:nonDiffractive = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0.py")
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
include("PutAlgsInSequence.py")

