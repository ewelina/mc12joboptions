evgenConfig.description = "Dijet truth jet slice JZ0W, with the AU2 CT10 tune and muon filter"
evgenConfig.keywords = ["QCD", "jets", "mufilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")

## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0W.py")
evgenConfig.minevents = 500

include("MC12JobOptions/LowPtMuonFilter.py")
include("PutAlgsInSequence.py")
