include ( "MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
  
evgenConfig.description = 'Wtaunu + 2 photon + 0 jet'
evgenConfig.keywords =["triboson","W","gamma","WtaunugammagammaNp0"]
evgenConfig.contact=['nhduongvn1@gmail.com']
evgenConfig.inputfilecheck = 'WtaunugammagammaNp0'
evgenConfig.minevents = 5000
  

