## POWHEG+Pythia8 DYee_3000M

evgenConfig.description = "POWHEG+Pythia8 DYee with mass cut 3000<M<GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYee_3000M"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
