## POWHEG+Pythia8 DYtautau_20M60_LeptonFilter

evgenConfig.description = "POWHEG+Pythia8 DYtautau with mass cut 20<M<60GeV and single lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "tau", "ptlepton>7GeV", "etalepton<3.0"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYtautau_20M60"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 7000.
topAlg.LeptonFilter.Etacut = 3.0
