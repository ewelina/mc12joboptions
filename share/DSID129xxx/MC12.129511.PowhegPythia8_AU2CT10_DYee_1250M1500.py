## POWHEG+Pythia8 DYee_1250M1500

evgenConfig.description = "POWHEG+Pythia8 DYee with mass cut 1250<M<1500GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYee_1250M1500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
