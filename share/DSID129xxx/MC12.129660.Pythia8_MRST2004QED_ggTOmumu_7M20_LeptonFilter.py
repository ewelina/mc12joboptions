## Pythia8 photon induced di-lepton, gammagamma -> mumu

evgenConfig.description = "gammagamma -> mumu production with MRST2004QED, 7<M<20GeV, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "muons", "7<M<20GeV", "lepton filter"]

include("MC12JobOptions/Pythia8_MRST2004QED_Common.py")

topAlg.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 7.", # lower invariant mass
    "PhaseSpace:mHatMax = 20." # upper invariant mass
]

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8

