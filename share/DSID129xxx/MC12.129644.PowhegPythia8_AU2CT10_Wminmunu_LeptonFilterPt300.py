## POWHEG+Pythia8 Wmin->munu, 300<ptlepton GeV

evgenConfig.description = "POWHEG+Pythia8 Wmin->munu production with high 300<ptlepton GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "300<ptlepton GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminmunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 300000.
topAlg.LeptonFilter.Etacut = 10.0
evgenConfig.minevents = 1000
