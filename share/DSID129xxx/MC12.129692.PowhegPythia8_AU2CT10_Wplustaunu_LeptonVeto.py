## POWHEG+Pythia8 Wplus->taunu central lepton veto

evgenConfig.description = "POWHEG+Pythia8 Wplus->taunu production with central lepton pt>15 GeV veto and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau", "ptlepton<15 GeV", "|etalepton|>2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# force leptonic tau decays
## topAlg.Pythia8.Commands += [
##     '15:onMode = off',
##     '15:onIfAny = 11 12 13 14'
##     ]

# lepton veto
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
StreamEVGEN.VetoAlgs += ["LeptonFilter"]
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
