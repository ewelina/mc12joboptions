##############################################################
# Job options fragment for pp->J/psi->e(3)e(8)X
# Created: 23 April 2012 by Gabriella.Pasztor@cern.ch
# Xsec: ~800 nb, Filter effi: 13%, 10 mins / 10 events (17.2.2.2)  
##############################################################

# Common Pythia8B config
include ("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# General evgen config options
evgenConfig.description = "Pythia8B inclusive pp->J/psi(e3e8) production"
evgenConfig.keywords = ["egamma", "Jpsi", "electrons"]
evgenConfig.minevents = 500

# General Charmonium fragment
include ("MC12JobOptions/Pythia8B_Charmonium_Common.py")

# Photos++
include("MC12JobOptions/Pythia8B_Photos.py")

# Close all J/psi decays apart from J/psi->ee
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:1:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [443,-11,11]

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = [3.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [2] 

# Filter
include("MC12JobOptions/ElectronFilter.py")
topAlg.ElectronFilter.Ptcut = 8000.
topAlg.ElectronFilter.Etacut =  2.7
