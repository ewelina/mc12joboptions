evgenConfig.description = "High-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Epos_Base_Fragment.py")
include("MC12JobOptions/JetFilter_MinbiasHigh.py")
evgenConfig.minevents = 100
