evgenConfig.description = "Pythia8 e/gamma performance sample (jets, gamma+jet, W/Z, ttbar), with EM jet pT > 35 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

## Configure Pythia
topAlg.Pythia8.Commands += ["HardQCD:all = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "WeakSingleBoson:all = on",
                            "Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "PhaseSpace:pTHatMin = 33",
                            "PhaseSpace:mHatMin = 66"]

## Set up EM jet filter (efficiency ~= 12%)
## Closeness of pTHatMin to jet cut is unphysical: ok for performance studies only
from GeneratorFilters.GeneratorFiltersConf import JetFilter
topAlg += JetFilter()
topAlg.JetFilter.JetNumber = 1
topAlg.JetFilter.EtaRange = 2.7
topAlg.JetFilter.JetThreshold = 35000.
topAlg.JetFilter.JetType = False # True = cone, False = grid
topAlg.JetFilter.GridSizeEta = 2 # Number of (approx 0.06 size) eta cells
topAlg.JetFilter.GridSizePhi = 2 # Number of (approx 0.06 size) phi cells
StreamEVGEN.RequireAlgs += ["JetFilter"]
