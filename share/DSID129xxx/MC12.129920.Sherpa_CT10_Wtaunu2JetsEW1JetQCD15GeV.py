include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "VBF-W production, includes dibosons"
evgenConfig.keywords = [ "Sherpa", "VBF", "EW","diboson"]
evgenConfig.contact  = [ "rking@cern.ch" ]
evgenConfig.minevents = 1000



evgenConfig.process="""
(processes){
!positron
Process 93 93  -> 15 -16 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

!electron
Process 93 93  -> -15 16 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

}(processes)

(selector){
Mass -15 16 10 E_CMS
Mass 15 -16 10 E_CMS
NJetFinder  2  15.0  0.0  0.4 1
}(selector)

(model){
ACTIVE[6]=0
}(model)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '129920.Sherpa_CT10_Wtaunu2JetsEW1JetQCD15GeV'
