## POWHEG+Pythia8 Z->ee, 200<ptlepton<300 GeV

evgenConfig.description = "POWHEG+Pythia8 Z->ee production with high 200<ptlepton<300 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e", "200<ptlepton<300 GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zee"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 200000.
topAlg.LeptonFilter.Etacut = 10.0
topAlg.LeptonFilter.PtcutMax = 300000.
