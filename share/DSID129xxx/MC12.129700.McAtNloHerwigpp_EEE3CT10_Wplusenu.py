# MC@NLO + Herwig++ W+ -> enu
evgenConfig.description = "MC@NLO + Herwig++ W+->enu, without lepton filter, CT10 and EEE3 tune"
evgenConfig.contact = ["Marc Goulette", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]

evgenConfig.inputfilecheck = "McAtNlo.*CT10.*Wplusenu"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]
