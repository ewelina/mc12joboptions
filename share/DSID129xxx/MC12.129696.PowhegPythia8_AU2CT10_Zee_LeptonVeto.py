## POWHEG+Pythia8 Z->ee central lepton veto

evgenConfig.description = "POWHEG+Pythia8 Z->ee production with central lepton pt>15 GeV veto and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e", "ptlepton<15 GeV", "|etalepton|>2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zee"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# lepton veto
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
topAlg += LeptonFilter()
StreamEVGEN.VetoAlgs += ["LeptonFilter"]
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7

evgenConfig.minevents = 1000
