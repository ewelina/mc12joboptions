evgenConfig.description = "Low-pT inelastic minimum bias events using EPOS"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Epos_Base_Fragment.py")
include("MC12JobOptions/JetFilter_MinbiasLow.py")
