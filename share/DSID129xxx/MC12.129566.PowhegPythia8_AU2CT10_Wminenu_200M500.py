## POWHEG+Pythia8 Wminenu_200M500

evgenConfig.description = "POWHEG+Pythia8 Wminenu with mass cut 200<M<500GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminenu_200M500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
