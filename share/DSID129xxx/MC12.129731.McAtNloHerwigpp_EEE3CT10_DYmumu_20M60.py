evgenConfig.description = "MC@NLO + Herwig++ Z/gamma*->mumu, 20<M<60 GeV, without lepton filter, CT10 and EEE3 tune"
evgenConfig.contact = ["Marc Goulette", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]

evgenConfig.inputfilecheck = "McAtNlo.*CT10.*DYmumu"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]

# for technical reasons need to generate MC@NLO events with lower mass cut and discard later
include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent = [23]  # Select Z
topAlg.ParentChildFilter.MassMinParent = 20000.  # min mass 20 GeV
#topAlg.ParentChildFilter.MassMaxParent = 60000.  # max mass 60 GeV
topAlg.ParentChildFilter.EtaRangeParent = 1e99  # catch also Z with 0 pt
topAlg.ParentChildFilter.PDGChild = [-13, 13]   # Select mu+ or mu- in Z decay
topAlg.ParentChildFilter.PtMinChild = 0.  # no filtering on child
