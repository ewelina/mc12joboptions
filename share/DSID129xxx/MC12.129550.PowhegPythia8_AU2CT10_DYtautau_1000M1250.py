## POWHEG+Pythia8 DYtautau_1000M1250

evgenConfig.description = "POWHEG+Pythia8 DYtautau with mass cut 1000<M<1250GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYtautau_1000M1250"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
