##############################################################
# Job options fragment for bb->J/psi->e(3)e(13)X
# Created: 23 April 2012 by Gabriella.Pasztor@cern.ch
# Xsec: 1840 nb, Filter effi: 62%, 2h17 / 10 ev (17.2.2.6)  
##############################################################

# Common Pythia8B config
include ("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# General evgen config options
evgenConfig.description = "Pythia8B inclusive bb->J/psi(e3e13) production"
evgenConfig.keywords = ["egamma", "Jpsi", "electrons"]
evgenConfig.minevents = 50

# General bb -> Charmonium fragment
include ("MC12JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

# Photos++
include("MC12JobOptions/Pythia8B_Photos.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3

# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 1

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 102.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->ee
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:1:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [443,-11,11]

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = [13.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [1]

# Filter
include("MC12JobOptions/MultiElectronFilter.py")
topAlg.MultiElectronFilter.Ptcut = 3000.
topAlg.MultiElectronFilter.Etacut =  2.7
topAlg.MultiElectronFilter.NElectrons =  2
