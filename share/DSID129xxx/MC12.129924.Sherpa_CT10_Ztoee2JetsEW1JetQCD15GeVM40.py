include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-Z production, with Z/gamma* -> ee."
evgenConfig.keywords = [ "Sherpa", "VBF" ]
evgenConfig.contact  = [ "kiran.joshi@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(processes){
  Process 93 93 -> 11 -11 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  End process;
}(processes)

(selector){
  Mass 11 -11 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '129924.Sherpa_CT10_Ztoee2JetsEW1JetQCD15GeVM40'
