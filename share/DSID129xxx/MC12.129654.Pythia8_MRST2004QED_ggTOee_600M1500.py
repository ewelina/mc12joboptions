## Pythia8 photon induced di-lepton, gammagamma -> ee

evgenConfig.description = "gammagamma -> ee production with MRST2004QED, 600<M<1500GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "electrons", "600<M<1500GeV"]

include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = MRST2004qed.LHgrid",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2ee= on", # gg->ee
    "PhaseSpace:mHatMin = 600.", # lower invariant mass
    "PhaseSpace:mHatMax = 1500." # upper invariant mass
]
evgenConfig.tune = "MRST2004qed"
