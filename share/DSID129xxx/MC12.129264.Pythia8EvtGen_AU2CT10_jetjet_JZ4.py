evgenConfig.description = "Dijet truth jet slice JZ4, with the AU2 CT10 tune and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 250."]

include("MC12JobOptions/JetFilter_JZ4.py")
evgenConfig.minevents = 2000

include("MC12JobOptions/Pythia8_EvtGenAfterburner.py")
include("PutAlgsInSequence.py")



