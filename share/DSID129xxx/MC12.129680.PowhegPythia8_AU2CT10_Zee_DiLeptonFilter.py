## POWHEG+Pythia8 Z->ee central di-lepton filter

evgenConfig.description = "POWHEG+Pythia8 Z->ee production with two central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e", "two ptlepton>15 GeV", "two |etalepton|<2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zee"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
