## POWHEG+Pythia8 Wplustaunu_1500M2500

evgenConfig.description = "POWHEG+Pythia8 Wplustaunu with mass cut 1500<M<2500GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu_1500M2500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
