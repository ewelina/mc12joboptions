evgenConfig.description = "Dijet truth jet slice JZ1W, with the AU2 CT10 tune mu filter"
evgenConfig.keywords = ["QCD", "jets", "mufilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 10."]

include("MC12JobOptions/JetFilter_JZ1W.py")
evgenConfig.minevents = 20

include("MC12JobOptions/LowPtMuonFilter.py")
include("PutAlgsInSequence.py")
