## Pythia8 photon induced di-lepton, gammagamma -> mumu

evgenConfig.description = "gammagamma -> mumu production with MRST2004QED, 60<M<200GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "muons", "60<M<200GeV"]

include("MC12JobOptions/Pythia8_MRST2004QED_Common.py")

topAlg.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 60.", # lower invariant mass
    "PhaseSpace:mHatMax = 200.", # upper invariant mass
]

