# MC@NLO + Herwig++ Z/gamma* -> ee
evgenConfig.description = "MC@NLO + Herwig++ Z/gamma*->ee, 800<M<1000 GeV, without lepton filter, CT10 and EEE4 tune"
evgenConfig.contact = ["Rob Hickling", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e"]

evgenConfig.inputfilecheck = "McAtNlo409.*DYee_800M1000"

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]

