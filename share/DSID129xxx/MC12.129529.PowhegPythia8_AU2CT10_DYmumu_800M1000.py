## POWHEG+Pythia8 DYmumu_800M1000

evgenConfig.description = "POWHEG+Pythia8 DYmumu with mass cut 800<M<1000GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*DYmumu_800M1000"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
