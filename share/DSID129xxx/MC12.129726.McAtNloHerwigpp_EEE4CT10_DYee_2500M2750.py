# MC@NLO + Herwig++ Z/gamma* -> ee
evgenConfig.description = "MC@NLO + Herwig++ Z/gamma*->ee, 2500<M<2750 GeV, without lepton filter, CT10 and EEE4 tune"
evgenConfig.contact = ["Rob Hickling", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e"]

evgenConfig.inputfilecheck = "McAtNlo409.*DYee_2500M2750"

include("MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]

