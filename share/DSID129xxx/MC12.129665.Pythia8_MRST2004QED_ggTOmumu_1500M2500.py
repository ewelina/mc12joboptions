## Pythia8 photon induced di-lepton, gammagamma -> mumu

evgenConfig.description = "gammagamma -> mumu production with MRST2004QED, 1500<M<2500GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "muons", "1500<M<2500GeV"]

include("MC12JobOptions/Pythia8_MRST2004QED_Common.py")

topAlg.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 1500.", # lower invariant mass
    "PhaseSpace:mHatMax = 2500." # upper invariant mass
]
