# MC@NLO + Herwig++ W+ -> taunu
evgenConfig.description = "MC@NLO + Herwig++ W+->taunu, without lepton filter, CT10 and EEE3 tune"
evgenConfig.contact = ["Marc Goulette", "Jan Kretzschmar"]
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]

evgenConfig.inputfilecheck = "McAtNlo.*CT10.*Wplustaunu"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_LHEF_Common.py" )
evgenConfig.generators += ["McAtNlo"]

# Include QED radiation (not yet on by default)
topAlg.Herwigpp.Commands += ["insert /Herwig/EventHandlers/LHEHandler:PostSubProcessHandlers[0] /Herwig/QEDRadiation/QEDRadiationHandler"]
