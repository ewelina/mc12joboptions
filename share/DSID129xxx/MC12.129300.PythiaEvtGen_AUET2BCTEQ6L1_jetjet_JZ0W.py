evgenConfig.description = "Dijet truth jet slice JZ0W, with Pythia 6 AUET2B and EvtGen Afterburner"
evgenConfig.keywords = ["QCD", "jets", "EvtGen"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

# use min bias ND for this slice
topAlg.Pythia.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           ]


include("MC12JobOptions/JetFilter_JZ0W.py")
evgenConfig.minevents = 5000
include("MC12JobOptions/Pythia_EvtGenAfterburner.py")
include("PutAlgsInSequence.py")


