include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "tau+- nu gamma gamma production with up to three jets in ME+PS."
evgenConfig.keywords = [ "W", "gamma" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]
evgenConfig.minevents = 1000

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  16 -15 22 22 93{3}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  End process;

  Process 93 93 ->  -16 15 22 22 93{3}
  CKKW sqr(20/E_CMS)
  Order_EW 4
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  End process;
}(processes)

(selector){
  PT 22  10 E_CMS
  PT 90  15 E_CMS
  DeltaR 22 22 0.2 1000
  DeltaR 22 93 0.2 1000
  DeltaR 22 90 0.3 1000
  DeltaR 93 90 0.2 1000
}(selector)
"""

evgenConfig.inputconfcheck = '129819.Sherpa_CT10_taunugammagamma'
