## Pythia8 photon induced di-lepton, gammagamma -> ee

evgenConfig.description = "gammagamma -> ee production with MRST2004QED, 200<M<600GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "electrons", "200<M<600GeV"]

include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = MRST2004qed.LHgrid",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2ee= on", # gg->ee
    "PhaseSpace:mHatMin = 200.", # lower invariant mass
    "PhaseSpace:mHatMax = 600." # upper invariant mass
]
evgenConfig.tune = "MRST2004qed"
