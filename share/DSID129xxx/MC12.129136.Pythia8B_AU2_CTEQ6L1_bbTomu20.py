##############################################################
# Job options fragment for bb->mu20X 
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive bb->mu20X production"
evgenConfig.keywords = ["bottom","mu","inclusive"]
evgenConfig.minevents = 200

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 20.']    
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 20.0
topAlg.Pythia8B.AntiQuarkPtCut = 20.0
topAlg.Pythia8B.QuarkEtaCut = 4.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = True

topAlg.Pythia8B.NHadronizationLoops = 4

include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [20.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
