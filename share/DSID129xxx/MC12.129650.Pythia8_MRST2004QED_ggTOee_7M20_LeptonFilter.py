## Pythia8 photon induced di-lepton, gammagamma -> ee

evgenConfig.description = "gammagamma -> ee production with MRST2004QED, 7<M<20GeV, central lepton filter pt>10 GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "electrons", "7<M<20GeV", "lepton filter"]

include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = MRST2004qed.LHgrid",
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2ee= on", # gg->ee
    "PhaseSpace:mHatMin = 7.", # lower invariant mass
    "PhaseSpace:mHatMax = 20." # upper invariant mass
]
evgenConfig.tune = "MRST2004qed"

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8
