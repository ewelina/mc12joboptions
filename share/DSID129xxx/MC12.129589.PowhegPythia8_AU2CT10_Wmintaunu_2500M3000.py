## POWHEG+Pythia8 Wmintaunu_2500M3000

evgenConfig.description = "POWHEG+Pythia8 Wmintaunu with mass cut 2500<M<3000GeV without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wmintaunu_2500M3000"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
