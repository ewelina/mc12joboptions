## POWHEG+Pythia8 Wmintaunu_3500M

evgenConfig.description = "POWHEG+Pythia8 Wmintaunu with mass cut 3500GeV<M without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wmintaunu_3500M"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
