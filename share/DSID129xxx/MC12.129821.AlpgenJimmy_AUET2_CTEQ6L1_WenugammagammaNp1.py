include ( "MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
  
evgenConfig.description = 'Wenu + 2 photon + 1 jet'
evgenConfig.keywords =["triboson","W","gamma","WenugammagammaNp1"]
evgenConfig.contact=['nhduongvn1@gmail.com']
evgenConfig.inputfilecheck = 'WenugammagammaNp1'
evgenConfig.minevents = 5000
  

