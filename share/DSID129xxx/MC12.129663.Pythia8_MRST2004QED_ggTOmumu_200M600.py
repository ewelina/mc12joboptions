## Pythia8 photon induced di-lepton, gammagamma -> mumu

evgenConfig.description = "gammagamma -> mumu production with MRST2004QED, 200<M<600GeV"
evgenConfig.keywords = ["photon-induced", "gammagamma", "leptons", "muons", "200<M<600GeV"]

include("MC12JobOptions/Pythia8_MRST2004QED_Common.py")

topAlg.Pythia8.Commands += [
    "SpaceShower:pTdampMatch = 1",
    "PhotonCollision:gmgm2mumu= on", # gg->mumu
    "PhaseSpace:mHatMin = 200.", # lower invariant mass
    "PhaseSpace:mHatMax = 600." # upper invariant mass
]
