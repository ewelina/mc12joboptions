include ( "MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
  
evgenConfig.description = 'Wmunu + 2 photon + 2 jets'
evgenConfig.keywords =["triboson","W","gamma","WmunugammagammaNp2"]
evgenConfig.contact=['nhduongvn1@gmail.com']
evgenConfig.inputfilecheck = 'WmunugammagammaNp2'
evgenConfig.minevents = 5000
  

