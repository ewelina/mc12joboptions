evgenConfig.description = "Dijet truth jet slice JZ0W, with the AU2 CT10 tune and Dstar filter"
evgenConfig.keywords = ["QCD", "jets", "DstarFilter"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")

## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0W.py")
evgenConfig.minevents = 100

include("MC12JobOptions/DstarFilter.py")
include("PutAlgsInSequence.py")


