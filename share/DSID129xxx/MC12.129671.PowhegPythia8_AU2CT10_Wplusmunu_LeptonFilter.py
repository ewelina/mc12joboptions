## POWHEG+Pythia8 Wplus->munu central lepton filter

evgenConfig.description = "POWHEG+Pythia8 Wplus->munu production with central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "ptlepton>15 GeV", "|etalepton|<2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusmunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
