## POWHEG+Pythia8 Z->tautau central di-lepton filter

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production with two central lepton pt>15 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau", "two ptlepton>15 GeV", "two |etalepton|<2.7"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Ztautau"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# force leptonic tau decays
topAlg.Pythia8.Commands += [
    '15:onMode = off',
    '15:onIfAny = 11 12 13 14'
    ]

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
