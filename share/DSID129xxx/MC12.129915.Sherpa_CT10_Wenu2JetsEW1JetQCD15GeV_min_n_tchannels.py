include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "VBF-W production. Min_N_Tchannels option enabled"
evgenConfig.keywords = [ "Sherpa", "VBF", "EW"]
evgenConfig.contact  = [ "rking@cern.ch" ]
evgenConfig.minevents = 1000



evgenConfig.process="""
(run){
EW_TCHAN_MODE=1
}(run)

(processes){
!positron
Process 93 93  -> 11 -12 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
Min_N_TChannels 1
End process;

!electron
Process 93 93  -> -11 12 93 93 93{1}
Order_EW 4
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)
Min_N_TChannels 1
End process;

}(processes)

(selector){
Mass -11 12 10 E_CMS
Mass 11 -12 10 E_CMS
NJetFinder  2  15.0  0.0  0.4 1
}(selector)

(model){
ACTIVE[6]=0
}(model)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '129915.Sherpa_CT10_Wenu2JetsEW1JetQCD15GeV_min_n_tchannels'
