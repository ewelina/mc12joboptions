## POWHEG+Pythia8 Wplus->taunu, 100<ptlepton<150 GeV

evgenConfig.description = "POWHEG+Pythia8 Wplus->taunu production with high 100<ptlepton<150 GeV filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau", "100<ptlepton<150 GeV"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 100000.
topAlg.LeptonFilter.Etacut = 10.0
topAlg.LeptonFilter.PtcutMax = 150000.
evgenConfig.minevents = 500
