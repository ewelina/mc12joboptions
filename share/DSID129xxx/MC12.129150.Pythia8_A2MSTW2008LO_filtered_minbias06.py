evgenConfig.description = "Inelastic minimum bias with grid-type Jet Filtering for egamma performances"
evgenConfig.keywords = ["QCD", "minbias", "egamma", "pileup"]

include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

from GeneratorFilters.GeneratorFiltersConf import JetFilter

if "JetFilter" not in topAlg:
    topAlg += JetFilter()
if "JetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["JetFilter"]

JetFilter = topAlg.JetFilter
JetFilter.JetNumber = 1
JetFilter.EtaRange = 2.7 
JetFilter.JetThreshold = 6000.;  
JetFilter.JetType=False; #true is a cone, false is a grid
JetFilter.GridSizeEta=2; # sets the number of (approx 0.06 size) eta
JetFilter.GridSizePhi=2; # sets the number of (approx 0.06 size) phi cells
