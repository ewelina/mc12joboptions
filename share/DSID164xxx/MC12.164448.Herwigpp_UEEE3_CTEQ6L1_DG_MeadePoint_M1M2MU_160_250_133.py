## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.CharginoDG')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

## Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_DG_MeadePoint_M1M2MU_160_250_133.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

# Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['neutralinos','charginos'], slha_file)

## Define metadata
evgenConfig.description = 'DG Search for Patrick Maede Point'
evgenConfig.keywords    = ['SUSY','Direct Gaugino','chargino','neutralino']
evgenConfig.contact     = ['alaettin.serhan.mete@cern.ch']

## Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.IncludeHadTaus = 0      # No hadronic taus in the filter

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

## Clean up
del cmds
