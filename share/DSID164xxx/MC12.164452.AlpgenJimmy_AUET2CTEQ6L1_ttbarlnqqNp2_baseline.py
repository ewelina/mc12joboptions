evgenConfig.description = "ALPGEN+Jimmy ttbar(->lnqq) process with AUET2 tune and pdf CTEQ6L1"
evgenConfig.contact = ["christopher.young@cern.ch","Sarah.Allwood@cern.ch"]
evgenConfig.keywords = ["top"]
evgenConfig.inputfilecheck = "ttbarlnqqNp2"
evgenConfig.minevents = 1000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
