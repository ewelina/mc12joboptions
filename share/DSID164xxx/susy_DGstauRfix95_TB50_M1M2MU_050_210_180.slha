#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     5.00000000e+01   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.23124800e+02   # h
        35     1.00658880e+03   # H
        36     1.00000000e+03   # A
        37     1.01125950e+03   # H+
   1000001     3.00058500e+03   # dL
   1000002     2.99952200e+03   # uL
   1000003     3.00058500e+03   # sL
   1000004     2.99952220e+03   # cL
   1000005     2.99641190e+03   # b1
   1000006     2.83296070e+03   # t1
   1000011     3.00037110e+03   # eL
   1000012     2.99930790e+03   # snue
   1000013     3.00037110e+03   # muL
   1000014     2.99930790e+03   # snum
   1000015     9.49737000e+01   # ta1
   1000016     2.99930790e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     4.62870000e+01   # N1
   1000023     1.43983500e+02   # N2
   1000024     1.42902500e+02   # C1
   1000025    -1.92003800e+02   # N3
   1000035     2.61733200e+02   # N4
   1000037     2.62706200e+02   # C2
   2000001     3.00010690e+03   # dR
   2000002     2.99978590e+03   # uR
   2000003     3.00010690e+03   # sR
   2000004     2.99978610e+03   # cR
   2000005     3.00427730e+03   # b2
   2000006     3.16331320e+03   # t2
   2000011     3.00032100e+03   # eR
   2000013     3.00032100e+03   # muR
   2000015     3.00038550e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -2.04514100e-02   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07389471e-01   # O_{11}
  1  2    -7.06823979e-01   # O_{12}
  2  1     7.06823979e-01   # O_{21}
  2  2     7.07389471e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.85256207e-01   # O_{11}
  1  2     7.28302088e-01   # O_{12}
  2  1    -7.28302088e-01   # O_{21}
  2  2     6.85256207e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2     1.00000000e+00   # O_{12}
  2  1    -1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.58464800e-01   # 
  1  2    -3.95502200e-02   # 
  1  3     2.72294850e-01   # 
  1  4    -7.50807100e-02   # 
  2  1     2.31692360e-01   # 
  2  2     6.00132580e-01   # 
  2  3    -5.95802430e-01   # 
  2  4     4.80811060e-01   # 
  3  1     1.28495720e-01   # 
  3  2    -1.40740140e-01   # 
  3  3    -6.70456710e-01   # 
  3  4    -7.17055920e-01   # 
  4  1    -1.05609340e-01   # 
  4  2     7.86428030e-01   # 
  4  3     3.48371890e-01   # 
  4  4    -4.99013780e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -4.96606710e-01   # Umix_{11}
  1  2     8.67975650e-01   # Umix_{12}
  2  1    -8.67975650e-01   # Umix_{21}
  2  2    -4.96606710e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -7.15966700e-01   # Vmix_{11}
  1  2     6.98134420e-01   # Vmix_{12}
  2  1    -6.98134420e-01   # Vmix_{21}
  2  2    -7.15966700e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.80000000e+02   # mu(Q)
     2     5.00000000e+01   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     5.00000000e+01   # M1
     2     2.10000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.00000000e+03   # m_eR
    35     3.00000000e+03   # m_muR
    36     8.47000000e+01   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     9.00000000e+03   # A_e
  2  2     9.00000000e+03   # A_mu
  3  3     9.00000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.78493387e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.54386210e-02    2      1000022         1                       
      1.03304030e-01    2      1000023         1                       
      8.93213130e-03    2      1000025         1                       
      2.13371710e-01    2      1000035         1                       
      1.63841780e-01    2     -1000024         2                       
      4.95111760e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.79169307e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.16037330e-03    2      1000022         2                       
      1.36862800e-01    2      1000023         2                       
      4.53643360e-03    2      1000025         2                       
      1.92964140e-01    2      1000035         2                       
      3.39841370e-01    2      1000024         1                       
      3.19634820e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.78493387e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.54386260e-02    2      1000022         3                       
      1.03304070e-01    2      1000023         3                       
      8.93213410e-03    2      1000025         3                       
      2.13371780e-01    2      1000035         3                       
      1.63841770e-01    2     -1000024         4                       
      4.95111700e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.79169307e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.16037290e-03    2      1000022         4                       
      1.36862740e-01    2      1000023         4                       
      4.53643310e-03    2      1000025         4                       
      1.92964080e-01    2      1000035         4                       
      3.39841460e-01    2      1000024         3                       
      3.19634880e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  8.89327262e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.56952170e-02    2      1000022         5                       
      2.18434560e-01    2      1000023         5                       
      1.37050390e-01    2      1000025         5                       
      2.08449680e-02    2      1000035         5                       
      4.08159820e-01    2     -1000024         6                       
      1.03345510e-01    2     -1000037         6                       
      3.64695940e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  8.60470893e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.74939780e-02    2      1000022         6                       
      6.71864750e-02    2      1000023         6                       
      1.62621320e-01    2      1000025         6                       
      2.32710450e-01    2      1000035         6                       
      1.39371020e-01    2      1000024         5                       
      3.70616730e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69377252e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125380e-01    2      1000022         1                       
      5.34871930e-02    2      1000023         1                       
      1.63923690e-02    2      1000025         1                       
      1.09950660e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.77439275e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125440e-01    2      1000022         2                       
      5.34871480e-02    2      1000023         2                       
      1.63923430e-02    2      1000025         2                       
      1.09950300e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69377252e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125380e-01    2      1000022         3                       
      5.34871930e-02    2      1000023         3                       
      1.63923690e-02    2      1000025         3                       
      1.09950660e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.77439275e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125500e-01    2      1000022         4                       
      5.34871480e-02    2      1000023         4                       
      1.63923430e-02    2      1000025         4                       
      1.09950320e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  9.26572442e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.86526040e-03    2      1000022         5                       
      8.84346890e-02    2      1000023         5                       
      1.94344730e-01    2      1000025         5                       
      1.53854040e-01    2      1000035         5                       
      1.26829010e-01    2     -1000024         6                       
      3.85356160e-01    2     -1000037         6                       
      4.73160890e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.21609637e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.68570800e-01    2      1000024         5                       
      3.77106290e-02    2      1000037         5                       
      1.47732090e-01    2           23   1000006                       
      2.89877210e-02    2           24   1000005                       
      2.71406580e-02    2           24   2000005                       
      3.73489520e-02    2      1000022         6                       
      1.35219440e-01    2      1000023         6                       
      1.86145840e-01    2      1000025         6                       
      3.11439560e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.12509401e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.26507380e-02    2      1000022        11                       
      1.61283970e-01    2      1000023        11                       
      1.49289530e-03    2      1000025        11                       
      1.59971070e-01    2      1000035        11                       
      1.50327880e-01    2     -1000024        12                       
      4.54273490e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.12845763e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.80350820e-02    2      1000022        12                       
      6.80395890e-02    2      1000023        12                       
      1.35495720e-02    2      1000025        12                       
      2.14764240e-01    2      1000035        12                       
      3.12084050e-01    2      1000024        11                       
      2.93527420e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.12509401e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.26507380e-02    2      1000022        13                       
      1.61283970e-01    2      1000023        13                       
      1.49289530e-03    2      1000025        13                       
      1.59971070e-01    2      1000035        13                       
      1.50327880e-01    2     -1000024        14                       
      4.54273490e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.12845763e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.80350820e-02    2      1000022        14                       
      6.80395890e-02    2      1000023        14                       
      1.35495720e-02    2      1000025        14                       
      2.14764240e-01    2      1000035        14                       
      3.12084050e-01    2      1000024        13                       
      2.93527420e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  2.74513075e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  1.78960820e+02   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.26158820e-02    2      1000022        16                       
      1.56961700e-02    2      1000023        16                       
      3.12577400e-03    2      1000025        16                       
      4.95443270e-02    2      1000035        16                       
      1.34374630e-01    2      1000024        15                       
      8.79237880e-02    2      1000037        15                       
      6.86719360e-01    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.52449334e+01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125320e-01    2      1000022        11                       
      5.34872150e-02    2      1000023        11                       
      1.63923820e-02    2      1000025        11                       
      1.09950860e-02    2      1000035        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.52449334e+01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.19125320e-01    2      1000022        13                       
      5.34872150e-02    2      1000023        13                       
      1.63923820e-02    2      1000025        13                       
      1.09950860e-02    2      1000035        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  1.79150789e+02   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.28893660e-02    2      1000022        15                       
      6.65067430e-02    2      1000023        15                       
      3.74058860e-02    2      1000025        15                       
      4.67742720e-02    2      1000035        15                       
      3.46139260e-02    2     -1000024        16                       
      1.04599300e-01    2     -1000037        16                       
      3.43890130e-01    2           36   1000015                       
      3.43320400e-01    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  3.01359828e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33936020e-03    3      1000024         1        -2             
      3.33936020e-03    3     -1000024         2        -1             
      3.33936020e-03    3      1000024         3        -4             
      3.33936020e-03    3     -1000024         4        -3             
      8.80813450e-02    3      1000024         5        -6             
      8.80813450e-02    3     -1000024         6        -5             
      9.98494490e-03    3      1000037         1        -2             
      9.98494490e-03    3     -1000037         2        -1             
      9.98494490e-03    3      1000037         3        -4             
      9.98494490e-03    3     -1000037         4        -3             
      1.47380290e-01    3      1000037         5        -6             
      1.47380290e-01    3     -1000037         6        -5             
      3.97666880e-05    2      1000022        21                       
      2.35403820e-03    3      1000022         1        -1             
      2.35403820e-03    3      1000022         3        -3             
      9.34905930e-03    3      1000022         5        -5             
      1.75865670e-02    3      1000022         6        -6             
      4.42955090e-04    2      1000023        21                       
      4.45324740e-03    3      1000023         1        -1             
      4.45324740e-03    3      1000023         3        -3             
      3.74100210e-02    3      1000023         5        -5             
      4.95531750e-02    3      1000023         6        -6             
      8.19272890e-04    2      1000025        21                       
      3.75553970e-04    3      1000025         1        -1             
      3.75553970e-04    3      1000025         3        -3             
      4.15021260e-02    3      1000025         5        -5             
      1.15941000e-01    3      1000025         6        -6             
      1.89300250e-04    2      1000035        21                       
      9.13202300e-03    3      1000035         1        -1             
      9.13202300e-03    3      1000035         3        -3             
      1.98569070e-02    3      1000035         5        -5             
      1.50459690e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  9.82036286e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.65776710e-02    2      1000022        23                       
      4.61711140e-01    2      1000015       -15                       
      4.61711140e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.23201571e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.22813050e-04    3     -1000024         2        -1             
      7.42710110e-05    3     -1000024        12       -11             
      7.42710110e-05    3     -1000024        14       -13             
      2.22813050e-04    3      1000024         1        -2             
      7.42710110e-05    3      1000024        11       -12             
      7.42710110e-05    3      1000024        13       -14             
      2.22813050e-04    3     -1000024         4        -3             
      7.42710110e-05    3     -1000024        16       -15             
      2.22813050e-04    3      1000024         3        -4             
      7.42710110e-05    3      1000024        15       -16             
      1.91173790e-01    2      1000022        23                       
      4.48244860e-05    3      1000023         2        -2             
      5.77826800e-05    3      1000023         1        -1             
      5.77826800e-05    3      1000023         3        -3             
      4.48244860e-05    3      1000023         4        -4             
      5.15158460e-05    3      1000023         5        -5             
      1.31098660e-05    3      1000023        11       -11             
      1.31098660e-05    3      1000023        13       -13             
      1.28629720e-05    3      1000023        15       -15             
      2.60900270e-05    3      1000023        12       -12             
      2.60900270e-05    3      1000023        14       -14             
      2.60900270e-05    3      1000023        16       -16             
      1.53219090e-02    2      1000022        25                       
      3.95896700e-01    2      1000015       -15                       
      3.95896700e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.93075562e-01   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.74790640e-01    2      1000024       -24                       
      3.74790640e-01    2     -1000024        24                       
      2.73909270e-02    2      1000022        23                       
      3.90279760e-03    2      1000023        23                       
      1.08937280e-04    3      1000025         2        -2             
      1.40545770e-04    3      1000025         1        -1             
      1.40545770e-04    3      1000025         3        -3             
      1.08937280e-04    3      1000025         4        -4             
      1.35301130e-04    3      1000025         5        -5             
      3.18777640e-05    3      1000025        11       -11             
      3.18777640e-05    3      1000025        13       -13             
      3.16764270e-05    3      1000025        15       -15             
      6.33968670e-05    3      1000025        12       -12             
      6.33968670e-05    3      1000025        14       -14             
      6.33968670e-05    3      1000025        16       -16             
      3.21741590e-02    2      1000022        25                       
      9.30155370e-02    2      1000015       -15                       
      9.30155370e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  1.02200208e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.85094450e-01    2      1000022        24                       
      8.14905520e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  5.48957465e-01   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.10791050e-02    2      1000022        24                       
      4.58157420e-01    2      1000023        24                       
      7.95861470e-04    3      1000025         2        -1             
      7.95861470e-04    3      1000025         4        -3             
      2.65258020e-04    3      1000025       -11        12             
      2.65258020e-04    3      1000025       -13        14             
      2.65262730e-04    3      1000025       -15        16             
      2.21335160e-01    2     -1000015        16                       
      2.47040840e-01    2      1000024        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.53714141e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.66873330e-04    2           13       -13                       
      4.77355640e-02    2           15       -15                       
      2.00228090e-03    2            3        -3                       
      6.50068580e-01    2            5        -5                       
      2.82415430e-02    2            4        -4                       
      1.83201910e-03    2           22        22                       
      3.56500820e-02    2           21        21                       
      6.15294320e-03    3           24        11       -12             
      6.15294320e-03    3           24        13       -14             
      6.15294320e-03    3           24        15       -16             
      1.84588280e-02    3           24        -2         1             
      1.84588280e-02    3           24        -4         3             
      6.15294320e-03    3          -24       -11        12             
      6.15294320e-03    3          -24       -13        14             
      6.15294320e-03    3          -24       -15        16             
      1.84588280e-02    3          -24         2        -1             
      1.84588280e-02    3          -24         4        -3             
      7.98048630e-04    3           23        12       -12             
      7.98048630e-04    3           23        14       -14             
      7.98048630e-04    3           23        16       -16             
      4.01649830e-04    3           23        11       -11             
      4.01649830e-04    3           23        13       -13             
      4.01649830e-04    3           23        15       -15             
      1.37601830e-03    3           23         2        -2             
      1.37601830e-03    3           23         4        -4             
      1.77265080e-03    3           23         1        -1             
      1.77265080e-03    3           23         3        -3             
      1.77265080e-03    3           23         5        -5             
      1.11881240e-01    2      1000022   1000022                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  6.95992387e+01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.59423450e-04    2           13       -13                       
      7.43014740e-02    2           15       -15                       
      3.04120850e-03    2            3        -3                       
      7.49969300e-01    2            5        -5                       
      2.58918620e-04    2            6        -6                       
      2.82551720e-03    2      1000022   1000022                       
      1.21280360e-02    2      1000022   1000023                       
      6.26686400e-03    2      1000022   1000025                       
      7.07184810e-05    2      1000022   1000035                       
      8.21798480e-03    2      1000023   1000023                       
      2.28818810e-03    2      1000023   1000025                       
      5.19712040e-03    2      1000023   1000035                       
      2.01579460e-03    2      1000025   1000025                       
      2.27879990e-02    2      1000025   1000035                       
      6.18480050e-03    2      1000035   1000035                       
      4.04932160e-02    2      1000024  -1000024                       
      8.42470580e-03    2      1000037  -1000037                       
      2.76343670e-02    2      1000024  -1000037                       
      2.76343670e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  6.91619032e+01   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.59357070e-04    2           13       -13                       
      7.42833760e-02    2           15       -15                       
      3.04059590e-03    2            3        -3                       
      7.50433330e-01    2            5        -5                       
      2.75123690e-04    2            6        -6                       
      1.06656650e-05    2           21        21                       
      2.91300170e-03    2      1000022   1000022                       
      1.31337270e-02    2      1000022   1000023                       
      5.42618850e-03    2      1000022   1000025                       
      4.30539990e-05    2      1000022   1000035                       
      9.54652480e-03    2      1000023   1000023                       
      1.68625710e-03    2      1000023   1000025                       
      6.36279350e-03    2      1000023   1000035                       
      2.16060620e-03    2      1000025   1000025                       
      1.70358720e-02    2      1000025   1000035                       
      9.49402620e-03    2      1000035   1000035                       
      4.60394550e-02    2      1000024  -1000024                       
      1.33519130e-02    2      1000037  -1000037                       
      2.22520470e-02    2      1000024  -1000037                       
      2.22520470e-02    2     -1000024   1000037                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  6.27454719e+01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.89098010e-04    2           14       -13                       
      8.28015950e-02    2           16       -15                       
      3.11525170e-03    2            4        -3                       
      7.23968630e-01    2            6        -5                       
      2.44725200e-02    2      1000024   1000022                       
      2.83181320e-03    2      1000024   1000023                       
      1.71178650e-02    2      1000024   1000025                       
      4.35129110e-02    2      1000024   1000035                       
      4.99934600e-04    2      1000037   1000022                       
      6.73693120e-02    2      1000037   1000023                       
      3.38037830e-02    2      1000037   1000025                       
      2.17415950e-04    2      1000037   1000035                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
