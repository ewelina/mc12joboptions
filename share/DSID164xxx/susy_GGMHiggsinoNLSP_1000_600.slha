#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     6.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05423505E+01   # W+
        25     1.26000000E+02   # h
        35     2.00378632E+03   # H
        36     2.00000000E+03   # A
        37     2.00196777E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50044985E+03   # ~d_L
   2000001     1.50008455E+03   # ~d_R
   1000002     1.49963460E+03   # ~u_L
   2000002     1.49983088E+03   # ~u_R
   1000003     1.50044985E+03   # ~s_L
   2000003     1.50008455E+03   # ~s_R
   1000004     1.49963460E+03   # ~c_L
   2000004     1.49983088E+03   # ~c_R
   1000005     1.49943053E+03   # ~b_1
   2000005     1.50110839E+03   # ~b_2
   1000006     1.48693141E+03   # ~t_1
   2000006     1.52883415E+03   # ~t_2
   1000011     1.50028078E+03   # ~e_L
   2000011     1.50025365E+03   # ~e_R
   1000012     1.49946543E+03   # ~nu_eL
   1000013     1.50028078E+03   # ~mu_L
   2000013     1.50025365E+03   # ~mu_R
   1000014     1.49946543E+03   # ~nu_muL
   1000015     1.49973450E+03   # ~tau_1
   2000015     1.50080184E+03   # ~tau_2
   1000016     1.49946543E+03   # ~nu_tauL
   1000021     1.00000000E+03   # ~g
   1000022     5.80883868E+02   # ~chi_10
   1000023    -6.00201242E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.01931761E+03   # ~chi_40
   1000024     5.85409840E+02   # ~chi_1+
   1000037     1.01489586E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.01710616E-01   # N_11
  1  2    -1.82341659E-01   # N_12
  1  3     6.93757638E-01   # N_13
  1  4    -6.89279921E-01   # N_14
  2  1     5.47404181E-03   # N_21
  2  2    -9.81356860E-03   # N_22
  2  3    -7.06469111E-01   # N_23
  2  4    -7.07654664E-01   # N_24
  3  1     8.73318905E-01   # N_31
  3  2     4.87148941E-01   # N_32
  3  3     2.66503784E-06   # N_33
  3  4    -3.82295202E-06   # N_34
  4  1     4.76393453E-01   # N_41
  4  2    -8.54007890E-01   # N_42
  4  3    -1.40006196E-01   # N_43
  4  4     1.55299924E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.75663043E-01   # U_11
  1  2     9.84450352E-01   # U_12
  2  1     9.84450352E-01   # U_21
  2  2     1.75663043E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.94870739E-01   # V_11
  1  2     9.80828933E-01   # V_12
  2  1     9.80828933E-01   # V_21
  2  2     1.94870739E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.08752036E-01   # cos(theta_t)
  1  2     7.05457689E-01   # sin(theta_t)
  2  1    -7.05457689E-01   # -sin(theta_t)
  2  2     7.08752036E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.25414844E-01   # cos(theta_b)
  1  2     7.80292428E-01   # sin(theta_b)
  2  1    -7.80292428E-01   # -sin(theta_b)
  2  2     6.25414844E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.98062387E-01   # cos(theta_tau)
  1  2     7.16036943E-01   # sin(theta_tau)
  2  1    -7.16036943E-01   # -sin(theta_tau)
  2  2     6.98062387E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89767805E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     6.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51341259E+02   # vev(Q)              
         4     3.93078299E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53954062E-01   # gprime(Q) DRbar
     2     6.34549464E-01   # g(Q) DRbar
     3     1.11185683E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.06818688E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.76906660E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80419105E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.00000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.33493791E+06   # M^2_Hd              
        22     4.30685393E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     7.11042607E-04   # gluino decays
#          BR         NDA      ID1       ID2
     1.11499303E-01    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.03980178E-01    2     1000023        21   # BR(~g -> ~chi_20 g)
#           BR         NDA      ID1       ID2       ID3
     6.98773173E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.36707123E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     5.42007974E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.93920720E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     6.98773173E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.36707123E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     5.42007974E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.93920720E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     7.45207107E-03    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     5.54565027E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     7.21676546E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.41572609E-02    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.10152756E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.10152756E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.10152756E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.10152756E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     3.10644815E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     3.10644815E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000024     2.18342011E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.53895688E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.53895688E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17965765E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17965765E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.62770943E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     4.04387968E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     4.75582141E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     7.16924531E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     9.26938004E-02    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     7.16924531E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     9.26938004E-02    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     5.08128744E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.11121699E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.11121699E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.01117022E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.21170999E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.21170999E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.21170999E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.45173410E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.45173410E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.45173410E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.45173410E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.15058921E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.15058921E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.15058921E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.15058921E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     2.00380652E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     2.00380652E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
