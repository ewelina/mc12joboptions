include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Vgamma decaying hadronically to qqgamma, merged with up to three additional QCD jets in ME+PS." 
evgenConfig.keywords = [ "EW", "Diboson" ]
evgenConfig.inputconfcheck = "gammaVtoqq"
evgenConfig.minevents = 5000
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[25]=0
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 23 22;
}(run)

(processes){
  Process 93 93 -> 22 9923[a] 93{3}
  Decay 9923[a] -> 94 94
  Order_EW 3;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  Selector_File *|(selector2){|}(selector2)
  End process;

  Process 93 93 -> 22 24[a] 93{3}
  Decay 24[a] -> 94 94
  Order_EW 3;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;

  Process 93 93 -> 22 -24[a] 93{3}
  Decay -24[a] -> 94 94
  Order_EW 3;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
  Integration_Error 0.05 {5,6,7}
  End process;
}(processes)

(selector){
  PT 22 70.0 E_CMS
  DeltaR 93 22 0.3 100.0
}(selector)

(selector2){
  PT 22 70.0 E_CMS
  DeltaR 93 22 0.3 100.0
  DecayMass 9923 7.0 E_CMS
}(selector2)
"""
