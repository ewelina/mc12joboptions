#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19693000e+02   # h
        35     1.00709770e+03   # H
        36     1.00000000e+03   # A
        37     1.00966200e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99900000e+03   # b1
   1000006     2.83487720e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     3.04285200e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.37420500e+02   # N1
   1000023     4.71121100e+02   # N2
   1000024     4.69625200e+02   # C1
   1000025    -5.03025400e+02   # N3
   1000035     6.34483600e+02   # N4
   1000037     6.34340500e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00165700e+03   # b2
   2000006     3.16163060e+03   # t2
   2000011     3.04312000e+02   # eR
   2000013     3.04312000e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68787850e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377087e-01   # O_{11}
  1  2    -7.06836372e-01   # O_{12}
  2  1     7.06836372e-01   # O_{21}
  2  2     7.07377087e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.43902393e-01   # O_{11}
  1  2     7.65107645e-01   # O_{12}
  2  1    -7.65107645e-01   # O_{21}
  2  2     6.43902393e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.94055450e-01   # 
  1  2    -1.00851000e-02   # 
  1  3     9.98712800e-02   # 
  1  4    -4.21626600e-02   # 
  2  1    -9.52299700e-02   # 
  2  2    -4.45159080e-01   # 
  2  3     6.42817440e-01   # 
  2  4    -6.16076950e-01   # 
  3  1     4.01217300e-02   # 
  3  2    -4.25558900e-02   # 
  3  3    -7.02909770e-01   # 
  3  4    -7.08870890e-01   # 
  4  1     3.42805100e-02   # 
  4  2    -8.94382950e-01   # 
  4  3    -2.87628680e-01   # 
  4  4     3.40842990e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -4.04832930e-01   # Umix_{11}
  1  2     9.14390620e-01   # Umix_{12}
  2  1    -9.14390620e-01   # Umix_{21}
  2  2    -4.04832930e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -4.80814370e-01   # Vmix_{11}
  1  2     8.76822410e-01   # Vmix_{12}
  2  1    -8.76822410e-01   # Vmix_{21}
  2  2    -4.80814370e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     5.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     6.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.01300000e+02   # m_eR
    35     3.01300000e+02   # m_muR
    36     3.01400000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.00000000e+03   # A_e
  2  2     3.00000000e+03   # A_mu
  3  3     3.00000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.52563072e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.31785380e-02    2      1000022         1                       
      6.23437280e-02    2      1000023         1                       
      8.42798090e-04    2      1000025         1                       
      2.65193580e-01    2      1000035         1                       
      1.11740400e-01    2     -1000024         2                       
      5.46701010e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.53149480e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05363670e-02    2      1000022         2                       
      7.27812570e-02    2      1000023         2                       
      4.18535080e-04    2      1000025         2                       
      2.57311730e-01    2      1000035         2                       
      1.57297360e-01    2      1000024         1                       
      5.01654740e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.52563072e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.31785400e-02    2      1000022         3                       
      6.23437390e-02    2      1000023         3                       
      8.42798270e-04    2      1000025         3                       
      2.65193640e-01    2      1000035         3                       
      1.11740390e-01    2     -1000024         4                       
      5.46700950e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.53149480e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05363620e-02    2      1000022         4                       
      7.27812280e-02    2      1000023         4                       
      4.18534910e-04    2      1000025         4                       
      2.57311610e-01    2      1000035         4                       
      1.57297340e-01    2      1000024         3                       
      5.01654860e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.34349284e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.91750930e-02    2      1000022         5                       
      5.09913340e-02    2      1000023         5                       
      4.16060820e-03    2      1000025         5                       
      9.93781460e-02    2      1000035         5                       
      4.13934320e-01    2     -1000024         6                       
      3.05336420e-01    2     -1000037         6                       
      8.70241080e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.72978610e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.27846690e-02    2      1000022         6                       
      1.34484930e-01    2      1000023         6                       
      2.21840650e-01    2      1000025         6                       
      2.03957300e-01    2      1000035         6                       
      5.77367540e-02    2      1000024         5                       
      3.39195730e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88726320e-01    2      1000022         1                       
      8.66839200e-03    2      1000023         1                       
      1.52781270e-03    2      1000025         1                       
      1.07752860e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88726440e-01    2      1000022         2                       
      8.66831470e-03    2      1000023         2                       
      1.52779640e-03    2      1000025         2                       
      1.07750940e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88726320e-01    2      1000022         3                       
      8.66839200e-03    2      1000023         3                       
      1.52781270e-03    2      1000025         3                       
      1.07752860e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.88726440e-01    2      1000022         4                       
      8.66831280e-03    2      1000023         4                       
      1.52779630e-03    2      1000025         4                       
      1.07750930e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.64207631e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.79403760e-02    2      1000022         5                       
      1.92916190e-02    2      1000023         5                       
      7.64765450e-03    2      1000025         5                       
      1.31514280e-01    2      1000035         5                       
      3.57597170e-01    2     -1000024         6                       
      3.71872450e-01    2     -1000037         6                       
      9.41364470e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.01738929e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.96761600e-01    2      1000024         5                       
      1.11130220e-02    2      1000037         5                       
      1.70699330e-01    2           23   1000006                       
      2.76810970e-02    2           24   1000005                       
      3.66426000e-02    2           24   2000005                       
      4.21922840e-02    2      1000022         6                       
      1.97963130e-01    2      1000023         6                       
      1.92424090e-01    2      1000025         6                       
      2.45229320e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.86426349e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.35953630e-02    2      1000022        11                       
      7.69510190e-02    2      1000023        11                       
      1.29787340e-04    2      1000025        11                       
      2.28627760e-01    2      1000035        11                       
      1.01941250e-01    2     -1000024        12                       
      4.98754830e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.86721504e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00655050e-01    2      1000022        12                       
      4.79193670e-02    2      1000023        12                       
      1.28710010e-03    2      1000025        12                       
      2.48436410e-01    2      1000035        12                       
      1.43631910e-01    2      1000024        11                       
      4.58070130e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.86426349e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.35953630e-02    2      1000022        13                       
      7.69510190e-02    2      1000023        13                       
      1.29787340e-04    2      1000025        13                       
      2.28627760e-01    2      1000035        13                       
      1.01941250e-01    2     -1000024        14                       
      4.98754830e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.86721504e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00655050e-01    2      1000022        14                       
      4.79193670e-02    2      1000023        14                       
      1.28710010e-03    2      1000025        14                       
      2.48436410e-01    2      1000035        14                       
      1.43631910e-01    2      1000024        13                       
      4.58070130e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  9.68967142e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.90855107e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.95913150e-02    2      1000022        16                       
      4.74129510e-02    2      1000023        16                       
      1.27349790e-03    2      1000025        16                       
      2.45810900e-01    2      1000035        16                       
      1.46587570e-01    2      1000024        15                       
      4.54081830e-01    2      1000037        15                       
      5.24192720e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  9.69266791e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  9.69266791e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.90553611e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.26608070e-02    2      1000022        15                       
      7.83489500e-02    2      1000023        15                       
      2.76089930e-03    2      1000025        15                       
      2.26638640e-01    2      1000035        15                       
      1.00862210e-01    2     -1000024        16                       
      4.93475650e-01    2     -1000037        16                       
      2.62898580e-03    2           36   1000015                       
      2.62407660e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.89524605e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.23433150e-03    3      1000024         1        -2             
      3.23433150e-03    3     -1000024         2        -1             
      3.23433150e-03    3      1000024         3        -4             
      3.23433150e-03    3     -1000024         4        -3             
      6.12789090e-02    3      1000024         5        -6             
      6.12789090e-02    3     -1000024         6        -5             
      1.52484770e-02    3      1000037         1        -2             
      1.52484770e-02    3     -1000037         2        -1             
      1.52484770e-02    3      1000037         3        -4             
      1.52484770e-02    3     -1000037         4        -3             
      1.40674340e-01    3      1000037         5        -6             
      1.40674340e-01    3     -1000037         6        -5             
      3.97598920e-05    2      1000022        21                       
      3.79413760e-03    3      1000022         1        -1             
      3.79413760e-03    3      1000022         3        -3             
      3.76929040e-03    3      1000022         5        -5             
      2.96195630e-02    3      1000022         6        -6             
      9.68892830e-04    2      1000023        21                       
      3.98466550e-03    3      1000023         1        -1             
      3.98466550e-03    3      1000023         3        -3             
      4.73528400e-03    3      1000023         5        -5             
      1.07423480e-01    3      1000023         6        -6             
      1.15204940e-03    2      1000025        21                       
      4.73154290e-05    3      1000025         1        -1             
      4.73154290e-05    3      1000025         3        -3             
      1.01831820e-03    3      1000025         5        -5             
      1.67194160e-01    3      1000025         6        -6             
      9.16328790e-05    2      1000035        21                       
      1.66025350e-02    3      1000035         1        -1             
      1.66025350e-02    3      1000035         3        -3             
      1.66467760e-02    3      1000035         5        -5             
      1.40645700e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.04032519e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.35581300e-01    2      1000022        23                       
      7.76125610e-01    2      1000022        25                       
      1.21374760e-02    2      2000011       -11                       
      1.21374760e-02    2     -2000011        11                       
      1.21374750e-02    2      2000013       -13                       
      1.21374750e-02    2     -2000013        13                       
      1.98714940e-02    2      1000015       -15                       
      1.98714940e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.69920755e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.92702420e-05    3     -1000024         2        -1             
      1.30900830e-05    3     -1000024        12       -11             
      1.30900830e-05    3     -1000024        14       -13             
      3.92702420e-05    3      1000024         1        -2             
      1.30900830e-05    3      1000024        11       -12             
      1.30900830e-05    3      1000024        13       -14             
      3.92702420e-05    3     -1000024         4        -3             
      1.30900830e-05    3     -1000024        16       -15             
      3.92702420e-05    3      1000024         3        -4             
      1.30900830e-05    3      1000024        15       -16             
      8.42356560e-01    2      1000022        23                       
      1.12691700e-05    3      1000023         1        -1             
      1.12691700e-05    3      1000023         3        -3             
      1.23910200e-01    2      1000022        25                       
      2.23787500e-03    2      2000011       -11                       
      2.23787500e-03    2     -2000011        11                       
      2.23787500e-03    2      2000013       -13                       
      2.23787500e-03    2     -2000013        13                       
      1.22617860e-02    2      1000015       -15                       
      1.22617860e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  2.34585501e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.49868630e-01    2      1000024       -24                       
      3.49868630e-01    2     -1000024        24                       
      6.57913040e-03    2      1000022        23                       
      2.72089010e-03    2      1000023        23                       
      1.36786090e-01    2      1000025        23                       
      1.93738400e-02    2      1000022        25                       
      1.30804420e-01    2      1000023        25                       
      1.63069450e-04    2      1000025        25                       
      4.79190490e-04    2      2000011       -11                       
      4.79190490e-04    2     -2000011        11                       
      4.79190490e-04    2      2000013       -13                       
      4.79190490e-04    2     -2000013        13                       
      9.59302360e-04    2      1000015       -15                       
      9.59302360e-04    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.83194217e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.82863660e-01    2      1000022        24                       
      1.71363300e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  2.33636235e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.46260990e-02    2      1000022        24                       
      3.54577840e-01    2      1000023        24                       
      1.81524290e-01    2      1000025        24                       
      9.68412090e-04    2     -1000015        16                       
      3.06941000e-01    2      1000024        23                       
      1.31362360e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56227906e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96543190e-04    2           13       -13                       
      5.62188180e-02    2           15       -15                       
      2.35927380e-03    2            3        -3                       
      7.69047440e-01    2            5        -5                       
      3.34374460e-02    2            4        -4                       
      1.90836120e-03    2           22        22                       
      3.96469270e-02    2           21        21                       
      4.93875700e-03    3           24        11       -12             
      4.93875700e-03    3           24        13       -14             
      4.93875700e-03    3           24        15       -16             
      1.48162700e-02    3           24        -2         1             
      1.48162700e-02    3           24        -4         3             
      4.93875700e-03    3          -24       -11        12             
      4.93875700e-03    3          -24       -13        14             
      4.93875700e-03    3          -24       -15        16             
      1.48162700e-02    3          -24         2        -1             
      1.48162700e-02    3          -24         4        -3             
      5.66779230e-04    3           23        12       -12             
      5.66779230e-04    3           23        14       -14             
      5.66779230e-04    3           23        16       -16             
      2.85254330e-04    3           23        11       -11             
      2.85254330e-04    3           23        13       -13             
      2.85254330e-04    3           23        15       -15             
      9.77257030e-04    3           23         2        -2             
      9.77257030e-04    3           23         4        -4             
      1.25894790e-03    3           23         1        -1             
      1.25894790e-03    3           23         3        -3             
      1.25894790e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.03178259e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.56966360e-05    2           13       -13                       
      2.45443730e-02    2           15       -15                       
      1.00461340e-03    2            3        -3                       
      2.47725320e-01    2            5        -5                       
      4.12304550e-01    2            6        -6                       
      2.17127080e-04    2           21        21                       
      1.39737850e-03    2           24       -24                       
      7.09478220e-04    2           23        23                       
      6.43608810e-03    2      1000022   1000022                       
      7.30762180e-02    2      1000022   1000023                       
      1.73585700e-01    2      1000022   1000025                       
      5.81240860e-04    2      1000022   1000035                       
      5.33812260e-03    2      1000023   1000023                       
      2.87128630e-02    2      1000023   1000025                       
      1.83543380e-02    2      1000024  -1000024                       
      5.51735280e-03    2           25        25                       
      1.39287630e-04    2      2000011  -2000011                       
      1.39258890e-04    2      2000013  -2000013                       
      1.31178780e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.87335962e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.66855410e-05    2           13       -13                       
      1.90996400e-02    2           15       -15                       
      7.81793850e-04    2            3        -3                       
      1.92950400e-01    2            5        -5                       
      3.41144560e-01    2            6        -6                       
      2.03021310e-04    2           21        21                       
      7.19389130e-03    2      1000022   1000022                       
      1.46670800e-01    2      1000022   1000023                       
      4.52174540e-02    2      1000022   1000025                       
      3.88332010e-03    2      1000022   1000035                       
      6.07400280e-02    2      1000023   1000023                       
      4.01597620e-04    2      1000023   1000025                       
      1.80595230e-01    2      1000024  -1000024                       
      1.05161670e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.86111715e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.11526880e-05    2           14       -13                       
      2.61073650e-02    2           16       -15                       
      9.90687170e-04    2            4        -3                       
      6.45669700e-01    2            6        -5                       
      2.68500750e-01    2      1000024   1000022                       
      2.35855390e-03    2      1000024   1000023                       
      4.97487110e-02    2      1000024   1000025                       
      5.07659790e-03    2      1000037   1000022                       
      1.45652400e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
