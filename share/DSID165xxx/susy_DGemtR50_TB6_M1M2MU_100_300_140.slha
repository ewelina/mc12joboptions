#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19482600e+02   # h
        35     5.04386300e+02   # H
        36     5.00000000e+02   # A
        37     5.09429400e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99989580e+03   # b1
   1000006     2.83344970e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.09770200e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     7.66182000e+01   # N1
   1000023     1.42905900e+02   # N2
   1000024     1.21553800e+02   # C1
   1000025    -1.47948100e+02   # N3
   1000035     3.28423900e+02   # N4
   1000037     3.28268800e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00076170e+03   # b2
   2000006     3.16290990e+03   # t2
   2000011     1.09751300e+02   # eR
   2000013     1.09751400e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80337430e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07374641e-01   # O_{11}
  1  2    -7.06838819e-01   # O_{12}
  2  1     7.06838819e-01   # O_{21}
  2  2     7.07374641e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     4.85828104e-01   # O_{11}
  1  2     8.74054377e-01   # O_{12}
  2  1    -8.74054377e-01   # O_{21}
  2  2     4.85828104e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     7.79522540e-01   # 
  1  2    -1.48455340e-01   # 
  1  3     5.09221910e-01   # 
  1  4    -3.33164840e-01   # 
  2  1    -6.13699910e-01   # 
  2  2    -3.04965110e-01   # 
  2  3     5.11168660e-01   # 
  2  4    -5.18725220e-01   # 
  3  1     1.07856650e-01   # 
  3  2    -1.08621490e-01   # 
  3  3    -6.71269300e-01   # 
  3  4    -7.25235280e-01   # 
  4  1     6.39076500e-02   # 
  4  2    -9.34429710e-01   # 
  4  3    -1.69698220e-01   # 
  4  4     3.06528600e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -2.35894840e-01   # Umix_{11}
  1  2     9.71778570e-01   # Umix_{12}
  2  1    -9.71778570e-01   # Umix_{21}
  2  2    -2.35894840e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -4.32715560e-01   # Vmix_{11}
  1  2     9.01530500e-01   # Vmix_{12}
  2  1    -9.01530500e-01   # Vmix_{21}
  2  2    -4.32715560e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     1.40000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     3.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.01100000e+02   # m_eR
    35     1.01100000e+02   # m_muR
    36     1.01500000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     8.40000000e+02   # A_e
  2  2     8.40000000e+02   # A_mu
  3  3     8.40000000e+02   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.74679797e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.85605790e-02    2      1000022         1                       
      1.24386350e-02    2      1000023         1                       
      5.52885420e-03    2      1000025         1                       
      2.94567050e-01    2      1000035         1                       
      3.73924780e-02    2     -1000024         2                       
      6.21512410e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.75299350e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.06923930e-05    2      1000022         2                       
      5.83453070e-02    2      1000023         2                       
      2.64354610e-03    2      1000025         2                       
      2.79602970e-01    2      1000035         2                       
      1.25569430e-01    2      1000024         1                       
      5.33828080e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.74679797e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.85605840e-02    2      1000022         3                       
      1.24386380e-02    2      1000023         3                       
      5.52885510e-03    2      1000025         3                       
      2.94567110e-01    2      1000035         3                       
      3.73924750e-02    2     -1000024         4                       
      6.21512290e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.75299350e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.06923900e-05    2      1000022         4                       
      5.83452920e-02    2      1000023         4                       
      2.64354560e-03    2      1000025         4                       
      2.79602920e-01    2      1000035         4                       
      1.25569430e-01    2      1000024         3                       
      5.33828080e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  2.12665590e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.41348470e-02    2      1000022         5                       
      3.24183550e-02    2      1000023         5                       
      2.24044290e-03    2      1000025         5                       
      1.07329380e-01    2      1000035         5                       
      3.88620290e-01    2     -1000024         6                       
      3.03043010e-01    2     -1000037         6                       
      8.22136700e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  7.17814494e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.58660540e-02    2      1000022         6                       
      1.63761590e-01    2      1000023         6                       
      2.15098830e-01    2      1000025         6                       
      1.95089850e-01    2      1000035         6                       
      7.30818440e-02    2      1000024         5                       
      3.27101830e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69094412e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.08477410e-01    2      1000022         1                       
      3.75917760e-01    2      1000023         1                       
      1.16073410e-02    2      1000025         1                       
      3.99749120e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76311625e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.08477530e-01    2      1000022         2                       
      3.75917610e-01    2      1000023         2                       
      1.16073360e-02    2      1000025         2                       
      3.99747350e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69094412e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.08477410e-01    2      1000022         3                       
      3.75917760e-01    2      1000023         3                       
      1.16073410e-02    2      1000025         3                       
      3.99749120e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76311625e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.08477590e-01    2      1000022         4                       
      3.75917670e-01    2      1000023         4                       
      1.16073370e-02    2      1000025         4                       
      3.99747350e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  6.29735936e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.98362040e-03    2      1000022         5                       
      1.06350010e-02    2      1000023         5                       
      9.89119800e-03    2      1000025         5                       
      1.39574350e-01    2      1000035         5                       
      3.63623080e-01    2     -1000024         6                       
      3.75622240e-01    2     -1000037         6                       
      9.16704540e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.05741734e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.91044500e-01    2      1000024         5                       
      1.80085260e-02    2      1000037         5                       
      1.68486600e-01    2           23   1000006                       
      1.53019860e-02    2           24   1000005                       
      4.85126900e-02    2           24   2000005                       
      1.01136680e-01    2      1000022         6                       
      1.16475130e-01    2      1000023         6                       
      2.17158030e-01    2      1000025         6                       
      2.38758830e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.08616836e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.41971130e-02    2      1000022        11                       
      1.26915500e-01    2      1000023        11                       
      7.48790630e-04    2      1000025        11                       
      2.44005200e-01    2      1000035        11                       
      3.42843200e-02    2     -1000024        12                       
      5.69849070e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.08946878e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02611800e-01    2      1000022        12                       
      3.21366420e-04    2      1000023        12                       
      8.66087530e-03    2      1000025        12                       
      2.83292200e-01    2      1000035        12                       
      1.15232410e-01    2      1000024        11                       
      4.89881310e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.08616836e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.41971130e-02    2      1000022        13                       
      1.26915500e-01    2      1000023        13                       
      7.48790630e-04    2      1000025        13                       
      2.44005200e-01    2      1000035        13                       
      3.42843200e-02    2     -1000024        14                       
      5.69849070e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.08946878e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02611800e-01    2      1000022        14                       
      3.21366420e-04    2      1000023        14                       
      8.66087530e-03    2      1000025        14                       
      2.83292200e-01    2      1000035        14                       
      1.15232410e-01    2      1000024        13                       
      4.89881310e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  8.89723972e-02   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.11323585e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02020260e-01    2      1000022        16                       
      3.19513780e-04    2      1000023        16                       
      8.61094710e-03    2      1000025        16                       
      2.81659070e-01    2      1000035        16                       
      1.19605850e-01    2      1000024        15                       
      4.87350820e-01    2      1000037        15                       
      4.33440520e-04    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  8.91447146e-02   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  8.91435072e-02   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.10989697e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.54460480e-02    2      1000022        15                       
      1.27575810e-01    2      1000023        15                       
      3.14811830e-03    2      1000025        15                       
      2.42749110e-01    2      1000035        15                       
      3.40864580e-02    2     -1000024        16                       
      5.66560510e-01    2     -1000037        16                       
      2.17056850e-04    2           36   1000015                       
      2.16896980e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.19802972e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.03613870e-03    3      1000024         1        -2             
      1.03613870e-03    3     -1000024         2        -1             
      1.03613870e-03    3      1000024         3        -4             
      1.03613870e-03    3     -1000024         4        -3             
      6.51797430e-02    3      1000024         5        -6             
      6.51797430e-02    3     -1000024         6        -5             
      1.68790390e-02    3      1000037         1        -2             
      1.68790390e-02    3     -1000037         2        -1             
      1.68790390e-02    3      1000037         3        -4             
      1.68790390e-02    3     -1000037         4        -3             
      1.37604950e-01    3      1000037         5        -6             
      1.37604950e-01    3     -1000037         6        -5             
      3.62402410e-04    2      1000022        21                       
      3.17863680e-03    3      1000022         1        -1             
      3.17863680e-03    3      1000022         3        -3             
      3.58075230e-03    3      1000022         5        -5             
      2.37861800e-02    3      1000022         6        -6             
      5.24313540e-04    2      1000023        21                       
      1.68869270e-03    3      1000023         1        -1             
      1.68869270e-03    3      1000023         3        -3             
      2.16635740e-03    3      1000023         5        -5             
      1.22781610e-01    3      1000023         6        -6             
      1.14486180e-03    2      1000025        21                       
      3.23628890e-04    3      1000025         1        -1             
      3.23628890e-04    3      1000025         3        -3             
      1.18600110e-03    3      1000025         5        -5             
      1.68673190e-01    3      1000025         6        -6             
      5.10294740e-05    2      1000035        21                       
      1.71555900e-02    3      1000035         1        -1             
      1.71555900e-02    3      1000035         3        -3             
      1.71104930e-02    3      1000035         5        -5             
      1.36709760e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.38013462e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.25294590e-05    3      1000022         2        -2             
      2.90236620e-05    3      1000022         1        -1             
      2.90236620e-05    3      1000022         3        -3             
      2.25294590e-05    3      1000022         4        -4             
      2.77854960e-05    3      1000022         5        -5             
      1.30953160e-05    3      1000022        12       -12             
      1.30953160e-05    3      1000022        14       -14             
      1.30953160e-05    3      1000022        16       -16             
      1.66871960e-01    2      2000011       -11                       
      1.66871960e-01    2     -2000011        11                       
      1.66870860e-01    2      2000013       -13                       
      1.66870860e-01    2     -2000013        13                       
      1.66172100e-01    2      1000015       -15                       
      1.66172100e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  6.96227972e-03   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.78407610e-05    2      1000022        22                       
      5.83867890e-04    3     -1000024         2        -1             
      1.94622630e-04    3     -1000024        12       -11             
      1.94622630e-04    3     -1000024        14       -13             
      5.83867890e-04    3      1000024         1        -2             
      1.94622630e-04    3      1000024        11       -12             
      1.94622630e-04    3      1000024        13       -14             
      5.83867890e-04    3     -1000024         4        -3             
      1.94622630e-04    3     -1000024        16       -15             
      5.83867890e-04    3      1000024         3        -4             
      1.94622630e-04    3      1000024        15       -16             
      8.51346180e-03    3      1000022         2        -2             
      1.09767250e-02    3      1000022         1        -1             
      1.09767250e-02    3      1000022         3        -3             
      8.51346180e-03    3      1000022         4        -4             
      1.05556840e-02    3      1000022         5        -5             
      2.49034380e-03    3      1000022        11       -11             
      2.49034380e-03    3      1000022        13       -13             
      2.47388660e-03    3      1000022        15       -15             
      4.95576300e-03    3      1000022        12       -12             
      4.95576300e-03    3      1000022        14       -14             
      4.95576300e-03    3      1000022        16       -16             
      1.27132150e-01    2      2000011       -11                       
      1.27132150e-01    2     -2000011        11                       
      1.27131510e-01    2      2000013       -13                       
      1.27131510e-01    2     -2000013        13                       
      2.08046870e-01    2      1000015       -15                       
      2.08046870e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  2.22124730e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.09232030e-01    2      1000024       -24                       
      3.09232030e-01    2     -1000024        24                       
      1.19404770e-03    2      1000022        23                       
      1.78406500e-02    2      1000023        23                       
      2.03803990e-01    2      1000025        23                       
      1.58579950e-02    2      1000022        25                       
      1.27110170e-01    2      1000023        25                       
      8.23034440e-03    2      1000025        25                       
      1.21184680e-03    2      2000011       -11                       
      1.21184680e-03    2     -2000011        11                       
      1.21184660e-03    2      2000013       -13                       
      1.21184660e-03    2     -2000013        13                       
      1.32566450e-03    2      1000015       -15                       
      1.32566450e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.20229531e-04   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.16225450e-01    3      1000022         2        -1             
      1.16225450e-01    3      1000022         4        -3             
      3.87391450e-02    3      1000022       -11        12             
      3.87391450e-02    3      1000022       -13        14             
      3.87391900e-02    3      1000022       -15        16             
      6.51331600e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  2.19312275e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.67995090e-02    2      1000022        24                       
      2.58141880e-01    2      1000023        24                       
      2.59221940e-01    2      1000025        24                       
      2.41241490e-04    2     -1000015        16                       
      2.76867240e-01    2      1000024        23                       
      1.68728250e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.06931608e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01296950e-04    2           13       -13                       
      5.75783030e-02    2           15       -15                       
      2.41640050e-03    2            3        -3                       
      7.87865400e-01    2            5        -5                       
      2.99272480e-02    2            4        -4                       
      1.70061160e-03    2           22        22                       
      3.51915320e-02    2           21        21                       
      4.32874260e-03    3           24        11       -12             
      4.32874260e-03    3           24        13       -14             
      4.32874260e-03    3           24        15       -16             
      1.29862280e-02    3           24        -2         1             
      1.29862280e-02    3           24        -4         3             
      4.32874260e-03    3          -24       -11        12             
      4.32874260e-03    3          -24       -13        14             
      4.32874260e-03    3          -24       -15        16             
      1.29862280e-02    3          -24         2        -1             
      1.29862280e-02    3          -24         4        -3             
      4.92512660e-04    3           23        12       -12             
      4.92512660e-04    3           23        14       -14             
      4.92512660e-04    3           23        16       -16             
      2.47876650e-04    3           23        11       -11             
      2.47876650e-04    3           23        13       -13             
      2.47876650e-04    3           23        15       -15             
      8.49204310e-04    3           23         2        -2             
      8.49204310e-04    3           23         4        -4             
      1.09398450e-03    3           23         1        -1             
      1.09398450e-03    3           23         3        -3             
      1.09398450e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.37885010e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.83558580e-05    2           13       -13                       
      1.09848910e-02    2           15       -15                       
      4.52414500e-04    2            3        -3                       
      1.21391040e-01    2            5        -5                       
      1.21218880e-01    2            6        -6                       
      1.74437680e-04    2           21        21                       
      2.44810250e-03    2           24       -24                       
      1.20347180e-03    2           23        23                       
      7.04108100e-02    2      1000022   1000022                       
      2.24379280e-02    2      1000022   1000023                       
      7.96270670e-02    2      1000022   1000025                       
      1.51980780e-02    2      1000022   1000035                       
      1.23478730e-04    2      1000023   1000023                       
      4.57164920e-03    2      1000023   1000025                       
      4.24112330e-03    2      1000023   1000035                       
      1.17576360e-02    2      1000025   1000025                       
      1.03664370e-01    2      1000025   1000035                       
      1.19215850e-01    2      1000024  -1000024                       
      1.49921820e-01    2      1000024  -1000037                       
      1.49921820e-01    2     -1000024   1000037                       
      1.01106630e-02    2           25        25                       
      3.01057790e-04    2      2000011  -2000011                       
      3.00997780e-04    2      2000013  -2000013                       
      2.84096140e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.52638628e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.66239580e-05    2           13       -13                       
      1.04894000e-02    2           15       -15                       
      4.32022440e-04    2            3        -3                       
      1.16070830e-01    2            5        -5                       
      1.72879060e-01    2            6        -6                       
      1.44515060e-04    2           21        21                       
      1.17345430e-01    2      1000022   1000022                       
      5.41592840e-02    2      1000022   1000023                       
      2.03307770e-02    2      1000022   1000025                       
      3.87606810e-02    2      1000022   1000035                       
      3.55897090e-04    2      1000023   1000023                       
      4.36142460e-03    2      1000023   1000025                       
      5.33976740e-02    2      1000023   1000035                       
      7.99505880e-03    2      1000025   1000025                       
      4.94445670e-03    2      1000025   1000035                       
      2.10978390e-01    2      1000024  -1000024                       
      9.26586460e-02    2      1000024  -1000037                       
      9.26586460e-02    2     -1000024   1000037                       
      2.00130350e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  3.19716326e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.11564290e-05    2           14       -13                       
      1.17875470e-02    2           16       -15                       
      4.47634050e-04    2            4        -3                       
      2.61205520e-01    2            6        -5                       
      1.13739520e-01    2      1000024   1000022                       
      1.02976850e-01    2      1000024   1000023                       
      3.99826690e-02    2      1000024   1000025                       
      1.58397900e-01    2      1000024   1000035                       
      8.25278610e-02    2      1000037   1000022                       
      1.10499440e-01    2      1000037   1000023                       
      1.15999800e-01    2      1000037   1000025                       
      2.39403080e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
