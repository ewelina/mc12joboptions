#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19693000e+02   # h
        35     1.00709770e+03   # H
        36     1.00000000e+03   # A
        37     1.00966200e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99900000e+03   # b1
   1000006     2.83487720e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.13041100e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.78270000e+01   # N1
   1000023     3.28329900e+02   # N2
   1000024     3.28025200e+02   # C1
   1000025    -5.03679400e+02   # N3
   1000035     5.27522600e+02   # N4
   1000037     5.27100700e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00165700e+03   # b2
   2000006     3.16163060e+03   # t2
   2000011     2.13123400e+02   # eR
   2000013     2.13123400e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68787850e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377087e-01   # O_{11}
  1  2    -7.06836372e-01   # O_{12}
  2  1     7.06836372e-01   # O_{21}
  2  2     7.07377087e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.43902393e-01   # O_{11}
  1  2     7.65107645e-01   # O_{12}
  2  1    -7.65107645e-01   # O_{21}
  2  2     6.43902393e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.94711520e-01   # 
  1  2    -1.55956600e-02   # 
  1  3     9.57908200e-02   # 
  1  4    -3.36111300e-02   # 
  2  1    -4.87944200e-02   # 
  2  2    -9.35422960e-01   # 
  2  3     2.81179010e-01   # 
  2  4    -2.08665100e-01   # 
  3  1     4.27599600e-02   # 
  3  2    -5.50156900e-02   # 
  3  3    -7.01758860e-01   # 
  3  4    -7.08999040e-01   # 
  4  1    -7.96209600e-02   # 
  4  2     3.48874990e-01   # 
  4  3     6.47531570e-01   # 
  4  4    -6.72792550e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.15719870e-01   # Umix_{11}
  1  2     4.01817230e-01   # Umix_{12}
  2  1    -4.01817230e-01   # Umix_{21}
  2  2    -9.15719870e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.54160870e-01   # Vmix_{11}
  1  2     2.99294200e-01   # Vmix_{12}
  2  1    -2.99294200e-01   # Vmix_{21}
  2  2    -9.54160870e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     5.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     3.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.08800000e+02   # m_eR
    35     2.08800000e+02   # m_muR
    36     2.08900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.00000000e+03   # A_e
  2  2     3.00000000e+03   # A_mu
  3  3     3.00000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.71990505e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.32600330e-02    2      1000022         1                       
      2.84498570e-01    2      1000023         1                       
      1.26662040e-03    2      1000025         1                       
      4.21226100e-02    2      1000035         1                       
      5.55878040e-01    2     -1000024         2                       
      1.02974190e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.72601189e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38978880e-03    2      1000022         2                       
      2.94996290e-01    2      1000023         2                       
      7.12383890e-04    2      1000025         2                       
      3.55599370e-02    2      1000035         2                       
      6.02326390e-01    2      1000024         1                       
      5.70152070e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.71990505e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.32600360e-02    2      1000022         3                       
      2.84498630e-01    2      1000023         3                       
      1.26662060e-03    2      1000025         3                       
      4.21226170e-02    2      1000035         3                       
      5.55877980e-01    2     -1000024         4                       
      1.02974170e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.72601189e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38978700e-03    2      1000022         4                       
      2.94996230e-01    2      1000023         4                       
      7.12383600e-04    2      1000025         4                       
      3.55599300e-02    2      1000035         4                       
      6.02326450e-01    2      1000024         3                       
      5.70152220e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.42402331e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.85718080e-02    2      1000022         5                       
      1.47189070e-01    2      1000023         5                       
      3.59854870e-03    2      1000025         5                       
      7.65898030e-03    2      1000035         5                       
      3.21845560e-01    2     -1000024         6                       
      3.96161440e-01    2     -1000037         6                       
      8.49745800e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.86211139e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.30279300e-02    2      1000022         6                       
      4.15087270e-02    2      1000023         6                       
      2.15036140e-01    2      1000025         6                       
      2.98464630e-01    2      1000035         6                       
      6.04390610e-02    2      1000024         5                       
      3.41523500e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69094412e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89968960e-01    2      1000022         1                       
      2.33037470e-03    2      1000023         1                       
      1.73137850e-03    2      1000025         1                       
      5.96935350e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76311625e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89968960e-01    2      1000022         2                       
      2.33036370e-03    2      1000023         2                       
      1.73135870e-03    2      1000025         2                       
      5.96927760e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69094412e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89968960e-01    2      1000022         3                       
      2.33037470e-03    2      1000023         3                       
      1.73137850e-03    2      1000025         3                       
      5.96935350e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76311625e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89968960e-01    2      1000022         4                       
      2.33036350e-03    2      1000023         4                       
      1.73135880e-03    2      1000025         4                       
      5.96927760e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.75681145e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.79836230e-02    2      1000022         5                       
      1.18201950e-01    2      1000023         5                       
      8.13728200e-03    2      1000025         5                       
      3.58597970e-02    2      1000035         5                       
      2.64014540e-01    2     -1000024         6                       
      4.63935430e-01    2     -1000037         6                       
      9.18673500e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.02408514e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.19854280e-01    2      1000024         5                       
      9.03556500e-02    2      1000037         5                       
      1.69583530e-01    2           23   1000006                       
      2.75001540e-02    2           24   1000005                       
      3.64030790e-02    2           24   2000005                       
      4.06701560e-02    2      1000022         6                       
      1.12429660e-01    2      1000023         6                       
      1.93373950e-01    2      1000025         6                       
      1.09829660e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.05945479e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75751300e-02    2      1000022        11                       
      2.81206550e-01    2      1000023        11                       
      2.91807520e-04    2      1000025        11                       
      2.72000990e-02    2      1000035        11                       
      5.09368480e-01    2     -1000024        12                       
      9.43579670e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.06246142e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.80540590e-02    2      1000022        12                       
      2.50447690e-01    2      1000023        12                       
      1.80935770e-03    2      1000025        12                       
      4.49911020e-02    2      1000035        12                       
      5.52407980e-01    2      1000024        11                       
      5.22897620e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.05945479e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75751300e-02    2      1000022        13                       
      2.81206550e-01    2      1000023        13                       
      2.91807520e-04    2      1000025        13                       
      2.72000990e-02    2      1000035        13                       
      5.09368480e-01    2     -1000024        14                       
      9.43579670e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.06246142e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.80540590e-02    2      1000022        14                       
      2.50447690e-01    2      1000023        14                       
      1.80935770e-03    2      1000025        14                       
      4.49911020e-02    2      1000035        14                       
      5.52407980e-01    2      1000024        13                       
      5.22897620e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  6.67457637e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.10400299e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.70634300e-02    2      1000022        16                       
      2.47917460e-01    2      1000023        16                       
      1.79107800e-03    2      1000025        16                       
      4.45365640e-02    2      1000035        16                       
      5.47666970e-01    2      1000024        15                       
      5.59943240e-02    2      1000037        15                       
      5.03017890e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  6.68196215e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  6.68196215e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.10093458e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.67382360e-02    2      1000022        15                       
      2.78772740e-01    2      1000023        15                       
      2.78729530e-03    2      1000025        15                       
      2.90436880e-02    2      1000035        15                       
      5.04214410e-01    2     -1000024        16                       
      9.34032200e-02    2     -1000037        16                       
      2.52261430e-03    2           36   1000015                       
      2.51800030e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.95132074e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.68836940e-02    3      1000024         1        -2             
      1.68836940e-02    3     -1000024         2        -1             
      1.68836940e-02    3      1000024         3        -4             
      1.68836940e-02    3     -1000024         4        -3             
      4.54069530e-02    3      1000024         5        -6             
      4.54069530e-02    3     -1000024         6        -5             
      3.01869350e-03    3      1000037         1        -2             
      3.01869350e-03    3     -1000037         2        -1             
      3.01869350e-03    3      1000037         3        -4             
      3.01869350e-03    3     -1000037         4        -3             
      1.56793610e-01    3      1000037         5        -6             
      1.56793610e-01    3     -1000037         6        -5             
      3.49586950e-05    2      1000022        21                       
      3.71928210e-03    3      1000022         1        -1             
      3.71928210e-03    3      1000022         3        -3             
      3.69328190e-03    3      1000022         5        -5             
      2.93473140e-02    3      1000022         6        -6             
      2.60482890e-04    2      1000023        21                       
      1.85255130e-02    3      1000023         1        -1             
      1.85255130e-02    3      1000023         3        -3             
      1.84943120e-02    3      1000023         5        -5             
      3.22152970e-02    3      1000023         6        -6             
      1.12729550e-03    2      1000025        21                       
      7.11900650e-05    3      1000025         1        -1             
      7.11900650e-05    3      1000025         3        -3             
      1.01175790e-03    3      1000025         5        -5             
      1.60822320e-01    3      1000025         6        -6             
      7.72478230e-04    2      1000035        21                       
      2.74928820e-03    3      1000035         1        -1             
      2.74928820e-03    3      1000035         3        -3             
      3.54740580e-03    3      1000035         5        -5             
      2.14531900e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  3.65504220e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07750240e-01    2      1000022        23                       
      7.66400220e-01    2      1000022        25                       
      1.82123780e-02    2      2000011       -11                       
      1.82123780e-02    2     -2000011        11                       
      1.82123650e-02    2      2000013       -13                       
      1.82123650e-02    2     -2000013        13                       
      2.65001380e-02    2      1000015       -15                       
      2.65001380e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.45084897e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.96818200e-01    2      1000024       -24                       
      2.96818200e-01    2     -1000024        24                       
      1.21470010e-01    2      1000022        23                       
      2.47230460e-01    2      1000023        23                       
      2.52501670e-02    2      1000022        25                       
      3.49394050e-03    2      1000023        25                       
      6.44045420e-04    2      2000011       -11                       
      6.44045420e-04    2     -2000011        11                       
      6.44045360e-04    2      2000013       -13                       
      6.44045360e-04    2     -2000013        13                       
      3.17135550e-03    2      1000015       -15                       
      3.17135550e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  2.77850479e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.41679220e-01    2      1000024       -24                       
      3.41679220e-01    2     -1000024        24                       
      2.51286610e-02    2      1000022        23                       
      8.63014250e-03    2      1000023        23                       
      8.47127070e-02    2      1000022        25                       
      1.81283100e-01    2      1000023        25                       
      2.14318440e-03    2      2000011       -11                       
      2.14318440e-03    2     -2000011        11                       
      2.14318400e-03    2      2000013       -13                       
      2.14318400e-03    2     -2000013        13                       
      4.15722930e-03    2      1000015       -15                       
      4.15722930e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.45984020e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.81210590e-01    2      1000022        24                       
      1.87895520e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  2.68938465e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07593190e-01    2      1000022        24                       
      3.63469930e-01    2      1000023        24                       
      4.22382220e-03    2     -1000015        16                       
      3.23707250e-01    2      1000024        23                       
      2.01005770e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56227906e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96544120e-04    2           13       -13                       
      5.62190790e-02    2           15       -15                       
      2.35928480e-03    2            3        -3                       
      7.69051020e-01    2            5        -5                       
      3.34376020e-02    2            4        -4                       
      1.90385050e-03    2           22        22                       
      3.96471140e-02    2           21        21                       
      4.93878030e-03    3           24        11       -12             
      4.93878030e-03    3           24        13       -14             
      4.93878030e-03    3           24        15       -16             
      1.48163400e-02    3           24        -2         1             
      1.48163400e-02    3           24        -4         3             
      4.93878030e-03    3          -24       -11        12             
      4.93878030e-03    3          -24       -13        14             
      4.93878030e-03    3          -24       -15        16             
      1.48163400e-02    3          -24         2        -1             
      1.48163400e-02    3          -24         4        -3             
      5.66781910e-04    3           23        12       -12             
      5.66781910e-04    3           23        14       -14             
      5.66781910e-04    3           23        16       -16             
      2.85255660e-04    3           23        11       -11             
      2.85255660e-04    3           23        13       -13             
      2.85255660e-04    3           23        15       -15             
      9.77261690e-04    3           23         2        -2             
      9.77261690e-04    3           23         4        -4             
      1.25895380e-03    3           23         1        -1             
      1.25895380e-03    3           23         3        -3             
      1.25895380e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.77664879e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.34099240e-05    2           13       -13                       
      9.56893620e-03    2           15       -15                       
      3.91661310e-04    2            3        -3                       
      9.65788660e-02    2            5        -5                       
      1.60742180e-01    2            6        -6                       
      8.46497570e-05    2           21        21                       
      5.44785750e-04    2           24       -24                       
      2.76599110e-04    2           23        23                       
      2.56489010e-03    2      1000022   1000022                       
      1.85555850e-02    2      1000022   1000023                       
      6.75664250e-02    2      1000022   1000025                       
      1.61163530e-02    2      1000022   1000035                       
      2.29986180e-02    2      1000023   1000023                       
      1.51098400e-01    2      1000023   1000025                       
      1.14308440e-02    2      1000023   1000035                       
      5.24550640e-02    2      1000024  -1000024                       
      1.93330020e-01    2      1000024  -1000037                       
      1.93330020e-01    2     -1000024   1000037                       
      2.15101060e-03    2           25        25                       
      6.17516930e-05    2      2000011  -2000011                       
      6.17389380e-05    2      2000013  -2000013                       
      5.81586650e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.36414594e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.50754450e-05    2           13       -13                       
      1.00460820e-02    2           15       -15                       
      4.11210150e-04    2            3        -3                       
      1.01488590e-01    2            5        -5                       
      1.79436160e-01    2            6        -6                       
      1.06785710e-04    2           21        21                       
      3.54570500e-03    2      1000022   1000022                       
      3.37407180e-02    2      1000022   1000023                       
      2.65683660e-02    2      1000022   1000025                       
      4.79420010e-02    2      1000022   1000035                       
      6.91348310e-02    2      1000023   1000023                       
      2.38820540e-02    2      1000023   1000025                       
      8.67740510e-02    2      1000023   1000035                       
      1.54549390e-01    2      1000024  -1000024                       
      1.30892950e-01    2      1000024  -1000037                       
      1.30892950e-01    2     -1000024   1000037                       
      5.53132330e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.50769933e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.47373210e-05    2           14       -13                       
      9.94923990e-03    2           16       -15                       
      3.77540390e-04    2            4        -3                       
      2.46057870e-01    2            6        -5                       
      5.02116720e-02    2      1000024   1000022                       
      1.71922640e-04    2      1000024   1000023                       
      2.02512800e-01    2      1000024   1000025                       
      2.00710650e-01    2      1000024   1000035                       
      5.71563280e-02    2      1000037   1000022                       
      2.32262160e-01    2      1000037   1000023                       
      5.55065810e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
