# SUSY Herwig++ jobOptions for pMSSM grid
# Use SUSYHIT and bottom-up schema for SLHA files 

from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.Stop')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

# setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_pMSSM_2688440_100FT_neut.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
sparticles['neutralinos234'] = ['~chi_20','~chi_30','~chi_40']
sparticles['charginos2']   = ['~chi_2+','~chi_2-']
sparticles['charginos1']   = ['~chi_1+','~chi_1-']

cmds = buildHerwigppCommands(['squarks','sbottoms','stops','neutralinos234','charginos2','gluino','sleptons','staus','sneutrinos'], slha_file)

# define metadata
evgenConfig.description = 'pMSSM grid generation' 
evgenConfig.keywords = ['SUSY','pMSSM']
evgenConfig.contact  = ['Takashi.Yamanaka@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# clean up
del cmds

#==============================================================
#
# End of job options file
#
###############################################################

