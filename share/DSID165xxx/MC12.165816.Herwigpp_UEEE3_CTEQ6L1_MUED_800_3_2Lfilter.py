include('MC12JobOptions/Herwigpp_UED_UEEE3_CTEQ6L1_MUED_P.py')

# 2 leptons filter
include ( 'MC12JobOptions/MultiLeptonFilter.py' )
topAlg.MultiLeptonFilter.Ptcut = 5000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
