#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19684600e+02   # h
        35     1.00710060e+03   # H
        36     1.00000000e+03   # A
        37     1.00966230e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99925680e+03   # b1
   1000006     2.83448070e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.63832300e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.36244800e+02   # N1
   1000023     3.91322100e+02   # N2
   1000024     3.88328400e+02   # C1
   1000025    -4.03220900e+02   # N3
   1000035     7.15654200e+02   # N4
   1000037     7.15637300e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00140040e+03   # b2
   2000006     3.16198580e+03   # t2
   2000011     2.63780600e+02   # eR
   2000013     2.63780700e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68789610e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376451e-01   # O_{11}
  1  2    -7.06837009e-01   # O_{12}
  2  1     7.06837009e-01   # O_{21}
  2  2     7.07376451e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.27768460e-01   # O_{11}
  1  2     7.78400129e-01   # O_{12}
  2  1    -7.78400129e-01   # O_{21}
  2  2     6.27768460e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.89192130e-01   # 
  1  2    -1.19884200e-02   # 
  1  3     1.31741630e-01   # 
  1  4    -6.32396500e-02   # 
  2  1    -1.37921530e-01   # 
  2  2    -2.04311950e-01   # 
  2  3     6.90632760e-01   # 
  2  4    -6.79897310e-01   # 
  3  1     4.75693600e-02   # 
  3  2    -4.26164300e-02   # 
  3  3    -7.01690440e-01   # 
  3  4    -7.09613620e-01   # 
  4  1     1.46158800e-02   # 
  4  2    -9.77904320e-01   # 
  4  3    -1.15328700e-01   # 
  4  4     1.73749860e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -1.62577020e-01   # Umix_{11}
  1  2     9.86695890e-01   # Umix_{12}
  2  1    -9.86695890e-01   # Umix_{21}
  2  2    -1.62577020e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -2.45551900e-01   # Vmix_{11}
  1  2     9.69383420e-01   # Vmix_{12}
  2  1    -9.69383420e-01   # Vmix_{21}
  2  2    -2.45551900e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     7.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.60300000e+02   # m_eR
    35     2.60300000e+02   # m_muR
    36     2.60500000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.40000000e+03   # A_e
  2  2     2.40000000e+03   # A_mu
  3  3     2.40000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.42153142e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37241400e-02    2      1000022         1                       
      1.14343780e-02    2      1000023         1                       
      9.37849570e-04    2      1000025         1                       
      3.15712030e-01    2      1000035         1                       
      1.88656360e-02    2     -1000024         2                       
      6.39325980e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.42741096e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05076510e-02    2      1000022         2                       
      1.87611390e-02    2      1000023         2                       
      4.08222320e-04    2      1000025         2                       
      3.11605990e-01    2      1000035         2                       
      4.29473590e-02    2      1000024         1                       
      6.15769680e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.42153142e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37241450e-02    2      1000022         3                       
      1.14343830e-02    2      1000023         3                       
      9.37850040e-04    2      1000025         3                       
      3.15712150e-01    2      1000035         3                       
      1.88656370e-02    2     -1000024         4                       
      6.39325860e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.42741096e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05076480e-02    2      1000022         4                       
      1.87611320e-02    2      1000023         4                       
      4.08222200e-04    2      1000025         4                       
      3.11605900e-01    2      1000035         4                       
      4.29473590e-02    2      1000024         3                       
      6.15769740e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.18432511e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.30909660e-02    2      1000022         5                       
      1.91644780e-02    2      1000023         5                       
      4.27545420e-03    2      1000025         5                       
      1.26049300e-01    2      1000035         5                       
      4.37219290e-01    2     -1000024         6                       
      2.82008440e-01    2     -1000037         6                       
      8.81920010e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.77788075e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.95993850e-02    2      1000022         6                       
      2.08197710e-01    2      1000023         6                       
      2.26176130e-01    2      1000025         6                       
      1.32682160e-01    2      1000035         6                       
      1.46421580e-01    2      1000024         5                       
      2.46923060e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79148030e-01    2      1000022         1                       
      1.84688290e-02    2      1000023         1                       
      2.19230630e-03    2      1000025         1                       
      1.90916170e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79148090e-01    2      1000022         2                       
      1.84687150e-02    2      1000023         2                       
      2.19229240e-03    2      1000025         2                       
      1.90911700e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79148030e-01    2      1000022         3                       
      1.84688290e-02    2      1000023         3                       
      2.19230630e-03    2      1000025         3                       
      1.90916170e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.79148090e-01    2      1000022         4                       
      1.84687170e-02    2      1000023         4                       
      2.19229240e-03    2      1000025         4                       
      1.90911680e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.77787456e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.60425040e-02    2      1000022         5                       
      5.60652740e-03    2      1000023         5                       
      7.73242770e-03    2      1000025         5                       
      1.42341720e-01    2      1000035         5                       
      4.17975310e-01    2     -1000024         6                       
      3.15328720e-01    2     -1000037         6                       
      9.49727820e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.02539336e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.53094200e-01    2      1000024         5                       
      5.31031270e-02    2      1000037         5                       
      1.70578750e-01    2           23   1000006                       
      2.61679930e-02    2           24   1000005                       
      3.82008110e-02    2           24   2000005                       
      4.48358250e-02    2      1000022         6                       
      1.87072220e-01    2      1000023         6                       
      1.94875400e-01    2      1000025         6                       
      3.20717470e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.76006855e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.45604670e-02    2      1000022        11                       
      2.54657590e-02    2      1000023        11                       
      8.78631690e-05    2      1000025        11                       
      2.81016110e-01    2      1000035        11                       
      1.71655030e-02    2     -1000024        12                       
      5.81704320e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.76307815e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.03163970e-01    2      1000022        12                       
      5.35343960e-03    2      1000023        12                       
      1.52946310e-03    2      1000025        12                       
      2.90051130e-01    2      1000035        12                       
      3.91131270e-02    2      1000024        11                       
      5.60788870e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.76006855e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.45604670e-02    2      1000022        13                       
      2.54657590e-02    2      1000023        13                       
      8.78631690e-05    2      1000025        13                       
      2.81016110e-01    2      1000035        13                       
      1.71655030e-02    2     -1000024        14                       
      5.81704320e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.76307815e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.03163970e-01    2      1000022        14                       
      5.35343960e-03    2      1000023        14                       
      1.52946310e-03    2      1000025        14                       
      2.90051130e-01    2      1000035        14                       
      3.91131270e-02    2      1000024        13                       
      5.60788870e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  7.05958063e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.79738072e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.02229190e-01    2      1000022        16                       
      5.30493210e-03    2      1000023        16                       
      1.51560460e-03    2      1000025        16                       
      2.87422960e-01    2      1000035        16                       
      4.42129670e-02    2      1000024        15                       
      5.55848660e-01    2      1000037        15                       
      3.46572860e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  7.05768818e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  7.05768818e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.79453476e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.38029070e-02    2      1000022        15                       
      2.79080870e-02    2      1000023        15                       
      2.84342630e-03    2      1000025        15                       
      2.78537120e-01    2      1000035        15                       
      1.70097260e-02    2     -1000024        16                       
      5.76425670e-01    2     -1000037        16                       
      1.73814160e-03    2           36   1000015                       
      1.73492680e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.93519934e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.26590850e-04    3      1000024         1        -2             
      5.26590850e-04    3     -1000024         2        -1             
      5.26590850e-04    3      1000024         3        -4             
      5.26590850e-04    3     -1000024         4        -3             
      9.66336950e-02    3      1000024         5        -6             
      9.66336950e-02    3     -1000024         6        -5             
      1.65795290e-02    3      1000037         1        -2             
      1.65795290e-02    3     -1000037         2        -1             
      1.65795290e-02    3      1000037         3        -4             
      1.65795290e-02    3     -1000037         4        -3             
      1.05333370e-01    3      1000037         5        -6             
      1.05333370e-01    3     -1000037         6        -5             
      5.20391480e-05    2      1000022        21                       
      3.69546870e-03    3      1000022         1        -1             
      3.69546870e-03    3      1000022         3        -3             
      3.68346620e-03    3      1000022         5        -5             
      2.79163640e-02    3      1000022         6        -6             
      1.04497640e-03    2      1000023        21                       
      7.46576460e-04    3      1000023         1        -1             
      7.46576460e-04    3      1000023         3        -3             
      1.68115120e-03    3      1000023         5        -5             
      1.61659730e-01    3      1000023         6        -6             
      1.15490540e-03    2      1000025        21                       
      5.33207200e-05    3      1000025         1        -1             
      5.33207200e-05    3      1000025         3        -3             
      1.04149300e-03    3      1000025         5        -5             
      1.73559500e-01    3      1000025         6        -6             
      1.85551590e-02    3      1000035         1        -1             
      1.85551590e-02    3      1000035         3        -3             
      1.84370560e-02    3      1000035         5        -5             
      9.13095180e-02    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  2.87398480e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.20373460e-01    2      1000022        23                       
      7.48597860e-01    2      1000022        25                       
      1.96091030e-02    2      2000011       -11                       
      1.96091030e-02    2     -2000011        11                       
      1.96090940e-02    2      2000013       -13                       
      1.96090940e-02    2     -2000013        13                       
      2.62961070e-02    2      1000015       -15                       
      2.62961070e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.79324393e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.77380970e-01    2      1000022        23                       
      8.89470880e-02    2      1000022        25                       
      2.71835990e-03    2      2000011       -11                       
      2.71835990e-03    2     -2000011        11                       
      2.71835850e-03    2      2000013       -13                       
      2.71835850e-03    2     -2000013        13                       
      1.13991620e-02    2      1000015       -15                       
      1.13991620e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.06356518e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.66020090e-01    2      1000024       -24                       
      2.66020090e-01    2     -1000024        24                       
      3.21455650e-04    2      1000022        23                       
      1.11840700e-02    2      1000023        23                       
      2.33561440e-01    2      1000025        23                       
      1.65360760e-04    2      1000022        25                       
      2.14403540e-01    2      1000023        25                       
      7.95166290e-03    2      1000025        25                       
      4.78813420e-05    2      2000011       -11                       
      4.78813420e-05    2     -2000011        11                       
      4.78813270e-05    2      2000013       -13                       
      4.78813270e-05    2     -2000013        13                       
      9.03890690e-05    2      1000015       -15                       
      9.03890690e-05    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.58340529e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.84424170e-01    2      1000022        24                       
      1.55758220e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  6.06300663e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.03772200e-04    2      1000022        24                       
      2.66883020e-01    2      1000023        24                       
      2.49398410e-01    2      1000025        24                       
      8.55252510e-05    2     -1000015        16                       
      2.60211470e-01    2      1000024        23                       
      2.23017920e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56164668e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96563170e-04    2           13       -13                       
      5.62245180e-02    2           15       -15                       
      2.35951580e-03    2            3        -3                       
      7.69133930e-01    2            5        -5                       
      3.34405230e-02    2            4        -4                       
      1.90488710e-03    2           22        22                       
      3.96443870e-02    2           21        21                       
      4.93436170e-03    3           24        11       -12             
      4.93436170e-03    3           24        13       -14             
      4.93436170e-03    3           24        15       -16             
      1.48030840e-02    3           24        -2         1             
      1.48030840e-02    3           24        -4         3             
      4.93436170e-03    3          -24       -11        12             
      4.93436170e-03    3          -24       -13        14             
      4.93436170e-03    3          -24       -15        16             
      1.48030840e-02    3          -24         2        -1             
      1.48030840e-02    3          -24         4        -3             
      5.66082480e-04    3           23        12       -12             
      5.66082480e-04    3           23        14       -14             
      5.66082480e-04    3           23        16       -16             
      2.84903650e-04    3           23        11       -11             
      2.84903650e-04    3           23        13       -13             
      2.84903650e-04    3           23        15       -15             
      9.76055690e-04    3           23         2        -2             
      9.76055690e-04    3           23         4        -4             
      1.25740010e-03    3           23         1        -1             
      1.25740010e-03    3           23         3        -3             
      1.25740010e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.17082571e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.19394260e-05    2           13       -13                       
      2.34682710e-02    2           15       -15                       
      9.60567850e-04    2            3        -3                       
      2.36864150e-01    2            5        -5                       
      3.94236360e-01    2            6        -6                       
      2.07611620e-04    2           21        21                       
      1.33741340e-03    2           24       -24                       
      6.79032700e-04    2           23        23                       
      1.04743860e-02    2      1000022   1000022                       
      8.71383030e-02    2      1000022   1000023                       
      1.98082720e-01    2      1000022   1000025                       
      8.27266720e-04    2      1000022   1000035                       
      3.53357960e-03    2      1000023   1000023                       
      3.49678470e-03    2      1000023   1000025                       
      1.78669270e-03    2      1000025   1000025                       
      3.11129780e-02    2      1000024  -1000024                       
      5.29334280e-03    2           25        25                       
      1.42389460e-04    2      2000011  -2000011                       
      1.42360090e-04    2      2000013  -2000013                       
      1.34083460e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.55207771e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.27184670e-05    2           13       -13                       
      2.08275520e-02    2           15       -15                       
      8.52521450e-04    2            3        -3                       
      2.10406300e-01    2            5        -5                       
      3.72007340e-01    2            6        -6                       
      2.21388300e-04    2           21        21                       
      1.38685730e-02    2      1000022   1000022                       
      1.91469610e-01    2      1000022   1000023                       
      6.55688570e-02    2      1000022   1000025                       
      1.18103730e-03    2      1000022   1000035                       
      1.53209640e-02    2      1000023   1000023                       
      1.00100970e-04    2      1000023   1000025                       
      2.19446700e-03    2      1000025   1000025                       
      1.04760770e-01    2      1000024  -1000024                       
      1.14787040e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.96981456e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.78148640e-05    2           14       -13                       
      2.51513650e-02    2           16       -15                       
      9.54410240e-04    2            4        -3                       
      6.22026500e-01    2            6        -5                       
      3.15386240e-01    2      1000024   1000022                       
      8.97533070e-03    2      1000024   1000023                       
      2.33817140e-02    2      1000024   1000025                       
      2.63216440e-03    2      1000037   1000022                       
      1.40455470e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
