#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19509200e+02   # h
        35     5.04374200e+02   # H
        36     5.00000000e+02   # A
        37     5.09428600e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99925680e+03   # b1
   1000006     2.83448070e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.91908400e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     2.41608600e+02   # N1
   1000023     3.42250400e+02   # N2
   1000024     3.37279200e+02   # C1
   1000025    -4.03762500e+02   # N3
   1000035     4.69903400e+02   # N4
   1000037     4.68165000e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00140040e+03   # b2
   2000006     3.16198580e+03   # t2
   2000011     2.91941000e+02   # eR
   2000013     2.91941000e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80313100e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376451e-01   # O_{11}
  1  2    -7.06837009e-01   # O_{12}
  2  1     7.06837009e-01   # O_{21}
  2  2     7.07376451e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.27768460e-01   # O_{11}
  1  2     7.78400129e-01   # O_{12}
  2  1    -7.78400129e-01   # O_{21}
  2  2     6.27768460e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1    -9.61027920e-01   # 
  1  2     9.26347500e-02   # 
  1  3    -2.13364970e-01   # 
  1  4     1.49397790e-01   # 
  2  1     2.45424450e-01   # 
  2  2     7.13303150e-01   # 
  2  3    -4.89243660e-01   # 
  2  4     4.37728170e-01   # 
  3  1    -3.95593700e-02   # 
  3  2     5.85431900e-02   # 
  3  3     7.00702250e-01   # 
  3  4     7.09946870e-01   # 
  4  1     1.20943700e-01   # 
  4  2    -6.92235590e-01   # 
  4  3    -4.73426640e-01   # 
  4  4     5.31083760e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -6.64803500e-01   # Umix_{11}
  1  2     7.47018340e-01   # Umix_{12}
  2  1    -7.47018340e-01   # Umix_{21}
  2  2    -6.64803500e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -7.47018100e-01   # Vmix_{11}
  1  2     6.64803680e-01   # Vmix_{12}
  2  1    -6.64803680e-01   # Vmix_{21}
  2  2    -7.47018100e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     4.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.88800000e+02   # m_eR
    35     2.88800000e+02   # m_muR
    36     2.88900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.40000000e+03   # A_e
  2  2     2.40000000e+03   # A_mu
  3  3     2.40000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.68842813e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.44039110e-02    2      1000022         1                       
      1.49000610e-01    2      1000023         1                       
      1.42898000e-03    2      1000025         1                       
      1.66293430e-01    2      1000035         1                       
      2.95080330e-01    2     -1000024         2                       
      3.63792780e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.69443197e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.34810470e-03    2      1000022         2                       
      1.91409930e-01    2      1000023         2                       
      8.66924650e-04    2      1000025         2                       
      1.46006060e-01    2      1000035         2                       
      3.71828880e-01    2      1000024         1                       
      2.87540140e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.68842813e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.44039170e-02    2      1000022         3                       
      1.49000640e-01    2      1000023         3                       
      1.42898030e-03    2      1000025         3                       
      1.66293470e-01    2      1000035         3                       
      2.95080330e-01    2     -1000024         4                       
      3.63792720e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.69443197e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.34810430e-03    2      1000022         4                       
      1.91409860e-01    2      1000023         4                       
      8.66924470e-04    2      1000025         4                       
      1.46006020e-01    2      1000035         4                       
      3.71828910e-01    2      1000024         3                       
      2.87540170e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.28836930e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.96264920e-02    2      1000022         5                       
      9.16449200e-02    2      1000023         5                       
      3.77214540e-03    2      1000025         5                       
      5.17341760e-02    2      1000035         5                       
      3.67110670e-01    2     -1000024         6                       
      3.50711020e-01    2     -1000037         6                       
      8.54005440e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.94670185e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.62956170e-02    2      1000022         6                       
      7.06599350e-02    2      1000023         6                       
      2.18716710e-01    2      1000025         6                       
      2.83820390e-01    2      1000035         6                       
      2.26915650e-03    2      1000024         5                       
      3.98238240e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67127949e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.24824950e-01    2      1000022         1                       
      5.95246370e-02    2      1000023         1                       
      1.53059860e-03    2      1000025         1                       
      1.41198830e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.24825250e-01    2      1000022         2                       
      5.95244800e-02    2      1000023         2                       
      1.53059160e-03    2      1000025         2                       
      1.41197790e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67127949e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.24824950e-01    2      1000022         3                       
      5.95246370e-02    2      1000023         3                       
      1.53059860e-03    2      1000025         3                       
      1.41198830e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.24825190e-01    2      1000022         4                       
      5.95244800e-02    2      1000023         4                       
      1.53059150e-03    2      1000025         4                       
      1.41197790e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.93995797e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.73660780e-02    2      1000022         5                       
      5.69094380e-02    2      1000023         5                       
      8.09023810e-03    2      1000025         5                       
      9.45080970e-02    2      1000035         5                       
      2.89091440e-01    2     -1000024         6                       
      4.42177650e-01    2     -1000037         6                       
      9.18570760e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.03493821e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.01824690e-01    2      1000024         5                       
      7.78052020e-03    2      1000037         5                       
      1.69004800e-01    2           23   1000006                       
      2.59265380e-02    2           24   1000005                       
      3.78483310e-02    2           24   2000005                       
      5.96893280e-02    2      1000022         6                       
      1.59465130e-01    2      1000023         6                       
      1.95053470e-01    2      1000025         6                       
      4.34072840e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.02371928e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.87753770e-02    2      1000022        11                       
      2.19963790e-01    2      1000023        11                       
      4.09799800e-04    2      1000025        11                       
      1.16946020e-01    2      1000035        11                       
      2.70463020e-01    2     -1000024        12                       
      3.33442000e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.02691955e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.19274770e-01    2      1000022        12                       
      1.02174220e-01    2      1000023        12                       
      1.94824230e-03    2      1000025        12                       
      1.71717380e-01    2      1000035        12                       
      3.41105190e-01    2      1000024        11                       
      2.63780270e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.02371928e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.87753770e-02    2      1000022        13                       
      2.19963790e-01    2      1000023        13                       
      4.09799800e-04    2      1000025        13                       
      1.16946020e-01    2      1000035        13                       
      2.70463020e-01    2     -1000024        14                       
      3.33442000e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.02691955e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.19274770e-01    2      1000022        14                       
      1.02174220e-01    2      1000023        14                       
      1.94824230e-03    2      1000025        14                       
      1.71717380e-01    2      1000035        14                       
      3.41105190e-01    2      1000024        13                       
      2.63780270e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.35767327e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.06271218e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.18227460e-01    2      1000022        16                       
      1.01277070e-01    2      1000023        16                       
      1.93113550e-03    2      1000025        16                       
      1.70209590e-01    2      1000035        16                       
      3.41050510e-01    2      1000024        15                       
      2.63753830e-01    2      1000037        15                       
      3.55040680e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.36152080e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.36152080e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.05945479e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.85032550e-02    2      1000022        15                       
      2.19290910e-01    2      1000023        15                       
      2.97492700e-03    2      1000025        15                       
      1.17079820e-01    2      1000035        15                       
      2.68085510e-01    2     -1000024        16                       
      3.30510850e-01    2     -1000037        16                       
      1.77801460e-03    2           36   1000015                       
      1.77668640e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.01364457e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.60002260e-03    3      1000024         1        -2             
      8.60002260e-03    3     -1000024         2        -1             
      8.60002260e-03    3      1000024         3        -4             
      8.60002260e-03    3     -1000024         4        -3             
      3.37537640e-02    3      1000024         5        -6             
      3.37537640e-02    3     -1000024         6        -5             
      1.03711550e-02    3      1000037         1        -2             
      1.03711550e-02    3     -1000037         2        -1             
      1.03711550e-02    3      1000037         3        -4             
      1.03711550e-02    3     -1000037         4        -3             
      1.68733490e-01    3      1000037         5        -6             
      1.68733490e-01    3     -1000037         6        -5             
      1.28595130e-04    2      1000022        21                       
      4.14289160e-03    3      1000022         1        -1             
      4.14289160e-03    3      1000022         3        -3             
      4.17382220e-03    3      1000022         5        -5             
      1.98803860e-02    3      1000022         6        -6             
      5.73902740e-04    2      1000023        21                       
      9.49309300e-03    3      1000023         1        -1             
      9.49309300e-03    3      1000023         3        -3             
      9.83819740e-03    3      1000023         5        -5             
      5.74749930e-02    3      1000023         6        -6             
      1.11729770e-03    2      1000025        21                       
      7.90781120e-05    3      1000025         1        -1             
      7.90781120e-05    3      1000025         3        -3             
      1.02672120e-03    3      1000025         5        -5             
      1.65580330e-01    3      1000025         6        -6             
      3.55282620e-04    2      1000035        21                       
      1.04364080e-02    3      1000035         1        -1             
      1.04364080e-02    3      1000035         3        -3             
      1.08234770e-02    3      1000035         5        -5             
      1.99864910e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  2.63828764e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.03651590e-01    2      1000022        23                       
      2.21916140e-05    3      1000022         5        -5             
      1.47431150e-01    2      2000011       -11                       
      1.47431150e-01    2     -2000011        11                       
      1.47430870e-01    2      2000013       -13                       
      1.47430870e-01    2     -2000013        13                       
      1.53301120e-01    2      1000015       -15                       
      1.53301120e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.30325087e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.70393650e-03    3     -1000024         2        -1             
      5.67978830e-04    3     -1000024        12       -11             
      5.67978830e-04    3     -1000024        14       -13             
      1.70393650e-03    3      1000024         1        -2             
      5.67978830e-04    3      1000024        11       -12             
      5.67978830e-04    3      1000024        13       -14             
      1.70393650e-03    3     -1000024         4        -3             
      5.67978830e-04    3     -1000024        16       -15             
      1.70393650e-03    3      1000024         3        -4             
      5.67978830e-04    3      1000024        15       -16             
      9.47788300e-01    2      1000022        23                       
      2.25876090e-04    3      1000023         2        -2             
      2.91217580e-04    3      1000023         1        -1             
      2.91217580e-04    3      1000023         3        -3             
      2.25876090e-04    3      1000023         4        -4             
      2.73990180e-04    3      1000023         5        -5             
      6.60646260e-05    3      1000023        11       -11             
      6.60646260e-05    3      1000023        13       -13             
      6.53782700e-05    3      1000023        15       -15             
      1.31467370e-04    3      1000023        12       -12             
      1.31467370e-04    3      1000023        14       -14             
      1.31467370e-04    3      1000023        16       -16             
      1.59070730e-02    2      1000022        25                       
      1.58868950e-03    2      2000011       -11                       
      1.58868950e-03    2     -2000011        11                       
      1.58868900e-03    2      2000013       -13                       
      1.58868900e-03    2     -2000013        13                       
      8.91309980e-03    2      1000015       -15                       
      8.91309980e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  1.05748530e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.39638290e-01    2      1000024       -24                       
      4.39638290e-01    2     -1000024        24                       
      7.13311420e-03    2      1000022        23                       
      9.05384370e-05    3      1000025         2        -2             
      1.16768740e-04    3      1000025         1        -1             
      1.16768740e-04    3      1000025         3        -3             
      9.05384370e-05    3      1000025         4        -4             
      1.11304120e-04    3      1000025         5        -5             
      2.64876330e-05    3      1000025        11       -11             
      2.64876330e-05    3      1000025        13       -13             
      2.62691030e-05    3      1000025        15       -15             
      5.26945550e-05    3      1000025        12       -12             
      5.26945550e-05    3      1000025        14       -14             
      5.26945550e-05    3      1000025        16       -16             
      7.20330920e-02    2      1000022        25                       
      7.86112100e-04    2      1000023        25                       
      6.23182160e-03    2      2000011       -11                       
      6.23182160e-03    2     -2000011        11                       
      6.23181970e-03    2      2000013       -13                       
      6.23181970e-03    2     -2000013        13                       
      7.54014540e-03    2      1000015       -15                       
      7.54014540e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  2.58279705e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.83151380e-01    2      1000022        24                       
      1.68487000e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  8.63394286e-01   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.31514370e-02    2      1000022        24                       
      5.56747970e-01    2      1000023        24                       
      3.81860850e-04    3      1000025         2        -1             
      3.81860850e-04    3      1000025         4        -3             
      1.27282870e-04    3      1000025       -11        12             
      1.27282870e-04    3      1000025       -13        14             
      1.27283160e-04    3      1000025       -15        16             
      3.28727900e-03    2     -1000015        16                       
      4.04749510e-01    2      1000024        23                       
      9.18348550e-04    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.07087827e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01233040e-04    2           13       -13                       
      5.75600560e-02    2           15       -15                       
      2.41562540e-03    2            3        -3                       
      7.87587820e-01    2            5        -5                       
      2.99248830e-02    2            4        -4                       
      1.71736660e-03    2           22        22                       
      3.52069550e-02    2           21        21                       
      4.34195300e-03    3           24        11       -12             
      4.34195300e-03    3           24        13       -14             
      4.34195300e-03    3           24        15       -16             
      1.30258590e-02    3           24        -2         1             
      1.30258590e-02    3           24        -4         3             
      4.34195300e-03    3          -24       -11        12             
      4.34195300e-03    3          -24       -13        14             
      4.34195300e-03    3          -24       -15        16             
      1.30258590e-02    3          -24         2        -1             
      1.30258590e-02    3          -24         4        -3             
      4.94556790e-04    3           23        12       -12             
      4.94556790e-04    3           23        14       -14             
      4.94556790e-04    3           23        16       -16             
      2.48905450e-04    3           23        11       -11             
      2.48905450e-04    3           23        13       -13             
      2.48905450e-04    3           23        15       -15             
      8.52729020e-04    3           23         2        -2             
      8.52729020e-04    3           23         4        -4             
      1.09852510e-03    3           23         1        -1             
      1.09852510e-03    3           23         3        -3             
      1.09852510e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  9.06349403e-01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.42984080e-04    2           13       -13                       
      4.09497920e-02    2           15       -15                       
      1.68652430e-03    2            3        -3                       
      4.52526510e-01    2            5        -5                       
      1.88943650e-05    2            4        -4                       
      4.51735170e-01    2            6        -6                       
      6.50109840e-04    2           21        21                       
      9.09630860e-03    2           24       -24                       
      4.47167920e-03    2           23        23                       
      1.42520440e-03    2      1000022   1000022                       
      3.72968320e-02    2           25        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  1.08818570e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.18685450e-04    2           13       -13                       
      3.39924770e-02    2           15       -15                       
      1.40003380e-03    2            3        -3                       
      3.76144980e-01    2            5        -5                       
      1.31256510e-05    2            4        -4                       
      5.60240570e-01    2            6        -6                       
      4.68322800e-04    2           21        21                       
      2.11576780e-02    2      1000022   1000022                       
      6.46417630e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  8.82010050e-01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.49188940e-04    2           14       -13                       
      4.27289610e-02    2           16       -15                       
      1.62263940e-03    2            4        -3                       
      9.46849640e-01    2            6        -5                       
      8.64963610e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
