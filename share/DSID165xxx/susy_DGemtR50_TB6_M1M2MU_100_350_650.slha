#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19704600e+02   # h
        35     1.00709250e+03   # H
        36     1.00000000e+03   # A
        37     1.00966140e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99861180e+03   # b1
   1000006     2.83547170e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.18534900e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.85071000e+01   # N1
   1000023     3.38622300e+02   # N2
   1000024     3.38525900e+02   # C1
   1000025    -6.53069000e+02   # N3
   1000035     6.65939900e+02   # N4
   1000037     6.65834700e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00204490e+03   # b2
   2000006     3.16109720e+03   # t2
   2000011     2.18514600e+02   # eR
   2000013     2.18514600e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68785650e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377928e-01   # O_{11}
  1  2    -7.06835530e-01   # O_{12}
  2  1     7.06835530e-01   # O_{21}
  2  2     7.07377928e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.58745998e-01   # O_{11}
  1  2     7.52365410e-01   # O_{12}
  2  1    -7.52365410e-01   # O_{21}
  2  2     6.58745998e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.97130510e-01   # 
  1  2    -1.07689000e-02   # 
  1  3     7.15661600e-02   # 
  1  4    -2.22124600e-02   # 
  2  1    -2.56266600e-02   # 
  2  2    -9.77872970e-01   # 
  2  3     1.75455970e-01   # 
  2  4    -1.11009290e-01   # 
  3  1     3.42160000e-02   # 
  3  2    -4.67380900e-02   # 
  3  3    -7.03588190e-01   # 
  3  4    -7.08243370e-01   # 
  4  1    -6.24788700e-02   # 
  4  2     2.03627650e-01   # 
  4  3     6.84877810e-01   # 
  4  4    -6.96832420e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.68339560e-01   # Umix_{11}
  1  2     2.49636610e-01   # Umix_{12}
  2  1    -2.49636610e-01   # Umix_{21}
  2  2    -9.68339560e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.87372520e-01   # Vmix_{11}
  1  2     1.58415730e-01   # Vmix_{12}
  2  1    -1.58415730e-01   # Vmix_{21}
  2  2    -9.87372520e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     6.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     3.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.14300000e+02   # m_eR
    35     2.14300000e+02   # m_muR
    36     2.14500000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.90000000e+03   # A_e
  2  2     3.90000000e+03   # A_mu
  3  3     3.90000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.72011530e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26781960e-02    2      1000022         1                       
      3.13405840e-01    2      1000023         1                       
      8.65694430e-04    2      1000025         1                       
      1.41957640e-02    2      1000035         1                       
      6.20603440e-01    2     -1000024         2                       
      3.82510050e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.72601189e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.99270290e-03    2      1000022         2                       
      3.18852810e-01    2      1000023         2                       
      5.03592660e-04    2      1000025         2                       
      1.13115820e-02    2      1000035         2                       
      6.43966910e-01    2      1000024         1                       
      1.53724420e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.72011530e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.26781990e-02    2      1000022         3                       
      3.13405930e-01    2      1000023         3                       
      8.65694660e-04    2      1000025         3                       
      1.41957680e-02    2      1000035         3                       
      6.20603380e-01    2     -1000024         4                       
      3.82510010e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.72601189e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.99270190e-03    2      1000022         4                       
      3.18852750e-01    2      1000023         4                       
      5.03592540e-04    2      1000025         4                       
      1.13115810e-02    2      1000035         4                       
      6.43966970e-01    2      1000024         3                       
      1.53724450e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.50255428e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.59011780e-02    2      1000022         5                       
      1.56440080e-01    2      1000023         5                       
      3.81409210e-03    2      1000025         5                       
      2.20646010e-03    2      1000035         5                       
      3.17225640e-01    2     -1000024         6                       
      3.99466370e-01    2     -1000037         6                       
      8.49462380e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.64767907e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.64489240e-02    2      1000022         6                       
      5.79232280e-02    2      1000023         6                       
      2.12501660e-01    2      1000025         6                       
      2.79106890e-01    2      1000035         6                       
      1.11139940e-01    2      1000024         5                       
      2.92879340e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69098757e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94755270e-01    2      1000022         1                       
      6.41795110e-04    2      1000023         1                       
      1.06522650e-03    2      1000025         1                       
      3.53776030e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76318575e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94755330e-01    2      1000022         2                       
      6.41792140e-04    2      1000023         2                       
      1.06520540e-03    2      1000025         2                       
      3.53768750e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69098757e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94755270e-01    2      1000022         3                       
      6.41795110e-04    2      1000023         3                       
      1.06522650e-03    2      1000025         3                       
      3.53776030e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76318575e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94755330e-01    2      1000022         4                       
      6.41792080e-04    2      1000023         4                       
      1.06520550e-03    2      1000025         4                       
      3.53768700e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.52806824e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.99020500e-02    2      1000022         5                       
      1.37129740e-01    2      1000023         5                       
      7.61837560e-03    2      1000025         5                       
      1.93775110e-02    2      1000035         5                       
      2.78617230e-01    2     -1000024         6                       
      4.44476660e-01    2     -1000037         6                       
      9.28783570e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.00217732e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.80034440e-01    2      1000024         5                       
      1.30560700e-01    2      1000037         5                       
      1.71444420e-01    2           23   1000006                       
      2.93093680e-02    2           24   1000005                       
      3.51593900e-02    2           24   2000005                       
      4.01524190e-02    2      1000022         6                       
      8.99608140e-02    2      1000023         6                       
      1.89259350e-01    2      1000025         6                       
      1.34119120e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.05945479e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.96125440e-02    2      1000022        11                       
      2.98376110e-01    2      1000023        11                       
      2.20275320e-04    2      1000025        11                       
      8.05906490e-03    2      1000035        11                       
      5.68681600e-01    2     -1000024        12                       
      3.50504260e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.06246142e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.68342800e-02    2      1000022        12                       
      2.81354990e-01    2      1000023        12                       
      1.21146590e-03    2      1000025        12                       
      1.59059190e-02    2      1000035        12                       
      5.90595190e-01    2      1000024        11                       
      1.40982340e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.05945479e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.96125440e-02    2      1000022        13                       
      2.98376110e-01    2      1000023        13                       
      2.20275320e-04    2      1000025        13                       
      8.05906490e-03    2      1000035        13                       
      5.68681600e-01    2     -1000024        14                       
      3.50504260e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.06246142e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.68342800e-02    2      1000022        14                       
      2.81354990e-01    2      1000023        14                       
      1.21146590e-03    2      1000025        14                       
      1.59059190e-02    2      1000035        14                       
      5.90595190e-01    2      1000024        13                       
      1.40982340e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  7.01458975e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.11735268e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.55435860e-02    2      1000022        16                       
      2.77604820e-01    2      1000023        16                       
      1.19531840e-03    2      1000025        16                       
      1.56939100e-02    2      1000035        16                       
      5.83044050e-01    2      1000024        15                       
      1.84483160e-02    2      1000037        15                       
      8.47001370e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  7.01533739e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  7.01533739e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.11426428e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.84434800e-02    2      1000022        15                       
      2.94550960e-01    2      1000023        15                       
      2.62259130e-03    2      1000025        15                       
      1.02242010e-02    2      1000035        15                       
      5.61088860e-01    2     -1000024        16                       
      3.45824810e-02    2     -1000037        16                       
      4.24761790e-03    2           36   1000015                       
      4.23984370e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.83752094e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.99875460e-02    3      1000024         1        -2             
      1.99875460e-02    3     -1000024         2        -1             
      1.99875460e-02    3      1000024         3        -4             
      1.99875460e-02    3     -1000024         4        -3             
      6.21436540e-02    3      1000024         5        -6             
      6.21436540e-02    3     -1000024         6        -5             
      1.15156010e-03    3      1000037         1        -2             
      1.15156010e-03    3     -1000037         2        -1             
      1.15156010e-03    3      1000037         3        -4             
      1.15156010e-03    3     -1000037         4        -3             
      1.39556740e-01    3      1000037         5        -6             
      1.39556740e-01    3     -1000037         6        -5             
      3.07077210e-05    2      1000022        21                       
      3.92564760e-03    3      1000022         1        -1             
      3.92564760e-03    3      1000022         3        -3             
      3.89212670e-03    3      1000022         5        -5             
      3.14077030e-02    3      1000022         6        -6             
      1.54207910e-04    2      1000023        21                       
      2.16738080e-02    3      1000023         1        -1             
      2.16738080e-02    3      1000023         3        -3             
      2.15359090e-02    3      1000023         5        -5             
      4.14913450e-02    3      1000023         6        -6             
      1.14384770e-03    2      1000025        21                       
      4.83614130e-05    3      1000025         1        -1             
      4.83614130e-05    3      1000025         3        -3             
      9.75891250e-04    3      1000025         5        -5             
      1.55472350e-01    3      1000025         6        -6             
      9.04929820e-04    2      1000035        21                       
      9.73812650e-04    3      1000035         1        -1             
      9.73812650e-04    3      1000035         3        -3             
      1.85138320e-03    3      1000035         5        -5             
      1.99939130e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.27220365e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.76217480e-02    2      1000022        23                       
      8.01961660e-01    2      1000022        25                       
      1.51395110e-02    2      2000011       -11                       
      1.51395110e-02    2     -2000011        11                       
      1.51395040e-02    2      2000013       -13                       
      1.51395040e-02    2     -2000013        13                       
      2.49292780e-02    2      1000015       -15                       
      2.49292780e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  4.71929447e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04013400e-01    2      1000024       -24                       
      3.04013400e-01    2     -1000024        24                       
      7.77807910e-02    2      1000022        23                       
      2.75664060e-01    2      1000023        23                       
      2.00863870e-02    2      1000022        25                       
      1.25056870e-02    2      1000023        25                       
      3.24927650e-04    2      2000011       -11                       
      3.24927650e-04    2     -2000011        11                       
      3.24927560e-04    2      2000013       -13                       
      3.24927560e-04    2     -2000013        13                       
      2.31826790e-03    2      1000015       -15                       
      2.31826790e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.78899884e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.18102600e-01    2      1000024       -24                       
      3.18102600e-01    2     -1000024        24                       
      2.17376000e-02    2      1000022        23                       
      1.66429230e-02    2      1000023        23                       
      6.88012090e-02    2      1000022        25                       
      2.46239140e-01    2      1000023        25                       
      1.09918290e-03    2      2000011       -11                       
      1.09918290e-03    2     -2000011        11                       
      1.09918260e-03    2      2000013       -13                       
      1.09918260e-03    2     -2000013        13                       
      2.98859810e-03    2      1000015       -15                       
      2.98859810e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  1.24364667e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.78811380e-01    2      1000022        24                       
      2.11886410e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.74583604e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.77624970e-02    2      1000022        24                       
      3.26765810e-01    2      1000023        24                       
      3.84565750e-03    2     -1000015        16                       
      3.13817170e-01    2      1000024        23                       
      2.67808970e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56322795e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96518010e-04    2           13       -13                       
      5.62116280e-02    2           15       -15                       
      2.35896810e-03    2            3        -3                       
      7.68937230e-01    2            5        -5                       
      3.34335120e-02    2            4        -4                       
      1.90070350e-03    2           22        22                       
      3.96508240e-02    2           21        21                       
      4.94491680e-03    3           24        11       -12             
      4.94491680e-03    3           24        13       -14             
      4.94491680e-03    3           24        15       -16             
      1.48347510e-02    3           24        -2         1             
      1.48347510e-02    3           24        -4         3             
      4.94491680e-03    3          -24       -11        12             
      4.94491680e-03    3          -24       -13        14             
      4.94491680e-03    3          -24       -15        16             
      1.48347510e-02    3          -24         2        -1             
      1.48347510e-02    3          -24         4        -3             
      5.67753680e-04    3           23        12       -12             
      5.67753680e-04    3           23        14       -14             
      5.67753680e-04    3           23        16       -16             
      2.85744750e-04    3           23        11       -11             
      2.85744750e-04    3           23        13       -13             
      2.85744750e-04    3           23        15       -15             
      9.78937140e-04    3           23         2        -2             
      9.78937140e-04    3           23         4        -4             
      1.26111240e-03    3           23         1        -1             
      1.26111240e-03    3           23         3        -3             
      1.26111240e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.56612667e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.28574090e-05    2           13       -13                       
      2.08670910e-02    2           15       -15                       
      8.54100390e-04    2            3        -3                       
      2.10610780e-01    2            5        -5                       
      3.50522640e-01    2            6        -6                       
      1.84592370e-04    2           21        21                       
      1.18656770e-03    2           24       -24                       
      6.02445100e-04    2           23        23                       
      3.12617070e-03    2      1000022   1000022                       
      1.91522590e-02    2      1000022   1000023                       
      9.30655520e-02    2      1000022   1000025                       
      2.37361270e-02    2      1000022   1000035                       
      2.13205960e-02    2      1000023   1000023                       
      1.06106450e-01    2      1000023   1000025                       
      1.00380100e-04    2      1000023   1000035                       
      4.55090700e-02    2      1000024  -1000024                       
      4.89585100e-02    2      1000024  -1000037                       
      4.89585100e-02    2     -1000024   1000037                       
      4.67157830e-03    2           25        25                       
      1.33907160e-04    2      2000011  -2000011                       
      1.33879530e-04    2      2000013  -2000013                       
      1.26102150e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.30853524e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.80731130e-05    2           13       -13                       
      2.23611970e-02    2           15       -15                       
      9.15297190e-04    2            3        -3                       
      2.25899640e-01    2            5        -5                       
      3.99400200e-01    2            6        -6                       
      2.37690280e-04    2           21        21                       
      4.29409510e-03    2      1000022   1000022                       
      3.33398060e-02    2      1000022   1000023                       
      3.07096390e-02    2      1000022   1000025                       
      8.22695050e-02    2      1000022   1000035                       
      6.36536110e-02    2      1000023   1000023                       
      7.61202830e-04    2      1000023   1000025                       
      1.34850380e-01    2      1000024  -1000024                       
      1.22969390e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  3.36864732e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.74175790e-05    2           14       -13                       
      2.21734460e-02    2           16       -15                       
      8.41408040e-04    2            4        -3                       
      5.48378590e-01    2            6        -5                       
      5.18390760e-02    2      1000024   1000022                       
      3.93139200e-05    2      1000024   1000023                       
      1.30197200e-01    2      1000024   1000025                       
      6.96256240e-02    2      1000024   1000035                       
      1.00545940e-01    2      1000037   1000022                       
      7.50465390e-02    2      1000037   1000023                       
      1.23554050e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
