#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19670500e+02   # h
        35     1.00710440e+03   # H
        36     1.00000000e+03   # A
        37     1.00966260e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99963530e+03   # b1
   1000006     2.83388600e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.90639700e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.30364000e+02   # N1
   1000023     2.50931600e+02   # N2
   1000024     2.41986600e+02   # C1
   1000025    -2.54157500e+02   # N3
   1000035     6.62862000e+02   # N4
   1000037     6.62856100e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00102220e+03   # b2
   2000006     3.16251900e+03   # t2
   2000011     1.90645700e+02   # eR
   2000013     1.90645700e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68792780e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07375610e-01   # O_{11}
  1  2    -7.06837850e-01   # O_{12}
  2  1     7.06837850e-01   # O_{21}
  2  2     7.07375610e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     5.79686359e-01   # O_{11}
  1  2     8.14839693e-01   # O_{12}
  2  1    -8.14839693e-01   # O_{21}
  2  2     5.79686359e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.49968160e-01   # 
  1  2    -3.20510500e-02   # 
  1  3     2.62511220e-01   # 
  1  4    -1.66195940e-01   # 
  2  1    -3.05003820e-01   # 
  2  2    -1.54258830e-01   # 
  2  3     6.63637880e-01   # 
  2  4    -6.65403490e-01   # 
  3  1     6.59957400e-02   # 
  3  2    -5.23453800e-02   # 
  3  3    -6.96412560e-01   # 
  3  4    -7.12680700e-01   # 
  4  1     1.33325200e-02   # 
  4  2    -9.86122310e-01   # 
  4  3    -7.53778500e-02   # 
  4  4     1.47321330e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -1.06163010e-01   # Umix_{11}
  1  2     9.94348760e-01   # Umix_{12}
  2  1    -9.94348760e-01   # Umix_{21}
  2  2    -1.06163010e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -2.08332540e-01   # Vmix_{11}
  1  2     9.78058040e-01   # Vmix_{12}
  2  1    -9.78058040e-01   # Vmix_{21}
  2  2    -2.08332540e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     2.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     6.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.85800000e+02   # m_eR
    35     1.85800000e+02   # m_muR
    36     1.86000000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.50000000e+03   # A_e
  2  2     1.50000000e+03   # A_mu
  3  3     1.50000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.47518479e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.53780510e-02    2      1000022         1                       
      3.46847300e-03    2      1000023         1                       
      1.48739750e-03    2      1000025         1                       
      3.21345510e-01    2      1000035         1                       
      8.08591770e-03    2     -1000024         2                       
      6.50234700e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.48125033e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.28449830e-03    2      1000022         2                       
      1.57919400e-02    2      1000023         2                       
      5.79292710e-04    2      1000025         2                       
      3.17502110e-01    2      1000035         2                       
      3.10742350e-02    2      1000024         1                       
      6.27767920e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.47518479e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.53780550e-02    2      1000022         3                       
      3.46847410e-03    2      1000023         3                       
      1.48739790e-03    2      1000025         3                       
      3.21345630e-01    2      1000035         3                       
      8.08591680e-03    2     -1000024         4                       
      6.50234640e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.48125033e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.28449740e-03    2      1000022         4                       
      1.57919340e-02    2      1000023         4                       
      5.79292540e-04    2      1000025         4                       
      3.17501960e-01    2      1000035         4                       
      3.10742410e-02    2      1000024         3                       
      6.27768040e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  2.80646399e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.58498800e-02    2      1000022         5                       
      1.20409030e-02    2      1000023         5                       
      4.10104870e-03    2      1000025         5                       
      1.28067810e-01    2      1000035         5                       
      4.34738160e-01    2     -1000024         6                       
      2.77920100e-01    2     -1000037         6                       
      8.72821360e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.93689136e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.96561230e-02    2      1000022         6                       
      2.27665960e-01    2      1000023         6                       
      2.26235670e-01    2      1000025         6                       
      1.22736950e-01    2      1000035         6                       
      1.61674410e-01    2      1000024         5                       
      2.32030810e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.03353810e-01    2      1000022         1                       
      9.21709610e-02    2      1000023         1                       
      4.31376930e-03    2      1000025         1                       
      1.61597070e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.03353810e-01    2      1000022         2                       
      9.21707750e-02    2      1000023         2                       
      4.31376040e-03    2      1000025         2                       
      1.61593840e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.03353810e-01    2      1000022         3                       
      9.21709610e-02    2      1000023         3                       
      4.31376930e-03    2      1000025         3                       
      1.61597070e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.03353870e-01    2      1000022         4                       
      9.21707900e-02    2      1000023         4                       
      4.31376090e-03    2      1000025         4                       
      1.61593850e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.30122423e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05445270e-02    2      1000022         5                       
      6.94058930e-03    2      1000023         5                       
      7.86525850e-03    2      1000025         5                       
      1.42971130e-01    2      1000035         5                       
      4.28737220e-01    2     -1000024         6                       
      3.08679370e-01    2     -1000037         6                       
      9.42618850e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.04125799e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.44560840e-01    2      1000024         5                       
      6.18207570e-02    2      1000037         5                       
      1.69776930e-01    2           23   1000006                       
      2.20548290e-02    2           24   1000005                       
      4.21484900e-02    2           24   2000005                       
      6.18470200e-02    2      1000022         6                       
      1.63207410e-01    2      1000023         6                       
      1.99562590e-01    2      1000025         6                       
      3.50211300e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.81388342e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.92297870e-02    2      1000022        11                       
      3.38332430e-02    2      1000023        11                       
      8.43209270e-05    2      1000025        11                       
      2.87031350e-01    2      1000035        11                       
      7.36747960e-03    2     -1000024        12                       
      5.92453840e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.81697982e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01196820e-01    2      1000022        12                       
      5.83684780e-05    2      1000023        12                       
      2.56036900e-03    2      1000025        12                       
      2.95335170e-01    2      1000035        12                       
      2.83393260e-02    2      1000024        11                       
      5.72509940e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.81388342e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.92297870e-02    2      1000022        13                       
      3.38332430e-02    2      1000023        13                       
      8.43209270e-05    2      1000025        13                       
      2.87031350e-01    2      1000035        13                       
      7.36747960e-03    2     -1000024        14                       
      5.92453840e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.81697982e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.01196820e-01    2      1000022        14                       
      5.83684780e-05    2      1000023        14                       
      2.56036900e-03    2      1000025        14                       
      2.95335170e-01    2      1000035        14                       
      2.83393260e-02    2      1000024        13                       
      5.72509940e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  2.47741644e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.84372810e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00488920e-01    2      1000022        16                       
      5.79601730e-05    2      1000023        16                       
      2.54245850e-03    2      1000025        16                       
      2.93269220e-01    2      1000035        16                       
      3.37304620e-02    2      1000024        15                       
      5.68566320e-01    2      1000037        15                       
      1.34458300e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  2.48049746e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  2.48049746e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.84081228e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.90691080e-02    2      1000022        15                       
      3.60855530e-02    2      1000023        15                       
      2.82593230e-03    2      1000025        15                       
      2.85051790e-01    2      1000035        15                       
      7.31587130e-03    2     -1000024        16                       
      5.88304340e-01    2     -1000037        16                       
      6.74315840e-04    2           36   1000015                       
      6.73085740e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.05008410e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.20688950e-04    3      1000024         1        -2             
      2.20688950e-04    3     -1000024         2        -1             
      2.20688950e-04    3      1000024         3        -4             
      2.20688950e-04    3     -1000024         4        -3             
      1.02259520e-01    3      1000024         5        -6             
      1.02259520e-01    3     -1000024         6        -5             
      1.64041370e-02    3      1000037         1        -2             
      1.64041370e-02    3     -1000037         2        -1             
      1.64041370e-02    3      1000037         3        -4             
      1.64041370e-02    3     -1000037         4        -3             
      9.99817770e-02    3      1000037         5        -6             
      9.99817770e-02    3     -1000037         6        -5             
      1.37519280e-04    2      1000022        21                       
      3.38058430e-03    3      1000022         1        -1             
      3.38058430e-03    3      1000022         3        -3             
      3.45928480e-03    3      1000022         5        -5             
      2.25601490e-02    3      1000022         6        -6             
      9.18858040e-04    2      1000023        21                       
      4.59462520e-04    3      1000023         1        -1             
      4.59462520e-04    3      1000023         3        -3             
      1.32406600e-03    3      1000023         5        -5             
      1.74835100e-01    3      1000023         6        -6             
      1.13360940e-03    2      1000025        21                       
      8.73194730e-05    3      1000025         1        -1             
      8.73194730e-05    3      1000025         3        -3             
      1.05157670e-03    3      1000025         5        -5             
      1.75991880e-01    3      1000025         6        -6             
      1.82582790e-02    3      1000035         1        -1             
      1.82582790e-02    3      1000035         3        -3             
      1.81336460e-02    3      1000035         5        -5             
      8.51012840e-02    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  9.18427148e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.14748360e-01    2      1000022        23                       
      1.80347310e-01    2      1000022        25                       
      1.15532520e-01    2      2000011       -11                       
      1.15532520e-01    2     -2000011        11                       
      1.15532280e-01    2      2000013       -13                       
      1.15532280e-01    2     -2000013        13                       
      1.21387380e-01    2      1000015       -15                       
      1.21387380e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  7.09909832e-02   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.26412940e-01    2      1000022        23                       
      2.46799600e-03    2      1000022        25                       
      7.58460820e-03    2      2000011       -11                       
      7.58460820e-03    2     -2000011        11                       
      7.58459420e-03    2      2000013       -13                       
      7.58459420e-03    2     -2000013        13                       
      2.03903390e-02    2      1000015       -15                       
      2.03903390e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  6.12108249e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.60709230e-01    2      1000024       -24                       
      2.60709230e-01    2     -1000024        24                       
      3.14307220e-04    2      1000022        23                       
      2.39748430e-02    2      1000023        23                       
      2.26964220e-01    2      1000025        23                       
      4.18753740e-03    2      1000022        25                       
      2.02209980e-01    2      1000023        25                       
      2.06465570e-02    2      1000025        25                       
      4.11890690e-05    2      2000011       -11                       
      4.11890690e-05    2     -2000011        11                       
      4.11890690e-05    2      2000013       -13                       
      4.11890690e-05    2     -2000013        13                       
      5.98704430e-05    2      1000015       -15                       
      5.98704430e-05    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  6.70736057e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.81152360e-01    2      1000022        24                       
      1.88476670e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  6.11880636e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.27931610e-03    2      1000022        24                       
      2.53495990e-01    2      1000023        24                       
      2.54725660e-01    2      1000025        24                       
      3.77175240e-05    2     -1000015        16                       
      2.56740540e-01    2      1000024        23                       
      2.29720850e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56038246e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96596970e-04    2           13       -13                       
      5.62341700e-02    2           15       -15                       
      2.35992600e-03    2            3        -3                       
      7.69280490e-01    2            5        -5                       
      3.34456560e-02    2            4        -4                       
      1.89834830e-03    2           22        22                       
      3.96399240e-02    2           21        21                       
      4.92694830e-03    3           24        11       -12             
      4.92694830e-03    3           24        13       -14             
      4.92694830e-03    3           24        15       -16             
      1.47808440e-02    3           24        -2         1             
      1.47808440e-02    3           24        -4         3             
      4.92694830e-03    3          -24       -11        12             
      4.92694830e-03    3          -24       -13        14             
      4.92694830e-03    3          -24       -15        16             
      1.47808440e-02    3          -24         2        -1             
      1.47808440e-02    3          -24         4        -3             
      5.64908030e-04    3           23        12       -12             
      5.64908030e-04    3           23        14       -14             
      5.64908030e-04    3           23        16       -16             
      2.84312550e-04    3           23        11       -11             
      2.84312550e-04    3           23        13       -13             
      2.84312550e-04    3           23        15       -15             
      9.74030700e-04    3           23         2        -2             
      9.74030700e-04    3           23         4        -4             
      1.25479150e-03    3           23         1        -1             
      1.25479150e-03    3           23         3        -3             
      1.25479150e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  6.48153619e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.00846290e-05    2           13       -13                       
      1.14806380e-02    2           15       -15                       
      4.69908210e-04    2            3        -3                       
      1.15873500e-01    2            5        -5                       
      1.92867310e-01    2            6        -6                       
      1.01567010e-04    2           21        21                       
      6.55407490e-04    2           24       -24                       
      3.32764060e-04    2           23        23                       
      1.92938610e-02    2      1000022   1000022                       
      4.55686190e-02    2      1000022   1000023                       
      1.07390930e-01    2      1000022   1000025                       
      5.29238580e-03    2      1000022   1000035                       
      4.48854190e-05    2      1000023   1000023                       
      1.95986500e-03    2      1000023   1000025                       
      1.29063460e-02    2      1000023   1000035                       
      4.29581620e-03    2      1000025   1000025                       
      1.33875280e-01    2      1000025   1000035                       
      3.07728780e-02    2      1000024  -1000024                       
      1.56975060e-01    2      1000024  -1000037                       
      1.56975060e-01    2     -1000024   1000037                       
      2.60531620e-03    2           25        25                       
      7.56880040e-05    2      2000011  -2000011                       
      7.56723820e-05    2      2000013  -2000013                       
      7.12786930e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  6.40583942e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.03227060e-05    2           13       -13                       
      1.15489670e-02    2           15       -15                       
      4.72726820e-04    2            3        -3                       
      1.16671200e-01    2            5        -5                       
      2.06279670e-01    2            6        -6                       
      1.22760760e-04    2           21        21                       
      3.19294000e-02    2      1000022   1000022                       
      1.04607090e-01    2      1000022   1000023                       
      4.32507510e-02    2      1000022   1000025                       
      1.28092200e-02    2      1000022   1000035                       
      1.18740250e-04    2      1000023   1000023                       
      2.39693070e-03    2      1000023   1000025                       
      1.16733670e-01    2      1000023   1000035                       
      2.88533840e-03    2      1000025   1000025                       
      1.25702310e-02    2      1000025   1000035                       
      5.63754480e-02    2      1000024  -1000024                       
      1.40275020e-01    2      1000024  -1000037                       
      1.40275020e-01    2     -1000024   1000037                       
      6.37615970e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  6.22705771e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.18792810e-05    2           14       -13                       
      1.19947940e-02    2           16       -15                       
      4.55162340e-04    2            4        -3                       
      2.96647130e-01    2            6        -5                       
      1.65494500e-01    2      1000024   1000022                       
      2.56050750e-02    2      1000024   1000023                       
      8.46465400e-03    2      1000024   1000025                       
      1.64282040e-01    2      1000024   1000035                       
      2.02743800e-02    2      1000037   1000022                       
      1.52730990e-01    2      1000037   1000023                       
      1.53338520e-01    2      1000037   1000025                       
      6.71013840e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
