#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19702500e+02   # h
        35     1.00709420e+03   # H
        36     1.00000000e+03   # A
        37     1.00966150e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99874150e+03   # b1
   1000006     2.83527340e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.70116100e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.37912000e+02   # N1
   1000023     2.02258100e+02   # N2
   1000024     2.02000200e+02   # C1
   1000025    -6.03594400e+02   # N3
   1000035     6.13424300e+02   # N4
   1000037     6.13377300e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00191530e+03   # b2
   2000006     3.16127510e+03   # t2
   2000011     1.70050900e+02   # eR
   2000013     1.70050900e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68786480e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377504e-01   # O_{11}
  1  2    -7.06835955e-01   # O_{12}
  2  1     7.06835955e-01   # O_{21}
  2  2     7.07377504e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.54614345e-01   # O_{11}
  1  2     7.55963001e-01   # O_{12}
  2  1    -7.55963001e-01   # O_{21}
  2  2     6.54614345e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.94294580e-01   # 
  1  2    -5.23976200e-02   # 
  1  3     8.68002100e-02   # 
  1  4    -3.31481800e-02   # 
  2  1    -6.72491300e-02   # 
  2  2    -9.83948710e-01   # 
  2  3     1.49193480e-01   # 
  2  4    -7.11603800e-02   # 
  3  1     3.46742700e-02   # 
  3  2    -5.76595600e-02   # 
  3  3    -7.02548560e-01   # 
  3  4    -7.08448050e-01   # 
  4  1     7.51920200e-02   # 
  4  2    -1.60545500e-01   # 
  4  3    -6.90385580e-01   # 
  4  4     7.01383950e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.76038580e-01   # Umix_{11}
  1  2     2.17597480e-01   # Umix_{12}
  2  1    -2.17597480e-01   # Umix_{21}
  2  2    -9.76038580e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.94551000e-01   # Vmix_{11}
  1  2     1.04251210e-01   # Vmix_{12}
  2  1    -1.04251210e-01   # Vmix_{21}
  2  2    -9.94551000e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     6.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     2.10000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.64600000e+02   # m_eR
    35     1.64600000e+02   # m_muR
    36     1.64900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.60000000e+03   # A_e
  2  2     3.60000000e+03   # A_mu
  3  3     3.60000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.78515153e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.82803590e-02    2      1000022         1                       
      3.12148000e-01    2      1000023         1                       
      1.25881370e-03    2      1000025         1                       
      9.30965880e-03    2      1000035         1                       
      6.29991170e-01    2     -1000024         2                       
      2.90119440e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.79125626e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.58392890e-03    2      1000022         2                       
      3.27539740e-01    2      1000023         2                       
      8.07082980e-04    2      1000025         2                       
      6.58618610e-03    2      1000035         2                       
      6.52837040e-01    2      1000024         1                       
      6.64597520e-03    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.78515153e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.82803650e-02    2      1000022         3                       
      3.12148090e-01    2      1000023         3                       
      1.25881400e-03    2      1000025         3                       
      9.30966160e-03    2      1000035         3                       
      6.29991110e-01    2     -1000024         4                       
      2.90119410e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.79125626e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.58392660e-03    2      1000022         4                       
      3.27539620e-01    2      1000023         4                       
      8.07082800e-04    2      1000025         4                       
      6.58618570e-03    2      1000035         4                       
      6.52837100e-01    2      1000024         3                       
      6.64597660e-03    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.51396081e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.93088120e-02    2      1000022         5                       
      1.54287040e-01    2      1000023         5                       
      3.51686500e-03    2      1000025         5                       
      1.59207550e-03    2      1000035         5                       
      3.14032200e-01    2     -1000024         6                       
      4.02996360e-01    2     -1000037         6                       
      8.42666700e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.77390472e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.21840060e-02    2      1000022         6                       
      6.86561390e-02    2      1000023         6                       
      2.10633550e-01    2      1000025         6                       
      2.73208560e-01    2      1000035         6                       
      1.32061200e-01    2      1000024         5                       
      2.73256600e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89169000e-01    2      1000022         1                       
      4.50292140e-03    2      1000023         1                       
      1.11225000e-03    2      1000025         1                       
      5.21587440e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74876191e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89169060e-01    2      1000022         2                       
      4.50291720e-03    2      1000023         2                       
      1.11223190e-03    2      1000025         2                       
      5.21578730e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89169000e-01    2      1000022         3                       
      4.50292140e-03    2      1000023         3                       
      1.11225000e-03    2      1000025         3                       
      5.21587440e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74876191e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.89169060e-01    2      1000022         4                       
      4.50291670e-03    2      1000023         4                       
      1.11223180e-03    2      1000025         4                       
      5.21578690e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.63553771e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.12916680e-02    2      1000022         5                       
      1.38553720e-01    2      1000023         5                       
      8.10323380e-03    2      1000025         5                       
      1.59453130e-02    2      1000035         5                       
      2.80830060e-01    2     -1000024         6                       
      4.43537710e-01    2     -1000037         6                       
      9.17382170e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.01163488e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.63632330e-01    2      1000024         5                       
      1.47424590e-01    2      1000037         5                       
      1.70450060e-01    2           23   1000006                       
      2.87056920e-02    2           24   1000005                       
      3.54342720e-02    2           24   2000005                       
      4.05443800e-02    2      1000022         6                       
      8.11434910e-02    2      1000023         6                       
      1.91440670e-01    2      1000025         6                       
      1.41224580e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.12380177e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.44462530e-02    2      1000022        11                       
      3.16294100e-01    2      1000023        11                       
      4.20149910e-04    2      1000025        11                       
      3.99641600e-03    2      1000035        11                       
      5.78215720e-01    2     -1000024        12                       
      2.66273410e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.12690451e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.09254240e-01    2      1000022        12                       
      2.71845580e-01    2      1000023        12                       
      1.65756490e-03    2      1000025        12                       
      1.14454920e-02    2      1000035        12                       
      5.99692170e-01    2      1000024        11                       
      6.10488700e-03    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.12380177e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.44462530e-02    2      1000022        13                       
      3.16294100e-01    2      1000023        13                       
      4.20149910e-04    2      1000025        13                       
      3.99641600e-03    2      1000035        13                       
      5.78215720e-01    2     -1000024        14                       
      2.66273410e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.12690451e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.09254240e-01    2      1000022        14                       
      2.71845580e-01    2      1000023        14                       
      1.65756490e-03    2      1000025        14                       
      1.14454920e-02    2      1000035        14                       
      5.99692170e-01    2      1000024        13                       
      6.10488700e-03    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  1.00225362e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.17719109e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07943780e-01    2      1000022        16                       
      2.68584910e-01    2      1000023        16                       
      1.63768310e-03    2      1000025        16                       
      1.13082080e-02    2      1000035        16                       
      5.92744170e-01    2      1000024        15                       
      1.06459860e-02    2      1000037        15                       
      7.13515100e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  1.00170451e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  1.00170451e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.17401230e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.35915450e-02    2      1000022        15                       
      3.12609230e-01    2      1000023        15                       
      2.81327430e-03    2      1000025        15                       
      6.25984840e-03    2      1000035        15                       
      5.71268800e-01    2     -1000024        16                       
      2.63074430e-02    2     -1000037        16                       
      3.57813040e-03    2           36   1000015                       
      3.57163490e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.89459140e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.03434510e-02    3      1000024         1        -2             
      2.03434510e-02    3     -1000024         2        -1             
      2.03434510e-02    3      1000024         3        -4             
      2.03434510e-02    3     -1000024         4        -3             
      6.77115400e-02    3      1000024         5        -6             
      6.77115400e-02    3     -1000024         6        -5             
      8.73660670e-04    3      1000037         1        -2             
      8.73660670e-04    3     -1000037         2        -1             
      8.73660670e-04    3      1000037         3        -4             
      8.73660670e-04    3     -1000037         4        -3             
      1.33917560e-01    3      1000037         5        -6             
      1.33917560e-01    3     -1000037         6        -5             
      3.87618380e-05    2      1000022        21                       
      4.19925810e-03    3      1000022         1        -1             
      4.19925810e-03    3      1000022         3        -3             
      4.16629710e-03    3      1000022         5        -5             
      2.87622570e-02    3      1000022         6        -6             
      1.12411580e-04    2      1000023        21                       
      2.11031620e-02    3      1000023         1        -1             
      2.11031620e-02    3      1000023         3        -3             
      2.09524760e-02    3      1000023         5        -5             
      4.80444580e-02    3      1000023         6        -6             
      1.13153990e-03    2      1000025        21                       
      6.97195860e-05    3      1000025         1        -1             
      6.97195860e-05    3      1000025         3        -3             
      9.92696620e-04    3      1000025         5        -5             
      1.55080930e-01    3      1000025         6        -6             
      9.20668010e-04    2      1000035        21                       
      6.43257280e-04    3      1000035         1        -1             
      6.43257280e-04    3      1000035         3        -3             
      1.53604250e-03    3      1000035         5        -5             
      1.98104020e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.21316008e-03   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.29172610e-05    3      1000022         2        -2             
      5.57863790e-05    3      1000022         1        -1             
      5.57863790e-05    3      1000022         3        -3             
      4.29172610e-05    3      1000022         4        -4             
      5.39497010e-05    3      1000022         5        -5             
      1.17179090e-05    3      1000022        11       -11             
      1.17179090e-05    3      1000022        13       -13             
      1.17752050e-05    3      1000022        15       -15             
      2.32339330e-05    3      1000022        12       -12             
      2.32339330e-05    3      1000022        14       -14             
      2.32339330e-05    3      1000022        16       -16             
      1.64746910e-01    2      2000011       -11                       
      1.64746910e-01    2     -2000011        11                       
      1.64745910e-01    2      2000013       -13                       
      1.64745910e-01    2     -2000013        13                       
      1.70329060e-01    2      1000015       -15                       
      1.70329060e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  4.65422147e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.05060800e-01    2      1000024       -24                       
      3.05060800e-01    2     -1000024        24                       
      9.12367630e-02    2      1000022        23                       
      2.47135580e-01    2      1000023        23                       
      1.69165710e-02    2      1000022        25                       
      2.85689640e-02    2      1000023        25                       
      3.36084220e-04    2      2000011       -11                       
      3.36084220e-04    2     -2000011        11                       
      3.36084220e-04    2      2000013       -13                       
      3.36084220e-04    2     -2000013        13                       
      2.33818660e-03    2      1000015       -15                       
      2.33818660e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.66775406e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.13140840e-01    2      1000024       -24                       
      3.13140840e-01    2     -1000024        24                       
      1.93411710e-02    2      1000022        23                       
      3.41946410e-02    2      1000023        23                       
      8.24185910e-02    2      1000022        25                       
      2.24228250e-01    2      1000023        25                       
      1.61027820e-03    2      2000011       -11                       
      1.61027820e-03    2     -2000011        11                       
      1.61027820e-03    2      2000013       -13                       
      1.61027820e-03    2     -2000013        13                       
      3.54741490e-03    2      1000015       -15                       
      3.54741490e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  5.02903423e-05   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.36622550e-01    3      1000022         2        -1             
      1.36622550e-01    3      1000022         4        -3             
      4.54600680e-02    3      1000022       -11        12             
      4.54600680e-02    3      1000022       -13        14             
      4.54600940e-02    3      1000022       -15        16             
      5.90374650e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.62186644e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.29958560e-02    2      1000022        24                       
      3.35919620e-01    2      1000023        24                       
      3.95516450e-03    2     -1000015        16                       
      3.11366320e-01    2      1000024        23                       
      2.75763060e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56322795e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96523720e-04    2           13       -13                       
      5.62132560e-02    2           15       -15                       
      2.35903710e-03    2            3        -3                       
      7.68961670e-01    2            5        -5                       
      3.34342420e-02    2            4        -4                       
      1.89723530e-03    2           22        22                       
      3.96501500e-02    2           21        21                       
      4.94381600e-03    3           24        11       -12             
      4.94381600e-03    3           24        13       -14             
      4.94381600e-03    3           24        15       -16             
      1.48314460e-02    3           24        -2         1             
      1.48314460e-02    3           24        -4         3             
      4.94381600e-03    3          -24       -11        12             
      4.94381600e-03    3          -24       -13        14             
      4.94381600e-03    3          -24       -15        16             
      1.48314460e-02    3          -24         2        -1             
      1.48314460e-02    3          -24         4        -3             
      5.67579350e-04    3           23        12       -12             
      5.67579350e-04    3           23        14       -14             
      5.67579350e-04    3           23        16       -16             
      2.85657010e-04    3           23        11       -11             
      2.85657010e-04    3           23        13       -13             
      2.85657010e-04    3           23        15       -15             
      9.78636560e-04    3           23         2        -2             
      9.78636560e-04    3           23         4        -4             
      1.26072510e-03    3           23         1        -1             
      1.26072510e-03    3           23         3        -3             
      1.26072510e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.42269436e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.50027190e-05    2           13       -13                       
      1.00251290e-02    2           15       -15                       
      4.10333480e-04    2            3        -3                       
      1.01183240e-01    2            5        -5                       
      1.68402520e-01    2            6        -6                       
      8.86840860e-05    2           21        21                       
      5.70323900e-04    2           24       -24                       
      2.89565270e-04    2           23        23                       
      2.33235140e-03    2      1000022   1000022                       
      1.16660900e-02    2      1000022   1000023                       
      6.20415580e-02    2      1000022   1000025                       
      1.53048600e-02    2      1000022   1000035                       
      1.43631330e-02    2      1000023   1000023                       
      1.51910110e-01    2      1000023   1000025                       
      2.68935260e-02    2      1000023   1000035                       
      3.38091850e-02    2      1000024  -1000024                       
      1.99114810e-01    2      1000024  -1000037                       
      1.99114810e-01    2     -1000024   1000037                       
      2.24711370e-03    2           25        25                       
      6.72116080e-05    2      2000011  -2000011                       
      6.71977250e-05    2      2000013  -2000013                       
      6.32923400e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.29525731e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.54066980e-05    2           13       -13                       
      1.01409570e-02    2           15       -15                       
      4.15093610e-04    2            3        -3                       
      1.02447050e-01    2            5        -5                       
      1.81130750e-01    2            6        -6                       
      1.07794200e-04    2           21        21                       
      3.29939810e-03    2      1000022   1000022                       
      1.77394670e-02    2      1000022   1000023                       
      1.76484060e-02    2      1000022   1000025                       
      5.44957030e-02    2      1000022   1000035                       
      2.38848660e-02    2      1000023   1000023                       
      3.12578160e-02    2      1000023   1000025                       
      1.31800470e-01    2      1000023   1000035                       
      5.59364330e-02    2      1000024  -1000024                       
      1.84551240e-01    2      1000024  -1000037                       
      1.84551240e-01    2     -1000024   1000037                       
      5.57931550e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.13859636e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.65333690e-05    2           14       -13                       
      1.04636510e-02    2           16       -15                       
      3.97060560e-04    2            4        -3                       
      2.58779910e-01    2            6        -5                       
      2.68372710e-02    2      1000024   1000022                       
      1.30325730e-04    2      1000024   1000023                       
      2.14783910e-01    2      1000024   1000025                       
      2.06441670e-01    2      1000024   1000035                       
      4.89351710e-02    2      1000037   1000022                       
      2.32611160e-01    2      1000037   1000023                       
      5.83320330e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
