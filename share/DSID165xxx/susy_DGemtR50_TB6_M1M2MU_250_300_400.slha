#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19509200e+02   # h
        35     5.04374200e+02   # H
        36     5.00000000e+02   # A
        37     5.09428600e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99925680e+03   # b1
   1000006     2.83448070e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     2.57712000e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     2.38763100e+02   # N1
   1000023     2.76753200e+02   # N2
   1000024     2.70750200e+02   # C1
   1000025    -4.04153900e+02   # N3
   1000035     4.38637800e+02   # N4
   1000037     4.35465300e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00140040e+03   # b2
   2000006     3.16198580e+03   # t2
   2000011     2.57763000e+02   # eR
   2000013     2.57763100e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80313100e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376451e-01   # O_{11}
  1  2    -7.06837009e-01   # O_{12}
  2  1     7.06837009e-01   # O_{21}
  2  2     7.07376451e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.27768460e-01   # O_{11}
  1  2     7.78400129e-01   # O_{12}
  2  1    -7.78400129e-01   # O_{21}
  2  2     6.27768460e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     8.96497490e-01   # 
  1  2    -2.99309850e-01   # 
  1  3     2.68302320e-01   # 
  1  4    -1.86331540e-01   # 
  2  1    -4.05954600e-01   # 
  2  2    -8.50012300e-01   # 
  2  3     2.65995380e-01   # 
  2  4    -2.04758780e-01   # 
  3  1    -3.95519800e-02   # 
  3  2     6.68522100e-02   # 
  3  3     6.99853780e-01   # 
  3  4     7.10050110e-01   # 
  4  1    -1.72998670e-01   # 
  4  2     4.28279460e-01   # 
  4  3     6.06188710e-01   # 
  4  4    -6.47444010e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -8.65035410e-01   # Umix_{11}
  1  2     5.01710770e-01   # Umix_{12}
  2  1    -5.01710770e-01   # Umix_{21}
  2  2    -8.65035410e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.23839210e-01   # Vmix_{11}
  1  2     3.82780640e-01   # Vmix_{12}
  2  1    -3.82780640e-01   # Vmix_{21}
  2  2    -9.23839210e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     3.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     2.54200000e+02   # m_eR
    35     2.54200000e+02   # m_muR
    36     2.54300000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.40000000e+03   # A_e
  2  2     2.40000000e+03   # A_mu
  3  3     2.40000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.74637145e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.15278160e-02    2      1000022         1                       
      1.99383230e-01    2      1000023         1                       
      1.78445490e-03    2      1000025         1                       
      6.83115350e-02    2      1000035         1                       
      4.96349160e-01    2     -1000024         2                       
      1.62643830e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.75256556e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.06033720e-03    2      1000022         2                       
      2.82625620e-01    2      1000023         2                       
      1.15238360e-03    2      1000025         2                       
      5.06788720e-02    2      1000035         2                       
      5.64998750e-01    2      1000024         1                       
      9.44839640e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.74637145e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.15278310e-02    2      1000022         3                       
      1.99383260e-01    2      1000023         3                       
      1.78445540e-03    2      1000025         3                       
      6.83115500e-02    2      1000035         3                       
      4.96349100e-01    2     -1000024         4                       
      1.62643820e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.75256556e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.06033630e-03    2      1000022         4                       
      2.82625560e-01    2      1000023         4                       
      1.15238340e-03    2      1000025         4                       
      5.06788720e-02    2      1000035         4                       
      5.64998920e-01    2      1000024         3                       
      9.44839860e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.31119831e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.30099900e-02    2      1000022         5                       
      1.07027260e-01    2      1000023         5                       
      3.48860890e-03    2      1000025         5                       
      1.41835950e-02    2      1000035         5                       
      3.25655040e-01    2     -1000024         6                       
      3.91821830e-01    2     -1000037         6                       
      8.48137290e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.98904179e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.22916690e-02    2      1000022         6                       
      6.15397100e-02    2      1000023         6                       
      2.15910780e-01    2      1000025         6                       
      3.08146120e-01    2      1000035         6                       
      3.69513300e-02    2      1000024         5                       
      3.65160440e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67127949e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.05042030e-01    2      1000022         1                       
      1.64350610e-01    2      1000023         1                       
      1.52991880e-03    2      1000025         1                       
      2.90774370e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68433720e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.05042270e-01    2      1000022         2                       
      1.64350520e-01    2      1000023         2                       
      1.52991180e-03    2      1000025         2                       
      2.90772620e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67127949e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.05042030e-01    2      1000022         3                       
      1.64350610e-01    2      1000023         3                       
      1.52991880e-03    2      1000025         3                       
      2.90774370e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68433720e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.05042270e-01    2      1000022         4                       
      1.64350520e-01    2      1000023         4                       
      1.52991160e-03    2      1000025         4                       
      2.90772580e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.97505669e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.37134860e-02    2      1000022         5                       
      8.58000290e-02    2      1000023         5                       
      8.46487470e-03    2      1000025         5                       
      5.00030670e-02    2      1000035         5                       
      2.57819380e-01    2     -1000024         6                       
      4.72996350e-01    2     -1000037         6                       
      9.12028180e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.03664971e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.42863940e-01    2      1000024         5                       
      6.73287660e-02    2      1000037         5                       
      1.68725880e-01    2           23   1000006                       
      2.58837490e-02    2           24   1000005                       
      3.77858650e-02    2           24   2000005                       
      7.22465370e-02    2      1000022         6                       
      1.01782050e-01    2      1000023         6                       
      1.96089010e-01    2      1000025         6                       
      8.72942730e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.08186047e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.14293690e-02    2      1000022        11                       
      3.50271760e-01    2      1000023        11                       
      6.07057010e-04    2      1000025        11                       
      3.28967240e-02    2      1000035        11                       
      4.55528020e-01    2     -1000024        12                       
      1.49267030e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.08515392e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.91429790e-01    2      1000022        12                       
      1.19396170e-01    2      1000023        12                       
      2.33826930e-03    2      1000025        12                       
      8.10701100e-02    2      1000035        12                       
      5.18978000e-01    2      1000024        11                       
      8.67876630e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.08186047e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.14293690e-02    2      1000022        13                       
      3.50271760e-01    2      1000023        13                       
      6.07057010e-04    2      1000025        13                       
      3.28967240e-02    2      1000035        13                       
      4.55528020e-01    2     -1000024        14                       
      1.49267030e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.08515392e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.91429790e-01    2      1000022        14                       
      1.19396170e-01    2      1000023        14                       
      2.33826930e-03    2      1000025        14                       
      8.10701100e-02    2      1000035        14                       
      5.18978000e-01    2      1000024        13                       
      8.67876630e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  2.09277924e-02   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.12070369e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.89771070e-01    2      1000022        16                       
      1.18361610e-01    2      1000023        16                       
      2.31800830e-03    2      1000025        16                       
      8.03676470e-02    2      1000035        16                       
      5.15798630e-01    2      1000024        15                       
      8.98745950e-02    2      1000037        15                       
      3.50842530e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  2.12432223e-02   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  2.12425367e-02   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.11761026e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.17109240e-02    2      1000022        15                       
      3.47602700e-01    2      1000023        15                       
      3.12791670e-03    2      1000025        15                       
      3.44971750e-02    2      1000035        15                       
      4.51576590e-01    2     -1000024        16                       
      1.47972260e-01    2     -1000037        16                       
      1.75696980e-03    2           36   1000015                       
      1.75566340e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.02953964e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.47049180e-02    3      1000024         1        -2             
      1.47049180e-02    3     -1000024         2        -1             
      1.47049180e-02    3      1000024         3        -4             
      1.47049180e-02    3     -1000024         4        -3             
      3.78357430e-02    3      1000024         5        -6             
      3.78357430e-02    3     -1000024         6        -5             
      4.70151520e-03    3      1000037         1        -2             
      4.70151520e-03    3     -1000037         2        -1             
      4.70151520e-03    3      1000037         3        -4             
      4.70151520e-03    3     -1000037         4        -3             
      1.64709310e-01    3      1000037         5        -6             
      1.64709310e-01    3     -1000037         6        -5             
      2.02269310e-04    2      1000022        21                       
      6.74861480e-03    3      1000022         1        -1             
      6.74861480e-03    3      1000022         3        -3             
      6.79842060e-03    3      1000022         5        -5             
      1.15638480e-02    3      1000022         6        -6             
      1.90137990e-04    2      1000023        21                       
      1.29960620e-02    3      1000023         1        -1             
      1.29960620e-02    3      1000023         3        -3             
      1.30011030e-02    3      1000023         5        -5             
      4.46823160e-02    3      1000023         6        -6             
      1.11344000e-03    2      1000025        21                       
      9.85193840e-05    3      1000025         1        -1             
      9.85193840e-05    3      1000025         3        -3             
      1.03693290e-03    3      1000025         5        -5             
      1.63356320e-01    3      1000025         6        -6             
      6.54818900e-04    2      1000035        21                       
      4.39080780e-03    3      1000035         1        -1             
      4.39080780e-03    3      1000035         3        -3             
      5.09229350e-03    3      1000035         5        -5             
      2.21124310e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.21906949e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.67108850e-01    2      2000011       -11                       
      1.67108850e-01    2     -2000011        11                       
      1.67105940e-01    2      2000013       -13                       
      1.67105940e-01    2     -2000013        13                       
      1.65785210e-01    2      1000015       -15                       
      1.65785210e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.28491947e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.01971550e-01    2      1000024       -24                       
      3.01971550e-01    2     -1000024        24                       
      2.86324350e-01    2      1000022        23                       
      9.75718350e-02    2      1000023        23                       
      5.27115400e-03    2      1000022        25                       
      2.07988460e-04    2      1000023        25                       
      4.40372240e-04    2      2000011       -11                       
      4.40372240e-04    2     -2000011        11                       
      4.40372010e-04    2      2000013       -13                       
      4.40372010e-04    2     -2000013        13                       
      2.45997400e-03    2      1000015       -15                       
      2.45997400e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  1.82534180e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.66379410e-01    2      1000024       -24                       
      3.66379410e-01    2     -1000024        24                       
      1.14911100e-02    2      1000022        23                       
      3.60165980e-03    2      1000023        23                       
      1.43014790e-01    2      1000022        25                       
      5.94752280e-02    2      1000023        25                       
      7.83922620e-03    2      2000011       -11                       
      7.83922620e-03    2     -2000011        11                       
      7.83922430e-03    2      2000013       -13                       
      7.83922430e-03    2     -2000013        13                       
      9.15074350e-03    2      1000015       -15                       
      9.15074350e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  4.06823660e-05   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.52212830e-01    3      1000022         2        -1             
      1.52212830e-01    3      1000022         4        -3             
      5.07322510e-02    3      1000022       -11        12             
      5.07322510e-02    3      1000022       -13        14             
      5.07323220e-02    3      1000022       -15        16             
      5.43377580e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  1.65851938e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.21953730e-03    2      1000022        24                       
      4.87456080e-01    2      1000023        24                       
      3.04559180e-03    2     -1000015        16                       
      3.55883270e-01    2      1000024        23                       
      1.51395530e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.07087827e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01232270e-04    2           13       -13                       
      5.75598370e-02    2           15       -15                       
      2.41561610e-03    2            3        -3                       
      7.87584780e-01    2            5        -5                       
      2.99247670e-02    2            4        -4                       
      1.72116750e-03    2           22        22                       
      3.52068210e-02    2           21        21                       
      4.34193620e-03    3           24        11       -12             
      4.34193620e-03    3           24        13       -14             
      4.34193620e-03    3           24        15       -16             
      1.30258090e-02    3           24        -2         1             
      1.30258090e-02    3           24        -4         3             
      4.34193620e-03    3          -24       -11        12             
      4.34193620e-03    3          -24       -13        14             
      4.34193620e-03    3          -24       -15        16             
      1.30258090e-02    3          -24         2        -1             
      1.30258090e-02    3          -24         4        -3             
      4.94554870e-04    3           23        12       -12             
      4.94554870e-04    3           23        14       -14             
      4.94554870e-04    3           23        16       -16             
      2.48904490e-04    3           23        11       -11             
      2.48904490e-04    3           23        13       -13             
      2.48904490e-04    3           23        15       -15             
      8.52725700e-04    3           23         2        -2             
      8.52725700e-04    3           23         4        -4             
      1.09852080e-03    3           23         1        -1             
      1.09852080e-03    3           23         3        -3             
      1.09852080e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  9.09782023e-01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.42444870e-04    2           13       -13                       
      4.07953710e-02    2           15       -15                       
      1.68016440e-03    2            3        -3                       
      4.50820000e-01    2            5        -5                       
      1.88231120e-05    2            4        -4                       
      4.50031670e-01    2            6        -6                       
      6.47658190e-04    2           21        21                       
      9.06200610e-03    2           24       -24                       
      4.45481620e-03    2           23        23                       
      5.19088750e-03    2      1000022   1000022                       
      3.71561830e-02    2           25        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  1.13340106e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.13949240e-04    2           13       -13                       
      3.26359870e-02    2           15       -15                       
      1.34416460e-03    2            3        -3                       
      3.61134680e-01    2            5        -5                       
      1.26018640e-05    2            4        -4                       
      5.37883760e-01    2            6        -6                       
      4.49634100e-04    2           21        21                       
      6.02189560e-02    2      1000022   1000022                       
      6.20621960e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  8.82010050e-01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.49188940e-04    2           14       -13                       
      4.27289610e-02    2           16       -15                       
      1.62263940e-03    2            4        -3                       
      9.46849640e-01    2            6        -5                       
      8.64963610e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
