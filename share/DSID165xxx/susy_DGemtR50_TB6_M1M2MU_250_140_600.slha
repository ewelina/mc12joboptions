#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19702500e+02   # h
        35     1.00709420e+03   # H
        36     1.00000000e+03   # A
        37     1.00966150e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99874150e+03   # b1
   1000006     2.83527340e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.90542600e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.33692200e+02   # N1
   1000023     2.47346800e+02   # N2
   1000024     1.33811900e+02   # C1
   1000025    -6.03733500e+02   # N3
   1000035     6.12694800e+02   # N4
   1000037     6.12070400e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00191530e+03   # b2
   2000006     3.16127510e+03   # t2
   2000011     1.90548200e+02   # eR
   2000013     1.90548300e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68786480e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377504e-01   # O_{11}
  1  2    -7.06835955e-01   # O_{12}
  2  1     7.06835955e-01   # O_{21}
  2  2     7.07377504e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.54614345e-01   # O_{11}
  1  2     7.55963001e-01   # O_{12}
  2  1    -7.55963001e-01   # O_{21}
  2  2     6.54614345e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     2.94365300e-02   # 
  1  2    -9.87523260e-01   # 
  1  3     1.44822610e-01   # 
  1  4    -5.43866900e-02   # 
  2  1     9.94265910e-01   # 
  2  2     4.47117500e-02   # 
  2  3     8.54133600e-02   # 
  2  4    -4.62678700e-02   # 
  3  1     3.02022300e-02   # 
  3  2    -6.30783300e-02   # 
  3  3    -7.02305320e-01   # 
  3  4    -7.08431960e-01   # 
  4  1     9.82679400e-02   # 
  4  2    -1.37186070e-01   # 
  4  3    -6.91735150e-01   # 
  4  4     7.02158030e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.79779420e-01   # Umix_{11}
  1  2     2.00080560e-01   # Umix_{12}
  2  1    -2.00080560e-01   # Umix_{21}
  2  2    -9.79779420e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.97130990e-01   # Vmix_{11}
  1  2     7.56954500e-02   # Vmix_{12}
  2  1    -7.56954500e-02   # Vmix_{21}
  2  2    -9.97130990e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     6.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     2.50000000e+02   # M1
     2     1.40000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.85700000e+02   # m_eR
    35     1.85700000e+02   # m_muR
    36     1.85900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.60000000e+03   # A_e
  2  2     3.60000000e+03   # A_mu
  3  3     3.60000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.80528415e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25922100e-01    2      1000022         1                       
      6.18512370e-03    2      1000023         1                       
      1.43851240e-03    2      1000025         1                       
      7.34084700e-03    2      1000035         1                       
      6.34705130e-01    2     -1000024         2                       
      2.44083650e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.81145405e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.18258760e-01    2      1000022         2                       
      1.68186200e-02    2      1000023         2                       
      1.00986940e-03    2      1000025         2                       
      4.32093580e-03    2      1000035         2                       
      6.56105280e-01    2      1000024         1                       
      3.48656690e-03    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.80528415e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25922130e-01    2      1000022         3                       
      6.18512420e-03    2      1000023         3                       
      1.43851250e-03    2      1000025         3                       
      7.34084800e-03    2      1000035         3                       
      6.34705010e-01    2     -1000024         4                       
      2.44083570e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.81145405e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.18258640e-01    2      1000022         4                       
      1.68186130e-02    2      1000023         4                       
      1.00986920e-03    2      1000025         4                       
      4.32093440e-03    2      1000035         4                       
      6.56105340e-01    2      1000024         3                       
      3.48656720e-03    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.52148093e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.61310270e-01    2      1000022         5                       
      3.27447840e-02    2      1000023         5                       
      3.45032710e-03    2      1000025         5                       
      1.28318950e-03    2      1000035         5                       
      3.13585430e-01    2     -1000024         6                       
      4.03541420e-01    2     -1000037         6                       
      8.40846370e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.78724633e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.92035630e-02    2      1000022         6                       
      4.39282580e-02    2      1000023         6                       
      2.09688650e-01    2      1000025         6                       
      2.70794510e-01    2      1000035         6                       
      1.44003390e-01    2      1000024         5                       
      2.62381520e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.67127949e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75554920e-04    2      1000022         1                       
      9.89276470e-01    2      1000023         1                       
      8.51937920e-04    2      1000025         1                       
      8.99614670e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.68440509e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75556670e-04    2      1000022         2                       
      9.89276470e-01    2      1000023         2                       
      8.51925640e-04    2      1000025         2                       
      8.99601360e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.67127949e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75554920e-04    2      1000022         3                       
      9.89276470e-01    2      1000023         3                       
      8.51937920e-04    2      1000025         3                       
      8.99614670e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.68440509e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.75556730e-04    2      1000022         4                       
      9.89276470e-01    2      1000023         4                       
      8.51925640e-04    2      1000025         4                       
      8.99601260e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.64666431e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.45126750e-01    2      1000022         5                       
      1.59805860e-02    2      1000023         5                       
      8.26454910e-03    2      1000025         5                       
      1.47387530e-02    2      1000035         5                       
      2.83056710e-01    2     -1000024         6                       
      4.41310940e-01    2     -1000037         6                       
      9.15217030e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.01172818e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.55166090e-01    2      1000024         5                       
      1.56017590e-01    2      1000037         5                       
      1.70433880e-01    2           23   1000006                       
      2.87029670e-02    2           24   1000005                       
      3.54309080e-02    2           24   2000005                       
      7.59376590e-02    2      1000022         6                       
      4.36533760e-02    2      1000023         6                       
      1.91881910e-01    2      1000025         6                       
      1.42775600e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.14066432e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.86626640e-01    2      1000022        11                       
      1.05151260e-01    2      1000023        11                       
      6.06597750e-04    2      1000025        11                       
      1.93773420e-03    2      1000035        11                       
      5.83248500e-01    2     -1000024        12                       
      2.24292870e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.14405339e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.05702540e-01    2      1000022        12                       
      7.56619500e-02    2      1000023        12                       
      1.78061230e-03    2      1000025        12                       
      1.02270900e-02    2      1000035        12                       
      6.03421210e-01    2      1000024        11                       
      3.20656600e-03    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.14066432e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.86626640e-01    2      1000022        13                       
      1.05151260e-01    2      1000023        13                       
      6.06597750e-04    2      1000025        13                       
      1.93773420e-03    2      1000035        13                       
      5.83248500e-01    2     -1000024        14                       
      2.24292870e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.14405339e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.05702540e-01    2      1000022        14                       
      7.56619500e-02    2      1000023        14                       
      1.78061230e-03    2      1000025        14                       
      1.02270900e-02    2      1000035        14                       
      6.03421210e-01    2      1000024        13                       
      3.20656600e-03    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  4.29186228e-04   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.64767680e-01    2      1000022        15                       
      3.35232320e-01    2     -1000024        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.19395947e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.02053060e-01    2      1000022        16                       
      7.47587010e-02    2      1000023        16                       
      1.75935530e-03    2      1000025        16                       
      1.01049990e-02    2      1000035        16                       
      5.96425410e-01    2      1000024        15                       
      7.80031270e-03    2      1000037        15                       
      7.09814020e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  2.16491794e-04   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  2.16491794e-04   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.19102197e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.83308240e-01    2      1000022        15                       
      1.03932040e-01    2      1000023        15                       
      2.98602110e-03    2      1000025        15                       
      4.22573510e-03    2      1000035        15                       
      5.76274220e-01    2     -1000024        16                       
      2.21611000e-02    2     -1000037        16                       
      3.55960850e-03    2           36   1000015                       
      3.55312600e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.89682997e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.06838660e-02    3      1000024         1        -2             
      2.06838660e-02    3     -1000024         2        -1             
      2.06838660e-02    3      1000024         3        -4             
      2.06838660e-02    3     -1000024         4        -3             
      7.11698530e-02    3      1000024         5        -6             
      7.11698530e-02    3     -1000024         6        -5             
      7.38301200e-04    3      1000037         1        -2             
      7.38301200e-04    3     -1000037         2        -1             
      7.38301200e-04    3      1000037         3        -4             
      7.38301200e-04    3     -1000037         4        -3             
      1.30576150e-01    3      1000037         5        -6             
      1.30576150e-01    3     -1000037         6        -5             
      1.08446080e-04    2      1000022        21                       
      2.19069700e-02    3      1000022         1        -1             
      2.19069700e-02    3      1000022         3        -3             
      2.17420090e-02    3      1000022         5        -5             
      4.79186210e-02    3      1000022         6        -6             
      3.52266800e-05    2      1000023        21                       
      3.40409510e-03    3      1000023         1        -1             
      3.40409510e-03    3      1000023         3        -3             
      3.37938100e-03    3      1000023         5        -5             
      3.00504960e-02    3      1000023         6        -6             
      1.13190380e-03    2      1000025        21                       
      7.89581250e-05    3      1000025         1        -1             
      7.89581250e-05    3      1000025         3        -3             
      1.00022000e-03    3      1000025         5        -5             
      1.54571340e-01    3      1000025         6        -6             
      9.26114390e-04    2      1000035        21                       
      5.23796250e-04    3      1000035         1        -1             
      5.23796250e-04    3      1000035         3        -3             
      1.41954620e-03    3      1000035         5        -5             
      1.96708230e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  6.24537432e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.09105130e-03    2      1000024       -24                       
      6.09105130e-03    2     -1000024        24                       
      3.43837800e-04    2      1000022        23                       
      1.64544240e-01    2      2000011       -11                       
      1.64544240e-01    2     -2000011        11                       
      1.64543880e-01    2      2000013       -13                       
      1.64543880e-01    2     -2000013        13                       
      1.64648890e-01    2      1000015       -15                       
      1.64648890e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  4.59252023e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.04770170e-01    2      1000024       -24                       
      3.04770170e-01    2     -1000024        24                       
      2.57974480e-01    2      1000022        23                       
      7.19806700e-02    2      1000023        23                       
      4.93279320e-02    2      1000022        25                       
      5.81520700e-03    2      1000023        25                       
      2.47225460e-04    2      2000011       -11                       
      2.47225460e-04    2     -2000011        11                       
      2.47225430e-04    2      2000013       -13                       
      2.47225430e-04    2     -2000013        13                       
      2.18628910e-03    2      1000015       -15                       
      2.18628910e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.60472926e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.08514120e-01    2      1000024       -24                       
      3.08514120e-01    2     -1000024        24                       
      5.62751110e-02    2      1000022        23                       
      7.31389410e-03    2      1000023        23                       
      2.33798550e-01    2      1000022        25                       
      6.58307970e-02    2      1000023        25                       
      2.66611300e-03    2      2000011       -11                       
      2.66611300e-03    2     -2000011        11                       
      2.66611300e-03    2      2000013       -13                       
      2.66611300e-03    2     -2000013        13                       
      4.54453240e-03    2      1000015       -15                       
      4.54453240e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.69920755e-17   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.00000000e-01    3      1000022       -11        12             
      5.00000000e-01    3      1000022       -13        14             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.54872149e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.00252910e-01    2      1000022        24                       
      1.08395300e-01    2      1000023        24                       
      3.86791090e-03    2     -1000015        16                       
      3.09376300e-01    2      1000024        23                       
      2.78107730e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56322795e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96518780e-04    2           13       -13                       
      5.62118440e-02    2           15       -15                       
      2.35897790e-03    2            3        -3                       
      7.68942360e-01    2            5        -5                       
      3.34334000e-02    2            4        -4                       
      1.92236760e-03    2           22        22                       
      3.96491510e-02    2           21        21                       
      4.94369170e-03    3           24        11       -12             
      4.94369170e-03    3           24        13       -14             
      4.94369170e-03    3           24        15       -16             
      1.48310740e-02    3           24        -2         1             
      1.48310740e-02    3           24        -4         3             
      4.94369170e-03    3          -24       -11        12             
      4.94369170e-03    3          -24       -13        14             
      4.94369170e-03    3          -24       -15        16             
      1.48310740e-02    3          -24         2        -1             
      1.48310740e-02    3          -24         4        -3             
      5.67565090e-04    3           23        12       -12             
      5.67565090e-04    3           23        14       -14             
      5.67565090e-04    3           23        16       -16             
      2.85649850e-04    3           23        11       -11             
      2.85649850e-04    3           23        13       -13             
      2.85649850e-04    3           23        15       -15             
      9.78611990e-04    3           23         2        -2             
      9.78611990e-04    3           23         4        -4             
      1.26069340e-03    3           23         1        -1             
      1.26069340e-03    3           23         3        -3             
      1.26069340e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.77747581e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.34062420e-05    2           13       -13                       
      9.56788190e-03    2           15       -15                       
      3.91618120e-04    2            3        -3                       
      9.65682640e-02    2            5        -5                       
      1.60721660e-01    2            6        -6                       
      8.46391920e-05    2           21        21                       
      5.44311300e-04    2           24       -24                       
      2.76358160e-04    2           23        23                       
      1.75734670e-02    2      1000022   1000022                       
      8.87734820e-03    2      1000022   1000023                       
      1.65860820e-01    2      1000022   1000025                       
      4.23833800e-02    2      1000022   1000035                       
      1.05942690e-03    2      1000023   1000023                       
      3.83381060e-02    2      1000023   1000025                       
      5.15204710e-03    2      1000023   1000035                       
      3.31437030e-02    2      1000024  -1000024                       
      2.08546680e-01    2      1000024  -1000037                       
      2.08546680e-01    2     -1000024   1000037                       
      2.14462280e-03    2           25        25                       
      6.30819560e-05    2      2000011  -2000011                       
      6.30689390e-05    2      2000013  -2000013                       
      5.94067310e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.66480733e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.36996420e-05    2           13       -13                       
      9.65203340e-03    2           15       -15                       
      3.95080800e-04    2            3        -3                       
      9.75077970e-02    2            5        -5                       
      1.72397940e-01    2            6        -6                       
      1.02597140e-04    2           21        21                       
      2.45664180e-02    2      1000022   1000022                       
      1.41141050e-02    2      1000022   1000023                       
      4.79023570e-02    2      1000022   1000025                       
      1.47214870e-01    2      1000022   1000035                       
      2.02771420e-03    2      1000023   1000023                       
      5.89886120e-03    2      1000023   1000025                       
      3.31048930e-02    2      1000023   1000035                       
      4.62195690e-02    2      1000024  -1000024                       
      1.99165540e-01    2      1000024  -1000037                       
      1.99165540e-01    2     -1000024   1000037                       
      5.31032100e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.49692469e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.47870050e-05    2           14       -13                       
      9.96346860e-03    2           16       -15                       
      3.78080320e-04    2            4        -3                       
      2.46409770e-01    2            6        -5                       
      2.07760590e-05    2      1000024   1000022                       
      2.66571120e-02    2      1000024   1000023                       
      2.24034820e-01    2      1000024   1000025                       
      2.13509320e-01    2      1000024   1000035                       
      2.20686170e-01    2      1000037   1000022                       
      5.77502060e-02    2      1000037   1000023                       
      5.55436530e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
