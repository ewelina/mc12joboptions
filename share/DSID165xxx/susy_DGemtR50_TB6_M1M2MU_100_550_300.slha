#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19675100e+02   # h
        35     1.00710330e+03   # H
        36     1.00000000e+03   # A
        37     1.00966250e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99951050e+03   # b1
   1000006     2.83408420e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.92786100e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     9.51908000e+01   # N1
   1000023     2.90380000e+02   # N2
   1000024     2.86468000e+02   # C1
   1000025    -3.04248400e+02   # N3
   1000035     5.68677900e+02   # N4
   1000037     5.68657800e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00114700e+03   # b2
   2000006     3.16234130e+03   # t2
   2000011     1.92790400e+02   # eR
   2000013     1.92790400e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68791730e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07375822e-01   # O_{11}
  1  2    -7.06837638e-01   # O_{12}
  2  1     7.06837638e-01   # O_{21}
  2  2     7.07375822e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.00851298e-01   # O_{11}
  1  2     7.99360818e-01   # O_{12}
  2  1    -7.99360818e-01   # O_{21}
  2  2     6.00851298e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.81554210e-01   # 
  1  2    -1.88842100e-02   # 
  1  3     1.72860590e-01   # 
  1  4    -7.94573000e-02   # 
  2  1    -1.78826420e-01   # 
  2  2    -2.38589560e-01   # 
  2  3     6.82752490e-01   # 
  2  4    -6.67042370e-01   # 
  3  1     6.41960800e-02   # 
  3  2    -5.52725400e-02   # 
  3  3    -6.97595950e-01   # 
  3  4    -7.11465840e-01   # 
  4  1     2.12324700e-02   # 
  4  2    -9.69362500e-01   # 
  4  3    -1.31637130e-01   # 
  4  4     2.06294860e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -1.85300990e-01   # Umix_{11}
  1  2     9.82681810e-01   # Umix_{12}
  2  1    -9.82681810e-01   # Umix_{21}
  2  2    -1.85300990e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -2.91625500e-01   # Vmix_{11}
  1  2     9.56532600e-01   # Vmix_{12}
  2  1    -9.56532600e-01   # Vmix_{21}
  2  2    -2.91625500e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     3.00000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.00000000e+02   # M1
     2     5.50000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.88000000e+02   # m_eR
    35     1.88000000e+02   # m_muR
    36     1.88200000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.80000000e+03   # A_e
  2  2     1.80000000e+03   # A_mu
  3  3     1.80000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.57212634e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.39331420e-02    2      1000022         1                       
      1.46997210e-02    2      1000023         1                       
      1.55627440e-03    2      1000025         1                       
      3.11281830e-01    2      1000035         1                       
      2.38401140e-02    2     -1000024         2                       
      6.34688970e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.57814624e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.12149720e-03    2      1000022         2                       
      2.54965780e-02    2      1000023         2                       
      6.54361730e-04    2      1000025         2                       
      3.05688950e-01    2      1000035         2                       
      5.89274020e-02    2      1000024         1                       
      6.00111130e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.57212634e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.39331450e-02    2      1000022         3                       
      1.46997240e-02    2      1000023         3                       
      1.55627460e-03    2      1000025         3                       
      3.11281890e-01    2      1000035         3                       
      2.38401110e-02    2     -1000024         4                       
      6.34688910e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.57814624e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.12149530e-03    2      1000022         4                       
      2.54965720e-02    2      1000023         4                       
      6.54361560e-04    2      1000025         4                       
      3.05688890e-01    2      1000035         4                       
      5.89274130e-02    2      1000024         3                       
      6.00111310e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.01968161e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.85783180e-02    2      1000022         5                       
      2.23661660e-02    2      1000023         5                       
      3.69983240e-03    2      1000025         5                       
      1.23623890e-01    2      1000035         5                       
      4.26341890e-01    2     -1000024         6                       
      2.88893250e-01    2     -1000037         6                       
      8.64966660e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.96507937e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.62285380e-02    2      1000022         6                       
      1.97355690e-01    2      1000023         6                       
      2.22081900e-01    2      1000025         6                       
      1.48020400e-01    2      1000035         6                       
      1.26585930e-01    2      1000024         5                       
      2.69727590e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.69094412e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.64066510e-01    2      1000022         1                       
      3.14659660e-02    2      1000023         1                       
      4.04753860e-03    2      1000025         1                       
      4.20117870e-04    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.76311625e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.64066510e-01    2      1000022         2                       
      3.14658660e-02    2      1000023         2                       
      4.04752280e-03    2      1000025         2                       
      4.20111640e-04    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.69094412e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.64066510e-01    2      1000022         3                       
      3.14659660e-02    2      1000023         3                       
      4.04753860e-03    2      1000025         3                       
      4.20117870e-04    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.76311625e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.64066510e-01    2      1000022         4                       
      3.14658620e-02    2      1000023         4                       
      4.04752230e-03    2      1000025         4                       
      4.20111670e-04    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.15911585e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.33804180e-02    2      1000022         5                       
      6.88760770e-03    2      1000023         5                       
      8.18814240e-03    2      1000025         5                       
      1.43508320e-01    2      1000035         5                       
      4.04834390e-01    2     -1000024         6                       
      3.30142140e-01    2     -1000037         6                       
      9.30590700e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.04082988e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.63830810e-01    2      1000024         5                       
      4.39884590e-02    2      1000037         5                       
      1.69246320e-01    2           23   1000006                       
      2.36744450e-02    2           24   1000005                       
      4.02822230e-02    2           24   2000005                       
      4.63906900e-02    2      1000022         6                       
      1.84457090e-01    2      1000023         6                       
      1.98504520e-01    2      1000025         6                       
      2.96255240e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.91157069e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.73299610e-02    2      1000022        11                       
      3.59582600e-02    2      1000023        11                       
      1.26368050e-04    2      1000025        11                       
      2.75236760e-01    2      1000035        11                       
      2.17702260e-02    2     -1000024        12                       
      5.79578460e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.91459498e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00347390e-01    2      1000022        12                       
      6.23009590e-03    2      1000023        12                       
      2.59055590e-03    2      1000025        12                       
      2.88475480e-01    2      1000035        12                       
      5.38595840e-02    2      1000024        11                       
      5.48496960e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.91157069e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.73299610e-02    2      1000022        13                       
      3.59582600e-02    2      1000023        13                       
      1.26368050e-04    2      1000025        13                       
      2.75236760e-01    2      1000035        13                       
      2.17702260e-02    2     -1000024        14                       
      5.79578460e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.91459498e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00347390e-01    2      1000022        14                       
      6.23009590e-03    2      1000023        14                       
      2.59055590e-03    2      1000025        14                       
      2.88475480e-01    2      1000035        14                       
      5.38595840e-02    2      1000024        13                       
      5.48496960e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  5.40039383e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  3.94367885e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.96087570e-02    2      1000022        16                       
      6.18423800e-03    2      1000023        16                       
      2.57148760e-03    2      1000025        16                       
      2.86352070e-01    2      1000035        16                       
      5.87544030e-02    2      1000024        15                       
      5.44642030e-01    2      1000037        15                       
      1.88693770e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  5.40261019e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  5.40261019e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  3.94060947e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.68531910e-02    2      1000022        15                       
      3.82486690e-02    2      1000023        15                       
      2.79048250e-03    2      1000025        15                       
      2.73300290e-01    2      1000035        15                       
      2.16097570e-02    2     -1000024        16                       
      5.75306650e-01    2     -1000037        16                       
      9.46303070e-04    2           36   1000015                       
      9.44576580e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.04759683e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      6.66255660e-04    3      1000024         1        -2             
      6.66255660e-04    3     -1000024         2        -1             
      6.66255660e-04    3      1000024         3        -4             
      6.66255660e-04    3     -1000024         4        -3             
      8.81917850e-02    3      1000024         5        -6             
      8.81917850e-02    3     -1000024         6        -5             
      1.68691560e-02    3      1000037         1        -2             
      1.68691560e-02    3     -1000037         2        -1             
      1.68691560e-02    3      1000037         3        -4             
      1.68691560e-02    3     -1000037         4        -3             
      1.14194780e-01    3      1000037         5        -6             
      1.14194780e-01    3     -1000037         6        -5             
      6.12110600e-05    2      1000022        21                       
      3.47822230e-03    3      1000022         1        -1             
      3.47822230e-03    3      1000022         3        -3             
      3.48734460e-03    3      1000022         5        -5             
      2.55724040e-02    3      1000022         6        -6             
      9.82039610e-04    2      1000023        21                       
      9.63170260e-04    3      1000023         1        -1             
      9.63170260e-04    3      1000023         3        -3             
      1.84827180e-03    3      1000023         5        -5             
      1.53297100e-01    3      1000023         6        -6             
      1.12558770e-03    2      1000025        21                       
      9.10304520e-05    3      1000025         1        -1             
      9.10304520e-05    3      1000025         3        -3             
      1.04693500e-03    3      1000025         5        -5             
      1.71145190e-01    3      1000025         6        -6             
      1.84094070e-02    3      1000035         1        -1             
      1.84094070e-02    3      1000035         3        -3             
      1.83123220e-02    3      1000035         5        -5             
      1.02323130e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  2.05366615e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37653040e-01    2      1000022        23                       
      6.32904770e-01    2      1000022        25                       
      3.59589980e-02    2      2000011       -11                       
      3.59589980e-02    2     -2000011        11                       
      3.59589640e-02    2      2000013       -13                       
      3.59589640e-02    2     -2000013        13                       
      4.28031090e-02    2      1000015       -15                       
      4.28031090e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  1.94589800e-01   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      8.61000660e-01    2      1000022        23                       
      8.31725080e-02    2      1000022        25                       
      5.86939040e-03    2      2000011       -11                       
      5.86939040e-03    2     -2000011        11                       
      5.86938630e-03    2      2000013       -13                       
      5.86938630e-03    2     -2000013        13                       
      1.61746090e-02    2      1000015       -15                       
      1.61746090e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  4.63717064e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.73883100e-01    2      1000024       -24                       
      2.73883100e-01    2     -1000024        24                       
      4.98019390e-04    2      1000022        23                       
      1.35797280e-02    2      1000023        23                       
      2.27518950e-01    2      1000025        23                       
      1.33173920e-04    2      1000022        25                       
      2.01178060e-01    2      1000023        25                       
      8.54537730e-03    2      1000025        25                       
      1.10136930e-04    2      2000011       -11                       
      1.10136930e-04    2     -2000011        11                       
      1.10136910e-04    2      2000013       -13                       
      1.10136910e-04    2     -2000013        13                       
      1.70099080e-04    2      1000015       -15                       
      1.70099080e-04    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  1.71790990e-01   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.82303320e-01    2      1000022        24                       
      1.76967050e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  4.63423220e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.70086320e-04    2      1000022        24                       
      2.74466840e-01    2      1000023        24                       
      2.49947320e-01    2      1000025        24                       
      1.21178170e-04    2     -1000015        16                       
      2.63320240e-01    2      1000024        23                       
      2.11674440e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56101448e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96586250e-04    2           13       -13                       
      5.62311040e-02    2           15       -15                       
      2.35979580e-03    2            3        -3                       
      7.69233880e-01    2            5        -5                       
      3.34440430e-02    2            4        -4                       
      1.89878620e-03    2           22        22                       
      3.96414550e-02    2           21        21                       
      4.92935770e-03    3           24        11       -12             
      4.92935770e-03    3           24        13       -14             
      4.92935770e-03    3           24        15       -16             
      1.47880730e-02    3           24        -2         1             
      1.47880730e-02    3           24        -4         3             
      4.92935770e-03    3          -24       -11        12             
      4.92935770e-03    3          -24       -13        14             
      4.92935770e-03    3          -24       -15        16             
      1.47880730e-02    3          -24         2        -1             
      1.47880730e-02    3          -24         4        -3             
      5.65289400e-04    3           23        12       -12             
      5.65289400e-04    3           23        14       -14             
      5.65289400e-04    3           23        16       -16             
      2.84504490e-04    3           23        11       -11             
      2.84504490e-04    3           23        13       -13             
      2.84504490e-04    3           23        15       -15             
      9.74688210e-04    3           23         2        -2             
      9.74688210e-04    3           23         4        -4             
      1.25563850e-03    3           23         1        -1             
      1.25563850e-03    3           23         3        -3             
      1.25563850e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  7.91524364e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.28249230e-05    2           13       -13                       
      9.40138570e-03    2           15       -15                       
      3.84803370e-04    2            3        -3                       
      9.48877110e-02    2            5        -5                       
      1.57935160e-01    2            6        -6                       
      8.31712170e-05    2           21        21                       
      5.36393960e-04    2           24       -24                       
      2.72338420e-04    2           23        23                       
      7.79989080e-03    2      1000022   1000022                       
      4.65642740e-02    2      1000022   1000023                       
      8.76821130e-02    2      1000022   1000025                       
      2.04917700e-03    2      1000022   1000035                       
      3.61850830e-03    2      1000023   1000023                       
      1.68538090e-03    2      1000023   1000025                       
      2.11019220e-02    2      1000023   1000035                       
      2.89562040e-03    2      1000025   1000025                       
      1.53765080e-01    2      1000025   1000035                       
      3.80551670e-02    2      1000024  -1000024                       
      1.84469130e-01    2      1000024  -1000037                       
      1.84469130e-01    2     -1000024   1000037                       
      2.12888630e-03    2           25        25                       
      6.18624760e-05    2      2000011  -2000011                       
      6.18497070e-05    2      2000013  -2000013                       
      5.82584470e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  7.69635528e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.35613370e-05    2           13       -13                       
      9.61242060e-03    2           15       -15                       
      3.93459340e-04    2            3        -3                       
      9.71076040e-02    2            5        -5                       
      1.71690400e-01    2            6        -6                       
      1.02176070e-04    2           21        21                       
      1.12818780e-02    2      1000022   1000022                       
      1.02064790e-01    2      1000022   1000023                       
      3.76536290e-02    2      1000022   1000025                       
      2.19650010e-03    2      1000022   1000035                       
      1.07499580e-02    2      1000023   1000023                       
      2.41393010e-05    2      1000023   1000025                       
      1.37792540e-01    2      1000023   1000035                       
      2.32191830e-03    2      1000025   1000025                       
      2.02574450e-02    2      1000025   1000035                       
      8.71655570e-02    2      1000024  -1000024                       
      1.54510860e-01    2      1000024  -1000037                       
      1.54510860e-01    2     -1000024   1000037                       
      5.30389780e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  7.63298582e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.41670240e-05    2           14       -13                       
      9.78589990e-03    2           16       -15                       
      3.71342150e-04    2            4        -3                       
      2.42018220e-01    2            6        -5                       
      1.45172740e-01    2      1000024   1000022                       
      8.80603590e-03    2      1000024   1000023                       
      1.70713370e-02    2      1000024   1000025                       
      1.91932930e-01    2      1000024   1000035                       
      4.95300300e-03    2      1000037   1000022                       
      2.02386190e-01    2      1000037   1000023                       
      1.76921050e-01    2      1000037   1000025                       
      5.47123840e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
