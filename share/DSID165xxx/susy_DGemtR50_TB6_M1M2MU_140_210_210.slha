#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19491200e+02   # h
        35     5.04383500e+02   # H
        36     5.00000000e+02   # A
        37     5.09429200e+02   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99973290e+03   # b1
   1000006     2.83372730e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.42184700e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.18431300e+02   # N1
   1000023     1.65851500e+02   # N2
   1000024     1.49696800e+02   # C1
   1000025    -2.17192700e+02   # N3
   1000035     2.82909800e+02   # N4
   1000037     2.80582600e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00092480e+03   # b2
   2000006     3.16266110e+03   # t2
   2000011     1.42167200e+02   # eR
   2000013     1.42167200e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.80330160e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07375193e-01   # O_{11}
  1  2    -7.06838268e-01   # O_{12}
  2  1     7.06838268e-01   # O_{21}
  2  2     7.07375193e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     5.55805728e-01   # O_{11}
  1  2     8.31312211e-01   # O_{12}
  2  1    -8.31312211e-01   # O_{21}
  2  2     5.55805728e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1    -7.68826430e-01   # 
  1  2     3.29488490e-01   # 
  1  3    -4.55743820e-01   # 
  1  4     3.04369450e-01   # 
  2  1     6.05927770e-01   # 
  2  2     6.45548880e-01   # 
  2  3    -3.55056170e-01   # 
  2  4     3.00088290e-01   # 
  3  1    -7.36104500e-02   # 
  3  2     1.11983830e-01   # 
  3  3     6.84020880e-01   # 
  3  4     7.17047150e-01   # 
  4  1    -1.90627380e-01   # 
  4  2     6.79826140e-01   # 
  4  3     4.45362930e-01   # 
  4  4    -5.50590520e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -6.25588830e-01   # Umix_{11}
  1  2     7.80152920e-01   # Umix_{12}
  2  1    -7.80152920e-01   # Umix_{21}
  2  2    -6.25588830e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -7.80152800e-01   # Vmix_{11}
  1  2     6.25589010e-01   # Vmix_{12}
  2  1    -6.25589010e-01   # Vmix_{21}
  2  2    -7.80152800e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     2.10000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     2.50000000e+05   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     2.10000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.35600000e+02   # m_eR
    35     1.35600000e+02   # m_muR
    36     1.35900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     1.26000000e+03   # A_e
  2  2     1.26000000e+03   # A_mu
  3  3     1.26000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.78493387e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.35887660e-02    2      1000022         1                       
      9.47626160e-02    2      1000023         1                       
      5.19818670e-03    2      1000025         1                       
      1.67451590e-01    2      1000035         1                       
      2.59884270e-01    2     -1000024         2                       
      3.99114580e-01    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.79125626e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.18129770e-02    2      1000022         2                       
      1.89452380e-01    2      1000023         2                       
      3.19714030e-03    2      1000025         2                       
      1.36050420e-01    2      1000035         2                       
      4.03364120e-01    2      1000024         1                       
      2.56123010e-01    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.78493387e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.35887890e-02    2      1000022         3                       
      9.47626460e-02    2      1000023         3                       
      5.19818860e-03    2      1000025         3                       
      1.67451640e-01    2      1000035         3                       
      2.59884240e-01    2     -1000024         4                       
      3.99114520e-01    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.79125626e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.18129740e-02    2      1000022         4                       
      1.89452310e-01    2      1000023         4                       
      3.19713940e-03    2      1000025         4                       
      1.36050360e-01    2      1000035         4                       
      4.03364150e-01    2      1000024         3                       
      2.56123040e-01    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  2.70308008e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.03333570e-02    2      1000022         5                       
      6.85201660e-02    2      1000023         5                       
      2.13811060e-03    2      1000025         5                       
      4.81979740e-02    2      1000035         5                       
      3.36881670e-01    2     -1000024         6                       
      3.70127110e-01    2     -1000037         6                       
      8.38016570e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  7.17524964e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.02937350e-03    2      1000022         6                       
      7.93163110e-02    2      1000023         6                       
      2.10977000e-01    2      1000025         6                       
      2.99016390e-01    2      1000035         6                       
      2.10932670e-03    2      1000024         5                       
      3.99551570e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.92084470e-01    2      1000022         1                       
      3.66661070e-01    2      1000023         1                       
      5.38761170e-03    2      1000025         1                       
      3.58668830e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.92084650e-01    2      1000022         2                       
      3.66661010e-01    2      1000023         2                       
      5.38760520e-03    2      1000025         2                       
      3.58667860e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.92084470e-01    2      1000022         3                       
      3.66661070e-01    2      1000023         3                       
      5.38761170e-03    2      1000025         3                       
      3.58668830e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.92084650e-01    2      1000022         4                       
      3.66660950e-01    2      1000023         4                       
      5.38760470e-03    2      1000025         4                       
      3.58667860e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  5.73344948e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.69819110e-02    2      1000022         5                       
      4.32049560e-02    2      1000023         5                       
      1.05733010e-02    2      1000025         5                       
      9.22574550e-02    2      1000035         5                       
      2.60653230e-01    2     -1000024         6                       
      4.75486840e-01    2     -1000037         6                       
      9.08423140e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.05423327e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.96571020e-01    2      1000024         5                       
      1.31066310e-02    2      1000037         5                       
      1.68162180e-01    2           23   1000006                       
      2.00468340e-02    2           24   1000005                       
      4.35815190e-02    2           24   2000005                       
      1.03927220e-01    2      1000022         6                       
      1.00338350e-01    2      1000023         6                       
      2.10184800e-01    2      1000025         6                       
      4.40814980e-02    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.12380177e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.64482460e-03    2      1000022        11                       
      2.91489090e-01    2      1000023        11                       
      1.55043010e-03    2      1000025        11                       
      9.94826330e-02    2      1000035        11                       
      2.38523560e-01    2     -1000024        12                       
      3.66309460e-01    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.12690451e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.72480930e-01    2      1000022        12                       
      2.96955780e-02    2      1000023        12                       
      7.03463940e-03    2      1000025        12                       
      1.84983970e-01    2      1000035        12                       
      3.70530550e-01    2      1000024        11                       
      2.35274340e-01    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.12380177e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.64482460e-03    2      1000022        13                       
      2.91489090e-01    2      1000023        13                       
      1.55043010e-03    2      1000025        13                       
      9.94826330e-02    2      1000035        13                       
      2.38523560e-01    2     -1000024        14                       
      3.66309460e-01    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.12690451e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.72480930e-01    2      1000022        14                       
      2.96955780e-02    2      1000023        14                       
      7.03463940e-03    2      1000025        14                       
      1.84983970e-01    2      1000035        14                       
      3.70530550e-01    2      1000024        13                       
      2.35274340e-01    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  3.97631849e-02   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.15268139e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.71408670e-01    2      1000022        16                       
      2.95109690e-02    2      1000023        16                       
      6.99090680e-03    2      1000025        16                       
      1.83833970e-01    2      1000035        16                       
      3.71434630e-01    2      1000024        15                       
      2.35855910e-01    2      1000037        15                       
      9.64978130e-04    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  4.00316263e-02   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  4.00316263e-02   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.14953978e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.72788730e-03    2      1000022        15                       
      2.90338250e-01    2      1000023        15                       
      3.99872010e-03    2      1000025        15                       
      9.99002010e-02    2      1000035        15                       
      2.37039100e-01    2     -1000024        16                       
      3.64029790e-01    2     -1000037        16                       
      4.83238750e-04    2           36   1000015                       
      4.82882610e-04    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.16534526e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.37202680e-03    3      1000024         1        -2             
      7.37202680e-03    3     -1000024         2        -1             
      7.37202680e-03    3      1000024         3        -4             
      7.37202680e-03    3     -1000024         4        -3             
      2.94123740e-02    3      1000024         5        -6             
      2.94123740e-02    3     -1000024         6        -5             
      1.11839050e-02    3      1000037         1        -2             
      1.11839050e-02    3     -1000037         2        -1             
      1.11839050e-02    3      1000037         3        -4             
      1.11839050e-02    3     -1000037         4        -3             
      1.73377690e-01    3      1000037         5        -6             
      1.73377690e-01    3     -1000037         6        -5             
      3.62535940e-04    2      1000022        21                       
      5.85170700e-03    3      1000022         1        -1             
      5.85170700e-03    3      1000022         3        -3             
      6.13355400e-03    3      1000022         5        -5             
      1.20523070e-02    3      1000022         6        -6             
      2.44055660e-04    2      1000023        21                       
      6.54456110e-03    3      1000023         1        -1             
      6.54456110e-03    3      1000023         3        -3             
      6.70876260e-03    3      1000023         5        -5             
      5.83695470e-02    3      1000023         6        -6             
      1.12337850e-03    2      1000025        21                       
      2.91838140e-04    3      1000025         1        -1             
      2.91838140e-04    3      1000025         3        -3             
      1.18631360e-03    3      1000025         5        -5             
      1.64121760e-01    3      1000025         6        -6             
      3.67013650e-04    2      1000035        21                       
      1.00665670e-02    3      1000035         1        -1             
      1.00665670e-02    3      1000035         3        -3             
      1.04202210e-02    3      1000035         5        -5             
      2.13597570e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  6.51425178e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.67234380e-01    2      2000011       -11                       
      1.67234380e-01    2     -2000011        11                       
      1.67232450e-01    2      2000013       -13                       
      1.67232450e-01    2     -2000013        13                       
      1.65533140e-01    2      1000015       -15                       
      1.65533140e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.13802145e-02   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.60194120e-05    2      1000023        22                       
      1.18083640e-02    3     -1000024         2        -1             
      3.93612120e-03    3     -1000024        12       -11             
      3.93612120e-03    3     -1000024        14       -13             
      1.18083640e-02    3      1000024         1        -2             
      3.93612120e-03    3      1000024        11       -12             
      3.93612120e-03    3      1000024        13       -14             
      1.18083640e-02    3     -1000024         4        -3             
      3.93612120e-03    3     -1000024        16       -15             
      1.18083640e-02    3      1000024         3        -4             
      3.93612120e-03    3      1000024        15       -16             
      6.58461990e-01    2      1000022        23                       
      2.57364770e-04    3      1000023         2        -2             
      3.31732120e-04    3      1000023         1        -1             
      3.31732120e-04    3      1000023         3        -3             
      2.57364770e-04    3      1000023         4        -4             
      3.00162440e-04    3      1000023         5        -5             
      7.52534540e-05    3      1000023        11       -11             
      7.52534540e-05    3      1000023        13       -13             
      7.39949760e-05    3      1000023        15       -15             
      1.49781900e-04    3      1000023        12       -12             
      1.49781900e-04    3      1000023        14       -14             
      1.49781900e-04    3      1000023        16       -16             
      3.11554970e-02    2      2000011       -11                       
      3.11554970e-02    2     -2000011        11                       
      3.11554520e-02    2      2000013       -13                       
      3.11554520e-02    2     -2000013        13                       
      7.19488410e-02    2      1000015       -15                       
      7.19488410e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  8.33248937e-01   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.31764070e-01    2      1000024       -24                       
      4.31764070e-01    2     -1000024        24                       
      7.81255490e-03    2      1000022        23                       
      1.66249940e-04    2      1000023        23                       
      9.30647440e-05    3      1000025         2        -2             
      1.20044400e-04    3      1000025         1        -1             
      1.20044400e-04    3      1000025         3        -3             
      9.30647440e-05    3      1000025         4        -4             
      1.14241960e-04    3      1000025         5        -5             
      2.72293220e-05    3      1000025        11       -11             
      2.72293220e-05    3      1000025        13       -13             
      2.69982770e-05    3      1000025        15       -15             
      5.41630000e-05    3      1000025        12       -12             
      5.41630000e-05    3      1000025        14       -14             
      5.41630000e-05    3      1000025        16       -16             
      2.01001220e-02    2      1000022        25                       
      1.75307190e-02    2      2000011       -11                       
      1.75307190e-02    2     -2000011        11                       
      1.75307080e-02    2      2000013       -13                       
      1.75307080e-02    2     -2000013        13                       
      1.87429900e-02    2      1000015       -15                       
      1.87429900e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  5.45409347e-05   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.37607840e-01    3      1000022         2        -1             
      1.37607840e-01    3      1000022         4        -3             
      4.58656620e-02    3      1000022       -11        12             
      4.58656620e-02    3      1000022       -13        14             
      4.58657150e-02    3      1000022       -15        16             
      5.87187230e-01    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  5.68934221e-01   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.51989400e-02    2      1000022        24                       
      5.07117090e-01    2      1000023        24                       
      4.73084570e-04    3      1000025         2        -1             
      4.73084570e-04    3      1000025         4        -3             
      1.57685560e-04    3      1000025       -11        12             
      1.57685560e-04    3      1000025       -13        14             
      1.57685910e-04    3      1000025       -15        16             
      3.91425750e-03    2     -1000015        16                       
      4.39098830e-01    2      1000024        23                       
      3.25191280e-03    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  5.07009706e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.01270070e-04    2           13       -13                       
      5.75706210e-02    2           15       -15                       
      2.41607540e-03    2            3        -3                       
      7.87751260e-01    2            5        -5                       
      2.99253590e-02    2            4        -4                       
      1.73803080e-03    2           22        22                       
      3.51953210e-02    2           21        21                       
      4.33285490e-03    3           24        11       -12             
      4.33285490e-03    3           24        13       -14             
      4.33285490e-03    3           24        15       -16             
      1.29985630e-02    3           24        -2         1             
      1.29985630e-02    3           24        -4         3             
      4.33285490e-03    3          -24       -11        12             
      4.33285490e-03    3          -24       -13        14             
      4.33285490e-03    3          -24       -15        16             
      1.29985630e-02    3          -24         2        -1             
      1.29985630e-02    3          -24         4        -3             
      4.93155500e-04    3           23        12       -12             
      4.93155500e-04    3           23        14       -14             
      4.93155500e-04    3           23        16       -16             
      2.48200170e-04    3           23        11       -11             
      2.48200170e-04    3           23        13       -13             
      2.48200170e-04    3           23        15       -15             
      8.50312820e-04    3           23         2        -2             
      8.50312820e-04    3           23         4        -4             
      1.09541240e-03    3           23         1        -1             
      1.09541240e-03    3           23         3        -3             
      1.09541240e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  3.86154297e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.35608600e-05    2           13       -13                       
      9.61163080e-03    2           15       -15                       
      3.95856650e-04    2            3        -3                       
      1.06215600e-01    2            5        -5                       
      1.06054900e-01    2            6        -6                       
      1.52619500e-04    2           21        21                       
      2.13997390e-03    2           24       -24                       
      1.05199730e-03    2           23        23                       
      6.63602200e-02    2      1000022   1000022                       
      3.67788080e-02    2      1000022   1000023                       
      1.14022740e-01    2      1000022   1000025                       
      3.33123750e-04    2      1000022   1000035                       
      4.00866100e-03    2      1000023   1000023                       
      1.48645700e-02    2      1000023   1000025                       
      8.04707180e-04    2      1000023   1000035                       
      2.15516190e-03    2      1000025   1000025                       
      3.22739560e-02    2      1000025   1000035                       
      1.60132380e-01    2      1000024  -1000024                       
      1.66539860e-01    2      1000024  -1000037                       
      1.66539860e-01    2     -1000024   1000037                       
      8.81852120e-03    2           25        25                       
      2.41652160e-04    2      2000011  -2000011                       
      2.41603950e-04    2      2000013  -2000013                       
      2.28033100e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.79540999e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.40280930e-05    2           13       -13                       
      9.74592290e-03    2           15       -15                       
      4.01401190e-04    2            3        -3                       
      1.07843850e-01    2            5        -5                       
      1.60625560e-01    2            6        -6                       
      1.34271990e-04    2           21        21                       
      1.37869690e-01    2      1000022   1000022                       
      9.38306970e-02    2      1000022   1000023                       
      2.59859300e-02    2      1000022   1000025                       
      1.16992850e-05    2      1000022   1000035                       
      1.29393630e-02    2      1000023   1000023                       
      1.87378020e-03    2      1000023   1000025                       
      4.91621250e-03    2      1000023   1000035                       
      3.95727580e-03    2      1000025   1000025                       
      3.92443390e-01    2      1000024  -1000024                       
      2.27646380e-02    2      1000024  -1000037                       
      2.27646380e-02    2     -1000024   1000037                       
      1.85761890e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  3.67155687e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.58398680e-05    2           14       -13                       
      1.02648390e-02    2           16       -15                       
      3.89808960e-04    2            4        -3                       
      2.27463140e-01    2            6        -5                       
      9.97630580e-02    2      1000024   1000022                       
      7.70880360e-02    2      1000024   1000023                       
      1.45470050e-01    2      1000024   1000025                       
      1.54712860e-01    2      1000024   1000035                       
      4.52714190e-02    2      1000037   1000022                       
      1.96325410e-01    2      1000037   1000023                       
      4.11328230e-02    2      1000037   1000025                       
      2.08271760e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
