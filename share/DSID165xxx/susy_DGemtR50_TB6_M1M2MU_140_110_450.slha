#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19688300e+02   # h
        35     1.00709920e+03   # H
        36     1.00000000e+03   # A
        37     1.00966220e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99912870e+03   # b1
   1000006     2.83467900e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     1.19455000e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.01115600e+02   # N1
   1000023     1.37778900e+02   # N2
   1000024     1.01713700e+02   # C1
   1000025    -4.55030800e+02   # N3
   1000035     4.66136400e+02   # N4
   1000037     4.66036500e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00152860e+03   # b2
   2000006     3.16180830e+03   # t2
   2000011     1.19493500e+02   # eR
   2000013     1.19493600e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68788580e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07376875e-01   # O_{11}
  1  2    -7.06836584e-01   # O_{12}
  2  1     7.06836584e-01   # O_{21}
  2  2     7.07376875e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.36731352e-01   # O_{11}
  1  2     7.71085719e-01   # O_{12}
  2  1    -7.71085719e-01   # O_{21}
  2  2     6.36731352e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     1.21731740e-01   # 
  1  2    -9.69366730e-01   # 
  1  3     1.99599910e-01   # 
  1  4    -7.52971300e-02   # 
  2  1     9.85629440e-01   # 
  2  2     1.43388670e-01   # 
  2  3     8.14383500e-02   # 
  2  4    -3.66378800e-02   # 
  3  1     4.34537400e-02   # 
  3  2    -8.32589400e-02   # 
  3  3    -6.98481860e-01   # 
  3  4    -7.09438380e-01   # 
  4  1     1.08756620e-01   # 
  4  2    -1.81207270e-01   # 
  4  3    -6.82386160e-01   # 
  4  4     6.99775280e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.64712620e-01   # Umix_{11}
  1  2     2.63305010e-01   # Umix_{12}
  2  1    -2.63305010e-01   # Umix_{21}
  2  2    -9.64712620e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.94901240e-01   # Vmix_{11}
  1  2     1.00854220e-01   # Vmix_{12}
  2  1    -1.00854220e-01   # Vmix_{21}
  2  2    -9.94901240e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     4.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     1.10000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     1.11600000e+02   # m_eR
    35     1.11600000e+02   # m_muR
    36     1.11900000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     2.70000000e+03   # A_e
  2  2     2.70000000e+03   # A_mu
  3  3     2.70000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.81189552e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25093120e-01    2      1000022         1                       
      4.56222040e-04    2      1000023         1                       
      2.63167750e-03    2      1000025         1                       
      1.27645460e-02    2      1000035         1                       
      6.15302380e-01    2     -1000024         2                       
      4.37521600e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.81808690e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.95924160e-01    2      1000022         2                       
      3.45592870e-02    2      1000023         2                       
      1.78958520e-03    2      1000025         2                       
      8.19121770e-03    2      1000035         2                       
      6.53129580e-01    2      1000024         1                       
      6.40622710e-03    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.81189552e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25093180e-01    2      1000022         3                       
      4.56222130e-04    2      1000023         3                       
      2.63167800e-03    2      1000025         3                       
      1.27645490e-02    2      1000035         3                       
      6.15302260e-01    2     -1000024         4                       
      4.37521560e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.81808690e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.95924100e-01    2      1000022         4                       
      3.45592800e-02    2      1000023         4                       
      1.78958460e-03    2      1000025         4                       
      8.19121670e-03    2      1000035         4                       
      6.53129640e-01    2      1000024         3                       
      6.40622850e-03    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.40930281e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.63161290e-01    2      1000022         5                       
      3.12206390e-02    2      1000023         5                       
      2.86190910e-03    2      1000025         5                       
      1.27957710e-03    2      1000035         5                       
      3.07250890e-01    2     -1000024         6                       
      4.10128060e-01    2     -1000037         6                       
      8.40976240e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.99736350e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      5.67909220e-02    2      1000022         6                       
      4.89738770e-02    2      1000023         6                       
      2.09033580e-01    2      1000025         6                       
      2.80792980e-01    2      1000035         6                       
      1.30179240e-01    2      1000024         5                       
      2.74229380e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.48559380e-02    2      1000022         1                       
      9.72015260e-01    2      1000023         1                       
      1.81100310e-03    2      1000025         1                       
      1.13178800e-02    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74869271e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.48559430e-02    2      1000022         2                       
      9.72015320e-01    2      1000023         2                       
      1.81098720e-03    2      1000025         2                       
      1.13177760e-02    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.48559380e-02    2      1000022         3                       
      9.72015260e-01    2      1000023         3                       
      1.81100310e-03    2      1000025         3                       
      1.13178800e-02    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74869271e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.48559440e-02    2      1000022         4                       
      9.72015320e-01    2      1000023         4                       
      1.81098750e-03    2      1000025         4                       
      1.13177770e-02    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.90608229e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.40513550e-01    2      1000022         5                       
      1.22212260e-02    2      1000023         5                       
      9.51137770e-03    2      1000025         5                       
      1.83782950e-02    2      1000035         5                       
      2.69122450e-01    2     -1000024         6                       
      4.59637850e-01    2     -1000037         6                       
      9.06152430e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  1.03216296e+02   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.60370230e-01    2      1000024         5                       
      1.50320840e-01    2      1000037         5                       
      1.68856440e-01    2           23   1000006                       
      2.67120060e-02    2           24   1000005                       
      3.69619390e-02    2           24   2000005                       
      7.67853410e-02    2      1000022         6                       
      4.38161010e-02    2      1000023         6                       
      1.98138980e-01    2      1000025         6                       
      1.38038160e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  4.15058645e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.47249040e-01    2      1000022        11                       
      1.42217840e-01    2      1000023        11                       
      1.02393820e-03    2      1000025        11                       
      4.27279340e-03    2      1000035        11                       
      5.65057280e-01    2     -1000024        12                       
      4.01791260e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  4.15372965e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25644430e-01    2      1000022        12                       
      4.80222960e-02    2      1000023        12                       
      3.33025560e-03    2      1000025        12                       
      1.68080070e-02    2      1000035        12                       
      6.00306930e-01    2      1000024        11                       
      5.88807980e-03    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  4.15058645e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.47249040e-01    2      1000022        13                       
      1.42217840e-01    2      1000023        13                       
      1.02393820e-03    2      1000025        13                       
      4.27279340e-03    2      1000035        13                       
      5.65057280e-01    2     -1000024        14                       
      4.01791260e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  4.15372965e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.25644430e-01    2      1000022        14                       
      4.80222960e-02    2      1000023        14                       
      3.33025560e-03    2      1000025        14                       
      1.68080070e-02    2      1000035        14                       
      6.00306930e-01    2      1000024        13                       
      5.88807980e-03    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  7.68234182e-04   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.40143530e-01    2      1000022        15                       
      5.98564820e-02    2     -1000024        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.19155575e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.22704050e-01    2      1000022        16                       
      4.75886760e-02    2      1000023        16                       
      3.30018510e-03    2      1000025        16                       
      1.66562390e-02    2      1000035        16                       
      5.95248280e-01    2      1000024        15                       
      1.04937080e-02    2      1000037        15                       
      4.00889390e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  7.26113385e-04   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  7.26105375e-04   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.18862161e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.45221500e-01    2      1000022        15                       
      1.40966650e-01    2      1000023        15                       
      3.46371390e-03    2      1000025        15                       
      6.56792060e-03    2      1000035        15                       
      5.59947430e-01    2     -1000024        16                       
      3.98158100e-02    2     -1000037        16                       
      2.01036990e-03    2           36   1000015                       
      2.00674080e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  2.00817671e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.90044680e-02    3      1000024         1        -2             
      1.90044680e-02    3     -1000024         2        -1             
      1.90044680e-02    3      1000024         3        -4             
      1.90044680e-02    3     -1000024         4        -3             
      6.52069230e-02    3      1000024         5        -6             
      6.52069230e-02    3     -1000024         6        -5             
      1.29313550e-03    3      1000037         1        -2             
      1.29313550e-03    3     -1000037         2        -1             
      1.29313550e-03    3      1000037         3        -4             
      1.29313550e-03    3     -1000037         4        -3             
      1.37155920e-01    3      1000037         5        -6             
      1.37155920e-01    3     -1000037         6        -5             
      1.28621720e-04    2      1000022        21                       
      2.06016040e-02    3      1000022         1        -1             
      2.06016040e-02    3      1000022         3        -3             
      2.04770300e-02    3      1000022         5        -5             
      4.01411280e-02    3      1000022         6        -6             
      2.18708060e-05    2      1000023        21                       
      2.78695250e-03    3      1000023         1        -1             
      2.78695250e-03    3      1000023         3        -3             
      2.76645300e-03    3      1000023         5        -5             
      3.32763640e-02    3      1000023         6        -6             
      1.12378130e-03    2      1000025        21                       
      1.45521040e-04    3      1000025         1        -1             
      1.45521040e-04    3      1000025         3        -3             
      1.07231580e-03    3      1000025         5        -5             
      1.58002790e-01    3      1000025         6        -6             
      8.90457190e-04    2      1000035        21                       
      8.59806200e-04    3      1000035         1        -1             
      8.59806200e-04    3      1000035         3        -3             
      1.74919590e-03    3      1000035         5        -5             
      2.05646400e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  1.25514874e-01   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.10530450e-05    3     -1000024         2        -1             
      1.10530450e-05    3      1000024         1        -2             
      1.10530450e-05    3     -1000024         4        -3             
      1.10530450e-05    3      1000024         3        -4             
      1.66550040e-01    2      2000011       -11                       
      1.66550040e-01    2     -2000011        11                       
      1.66546780e-01    2      2000013       -13                       
      1.66546780e-01    2     -2000013        13                       
      1.66881100e-01    2      1000015       -15                       
      1.66881100e-01    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  3.35149448e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.07638940e-01    2      1000024       -24                       
      3.07638940e-01    2     -1000024        24                       
      2.77642520e-01    2      1000022        23                       
      4.33802600e-02    2      1000023        23                       
      5.05340400e-02    2      1000022        25                       
      5.52110890e-03    2      1000023        25                       
      5.65145980e-04    2      2000011       -11                       
      5.65145980e-04    2     -2000011        11                       
      5.65145860e-04    2      2000013       -13                       
      5.65145860e-04    2     -2000013        13                       
      2.69189480e-03    2      1000015       -15                       
      2.69189480e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  3.37451935e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.15572770e-01    2      1000024       -24                       
      3.15572770e-01    2     -1000024        24                       
      6.25424460e-02    2      1000022        23                       
      7.21931600e-03    2      1000023        23                       
      2.36143980e-01    2      1000022        25                       
      3.71529390e-02    2      1000023        25                       
      3.62687370e-03    2      2000011       -11                       
      3.62687370e-03    2     -2000011        11                       
      3.62687390e-03    2      2000013       -13                       
      3.62687390e-03    2     -2000013        13                       
      5.64420130e-03    2      1000015       -15                       
      5.64420130e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  3.72411452e-13   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.68942360e-01    2      1000022       211                       
      1.14004980e-02    3      1000022       211       111             
      1.09828550e-01    3      1000022       -11        12             
      1.09828550e-01    3      1000022       -13        14             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  3.29330531e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.71662030e-01    2      1000022        24                       
      1.49418060e-01    2      1000023        24                       
      4.22097880e-03    2     -1000015        16                       
      3.12958690e-01    2      1000024        23                       
      2.61740210e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56196285e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96554960e-04    2           13       -13                       
      5.62221780e-02    2           15       -15                       
      2.35941630e-03    2            3        -3                       
      7.69098160e-01    2            5        -5                       
      3.34393750e-02    2            4        -4                       
      1.90307700e-03    2           22        22                       
      3.96457460e-02    2           21        21                       
      4.93631510e-03    3           24        11       -12             
      4.93631510e-03    3           24        13       -14             
      4.93631510e-03    3           24        15       -16             
      1.48089450e-02    3           24        -2         1             
      1.48089450e-02    3           24        -4         3             
      4.93631510e-03    3          -24       -11        12             
      4.93631510e-03    3          -24       -13        14             
      4.93631510e-03    3          -24       -15        16             
      1.48089450e-02    3          -24         2        -1             
      1.48089450e-02    3          -24         4        -3             
      5.66390810e-04    3           23        12       -12             
      5.66390810e-04    3           23        14       -14             
      5.66390810e-04    3           23        16       -16             
      2.85058840e-04    3           23        11       -11             
      2.85058840e-04    3           23        13       -13             
      2.85058840e-04    3           23        15       -15             
      9.76587410e-04    3           23         2        -2             
      9.76587410e-04    3           23         4        -4             
      1.25808500e-03    3           23         1        -1             
      1.25808500e-03    3           23         3        -3             
      1.25808500e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  1.09895981e+01   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.36418910e-05    2           13       -13                       
      6.77127300e-03    2           15       -15                       
      2.77151560e-04    2            3        -3                       
      6.83421570e-02    2            5        -5                       
      1.13747130e-01    2            6        -6                       
      5.99012380e-05    2           21        21                       
      3.85663240e-04    2           24       -24                       
      1.95809260e-04    2           23        23                       
      2.64019480e-02    2      1000022   1000022                       
      7.96807560e-03    2      1000022   1000023                       
      1.76279190e-01    2      1000022   1000025                       
      5.71651010e-02    2      1000022   1000035                       
      5.98509970e-04    2      1000023   1000023                       
      2.62518800e-02    2      1000023   1000025                       
      7.72157590e-03    2      1000023   1000035                       
      4.53900950e-04    2      1000025   1000025                       
      1.00671730e-02    2      1000025   1000035                       
      7.47220130e-04    2      1000035   1000035                       
      4.24074420e-02    2      1000024  -1000024                       
      1.12924510e-04    2      1000037  -1000037                       
      2.26180020e-01    2      1000024  -1000037                       
      2.26180020e-01    2     -1000024   1000037                       
      1.52460230e-03    2           25        25                       
      4.68526520e-05    2      2000011  -2000011                       
      4.68429830e-05    2      2000013  -2000013                       
      4.41236810e-05    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  1.08716119e+01   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.37593250e-05    2           13       -13                       
      6.80499150e-03    2           15       -15                       
      2.78544550e-04    2            3        -3                       
      6.87461050e-02    2            5        -5                       
      1.21546050e-01    2            6        -6                       
      7.23342600e-05    2           21        21                       
      3.56622750e-02    2      1000022   1000022                       
      1.12092710e-02    2      1000022   1000023                       
      6.76995290e-02    2      1000022   1000025                       
      1.51095140e-01    2      1000022   1000035                       
      8.80745590e-04    2      1000023   1000023                       
      9.17593110e-03    2      1000023   1000025                       
      2.23466830e-02    2      1000023   1000035                       
      1.20692080e-03    2      1000025   1000025                       
      1.12834690e-03    2      1000025   1000035                       
      1.01050920e-02    2      1000035   1000035                       
      5.70839160e-02    2      1000024  -1000024                       
      5.42304340e-03    2      1000037  -1000037                       
      2.14568230e-01    2      1000024  -1000037                       
      2.14568230e-01    2     -1000024   1000037                       
      3.74831170e-04    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  1.07405111e+01   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.42815310e-05    2           14       -13                       
      6.95456010e-03    2           16       -15                       
      2.63902270e-04    2            4        -3                       
      1.71995460e-01    2            6        -5                       
      4.48396110e-04    2      1000024   1000022                       
      3.11136600e-02    2      1000024   1000023                       
      2.37289320e-01    2      1000024   1000025                       
      2.29021550e-01    2      1000024   1000035                       
      2.19172880e-01    2      1000037   1000022                       
      9.72318800e-02    2      1000037   1000023                       
      4.56355840e-03    2      1000037   1000025                       
      1.53246960e-03    2      1000037   1000035                       
      3.88150480e-04    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
