#=====================================================================
#  ISAJET SUSY parameters in SUSY Les Houches Accord format
#   Created from isa.wig and isa.out by isa2lha.py (b.k.gjelsten@fys.uio.no)
#===================================================================== SPINFO
Block SPINFO   # Program information
     1   ISASUSY FROM ISAJET            # Spectrum Calculator
     2   V7.80   29-OCT-2009 12:50:36   # Version
#===================================================================== MODSEL
Block MODSEL   # Model selection
     1     0   # General MSSM Simulation
#===================================================================== SMINPUTS
Block SMINPUTS   # Standard Model inputs
     1     1.28040973e+02   # alpha_em^{-1}
     3     1.20000000e-01   # alpha_s(m_Z)
     6     1.72500000e+02   # m_{top}(pole)
#===================================================================== MINPAR
Block MINPAR   # SUSY breaking input parameters
     3     6.00000000e+00   # tan(beta)
#===================================================================== MASS
Block MASS   # Scalar and gaugino mass spectrum
# PDG code     mass               particle
        25     1.19704600e+02   # h
        35     1.00709250e+03   # H
        36     1.00000000e+03   # A
        37     1.00966140e+03   # H+
   1000001     3.00055370e+03   # dL
   1000002     2.99954740e+03   # uL
   1000003     3.00055370e+03   # sL
   1000004     2.99954790e+03   # cL
   1000005     2.99861180e+03   # b1
   1000006     2.83547170e+03   # t1
   1000011     3.00035110e+03   # eL
   1000012     2.99934470e+03   # snue
   1000013     3.00035110e+03   # muL
   1000014     2.99934470e+03   # snum
   1000015     3.07950500e+02   # ta1
   1000016     2.99934470e+03   # snut
   1000021     3.00000000e+03   # gl
   1000022     1.38282500e+02   # N1
   1000023     4.77561200e+02   # N2
   1000024     4.77359200e+02   # C1
   1000025    -6.52739200e+02   # N3
   1000035     6.76895800e+02   # N4
   1000037     6.76434800e+02   # C2
   2000001     3.00010130e+03   # dR
   2000002     2.99979740e+03   # uR
   2000003     3.00010130e+03   # sR
   2000004     2.99979760e+03   # cR
   2000005     3.00204490e+03   # b2
   2000006     3.16109720e+03   # t2
   2000011     3.07876800e+02   # eR
   2000013     3.07876900e+02   # muR
   2000015     3.00036500e+03   # ta2
#===================================================================== ALPHA
Block ALPHA   # Effective Higgs mixing parameter
          -1.68785650e-01   # alpha_h
#===================================================================== STOPMIX
Block STOPMIX   # stop mixing matrix
  1  1     7.07377928e-01   # O_{11}
  1  2    -7.06835530e-01   # O_{12}
  2  1     7.06835530e-01   # O_{21}
  2  2     7.07377928e-01   # O_{22}
#===================================================================== SBOTMIX
Block SBOTMIX   # sbottom mixing matrix
  1  1     6.58745998e-01   # O_{11}
  1  2     7.52365410e-01   # O_{12}
  2  1    -7.52365410e-01   # O_{21}
  2  2     6.58745998e-01   # O_{22}
#===================================================================== STAUMIX
Block STAUMIX   # stau mixing matrix
  1  1    -4.32051034e-08   # O_{11}
  1  2    -1.00000000e+00   # O_{12}
  2  1     1.00000000e+00   # O_{21}
  2  2    -4.32051034e-08   # O_{22}
#===================================================================== NMIX
Block NMIX   # neutralino mixing matrix
  1  1     9.96880770e-01   # 
  1  2    -8.61192000e-03   # 
  1  3     7.36620400e-02   # 
  1  4    -2.69909400e-02   # 
  2  1    -3.41989100e-02   # 
  2  2    -9.36056490e-01   # 
  2  3     2.73041930e-01   # 
  2  4    -2.19264250e-01   # 
  3  1     3.24984500e-02   # 
  3  2    -4.06628400e-02   # 
  3  3    -7.04065980e-01   # 
  3  4    -7.08224000e-01   # 
  4  1    -6.32697200e-02   # 
  4  2     3.49386220e-01   # 
  4  3     6.51393000e-01   # 
  4  4    -6.70532410e-01   # 
#===================================================================== UMIX
Block UMIX   # chargino U mixing matrix
  1  1    -9.21339150e-01   # Umix_{11}
  1  2     3.88759790e-01   # Umix_{12}
  2  1    -3.88759790e-01   # Umix_{21}
  2  2    -9.21339150e-01   # Umix_{22}
#===================================================================== VMIX
Block VMIX   # chargino V mixing matrix
  1  1    -9.49810090e-01   # Vmix_{11}
  1  2     3.12827110e-01   # Vmix_{12}
  2  1    -3.12827110e-01   # Vmix_{21}
  2  2    -9.49810090e-01   # Vmix_{22}
#===================================================================== HMIX
Block HMIX  Q= 3.000e+03   # Higgs mixing parameters
     1     6.50000000e+02   # mu(Q)
     2     6.00000000e+00   # tan(beta)(M_{GUT})
     4     1.00000000e+06   # m_A^2(Q)
#===================================================================== MSOFT
Block MSOFT  Q=  3.000e+03   # DRbar SUSY breaking parameters
     1     1.40000000e+02   # M1
     2     5.00000000e+02   # M2
     3     3.00000000e+03   # not M3 but m(gluino) ...
    31     3.00000000e+03   # m_L1 (m_eL)
    32     3.00000000e+03   # m_L2 (m_muL)
    33     3.00000000e+03   # m_L3 (m_tauL)
    34     3.04900000e+02   # m_eR
    35     3.04900000e+02   # m_muR
    36     3.05100000e+02   # m_tauR
    41     3.00000000e+03   # m_Q1
    42     3.00000000e+03   # m_Q2
    43     3.00000000e+03   # m_Q3
    44     3.00000000e+03   # m_uR
    45     3.00000000e+03   # m_cR
    46     3.00000000e+03   # m_tR
    47     3.00000000e+03   # m_dR
    48     3.00000000e+03   # m_sR
    49     3.00000000e+03   # m_bR
#===================================================================== AU
Block AU  Q=  3.000e+03   #
  1  1     7.34850000e+03   # A_u
  2  2     7.34850000e+03   # A_c
  3  3     7.34850000e+03   # A_t
#===================================================================== AD
Block AD  Q=  3.000e+03   #
  1  1     0.00000000e+00   # A_d
  2  2     0.00000000e+00   # A_s
  3  3     0.00000000e+00   # A_b
#===================================================================== AE
Block AE  Q=  3.000e+03   #
  1  1     3.90000000e+03   # A_e
  2  2     3.90000000e+03   # A_mu
  3  3     3.90000000e+03   # A_tau
#=====================================================================
#
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000001  3.61509310e+01   # dL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.27244080e-02    2      1000022         1                       
      2.86931190e-01    2      1000023         1                       
      6.89078360e-04    2      1000025         1                       
      4.10096240e-02    2      1000035         1                       
      5.63501720e-01    2     -1000024         2                       
      9.51439740e-02    2     -1000037         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000002  3.62105958e+01   # uL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05148220e-02    2      1000022         2                       
      2.94124510e-01    2      1000023         2                       
      3.81221270e-04    2      1000025         2                       
      3.58377170e-02    2      1000035         2                       
      5.97661380e-01    2      1000024         1                       
      6.14804030e-02    2      1000037         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000003  3.61509310e+01   # sL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.27244120e-02    2      1000022         3                       
      2.86931280e-01    2      1000023         3                       
      6.89078530e-04    2      1000025         3                       
      4.10096380e-02    2      1000035         3                       
      5.63501720e-01    2     -1000024         4                       
      9.51439590e-02    2     -1000037         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000004  3.62105958e+01   # cL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.05148180e-02    2      1000022         4                       
      2.94124360e-01    2      1000023         4                       
      3.81221120e-04    2      1000025         4                       
      3.58377060e-02    2      1000035         4                       
      5.97661500e-01    2      1000024         3                       
      6.14804100e-02    2      1000037         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000005  3.45675122e+01   # b1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.62266560e-02    2      1000022         5                       
      1.48197920e-01    2      1000023         5                       
      4.09865800e-03    2      1000025         5                       
      8.02130440e-03    2      1000035         5                       
      3.27243480e-01    2     -1000024         6                       
      3.90142890e-01    2     -1000037         6                       
      8.60691370e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000006  6.57411107e+01   # t1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      4.61775060e-02    2      1000022         6                       
      4.02834120e-02    2      1000023         6                       
      2.16190100e-01    2      1000025         6                       
      2.96111260e-01    2      1000035         6                       
      5.70370630e-02    2      1000024         5                       
      3.44200700e-01    2      1000037         5                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000001  1.68734619e+00   # dR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94297390e-01    2      1000022         1                       
      1.11637250e-03    2      1000023         1                       
      9.63119850e-04    2      1000025         1                       
      3.62315700e-03    2      1000035         1                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000002  6.74876191e+00   # uR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94297500e-01    2      1000022         2                       
      1.11636170e-03    2      1000023         2                       
      9.63101510e-04    2      1000025         2                       
      3.62308140e-03    2      1000035         2                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000003  1.68734619e+00   # sR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94297390e-01    2      1000022         3                       
      1.11637250e-03    2      1000023         3                       
      9.63119850e-04    2      1000025         3                       
      3.62315700e-03    2      1000035         3                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000004  6.74876191e+00   # cR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.94297500e-01    2      1000022         4                       
      1.11636180e-03    2      1000023         4                       
      9.63101510e-04    2      1000025         4                       
      3.62308180e-03    2      1000035         4                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000005  4.46812844e+01   # b2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.99326820e-02    2      1000022         5                       
      1.19070730e-01    2      1000023         5                       
      7.38693030e-03    2      1000025         5                       
      3.56134100e-02    2      1000035         5                       
      2.69563880e-01    2     -1000024         6                       
      4.54306390e-01    2     -1000037         6                       
      9.41259410e-02    2          -24   1000006                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000006  9.98710265e+01   # t2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.23924910e-01    2      1000024         5                       
      8.54418130e-02    2      1000037         5                       
      1.72039570e-01    2           23   1000006                       
      2.94111110e-02    2           24   1000005                       
      3.52814420e-02    2           24   2000005                       
      4.09369390e-02    2      1000022         6                       
      1.15935260e-01    2      1000023         6                       
      1.88841120e-01    2      1000025         6                       
      1.08187880e-01    2      1000035         6                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000011  3.95362806e+01   # eL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.25088380e-02    2      1000022        11                       
      2.76669950e-01    2      1000023        11                       
      1.50720110e-04    2      1000025        11                       
      2.84809130e-02    2      1000035        11                       
      5.15201450e-01    2     -1000024        12                       
      8.69881210e-02    2     -1000037        12                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000012  3.95671776e+01   # snue decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.84028730e-02    2      1000022        12                       
      2.55022050e-01    2      1000023        12                       
      9.91815580e-04    2      1000025        12                       
      4.24147700e-02    2      1000035        12                       
      5.46909330e-01    2      1000024        11                       
      5.62592150e-02    2      1000037        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000013  3.95362806e+01   # muL decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.25088380e-02    2      1000022        13                       
      2.76669950e-01    2      1000023        13                       
      1.50720110e-04    2      1000025        13                       
      2.84809130e-02    2      1000035        13                       
      5.15201450e-01    2     -1000024        14                       
      8.69881210e-02    2     -1000037        14                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000014  3.95671776e+01   # snum decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.84028730e-02    2      1000022        14                       
      2.55022050e-01    2      1000023        14                       
      9.91815580e-04    2      1000025        14                       
      4.24147700e-02    2      1000035        14                       
      5.46909330e-01    2      1000024        13                       
      5.62592150e-02    2      1000037        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000015  9.91967206e-01   # ta1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000016  4.01121336e+01   # snut decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.70629900e-02    2      1000022        16                       
      2.51549600e-01    2      1000023        16                       
      9.78310710e-04    2      1000025        16                       
      4.18372380e-02    2      1000035        16                       
      5.40242490e-01    2      1000024        15                       
      5.97004670e-02    2      1000037        15                       
      8.62883500e-03    2           37   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000011  9.91623478e-01   # eR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        11                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000013  9.91623478e-01   # muR decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.00000000e+00    2      1000022        13                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   2000015  4.00828208e+01   # ta2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.12768100e-02    2      1000022        15                       
      2.73281340e-01    2      1000023        15                       
      2.62122180e-03    2      1000025        15                       
      3.01976520e-02    2      1000035        15                       
      5.08174300e-01    2     -1000024        16                       
      8.58016610e-02    2     -1000037        16                       
      4.32754210e-03    2           36   1000015                       
      4.31945690e-03    2           35   1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000021  1.80858956e+00   # gl decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.74988750e-02    3      1000024         1        -2             
      1.74988750e-02    3     -1000024         2        -1             
      1.74988750e-02    3      1000024         3        -4             
      1.74988750e-02    3     -1000024         4        -3             
      4.59274580e-02    3      1000024         5        -6             
      4.59274580e-02    3     -1000024         6        -5             
      2.81998120e-03    3      1000037         1        -2             
      2.81998120e-03    3     -1000037         2        -1             
      2.81998120e-03    3      1000037         3        -4             
      2.81998120e-03    3     -1000037         4        -3             
      1.55650050e-01    3      1000037         5        -6             
      1.55650050e-01    3     -1000037         6        -5             
      3.29900070e-05    2      1000022        21                       
      3.98539470e-03    3      1000022         1        -1             
      3.98539470e-03    3      1000022         3        -3             
      3.95211950e-03    3      1000022         5        -5             
      3.14999260e-02    3      1000022         6        -6             
      2.82350030e-04    2      1000023        21                       
      1.95716270e-02    3      1000023         1        -1             
      1.95716270e-02    3      1000023         3        -3             
      1.95311660e-02    3      1000023         5        -5             
      3.18584400e-02    3      1000023         6        -6             
      1.15779230e-03    2      1000025        21                       
      3.83499970e-05    3      1000025         1        -1             
      3.83499970e-05    3      1000025         3        -3             
      9.81928200e-04    3      1000025         5        -5             
      1.58757090e-01    3      1000025         6        -6             
      7.91235240e-04    2      1000035        21                       
      2.75314760e-03    3      1000035         1        -1             
      2.75314760e-03    3      1000035         3        -3             
      3.54877070e-03    3      1000035         5        -5             
      2.10478830e-01    3      1000035         6        -6             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000023  5.42979706e-02   # N2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.09258500e-01    2      1000022        23                       
      8.21156620e-01    2      1000022        25                       
      8.93373410e-03    2      2000011       -11                       
      8.93373410e-03    2     -2000011        11                       
      8.93372950e-03    2      2000013       -13                       
      8.93372950e-03    2     -2000013        13                       
      1.69249610e-02    2      1000015       -15                       
      1.69249610e-02    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000025  2.81980978e+00   # N3 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      2.88220580e-01    2      1000024       -24                       
      2.88220580e-01    2     -1000024        24                       
      1.38747620e-01    2      1000022        23                       
      2.47785310e-01    2      1000023        23                       
      2.77611050e-02    2      1000022        25                       
      1.88987320e-03    2      1000023        25                       
      3.75864390e-04    2      2000011       -11                       
      3.75864390e-04    2     -2000011        11                       
      3.75864330e-04    2      2000013       -13                       
      3.75864330e-04    2     -2000013        13                       
      2.93572410e-03    2      1000015       -15                       
      2.93572410e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000035  3.21732330e+00   # N4 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.35583540e-01    2      1000024       -24                       
      3.35583540e-01    2     -1000024        24                       
      2.55405160e-02    2      1000022        23                       
      4.78350320e-03    2      1000023        23                       
      9.94262550e-02    2      1000022        25                       
      1.86921120e-01    2      1000023        25                       
      1.34731900e-03    2      2000011       -11                       
      1.34731900e-03    2     -2000011        11                       
      1.34731890e-03    2      2000013       -13                       
      1.34731890e-03    2     -2000013        13                       
      3.38623740e-03    2      1000015       -15                       
      3.38623740e-03    2     -1000015        15                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000024  5.23877746e-02   # C1 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.82765500e-01    2      1000022        24                       
      1.72346160e-02    2     -1000015        16                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY   1000037  3.13130352e+00   # C2 decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.24729750e-01    2      1000022        24                       
      3.51836000e-01    2      1000023        24                       
      4.23114420e-03    2     -1000015        16                       
      3.18609980e-01    2      1000024        23                       
      2.00593170e-01    2      1000024        25                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        25  4.56322795e-03   # h decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.96516560e-04    2           13       -13                       
      5.62112110e-02    2           15       -15                       
      2.35895070e-03    2            3        -3                       
      7.68931510e-01    2            5        -5                       
      3.34332620e-02    2            4        -4                       
      1.90806810e-03    2           22        22                       
      3.96505300e-02    2           21        21                       
      4.94488000e-03    3           24        11       -12             
      4.94488000e-03    3           24        13       -14             
      4.94488000e-03    3           24        15       -16             
      1.48346410e-02    3           24        -2         1             
      1.48346410e-02    3           24        -4         3             
      4.94488000e-03    3          -24       -11        12             
      4.94488000e-03    3          -24       -13        14             
      4.94488000e-03    3          -24       -15        16             
      1.48346410e-02    3          -24         2        -1             
      1.48346410e-02    3          -24         4        -3             
      5.67749430e-04    3           23        12       -12             
      5.67749430e-04    3           23        14       -14             
      5.67749430e-04    3           23        16       -16             
      2.85742630e-04    3           23        11       -11             
      2.85742630e-04    3           23        13       -13             
      2.85742630e-04    3           23        15       -15             
      9.78929920e-04    3           23         2        -2             
      9.78929920e-04    3           23         4        -4             
      1.26110300e-03    3           23         1        -1             
      1.26110300e-03    3           23         3        -3             
      1.26110300e-03    3           23         5        -5             
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        35  2.61429082e+00   # H decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      9.93817450e-05    2           13       -13                       
      2.84639260e-02    2           15       -15                       
      1.16504260e-03    2            3        -3                       
      2.87285360e-01    2            5        -5                       
      1.04799010e-05    2            4        -4                       
      4.78133230e-01    2            6        -6                       
      2.51794700e-04    2           21        21                       
      1.61854720e-03    2           24       -24                       
      8.21770110e-04    2           23        23                       
      4.13887710e-03    2      1000022   1000022                       
      2.78050420e-02    2      1000022   1000023                       
      1.28250120e-01    2      1000022   1000025                       
      2.00025350e-02    2      1000022   1000035                       
      4.69547700e-03    2      1000023   1000023                       
      1.04143210e-02    2      1000024  -1000024                       
      6.37230420e-03    2           25        25                       
      1.60431070e-04    2      2000011  -2000011                       
      1.60397930e-04    2      2000013  -2000013                       
      1.51061740e-04    2      1000015  -1000015                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        36  3.24988891e+00   # A decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      7.94814040e-05    2           13       -13                       
      2.27645490e-02    2           15       -15                       
      9.31807390e-04    2            3        -3                       
      2.29974420e-01    2            5        -5                       
      4.06604620e-01    2            6        -6                       
      2.41977770e-04    2           21        21                       
      4.58280460e-03    2      1000022   1000022                       
      4.91989220e-02    2      1000022   1000023                       
      2.55870020e-02    2      1000022   1000025                       
      6.80086540e-02    2      1000022   1000035                       
      6.02039060e-02    2      1000023   1000023                       
      1.30570050e-01    2      1000024  -1000024                       
      1.25187530e-03    2           25        23                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY        37  2.42466662e+00   # H+ decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      1.07557760e-04    2           14       -13                       
      3.08060030e-02    2           16       -15                       
      1.16898480e-03    2            4        -3                       
      7.61873190e-01    2            6        -5                       
      9.46839530e-02    2      1000024   1000022                       
      7.84965160e-05    2      1000024   1000023                       
      1.09565130e-01    2      1000037   1000022                       
      1.71656080e-03    2           25        24                       
#----------------------------------------------------------------------
#           PDG      WIDTH     
DECAY         6  1.14678979e+00   # t decays 
#          BR         NDA          ID1       ID2       ID3       ID4
      3.33333340e-01    3            2        -1         5             
      3.33333340e-01    3            4        -3         5             
      1.11111110e-01    3           12       -11         5             
      1.11111110e-01    3           14       -13         5             
      1.11111110e-01    3           16       -15         5             
