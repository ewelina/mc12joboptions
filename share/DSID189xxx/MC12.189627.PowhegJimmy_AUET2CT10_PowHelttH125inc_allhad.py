## JO for showering POWHEG LHEF showered with fHerwig

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
ServiceMgr.MessageSvc.OutputLevel = ERROR

#--------------------------------------------------------------
# Event related parameters
#--------------------------------------------------------------

# ... Main generator : fHerwig

#--------------------------------------------------------------
# Configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+fHerwig/Jimmy using PowHel ttH H->inclusive (hadronic top decays) with AUET2,CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "allhadronic"]
evgenConfig.contact = ["mamolla@cern.ch"]
evgenConfig.inputfilecheck = "PowHel-ttH_125"
evgenConfig.minevents = 2000

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
evgenConfig.generators += ["Powheg"]
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
topAlg.Herwig.HerwigCommand += ["iproc -199"] #==> LHE files and all Higgs decays (-199) #ID= 1-6 for quarks, 7-9 for leptons, 10/11 for W+W-/Z0Z0 pairs, and 12 for photons

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 0
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0
