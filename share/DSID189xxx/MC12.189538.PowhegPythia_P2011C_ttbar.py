include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011c tune'
evgenConfig.keywords    = [ 'top', 'ttbar', 'leptonic' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]

postGenerator="Pythia6TauolaPhotos"
process="tt_all"

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarWToLeptonFilter.py')
