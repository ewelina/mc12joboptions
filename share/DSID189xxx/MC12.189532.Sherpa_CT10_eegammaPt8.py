include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e e gamma production with up to three jets in ME+PS and pT_gamma>8 GeV."
evgenConfig.keywords = [ "gamma", "Z"]
evgenConfig.contact  = [ "frank.siegert@cern.ch" , "tatsuya.masubuchi@cern.ch" ]
evgenConfig.inputconfcheck="sherpa010403.189532.Sherpa_eegammaPt8"
evgenConfig.process="""

(run){
  ACTIVE[25]=0
  ERROR=0.05
}(run)

(processes){
  Process 93 93 ->  11 -11 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 10 7000
  PT 22  8 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
