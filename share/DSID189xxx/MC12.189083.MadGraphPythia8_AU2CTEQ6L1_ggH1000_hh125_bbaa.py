include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs H(1000)->hh(125)->bbaa"
evgenConfig.keywords = ["nonSMhiggs","hh","b"]
evgenConfig.inputfilecheck = 'Hhh_bbaa_mA1000'
