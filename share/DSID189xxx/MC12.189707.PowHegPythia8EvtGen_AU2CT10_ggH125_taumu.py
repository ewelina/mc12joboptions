evgenConfig.description = "POWHEG+PYTHIA8 ggH H->tautau->lh with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "taumu"]
evgenConfig.inputfilecheck = "ggH_SM_M125"
evgenConfig.auxfiles += ["HiggsTomutau.DEC"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

include("MC12JobOptions/Pythia8_EvtGen.py")
topAlg.EvtInclusiveDecay.userDecayFile = "HiggsTomutau.DEC"
topAlg.EvtInclusiveDecay.whiteList = [25]
topAlg.EvtInclusiveDecay.prohibitRemoveSelfDecay = True

