include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "b bbar -> Higgs [-> tau tau -> dileptonic(EF)] production with up to 3 jets in ME+PS."
evgenConfig.keywords = [ "nonSMhiggs", "ggF", "tau", "leptonic", "bbA"]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch" ]

evgenConfig.process="""
(run){
  MASS[25]=70 
  WIDTH[25]=0.1
  MASSIVE[15]=1
  MASSIVE[5]=1
  SOFT_SPIN_CORRELATIONS=1
}(run)

(processes){
  Process : 93 93 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 93 93 -> 25[a] 5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 93 -5 -> 25[a] -5 93{2}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 93 -5 -> 25[a] -5 5 -5
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 5 93 -> 25[a] 5 93{2}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 5 93 -> 25[a] 5 5 -5
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 5 5 -> 25[a] 5 5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : -5 -5 -> 25[a] -5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 5 -5 -> 25[a] 93{3}
  CKKW sqr(20.0/E_CMS)
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
  Process : 5 -5 -> 25[a] 5 -5 93{1}
  CKKW sqr(20.0/E_CMS)
  Cut_Core 1
  Decay : 25[a] -> 15 -15
  Order_EW : 2
  End process
  
}(processes)
  """

topAlg.Sherpa_i.Parameters += [ "DECAYFILE=HadronDecaysTauLL.dat" ]
topAlg.Sherpa_i.CrossSectionScaleFactor=0.123904

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ATauFilter
topAlg += ATauFilter()

ATauFilter = topAlg.ATauFilter
ATauFilter.llPtcute  = 12000.
ATauFilter.llPtcutmu = 8000.
ATauFilter.Etacut = 3.0

#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------

StreamEVGEN.RequireAlgs +=  [ "ATauFilter" ]

evgenConfig.minevents = 100
