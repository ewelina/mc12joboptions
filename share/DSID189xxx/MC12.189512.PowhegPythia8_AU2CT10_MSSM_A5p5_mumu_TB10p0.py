#########################
### Job Options File  ###
## PowHeg MSSM A->mumu ##
##    with Pythia8     ##
#########################

evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
evgenConfig.description = 'POWHEG MSSM h->mumu using CT10 PDF and PYTHIA8 with AU2_CT10 configuration'
evgenConfig.keywords = ['nonSMHiggs', 'pseudoscalar', 'ggF', 'mu']
evgenConfig.contact = ['renat@cern.ch']
evgenConfig.inputfilecheck = 'MSSM_A5p5_mumu_TB10p0'

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")

topAlg.Pythia8.Commands += [
                            'TimeShower:QEDshowerByL = on',
                            'Higgs:useBSM = on',
                            '36:onMode = off',#decay of Higgs
                            '36:onIfMatch = 13 13'
                           ]

include ( 'MC12JobOptions/MultiMuonFilter.py' )
topAlg.MultiMuonFilter.Ptcut = 2500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2




