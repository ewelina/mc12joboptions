evgenConfig.generators += [ 'Pythia' ]
evgenConfig.description = "SM Higgs gg->A->mumu with CTEQ6L1"
evgenConfig.keywords = ["NMSSM", "ggF", "2mu"]
evgenConfig.contact = ['renat@cern.ch']

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand  += [
# Higgs production
                         "pysubs msel 0",   # User control production
                         "pysubs msub 102 1",   # gg->h
                         "pydat2 pmas 25 1 8.0",  # Higgs mass    
           # Higgs decay
                         "pydat3 mdme 210 1 0",  # H -> dd
                         "pydat3 mdme 211 1 0",  # H -> uu
                         "pydat3 mdme 212 1 0",  # H -> ss
                         "pydat3 mdme 213 1 0",  # H -> cc
                         "pydat3 mdme 214 1 0",  # H -> bb
                         "pydat3 mdme 215 1 0",  # H -> tt
                         "pydat3 mdme 218 1 0",  # H -> e+e-
                         "pydat3 mdme 219 1 1",  # H -> mu+mu-      (on)
                         "pydat3 mdme 220 1 0",  # H -> tau+tau-
                         "pydat3 mdme 222 1 0",  # H -> gluon gluon
                         "pydat3 mdme 223 1 0",  # H -> gamma gamma
                         "pydat3 mdme 224 1 0",  # H -> gluon + gamma
                         "pydat3 mdme 225 1 0",  # H -> ZZ             
                         "pydat3 mdme 226 1 0"  # H -> WW
                         ]

include ( 'MC12JobOptions/MultiMuonFilter.py' )
topAlg.MultiMuonFilter.Ptcut = 2000.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2

