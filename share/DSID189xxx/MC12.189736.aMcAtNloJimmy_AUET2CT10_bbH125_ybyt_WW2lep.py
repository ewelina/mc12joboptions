evgenConfig.description = "MadGraph5_aMC@NLO+Herwig bbH production"
evgenConfig.keywords = ["bbH", "Higgs"]
evgenConfig.contact  = ["Junichi Tanaka <Junichi.Tanaka@cern.ch>"]
evgenConfig.inputfilecheck = 'MG5aMCAtNloHW6_bbH125_ybyt'

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
evgenConfig.generators += [ "aMcAtNlo" ]

topAlg.Herwig.HerwigCommand += ["iproc lhef"]
topAlg.Herwig.HerwigCommand += ["softme false",
                                "hardme false",
                                "ptmin 0.5D0"]
topAlg.Herwig.HerwigCommand += [ "iproc -110" ]
# W decay
topAlg.Herwig.HerwigCommand += [ "modbos 1 5" ]
topAlg.Herwig.HerwigCommand += [ "modbos 2 5" ]
topAlg.Herwig.HerwigCommand += [ "taudec TAUOLA" ]
