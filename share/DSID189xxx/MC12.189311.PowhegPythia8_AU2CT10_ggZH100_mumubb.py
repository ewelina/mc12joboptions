evgenConfig.description = "POWHEG+Pythia8 gg->ZH, Z->mumu, H->bb production and AU2 CT10 tune" 
evgenConfig.keywords = ["ggZH", "NLO", "Powheg", "Pythia8", "bottom", "muon"] 
evgenConfig.inputfilecheck = 'powheg.189311.ggZmumuH_SM_M100' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py") 
include("MC12JobOptions/Pythia8_Photos.py") 

topAlg.Pythia8.Commands += [ 
	 '25:onMode = off', 
	 '25:onIfMatch = 5 5' 
	 ]
