evgenConfig.description = "MadGraph+Pythia6 pp->X2->ZZ->4l(l=e,mu) with CT10"
evgenConfig.keywords    = [ "Spin2p" , "MG5", "Z", "leptonic", "SpinCP" ]
evgenConfig.generators  += [ "MadGraph" ]
evgenConfig.contact     = ["Rostislav Konoplich <rk60@nyu.edu>",
                           "Nikita Belyaev <nbelyaev@cern.ch>"]

TMPBASEIF = 'ggH125p5_Spin2p_kg1_kq0_ZZ4lep'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000
	
include("MC12JobOptions/Pythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Tauola.py" )
include("MC12JobOptions/Pythia_Photos.py")
                                                                                                       
topAlg.Pythia.PythiaCommand += [ "pyinit user lhef",
                                 "pyinit pylisti -1",
                                 "pyinit pylistf 1",
                                 "pyinit dumpr 1 2",
                                 "pydat3 mdcy 15 1 0",# Turn off tau decays.
                                 "pydat3 mdcy 23 1 0",# Turn off Z decays.
                                 "pydat3 mdcy 25 1 0",# Turn off Higgs decays.
                               ] 
### THE FOLLOWING LINES ARE ONLY NECESSARY IF YOU WANT THE MATCHING ME/PS
## the parameters need to be tuned
## see documentation of MadGraph matching in: http://cp3wks05.fynu.ucl.ac.be/twik...
phojf=open('./pythia_card.dat', 'w')
phojinp = """
! exclusive or inclusive matching
IEXCFILE=0
showerkt=T
qcut=40
"""
phojf.write(phojinp)
phojf.close()
