evgenConfig.description = "MadGraph5+Pythia8 for gg>t(b)ChargedHiggs"

evgenConfig.keywords = ["ChargedHiggs"]

evgenConfig.inputfilecheck = "tHpl_Whbb"

evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")
