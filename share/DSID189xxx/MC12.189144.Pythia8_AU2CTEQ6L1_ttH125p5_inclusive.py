evgenConfig.description = "PYTHIA8 ttH H->all with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "inclusive"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 125.5',
                            '25:mWidth = 0.00414',
                            '25:doForceWidth = true',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            ]
include("MC12JobOptions/Pythia8_H125p5_HXSWG_InclusiveBRs.py")
