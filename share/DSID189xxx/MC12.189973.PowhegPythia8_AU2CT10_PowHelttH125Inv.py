evgenConfig.description = "POWHEG+Pythia8 using PowHel ttH H->Invisible with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ttH", "invisible"]
evgenConfig.contact = ["bnachman@cern.ch"]
evgenConfig.inputfilecheck = "PowHel-ttH_125"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

assert hasattr(topAlg, "Pythia8")
topAlg.Pythia8.Commands += [
    "25:onMode = off",
    "25:onIfMatch = 23 23",
    "23:onMode = off",
    "23:onIfAny =12 12",
    ]
