evgenConfig.description = "POWHEG+PYTHIA8 using NWA, VBF H->hh-bbbb with AU2,CT10 "
evgenConfig.keywords = ["nonSMhiggs", "VBF", "di-higgs","b"]
evgenConfig.inputfilecheck = "VBFH35_NW_M500"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            'Higgs:useBSM on',
                            '35:onMode = off',#decay of Higgs
                            '35:onIfMatch = 25 25',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 5 5',
                            ]

# ... Filter H->VV->Children
#include("MC12JobOptions/XtoVVDecayFilter.py")
#topAlg.XtoVVDecayFilter.PDGGrandParent = 25
#topAlg.XtoVVDecayFilter.PDGParent = 25
#topAlg.XtoVVDecayFilter.StatusParent = 2
#topAlg.XtoVVDecayFilter.PDGChild1 = [5,5]
#topAlg.XtoVVDecayFilter.PDGChild2 = [5,5]
