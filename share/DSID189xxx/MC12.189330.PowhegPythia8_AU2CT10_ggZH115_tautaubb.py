evgenConfig.description = "POWHEG+Pythia8 gg->ZH, Z->tautau, H->bb production and AU2 CT10 tune" 
evgenConfig.keywords = ["ggZH", "NLO", "Powheg", "Pythia8", "bottom", "tau"] 
evgenConfig.inputfilecheck = 'powheg.189330.ggZtautauH_SM_M115' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py") 
include("MC12JobOptions/Pythia8_Photos.py") 

topAlg.Pythia8.Commands += [ 
	 '25:onMode = off', 
	 '25:onIfMatch = 5 5' 
	 ]
