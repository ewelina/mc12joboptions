evgenConfig.description = "VpT Weighted POWHEG+Pythia8 W->enuH->cc production and AU2 CT10 tune"
evgenConfig.keywords = ["WH" ,"NLO", "Powheg", "pythia8", "charm","electron" , "VpT Weighted"]
evgenConfig.inputfilecheck = 'powheg.189423.WmenuH_SM_M125_VpT_Weighted' 

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',
                            '25:onIfMatch = 4 4'
                            ]
