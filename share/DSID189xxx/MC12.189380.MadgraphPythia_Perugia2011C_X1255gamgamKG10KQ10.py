evgenConfig.description = "MG5 PYTHIA6 pp --> x2, x2 --> gammagamma with Perugia2011C"
evgenConfig.keywords = ["spin2", "Higgs", "gamma"]
evgenConfig.generators += [ "MadGraph" ]
evgenConfig.inputfilecheck = "gamgamkg10kq10"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user madgraph",  
                                "pyinit dumpr 1 12",
                                "pyinit pylistf 1"
                                ]

### THE FOLLOWING LINES ARE ONLY NECESSARY IF YOU WANT THE MATCHING ME/PS 
## the parameters need to be tuned
## see documentation of MadGraph matching in: http://cp3wks05.fynu.ucl.ac.be/twiki/bin/view/Software/Matching 
phojf=open('./pythia_card.dat', 'w')
phojinp = """
! exclusive or inclusive matching
      IEXCFILE=0
      showerkt=T
      qcut=30
      """
phojf.write(phojinp)
phojf.close()
