evgenConfig.description = "POWHEG+PYTHIA8 ggH CPS DYS H->ZZ->4nu with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "Z","nu"]
evgenConfig.inputfilecheck = "ggH_CPS_DYS_TopBottomLoop_SM_M110"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfMatch = 12 12',
                            '23:onIfMatch = 14 14',
                            '23:onIfMatch = 16 16'
                            ]
