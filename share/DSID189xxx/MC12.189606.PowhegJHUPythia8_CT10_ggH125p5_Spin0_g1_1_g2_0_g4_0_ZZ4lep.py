evgenConfig.description = "Powheg+JHU+PYTHIA8 ggH H->ZZ->4l (l=e,mu) with CT10 SpinCP 0"
evgenConfig.keywords    = ["SMhiggs", "ggF", "Z","leptonic", "SpinCP"]
evgenConfig.contact     = ["Rostislav Konoplich <rk60@nyu.edu>",
                           "Antonio Salvucci <antonio.salvucci@cern.ch>"]

TMPBASEIF = 'ggH125p5_Spin0_g1_1_g2_0_g4_0_ZZ4lep'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

evgenConfig.minevents = 5000

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_JHU.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '39:onMode = off',#decay of Higgs
                            '23:onMode = off'#decay of Z boson
                            ]
