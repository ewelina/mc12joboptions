include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs A(950)->Zh(125)->tautaubb"
evgenConfig.keywords = ["nonSMhiggs","Zh","b"]
evgenConfig.inputfilecheck = 'AZh_tautaubb_mA950'

evgenConfig.minevents=2000

topAlg.Pythia8.Commands += [
    '25:onMode = off',#decay of Higgs
    '25:onIfMatch = 5 5'
    ]

