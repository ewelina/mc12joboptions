include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')
include('MC12JobOptions/Pythia8_Photos.py')

evgenConfig.inputfilecheck = 'H260_hh_XXyy_nobbyy_8TeV'
evgenConfig.description = 'HeavyScalar 260.0 GeV production to XXyy excluding bbyy with the AU2 CTEQ6L1 tune with Madgraph5'
evgenConfig.keywords =["HeavyScalar","hh","XXyy"]
evgenConfig.contact = ["Xiaohu.Sun@cern.ch"]


#Pythia8 Commands
topAlg.Pythia8.Commands += [ "25:onMode = off",
                             "25:oneChannel = on 0.5 100 22 22 ", # setting yy with 50% br, all other channels rescale to sum=0.5 excluding bb
                             "25:addChannel = on 0.0750 100 15 -15", # tau tau
                             "25:addChannel = on 0.0003 100 13 -13", # mu mu
                             "25:addChannel = on 0.0345 100 4 -4", # c c
                             "25:addChannel = on 0.0003 100 3 -3", # s s
                             "25:addChannel = on 0.1017 100 21 21", # g g
                             "25:addChannel = on 0.0018 100 23 22", # Z y 
                             "25:addChannel = on 0.2551 100 24 -24", # W W
                             "25:addChannel = on 0.0313 100 23 23" # Z Z
                             ]

# Generator Filters
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 1560
topAlg.XtoVVDecayFilter.PDGParent = 25
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [22]
topAlg.XtoVVDecayFilter.PDGChild2 = [15, 13, 4, 3, 21, 23, 24 ] # no 22

StreamEVGEN.RequireAlgs = ["XtoVVDecayFilter"]

