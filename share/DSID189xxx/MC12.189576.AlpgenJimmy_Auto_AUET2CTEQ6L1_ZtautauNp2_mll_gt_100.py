evgenConfig.description = "Filtered Alpgen+Herwig Z(+jets)->tautau mll>100 Np2"
evgenConfig.keywords    = ["Z","tau"]
evgenConfig.contact     = ["Justin Griffiths <justin.griffiths@cern.ch>"]

include('MC12JobOptions/AlpgenPythia_WZjets.py')
evgenConfig.minevents = 1000
