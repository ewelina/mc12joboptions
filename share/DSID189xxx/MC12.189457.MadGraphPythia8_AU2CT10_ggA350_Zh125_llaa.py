include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

## For debugging only: print out some Pythia config info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]

evgenConfig.description = "non SM higgs A(350)->Zh(125)->llaa"
evgenConfig.keywords = ["nonSMhiggs","Zh","b"]
evgenConfig.inputfilecheck = 'AZh_llaa_mA350'
