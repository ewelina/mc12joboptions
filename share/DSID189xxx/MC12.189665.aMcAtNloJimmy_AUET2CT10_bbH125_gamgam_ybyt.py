evgenConfig.description = "MadGraph5_aMC@NLO+Herwig bbH production"
evgenConfig.keywords = ["bbH", "Higgs"]
evgenConfig.contact  = ["Junichi Tanaka <Junichi.Tanaka@cern.ch>"]
evgenConfig.inputfilecheck = 'MG5aMCAtNloHW6_bbH125_ybyt'

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
topAlg.Herwig.HerwigCommand += ["softme false",
                                "hardme false",
                                "ptmin 0.5D0"]
topAlg.Herwig.HerwigCommand += [ "iproc -112" ]
evgenConfig.generators += [ "aMcAtNlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
