evgenConfig.description = "MadGraph5_aMC@NLO+Herwig++ bbH production"
evgenConfig.keywords = ["bbH", "Higgs","W","leptonic"]
evgenConfig.contact  = ["Junichi Tanaka <Junichi.Tanaka@cern.ch>"]
evgenConfig.inputfilecheck = 'MG5aMCAtNloHWpp_bbH125_ybyt'
evgenConfig.generators = ["aMcAtNlo", "Herwigpp"]
evgenConfig.minevents = 2000

include("MC12JobOptions/Herwigpp_UEEE4_MRSTMCal_CT10ME_LHEF_Common.py")

cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost

set /Herwig/Particles/h0/h0->W+,W-;:OnOff 1
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 0
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## W boson
set /Herwig/Particles/W+/W+->bbar,c;:OnOff 0
set /Herwig/Particles/W+/W+->c,dbar;:OnOff 0
set /Herwig/Particles/W+/W+->c,sbar;:OnOff 0
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff 1
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff 1
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff 1
set /Herwig/Particles/W+/W+->sbar,u;:OnOff 0
set /Herwig/Particles/W+/W+->u,dbar;:OnOff 0
set /Herwig/Particles/W-/W-->b,cbar;:OnOff 0
set /Herwig/Particles/W-/W-->cbar,d;:OnOff 0
set /Herwig/Particles/W-/W-->cbar,s;:OnOff 0
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff 1
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff 1
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff 1
set /Herwig/Particles/W-/W-->s,ubar;:OnOff 0
set /Herwig/Particles/W-/W-->ubar,d;:OnOff 0

"""

topAlg.Herwigpp.Commands += cmds.splitlines()

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

topAlg.Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
topAlg.Multi1TLeptonFilter.Ptcut = 15000.
topAlg.Multi1TLeptonFilter.Etacut = 5.0
topAlg.Multi1TLeptonFilter.NLeptons = 1

topAlg.Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
topAlg.Multi2LLeptonFilter.Ptcut = 5000.
topAlg.Multi2LLeptonFilter.Etacut = 5.0
topAlg.Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
