evgenConfig.description = "AcerMCPythia6 singletop-tchan-had production with P2011C CTEQ6L1 tune 4jets20 eta4"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "tchan", "hadronic"]
evgenConfig.contact  = ["saverio.dauria@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan_had"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/MultiJetFilterAkt4.py")
topAlg.QCDTruthMultiJetFilter.Njet = 4
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 20.*GeV
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = -1.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = float(runArgs.ecmEnergy)*GeV
topAlg.QCDTruthMultiJetFilter.MaxEta = 3.6
topAlg.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthMultiJetFilter.DoShape = False
