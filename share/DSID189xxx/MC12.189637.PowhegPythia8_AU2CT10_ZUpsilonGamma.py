evgenConfig.description = "POWHEG+PYTHIA8 Z"
evgenConfig.keywords = ["SM", "Z","leptonic"]
evgenConfig.inputfilecheck = "ZUpsilonGamma"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
#                            '25:onIfMatch = 23 23',
#                            '23:onMode = off',#decay of Z
#                            '23:mMin = 2.0',
#                            '23:onIfMatch = 11 11',
#                            '23:onIfMatch = 13 13'
                            ]

