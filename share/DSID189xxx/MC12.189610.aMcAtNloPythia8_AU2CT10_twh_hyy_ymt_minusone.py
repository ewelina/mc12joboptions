#
# Job options file for aMC@NLO WtH showering with Pythia8
# by Giuseppe Salamanna (QMUL) / Andrey Loginov (Yale)
#
evgenConfig.description = "Generic WtH production"
evgenConfig.keywords = ["WtH", "generic", "yukawa", "top", "coupling", "Higgs"]
evgenConfig.contact  = ["Andrey Loginov <andrey.loginov@yale.edu>"]
#
evgenConfig.inputfilecheck = 'twh_hyy_np_ct10'
#
include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
#
#
topAlg.Pythia8.Commands += [
    "SpaceShower:pTmaxMatch = 1",
    "SpaceShower:pTmaxFudge = 1",
    "SpaceShower:MEcorrections = off",
    "TimeShower:pTmaxMatch = 1",
    "TimeShower:pTmaxFudge = 1",
    "TimeShower:MEcorrections = off",
    "TimeShower:globalRecoil = on",
#    "TimeShower:limitPTmaxGlobal = on",
    "TimeShower:nMaxGlobalRecoil = 1",
#    "TimeShower:globalRecoilMode = 2",
#    "TimeShower:nMaxGlobalBranch = 1"
    ]
#
topAlg.Pythia8.Commands += [
    '25:onMode = off', # switch OFF all Higgs decay channels
#    '25:onIfMatch = 24 24' # H -> WW
#    '25:onIfMatch = 5 5'   # H -> bbbar
    '25:onIfMatch = 22 22' # H -> gamma gamma
#    '25:onIfMatch = 23 23' # H -> ZZ
#    '25:onIfMatch = 15 15' # H -> tau tau
    ]
#
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]

# OUTPUT PRINTOUT LEVEL
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
# you can override this for individual modules if necessary
MessageSvc = Service( "MessageSvc" )
MessageSvc.OutputLevel               = 5
