evgenConfig.description = "Filtered Alpgen+Herwig Z(+jets)->tautau mll>100 Np5"
evgenConfig.keywords    = ["Z","tau"]
evgenConfig.contact     = ["Justin Griffiths <justin.griffiths@cern.ch>"]

include('MC12JobOptions/AlpgenPythia_WZjets.py')
evgenConfig.inputconfcheck = "Alpgen_CTEQ6L1_Zee_Np5"
evgenConfig.minevents = 20
