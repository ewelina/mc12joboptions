evgenConfig.description = "PYTHIA8 ZH H->tautau->lh with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH", "taumu"]
evgenConfig.auxfiles += ["HiggsTomutau_phasespace.DEC"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            '25:mWidth = 0.00407',
                            '25:doForceWidth = true',
                            'HiggsSM:ffbar2HZ = on',
                            '23:onMode = off',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]



include("MC12JobOptions/Pythia8_EvtGen.py")
topAlg.EvtInclusiveDecay.userDecayFile = "HiggsTomutau_phasespace.DEC"
topAlg.EvtInclusiveDecay.whiteList = [25]
topAlg.EvtInclusiveDecay.prohibitRemoveSelfDecay = True


