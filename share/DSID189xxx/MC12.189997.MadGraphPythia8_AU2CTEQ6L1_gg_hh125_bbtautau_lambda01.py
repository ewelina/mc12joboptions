# Setting PDF
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

# Loading LHE jobOptions
include("MC12JobOptions/Pythia8_LHEF.py")

# Settings for LHE interface
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = "gg>hh at LO with MadGraph5_aMC@NLO, Higgs decays to bbtautau using Pythia8, to be showered with Pythia8 using CTEQ6L1 PDFs"
evgenConfig.keywords = ["hh","bbtautau","lambda=1"]

evgenConfig.inputfilecheck = 'gg_hh125_bbtautau_lambda01'
