evgenConfig.description = "Filtered Alpgen+Pythia Z(+jets)->tautau mll>100 Np3"
evgenConfig.keywords    = ["Z","tau"]
evgenConfig.contact     = ["Justin Griffiths <justin.griffiths@cern.ch>"]

include('MC12JobOptions/AlpgenPythia_WZjets.py')
evgenConfig.minevents = 1000
