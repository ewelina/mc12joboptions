include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_MadGraph.py')
include('MC12JobOptions/Pythia8_Photos.py')

evgenConfig.inputfilecheck = 'sm_hh' # in case of bbgamga: evgenConfig.inputfilecheck = 'sm_hh_yybb'
evgenConfig.description = 'standard model dihiggs production, to bbtautau, with Madgraph includes the box'
evgenConfig.keywords =["SM", "hh", "non-resonant", "bbtautau"]
evgenConfig.contact = ["keita.hanawa@cern.ch"]


#Pythia8 Commands
topAlg.Pythia8.Commands += ["25:oneChannel = on 0.5 100 5 -5 ", # turn on b bbar
                            "25:addChannel = on 0.5 100 15 -15 "] # turn on tau tau

# Generator Filters
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
topAlg += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
topAlg += ParentChildFilter("htautauFilter", PDGParent = [25], PDGChild = [15])
StreamEVGEN.RequireAlgs = ["hbbFilter", "htautauFilter"]
# ... Filter  H->VV->Children
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 15
topAlg.XtoVVDecayFilter.StatusParent = 2
topAlg.XtoVVDecayFilter.PDGChild1 = [11,13]
topAlg.XtoVVDecayFilter.PDGChild2 = [111,130,211,221,223,310,311,321,323] 
