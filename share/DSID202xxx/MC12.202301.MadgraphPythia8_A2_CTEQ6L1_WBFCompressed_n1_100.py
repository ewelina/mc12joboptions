include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "WBF SUSY"
evgenConfig.keywords = ["WBF", "SUSY", "Compressed spectra"]
evgenConfig.contact  = ["andrew.james.nelson@cern.ch"]
evgenConfig.minevents = 4000

evgenConfig.inputfilecheck = 'bh_grid'

evgenConfig.auxfiles += [ "susy_bh_grid_100.202301.slha" ]

topAlg.Pythia8.Commands += ["SLHA:file = susy_bh_grid_100.202301.slha"]
