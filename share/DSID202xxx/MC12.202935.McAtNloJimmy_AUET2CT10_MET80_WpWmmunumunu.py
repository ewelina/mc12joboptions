## MC@NLO+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable MC@NLO mode


include('MC12JobOptions/Jimmy_AUET2_CT10_Common.py')

topAlg.Herwig.HerwigCommand += ['iproc mcatnlo']
topAlg.Herwig.HerwigCommand += [ "rmass 6 0.1725E+03"]
topAlg.Herwig.HerwigCommand += [ "rmass 198 0.8042E+02"]
topAlg.Herwig.HerwigCommand += [ "rmass 199 0.8042E+02"]
topAlg.Herwig.HerwigCommand += [ "gamw 0.2124E+01"]

include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')

evgenConfig.generators += [ 'McAtNlo', 'Herwig' ]
evgenConfig.description = 'MC@NLO WW->lnulnu using CT10 PDF and fHerwig with AUET2_CT10 configuration'
evgenConfig.keywords = ['EW', 'diboson', 'leptonic']
evgenConfig.contact = ['kerim.suruliz@cern.ch']
evgenConfig.inputfilecheck = 'mcatnlo4.202935.WW_mm'

include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

from GeneratorFilters.GeneratorFiltersConf import METFilter
topAlg += METFilter()
       
METFilter = topAlg.METFilter
METFilter.MissingEtCut = 80.0*GeV
evgenLog.info("Using a MET filter lower cut at " + str(METFilter.MissingEtCut) + " MeV.")

try:
    METFilter.MissingEtUpperCut = runArgs.METfilterUpperCut*GeV
    evgenLog.info("Using a MET filter upper cut at " + str(METFilter.MissingEtUpperCut) + " MeV.")
except:
    evgenLog.info("Not using a MET filter upper cut.")
    METFilter.MissingEtCalcOption = 1
    
#---------------------------------------------------------------
# POOL / Root output
#---------------------------------------------------------------
 
StreamEVGEN.RequireAlgs +=  [ "METFilter" ]
