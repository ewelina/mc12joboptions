evgenConfig.description = "fully leptonic tW + gamma (in top decay) Whizard+Pythia6, P2011C CTEQ6L1 tune"
evgenConfig.keywords = ["top", "singletop", "tW", "leptonic", "photon"]
evgenConfig.inputfilecheck = "tW_dilep_gammatDec"
evgenConfig.generators = ["Whizard", "Pythia"]

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
 
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 80000.
PhotonFilter.Etacut = 4
PhotonFilter.NPhotons = 1

StreamEVGEN.AcceptAlgs +=  [ "PhotonFilter" ]
