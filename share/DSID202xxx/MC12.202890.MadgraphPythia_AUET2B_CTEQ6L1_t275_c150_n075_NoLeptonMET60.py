include( 'MC12JobOptions/MadGraphControl_SM_TT_onestepBB.py' )
include( 'MC12JobOptions/LeptonFilter.py' )

topAlg.LeptonFilter.Ptcut = -1.
topAlg.LeptonFilter.Etacut = 2.8
topAlg.LeptonFilter.PtcutMax = 10000.

include("MC12JobOptions/METFilter.py")
