include('MC12JobOptions/MadGraphControl_ttbargamma.py')

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------

from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
topAlg += PhotonFilter()
 
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 80000.
PhotonFilter.Etacut = 4
PhotonFilter.NPhotons = 1

StreamEVGEN.AcceptAlgs +=  [ "PhotonFilter" ]
