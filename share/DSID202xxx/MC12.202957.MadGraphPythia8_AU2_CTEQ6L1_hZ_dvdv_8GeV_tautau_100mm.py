
evgenConfig.description = "ZH 125 GeV, with the AU2 CTEQ6L1 tune, to 2 displaced vertices"
evgenConfig.keywords = ["ZH dvdv"]

#Tune
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

#Photos
include("MC12JobOptions/Pythia8_Photos.py")

#Pythia8 Commands
topAlg.Pythia8.Commands += ["Higgs:useBSM on", 
#                          "HiggsBSM:allH2 on",
                           "HiggsBSM:ffbar2H2Z on", #2H2Z or 2H2W (dont forget to change gen filter below)
                           "35:m0 125.0", #H0 mass
                           "35:onMode = off", #turn off all H0 decays
                           "35:onIfMatch = 25 25", #H0->hh

                           "25:m0 8.0", #h mass
                           "25:tau0 100.0", #h lifetime

		           "25:onMode = off", #turn off all h decays
#	                   "25:onIfAny = 5", #h->bb
                           "25:onIfAny = 15", #h->tautau

                           "24:onMode = off", #turn off all W decays
 	                   "24:onIfAny = 11 13 15", #W->e,mu,tau

	                   "23:onMode = off", #turn off all Z decays
#                          "23:onIfAny = 12 14 16", #Z->vv
	                   "23:onIfAny = 11 13 15"] #Z->ee,mumu,tautau

# Generator Filters
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()
MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 20000.
MultiElecMuTauFilter.MaxEta = 2.5
MultiElecMuTauFilter.IncludeHadTaus = 0
StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

