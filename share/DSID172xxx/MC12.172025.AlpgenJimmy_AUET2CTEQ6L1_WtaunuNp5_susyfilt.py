evgenConfig.description = "ALPGEN+Jimmy W(->taunu) process with AUET2 tune and pdf CTEQ6L1 and SUSY MET/jet filter"
evgenConfig.contact = ["mark.hohlfeld@cern.ch"]
evgenConfig.keywords = ["EW", "W", "tau"]
evgenConfig.inputfilecheck = "WtaunuNp5"
evgenConfig.minevents = 5000
	
include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
include("MC12JobOptions/METFilter.py")
topAlg.METFilter.MissingEtCut = 100*GeV
topAlg.METFilter.MissingEtCalcOption = 1
