evgenConfig.description = "POWHEG+PYTHIA8 VBFH H->bb with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "bottom"]
evgenConfig.inputfilecheck = "VBFH_SM_M120"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 5 5'
                            ]
