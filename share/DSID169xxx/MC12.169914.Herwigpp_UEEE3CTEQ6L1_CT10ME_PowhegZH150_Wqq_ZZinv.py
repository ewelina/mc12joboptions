evgenConfig.description = "WH W->qq H->invisible with UEEE3, CTEQ6L1"
evgenConfig.keywords = ["SMHiggs", "W","nu","hadronic"]
evgenConfig.contact = ['calfayan@cern.ch']

include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_Common.py' )

## Add to commands
cmds = """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG
## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Set up Higgs + W process at NLO (set jet pT cut to zero so no cut on W decay products)
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2WH
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:FactorizationScaleOption Dynamic
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV

## Higgs particle
set /Herwig/Particles/h0:NominalMass 150*GeV
set /Herwig/Particles/h0:Width 0.0173*GeV
set /Herwig/Particles/h0:WidthCut 0.0173*GeV
set /Herwig/Particles/h0:WidthLoCut 0.0173*GeV
set /Herwig/Particles/h0:WidthUpCut 0.0173*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 1
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## W boson
set /Herwig/Particles/W+/W+->bbar,c;:OnOff 1
set /Herwig/Particles/W+/W+->c,dbar;:OnOff 1
set /Herwig/Particles/W+/W+->c,sbar;:OnOff 1
set /Herwig/Particles/W+/W+->nu_e,e+;:OnOff 0
set /Herwig/Particles/W+/W+->nu_mu,mu+;:OnOff 0
set /Herwig/Particles/W+/W+->nu_tau,tau+;:OnOff 0
set /Herwig/Particles/W+/W+->sbar,u;:OnOff 1
set /Herwig/Particles/W+/W+->u,dbar;:OnOff 1
set /Herwig/Particles/W-/W-->b,cbar;:OnOff 1
set /Herwig/Particles/W-/W-->cbar,d;:OnOff 1
set /Herwig/Particles/W-/W-->cbar,s;:OnOff 1
set /Herwig/Particles/W-/W-->nu_ebar,e-;:OnOff 0
set /Herwig/Particles/W-/W-->nu_mubar,mu-;:OnOff 0
set /Herwig/Particles/W-/W-->nu_taubar,tau-;:OnOff 0
set /Herwig/Particles/W-/W-->s,ubar;:OnOff 1
set /Herwig/Particles/W-/W-->ubar,d;:OnOff 1
"""

## Set commands
topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 23
topAlg.XtoVVDecayFilter.StatusParent = 11 #Herwig++
topAlg.XtoVVDecayFilter.PDGChild1 = [12,14,16]
topAlg.XtoVVDecayFilter.PDGChild2 = [12,14,16]

