evgenConfig.description = "Pythia8 e/gamma performance sample (jets, gamma+jet, W/Z, ttbar)"
evgenConfig.keywords = ["egamma", "performance", "jets"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

## Configure Pythia
topAlg.Pythia8.Commands += ["HardQCD:all = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "WeakSingleBoson:all = on",
                            "Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "PhaseSpace:pTHatMin = 15",
                            "PhaseSpace:mHatMin = 30"]
