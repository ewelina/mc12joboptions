evgenConfig.description = "PYTHIA8 ttH H->ZZ->inclusive with AU2 CT10"
evgenConfig.generators = ["Pythia8"]
evgenConfig.keywords = ["SMhiggs", "ttH", "Z","inclusive"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 125',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 23 23',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]
