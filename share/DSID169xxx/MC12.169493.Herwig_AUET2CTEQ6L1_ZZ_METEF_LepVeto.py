evgenConfig.description = "Herwig ZZ inclusive"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson","Z"]
evgenConfig.contact  = ["thorsten.kuhl@cern.ch"]
#
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12810"]    # note that 10000 is added to the herwig process number
# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )
# Add the filters:
# Electron or Muon filter
##include("MC12JobOptions/LeptonFilter.py")
if not hasattr(topAlg, "LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    topAlg += LeptonFilter()
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8

# MissingEt filter
#include("MC12JobOptions/MissingEtFilter.py")
if not hasattr(topAlg, "MissingEtFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MissingEtFilter
    topAlg += MissingEtFilter()
topAlg.MissingEtFilter.MEtcut = 30000.

StreamEVGEN.RequireAlgs += [ "MissingEtFilter" ]
StreamEVGEN.VetoAlgs += [ "LeptonFilter" ]

