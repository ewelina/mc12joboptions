evgenConfig.description = "POWHEG+Pythia8 ZZ_4e production mll>4 GeV, with mass filter m4l>500GeV and m4l<50TeV and muon filter pt>3GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.minevents = 2000
evgenConfig.contact = ["Antonio Salvucci <antonio.salvucci@cern.ch>"]

TMPBASEIF = 'ZZ_4e'
if runArgs.ecmEnergy == 7000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_7TeV'
elif runArgs.ecmEnergy == 8000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_8TeV'
elif runArgs.ecmEnergy == 13000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_13TeV'
elif runArgs.ecmEnergy == 14000:
    evgenConfig.inputfilecheck = TMPBASEIF+'_14TeV'
else:
    raise Exception("Incompatible inputGeneratorFile")

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

include("MC12JobOptions/FourLeptonInvMassFilter.py")
topAlg.FourLeptonInvMassFilter.MinPt = 3.*GeV
topAlg.FourLeptonInvMassFilter.MaxEta = 5.
topAlg.FourLeptonInvMassFilter.MinMass = 500.*GeV
topAlg.FourLeptonInvMassFilter.MaxMass = 50000.*GeV
