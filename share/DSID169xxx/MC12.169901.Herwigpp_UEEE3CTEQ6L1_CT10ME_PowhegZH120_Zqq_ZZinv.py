evgenConfig.description = "ZH Z->qq H->invisible with UEEE3, CTEQ6L1"
evgenConfig.keywords = ["SMHiggs", "Z","nu","hadronic"]
evgenConfig.contact = ['calfayan@cern.ch']

include ( "MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_CT10ME_Common.py" )

## Add to commands
cmds = """
## Set up Powheg truncated shower
set /Herwig/Shower/Evolver:HardEmissionMode POWHEG
## Use 2-loop alpha_s
create Herwig::O2AlphaS /Herwig/AlphaQCD_O2
set /Herwig/Generators/LHCGenerator:StandardModelParameters:QCD/RunningAlphaS /Herwig/AlphaQCD_O2

## Set up Higgs + Z process at NLO (set jet pT cut to zero so no cut on Z decay products)
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/PowhegMEPP2ZH
set /Herwig/MatrixElements/SimpleQCD:MatrixElements[0]:FactorizationScaleOption Dynamic
set /Herwig/Cuts/JetKtCut:MinKT 0.0*GeV

## Higgs particle
set /Herwig/Particles/h0:NominalMass 120*GeV
set /Herwig/Particles/h0:Width 0.00347*GeV
set /Herwig/Particles/h0:WidthCut 0.00347*GeV
set /Herwig/Particles/h0:WidthLoCut 0.00347*GeV
set /Herwig/Particles/h0:WidthUpCut 0.00347*GeV
set /Herwig/Particles/h0/h0->W+,W-;:OnOff 0
set /Herwig/Particles/h0/h0->Z0,Z0;:OnOff 1
set /Herwig/Particles/h0/h0->b,bbar;:OnOff 0
set /Herwig/Particles/h0/h0->c,cbar;:OnOff 0
set /Herwig/Particles/h0/h0->g,g;:OnOff 0
set /Herwig/Particles/h0/h0->gamma,gamma;:OnOff 0
set /Herwig/Particles/h0/h0->mu-,mu+;:OnOff 0
set /Herwig/Particles/h0/h0->t,tbar;:OnOff 0
set /Herwig/Particles/h0/h0->tau-,tau+;:OnOff 0

## Z boson
set /Herwig/Particles/Z0/Z0->u,ubar;:OnOff 1
set /Herwig/Particles/Z0/Z0->d,dbar;:OnOff 1
set /Herwig/Particles/Z0/Z0->c,cbar;:OnOff 1
set /Herwig/Particles/Z0/Z0->s,sbar;:OnOff 1
set /Herwig/Particles/Z0/Z0->b,bbar;:OnOff 1
set /Herwig/Particles/Z0/Z0->e-,e+;:OnOff 0
set /Herwig/Particles/Z0/Z0->mu-,mu+;:OnOff 0
set /Herwig/Particles/Z0/Z0->tau-,tau+;:OnOff 0
set /Herwig/Particles/Z0/Z0->nu_e,nu_ebar;:OnOff 1
set /Herwig/Particles/Z0/Z0->nu_mu,nu_mubar;:OnOff 1
set /Herwig/Particles/Z0/Z0->nu_tau,nu_taubar;:OnOff 1

"""

## Set commands
topAlg.Herwigpp.Commands += cmds.splitlines()


# H->ZZ->4nu Filter
include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 23
topAlg.XtoVVDecayFilter.StatusParent = 11 #Herwig++
topAlg.XtoVVDecayFilter.PDGChild1 = [12,14,16]
topAlg.XtoVVDecayFilter.PDGChild2 = [12,14,16]

# Z->qq Filter
include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent  = [23]
topAlg.ParentChildFilter.PtMinParent =  -1e12
topAlg.ParentChildFilter.PtMaxParent = 1e12
topAlg.ParentChildFilter.EtaRangeParent = 1e12
topAlg.ParentChildFilter.MassMinParent = -1e12
topAlg.ParentChildFilter.MassMaxParent = 1e12
topAlg.ParentChildFilter.PDGChild = [1,2,3,4,5]
topAlg.ParentChildFilter.PtMinChild = -1e12
topAlg.ParentChildFilter.EtaRangeChild = 1e12
