include('MC12JobOptions/AlpgenControl_Wqq.py') 
include("MC12JobOptions/JetFilterAkt4.py")

from GeneratorFilters.GeneratorFiltersConf import TruthJetFilter
if "TruthJetFilter" not in topAlg:
    topAlg += TruthJetFilter()
if "TruthJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["TruthJetFilter"]

topAlg.TruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.TruthJetFilter.Njet = 2
topAlg.TruthJetFilter.NjetMinPt = 15.*GeV
topAlg.TruthJetFilter.NjetMaxEta = 2.7
topAlg.TruthJetFilter.jet_pt1 =25.*GeV
topAlg.TruthJetFilter.applyDeltaPhiCut=False

evgenConfig.minevents = 5000 

