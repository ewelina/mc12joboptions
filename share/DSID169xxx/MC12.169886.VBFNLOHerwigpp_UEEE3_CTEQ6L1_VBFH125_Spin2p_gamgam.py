evgenConfig.description = "VBFNLO VBF X->gamma gamma with UEEE3,CTEQ6L1 SpinCP 2p"
evgenConfig.keywords = ["vbf", "higgs", "gamma", "SpinCP"]
evgenConfig.inputfilecheck = "vbf_H125_gamgam_Spin2p"

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_LHEF_Common.py")
evgenConfig.generators += ["VBFNLO"]

cmds = """
create ThePEG::ParticleData graviton
setup graviton 39 G 125.0 0.0 0.0 0.0 3 0 1 0

decaymode graviton->gamma,gamma; 1.0 1 /Herwig/Decays/Mambo
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

