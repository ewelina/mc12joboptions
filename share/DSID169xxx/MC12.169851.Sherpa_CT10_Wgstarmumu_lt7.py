include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W(->lv) + gamma*(->mumu) with mumu between 2mmu and 7 GeV"
evgenConfig.keywords    = ["diboson", "Wgamma*", "Wgammastar"]
evgenConfig.contact     = ["david.hall@cern.ch"]
evgenConfig.minevents   = 10

evgenConfig.process = """
(run){
  INT_MINSIJ_FACTOR=1.e-14
  ERROR=0.01
  ACTIVE[25]=0
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 13 -13 -11 12;
  Order_EW 4
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 13 -13 -13 14;
  Order_EW 4
  Selector_File *|(selector2){|}(selector2)
  End process;

  Process 93 93 -> 13 -13 -15 16;
  Order_EW 4
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 13 -13 11 -12;
  Order_EW 4
  Selector_File *|(selector1){|}(selector1)
  End process;

  Process 93 93 -> 13 -13 13 -14;
  Order_EW 4
  Selector_File *|(selector2){|}(selector2)
  End process;

  Process 93 93 -> 13 -13 15 -16;
  Order_EW 4
  Selector_File *|(selector1){|}(selector1)
  End process;
}(processes)

(selector1){
  "m" 13,-13 0.2114,7.0
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -3.0,3.0:-3.0,3.0 [PT_UP]
}(selector1)

(selector2){
  MinSelector {
    "m" 13,-13 0.2114,7.0:0.2114,E_CMS
    "m" 13,-13 0.2114,E_CMS:0.2114,7.0
  }
  "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
  "Eta" 991 -3.0,3.0:-3.0,3.0 [PT_UP]
}(selector2)
"""
