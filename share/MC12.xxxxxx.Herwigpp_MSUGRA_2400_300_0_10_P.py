evgenConfig.description = 'MSUGRA grid generation with m_0 = 2400, m_1/2 = 300, tan beta = 10, A_0 = 0, mu>0'
evgenConfig.generators = ['Herwigpp']
evgenConfig.keywords = ['SUSY']
evgenConfig.contact  = ['teng.jian.khoo@cern.ch']
evgenConfig.auxfiles += ['susy*.txt', 'MSSM.model']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('gluino')
sparticle_list.append('squarks')
sparticle_list.append('sbottoms')
sparticle_list.append('stops')
slha_file = 'susy_2400_300_0_10_P_softsusy.slha.txt'
cmds = buildHerwigppCommands(sparticle_list, slha_file)

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds
