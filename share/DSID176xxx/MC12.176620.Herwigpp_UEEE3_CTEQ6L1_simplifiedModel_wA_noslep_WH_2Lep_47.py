## Herwig++ job option file for Susy 2-parton -> 2-sparticle processes

## Get a handle on the top level algorithms' sequence
from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.SMModeADG')

if not 'evgenConfig' in dir():
    raise RuntimeError('These jobOptions should be run through Generate_trf.py')

## Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_simplifiedModel_wA_nosl_WH_176620.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )

# Add Herwig++ parameters for this process
cmds = buildHerwigppCommands(['~chi_1+','~chi_20'],slha_file,'Exclusive')

## Define metadata
evgenConfig.description = 'Simplified Model Mode A grid generation for direct gaugino search...'
evgenConfig.keywords    = ['SUSY','Direct Gaugino','Simplified Models','chargino','neutralino','Higgs']
evgenConfig.contact     = ['alaettin.serhan.mete@cern.ch']

## Print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()

# Generator Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter()

MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
MultiElecMuTauFilter.NLeptons  = 2
MultiElecMuTauFilter.MinPt = 5000.
MultiElecMuTauFilter.MaxEta = 2.7
MultiElecMuTauFilter.MinVisPtHadTau = 10000. # pt-cut on the visible hadronic tau
MultiElecMuTauFilter.IncludeHadTaus = 1      # include hadronic taus

StreamEVGEN.RequireAlgs = [ "MultiElecMuTauFilter" ]

## Clean up
del cmds
