MODEL = 'generic'
CASE = 'gluino'
MASS = 700
GBALLPROB = 0.1

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic gluino gball0.1 700GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)

topAlg.PythiaRhad.PythiaCommand += [ "pypars parp 64 4", ]
