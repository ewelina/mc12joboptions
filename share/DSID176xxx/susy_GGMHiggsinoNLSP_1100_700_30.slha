#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     7.00000000E+02   # mu(EWSB)            
        25     3.00000000E+01   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.04986013E+01   # W+
        25     1.26000000E+02   # h
        35     1.99995076E+03   # H
        36     2.00000000E+03   # A
        37     2.00137250E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50116253E+03   # ~d_L
   2000001     1.50021911E+03   # ~d_R
   1000002     1.49905585E+03   # ~u_L
   2000002     1.49956168E+03   # ~u_R
   1000003     1.50116253E+03   # ~s_L
   2000003     1.50021911E+03   # ~s_R
   1000004     1.49905585E+03   # ~c_L
   2000004     1.49956168E+03   # ~c_R
   1000005     1.48315014E+03   # ~b_1
   2000005     1.51803307E+03   # ~b_2
   1000006     1.50659018E+03   # ~t_1
   2000006     1.50912161E+03   # ~t_2
   1000011     1.50072455E+03   # ~e_L
   2000011     1.50065724E+03   # ~e_R
   1000012     1.49861726E+03   # ~nu_eL
   1000013     1.50072455E+03   # ~mu_L
   2000013     1.50065724E+03   # ~mu_R
   1000014     1.49861726E+03   # ~nu_muL
   1000015     1.48791331E+03   # ~tau_1
   2000015     1.51336279E+03   # ~tau_2
   1000016     1.49861726E+03   # ~nu_tauL
   1000021     1.10000000E+03   # ~g
   1000022     6.86008778E+02   # ~chi_10
   1000023    -7.02282838E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.01627430E+03   # ~chi_40
   1000024     6.90870231E+02   # ~chi_1+
   1000037     1.01260430E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     1.00290658E-01   # N_11
  1  2    -1.79542976E-01   # N_12
  1  3     6.98495667E-01   # N_13
  1  4    -6.85426806E-01   # N_14
  2  1     1.78630457E-02   # N_21
  2  2    -3.19788117E-02   # N_22
  2  3    -7.05401487E-01   # N_23
  2  4    -7.07860869E-01   # N_24
  3  1     8.73024280E-01   # N_31
  3  2     4.87676743E-01   # N_32
  3  3     3.23205249E-06   # N_33
  3  4    -4.56499012E-06   # N_34
  4  1     4.76930086E-01   # N_41
  4  2    -8.53757614E-01   # N_42
  4  3    -1.20468026E-01   # N_43
  4  4     1.70654867E-01   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -1.52299040E-01   # U_11
  1  2     9.88334459E-01   # U_12
  2  1     9.88334459E-01   # U_21
  2  2     1.52299040E-01   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -2.15080852E-01   # V_11
  1  2     9.76596246E-01   # V_12
  2  1     9.76596246E-01   # V_21
  2  2     2.15080852E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.74173291E-01   # cos(theta_t)
  1  2     6.32973708E-01   # sin(theta_t)
  2  1    -6.32973708E-01   # -sin(theta_t)
  2  2     7.74173291E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.97478650E-01   # cos(theta_b)
  1  2     7.16605563E-01   # sin(theta_b)
  2  1    -7.16605563E-01   # -sin(theta_b)
  2  2     6.97478650E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.06170974E-01   # cos(theta_tau)
  1  2     7.08041352E-01   # sin(theta_tau)
  2  1    -7.08041352E-01   # -sin(theta_tau)
  2  2     7.06170974E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -3.35243450E-02   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     7.00000000E+02   # mu(Q)               
         2     2.99999977E+01   # tanbeta(Q)          
         3     2.51152701E+02   # vev(Q)              
         4     3.19191134E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54033285E-01   # gprime(Q) DRbar
     2     6.33797690E-01   # g(Q) DRbar
     3     1.11011501E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     9.03275650E-01   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     4.21150260E-01   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     3.07378801E-01   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     3.34816789E+06   # M^2_Hd              
        22    -7.68331078E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     1.00355004E-03   # gluino decays
#          BR         NDA      ID1       ID2
     6.10402449E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     6.94939923E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     2.52833921E-11    2     1000025        21   # BR(~g -> ~chi_30 g)
     6.34023866E-05    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     6.20682562E-03    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     6.33299891E-05    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     6.93220124E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.30529799E-04    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     4.84002893E-03    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     4.94045456E-05    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     2.79299147E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.01998917E-04    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     6.20682562E-03    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     6.33299891E-05    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     6.93220124E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.30529799E-04    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     4.84002893E-03    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     4.94045456E-05    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     2.79299147E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.01998917E-04    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     1.42585605E-01    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     1.18002674E-01    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     6.70381604E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.32685556E-04    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     4.31926769E-02    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     9.14128257E-03    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.00101003E-02    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.00101003E-02    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.37228817E-04    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     3.37228817E-04    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     1.00101003E-02    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.00101003E-02    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     3.37228817E-04    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     3.37228817E-04    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     2.45704802E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.45704802E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000024     3.12998309E-09   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.51787273E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.51787273E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.17264299E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.17264299E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     6.18968561E-02    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     1.50529989E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     8.39809998E-02    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.18755104E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.05878090E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.18755104E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.05878090E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     4.31146598E-02    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.41258502E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.41258502E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.25932563E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.81426068E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.81426068E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.81426068E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     4.76053745E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     4.76053745E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     4.76053745E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     4.76053745E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.58687474E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.58687474E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.58687474E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.58687474E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.41139374E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.41139374E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
