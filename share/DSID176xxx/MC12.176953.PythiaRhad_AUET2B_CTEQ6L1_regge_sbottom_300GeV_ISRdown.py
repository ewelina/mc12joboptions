MODEL = 'regge'
CASE = 'sbottom'
MASS = 300

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic sbottom 300GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)

topAlg.PythiaRhad.PythiaCommand += [ "pypars parp 64 0.25", ]
