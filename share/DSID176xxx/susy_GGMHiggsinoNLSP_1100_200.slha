#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05398158E+01   # W+
        25     1.26000000E+02   # h
        35     2.00398385E+03   # H
        36     2.00000000E+03   # A
        37     2.00208676E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50045066E+03   # ~d_L
   2000001     1.50008449E+03   # ~d_R
   1000002     1.49963372E+03   # ~u_L
   2000002     1.49983101E+03   # ~u_R
   1000003     1.50045066E+03   # ~s_L
   2000003     1.50008449E+03   # ~s_R
   1000004     1.49963372E+03   # ~c_L
   2000004     1.49983101E+03   # ~c_R
   1000005     1.49994100E+03   # ~b_1
   2000005     1.50059907E+03   # ~b_2
   1000006     1.50103628E+03   # ~t_1
   2000006     1.51501582E+03   # ~t_2
   1000011     1.50028172E+03   # ~e_L
   2000011     1.50025344E+03   # ~e_R
   1000012     1.49946469E+03   # ~nu_eL
   1000013     1.50028172E+03   # ~mu_L
   2000013     1.50025344E+03   # ~mu_R
   1000014     1.49946469E+03   # ~nu_muL
   1000015     1.50009034E+03   # ~tau_1
   2000015     1.50044691E+03   # ~tau_2
   1000016     1.49946469E+03   # ~nu_tauL
   1000021     1.10000000E+03   # ~g
   1000022     1.90097534E+02   # ~chi_10
   1000023    -2.00272092E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.01017480E+03   # ~chi_40
   1000024     1.92617160E+02   # ~chi_1+
   1000037     1.00779125E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.34531510E-02   # N_11
  1  2    -9.59655411E-02   # N_12
  1  3     7.06386731E-01   # N_13
  1  4    -6.99250429E-01   # N_14
  2  1     7.38763153E-03   # N_21
  2  2    -1.32631615E-02   # N_22
  2  3    -7.04618151E-01   # N_23
  2  4    -7.09424254E-01   # N_24
  3  1     8.73602502E-01   # N_31
  3  2     4.86640183E-01   # N_32
  3  3     2.54380735E-06   # N_33
  3  4    -3.89586728E-06   # N_34
  4  1     4.83644843E-01   # N_41
  4  2    -8.68211122E-01   # N_42
  4  3    -6.73130542E-02   # N_43
  4  4     8.81252841E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -8.34840861E-02   # U_11
  1  2     9.96509111E-01   # U_12
  2  1     9.96509111E-01   # U_21
  2  2     8.34840861E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.09447886E-01   # V_11
  1  2     9.93992535E-01   # V_12
  2  1     9.93992535E-01   # V_21
  2  2     1.09447886E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.12051770E-01   # cos(theta_t)
  1  2     7.02126966E-01   # sin(theta_t)
  2  1    -7.02126966E-01   # -sin(theta_t)
  2  2     7.12051770E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.70942621E-01   # cos(theta_b)
  1  2     8.82163844E-01   # sin(theta_b)
  2  1    -8.82163844E-01   # -sin(theta_b)
  2  2     4.70942621E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.78489266E-01   # cos(theta_tau)
  1  2     7.34610316E-01   # sin(theta_tau)
  2  1    -7.34610316E-01   # -sin(theta_tau)
  2  2     6.78489266E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89787073E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51099007E+02   # vev(Q)              
         4     3.95872399E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54153224E-01   # gprime(Q) DRbar
     2     6.35817781E-01   # g(Q) DRbar
     3     1.11019580E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.07014030E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.77639863E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80481366E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.10000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.67738867E+06   # M^2_Hd              
        22     7.35222147E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     2.61910318E-02   # gluino decays
#          BR         NDA      ID1       ID2
     1.28461556E-02    2     1000022        21   # BR(~g -> ~chi_10 g)
     1.31439234E-02    2     1000023        21   # BR(~g -> ~chi_20 g)
     1.73209631E-13    2     1000025        21   # BR(~g -> ~chi_30 g)
     1.07438032E-06    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     6.45280248E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     7.60487522E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     2.66800661E-06    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     7.27076357E-06    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     5.00585427E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     5.90044033E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.07013734E-05    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     5.64761256E-06    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     6.45280248E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     7.60487522E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     2.66800661E-06    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     7.27076357E-06    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     5.00585427E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     5.90044033E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.07013734E-05    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     5.64761256E-06    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     8.85241074E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.59743059E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     2.57452486E-06    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     7.06224555E-06    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     2.13765703E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     1.97080485E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     1.04520547E-03    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     1.04520547E-03    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     1.71853876E-05    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     1.71853876E-05    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     1.04520547E-03    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     1.04520547E-03    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     1.71853876E-05    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     1.71853876E-05    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     2.77693579E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.77693579E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
#
#         PDG            Width
DECAY   1000024     1.06455823E-10   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.72079999E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.72079999E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24027010E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24027010E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     7.78598268E-03    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     1.41336222E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     6.55720009E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.25275249E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.06677235E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.25275249E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.06677235E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     6.83469070E-04    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     2.42865589E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.42865589E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     2.01349434E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.84387724E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.84387724E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.84387724E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.84553179E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.84553179E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.84553179E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.84553179E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.28185104E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.28185104E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.28185104E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.28185104E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.76150600E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.76150600E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
