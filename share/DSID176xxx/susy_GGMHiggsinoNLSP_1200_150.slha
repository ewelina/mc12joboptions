#
#                               =====================
#                               | THE SDECAY OUTPUT |
#                               =====================
#
#
#              -----------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays |
#              |                                                   |
#              |                     SDECAY 1.3                    |
#              |                                                   |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46          |
#              |           [hep-ph/0311167]                        |
#              |                                                   |
#              |  In case of problems please send an email to      |
#              |           muehlleitner@lapp.in2p3.fr              |
#              |           djouadi@mail.cern.ch                    |
#              |           yann.mambrini@th.u-psud.fr              |
#              |                                                   |
#              |  If not stated otherwise all DRbar couplings and  |
#              |  soft SUSY breaking masses are given at the scale |
#              |  Q=  0.91187600E+02
#              |                                                   |
#              -----------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY      # decay calculator
     2   1.3         # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.41         # version number                     
#
BLOCK MODSEL  # Model selection
     1     0   # #general MSSM low scale                           
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27934000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.75000000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05397416E+01   # W+
        25     1.26000000E+02   # h
        35     2.00399026E+03   # H
        36     2.00000000E+03   # A
        37     2.00208930E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     1.50045100E+03   # ~d_L
   2000001     1.50008449E+03   # ~d_R
   1000002     1.49963338E+03   # ~u_L
   2000002     1.49983101E+03   # ~u_R
   1000003     1.50045100E+03   # ~s_L
   2000003     1.50008449E+03   # ~s_R
   1000004     1.49963338E+03   # ~c_L
   2000004     1.49983101E+03   # ~c_R
   1000005     1.49999522E+03   # ~b_1
   2000005     1.50054522E+03   # ~b_2
   1000006     1.50278857E+03   # ~t_1
   2000006     1.51327244E+03   # ~t_2
   1000011     1.50028206E+03   # ~e_L
   2000011     1.50025345E+03   # ~e_R
   1000012     1.49946435E+03   # ~nu_eL
   1000013     1.50028206E+03   # ~mu_L
   2000013     1.50025345E+03   # ~mu_R
   1000014     1.49946435E+03   # ~nu_muL
   1000015     1.50013476E+03   # ~tau_1
   2000015     1.50040284E+03   # ~tau_2
   1000016     1.49946435E+03   # ~nu_tauL
   1000021     1.20000000E+03   # ~g
   1000022     1.40663675E+02   # ~chi_10
   1000023    -1.50286003E+02   # ~chi_20
   1000025     1.00000076E+03   # ~chi_30
   1000035     1.00962256E+03   # ~chi_40
   1000024     1.43058941E+02   # ~chi_1+
   1000037     1.00736758E+03   # ~chi_2+
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.03985481E-02   # N_11
  1  2    -9.05190018E-02   # N_12
  1  3     7.07805109E-01   # N_13
  1  4    -6.98769078E-01   # N_14
  2  1     7.76024816E-03   # N_21
  2  2    -1.39378958E-02   # N_22
  2  3    -7.03641878E-01   # N_23
  2  4    -7.10375690E-01   # N_24
  3  1     8.73686180E-01   # N_31
  3  2     4.86489937E-01   # N_32
  3  3     2.54938158E-06   # N_33
  3  4    -3.88915729E-06   # N_34
  4  1     4.83814421E-01   # N_41
  4  2    -8.68870195E-01   # N_42
  4  3    -6.24502656E-02   # N_43
  4  4     8.41911755E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -7.74135970E-02   # U_11
  1  2     9.96999065E-01   # U_12
  2  1     9.96999065E-01   # U_21
  2  2     7.74135970E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -1.04531115E-01   # V_11
  1  2     9.94521617E-01   # V_12
  2  1     9.94521617E-01   # V_21
  2  2     1.04531115E-01   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     7.13704285E-01   # cos(theta_t)
  1  2     7.00447138E-01   # sin(theta_t)
  2  1    -7.00447138E-01   # -sin(theta_t)
  2  2     7.13704285E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.08414769E-01   # cos(theta_b)
  1  2     9.12796459E-01   # sin(theta_b)
  2  1    -9.12796459E-01   # -sin(theta_b)
  2  2     4.08414769E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.68303049E-01   # cos(theta_tau)
  1  2     7.43889128E-01   # sin(theta_tau)
  2  1    -7.43889128E-01   # -sin(theta_tau)
  2  2     6.68303049E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89787726E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51064828E+02   # vev(Q)              
         4     3.96225812E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.54203779E-01   # gprime(Q) DRbar
     2     6.36171804E-01   # g(Q) DRbar
     3     1.10869174E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.07012673E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.77664561E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80486318E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.00000000E+03   # M_1                 
         2     1.00000000E+03   # M_2                 
         3     1.20000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.69620336E+06   # M^2_Hd              
        22     7.52066492E+05   # M^2_Hu              
        31     1.50000000E+03   # M_eL                
        32     1.50000000E+03   # M_muL               
        33     1.50000000E+03   # M_tauL              
        34     1.50000000E+03   # M_eR                
        35     1.50000000E+03   # M_muR               
        36     1.50000000E+03   # M_tauR              
        41     1.50000000E+03   # M_q1L               
        42     1.50000000E+03   # M_q2L               
        43     1.50000000E+03   # M_q3L               
        44     1.50000000E+03   # M_uR                
        45     1.50000000E+03   # M_cR                
        46     1.50000000E+03   # M_tR                
        47     1.50000000E+03   # M_dR                
        48     1.50000000E+03   # M_sR                
        49     1.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
#
#
#         PDG            Width
DECAY   1000021     5.49379028E-02   # gluino decays
#          BR         NDA      ID1       ID2
     9.10899496E-03    2     1000022        21   # BR(~g -> ~chi_10 g)
     9.40272419E-03    2     1000023        21   # BR(~g -> ~chi_20 g)
     3.18121631E-12    2     1000025        21   # BR(~g -> ~chi_30 g)
     3.93608953E-06    2     1000035        21   # BR(~g -> ~chi_40 g)
#           BR         NDA      ID1       ID2       ID3
     5.12165759E-04    3     1000022         1        -1   # BR(~g -> ~chi_10 d  db)
     8.72550530E-06    3     1000023         1        -1   # BR(~g -> ~chi_20 d  db)
     4.43051447E-05    3     1000025         1        -1   # BR(~g -> ~chi_30 d  db)
     1.61618552E-04    3     1000035         1        -1   # BR(~g -> ~chi_40 d  db)
     3.97454089E-04    3     1000022         2        -2   # BR(~g -> ~chi_10 u  ub)
     6.77229218E-06    3     1000023         2        -2   # BR(~g -> ~chi_20 u  ub)
     1.77766720E-04    3     1000025         2        -2   # BR(~g -> ~chi_30 u  ub)
     1.25609154E-04    3     1000035         2        -2   # BR(~g -> ~chi_40 u  ub)
     5.12165759E-04    3     1000022         3        -3   # BR(~g -> ~chi_10 s  sb)
     8.72550530E-06    3     1000023         3        -3   # BR(~g -> ~chi_20 s  sb)
     4.43051447E-05    3     1000025         3        -3   # BR(~g -> ~chi_30 s  sb)
     1.61618552E-04    3     1000035         3        -3   # BR(~g -> ~chi_40 s  sb)
     3.97454089E-04    3     1000022         4        -4   # BR(~g -> ~chi_10 c  cb)
     6.77229218E-06    3     1000023         4        -4   # BR(~g -> ~chi_20 c  cb)
     1.77766720E-04    3     1000025         4        -4   # BR(~g -> ~chi_30 c  cb)
     1.25609154E-04    3     1000035         4        -4   # BR(~g -> ~chi_40 c  cb)
     7.40407220E-04    3     1000022         5        -5   # BR(~g -> ~chi_10 b  bb)
     2.46483586E-04    3     1000023         5        -5   # BR(~g -> ~chi_20 b  bb)
     4.39003911E-05    3     1000025         5        -5   # BR(~g -> ~chi_30 b  bb)
     1.60611082E-04    3     1000035         5        -5   # BR(~g -> ~chi_40 b  bb)
     2.14846157E-01    3     1000022         6        -6   # BR(~g -> ~chi_10 t  tb)
     2.11083660E-01    3     1000023         6        -6   # BR(~g -> ~chi_20 t  tb)
     8.35341438E-04    3     1000024         1        -2   # BR(~g -> ~chi_1+ d  ub)
     8.35341438E-04    3    -1000024         2        -1   # BR(~g -> ~chi_1- u  db)
     3.55443394E-04    3     1000037         1        -2   # BR(~g -> ~chi_2+ d  ub)
     3.55443394E-04    3    -1000037         2        -1   # BR(~g -> ~chi_2- u  db)
     8.35341438E-04    3     1000024         3        -4   # BR(~g -> ~chi_1+ s  cb)
     8.35341438E-04    3    -1000024         4        -3   # BR(~g -> ~chi_1- c  sb)
     3.55443394E-04    3     1000037         3        -4   # BR(~g -> ~chi_2+ s  cb)
     3.55443394E-04    3    -1000037         4        -3   # BR(~g -> ~chi_2- c  sb)
     2.73364997E-01    3     1000024         5        -6   # BR(~g -> ~chi_1+ b  tb)
     2.73364997E-01    3    -1000024         6        -5   # BR(~g -> ~chi_1- t  bb)
     5.78940501E-07    3     1000037         5        -6   # BR(~g -> ~chi_2+ b  tb)
     5.78940501E-07    3    -1000037         6        -5   # BR(~g -> ~chi_2- t  bb)
#
#         PDG            Width
DECAY   1000024     8.20182922E-11   # chargino1+ decays
#           BR         NDA      ID1       ID2       ID3
     3.73066507E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.73066507E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.24355832E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.24355832E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     5.15532286E-03    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000023     1.04759773E-07   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.38098620E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#           BR         NDA      ID1       ID2       ID3
     8.27601508E-02    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.06970150E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     8.27601508E-02    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.06970150E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     2.43502608E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     2.43502608E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     1.96780802E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     4.85627291E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     4.85627291E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     4.85627291E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     6.84291846E-02    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     6.84291846E-02    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     6.84291846E-02    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     6.84291846E-02    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     2.28097981E-02    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     2.28097981E-02    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     2.28097981E-02    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     2.28097981E-02    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.70678459E-02    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.70678459E-02    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
