evgenConfig.description = "Protos+Pythia6 ttbar production (dilepton) with P2011C CTEQ6L1 tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar"]
evgenConfig.contact  = ["cescobar@cern.ch"]
evgenConfig.inputfilecheck = "dilepton"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

# dilepton configuration
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.0
