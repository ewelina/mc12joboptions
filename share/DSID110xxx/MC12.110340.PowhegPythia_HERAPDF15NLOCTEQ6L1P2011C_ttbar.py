evgenConfig.description = "POWHEG(HERAPDF15NLO,hdamp=mt)+Pythia6(CTEQ6l1,P2011C) ttbar, pt>1 MeV lepton filter"
evgenConfig.keywords = ["top", "ttbar", "leptonic", "hdamp" ]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = 'hdampMt'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
