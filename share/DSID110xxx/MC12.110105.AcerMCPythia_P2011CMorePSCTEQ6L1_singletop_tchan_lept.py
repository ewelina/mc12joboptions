evgenConfig.description = "AcerMCPythia6 singletop-tchan-l production with P2011C CTEQ6L1 tune and MorePS param. settings"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "tchan", "leptonic", "PS"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan_lept"

#include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_Perugia2011C_MorePS_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
