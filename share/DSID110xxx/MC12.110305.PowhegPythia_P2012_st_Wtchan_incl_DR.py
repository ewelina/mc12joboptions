evgenConfig.description = "POWHEG+Pythia6 Wt inclusive production with PythiaPerugia2012 tune"
evgenConfig.keywords = ["top", "Wt", "inclusive", "DR"]
evgenConfig.contact  = ["huaqiao.zhang@cern.ch"]
evgenConfig.inputfilecheck = 'st_Wtchan_incl_DR'

include("MC12JobOptions/PowhegPythia_Perugia2012_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")