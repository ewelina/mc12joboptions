evgenConfig.description = "ALPGEN+Pythia Z(->tautau)+cc process with PythiaPerugia2011C tune"
evgenConfig.keywords = ["EW", "Z"]
evgenConfig.inputfilecheck = "ZtautauccNp0"
evgenConfig.minevents = 5000

include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )
