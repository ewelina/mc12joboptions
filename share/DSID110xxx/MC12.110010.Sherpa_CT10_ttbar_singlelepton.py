include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ttbar + up to 3 jets in ME+PS. Decay modes: lept had."
evgenConfig.keywords = [ "top", "ttbar", "leptonic" ]
evgenConfig.contact  = [ "frank.siegert@cern.ch","christian.schillo@cern.ch","kiran.joshi@cern.ch" ]
evgenConfig.minevents = 2000

evgenConfig.process="""
(processes){
  # lept had
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> 90 91
  Decay -24[d] -> 94 94
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10}; # the quarks in the decay don't count
  End process

  # had lept
  Process 93 93 ->  6[a] -6[b] 93{3}
  Order_EW 4
  Decay 6[a] -> 24[c] 5
  Decay -6[b] -> -24[d] -5
  Decay 24[c] -> 94 94
  Decay -24[d] -> 90 91
  CKKW sqr(30/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {8,9,10}
  Integration_Error 0.1 {7,8,9,10}
  Max_N_Quarks 6 {10}; # the quarks in the decay don't count
  End process
}(processes)

(selector){
  DecayMass 24 19.3 141.5
  DecayMass -24 19.3 141.5
  DecayMass 6 128.3 216.7
  DecayMass -6 128.3 216.7
}(selector)
"""

# Take tau BR's into account:
topAlg.Sherpa_i.CrossSectionScaleFactor=1.0

# Overwrite default MC11 widths with LO widths for top and W
topAlg.Sherpa_i.Parameters += [ "WIDTH[6]=1.47211", "WIDTH[24]=2.035169" ]
