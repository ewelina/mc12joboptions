evgenConfig.description = "AcerMCPythia6 ttbar_dilept production with AUET2B CTEQ6L1 tune and MorePS parameter settings"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "PS", "dileptonic"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_MorePS_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
