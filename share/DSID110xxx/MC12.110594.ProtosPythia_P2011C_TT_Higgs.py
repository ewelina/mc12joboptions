evgenConfig.description = "Vector-like TT production with Protos+Pythia, tune P2011C"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["exotics","top","heavyquark","Higgs"]
evgenConfig.contact  = ["Sergey.Burdin@cern.ch"]
#evgenConfig.inputfilecheck = "TT"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pydat3 mdme 210 1 0 #higgs decay",
                                "pydat3 mdme 211 1 0",
                                "pydat3 mdme 212 1 0",
                                "pydat3 mdme 213 1 0",
                                "pydat3 mdme 214 1 0",
                                "pydat3 mdme 215 1 0",
                                "pydat3 mdme 218 1 0",
                                "pydat3 mdme 219 1 0",
                                "pydat3 mdme 220 1 1",
                                "pydat3 mdme 222 1 0",
                                "pydat3 mdme 223 1 0",
                                "pydat3 mdme 224 1 0",
                                "pydat3 mdme 225 1 0",
                                "pydat3 mdme 226 1 0"]
topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Tauola.py")
#include("MC12JobOptions/Pythia_Photos.py")

