evgenConfig.description = "MEtop+Pythia strong FCNC inclusive direct top at NLO & top+light quark production with the Perugia2012 tune"
evgenConfig.keywords = ["singletop","FCNC","leptonic"]
evgenConfig.contact  = ["conrad.friedrich@cern.ch"]
evgenConfig.inputfilecheck = "MEtop10.117013.st_strongFCNC_ugt"

include ( "MC12JobOptions/MEtopPythia_Perugia2012_Common.py" )
                          
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

