evgenConfig.description = "Protos+Pythia ttbar with FCNC decays. ttbar->bWuZ (W->jj, Z->ll, l=e,mu,tau). CTEQ6L1 PDF for hard scatter and CTEQ6L1 PDF with P2011C tune for shower+MPI"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "fcnc", "hadronic"]
evgenConfig.contact  = ["Tamar.Djobava@cern.ch"]
evgenConfig.inputfilecheck = "TTbar_bWuZhad"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

