evgenConfig.description = "Protos22+Pythia ttbar production with FCNC (laL=laR=0.01, Br(t->Aq)=4.648e-3) decays ttbar->bWuGamma(W ->lnu, l=e,mu,tau), CTEQ6L1, P2012 tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "FCNC", "photon"]
evgenConfig.contact  = ["vadim.kostyukhin@cern.ch"]
evgenConfig.inputfilecheck = "tt_bWuGamma"

include("MC12JobOptions/Pythia_Perugia2012_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
	
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")
