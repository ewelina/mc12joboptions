evgenConfig.description = "POWHEG+Pythia6 t-channel single top production (2->3) (top) with Perugia2012mpiHi CTEQ6L1 tune"
evgenConfig.keywords = ["top", "singletop", "t-channel", "leptonic"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_top_lept"

include("MC12JobOptions/PowhegPythia_Perugia2012mpiHi_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
