evgenConfig.description = "POWHEG+fHerwig/Jimmy t-channel single top production (2->3) (antitop) with AUET2 CT10 tune"
evgenConfig.generators = ["Powheg", "Herwig"]
evgenConfig.keywords = ["top", "singletop", "t-channel", "leptonic"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.inputfilecheck = "singletop_tchan2to3nlo_antitop_lept"

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
