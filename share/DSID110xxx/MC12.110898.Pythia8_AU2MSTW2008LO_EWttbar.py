# EW ttbar production JO: 19/11/2013 - J. Ferrando <james.ferrando@glasgow.ac.uk>
# zprime mass in GeV - not actually used because mode 4 disables Z'
m_zprime=500.0

evgenConfig.description = "SSM Z prime ("+str(m_zprime)+") to ttbar"
evgenConfig.keywords = ["Exotics", "Top" ,"EW ttbar"]
evgenConfig.contact  = ["james.ferrando@glasgow.ac.uk"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

# turn on the Z' process
topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on"]
# set mass and disable all decay modes except Z' -> ttbar
topAlg.Pythia8.Commands += ["32:m0 ="+str(m_zprime) ]
topAlg.Pythia8.Commands += ["32:onMode = off"]
topAlg.Pythia8.Commands += ["32:onIfAny = 6 -6"]

# only Z*/gamma
topAlg.Pythia8.Commands += ["Zprime:gmZmode= 4"]

