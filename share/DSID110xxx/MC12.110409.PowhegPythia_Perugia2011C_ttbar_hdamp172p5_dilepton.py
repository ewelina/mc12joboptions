#--------------------------------------------------------------
# Powheg_ttbar setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.nEvents = 55000
PowhegConfig.hdamp   = 172.5
PowhegConfig.PDF     = 10800
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia_Perugia2011C_Common.py')
include('MC12JobOptions/Pythia_Tauola.py')
include('MC12JobOptions/Pythia_Photos.py')

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Lepton filter (-1: non-allhadronic events, 2 dileptonic, ...)
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=2

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with 10800, hdamp=172.5, and Perugia2011C tune'
evgenConfig.keywords    = [ 'top', 'dilepton']
evgenConfig.contact     = [ 'aknue@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 5000
