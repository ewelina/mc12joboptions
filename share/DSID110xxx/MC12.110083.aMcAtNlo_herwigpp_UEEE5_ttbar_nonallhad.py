#g++ config for the CTEQ6L1 UE-EE-5 tune series with NLO events read from an LHEF file
## needs Herwig++ >= 2.7.0
include("MC12JobOptions/Herwigpp_UEEE5_CTEQ6L1_CT10ME_LHEF_Common.py")

## Add to commands
cmds = """
set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
"""
topAlg.Herwigpp.Commands += cmds.splitlines()

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators     = ["aMcAtNlo", "Herwigpp"]
evgenConfig.keywords       = ["top", "ttbar","dilepton"]
evgenConfig.description    = "ttbar MG5_aMC@NLO+Herwig++ - CT10 PDF for hard scatter and CTEQ6l1 with UEEE5 tune for shower+MPI"
evgenConfig.inputfilecheck = 'group.phys-gener.MG5_aMCatNLO221.110083.ttbar_inclusive_CTEQ6L1_8TeV.TXT.mc12_v2'
evgenConfig.contact        = ["Maria Moreno Llacer"]
evgenConfig.minevents      = 5000

include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = -1 #(non-all had)


