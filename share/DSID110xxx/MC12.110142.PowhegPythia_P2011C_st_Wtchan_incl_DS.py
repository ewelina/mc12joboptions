evgenConfig.description = "POWHEG+Pythia6 Wt inclusive production with PythiaPerugia2011C tune"
evgenConfig.keywords = ["top", "Wt", "inclusive", "DS"]
evgenConfig.contact  = ["huaqiao.zhang@cern.ch"]
evgenConfig.inputfilecheck = 'st_Wtchan_incl_DS'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

