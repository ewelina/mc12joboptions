evgenConfig.description = "Protos+Pythia ttbar production (l+jets) with P2011C CTEQ6L1 tune with non-SM BR for top"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "b-tagging", "topproperties", "V_ts", "V_td"]
evgenConfig.contact  = ["Christopher.Schmitt@cern.ch"]
evgenConfig.inputfilecheck = "singlelepton"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

# lepton+jets configuration
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=1
