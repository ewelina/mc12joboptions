#--------------------------------------------------------------
# Powheg_ttbar setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.nEvents = 15000
PowhegConfig.hdamp   = 345.0
PowhegConfig.PDF     = 10800
PowhegConfig.mu_F    = .50000000000000000000
PowhegConfig.mu_R    = .50000000000000000000
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia_Perugia2012radHi_Common.py')
include('MC12JobOptions/Pythia_Tauola.py')
include('MC12JobOptions/Pythia_Photos.py')

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Lepton filter (-1: non-allhadronic events, 2 dileptonic, ...)
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=-1

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with 10800, hdamp=345.0, mu=.50000000000000000000 and Perugia2012radHi tune'
evgenConfig.keywords    = [ 'top' ]
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 5000
