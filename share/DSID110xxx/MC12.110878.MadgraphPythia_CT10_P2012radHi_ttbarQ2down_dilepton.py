include("MC12JobOptions/Pythia_Perugia2012radHi_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph",
   "pypars mstp 143 1"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "MadGraph(CT10) Pythia Perugia 2012radHi ttbar Q2down up to 3 additional partons"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["top", "ttbar", "dilep" ]
evgenConfig.inputfilecheck = 'madgraph.110876.ttbarQ2down_0to3inc_nodecay'
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.minevents=1000

topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 1.
   
#### Add matching parameters ####  
phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
IEXCFILE=0
qcut=40
"""
phojf.write(phojinp)
phojf.close()
