evgenConfig.description = "AcerMCPythia6 singletop-Wtchan_inclusive production with P2011C CTEQ6L1 tune and LessPS param. settings"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "Wtchan", "inclusive", "PS"]
evgenConfig.contact  = ["liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = "singletop_Wtchan_inc"

#include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_Perugia2011C_LessPS_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
