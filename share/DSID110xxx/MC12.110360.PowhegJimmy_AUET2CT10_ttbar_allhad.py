include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Jimmy ttbar production with AUET2 tune, allhadronic filter"
evgenConfig.keywords    = [ "top", "ttbar", "allhad" ]
evgenConfig.contact     = [ "james.robinson@cern.ch", "alexander.grohsjean@desy.de" ]

postGenerator="JimmyTauolaPhotos"
process="tt_all"
postGeneratorTune = "AUET2_CT10"

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons = 0
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
