evgenConfig.description = "McAtNlo+fHerwig/Jimmy dileptonic ttbar production - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "leptonic", "dileptonic"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
#dilepton configuration not yet in place, use with dileptonic inputs only!
#include("MC12JobOptions/TTbarWToLeptonFilter.py")

