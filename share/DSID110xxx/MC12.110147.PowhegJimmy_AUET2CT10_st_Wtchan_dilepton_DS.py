evgenConfig.description = "POWHEG+fHerwig/Jimmy Wt dilepton production with AUET2 CT10 tune"
evgenConfig.generators = ["Powheg", "Herwig"]
evgenConfig.keywords = ["top", "Wt", "dilepton", "DS"]
evgenConfig.contact  = ["huaqiao.zhang@cern.ch"]
evgenConfig.inputfilecheck = 'st_Wtchan_dilepton_DS'

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

