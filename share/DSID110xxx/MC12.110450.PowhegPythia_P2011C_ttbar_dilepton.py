include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011c tune, dileptonic filter'
evgenConfig.keywords    = [ 'top', 'ttbar', 'dilepton' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'alexander.grohsjean@desy.de' ]

postGenerator="Pythia6TauolaPhotos"
process="tt_all"
postGeneratorTune ="Perugia2011C"

# compensate filter efficiency
evt_multiplier = 12.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarWToLeptonFilter.py')
topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 0.
