evgenConfig.description = "Protos2.2+Pythia6 singletop t-channel (lept.) production with P2011C CTEQ6L1 tune with anomalous couplings (VL= 0.968 ReVR= 0.25 )"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "singletop", "tchan", "leptonic"]
evgenConfig.contact  = ["spedraza@cern.ch"]
evgenConfig.inputfilecheck = "st_tch_lept_VG"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")