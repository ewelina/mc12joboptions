evgenConfig.description = "MadGraph5+Pythia8 for qqbar -> H+ -> tb (lept. decay s-channel) with mH = 1600 GeV"
evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.keywords = ["ChargedHiggs"]
evgenConfig.inputfilecheck = "ChargedH"

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
