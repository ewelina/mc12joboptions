evgenConfig.description = "Protos22+Pythia ttbar production with FCNC(etaL=etaR=0.01, Br(t->Hq)=2.67e-3) decay ttbar->bW(->lnu)Hu, CTEQ6L1, P2012 tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["ttbar","top","Higgs","FCNC"]
evgenConfig.contact  = ["vadim.kostyukhin@cern.ch"]
evgenConfig.inputfilecheck = "tt_bWuHincl"

include("MC12JobOptions/Pythia_Perugia2012_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
