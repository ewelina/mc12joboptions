include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand +=  [
   "pyinit user madgraph",
   "pypars mstp 143 1"
   ]

include ( "MC12JobOptions/Pythia_Photos.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.generators += ["MadGraph", "Pythia"]

evgenConfig.description = "MadGraph(CT10) Pythia Perugia 2011c ttbar Matchup up to 3 additional partons"
evgenConfig.keywords = ["top", "ttbar", "dilepton" ]
evgenConfig.inputfilecheck = 'madgraph.110879.ttbarMatchup_0to3inc_nodecay'
evgenConfig.contact  = ["alexander.grohsjean@desy.de"]
evgenConfig.minevents=1000

topAlg.TTbarWToLeptonFilter.NumLeptons = 2
topAlg.TTbarWToLeptonFilter.Ptcut = 1.
   
#### Add matching parameters ####  
phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
IEXCFILE=0
qcut=80
"""
phojf.write(phojinp)
phojf.close()
