evgenConfig.description = "Protos+Pythia ttbar production with FCNC decays ttbar -> bWqGamma (W -> l nu, l = e, mu, tau) - CTEQ6L1 PDF, P2011C tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "fcnc", "leptonic"]
evgenConfig.contact  = ["elizabeth.brost@cern.ch"]
evgenConfig.inputfilecheck = "tt_bWqGamma"

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]
	
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")
