evgenConfig.description = "POWHEG+Pythia6 s-channel Wlep production (facsc=0.5, rensc=0.5) with Perugia 2012 radLo tune"
evgenConfig.keywords = ["top", "s-channel", "Wlep"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_schan_lept'

include("MC12JobOptions/PowhegPythia_Perugia2012radLo_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
