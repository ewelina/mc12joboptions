evgenConfig.description = "POWHEG+Pythia6 s-channel Wlep production with PythiaPerugia2012 tune"
evgenConfig.keywords = ["top", "s-channel", "Wlep"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_sch_wlep'

include("MC12JobOptions/PowhegPythia_Perugia2012_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")