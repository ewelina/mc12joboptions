evgenConfig.description = "POWHEG+Pythia6 Wt-channel inclusive production (facsc=2, rensc=2) under DR with Perugia2012 radHi tune"
evgenConfig.keywords = ["top", "Wt","DR", "Incl"]
evgenConfig.contact  = ["cunfeng.feng@cern.ch"]
evgenConfig.inputfilecheck = 'singletop_Wtchan_incl_DR'

include("MC12JobOptions/PowhegPythia_Perugia2012radHi_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

