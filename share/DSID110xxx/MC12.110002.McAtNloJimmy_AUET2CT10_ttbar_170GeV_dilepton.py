evgenConfig.description = "McAtNlo+fHerwig/Jimmy dileptonic ttbar production with m_top=170GeV - CT10 PDF, AUET2 tune"
evgenConfig.generators = ["McAtNlo", "Herwig"]
evgenConfig.keywords = ["top", "ttbar", "leptonic", "dileptonic","mass"]
evgenConfig.contact  = ["christoph.wasicki@cern.ch"]
evgenConfig.inputfilecheck = "ttbar"

include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand+=[ "iproc mcatnlo" ]
topAlg.Herwig.HerwigCommand+=[ "rmass 6 170.0" ]
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
#for now use with dileptonic inputs only!
#include("MC12JobOptions/TTbarWToLeptonFilter.py")
#topAlg.TTbarWToLeptonFilter.NumLeptons=2 #require exactly two leptons (not yet released)

