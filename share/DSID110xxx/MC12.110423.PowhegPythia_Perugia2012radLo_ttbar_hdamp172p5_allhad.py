#--------------------------------------------------------------
# Powheg_ttbar setup
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.nEvents = 15000
PowhegConfig.hdamp   = 172.5
PowhegConfig.PDF     = 10800
PowhegConfig.mu_F    = 2
PowhegConfig.mu_R    = 2
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2012) showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia_Perugia2012radLo_Common.py')
include('MC12JobOptions/Pythia_Tauola.py')
include('MC12JobOptions/Pythia_Photos.py')

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
# Lepton filter (-1: non-allhadronic events, 2 dileptonic, ...)
include("MC12JobOptions/TTbarWToLeptonFilter.py")
topAlg.TTbarWToLeptonFilter.NumLeptons=0

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with 10800, hdamp=172.5, mu=2 and Perugia2012radLo tune'
evgenConfig.keywords    = [ 'top', 'allhad']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.generators += [ 'Powheg' ]
evgenConfig.minevents   = 5000
