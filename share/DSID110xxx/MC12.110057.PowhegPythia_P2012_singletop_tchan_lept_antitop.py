evgenConfig.description = "POWHEG+Pythia6 t-channel single top production (2->3) (top) with P2012 tune"
evgenConfig.keywords = ["top", "singletop", "t-channel", "leptonic"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch"]
evgenConfig.inputfilecheck = "powheg2556.110057.st_tch2to3nlo_antitop_lept"
                            
include("MC12JobOptions/PowhegPythia_Perugia2012_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

