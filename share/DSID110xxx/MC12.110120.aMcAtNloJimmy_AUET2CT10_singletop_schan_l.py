evgenConfig.description = "MG5_aMC@NLO v.2.1.2+fHerwig/Jimmy s-channel single top production and AUET2 CT10 tune"
evgenConfig.keywords = ["top", "t-channel", "single top"]
evgenConfig.contact  = ["dominic.hirschbuehl@cern.ch"]

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

evgenConfig.generators += [ "aMcAtNlo"]
include("MC12JobOptions/Jimmy_Tauola.py")

include("MC12JobOptions/Jimmy_Photos.py")
