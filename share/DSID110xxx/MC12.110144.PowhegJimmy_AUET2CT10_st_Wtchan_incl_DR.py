evgenConfig.description = "POWHEG+fHerwig/Jimmy Wt inclusive production with AUET2 CT10 tune"
evgenConfig.generators = ["Powheg", "Herwig"]
evgenConfig.keywords = ["top", "Wt", "inclusive", "DR"]
evgenConfig.contact  = ["huaqiao.zhang@cern.ch"]
evgenConfig.inputfilecheck = 'st_Wtchan_incl_DR'

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")

