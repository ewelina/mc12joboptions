# ttbar pt>700 GeV: 13/6/2012 - <Thorsten.Kuhl@cern.ch>
evgenConfig.description = "ttbar pt gt 700"
evgenConfig.keywords = ["Top" ,"ttbar"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")


# turn on all ttbar production channels
topAlg.Pythia8.Commands += [" Top:gg2ttbar = on"]
topAlg.Pythia8.Commands += [" Top:qqbar2ttbar = on"]
# set phase space cuts
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMin = 700."]
topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 700."]

