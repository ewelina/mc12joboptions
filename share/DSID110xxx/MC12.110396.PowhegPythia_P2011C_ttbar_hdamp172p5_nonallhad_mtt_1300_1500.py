include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia6 ttbar production with Perugia 2011c tune, hdamp=172p5 and LeptonFilter mtt slice 1300-1500 GeV"
evgenConfig.keywords = ["top", "ttbar", "leptonic"]
evgenConfig.contact  = ["Christoph.Wasicki@cern.ch"]
evgenConfig.minevents      = 1000  

postGenerator="Pythia6TauolaPhotos"
process="tt_all"

def powheg_override():
    PowhegConfig.hdamp = 172.5

# compensate filter efficiency
evt_multiplier = 5e3

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarMassFilter.py')
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 1300000.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 1500000.

include('MC12JobOptions/TTbarWToLeptonFilter.py')
