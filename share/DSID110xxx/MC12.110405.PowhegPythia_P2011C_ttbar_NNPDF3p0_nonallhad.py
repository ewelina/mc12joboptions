include("MC12JobOptions/PowhegControl_preInclude.py")

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Powheg and NNPDF 3.0, Perugia 2011c tune, at least one lepton filter'
evgenConfig.keywords    = [ 'top', 'ttbar', 'nonallhad' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch', 'alexander.grohsjean@desy.de' ]


postGenerator="Pythia6TauolaPhotos"
process="tt_all"
postGeneratorTune ="Perugia2011C"

def powheg_override():
    PowhegConfig.PDF = 260000

# compensate filter efficiency
evt_multiplier = 3.

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('MC12JobOptions/TTbarWToLeptonFilter.py')
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


