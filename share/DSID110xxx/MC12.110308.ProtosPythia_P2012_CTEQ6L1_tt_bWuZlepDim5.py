evgenConfig.description = "Protos22+Pythia ttbar production with Dim5(XL=XR=0.01, kL=kR=0, Br(t->Zq)=9.378e-3) FCNC decays ttbar->bWuZ (W ->lnu, Z->ll, l=e,mu,tau), CTEQ6L1, P2012 tune"
evgenConfig.generators = ["Protos", "Pythia"]
evgenConfig.keywords = ["top", "ttbar", "FCNC", "leptonic"]
evgenConfig.contact  = ["vadim.kostyukhin@cern.ch"]
evgenConfig.inputfilecheck = "tt_bWuZlepDim5"

include("MC12JobOptions/Pythia_Perugia2012_Common.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")
