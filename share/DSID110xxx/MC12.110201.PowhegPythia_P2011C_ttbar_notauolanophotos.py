evgenConfig.description = "POWHEG+Pythia6 ttbar production with a 1 MeV lepton filter, PythiaPerugia2011C tune and EvtGen"
evgenConfig.keywords = ["top", "ttbar", "leptonic", "EvtGen"]
evgenConfig.contact  = ["Gia.Khoriauli@cern.ch", "liza.mijovic@cern.ch"]
evgenConfig.inputfilecheck = 'ttbar'

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")

# as of 17.2.8.10 Powheg + Pythia + EVTGEN + Taoula crashes, so leave out for now
# include("MC12JobOptions/Pythia_Tauola.py")
#include("MC12JobOptions/Pythia_Photos.py")

include("MC12JobOptions/TTbarWToLeptonFilter.py")
include("MC12JobOptions/Pythia_EvtGenAfterburner.py")
