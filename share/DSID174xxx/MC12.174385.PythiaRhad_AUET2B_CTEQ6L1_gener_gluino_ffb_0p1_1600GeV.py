MODEL = 'generic'
CASE = 'gluino'
MASS = 1600
PROC = 'ffbar'
GBALLPROB = 0.1

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic gluino ffbar gball0.1 1600GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
