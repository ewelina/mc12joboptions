#
#                          ======================
#                          |    SPICE OUTPUT    |
#                          ======================
#
#          ------------------------------------------------------
#          |         Output in LesHouches Accord Format         |
#          ------------------------------------------------------
#
#          ------------------------------------------------------
#          |    Created by G. Engelhard, J. Feng, I. Galon,     |
#          |  D. Sanford, and F. Yu.  For problems with SPICE   |
#          |  send e-mail to                                    |
#          |          spice@hep.ps.uci.edu                      |
#          |                                                    |
#          |    If you use SPICE, please cite                   |
#          |      arXiv:0904.1415v1 [hep-ph]                    |
#          |                                                    |
#          |    If your model involves flavor-violating slepton |
#          |      3-body decays of sleptons, please cite        |
#          |      arXiv:0904.1416v1 [hep-ph]                    |
#          |                                                    |
#          |    We reprint the original SOFTSUSY and SUSYHIT    |
#          |  headers below.                                    |
#          ------------------------------------------------------
#
#
#
#                          ======================
#                          |   SOFTSUSY HEADER  |
#                          ======================
#
# SOFTSUSY3.0.2
# B.C. Allanach, Comput. Phys. Commun. 143 (2002) 305-331, hep-ph/0104145
#
#
#
#                          ======================
#                          |   SUSYHIT HEADER   |
#                          ======================
#
#
#          ------------------------------------------------------
#          |     This is the output of the SUSY-HIT package     |
#          |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#          |  In case of problems with SUSY-HIT email to        |
#          |           margarete.muehlleitner@cern.ch           |
#          |           michael.spira@psi.ch                     |
#          |           abdelhak.djouadi@cern.ch                 |
#          ------------------------------------------------------
#
#          ------------------------------------------------------
#          |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#          |              based on the decay programs           |
#          |                                                    |
#          |                     SDECAY 1.3                     |
#          |                                                    |
#          |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#          |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#          |           [hep-ph/0311167]                         |
#          |                                                    |
#          |                     HDECAY 3.302                   |
#          |                                                    |
#          |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#          |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#          |           [hep-ph/9704448]                         |
#          |                                                    |
#          |                                                    |
#          |  If not stated otherwise all DRbar couplings and   |
#          |  soft SUSY breaking masses are given at the scale  |
#          |  Q=  0.97084150E+03                                |
#          |                                                    |
#          ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.3b  /3.4    # version number
BLOCK SPINFO  # Spectrum calculator information
     1    SOFTSUSY    # spectrum calculator                
     2    3.3.5       # version number                     
BLOCK MODSEL  # Model selection
     1     2   #  gmsb                                             
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27916000E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16637000E-05   # G_F [GeV^-2]
         3     1.18400000E-01   # alpha_S(M_Z)^MSbar
         4     9.11876000E+01   # M_Z pole mass
         5     4.18000000E+00   # mb(mb)^MSbar
         6     1.73500000E+02   # mt pole mass
         7     1.77699000E+00   # mtau pole mass
BLOCK MINPAR  # Input parameters - minimal models
         1     8.00000000E+04   # lambda              
         2     2.50000000E+05   # M_mess              
         3     6.00000000E+01   # tanb                
         4     1.00000000E+00   # sign(mu)            
         5     3.00000000E+00   # N5                  
         6     5.00000000E+03   # cgrav               
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.03850198e+01
         5     4.93386190e+00   # b-quark pole mass calculated from mb(mb)_Msbar
         6     1.73500000e+02
        25     1.15689151e+02
        35     6.34342620e+02
        36     6.34273647e+02
        37     6.40073272e+02
   1000001     1.69406566e+03
   1000002     1.69234190e+03
   1000003     1.69404463e+03
   1000004     1.69231658e+03
   1000005     1.51057200e+03
   1000006     1.49811194e+03
   2000001     1.62191259e+03
   2000002     1.62680622e+03
   2000003     1.62185562e+03
   2000004     1.62680462e+03
   2000005     1.58417007e+03
   2000006     1.58845044e+03
   1000015     1.29874384e+02
   1000016     5.00333731e+02
   2000013     2.55380980e+02
   1000014     5.09254477e+02
   2000011     2.55600908e+02
   1000012     5.09278417e+02
   1000011     5.15743727e+02
   2000012     0.00000000e+00
   1000013     5.15745607e+02
   2000014     0.00000000e+00
   2000015     5.20317360e+02
   2000016     0.00000000e+00
   1000021     1.74214235e+03
   1000022     3.28864597e+02
   1000023     5.51983094e+02
   1000025    -5.84722584e+02
   1000035     6.78727137e+02
   1000024     5.50717971e+02
   1000037     6.78503497e+02
   1000039     2.37000000e-05
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     9.90815175E-01   # N_11
  1  2    -1.72175274E-02   # N_12
  1  3     1.15976167E-01   # N_13
  1  4    -6.73674547E-02   # N_14
  2  1    -1.20860873E-01   # N_21
  2  2    -5.12418687E-01   # N_22
  2  3     6.16252879E-01   # N_23
  2  4    -5.85706520E-01   # N_24
  3  1    -3.34520039E-02   # N_31
  3  2     4.48513490E-02   # N_32
  3  3     7.03810933E-01   # N_33
  3  4     7.08180408E-01   # N_34
  4  1    -5.05855839E-02   # N_41
  4  2     8.57390811E-01   # N_42
  4  3     3.33814452E-01   # N_43
  4  4    -3.88445630E-01   # N_44
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.70168063E-01   # U_11
  1  2     8.82576905E-01   # U_12
  2  1    -8.82576905E-01   # U_21
  2  2    -4.70168063E-01   # U_22
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.45371637E-01   # V_11
  1  2     8.38194356E-01   # V_12
  2  1    -8.38194356E-01   # V_21
  2  2    -5.45371637E-01   # V_22
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     2.90703330E-01   # cos(theta_t)
  1  2     9.56813239E-01   # sin(theta_t)
  2  1    -9.56813239E-01   # -sin(theta_t)
  2  2     2.90703330E-01   # cos(theta_t)
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     3.19941638E-01   # cos(theta_b)
  1  2     9.47437253E-01   # sin(theta_b)
  2  1    -9.47437253E-01   # -sin(theta_b)
  2  2     3.19941638E-01   # cos(theta_b)
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     2.84277027E-01   # cos(theta_tau)
  1  2     9.58742182E-01   # sin(theta_tau)
  2  1    -9.58742182E-01   # -sin(theta_tau)
  2  2     2.84277027E-01   # cos(theta_tau)
BLOCK ALPHA  # Higgs mixing
          -1.75708118E-02   # Mixing angle in the neutral Higgs boson sector
BLOCK HMIX Q=  1.50615446E+03  # DRbar Higgs Parameters
         1     5.79361544E+02   # mu(Q)MSSM           
         2     5.99450344E+01   # tan                 
         3     2.43134234E+02   # higgs               
         4     7.42806643E+05   # mA^2(Q)MSSM         
BLOCK GAUGE Q=  1.50615446E+03  # The gauge couplings
     1     3.63954305E-01   # gprime(Q) DRbar
     2     6.41288832E-01   # g(Q) DRbar
     3     1.03989130E+00   # g3(Q) DRbar
BLOCK Yu Q=  1.50615446E+03  # The Yukawa couplings
  1  1     7.10852160E-06   # y_u(Q) DRbar
  2  2     3.23621218E-03   # y_c(Q) DRbar
  3  3     8.43774326E-01   # y_t(Q) DRbar
BLOCK Yd Q=  1.50615446E+03  # The Yukawa couplings
  1  1     8.48534195E-04   # y_d(Q) DRbar
  2  2     1.85788073E-02   # y_s(Q) DRbar
  3  3     7.40069343E-01   # y_b(Q) DRbar
BLOCK Ye Q=  1.50615446E+03  # The Yukawa couplings
  1  1     1.72417943E-04   # y_e(Q) DRbar
  2  2     3.56529270E-02   # y_mu(Q) DRbar
  3  3     6.92647979E-01   # y_tau(Q) DRbar
BLOCK MSOFT Q=  1.50615446E+03  # The soft SUSY breaking masses at the scale Q
         1     3.39043269E+02   # M_1(Q)              
         2     6.29239242E+02   # M_2(Q)              
         3     1.66282871E+03   # M_3(Q)              
        21    -1.81613460E+05   # mH1^2(Q)            
        22    -2.91046580E+05   # mH2^2(Q)            
#         PDG            Width
DECAY         6     1.45737248e+00
#          BR         NDA      ID1       ID2       ID3
     1.00000000e+00    2           5        24
#         PDG            Width
DECAY        25     1.10339212e+20
#          BR         NDA      ID1       ID2       ID3
     9.52340129e-01    2          22        22
     4.76598707e-02    2          22        23
#         PDG            Width
DECAY        35     8.51089979e+30
#          BR         NDA      ID1       ID2       ID3
     2.51353639e-09    2          22        22
     2.17563200e-09    2          23        22
     9.99999995e-01    2          25        25
#         PDG            Width
DECAY        36     3.21151252e+01
#          BR         NDA      ID1       ID2       ID3
     8.51178809e-01    2           5        -5
     1.47078132e-01    2         -15        15
     5.19987495e-04    2         -13        13
     7.43374657e-04    2           3        -3
     2.60204817e-09    2           4        -4
     2.59492299e-04    2           6        -6
     2.18295798e-04    2          21        21
     1.01860244e-07    2          22        22
     6.25060035e-08    2          23        22
     1.74235490e-06    2          23        25
#         PDG            Width
DECAY        37     2.51619872e+01
#          BR         NDA      ID1       ID2       ID3
     2.00584543e-05    2    -1000015   1000016
     1.36928923e-03    2           4        -5
     1.89437491e-01    2         -15        16
     6.69746738e-04    2         -13        14
     8.76325193e-06    2           2        -5
     4.54763963e-05    2           2        -3
     9.35340765e-04    2           4        -3
     8.07511505e-01    2           6        -5
     2.32824768e-06    2          24        25
#         PDG            Width
DECAY   1000001     1.37930028e+01
#          BR         NDA      ID1       ID2       ID3
     1.77845565e-02    2     1000022         1
     8.71263975e-02    2     1000023         1
     9.22826589e-04    2     1000025         1
     2.38590798e-01    2     1000035         1
     1.60912458e-01    2    -1000024         2
     4.94662964e-01    2    -1000037         2
#         PDG            Width
DECAY   1000002     1.26543251e+01
#          BR         NDA      ID1       ID2       ID3
     1.26281440e-02    2     1000022         2
     1.04689852e-01    2     1000023         2
     5.23148026e-04    2     1000025         2
     2.24846306e-01    2     1000035         2
     2.17635309e-01    2     1000024         1
     4.39677241e-01    2     1000037         1
#         PDG            Width
DECAY   1000003     1.37930028e+01
#          BR         NDA      ID1       ID2       ID3
     1.77845565e-02    2     1000022         3
     8.71263975e-02    2     1000023         3
     9.22826589e-04    2     1000025         3
     2.38590798e-01    2     1000035         3
     1.60912458e-01    2    -1000024         4
     4.94662964e-01    2    -1000037         4
#         PDG            Width
DECAY   1000004     1.26543251e+01
#          BR         NDA      ID1       ID2       ID3
     1.26281440e-02    2     1000022         4
     1.04689852e-01    2     1000023         4
     5.23148026e-04    2     1000025         4
     2.24846306e-01    2     1000035         4
     2.17635309e-01    2     1000024         3
     4.39677241e-01    2     1000037         3
#         PDG            Width
DECAY   1000005     2.82490166e+01
#          BR         NDA      ID1       ID2       ID3
     4.88937345e-02    2     1000022         5
     2.33288391e-01    2     1000023         5
     2.20343029e-01    2     1000025         5
     1.25110076e-02    2     1000035         5
     4.56233017e-01    2    -1000024         6
     2.87308207e-02    2    -1000037         6
#         PDG            Width
DECAY   1000006     3.84133508e+01
#          BR         NDA      ID1       ID2       ID3
     8.51609578e-02    2     1000022         6
     1.91319315e-01    2     1000023         6
     2.35563891e-01    2     1000025         6
     1.95029235e-02    2     1000035         6
     4.27010665e-01    2     1000024         5
     4.14422479e-02    2     1000037         5
#         PDG            Width
DECAY   2000001     8.95498573e-01
#          BR         NDA      ID1       ID2       ID3
     9.84228381e-01    2     1000022         1
     1.28156690e-02    2     1000023         1
     9.55806456e-04    2     1000025         1
     2.00014393e-03    2     1000035         1
#         PDG            Width
DECAY   2000002     3.43827518e+00
#          BR         NDA      ID1       ID2       ID3
     9.84450963e-01    2     1000022         2
     1.26555392e-02    2     1000023         2
     9.41354901e-04    2     1000025         2
     1.95214336e-03    2     1000035         2
#         PDG            Width
DECAY   2000003     8.95498573e-01
#          BR         NDA      ID1       ID2       ID3
     9.84228381e-01    2     1000022         3
     1.28156690e-02    2     1000023         3
     9.55806456e-04    2     1000025         3
     2.00014393e-03    2     1000035         3
#         PDG            Width
DECAY   2000004     3.43827518e+00
#          BR         NDA      ID1       ID2       ID3
     9.84450963e-01    2     1000022         4
     1.26555392e-02    2     1000023         4
     9.41354901e-04    2     1000025         4
     1.95214336e-03    2     1000035         4
#         PDG            Width
DECAY   2000005     4.46005862e+01
#          BR         NDA      ID1       ID2       ID3
     2.95474324e-03    2     1000022         5
     1.14741313e-01    2     1000023         5
     1.59072964e-01    2     1000025         5
     1.34392224e-01    2     1000035         5
     2.55931850e-01    2    -1000024         6
     3.32906905e-01    2    -1000037         6
#         PDG            Width
DECAY   2000006     4.48475856e+01
#          BR         NDA      ID1       ID2       ID3
     7.78627536e-03    2     1000022         6
     1.29836537e-01    2     1000023         6
     1.81982796e-01    2     1000025         6
     1.75883978e-01    2     1000035         6
     2.22625029e-01    2     1000024         5
     2.81885385e-01    2     1000037         5
#         PDG            Width
DECAY   1000016     4.93947162e+00
#          BR         NDA      ID1       ID2       ID3
     4.48956227e-02    2     1000022        16
     9.55104377e-01    2     1000015        24
#         PDG            Width
DECAY   2000013     4.79294583e-05
#          BR         NDA      ID1       ID2       ID3
     2.65168414e-01    3     1000015        13       -15
     1.65177090e-04    3     1000015        14       -16
     7.34666409e-01    3    -1000015        13        15
#         PDG            Width
DECAY   1000014     2.47020762e-01
#          BR         NDA      ID1       ID2       ID3
     9.62659865e-01    2     1000022        14
     3.73401354e-02    2     2000013        24
#         PDG            Width
DECAY   2000011     4.83259858e-05
#          BR         NDA      ID1       ID2       ID3
     2.65345247e-01    3     1000015        11       -15
     3.86409556e-09    3     1000015        12       -16
     7.34654749e-01    3    -1000015        11        15
     8.53942344e-15    3     2000013        11       -13
     1.42135685e-14    3    -2000013        11        13
#         PDG            Width
DECAY   1000012     2.37840354e-01
#          BR         NDA      ID1       ID2       ID3
     9.99999094e-01    2     1000022        12
     9.06144567e-07    2     2000011        24
#         PDG            Width
DECAY   1000011     2.20749383e-01
#          BR         NDA      ID1       ID2       ID3
     9.99998907e-01    2     1000022        11
     5.80948673e-07    2     2000011        25
     5.11932152e-07    2     2000011        23
#         PDG            Width
DECAY   1000013     2.31212961e-01
#          BR         NDA      ID1       ID2       ID3
     9.55380462e-01    2     1000022        13
     2.37016502e-02    2     2000013        25
     2.09178883e-02    2     2000013        23
#         PDG            Width
DECAY   2000015     4.19596430e+00
#          BR         NDA      ID1       ID2       ID3
     6.39363959e-02    2     1000022        15
     3.39389856e-01    2     1000015        25
     5.96673748e-01    2     1000015        23
#         PDG            Width
DECAY   1000021     8.69824008e+00
#          BR         NDA      ID1       ID2       ID3
     5.87295780e-02    2     1000001        -1
     5.87295780e-02    2    -1000001         1
     5.80993921e-03    2     2000001        -1
     5.80993921e-03    2    -2000001         1
     1.32988817e-01    2     1000002        -2
     1.32988817e-01    2    -1000002         2
     3.21135669e-02    2     2000002        -2
     3.21135669e-02    2    -2000002         2
     5.87295780e-02    2     1000003        -3
     5.87295780e-02    2    -1000003         3
     5.80993921e-03    2     2000003        -3
     5.80993921e-03    2    -2000003         3
     1.32988817e-01    2     1000004        -4
     1.32988817e-01    2    -1000004         4
     3.21135669e-02    2     2000004        -4
     3.21135669e-02    2    -2000004         4
     3.43314613e-02    2     1000005        -5
     3.43314613e-02    2    -1000005         5
     6.38473696e-03    2     2000005        -5
     6.38473696e-03    2    -2000005         5
#         PDG            Width
DECAY   1000022     1.75625038e+00
#          BR         NDA      ID1       ID2       ID3
     3.47724412e-01    2     1000015       -15
     3.47724412e-01    2    -1000015        15
     7.63362682e-02    2     2000013       -13
     7.63362682e-02    2    -2000013        13
     7.59393200e-02    2     2000011       -11
     7.59393200e-02    2    -2000011        11
#         PDG            Width
DECAY   1000023     2.71852061e+00
#          BR         NDA      ID1       ID2       ID3
     4.30024806e-01    2     1000015       -15
     4.30024806e-01    2    -1000015        15
     5.64924740e-03    2     2000013       -13
     5.64924740e-03    2    -2000013        13
     4.82354902e-03    2     2000011       -11
     4.82354902e-03    2    -2000011        11
     2.26109411e-03    2     1000011       -11
     2.26109411e-03    2    -1000011        11
     2.27200525e-03    2     1000013       -13
     2.27200525e-03    2    -1000013        13
     5.15874728e-03    2     2000015       -15
     5.15874728e-03    2    -2000015        15
     2.60324870e-03    2     1000016       -16
     2.60324870e-03    2    -1000016        16
     1.81198912e-03    2     1000014       -14
     1.81198912e-03    2    -1000014        14
     1.81004093e-03    2     1000012       -12
     1.81004093e-03    2    -1000012        12
     6.62972310e-03    2     1000022        23
     8.05408211e-02    2     1000022        25
#         PDG            Width
DECAY   1000025     2.85620404e+00
#          BR         NDA      ID1       ID2       ID3
     4.22406396e-01    2     1000015       -15
     4.22406396e-01    2    -1000015        15
     1.20119134e-03    2     2000013       -13
     1.20119134e-03    2    -2000013        13
     3.95053574e-04    2     2000011       -11
     3.95053574e-04    2    -2000011        11
     1.38113444e-05    2     1000011       -11
     1.38113444e-05    2    -1000011        11
     7.96975194e-05    2     1000013       -13
     7.96975194e-05    2    -1000013        13
     2.20191050e-02    2     2000015       -15
     2.20191050e-02    2    -2000015        15
     1.22391791e-04    2     1000016       -16
     1.22391791e-04    2    -1000016        16
     9.94995004e-05    2     1000014       -14
     9.94995004e-05    2    -1000014        14
     9.94407366e-05    2     1000012       -12
     9.94407366e-05    2    -1000012        12
     1.00716417e-01    2     1000022        23
     6.41041064e-03    2     1000022        25
#         PDG            Width
DECAY   1000035     3.57480976e+00
#          BR         NDA      ID1       ID2       ID3
     2.61242207e-02    2     1000015       -15
     2.61242207e-02    2    -1000015        15
     9.66142285e-04    2     2000013       -13
     9.66142285e-04    2    -2000013        13
     9.42926265e-04    2     2000011       -11
     9.42926265e-04    2    -2000011        11
     4.76269043e-02    2     1000011       -11
     4.76269043e-02    2    -1000011        11
     4.77136549e-02    2     1000013       -13
     4.77136549e-02    2    -1000013        13
     7.43151402e-02    2     2000015       -15
     7.43151402e-02    2    -2000015        15
     6.35673911e-02    2     1000016       -16
     6.35673911e-02    2    -1000016        16
     5.82402284e-02    2     1000014       -14
     5.82402284e-02    2    -1000014        14
     5.82261221e-02    2     1000012       -12
     5.82261221e-02    2    -1000012        12
     2.17307680e-03    2     1000022        23
     7.35797544e-04    2     1000023        23
     1.83302911e-03    2     1000025        23
     1.00568197e-01    2     1000024       -24
     1.00568197e-01    2    -1000024        24
     1.68966271e-02    2     1000022        25
     2.17796147e-02    2     1000023        25
#         PDG            Width
DECAY   1000024     2.54951764e+00
#          BR         NDA      ID1       ID2       ID3
     3.35097230e-02    2     1000016       -15
     5.56638233e-03    2     1000014       -13
     5.51297255e-03    2     1000012       -11
     8.64886123e-01    2    -1000015        16
     1.79285537e-03    2    -2000013        14
     4.19061829e-08    2    -2000011        12
     2.95430375e-03    2    -1000011        12
     2.94216760e-03    2    -1000013        14
     3.29167260e-04    2    -2000015        16
     8.25062633e-02    2     1000022        24
#         PDG            Width
DECAY   1000037     3.50834234e+00
#          BR         NDA      ID1       ID2       ID3
     1.56546622e-01    2     1000016       -15
     1.06083969e-01    2     1000014       -13
     1.05959918e-01    2     1000012       -11
     4.08852200e-02    2    -1000015        16
     6.43932353e-05    2    -2000013        14
     1.50443309e-09    2    -2000011        12
     1.09859346e-01    2    -1000011        12
     1.09937935e-01    2    -1000013        14
     1.31712337e-01    2    -2000015        16
     7.65201694e-02    2     1000024        23
     1.61895695e-02    2     1000022        24
     1.05630245e-01    2     1000023        24
     1.79880575e-02    2     1000025        24
     2.26222179e-02    2     1000024        25
