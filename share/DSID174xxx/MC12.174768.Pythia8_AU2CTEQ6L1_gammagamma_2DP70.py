evgenConfig.description = "Pythia8 diphoton sample. gammagamma events with >=2 hard process or parton shower photons with pT > 70 GeV"
evgenConfig.keywords = ["egamma", "performance", "gamma", "QCD"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PromptPhoton:ffbar2gammagamma = on",
                            "PromptPhoton:gg2gammagamma = on",
                            "PhaseSpace:pTHatMin = 65"]

include("MC12JobOptions/DirectPhotonFilter.py")

topAlg.DirectPhotonFilter.Ptcut = 70000.
topAlg.DirectPhotonFilter.Etacut =  2.7
topAlg.DirectPhotonFilter.NPhotons = 2



