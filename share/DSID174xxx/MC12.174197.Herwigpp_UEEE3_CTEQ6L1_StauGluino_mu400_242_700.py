from AthenaCommon import Logging
log = Logging.logging.getLogger('Generate.gmsb')


if not 'evgenConfig' in dir():
  raise RuntimeError('These jobOptions should be run through Generate_trf.py')


evgenConfig.description = 'GMSB inspired Simplified Model slha file Herwigpp_UEEE3_CTEQ6L1_Stau.mu400.242.479326_700.000000.slha from James Barnard (Melb Uni)'
evgenConfig.generators = ['Herwigpp']
evgenConfig.keywords = ['SUSY']
evgenConfig.contact  = ['Aldo.Saavedra@cern.ch']
evgenConfig.auxfiles += ['MSSM.model']

include("MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_Common.py")
include("MC12JobOptions/Herwigpp_SUSYConfig.py")

## Add Herwig++ parameters for this process
sparticle_list = []
sparticle_list.append('gluino')




slha_file = 'susy_stau_mu400_242_479326_700_000000.slha'
cmds = buildHerwigppCommands(sparticle_list, slha_file, 'TwoParticleInclusive')

print '*** Begin Herwig++ SUSY commands ***'
print cmds
print '*** End Herwig++ SUSY commands ***'

## Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds
