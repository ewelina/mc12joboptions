MODEL = 'regge'
CASE = 'stop'
MASS = 1200

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic stop 1200GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
