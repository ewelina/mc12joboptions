include('MC12JobOptions/Herwigpp_UED_UEEE3_CTEQ6L1_MUED_P.py')

# no lepton filter
include ( 'MC12JobOptions/MultiLeptonFilter.py' )
topAlg.MultiLeptonFilter.NLeptons = 0

include ( 'MC12JobOptions/METFilter.py' )
include ( 'MC12JobOptions/JetFilterAkt4.py' )
topAlg.METFilter.MissingEtCut  = 60*GeV
topAlg.QCDTruthJetFilter.MinPt = 80*GeV
topAlg.QCDTruthJetFilter.MaxEta= 3.2
