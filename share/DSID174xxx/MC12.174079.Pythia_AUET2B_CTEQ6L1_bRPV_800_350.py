include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
slha_file = "susy_bRPV_800_350.slha"
topAlg.Pythia.SusyInputFile = slha_file

topAlg.Pythia.PythiaCommand += ["pysubs msel 39", "pymssm imss 1 11"]
topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000"]
topAlg.Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]
topAlg.Pythia.PythiaCommand += ["pypars mstp 95 0"]
topAlg.Pythia.PythiaCommand += ["pydat1 mstj 22 3"]
topAlg.Pythia.PythiaCommand += ["pydat1 parj 72 100000."]

evgenConfig.contact = [ "Andreas.Redelbach@physik.uni-wuerzburg.de" ]
evgenConfig.description = "SUSY model with bilinear R-parity violation"
evgenConfig.process = "add process"
evgenConfig.keywords = ["SUSY", "addkw2"]
evgenConfig.auxfiles += [ slha_file ]
