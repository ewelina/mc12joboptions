
evgenConfig.generators = ["Pythia8"]

include("MC12JobOptions/Pythia8_4C_Common.py")

topAlg.Pythia8.Commands += ["SLHA:file = susy_pMSSM_ecfa_2.slha",
                            "SUSY:all = on",
                            "PartonLevel:MPI = on",
                            "PartonLevel:ISR = on",
                            "PartonLevel:FSR = on",
                            "HadronLevel:Hadronize = on",
                            ]

evgenConfig.contact = ['william.kalderon@cern.ch']
evgenConfig.description = "pMSSM points for ECFA studies; light stop, sbottom, neutralino and chargino"
evgenConfig.keywords = ["SUSY","pMSSM","inclusive","sbottom","stop","neutralino","chargino"]
