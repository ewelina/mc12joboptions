# NUHM with gaugino mediation and snu_tau NLSP model
# Paper: arXiv:hep-ph/0703130v3
# Title: Collider signatures of gravitino dark matter with a sneutrino NLSP
# By: Laura Covi, Sabine Kraml

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

slha_file = "susy_NUHMG_7200000_810_10_2180.slha"
topAlg.Pythia.SusyInputFile = slha_file

# Include production channels  
topAlg.Pythia.PythiaCommand += ["pysubs msel 40",
                                "pymssm imss 1 11"]   # Read mass spectrum from LesHouches file

include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

evgenConfig.contact = [ "Judita.Mamuzic at cern.ch" ]
evgenConfig.description = "PYTHIA 6 generation of Non universal Higgs Masses model with gaugino mediation and snu_tau NLSP with gravitino dark matter"
evgenConfig.process = "PYTHIA 6 supprots three body decays needed by NUHMG model using all production channels and no generator filters"
evgenConfig.keywords = ["SUSY", "NUHM", "NUHMG", "gaugino", "snu_tau", "lsp"]
evgenConfig.auxfiles += [ slha_file ]

