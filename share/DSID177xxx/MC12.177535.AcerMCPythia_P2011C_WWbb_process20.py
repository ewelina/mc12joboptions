evgenConfig.description = "AcerMC+Pythia6 WWbb production with P2011C tune"
evgenConfig.generators = ["AcerMC", "Pythia"]
evgenConfig.keywords = ["WWbb","top","ttbar","Wt"]
evgenConfig.contact  = ["Antoine Marzin <antoine.marzin@cern.ch>"]
evgenConfig.inputfilecheck = 'WWbb_process_20'

include("MC12JobOptions/Pythia_Perugia2011C_Common.py")
topAlg.Pythia.PythiaCommand += ["pyinit user acermc"]
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
