evgenConfig.description = "ALPGEN+Jimmy ttbar(->lnln) process with AUET2 tune and pdf CTEQ6L1, MLM syst variation"
evgenConfig.contact = ["mark.hohlfeld@cern.ch"]
evgenConfig.keywords = ["top"]
evgenConfig.inputfilecheck = "ttbarlnlnNp1"
evgenConfig.minevents = 5000

include("MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py")
include("MC12JobOptions/Jimmy_Tauola.py")
include("MC12JobOptions/Jimmy_Photos.py")
