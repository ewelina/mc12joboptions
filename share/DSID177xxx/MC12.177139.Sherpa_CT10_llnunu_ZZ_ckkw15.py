include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa 1.4.0 diboson llnunu, exclusive only Z(y*)Z(y*), high mass sample, high mass sample, up to 3 jets with ME+PS, merging cut variation"
evgenConfig.keywords = ["diboson", "llnunu", "llnn", "ZZ", "ckkw15"]
evgenConfig.contact  = ["frank.siegert@cern.ch", "christian.schillo@cern.ch"]

evgenConfig.process="""
(run){
  ACTIVE[6]=0
  ACTIVE[24]=0
  ACTIVE[25]=0
  ERROR=0.02
  MASSIVE[11]=1
  MASSIVE[13]=1
  MASSIVE[15]=1
  PARTICLE_CONTAINER 991[m:-1] leptons 11 -11 13 -13 15 -15;
}(run)

(processes){
  Process 93 93 -> 11 -11 91 91 93{2};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 13 -13 91 91 93{2};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;

  Process 93 93 -> 15 -15 91 91 93{2};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)

(selector){
   Mass  11 -11  4.0  E_CMS
   Mass  13 -13  4.0  E_CMS
   Mass  15 -15  4.0  E_CMS
   "PT" 991 5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""
