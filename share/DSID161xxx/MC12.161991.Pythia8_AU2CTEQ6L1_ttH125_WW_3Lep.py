evgenConfig.description = "SM Higgs (125) to WW with 3Lepton requirement"
evgenConfig.keywords = ["SMhiggs","ttH","diboson","W"]
evgenConfig.contact = ['Huaqiao.Zhang@cern.ch']

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
topAlg.Pythia8.Commands += [
## higgs mass point setting
				'25:m0 = 125',
## higgs width setting
				'25:mWidth = 0.00407',
				'25:doForceWidth = true',
## higgs production mode setting
				'HiggsSM:gg2Httbar = on',
				'HiggsSM:qqbar2Httbar = on',
## higgs decay mode setting
				'25:onMode = off', ## turn off all Higgs decay channel
				'25:onIfMatch = 24 24', ## Turn on H->WW
## W decay mode setting
				'24:onMode = off', ## turn off all W decay channel
				'24:mMin = 2.0',
				'24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                              ]
#---------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import TtHtoVVDecayFilter
topAlg += TtHtoVVDecayFilter()
topAlg.TtHtoVVDecayFilter.PDGGrandParent = [25,6,-6]
topAlg.TtHtoVVDecayFilter.PDGParent = [24]
topAlg.TtHtoVVDecayFilter.StatusParent = [22]
topAlg.TtHtoVVDecayFilter.PDGChild = [11,12,13,14,15,16]
topAlg.TtHtoVVDecayFilter.TotalParent = 3
topAlg.TtHtoVVDecayFilter.SameChargeParent = 0
# Set output level threshold (2=DEBUG, 3=INFO, 4=WARNING, 5=ERROR, 6=FATAL )
topAlg.TtHtoVVDecayFilter.OutputLevel = ERROR
StreamEVGEN.RequireAlgs = [ "TtHtoVVDecayFilter" ]

