evgenConfig.description = "PYTHIA8 ttH H->WW inclusive with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ttH", "W","inclusive"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 110',
                            '25:mWidth = 0.00285',
                            '25:doForceWidth = true',
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            'HiggsSM:gg2Httbar = on',
                            'HiggsSM:qqbar2Httbar = on'     
                            ]
