evgenConfig.description = "POWHEG+JIMMY VBF H->WW->lvlv with AUET2 CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "W","leptonic"]
evgenConfig.inputfilecheck = "VBFH_SM_M125"

include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
evgenConfig.generators += ["Powheg"]

include("MC12JobOptions/Jimmy_Tauola.py" )
include("MC12JobOptions/Jimmy_Photos.py" )

# ID=1-6 for dd,...,tt, 7-9 for ee,mumu,tautau, 10,11,12 for WW,ZZ,gamgam
# iproc=-100-ID (lhef=-100)
topAlg.Herwig.HerwigCommand += [ "iproc -110" ]
# W decay
topAlg.Herwig.HerwigCommand += [ "modbos 1 5" ]
topAlg.Herwig.HerwigCommand += [ "modbos 2 5" ]
topAlg.Herwig.HerwigCommand += [ "taudec TAUOLA" ]

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

topAlg.Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
topAlg.Multi1TLeptonFilter.Ptcut = 15000.
topAlg.Multi1TLeptonFilter.Etacut = 5.0
topAlg.Multi1TLeptonFilter.NLeptons = 1

topAlg.Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
topAlg.Multi2LLeptonFilter.Ptcut = 5000.
topAlg.Multi2LLeptonFilter.Etacut = 5.0
topAlg.Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
