evgenConfig.description = "Herwig WW inclusive"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson","W"]
evgenConfig.contact  = ["thorsten.kuhl@cern.ch"]

include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12800"]    # note that 10000 is added to the herwig process number

# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )

