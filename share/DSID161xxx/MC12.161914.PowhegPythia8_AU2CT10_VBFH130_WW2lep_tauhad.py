evgenConfig.description = "POWHEG+PYTHIA8 VBF H->WW->tau_had v l v with AU2 CT10"
evgenConfig.keywords = ["SMhiggs", "VBF", "W","tau"]
evgenConfig.inputfilecheck = "VBFH_SM_M130"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
# ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 24 24',
                            '24:onMode = off',#decay of W
                            '24:mMin = 2.0',
                            '24:onIfMatch = 11 12',
                            '24:onIfMatch = 13 14',
                            '24:onIfMatch = 15 16'
                            ]

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import TauFilter
topAlg += TauFilter()

TauFilter = topAlg.TauFilter
TauFilter.Ntaus = 1
TauFilter.EtaMaxe = 0
TauFilter.EtaMaxmu = 0
TauFilter.Ptcuthad = 0
TauFilter.EtaMaxhad = 10
TauFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=  [ "TauFilter" ]

