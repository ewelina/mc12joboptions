evgenConfig.description = "POWHEG+PYTHIA6 VBFH H->WW->lvlv with AUET2B,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "W","leptonic"]
evgenConfig.inputfilecheck = "VBFH_SM_M125"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
evgenConfig.generators += ["Lhef"]

topAlg.Pythia.PythiaCommand += [   "pysubs ckin 45 2.",
                                   "pysubs ckin 47 2.",
                                   "pydat3 mdme 190 1 0", # W decay
                                   "pydat3 mdme 191 1 0",
                                   "pydat3 mdme 192 1 0",
                                   "pydat3 mdme 194 1 0",
                                   "pydat3 mdme 195 1 0",
                                   "pydat3 mdme 196 1 0",
                                   "pydat3 mdme 198 1 0",
                                   "pydat3 mdme 199 1 0",
                                   "pydat3 mdme 200 1 0",
                                   "pydat3 mdme 206 1 1",
                                   "pydat3 mdme 207 1 1",
                                   "pydat3 mdme 208 1 1",
                                   "pydat3 mdme 210 1 0", # Higgs decay
                                   "pydat3 mdme 211 1 0",
                                   "pydat3 mdme 212 1 0",
                                   "pydat3 mdme 213 1 0",
                                   "pydat3 mdme 214 1 0",
                                   "pydat3 mdme 215 1 0",
                                   "pydat3 mdme 218 1 0",
                                   "pydat3 mdme 219 1 0",
                                   "pydat3 mdme 220 1 0",
                                   "pydat3 mdme 222 1 0",
                                   "pydat3 mdme 223 1 0",
                                   "pydat3 mdme 224 1 0",
                                   "pydat3 mdme 225 1 0",
                                   "pydat3 mdme 226 1 1" ]

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter("Multi1TLeptonFilter")
topAlg += MultiLeptonFilter("Multi2LLeptonFilter")

Multi1TLeptonFilter = topAlg.Multi1TLeptonFilter
Multi1TLeptonFilter.Ptcut = 15000.
Multi1TLeptonFilter.Etacut = 5.0
Multi1TLeptonFilter.NLeptons = 1

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.Ptcut = 5000.
Multi2LLeptonFilter.Etacut = 5.0
Multi2LLeptonFilter.NLeptons = 2

StreamEVGEN.RequireAlgs  = [ "Multi1TLeptonFilter" ]
StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
