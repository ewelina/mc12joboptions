evgenConfig.description = "Herwig ZZ inclusive"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson","Z"]
evgenConfig.contact  = ["thorsten.kuhl@cern.ch"]
#
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12810"]    # note that 10000 is added to the herwig process number
# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )
