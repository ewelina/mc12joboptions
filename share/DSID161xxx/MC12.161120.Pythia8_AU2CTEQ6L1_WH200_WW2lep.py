evgenConfig.description = "PYTHIA8 WH H->WW->lvlv with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "WH","W","leptonic"]

#include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 200',
                            '25:mWidth = 1.43',
                            '25:onMode = off',
                            '25:doForceWidth = true',
                            '25:onIfMatch = 24 24',
                            'HiggsSM:ffbar2HW = on',
                            '24:onMode = off',
                            '24:mMin = 2.0',
                            '24:onIfAny = 1 2 3 4 5 6 11 12 13 14 15 16'
                            ]

include("MC12JobOptions/XtoVVDecayFilter.py")
topAlg.XtoVVDecayFilter.PDGGrandParent = 25
topAlg.XtoVVDecayFilter.PDGParent = 24
topAlg.XtoVVDecayFilter.StatusParent = 22
topAlg.XtoVVDecayFilter.PDGChild1 = [11,12,13,14,15,16]
topAlg.XtoVVDecayFilter.PDGChild2 = [11,12,13,14,15,16]

