evgenConfig.description = "PYTHIA8 ZH Z->nunu H->bb with AU2 CTEQ6L1"
evgenConfig.keywords = ["SMhiggs", "ZH","nu","bottom"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
                            '25:m0 = 110',
                            '25:mWidth = 0.00285',
                            '25:doForceWidth = true', 
                            'HiggsSM:ffbar2HZ = on',
                            '25:onMode = off',
                            '25:onIfMatch = 5 5',
                            '23:onMode = off',
                            '23:onIfMatch = 12 12',
                            '23:onIfMatch = 14 14',
                            '23:onIfMatch = 16 16',
                            'PhaseSpace:pTHatMin = 100'
                            ]
