evgenConfig.description = "POWHEG+PYTHIA6 ggH H->WW->lvlv with P2011C tuning, Renomalization/Factorization scale up "
evgenConfig.keywords = ["SMhiggs", "ggF", "W","leptonic"]
evgenConfig.contact = ["david.hall@cern.ch"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/PowhegPythia_Perugia2011C_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

topAlg.Pythia.PythiaCommand += [   "pysubs ckin 45 2.",
                                   "pysubs ckin 47 2.",
                                   "pydat3 mdme 190 1 0", # W decay
                                   "pydat3 mdme 191 1 0",
                                   "pydat3 mdme 192 1 0",
                                   "pydat3 mdme 194 1 0",
                                   "pydat3 mdme 195 1 0",
                                   "pydat3 mdme 196 1 0",
                                   "pydat3 mdme 198 1 0",
                                   "pydat3 mdme 199 1 0",
                                   "pydat3 mdme 200 1 0",
                                   "pydat3 mdme 206 1 1",
                                   "pydat3 mdme 207 1 1",
                                   "pydat3 mdme 208 1 1",
                                   "pydat3 mdme 210 1 0", # Higgs decay
                                   "pydat3 mdme 211 1 0",
                                   "pydat3 mdme 212 1 0",
                                   "pydat3 mdme 213 1 0",
                                   "pydat3 mdme 214 1 0",
                                   "pydat3 mdme 215 1 0",
                                   "pydat3 mdme 218 1 0",
                                   "pydat3 mdme 219 1 0",
                                   "pydat3 mdme 220 1 0",
                                   "pydat3 mdme 222 1 0",
                                   "pydat3 mdme 223 1 0",
                                   "pydat3 mdme 224 1 0",
                                   "pydat3 mdme 225 1 0",
                                   "pydat3 mdme 226 1 1" ]

