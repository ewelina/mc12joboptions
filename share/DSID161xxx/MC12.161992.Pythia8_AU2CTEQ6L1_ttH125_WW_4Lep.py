evgenConfig.description = "SM Higgs (125) to WW with 4Lepton requirement"
evgenConfig.keywords = ["SMhiggs","ttH","diboson","W"]
evgenConfig.contact = ['Huaqiao.Zhang@cern.ch']

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")
topAlg.Pythia8.Commands += [
## higgs mass point setting
				'25:m0 = 125',
## higgs width setting
				'25:mWidth = 0.00407',
				'25:doForceWidth = true',
## higgs production mode setting
				'HiggsSM:gg2Httbar = on',
				'HiggsSM:qqbar2Httbar = on',
## higgs decay mode setting
				'25:onMode = off', ## turn off all Higgs decay channel
				'25:onIfMatch = 24 24', ## Turn on H->WW
## W decay mode setting
				'24:onMode = off', ## turn off all W decay channel
				'24:mMin = 2.0',
				'24:onIfMatch = 11 12',
				'24:onIfMatch = 13 14',
				'24:onIfMatch = 15 16'
                              ]
#---------------------------------------------------------------

