evgenConfig.description = "DD minimum bias, with Monash tune"
evgenConfig.keywords = ["QCD", "minBias", "DD"]

include("MC12JobOptions/Pythia8_Monash_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]


