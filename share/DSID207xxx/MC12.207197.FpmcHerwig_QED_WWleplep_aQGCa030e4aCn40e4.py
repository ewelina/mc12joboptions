evgenConfig.description = "aQGC a0W=3.0e-4 and aCW=-4.0e-4 with 500GeV cutoff in exclusive gamgam->WW->lnulnu no hadronic tau"          
evgenConfig.keywords = ["QED" ,"WW", "exclusive", "AQGC" ]                                                                                    

evgenConfig.contact = ["Chav Chhiv Chau <chav.chhiv.chau@cern.ch>"]
evgenConfig.generators += ["FPMC"]
evgenConfig.inputfilecheck = 'FpmcHerwig_aQGCa030e4acn40e4'
include("MC12JobOptions/HepMCReadFromFile_Common.py")
evgenConfig.tune = "none"
