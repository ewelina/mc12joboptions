include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "WZgamma -> e nu e-e+ gamma  production with up to 1 jets in ME+PS"
evgenConfig.keywords = [ "EW", "Triboson" ]
evgenConfig.minevents = 3000
evgenConfig.contact = [ "jhofmann@cern.ch" ]
evgenConfig.inputconfcheck = "207262.Sherpa_CT10_WZgamma_elelelgamma"

evgenConfig.process="""
(run){
ACTIVE[25]=0
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
 #W plus
 Process 93 93 -> 11 -12 11 -11 22 93{1}
 Order_EW 5;
 CKKW sqr(20/E_CMS)
 Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
 Integration_Error 0.05 {5,6,7}
 End process;
 
 #W minus
 Process 93 93 -> -11 12 11 -11 22 93{1}
 Order_EW 5;
 CKKW sqr(20/E_CMS)
 Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
 Integration_Error 0.05 {5,6,7}
 End process;
 
}(processes)

(selector){
 PT 22 10.0 E_CMS
 PT 90 12.0 E_CMS
 
 PseudoRapidity 22 -3.0 3.0
 PseudoRapidity 90 -3.0 3.0
 PseudoRapidity 93 -6.0 6.0
 
 DeltaR 22 22 0.3 1000
 DeltaR 22 93 0.3 1000
 DeltaR 22 90 0.3 1000
 DeltaR 93 90 0.2 1000
 DeltaR 93 93 0.3 1000
 
 Mass 11 -11 1.0 E_CMS
 Mass 13 -13 1.0 E_CMS
 Mass 15 -15 1.0 E_CMS
}(selector)

"""
