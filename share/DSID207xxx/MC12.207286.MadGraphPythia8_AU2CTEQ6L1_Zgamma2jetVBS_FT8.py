include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfMatch = 11',
                            '23:onIfMatch = 13',
                            '23:onIfMatch = 15',]
## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.description = "Madgraph Z gamma +2jet vector boson scattering with Z->l+l-(LO EWK  contribution only) with FT8=3 TeV^-4"
evgenConfig.keywords = ["Zgamma", "2jet", "VBS", "2lepton"]
evgenConfig.inputfilecheck = 'Zgamma2jetsVBS_2lepton_LT8_LT9_FT8'
