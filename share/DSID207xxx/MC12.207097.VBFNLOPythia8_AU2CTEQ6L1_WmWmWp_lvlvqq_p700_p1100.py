evgenConfig.description = "VBFNLO+Pythia8+AU2CTEQ6L1 production W-W-W+ -> lvlvqq (l=e,mu,tau) with aQGC"
evgenConfig.keywords = ["triboson", "EW", "WWW", "aQGC"]
evgenConfig.inputfilecheck = "vbfnlo.207097.WmWmWp_lvlvqq_p700_p1100"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Jianbei Liu<Jianbei.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
evgenConfig.generators = ["VBFNLO", "Pythia8"]
include("MC12JobOptions/Pythia8_Photos.py")

