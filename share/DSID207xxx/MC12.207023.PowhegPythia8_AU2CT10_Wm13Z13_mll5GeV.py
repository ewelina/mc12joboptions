#--------------------------------------------------------------
# Powheg WZ setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WZ_Common.py')
PowhegConfig.vdecaymodeW      = -13
PowhegConfig.vdecaymodeZ      = 13
PowhegConfig.mllmin     = 5.0
PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()


#--------------------------------------------------------------
# Pythia8 showering with Photos and CT10
#--------------------------------------------------------------
include('MC12JobOptions/Pythia8_AU2_CT10_Common.py')
include("MC12JobOptions/Pythia8_Powheg.py")
include('MC12JobOptions/Pythia8_Photos.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WZ production with AU2 CT10 tune'
evgenConfig.keywords    = [ 'SM', 'WZ' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
