evgenConfig.description = "Double-diffractive events, with the A2 MSTW2008LO tune. Default PomFlux4"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]
topAlg.Pythia8.Commands += ["Diffraction:PomFlux = 4"]
