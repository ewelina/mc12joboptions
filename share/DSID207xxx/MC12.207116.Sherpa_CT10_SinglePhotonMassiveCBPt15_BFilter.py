evgenConfig.description = "Gamma + up to 4 jets with ME+PS, slice in photon pT and with massive C and B quarks"
evgenConfig.keywords = [ "Z", "photon", "heavyFlavour" ]
evgenConfig.contact  = ["Paul Thompson <thompson@mail.cern.ch>", "frank.siegert@cern.ch"]
evgenConfig.process="SinglePhoton"
evgenConfig.minevents = 100
evgenConfig.inputconfcheck='Sherpa_CT10_SinglePhotonMassiveCBPt15_BFilter'

include( "MC12JobOptions/Sherpa_CT10_Common.py" )

"""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
  QCUT:=30.0
}(run)

(processes){
Process 93  93 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 5  -5 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 4 -4 -> 22 93 93{3}
Order_EW 1
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 93 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 5 -5 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 5 -> 22 5 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 -5 -> 22 -5 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 93 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  4 -> 22 4 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process 93 -4 -> 22 -4 93{3}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
End process;

Process  4 -4 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  5 -5 -> 22 4 -4 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4 -4 -> 22 5 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  5 -> 22 5 4 -4 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93  4 -> 22 4 5 -5 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 -5 -> 22 -5 4 -4 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process 93 -4 -> 22 -4 5 -5 93{1}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4  5 -> 22 4 5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process -4  5 -> 22 -4 5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process  4 -5 -> 22 4 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;

Process -4 -5 -> 22 -4 -5 93{2}
Order_EW 1; Max_N_Quarks 6
CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/PPerp2(p[2]));
Integration_Error 0.1 {4,5};
Scales LOOSE_METS{MU_F2}{MU_R2} {4,5}
Cut_Core 1
End process;
}(processes)

(selector){
DeltaR  22  93  0.3  20.0
PT 22 15.0 E_CMS
PT 93 1.0 E_CMS
PT 4 1.0 E_CMS
PT -4 1.0 E_CMS
PT 5 1.0 E_CMS
PT -5 1.0 E_CMS
}(selector)
"""


from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
topAlg += HeavyFlavorHadronFilter()
HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
HeavyFlavorHadronFilter.RequestBottom=True
HeavyFlavorHadronFilter.RequestCharm=False
HeavyFlavorHadronFilter.Request_cQuark=False
HeavyFlavorHadronFilter.Request_bQuark=False
HeavyFlavorHadronFilter.RequestSpecificPDGID=False
HeavyFlavorHadronFilter.RequireTruthJet=False
HeavyFlavorHadronFilter.BottomPtMin=0*GeV
HeavyFlavorHadronFilter.BottomEtaMax=4.0
StreamEVGEN.RequireAlgs += [ "HeavyFlavorHadronFilter" ]
