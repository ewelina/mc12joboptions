include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "Z(->jj) mu nu with one photon, merged with up to 1 additional QCD jets in ME+PS."
evgenConfig.keywords = [ "electroweak", "SM", "triboson" ]
evgenConfig.minevents = 5000
evgenConfig.contact = [ "abaas@cern.ch" ]
evgenConfig.inputconfcheck = "Sherpa_CT10_WZgamma_mujjgamma"

evgenConfig.process="""
(run){
  MASSIVE[5]=1
  ACTIVE[25]=0
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
Process 93 93 -> 22 13 -14 23[a] 93{1}
DecayOS 23[a] -> 94 94
Order_EW 5
CKKW sqr(20/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
Integration_Error 0.05 {5,6,7}
End process;

Process 93 93 -> 22 13 -14 23[a] 93{1}
DecayOS 23[a] -> 5 -5
Order_EW 5
CKKW sqr(20/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
Integration_Error 0.05 {5,6,7}
End process;

Process 93 93 -> 22 -13 14 23[a] 93{1}
DecayOS 23[a] -> 94 94
Order_EW 5
CKKW sqr(20/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
Integration_Error 0.05 {5,6,7}
End process;

Process 93 93 -> 22 -13 14 23[a] 93{1}
DecayOS 23[a] -> 5 -5
Order_EW 5
CKKW sqr(20/E_CMS)
Scales LOOSE_METS{MU_F2}{MU_R2} {6,7}
Integration_Error 0.05 {5,6,7}
End process;

}(processes)

(selector){
PT 22 10.0 E_CMS
PT 90 12.0 E_CMS
PseudoRapidity 22 -3.0 3.0
PseudoRapidity 90 -3.0 3.0
PseudoRapidity 93 -6.0 6.0

DeltaR 22 22 0.3 1000
DeltaR 22 93 0.3 1000
DeltaR 22 90 0.3 1000
DeltaR 90 93 0.2 1000
DeltaR 93 93 0.3 1000
}(selector)

"""

