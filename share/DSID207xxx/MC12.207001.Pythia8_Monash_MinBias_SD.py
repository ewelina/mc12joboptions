evgenConfig.description = "SD minimum bias, with Monash tune"
evgenConfig.keywords = ["QCD", "minBias","SD"]

include("MC12JobOptions/Pythia8_Monash_Common.py")

topAlg.Pythia8.Commands += ["SoftQCD:singleDiffractive = on"]

