include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10",
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfMatch = 12',
                            '23:onIfMatch = 14',
                            '23:onIfMatch = 16',]
## ... Photos
include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.description = "Madgraph Z gamma +2jet vector boson scattering where Z-->vv~-(LO EWK  contribution only) with FM0=50 TeV^-4"
evgenConfig.keywords = ["Zgamma", "2jet", "VBS", "neutrino"]
evgenConfig.inputfilecheck = 'Zgamma2jetsVBS_2neutrino_LM0123_FM0'
