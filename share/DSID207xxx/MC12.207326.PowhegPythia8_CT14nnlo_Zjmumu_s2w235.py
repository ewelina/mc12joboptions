#--------------------------------------------------------------
# Powheg Zj setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_Zj_Common.py')
PowhegConfig.bornktmin=0.
PowhegConfig.bornsuppfact=0.
PowhegConfig.PDF=13000
PowhegConfig.vdecaymode=2
PowhegConfig.sin2thW_eff=0.235
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and CTEQ6L1
#--------------------------------------------------------------
include('./MC12JobOpts/Pythia8_AZNLO_CTEQ6L1_corr_Common.py')
include('MC12JobOptions/Pythia8_Photos.py')
include('MC12JobOptions/Pythia8_Powheg_Main31.py')
topAlg.Photospp.PhotonSplitting=True
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Zj production with AZNLO tune at s2w=0.235'
evgenConfig.keywords    = [ 'SM', 'Z', 'jet' ]
evgenConfig.contact     = [ 'aaron.james.armbruster@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia8' ]
