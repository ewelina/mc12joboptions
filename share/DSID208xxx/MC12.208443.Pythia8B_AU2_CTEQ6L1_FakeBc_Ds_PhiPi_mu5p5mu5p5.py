##################################################################
# Job options for EvtGen generation of B+ -> K*+(K0Pi+)mu3.5mu3.5.
##################################################################

evgenConfig.description = "Fake B_c+->J/psi(mu5p5mu5p5) D_s+ (->Phi(K+K-) pi+) production"
evgenConfig.keywords    = [ "exclusiveB", "Bc", "dimuons" ]
evgenConfig.minevents   = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

# dirty hack below: let the B+ be B_c+
# NOT for physics studies!
topAlg.Pythia8B.Commands += ['521:name = FakeB_c+'] 
topAlg.Pythia8B.Commands += ['521:antiName = FakeB_c-'] 
topAlg.Pythia8B.Commands += ['521:m0 = 6.2756'] 
topAlg.Pythia8B.Commands += ['521:tau0 = 1.35500e-01'] 

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 10.' ]
topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 10.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 3.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True
topAlg.Pythia8B.NHadronizationLoops       = 5

topAlg.Pythia8B.Commands += ['521:addChannel = 2 1.0 0 443 431']

topAlg.Pythia8B.Commands += ['443:onMode = off' ]
#topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.Commands += ['443:onIfMatch = 13 -13']

topAlg.Pythia8B.Commands += ['431:onMode = 3' ]
topAlg.Pythia8B.Commands += ['431:onIfMatch = 333 211']

topAlg.Pythia8B.SignalPDGCodes = [ 521, 443, 13, -13, 431, 211, 333, 321, -321 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 5.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]


