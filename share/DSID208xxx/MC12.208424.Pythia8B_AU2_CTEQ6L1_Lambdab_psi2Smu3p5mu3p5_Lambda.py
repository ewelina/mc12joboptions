#######################################################################
# Job options for Pythia8B_i generation of Lambda_b->J/psi(mumu)Lambda
#######################################################################
evgenConfig.description = "Signal Lambda_b->psi2S(mumu)Lambda"
evgenConfig.keywords = ["exclusiveBtoJpsi","dimuons","Lambda"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['5122:m0 = 5.6194']
topAlg.Pythia8B.Commands += ['5122:tau0 = 0.427']
topAlg.Pythia8B.Commands += ['5122:33:onMode = on']
topAlg.Pythia8B.Commands += ['100443:onMode = off']
topAlg.Pythia8B.Commands += ['100443:1:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [-5122,100443,-13,13,-3122]
topAlg.Pythia8B.OutputLevel = INFO

topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent  = [5122]
topAlg.ParentChildFilter.PtMinParent =  0.0
topAlg.ParentChildFilter.EtaRangeParent = 1000.
topAlg.ParentChildFilter.PDGChild = [3122]
topAlg.ParentChildFilter.PtMinChild = 1000.
topAlg.ParentChildFilter.EtaRangeChild = 1000.

