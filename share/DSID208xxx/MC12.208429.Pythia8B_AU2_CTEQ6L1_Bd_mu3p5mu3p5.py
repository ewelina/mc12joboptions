######################################################
# Job options for Pythia8B_i generation of Bd->mu3p5mu3p5
######################################################
evgenConfig.description = "Exclusive Bd->mu3p5mu3p5 production"
evgenConfig.keywords = ["exclusiveB","Bd","dimuons"]
evgenConfig.minevents = 1000
 
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
# the file above includes also the options commented below:
#topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
#topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
#topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#topAlg.Pythia8B.SelectBQuarks = True
#topAlg.Pythia8B.SelectCQuarks = False
#include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 7.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

topAlg.Pythia8B.Commands += ['511:addChannel = 2 1.0 0 -13 13']
topAlg.Pythia8B.SignalPDGCodes = [511,-13,13]

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.6
topAlg.Pythia8B.MinimumCountPerCut = [2]

