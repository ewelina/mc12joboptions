##############################################################
# bb->J/psi(mu2.5mu2.5)X + Z->ee  
##############################################################
evgenConfig.description = "pp->bb->Jpsi(mumu) + Z->ee via second hard process"
evgenConfig.keywords = ["inclusiveBtoJpsi","dimuons","dielectrons","Z"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.'] # Equivalent of CKIN3

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 4.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# Second hard process (Z->mumu)
topAlg.Pythia8B.Commands += ['23:onMode = off']
topAlg.Pythia8B.Commands += ['23:5:onMode = on']
topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:SingleGmZ = on']

# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]
topAlg.Pythia8B.SignalPtCuts = [0.0,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [102.5,2.5,2.5]

# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 1

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = [20.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
