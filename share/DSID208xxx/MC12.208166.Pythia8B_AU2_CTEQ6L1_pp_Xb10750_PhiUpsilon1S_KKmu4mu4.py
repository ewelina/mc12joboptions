#############################################################################
# Job options fragment for pp->X Xb(10750)(->Upsilon1S(mu4mu4),phi(K+K-))
#############################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Xb(10750)(->phi(K,K),Upsi(mu4mu4))"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

#need to add a chi_b1(3P)
topAlg.Pythia8B.Commands += ['220553:all = chi_1b3P chi_1b3P 3 0 0 10.534 0.00002 10.5337 10.5343 0']

#change mass to mass of "Xb"
mass = 10.750
topAlg.Pythia8B.Commands += ['220553:m0 = '+str(mass)]
topAlg.Pythia8B.Commands += ['220553:mMin = '+str(mass-0.0003)]
topAlg.Pythia8B.Commands += ['220553:mMax = '+str(mass+0.0003)]
topAlg.Pythia8B.Commands += ['220553:mWidth = 0.00002']

topAlg.Pythia8B.Commands += ['9900553:m0 = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900553:mMin = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900553:mMax = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900553:0:products = 220553 21']

topAlg.Pythia8B.Commands += ['9900551:m0 = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900551:mMin = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900551:mMax = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9900551:0:products = 220553 21']

topAlg.Pythia8B.Commands += ['9910551:m0 = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9910551:mMin = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9910551:mMax = ' + str(mass+0.2)]
topAlg.Pythia8B.Commands += ['9910551:0:products = 220553 21']

topAlg.Pythia8B.Commands += ['220553:onMode = off']
#add the chi_b1(3P)->phi,Upsilon(1S) decay mode
topAlg.Pythia8B.Commands += ['220553:addChannel = on 0.5 0 553 333']

#ups->mu mu decay
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']

#omega->pi pi decay
topAlg.Pythia8B.Commands += ['333:onMode = off']
topAlg.Pythia8B.Commands += ['333:0:onMode = on']


topAlg.Pythia8B.SignalPDGCodes = [220553,553,-13,13,333,-321,321]
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.0, 0.4, 0.4]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.3, 2.3, 100.0, 2.5, 2.5]
