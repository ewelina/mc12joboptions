evgenConfig.description = "Fake Bc(2S)+ -> B_c+ pi pi, B_c+->(Psi2S)J/psi(mu2p5mu2p5) mu2p5 nu_mu production"
evgenConfig.keywords    = [ "exclusiveB", "Bc", "dimuons" ]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

# let the B+ be B_c+
topAlg.Pythia8B.Commands += ['521:name = FakeB_c+'] 
topAlg.Pythia8B.Commands += ['521:antiName = FakeB_c-'] 
topAlg.Pythia8B.Commands += ['521:m0 = 6.2751'] #PDG2016
topAlg.Pythia8B.Commands += ['521:tau0 = 1.5199e-01'] #PDG2016

# adjust also normal B_c+ parameters to PDG2016
topAlg.Pythia8B.Commands += ['541:m0 = 6.2751'] #PDG2016
topAlg.Pythia8B.Commands += ['541:tau0 = 1.5199e-01'] #PDG2016

# let the B*+ B_c(2S)+
topAlg.Pythia8B.Commands += ['523:name = FakeB_c(2S)+'] 
topAlg.Pythia8B.Commands += ['523:antiName = FakeB_c(2S)-'] 
topAlg.Pythia8B.Commands += ['523:m0 = 6.8425'] # M(Bc)+288.3+2m(pi)

# disable excited B decays to 521 and 523 in order to avoid energy
# conservation problems
topAlg.Pythia8B.Commands += ['545:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['535:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['525:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['515:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['541:offIfAny = 521 523']

# close B+ -> Charmonia decay to avoid unphysical background
topAlg.Pythia8B.Commands += ['521:offIfAny = 443 100443 445 10441 10443 20443']

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 8.' ]
topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 8.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 2.7
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True
topAlg.Pythia8B.NHadronizationLoops       = 1

topAlg.Pythia8B.Commands += ['523:onMode = 3']
topAlg.Pythia8B.Commands += ['523:addChannel = 2 1.0 0 521 211 -211']

topAlg.Pythia8B.Commands += ['521:addChannel = 2 0.19 0 443 -13 14']
topAlg.Pythia8B.Commands += ['521:addChannel = 2 0.0094 0 100443 -13 14']

topAlg.Pythia8B.Commands += ['443:onMode = off' ]
topAlg.Pythia8B.Commands += ['443:onIfMatch = 13 -13']

topAlg.Pythia8B.SignalPDGCodes = [523]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 2.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 3 ]


