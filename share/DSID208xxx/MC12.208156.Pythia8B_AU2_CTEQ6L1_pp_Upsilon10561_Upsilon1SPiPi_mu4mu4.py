#######################################################################
# Job options fragment for pp->X Upsilon(10561)(->Upsilon1S(mu4mu4),pi,pi)
#######################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Up(10561)(->pi,pi,Upsi(mu4mu4))"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

mass = 10.561
topAlg.Pythia8B.Commands += ['100553:m0 = '+str(mass)]     #Convert 2S into a 1D3J
topAlg.Pythia8B.Commands += ['100553:mMin = '+str(mass-0.0003)]
topAlg.Pythia8B.Commands += ['100553:mMax = '+str(mass+0.0003)]
topAlg.Pythia8B.Commands += ['100553:mWidth = 0.00002']

topAlg.Pythia8B.Commands += ['9900553:m0 = 10.6']
topAlg.Pythia8B.Commands += ['9900553:mMin = 10.6']
topAlg.Pythia8B.Commands += ['9900553:mMax = 10.6']
topAlg.Pythia8B.Commands += ['9900553:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9900551:m0 = 10.6']
topAlg.Pythia8B.Commands += ['9900551:mMin = 10.6']
topAlg.Pythia8B.Commands += ['9900551:mMax = 10.6']
topAlg.Pythia8B.Commands += ['9900551:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9910551:m0 = 10.6']
topAlg.Pythia8B.Commands += ['9910551:mMin = 10.6']
topAlg.Pythia8B.Commands += ['9910551:mMax = 10.6']
topAlg.Pythia8B.Commands += ['9910551:0:products = 100553 21']


topAlg.Pythia8B.Commands += ['100553:onMode = off']
topAlg.Pythia8B.Commands += ['100553:7:onMode = on']
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100553,553,-13,13,-211,211]
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.38, 0.38]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]
