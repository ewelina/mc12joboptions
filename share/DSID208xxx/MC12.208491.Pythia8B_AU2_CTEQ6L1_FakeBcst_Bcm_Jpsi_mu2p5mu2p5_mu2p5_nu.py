evgenConfig.description = "Fake Bc*- -> B_c- gamma, B_c-->(Psi2S)J/psi(mu2p5mu2p5) mu2p5 nu_mu production"
evgenConfig.keywords    = [ "exclusiveB", "Bc", "dimuons" ]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveAntiB_Common.py")

# let the B+ be B_c+
topAlg.Pythia8B.Commands += ['521:name = FakeB_c+'] 
topAlg.Pythia8B.Commands += ['521:antiName = FakeB_c-'] 
topAlg.Pythia8B.Commands += ['521:m0 = 6.2749'] #PDG2018
topAlg.Pythia8B.Commands += ['521:tau0 = 1.5199e-01'] #PDG2018

# adjust also normal B_c+ parameters to PDG2016
topAlg.Pythia8B.Commands += ['541:m0 = 6.2749'] #PDG2018
topAlg.Pythia8B.Commands += ['541:tau0 = 1.5199e-01'] #PDG2018

# let the B*+ B_c(2S)+
topAlg.Pythia8B.Commands += ['523:name = FakeB_c*+'] 
topAlg.Pythia8B.Commands += ['523:antiName = FakeB_c*-'] 
topAlg.Pythia8B.Commands += ['523:m0 = 6.3289'] # M(Bc)+54MeV

# disable excited B decays to 521 and 523 in order to avoid energy
# conservation problems
topAlg.Pythia8B.Commands += ['545:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['535:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['525:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['515:offIfAny = 521 523']
topAlg.Pythia8B.Commands += ['541:offIfAny = 521 523']

# close B+ -> Charmonia decay to avoid unphysical background
topAlg.Pythia8B.Commands += ['521:offIfAny = 443 100443 445 10441 10443 20443']

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 35.' ]
topAlg.Pythia8B.QuarkPtCut                = 35.
topAlg.Pythia8B.AntiQuarkPtCut            = 0.
topAlg.Pythia8B.QuarkEtaCut               = 2.7
topAlg.Pythia8B.AntiQuarkEtaCut           = 102.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True
topAlg.Pythia8B.NHadronizationLoops       = 2

topAlg.Pythia8B.Commands += ['523:onMode = 2']
topAlg.Pythia8B.Commands += ['523:addChannel = 3 1.0 0 521 22']

topAlg.Pythia8B.Commands += ['521:addChannel = 3 0.19 0 443 -13 14']
topAlg.Pythia8B.Commands += ['521:addChannel = 3 0.0094 0 100443 -13 14']

topAlg.Pythia8B.Commands += ['443:onMode = off' ]
topAlg.Pythia8B.Commands += ['443:onIfMatch = 13 -13']

topAlg.Pythia8B.SignalPDGCodes = [-523]

include("MC12JobOptions/BSignalFilter.py")
topAlg.BSignalFilter.LVL1MuonCutOn  = True
topAlg.BSignalFilter.LVL2MuonCutOn  = True
topAlg.BSignalFilter.LVL1MuonCutPT  = 2500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT  = 2500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent  = [523]
topAlg.ParentChildFilter.PtMinParent =  0.
topAlg.ParentChildFilter.EtaRangeParent = 102.7
topAlg.ParentChildFilter.PDGChild = [22]
topAlg.ParentChildFilter.PtMinChild = 800.
topAlg.ParentChildFilter.EtaRangeChild = 102.7
