##############################################################
#
# chiBToJpsimu3p5mu3p5Jpsimu2p5mu2p5
#
# Heavy Quarkonia production instructions followed from twiki
#
# author: Konstantinos.Karakostas @ cern.ch
##############################################################

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "chiB0_1P->J/psi(mu3.5mu3.5)J/psi(mu3.5mu3.5) production with chi_b mass 18 GeV"
evgenConfig.keywords = ["chi_b","2muon","Jpsi"]
evgenConfig.minevents = 2000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

# Process for chi_b(0)1P
topAlg.Pythia8B.Commands += ['Bottomonium:gg2QQbar[3P0(1)]g = on']
topAlg.Pythia8B.Commands += ['Bottomonium:qg2QQbar[3P0(1)]q = on']
topAlg.Pythia8B.Commands += ['10551:m0 = 18.']  # change chi_b mass to 18 GeV
topAlg.Pythia8B.Commands += ['10551:oneChannel = 1 1. 0 443 443']  # <--- chiB_Sig

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 5.']

# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Trigger pattern selections
topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStatePtCut  = [3.5, 2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [2, 4]

