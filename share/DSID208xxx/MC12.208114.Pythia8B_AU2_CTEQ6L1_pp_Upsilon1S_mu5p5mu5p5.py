##############################################################################
# Job options fragment for pp->Upsilon1S(mu5.5mu5.5)X for trigger LS1 studies.
##############################################################################

evgenConfig.description = "Inclusive pp->Upsilon(mu5.5mu5.5)X production"
evgenConfig.keywords    = [ "bottomonium", "dimuons" ]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

#topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 6.' ]
topAlg.Pythia8B.Commands += [ '553:onMode = off' ]
topAlg.Pythia8B.Commands += [ '553:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 553, -13,13 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 5.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]
