##############################################################
# H -> Z(mumu) + J/psi(mumu) 
# A. Cerri - A. Chisholm - S. Leontsinis - K. Nikolopoulos - D. Price
# For the study of  Higgs decay to Z+J/psi
##############################################################

evgenConfig.description = "POWHEG+PYTHIA8 ggF H->Z+J/psi->mumu mumu with AU2,CT10"
evgenConfig.keywords = ["SMhiggs", "ggF", "Jpsi","leptonic"]
evgenConfig.inputfilecheck = "ggH_SM_M125"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:addChannel = 1 0.0001 100 23 443',
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfMatch = 13 -13',
                            '443:onMode = off',
                            '443:onIfMatch = 13 -13'
                            ]
