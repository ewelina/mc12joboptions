evgenConfig.description = "bb->mu4mu4 production, only b->mu-, bbar->c->mu+"
evgenConfig.keywords = ["inclusive","bbbar","dimuons"]
evgenConfig.minevents = 5000

f = open("BbAbbar.DEC","w")
f.write("""
Alias MyD0 D0 
Alias MyD+ D+ 
Alias MyD_s+ D_s+ 
Alias MyLambda_c+ Lambda_c+ 
Alias MyD*+ D*+ 
Alias MyD_2*0 D_2*0 
Alias MyD'_10 D'_10 
Alias MyD_2*+ D_2*+ 
Alias MyD(2S)+ D(2S)+ 
Alias MyD_0*0 D_0*0 
Alias MyLambda_c(2593)+ Lambda_c(2593)+ 
Alias MyD_10 D_10 
Alias MyD_0*+ D_0*+ 
Alias MyD*(2S)+ D*(2S)+ 
Alias MyD_s0*+ D_s0*+ 
Alias MyD_s*+ D_s*+ 
Alias MyD*(2S)0 D*(2S)0 
Alias MyD*0 D*0 
Alias MyD_s1+ D_s1+ 
Alias MyD_s2*+ D_s2*+ 
Alias MyD'_1+ D'_1+ 
Alias MyXi_c+ Xi_c+ 
Alias MySigma_c+ Sigma_c+ 
Alias MySigma_c*0 Sigma_c*0 
Alias MySigma_c++ Sigma_c++ 
Alias MyXi'_c0 Xi'_c0 
Alias MyXi_c0 Xi_c0 
Alias MyD(2S)0 D(2S)0 
Alias MyXi_c*0 Xi_c*0 
Alias MyXi_c*+ Xi_c*+ 
Alias MyLambda_c(2625)+ Lambda_c(2625)+ 
Alias MyXi'_c+ Xi'_c+ 
Alias MyOmega_c0 Omega_c0 
Alias MySigma_c0 Sigma_c0 
Alias MyD_1+ D_1+ 
Alias MySigma_c*+ Sigma_c*+ 
Alias MySigma_c*++ Sigma_c*++ 

Decay anti-B0
  0.050100000 D*+ mu- anti-nu_mu PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.021700000 D+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0054 D_1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0020 D_0*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0050 D'_1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0022 D_2*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0003 D*+ pi0 mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.004900000 D*0 pi+ mu- anti-nu_mu PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0010 D+ pi0 mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.0000 D0 pi+ mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.000134000 pi+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000247000 rho+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000000 D(2S)+ mu- anti-nu_mu PHOTOS ISGW2;
  0.000000 D*(2S)+ mu- anti-nu_mu PHOTOS ISGW2;
  #0.001892 Xu+ mu- anti-nu_mu PHOTOS VUB 4.8 1.29 0.22 20 0.30 0.55 1.20 0.61 1.26 0.85 1.34 1.08 1.41 1.21 1.48 1.30 1.55 1.30 1.61 1.33 1.67 1.36 1.73 1.39 1.79 1.33 1.84 1.42 1.90 1.39 1.95 1.39 2.00 1.37 2.50 1.30 3.00 0.74 3.50 0.99 4.00 1.09 4.50 1.00; #TODO -- temporary workaround
  0.000000450 anti-K0 mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  0.000001050 anti-K*0 mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  #0.0000025 anti-Xsd mu+ mu- PHOTOS BTOXSLL 4.8 0.2 0.0 0.41;
Enddecay

Decay B-
  0.056800000 D*0 mu- anti-nu_mu PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.022300000 D0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0040 D_10 mu- anti-nu_mu PHOTOS ISGW2;
  0.0024 D_0*0 mu- anti-nu_mu PHOTOS ISGW2;
  0.0007 D'_10 mu- anti-nu_mu PHOTOS ISGW2;
  0.0018 D_2*0 mu- anti-nu_mu PHOTOS ISGW2;
  0.006100000 D*+ pi- mu- anti-nu_mu PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0003 D*0 pi0 mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.0000 D+ pi- mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.0010 D0 pi0 mu- anti-nu_mu PHOTOS GOITY_ROBERTS;
  0.000077000 pi0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000037000 eta mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000128000 rho0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000115000 omega mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000270 eta' mu- anti-nu_mu PHOTOS ISGW2;
  0.000000 D(2S)0 mu- anti-nu_mu PHOTOS ISGW2;
  0.000000 D*(2S)0 mu- anti-nu_mu PHOTOS ISGW2;
  0.001948 Xu0 mu- anti-nu_mu VUB 4.8 1.29 0.22 20 0.30 0.54 1.20 0.95 1.26 0.78 1.34 0.98 1.41 0.91 1.48 1.23 1.55 1.36 1.61 1.39 1.67 1.38 1.73 1.43 1.79 1.41 1.84 1.42 1.90 1.45 1.95 1.40 2.00 1.42 2.50 1.31 3.00 1.36 3.50 1.15 4.00 1.01 4.50 1.51;
  0.000000520 K- mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  0.000001160 K*- mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  #0.0000025 anti-Xsu mu+ mu- PHOTOS BTOXSLL 4.8 0.2 0.0 0.41;
  0.000000 mu- anti-nu_mu PHOTOS SLN;
Enddecay

Decay anti-B_s0
  0.0210 D_s+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0490 D_s*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0040 D_s1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0040 D_s0*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0070 D'_s1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0070 D_s2*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.000200 K+ mu- anti-nu_mu PHOTOS ISGW2;
  0.000300 K*+ mu- anti-nu_mu PHOTOS ISGW2;
  0.000300 K_1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.000200 K'_1+ mu- anti-nu_mu PHOTOS ISGW2;
  0.0000000035 mu- mu+ PHSP;
  0.0000023 phi mu- mu+ BTOSLLALI;
Enddecay

Decay Lambda_b0
  0.050000000 Lambda_c+ mu- anti-nu_mu PHSP; #[Reconstructed PDG2011]
  0.006300000 Lambda_c(2593)+ mu- anti-nu_mu PHSP; #[Reconstructed PDG2011]
  0.011000000 Lambda_c(2625)+ mu- anti-nu_mu PHSP; #[Reconstructed PDG2011]
  0.00000 Sigma_c0 pi+ mu- anti-nu_mu PHSP;
  0.00000 Sigma_c+ pi0 mu- anti-nu_mu PHSP;
  0.00000 Sigma_c++ pi- mu- anti-nu_mu PHSP;
  0.00000 Sigma_c*0 pi+ mu- anti-nu_mu PHSP;
  0.00000 Sigma_c*+ pi0 mu- anti-nu_mu PHSP;
  0.00000 Sigma_c*++ pi- mu- anti-nu_mu PHSP;
  0.056000000 Lambda_c+ pi+ pi- mu- anti-nu_mu PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B0
  0.000024000 MyD_s+ pi- PHSP; #[Reconstructed PDG2011]
  0.000021000 MyD_s*+ pi- SVS; #[Reconstructed PDG2011]
  0.000016 rho- MyD_s+ SVS;
  0.000041000 MyD_s*+ rho- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
  0.000211000 D- MyD+ PHSP; #[Reconstructed PDG2011]
  0.000305 D*- MyD+ SVS;
  0.000610000 MyD*+ D- SVS; #[Reconstructed PDG2011]
  0.000820000 D*- MyD*+ SVV_HELAMP 0.56 0.0 0.96 0.0 0.47 0.0; #[Reconstructed PDG2011]
  0.007200000 D- MyD_s+ PHSP; #[Reconstructed PDG2011]
  0.008000000 D*- MyD_s+ SVS; #[Reconstructed PDG2011]
  0.007400000 MyD_s*+ D- SVS; #[Reconstructed PDG2011]
  0.017700000 MyD_s*+ D*- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.0006 D'_1- MyD_s+ SVS;
  0.0012 D'_1- MyD_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 D_1- MyD_s+ SVS;
  0.0024 D_1- MyD_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 D_2*- MyD_s+ STS;
  0.0040 D_2*- MyD_s*+ PHSP;
  0.0018 MyD_s+ D- pi0 PHSP;
  0.0037 MyD_s+ anti-D0 pi- PHSP;
  0.0018 MyD_s*+ D- pi0 PHSP;
  0.0037 MyD_s*+ anti-D0 pi- PHSP;
  0.0030 MyD_s+ D- pi- pi+ PHSP;
  0.0022 MyD_s+ D- pi0 pi0 PHSP;
  0.0022 MyD_s+ anti-D0 pi- pi0 PHSP;
  0.0030 MyD_s*+ D- pi- pi+ PHSP;
  0.0022 MyD_s*+ D- pi0 pi0 PHSP;
  0.0022 MyD_s*+ anti-D0 pi- pi0 PHSP;
  0.001700000 D- MyD0 K+ PHSP; #[Reconstructed PDG2011]
  0.004600000 D- MyD*0 K+ PHSP; #[Reconstructed PDG2011]
  0.003100000 D*- MyD0 K+ PHSP; #[Reconstructed PDG2011]
  0.011800000 D*- MyD*0 K+ PHSP; #[Reconstructed PDG2011]
  0.0015 D- MyD+ K0 PHSP;
  0.0018 D*- MyD+ K0 PHSP;
  0.0047 D- MyD*+ K0 PHSP;
  0.007800000 D*- MyD*+ K0 PHSP; #[Reconstructed PDG2011]
  0.0005 MyD0 anti-D0 K0 PHSP;
  0.0005 MyD*0 anti-D0 K0 PHSP;
  0.0015 MyD0 anti-D*0 K0 PHSP;
  0.0015 MyD*0 anti-D*0 K0 PHSP;
  0.0025 D- MyD0 K*+ PHSP;
  0.0025 D*- MyD0 K*+ PHSP;
  0.0025 D- MyD*0 K*+ PHSP;
  0.0050 D*- MyD*0 K*+ PHSP;
  0.0025 D- MyD+ K*0 PHSP;
  0.0025 D*- MyD+ K*0 PHSP;
  0.0025 D- MyD*+ K*0 PHSP;
  0.0050 D*- MyD*+ K*0 PHSP;
  0.0005 MyD0 anti-D0 K*0 PHSP;
  0.0005 MyD0 anti-D*0 K*0 PHSP;
  0.0005 MyD*0 anti-D0 K*0 PHSP;
  0.0010 MyD*0 anti-D*0 K*0 PHSP;
  0.00001 K*0 MyD0 SVS;
  0.00001 MyD*0 K*0 SVV_HELAMP 1. 0. 1. 0. 1. 0.;
  0.0016 MyD_s0*+ D- PHSP;
  0.0015 D*- MyD_s0*+ SVS;
  0.003500000 MyD_s1+ D- SVS; #[Reconstructed PDG2011]
  0.009300000 D*- MyD_s1+ SVV_HELAMP 0.4904 0. 0.7204 0. 0.4904 0.; #[Reconstructed PDG2011]
  0.000006000 MyD0 K+ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002700000 MyD*0 pi+ pi+ pi- pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000170000 K0 MyD0 anti-D0 pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000011000 anti-Lambda0 Lambda0 MyD0 PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B+
  0.000016000 MyD_s+ pi0 PHSP; #[Reconstructed PDG2011]
  0.000020 MyD_s*+ pi0 SVS;
  0.000028 rho0 MyD_s+ SVS;
  0.000028 MyD_s*+ rho0 SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.010000000 anti-D0 MyD_s+ PHSP; #[Reconstructed PDG2011]
  0.008200000 anti-D*0 MyD_s+ SVS; #[Reconstructed PDG2011]
  0.007600000 MyD_s*+ anti-D0 SVS; #[Reconstructed PDG2011]
  0.017100000 MyD_s*+ anti-D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0; #[Reconstructed PDG2011]
  0.0006 anti-D'_10 MyD_s+ SVS;
  0.0012 anti-D'_10 MyD_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 anti-D_10 MyD_s+ SVS;
  0.0024 anti-D_10 MyD_s*+ SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 anti-D_2*0 MyD_s+ STS;
  0.0040 anti-D_2*0 MyD_s*+ PHSP;
  0.0036 MyD_s+ D- pi+ PHSP;
  0.0018 MyD_s+ anti-D0 pi0 PHSP;
  0.0037 MyD_s*+ D- pi+ PHSP;
  0.0018 MyD_s*+ anti-D0 pi0 PHSP;
  0.0033 MyD_s+ D- pi+ pi0 PHSP;
  0.0033 MyD_s+ anti-D0 pi+ pi- PHSP;
  0.0008 MyD_s+ anti-D0 pi0 pi0 PHSP;
  0.0033 MyD_s*+ D- pi+ pi0 PHSP;
  0.0033 MyD_s*+ anti-D0 pi+ pi- PHSP;
  0.0008 MyD_s*+ anti-D0 pi0 pi0 PHSP;
  0.0017 anti-D0 MyD+ K0 PHSP;
  0.0052 anti-D0 MyD*+ K0 PHSP;
  0.0031 anti-D*0 MyD+ K0 PHSP;
  0.007800000 anti-D*0 MyD*+ K0 PHSP; #[Reconstructed PDG2011]
  0.002100000 anti-D0 MyD0 K+ PHSP; #[Reconstructed PDG2011]
  0.0018 anti-D*0 MyD0 K+ PHSP;
  0.004700000 anti-D0 MyD*0 K+ PHSP; #[Reconstructed PDG2011]
  0.005300000 anti-D*0 MyD*0 K+ PHSP; #[Reconstructed PDG2011]
  0.0005 MyD+ D- K+ PHSP;
  0.0005 MyD*+ D- K+ PHSP;
  0.001500000 MyD+ D*- K+ PHSP; #[Reconstructed PDG2011]
  0.0015 MyD*+ D*- K+ PHSP;
  0.0025 anti-D0 MyD+ K*0 PHSP;
  0.0025 anti-D*0 MyD+ K*0 PHSP;
  0.0025 anti-D0 MyD*+ K*0 PHSP;
  0.0050 anti-D*0 MyD*+ K*0 PHSP;
  0.0025 anti-D0 MyD0 K*+ PHSP;
  0.0025 anti-D*0 MyD0 K*+ PHSP;
  0.0025 anti-D0 MyD*0 K*+ PHSP;
  0.0050 anti-D*0 MyD*0 K*+ PHSP;
  0.0005 MyD+ D- K*+ PHSP;
  0.0005 MyD*+ D- K*+ PHSP;
  0.0005 MyD+ D*- K*+ PHSP;
  0.0010 MyD*+ D*- K*+ PHSP;
  0.000380000 MyD+ anti-D0 PHSP; #[Reconstructed PDG2011]
  0.000390000 MyD*+ anti-D0 SVS; #[Reconstructed PDG2011]
  0.000630000 anti-D*0 MyD+ SVS; #[Reconstructed PDG2011]
  0.000810000 anti-D*0 MyD*+ SVV_HELAMP 0.56 0.0 0.96 0.0 0.47 0.0; #[Reconstructed PDG2011]
  0.0000005 MyD+ pi0 PHSP;
  0.0000005 MyD*+ pi0 SVS;
  0.000011 MyD+ anti-K0 PHSP;
  0.000006 MyD*+ anti-K0 SVS;
  0.00075 anti-D0 MyD_s0*+ PHSP;
  0.00090 anti-D*0 MyD_s0*+ SVS;
  0.003100000 MyD_s1+ anti-D0 SVS; #[Reconstructed PDG2011]
  0.012000000 anti-D*0 MyD_s1+ SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
Enddecay

Decay B_s0
  0.010400000 D_s- MyD_s+ PHSP; #[Reconstructed PDG2011]
  0.0099 MyD_s*+ D_s- SVS;
  0.0099 D_s*- MyD_s+ SVS;
  0.0197 D_s*- MyD_s*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0096 MyD_s+ D- anti-K0 PHSP;
  0.0096 MyD_s+ MyD0 K- PHSP;
  0.0096 MyD_s*+ D- anti-K0 PHSP;
  0.0096 MyD_s*+ anti-D0 K- PHSP;
  0.0024 MyD_s+ D- pi0 anti-K0 PHSP;
  0.0048 MyD_s+ anti-D0 pi- anti-K0 PHSP;
  0.0048 MyD_s+ D- pi+ K- PHSP;
  0.0024 MyD_s+ anti-D0 pi0 K- PHSP;
  0.0024 MyD_s*+ D- pi0 anti-K0 PHSP;
  0.0048 MyD_s*+ anti-D0 pi- anti-K0 PHSP;
  0.0048 MyD_s*+ D- pi+ K- PHSP;
  0.0024 MyD_s*+ anti-D0 pi0 K- PHSP;
  0.0150 D_s*- MyD*0 K+ PHSP;
  0.0150 D_s*- MyD*+ K0 PHSP;
  0.0050 D_s*- MyD0 K+ PHSP;
  0.0050 D_s*- MyD+ K0 PHSP;
  0.0050 D_s- MyD*0 K+ PHSP;
  0.0050 D_s- MyD*+ K0 PHSP;
  0.0020 D_s- MyD0 K+ PHSP;
  0.0020 D_s- MyD+ K0 PHSP;
  0.0030 D_s*- MyD*0 K*+ PHSP;
  0.0030 D_s*- MyD*+ K*0 PHSP;
  0.0050 D_s*- MyD0 K*+ PHSP;
  0.0050 D_s*- MyD+ K*0 PHSP;
  0.0025 D_s- MyD*0 K*+ PHSP;
  0.0025 D_s- MyD*+ K*0 PHSP;
  0.0025 D_s- MyD0 K*+ PHSP;
  0.0025 D_s- MyD+ K*0 PHSP;
  0.0017 MyD_s+ D- PHSP;
  0.0017 D*- MyD_s+ SVS;
  0.0017 MyD_s*+ D- SVS;
  0.0017 MyD_s*+ D*- SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0007 MyD+ D- anti-K0 PHSP;
  0.0007 MyD+ MyD0 K- PHSP;
  0.0007 MyD*+ D- anti-K0 PHSP;
  0.0007 MyD*+ anti-D0 K- PHSP;
  0.0003 MyD+ D- pi0 anti-K0 PHSP;
  0.0007 MyD+ anti-D0 pi- anti-K0 PHSP;
  0.0003 MyD+ D- pi+ K- PHSP;
  0.0007 MyD+ anti-D0 pi0 K- PHSP;
  0.0003 MyD*+ D- pi0 anti-K0 PHSP;
  0.0007 MyD*+ anti-D0 pi- anti-K0 PHSP;
  0.0003 MyD*+ D- pi+ K- PHSP;
  0.0007 MyD*+ anti-D0 pi0 K- PHSP;
  0.000150000 MyD_s+ K- PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay anti-Lambda_b0
  0.02200 anti-Lambda_c- MyD_s+ PHSP;
  0.04400 anti-Lambda_c- MyD_s*+ PHSP;
  0.00080 anti-Lambda0 MyD0 PHSP;
Enddecay

Decay MyD*+
  0.6770 MyD0 pi+ VSS;
  0.3070 MyD+ pi0 VSS;
  0.0160 MyD+ gamma VSP_PWAVE;
Enddecay

Decay MyD*0
  0.619000000 MyD0 pi0 VSS; #[Reconstructed PDG2011]
  0.381000000 MyD0 gamma VSP_PWAVE; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s*+
  0.942000000 MyD_s+ gamma VSP_PWAVE; #[Reconstructed PDG2011]
  0.058000000 MyD_s+ pi0 VSS; #[Reconstructed PDG2011]
Enddecay

Decay MyD_0*+
  0.3333 MyD+ pi0 PHSP;
  0.6667 MyD0 pi+ PHSP;
Enddecay

Decay MyD_0*0
  0.3333 MyD0 pi0 PHSP;
  0.6667 MyD+ pi- PHSP;
Enddecay

Decay MyD_1+
  0.3333 MyD*+ pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*0 pi+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD_10
  0.3333 MyD*0 pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*+ pi- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD'_1+
  0.3333 MyD*+ pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 MyD*0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD'_10
  0.6667 MyD*+ pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD_2*+
  0.1030 MyD*+ pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 MyD*0 pi+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD+ pi0 TSS;
  0.4590 MyD0 pi+ TSS;
Enddecay

Decay MyD_2*0
  0.2090 MyD*+ pi- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.1030 MyD*0 pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD0 pi0 TSS;
  0.4590 MyD+ pi- TSS;
Enddecay

Decay MyD_s0*+
  1.000 MyD_s+ pi0 PHSP;
Enddecay

Decay D'_s1+
  0.5000 MyD*+ K0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.5000 MyD*0 K+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.0000 gamma MyD_s*+ PHSP;
  0.0000 gamma MyD_s+ PHSP;
Enddecay

Decay MyD_s1+
  0.80000 MyD_s*+ pi0 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.20000 MyD_s+ gamma VSP_PWAVE;
Enddecay

Decay MyD_s2*+
  0.0500 MyD*+ K0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.0500 MyD*0 K+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.4300 MyD+ K0 TSS;
  0.4700 MyD0 K+ TSS;
Enddecay

Decay MyD(2S)0
  0.6667 MyD*+ pi- SVS;
  0.3333 MyD*0 pi0 SVS;
Enddecay

Decay MyD(2S)+
  0.3333 MyD*+ pi0 SVS;
  0.6667 MyD*0 pi+ SVS;
Enddecay

Decay MyD*(2S)0
  0.3333 MyD*+ pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD0 pi0 VSS;
  0.3333 MyD+ pi- VSS;
Enddecay

Decay MyD*(2S)+
  0.1667 MyD*+ pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*0 pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD+ pi0 VSS;
  0.3333 MyD0 pi+ VSS;
Enddecay

Decay MySigma_c0
  1.0000 MyLambda_c+ pi- PHSP;
Enddecay

Decay MySigma_c+
  1.0000 MyLambda_c+ pi0 PHSP;
Enddecay

Decay MySigma_c++
  1.0000 MyLambda_c+ pi+ PHSP;
Enddecay

Decay MyXi'_c+
  0.8200 gamma MyXi_c+ PHSP;
Enddecay

Decay MyXi'_c0
  1.0000 gamma MyXi_c0 PHSP;
Enddecay

Decay MySigma_c*0
  1.0000 MyLambda_c+ pi- PHSP;
Enddecay

Decay MySigma_c*+
  1.0000 MyLambda_c+ pi0 PHSP;
Enddecay

Decay MySigma_c*++
  1.0000 MyLambda_c+ pi+ PHSP;
Enddecay

Decay MyLambda_c(2593)+
  0.096585366 MySigma_c++ pi- PHSP; #[Reconstructed PDG2011]
  0.096585366 MySigma_c0 pi+ PHSP; #[Reconstructed PDG2011]
  0.190000000 MyLambda_c+ pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.096585366 MySigma_c+ pi0 PHSP; #[Reconstructed PDG2011]
  0.036219512 MyLambda_c+ pi0 pi0 PHSP; #[Reconstructed PDG2011]
  0.004024390 MyLambda_c+ gamma PHSP; #[Reconstructed PDG2011]
  0.240000000 MySigma_c*++ pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.240000000 MySigma_c*0 pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay MyLambda_c(2625)+
  0.670000000 MyLambda_c+ pi+ pi- PHSP; #[Reconstructed PDG2011]
  0.320294118 MyLambda_c+ pi0 PHSP; #[Reconstructed PDG2011]
  0.009705882 MyLambda_c+ gamma PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD+
  0.055000000 anti-K*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.094000000 anti-K0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002773020 anti-K_10 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002927076 anti-K_2*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.003312218 pi0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002002736 eta mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000385142 eta' mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002500000 rho0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002156793 omega mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.039000000 K- pi+ mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.001078397 anti-K0 pi0 mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000382000 mu+ nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
Enddecay

Decay MyD0
  0.019800000 K*- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.033100000 K- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000815539 K_1- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001374504 K_2*- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002370000 pi- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002015940 rho- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001007970 anti-K0 pi- mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000549802 K- pi0 mu+ nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000000000 mu+ mu- PHSP; #[Reconstructed PDG2011]
  #
  0.021700000 K*- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.035500000 K- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000760000 K_1- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001374504 K_2*- e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.038900000 K- pi+ PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s+
  0.018309605 phi mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.022845082 eta mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.008186726 eta' mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 anti-K0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 anti-K*0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.005800000 mu+ nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
  #
  0.024900000 phi e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.026700000 eta e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.009900000 eta' e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 anti-K0 e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 anti-K*0 e+ nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002300000 omega pi+ SVS; #[Reconstructed PDG2011]
  0.000200000 rho0 pi+ SVS; #[Reconstructed PDG2011]
  0.000304906 rho+ pi0 SVS; #[Reconstructed PDG2011]
Enddecay

Decay MyLambda_c+
  0.020000000 mu+ nu_mu Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 mu+ nu_mu Sigma0 PYTHIA 42;
  0.00500 mu+ nu_mu Sigma*0 PYTHIA 42;
  0.00300 mu+ nu_mu n0 PYTHIA 42;
  0.00200 mu+ nu_mu Delta0 PYTHIA 42;
  0.00600 mu+ nu_mu p+ pi- PYTHIA 42;
  0.00600 mu+ nu_mu n0 pi0 PYTHIA 42;
  #
  0.021000000 e+ nu_e Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 e+ nu_e Sigma0 PYTHIA 42;
  0.00500 e+ nu_e Sigma*0 PYTHIA 42;
  0.00300 e+ nu_e n0 PYTHIA 42;
  0.00200 e+ nu_e Delta0 PYTHIA 42;
  0.00600 e+ nu_e p+ pi- PYTHIA 42;
  0.00600 e+ nu_e n0 pi0 PYTHIA 42;
  0.008600000 Delta++ K- PYTHIA 0; #[Reconstructed PDG2011]
  0.02500 Delta++ K*- PYTHIA 0;
  0.023000000 p+ anti-K0 PYTHIA 0; #[Reconstructed PDG2011]
  0.016000000 p+ anti-K*0 PYTHIA 0; #[Reconstructed PDG2011]
  0.00500 Delta+ anti-K0 PYTHIA 0;
  0.010700000 Lambda0 pi+ PYTHIA 0; #[Reconstructed PDG2011]
Enddecay

End
""")

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py") 
include("MC12JobOptions/MultiMuonFilter.py")
include("MC12JobOptions/DiLeptonMassFilter.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
#topAlg.Pythia8B.Commands += ['HardQCD:gg2bbbar = on']
#topAlg.Pythia8B.Commands += ['HardQCD:qqbar2bbbar = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 5.0
topAlg.Pythia8B.AntiQuarkPtCut = 5.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
topAlg.Pythia8B.NHadronizationLoops = 10
topAlg.Pythia8B.NDecayLoops = 1

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

topAlg.EvtInclusiveDecay.userDecayFile = "BbAbbar.DEC"
topAlg.EvtInclusiveDecay.allowAllKnownDecays = False

topAlg.EvtInclusiveDecay.maxNRepeatedDecays = 150

topAlg.EvtInclusiveDecay.applyUserSelection = True
topAlg.EvtInclusiveDecay.userSelRequireOppositeSignedMu = True
topAlg.EvtInclusiveDecay.userSelMu1MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu2MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu1MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMu2MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMinDimuMass = 4000.
topAlg.EvtInclusiveDecay.userSelMaxDimuMass = 10000.

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2
topAlg.DiLeptonMassFilter.MinPt = 3500.
topAlg.DiLeptonMassFilter.MaxEta = 3.0
topAlg.DiLeptonMassFilter.MinMass = 4000.
topAlg.DiLeptonMassFilter.MaxMass = 10000.
topAlg.DiLeptonMassFilter.AllowSameCharge = False

