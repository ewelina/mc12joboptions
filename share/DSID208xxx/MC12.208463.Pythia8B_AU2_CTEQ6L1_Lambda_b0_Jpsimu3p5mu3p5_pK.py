########################################################################
# Job options for Pythia8B_i generation of Lambda_b0 -> J/psi(mumu) p K-
########################################################################
evgenConfig.description = "Lambda_b0 -> J/psi p K"
evgenConfig.keywords = ["BtoJpsi","dimuons","pentaquarks"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkEtaCut = 1000.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# J/psi
topAlg.Pythia8B.Commands += ['443:m0 = 3.096916']      # PDG 2015
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2015
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Lambda_b0
topAlg.Pythia8B.Commands += ['5122:m0 = 5.61951']      # PDG 2015
topAlg.Pythia8B.Commands += ['5122:tau0 = 0.4395']     # PDG 2015

topAlg.Pythia8B.Commands += ['5122:onMode = 3']
topAlg.Pythia8B.Commands += ['5122:addChannel = 2 1.0 0 443 2212 -321']


topAlg.Pythia8B.OutputLevel = INFO
topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.SignalPDGCodes = [5122,443,13,-13,2212,-321]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

include("MC12JobOptions/ParentsTracksFilter.py")
topAlg.ParentsTracksFilter.PDGParent = [5122]
topAlg.ParentsTracksFilter.PtMinParent =  11000.
topAlg.ParentsTracksFilter.EtaRangeParent = 2.3
topAlg.ParentsTracksFilter.PtMinLeptons = 3500.
topAlg.ParentsTracksFilter.EtaRangeLeptons = 2.5
topAlg.ParentsTracksFilter.PtMinHadrons = 950.
topAlg.ParentsTracksFilter.EtaRangeHadrons = 2.7
topAlg.ParentsTracksFilter.NumMinTracks = 4
topAlg.ParentsTracksFilter.NumMaxTracks = 4
topAlg.ParentsTracksFilter.NumMinLeptons = 2
topAlg.ParentsTracksFilter.NumMaxLeptons = 2
topAlg.ParentsTracksFilter.NumMinOthers = 0
topAlg.ParentsTracksFilter.NumMaxOthers = 0
