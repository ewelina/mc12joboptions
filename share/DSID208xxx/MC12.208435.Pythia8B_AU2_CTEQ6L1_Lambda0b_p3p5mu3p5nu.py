###############################################################
# Python snippet to generate EvtGen user decay file on the fly
# Lambda_0b->p3p5mu3p5nu
###############################################################
f = open("LAMBDAB0_PMUNU.DEC","w")
f.write("Decay anti-Lambda_b0\n")
f.write("1.0000  p+    mu-  anti-nu_mu       PHSP;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
###############################################################

evgenConfig.description = "Exclusive Lambda_b0->pmunu production"
evgenConfig.keywords = ["exclusiveLambda","Lambda_b0","semileptonic"]
evgenConfig.minevents = 50

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py")
include("MC12JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 7.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.Pythia8B.SignalPDGCodes = [-5122]
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.EvtInclusiveDecay.userDecayFile = "LAMBDAB0_PMUNU.DEC"
topAlg.BSignalFilter.LVL1MuonCutOn = True
topAlg.BSignalFilter.LVL1MuonCutPT = 3500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutOn = False

topAlg.BSignalFilter.B_PDGCode = -5122
topAlg.BSignalFilter.Cuts_Final_mu_switch = False
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT = 3500
topAlg.BSignalFilter.Cuts_Final_hadrons_eta = 2.6

topAlg.BSignalFilter.InvMass_switch = True;
topAlg.BSignalFilter.InvMass_PartId1 = 13
topAlg.BSignalFilter.InvMass_PartId2 = 2212
topAlg.BSignalFilter.InvMassMin = 3000
topAlg.BSignalFilter.InvMassMax = 7000
