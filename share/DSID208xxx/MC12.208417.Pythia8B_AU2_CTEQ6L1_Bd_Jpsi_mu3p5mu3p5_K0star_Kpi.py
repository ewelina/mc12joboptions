##########################################################################
# Job options for Pythia8B_i generation of Bd->J/psi(mu3p5m3p5)K*0(K+Pi-)
##########################################################################
evgenConfig.description = "Signal Bd->J/psi(mu3p5mu3p5)Kstar(K+Pi-)"
evgenConfig.keywords = ["exclusiveB","Bd","dimuons"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['511:onIfMatch = 443 313']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:onIfMatch = -13 13']
topAlg.Pythia8B.SignalPDGCodes = [511,443,-13,13,313,321,-211]
# Select the required method in the userselections file
topAlg.Pythia8B.UserSelection = 'BDJPSIKSTAR_TRANS'
# Pass the requested physical parameters to the algorithm
topAlg.Pythia8B.UserSelectionVariables = [ 1.0, 1.2, 1.0, 0.519, 0.252, 0.0, 0.659, 0.0 ,0.507, 0.0, 3.02, -2.87, 0.86]
# Meaning of each double in the array
#userVars[0] 0(flat)/1(angle)
#[1] prob_limit
#[2] tag mode
#[3] A0
#[4] Al
#[5] As
#[6] GammaS
#[7] DeltaGamma
#[8] DeltaM
#[9] phiS
#[10] delta_p
#[11] delta_l
#[12]delta_s

topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]

