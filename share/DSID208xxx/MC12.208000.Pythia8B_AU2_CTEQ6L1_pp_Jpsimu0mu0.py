##############################################################
# Job options fragment for pp->J/psi(mu0mu0)X  
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive pp->J/psi(mu0mu0) production"
evgenConfig.keywords = ["charmonium","dimuons","inclusive"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [0.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]
