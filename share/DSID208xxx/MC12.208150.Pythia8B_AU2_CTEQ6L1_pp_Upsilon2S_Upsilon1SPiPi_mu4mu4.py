#######################################################################
# Job options fragment for pp->X Upsilon(2S)(->Upsilon1S(mu4mu4),pi,pi)
#######################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Up(2S)(->pi,pi,Upsi(mu4mu4))"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")
topAlg.Pythia8B.Commands += ['9900553:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9900553:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9900553:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9900553:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9900551:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9900551:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9900551:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9900551:0:products = 100553 21']

topAlg.Pythia8B.Commands += ['9910551:m0 = 10.5']
topAlg.Pythia8B.Commands += ['9910551:mMin = 10.5']
topAlg.Pythia8B.Commands += ['9910551:mMax = 10.5']
topAlg.Pythia8B.Commands += ['9910551:0:products = 100553 21']


topAlg.Pythia8B.Commands += ['100553:onMode = off']
topAlg.Pythia8B.Commands += ['100553:7:onMode = on']
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100553,553,-13,13,-211,211]
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.38, 0.38]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]
