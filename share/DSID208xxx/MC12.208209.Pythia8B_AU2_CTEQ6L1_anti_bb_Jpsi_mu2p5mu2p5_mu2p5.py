##############################################################
# bb->J/psi(mu4mu4)X  
##############################################################
evgenConfig.description = "Inclusive bb->J/psi(mu2.5 mu2.5) mu2.5 production, J/psi from b-quark"
evgenConfig.keywords = ["exclusiveAntiBtoJpsi","dimuons"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_inclusiveAntiBJpsi_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 4.0
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 2.7
topAlg.Pythia8B.AntiQuarkEtaCut = 102.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

# J/psi coming from b-quark (default: from anti-b)
topAlg.Pythia8B.UserSelectionVariables = [ -1 ]

# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 5

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [3]

