##############################################################
# J/psi(mu2.5mu2.5)X + J/psi(mu2.5mu2.5)X
# S. Leontsinis - M. Watson - A. Chisholm
# For the study of associated production J/\psi+J/\psi (both prompt)
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->Jpsi(mumu) + Jpsi(mumu) via second hard process"
evgenConfig.keywords = ["charmonium","dimuons","Jpsi","Jpsi"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

topAlg.Pythia8B.Commands += ['Charmonium:O(3PJ)[3P0(1)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Charmonium:O(3PJ)[3S1(8)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Charmonium:O(3DJ)[3D1(1)] = [0,0,0]']
topAlg.Pythia8B.Commands += ['Charmonium:O(3DJ)[3P0(8)] = [0,0,0]']
#topAlg.Pythia8B.Commands += ['Charmonium:gg2ccbar(3PJ)[3PJ(1)]g =  [false,false,false]']

topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:Charmonium = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]
topAlg.Pythia8B.SignalPtCuts = [2.5,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [2.5,2.5,2.5]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [4]
