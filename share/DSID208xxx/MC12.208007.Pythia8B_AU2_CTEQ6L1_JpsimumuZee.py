include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->Jpsi(mumu) + Z->ee via second hard process"
evgenConfig.keywords = ["charmonium","dimuons","Jpsi","Z","dielectrons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.Commands += ['23:onMode = off']
topAlg.Pythia8B.Commands += ['23:5:onMode = on']
topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:SingleGmZ = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = [20.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
