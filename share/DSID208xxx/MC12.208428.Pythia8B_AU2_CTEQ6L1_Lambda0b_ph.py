######################################################
# Job options for Pythia8B_i generation of Lambda_b0 -> p3p5h3p5
######################################################

evgenConfig.description = "Exclusive Lambda_b0->ph production"
evgenConfig.keywords = ["ExclusiveB","Lambda_b0","peaking"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
include("MC12JobOptions/BSignalFilter.py")

# the file MC12JobOptions/Pythia8B_exclusiveB_Common.py includes also the options commented below:
#topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
#topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
#topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#topAlg.Pythia8B.SelectBQuarks = True
#topAlg.Pythia8B.SelectCQuarks = False
#include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 7.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

# anti-Lb -> anti-p pi+
topAlg.Pythia8B.Commands += ['5122:addChannel = 3 0.0175 0 -2212 211']
# anti-Lb -> anti-p K+
topAlg.Pythia8B.Commands += ['5122:addChannel = 3 0.0275 0 -2212 321']
# anti-Lb -> p pi-
topAlg.Pythia8B.Commands += ['5122:addChannel = 3 0.0175 0 2212 -211']
# anti-Lb -> p K-
topAlg.Pythia8B.Commands += ['5122:addChannel = 3 0.0275 0 2212 -321']

topAlg.Pythia8B.SignalPDGCodes = [-5122] 
topAlg.Pythia8B.NHadronizationLoops = 1

#topAlg.Pythia8B.TriggerPDGCode = 13
#topAlg.Pythia8B.TriggerStatePtCut = [3.5]
#topAlg.Pythia8B.TriggerStateEtaCut = 2.6
#topAlg.Pythia8B.MinimumCountPerCut = [2]

# Filtering Lb hadron daughters
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT = 3500. 
topAlg.BSignalFilter.Cuts_Final_hadrons_eta = 2.6
topAlg.BSignalFilter.B_PDGCode = -5122
