##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B0 -> K*0 (K+pi-) mu3.5mu3.5 (flat angles)
##############################################################
f = open("B0_KSTAR_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias my_K*0   K*0\n")
f.write("Decay B0\n")
f.write("1.0000  my_K*0     mu+   mu-       PHSP;\n")
f.write("Enddecay\n")
f.write("Decay my_K*0\n")
f.write("1.0000    K+    pi-           VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B0->K*0_Kpi_mu3p5mu3p5 production"
evgenConfig.keywords    = ["exclusiveBtoK*0","B0","K*0","dimuons"]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py")
include("MC12JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 7.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True

topAlg.Pythia8B.NHadronizationLoops = 2

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.Pythia8B.SignalPDGCodes = [511]
topAlg.EvtInclusiveDecay.userDecayFile = "B0_KSTAR_MUMU_USER.DEC"

topAlg.BSignalFilter.LVL1MuonCutOn  = True
topAlg.BSignalFilter.LVL2MuonCutOn  = True
topAlg.BSignalFilter.LVL1MuonCutPT  = 3500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT  = 3500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

topAlg.BSignalFilter.B_PDGCode = 511
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
topAlg.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
