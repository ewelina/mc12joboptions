##############################################################
# J/psi(mu2.5mu2.5)X + W->enu  
# M. Watson - S. Leontsinis
# For the study of associated production W+J/\psi
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->Jpsi(mumu) + W->enu via second hard process"
evgenConfig.keywords = ["charmonium","dimuons","Jpsi","W"]
evgenConfig.minevents = 2000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.Commands += ['24:onMode = off']
topAlg.Pythia8B.Commands += ['24:6:onMode = on']
topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:SingleW = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]
topAlg.Pythia8B.SignalPtCuts = [0.0,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [102.5,2.5,2.5]

topAlg.Pythia8B.TriggerPDGCode = 11
topAlg.Pythia8B.TriggerStatePtCut = [20.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
