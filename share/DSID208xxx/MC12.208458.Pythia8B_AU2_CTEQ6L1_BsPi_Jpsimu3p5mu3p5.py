##############################################################
# BsToJpsimu3p5mu3p5phi.py
# Job options for Pythia8B_i generation of BsPi+->Bs(->J/psi(mumu)phi(KK)) Pi  
# Must be run via Generate_trf.py
##############################################################
include("MC12JobOptions/Pythia82B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Signal BsPi+->Bs(->J/psi(mumu)phi(KK)) Pi"
evgenConfig.keywords = ["BPHYS"]
evgenConfig.minevents = 500

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Uncomment for MSEL1
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11.'] # Equivalent of CKIN3    
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 8.5
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

# Job options for closing B decays 
include ("Pythia8B_i/CloseBDecays.py")

# Signal decay (Bs->J/psi phi)
topAlg.Pythia8B.Commands += ['531:87:onMode = on']
# Close all J/psi decays apart from J/psi->mumu
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.Commands += ['1000000:new = Bsplus Bsminus 0 3 0  5.5686 0.0206 5.4656 5.6716 0']
topAlg.Pythia8B.Commands += ['1000000:addchannel = 1 1 0 531 211'] 
topAlg.Pythia8B.Commands += ["533:m0 = 5.5686"]
topAlg.Pythia8B.Commands += ["533:mWidth = 0.0206"]
topAlg.Pythia8B.Commands += ["533:mMin = 5.4656"]
topAlg.Pythia8B.Commands += ["533:mMax = 5.6716"]
topAlg.Pythia8B.Commands += ["533:tau0 = 0.0"]

# Signal topology - only events containing this sequence will be accepted 
topAlg.Pythia8B.SignalPDGCodes = [ 1000000,  531,  443,  -13,  13 ,   333,   321 ,   -321,  211]
topAlg.Pythia8B.SignalPtCuts =   [0.0     ,  0.0, 0.0,   0.0,  0.0,   0.0,  0.500,   0.500, 0.400]
topAlg.Pythia8B.SignalEtaCuts =  [102.5,   102.5, 102.5, 2.5, 2.5, 102.5,    2.5,    2.5 , 2.5]


topAlg.Pythia8B.ReplaceUndecayed = [ 533, 1000000 ]
topAlg.Pythia8B.OutputLevel= INFO
topAlg.Pythia8B.UserSelectionVariables = [ ]

# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 10

# List of B-species - for counting purposes (no effect on generation)
include("Pythia8B_i/BPDGCodes.py")

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]
