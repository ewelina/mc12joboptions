#######################################################################
# Job options fragment for pp->X X(3872)(->pi,pi,Jpsi(mu4mu4))
#######################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Psi(2S)(->pi,pi,Jpsi(mu4mu4))"
evgenConfig.keywords = ["charmonium","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_Charmonium_Common.py")

#c-cbar colour octet states P0, S0, S1 set with mass 4.4 GeV decaying to Psi(2S)g

topAlg.Pythia8B.Commands += ['9900443:m0 = 4.4']
topAlg.Pythia8B.Commands += ['9900443:mMin = 4.4']
topAlg.Pythia8B.Commands += ['9900443:mMax = 4.4']
topAlg.Pythia8B.Commands += ['9900443:0:products = 100443 21']

topAlg.Pythia8B.Commands += ['9900441:m0 = 4.4']
topAlg.Pythia8B.Commands += ['9900441:mMin = 4.4']
topAlg.Pythia8B.Commands += ['9900441:mMax = 4.4']
topAlg.Pythia8B.Commands += ['9900441:0:products = 100443 21']

topAlg.Pythia8B.Commands += ['9910441:m0 = 4.4']
topAlg.Pythia8B.Commands += ['9910441:mMin = 4.4']
topAlg.Pythia8B.Commands += ['9910441:mMax = 4.4']
topAlg.Pythia8B.Commands += ['9910441:0:products = 100443 21']


#Turn all decays off, then allow decay number 7, which is ->
topAlg.Pythia8B.Commands += ['100443:0:products = 443 211 -211']
topAlg.Pythia8B.Commands += ['100443:onMode = off']
topAlg.Pythia8B.Commands += ['100443:0:onMode = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100443,443,-13,13,-211,211] #mu+ mu- pi- pi+
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 3.5, 3.5, 0.38, 0.38]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.5, 2.5, 2.5, 2.5]
