#############################################################
# Job options fragment for Bd->K*0mu11mu11 for Run 2 studies.
#############################################################

evgenConfig.description = "Exclusive Bd->K*0mu11mu11 production"
evgenConfig.keywords    = [ "exclusiveB", "Bd", "dimuons" ]
evgenConfig.minevents   = 100

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 30.' ]
#topAlg.Pythia8B.Commands += [ '511:onMode = off' ]
topAlg.Pythia8B.Commands += [ '511:onIfMatch = 313 -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 511, -13, 13, 313, 321, -211 ]
topAlg.Pythia8B.SignalPtCuts   = [  0., 11.,11.,  0., 0.5,  0.5 ]
topAlg.Pythia8B.SignalPtCuts   = [ 2.5, 2.5,2.5, 2.5, 2.5,  2.5 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerStatePtCut  = [ 11.0 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]

topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 28.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 2.7
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.NHadronizationLoops       = 1
