#############################################################################
# Job options fragment for pp->X Xb(10900)(->Upsilon1S(mu4mu4),omega(pi+pi-))
#############################################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->X Xb(10900)(->omega(pi,pi),Upsi(mu4mu4))"
evgenConfig.keywords = ["bottomonium","dimuons"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Bottomonium_Common.py")

#need to add a chi_b1(2P)
topAlg.Pythia8B.Commands += ['120553:all = chi_1b2P chi_1b2P 3 0 0 10.25546 0.00002 10.25516 10.25576 0'] 

#change mass to mass of "Xb"
mass = 10.900
topAlg.Pythia8B.Commands += ['120553:m0 = '+str(mass)] 
topAlg.Pythia8B.Commands += ['120553:mMin = '+str(mass-0.0003)]
topAlg.Pythia8B.Commands += ['120553:mMax = '+str(mass+0.0003)]
topAlg.Pythia8B.Commands += ['120553:mWidth = 0.00002']

topAlg.Pythia8B.Commands += ['9900553:m0 = 11.1']
topAlg.Pythia8B.Commands += ['9900553:mMin = 11.1']
topAlg.Pythia8B.Commands += ['9900553:mMax = 11.1']
topAlg.Pythia8B.Commands += ['9900553:0:products = 120553 21']

topAlg.Pythia8B.Commands += ['9900551:m0 = 11.1']
topAlg.Pythia8B.Commands += ['9900551:mMin = 11.1']
topAlg.Pythia8B.Commands += ['9900551:mMax = 11.1']
topAlg.Pythia8B.Commands += ['9900551:0:products = 120553 21']

topAlg.Pythia8B.Commands += ['9910551:m0 = 11.1']
topAlg.Pythia8B.Commands += ['9910551:mMin = 11.1']
topAlg.Pythia8B.Commands += ['9910551:mMax = 11.1']
topAlg.Pythia8B.Commands += ['9910551:0:products = 120553 21']

topAlg.Pythia8B.Commands += ['120553:onMode = off']
#add the chi_b1(2P)->omega,Upsilon(1S) decay mode
topAlg.Pythia8B.Commands += ['120553:addChannel = on 0.0163 0 553 223'] 

#ups->mu mu decay
topAlg.Pythia8B.Commands += ['553:onMode = off']
topAlg.Pythia8B.Commands += ['553:3:onMode = on']

#omega->pi pi decay
topAlg.Pythia8B.Commands += ['223:onMode = off']
topAlg.Pythia8B.Commands += ['223:2:onMode = on']


topAlg.Pythia8B.SignalPDGCodes = [120553,553,-13,13,223,-211,211]
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 4.0, 4.0, 0.0, 0.4, 0.4]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.3, 2.3, 100.0, 2.5, 2.5]


