#######################################################################
# Job options for Pythia8B_i generation of Lambda_b->J/psi(mumu)pK
#######################################################################
evgenConfig.description = "Signal Lambda_b->J/psi(mumu)pK"
evgenConfig.keywords = ["LambdaBtoJpsi","dimuons","pentaquarks"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = False
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkEtaCut = 1000.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

#
# Lambda_b:
#
topAlg.Pythia8B.Commands += ['5122:m0 = 5.61951']  # PDG 2014
topAlg.Pythia8B.Commands += ['5122:tau0 = 0.4395'] # PDG 2014
#
# Three Lambda_b decays:
#
topAlg.Pythia8B.Commands += ['5122:onMode = 3']
topAlg.Pythia8B.Commands += ['5122:10:onMode = 2']
topAlg.Pythia8B.Commands += ['5122:10:bRatio = 0.3333334']
topAlg.Pythia8B.Commands += ['5122:10:products = 443 2212 -321']
#
topAlg.Pythia8B.Commands += ['5122:11:onMode = 2']
topAlg.Pythia8B.Commands += ['5122:11:bRatio = 0.3333333']
topAlg.Pythia8B.Commands += ['5122:11:products = -1000011 -321']
#
topAlg.Pythia8B.Commands += ['5122:24:onMode = 2']
topAlg.Pythia8B.Commands += ['5122:24:bRatio = 0.3333333']
topAlg.Pythia8B.Commands += ['5122:24:products = -1000013 -321']

#
# ~e_L- -> wide LHCb PQ:
#
topAlg.Pythia8B.Commands += ['1000011:spinType = 4']
topAlg.Pythia8B.Commands += ['1000011:m0 = 4.380']
topAlg.Pythia8B.Commands += ['1000011:mWidth = 0.205']
topAlg.Pythia8B.Commands += ['1000011:mMin = 4.070']
topAlg.Pythia8B.Commands += ['1000011:mMax = 5.100']
topAlg.Pythia8B.Commands += ['1000011:isResonance = 0']
#
topAlg.Pythia8B.Commands += ['1000011:onMode = off']
topAlg.Pythia8B.Commands += ['1000011:1:onMode = 1']
topAlg.Pythia8B.Commands += ['1000011:1:bRatio = 1.']
topAlg.Pythia8B.Commands += ['1000011:1:products = 443 -2212']

#
# ~mu_L- -> narrow LHCb PQ:
#
topAlg.Pythia8B.Commands += ['1000013:spinType = 6']
topAlg.Pythia8B.Commands += ['1000013:m0 = 4.4498']
topAlg.Pythia8B.Commands += ['1000013:mWidth = 0.039']
topAlg.Pythia8B.Commands += ['1000013:mMin = 4.400']
topAlg.Pythia8B.Commands += ['1000013:mMax = 4.700']
topAlg.Pythia8B.Commands += ['1000013:isResonance = 0']
#
topAlg.Pythia8B.Commands += ['1000013:onMode = off']
topAlg.Pythia8B.Commands += ['1000013:1:onMode = 1']
topAlg.Pythia8B.Commands += ['1000013:1:bRatio = 1.']
topAlg.Pythia8B.Commands += ['1000013:1:products = 443 -2212']

#
# J/psi:
#
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.Commands += ['443:2:bRatio = 1.']

#topAlg.Pythia8B.SignalPDGCodes = [5122]
#topAlg.Pythia8B.OutputLevel = INFO
topAlg.Pythia8B.OutputLevel = DEBUG
topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

include("MC12JobOptions/ParentChildFilter.py")
topAlg.ParentChildFilter.PDGParent  = [5122]
topAlg.ParentChildFilter.PtMinParent =  9000.
topAlg.ParentChildFilter.EtaRangeParent = 2.7
topAlg.ParentChildFilter.PDGChild = [321]
topAlg.ParentChildFilter.PtMinChild = 0.
topAlg.ParentChildFilter.EtaRangeChild = 1000.
