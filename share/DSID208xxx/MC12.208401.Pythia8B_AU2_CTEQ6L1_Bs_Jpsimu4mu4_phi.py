##############################################################
# Job options for Pythia8B_i generation of Bs->J/psi(mu4m4)phi(KK)   
##############################################################
evgenConfig.description = "Signal Bs->J/psi(mu4mu4)phi(KK)"
evgenConfig.keywords = ["exclusiveBtoJpsi","dimuons"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.'] 
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 9.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['531:87:onMode = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [531,443,-13,13,333,321,-321]

topAlg.Pythia8B.NHadronizationLoops = 4 

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [4.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]

