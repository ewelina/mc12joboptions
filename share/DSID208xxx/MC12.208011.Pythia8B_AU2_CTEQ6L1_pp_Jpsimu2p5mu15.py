##############################################################
# Job options fragment for pp->J/psi(mu2.5mu15)X  
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive pp->J/psi(mu2.5mu15) production"
evgenConfig.keywords = ["charmonium","dimuons","inclusive"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5,15.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [2,1]
