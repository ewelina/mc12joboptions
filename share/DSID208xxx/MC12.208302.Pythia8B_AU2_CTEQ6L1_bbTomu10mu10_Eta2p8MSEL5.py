##############################################################
# Job options fragment for bb->mu10mu10X
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_Photos.py")
evgenConfig.description = "Inclusive bb->mu10mu10X production"
evgenConfig.keywords = ["bottom","dimuons","inclusive"]
evgenConfig.minevents = 500

topAlg.Pythia8B.Commands += ['HardQCD:gg2bbbar = on'] 
topAlg.Pythia8B.Commands += ['HardQCD:qqbar2bbbar = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 18.']   
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 10.0
topAlg.Pythia8B.AntiQuarkPtCut = 10.0
topAlg.Pythia8B.QuarkEtaCut = 4.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = False

topAlg.Pythia8B.NHadronizationLoops = 5

include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [10.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.8
topAlg.Pythia8B.MinimumCountPerCut = [2]
