##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B0 -> psi(2S)(mu3.5mu3.5) K*0(K+pi-) 
##############################################################
f = open("B0_PSI_KSTAR_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias my_K*0   K*0\n")
f.write("Alias my_psi(2S) psi(2S)\n")
f.write("Decay B0\n")
#SVV_HELAMP PKHminus PKphHminus PKHzero PKphHzero PKHplus PKphHplus
f.write("1.0000  my_K*0     my_psi(2S)       SVV_HELAMP 1.0 0.0 1.4142135623 0.0 1.0 0.0; \n")
f.write("Enddecay\n")
f.write("Decay my_K*0\n")
f.write("1.0000    K+    pi-           VSS;\n")
f.write("Enddecay\n")
f.write("Decay my_psi(2S)\n")
f.write("1.0000    mu+   mu-   PHOTOS VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

##############################################################

evgenConfig.description = "Exclusive B0->K*0_Kpi_psi(2S)_mu3p5mu3p5 production"
evgenConfig.keywords    = ["exclusiveBtoK*0","B0","K*0", "psi(2S)", "dimuons"]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py")
include("MC12JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 7.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True

topAlg.Pythia8B.NHadronizationLoops = 2

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.Pythia8B.SignalPDGCodes = [511]
topAlg.EvtInclusiveDecay.userDecayFile = "B0_PSI_KSTAR_USER.DEC"

topAlg.BSignalFilter.LVL1MuonCutOn  = True
topAlg.BSignalFilter.LVL2MuonCutOn  = True
topAlg.BSignalFilter.LVL1MuonCutPT  = 3500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT  = 3500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

topAlg.BSignalFilter.B_PDGCode = 511
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
topAlg.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
