##############################################################
# Job options fragment for gg->X->Jpsill->mumu + ll (l=e/mu)  
# with filter 3 muon with pt > 2GeV within eta 2.8
# Created: 10 Sep. 2016 by Tiesheng.Dai@cern.ch
##############################################################
evgenConfig.description = "PYTHIA8 gg->X->Jpsill->mumu + ll with AU2 CTEQ6L1"
evgenConfig.keywords = ["bphysics","Jpsi","muons","electrons"]
evgenConfig.minevents = 5000
 
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ['Higgs:useBSM = on',
                            'HiggsBSM:gg2H2 = on',
                            'HiggsH2:coup2d = 1.0',
                            'HiggsH2:coup2u = 1.0',
                            'HiggsH2:coup2Z = 0.0',
                            'HiggsH2:coup2W = 0.0',
                            'HiggsA3:coup2H2Z = 0.0',
                            'HiggsH2:coup2A3A3 = 0.0',
                            'HiggsH2:coup2H1H1 = 0.0',
                            '443:onMode = off',
                            '443:onIfMatch 13 13',
                            '35:mMin = 0',
                            '35:mMax = 25',
                            '35:m0   = 18.0',
                            '35:mWidth = 0.00',
                            '35:addChannel 1 0.50 100 11 -11 443',
                            '35:addChannel 1 0.50 100 13 -13 443',
                            '35:onMode = off',
                            '35:onIfMatch 11 -11 443', ## Jpsi ee
                            '35:onIfMatch 13 -13 443'  ## Jpsi mumu
                           ]

#
### Filter
#
include("MC12JobOptions/MultiLeptonFilter.py")
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.NLeptons  = 3
MultiLeptonFilter.Etacut = 2.8
MultiLeptonFilter.Ptcut = 2000.0

