##############################################################
# J/psi(mu2.5mu2.5)X + J/psi(mu2.5mu2.5)X
# S. Leontsinis - M. Watson - A. Chisholm
# For the study of associated production J/\psi+J/\psi (both prompt)
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "pp->bb->Jpsi(mumu) + Jpsi(mumu) via second hard process"
evgenConfig.keywords = ["charmonium","dimuons","Jpsi","Jpsi"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.'] # Equivalent of CKIN3

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 4.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

topAlg.Pythia8B.Commands += ['SecondHard:generate = on']
topAlg.Pythia8B.Commands += ['SecondHard:Charmonium = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

topAlg.Pythia8B.SignalPDGCodes = [443,-13,13]
topAlg.Pythia8B.SignalPtCuts = [2.5,2.5,2.5]
topAlg.Pythia8B.SignalEtaCuts = [2.5,2.5,2.5]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [4]
