############################################################################
# Job options fragment for Bd->J/psi(mu5.5mu5.5)K*0 for trigger LS1 studies.
############################################################################

evgenConfig.description = "Exclusive Bd->J/psi(mu5.5mu5.5)K*0 production"
evgenConfig.keywords    = [ "exclusiveB", "Bd", "dimuons" ]
evgenConfig.minevents   = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
#include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 10.' ]
#topAlg.Pythia8B.Commands += [ '511:onMode = off' ]
topAlg.Pythia8B.Commands += [ '511:onIfMatch = 443 313' ]
topAlg.Pythia8B.Commands += [ '443:onMode = off' ]
topAlg.Pythia8B.Commands += [ '443:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 511, 443, -13,13, 313, 321,-211 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 5.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]

topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 10.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 3.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents         = True
topAlg.Pythia8B.NHadronizationLoops       = 1
