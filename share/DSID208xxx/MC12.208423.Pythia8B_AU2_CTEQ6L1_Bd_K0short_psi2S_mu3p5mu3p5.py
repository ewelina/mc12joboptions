############################################################################
# Job options fragment for Bd->psi(2S)(mu3.5mu3.5)K0short(pi+pi-) 
############################################################################
 
evgenConfig.description = "Exclusive Bd->psi(2S)(mu3.5mu3.5)K0(pi+pi-) production"
evgenConfig.keywords    = [ "exclusiveB", "Bd", "dimuons" ]
evgenConfig.minevents   = 2000
 
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
 
topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 8.' ]
topAlg.Pythia8B.QuarkPtCut                = 0.0
topAlg.Pythia8B.AntiQuarkPtCut            = 8.0
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
# topAlg.Pythia8B.VetoDoubleBEvents         = True

topAlg.Pythia8B.Commands += [ '511:onIfMatch = 100443 310' ]
topAlg.Pythia8B.Commands += [ '100443:onMode = off' ]
topAlg.Pythia8B.Commands += [ '100443:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 511, 100443, -13, 13, 310 ]
 
topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]
 
topAlg.Pythia8B.NHadronizationLoops = 1

