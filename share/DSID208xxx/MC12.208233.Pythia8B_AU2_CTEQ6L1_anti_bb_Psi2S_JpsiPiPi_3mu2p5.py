#######################################################################
# Job options fragment for anti-bb->psi(2S)(->pi,pi,Jpsi(mu2p5mu2p5))
#######################################################################
evgenConfig.description = "anti-bb->Psi(2S)(->pi,pi,Jpsi(mu2p5mu2p5))"
evgenConfig.keywords = ["charmonium","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_inclusiveAntiBPsi2S_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 6.'] # Equivalent of CKIN3

# Quark cuts
topAlg.Pythia8B.QuarkPtCut = 4.0
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 2.7
topAlg.Pythia8B.AntiQuarkEtaCut = 102.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.NHadronizationLoops = 6

#Turn all decays off, then allow decay number 7, which is ->
topAlg.Pythia8B.Commands += ['100443:0:products = 443 211 -211']
topAlg.Pythia8B.Commands += ['100443:onMode = off']
topAlg.Pythia8B.Commands += ['100443:0:onMode = on']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100443,443,-13,13,-211,211] #mu+ mu- pi- pi+
topAlg.Pythia8B.SignalPtCuts = [0.0, 0.0, 2.5, 2.5, 0.38, 0.38]
topAlg.Pythia8B.SignalEtaCuts = [100.0, 100.0, 2.7, 2.7, 2.7, 2.7]

# J/psi coming from b-quark (default: from anti-b)
topAlg.Pythia8B.UserSelectionVariables = [ -1 ]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.MinimumCountPerCut = [3]