########################################################################
# Job options for Pythia8B_i generation of Xi_b- -> J/psi(mumu) Xi- and Xi_b- -> J/psi(mumu) K- Lambda0
########################################################################
evgenConfig.description = "Xi_b- -> J/psi Xi-; Xi_b- -> J/psi K- Lambda0"
evgenConfig.keywords = ["BtoJpsi","dimuons"]
evgenConfig.minevents = 100

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkEtaCut = 1000.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

# J/psi
topAlg.Pythia8B.Commands += ['443:m0 = 3.096916']      # PDG 2015
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2015
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

# Xi_b-
topAlg.Pythia8B.Commands += ['5132:m0 = 5.7945']       # PDG 2016
topAlg.Pythia8B.Commands += ['5132:tau0 = 0.4677']     # PDG 2016
topAlg.Pythia8B.Commands += ['5132:onMode = 3']
topAlg.Pythia8B.Commands += ['5132:addChannel = 2 0.5 0 443 3312']
topAlg.Pythia8B.Commands += ['5132:addChannel = 2 0.5 0 443 -321 3122']

topAlg.Pythia8B.OutputLevel = INFO
topAlg.Pythia8B.NHadronizationLoops = 10

topAlg.Pythia8B.SignalPDGCodes = [5132]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]
