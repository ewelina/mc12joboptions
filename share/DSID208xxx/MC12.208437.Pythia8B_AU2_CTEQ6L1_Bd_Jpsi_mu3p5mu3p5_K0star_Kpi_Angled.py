##########################################################################
# Job options for Pythia8B_i generation of Bd->J/psi(mu3p5m3p5)K*0(K+Pi-).
##########################################################################

evgenConfig.description = "Signal Bd->J/psi(mu3p5mu3p5)Kstar(K+Pi-)"
evgenConfig.keywords    = [ "exclusiveB", "Bd", "dimuons" ]
evgenConfig.minevents   = 500

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 8.' ]
topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 8.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += [ '511:onIfMatch = 443 313' ]
topAlg.Pythia8B.Commands += [ '443:onMode = off' ]
topAlg.Pythia8B.Commands += [ '443:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [   511,   443,   -13,    13,   313, 321, -211 ]
topAlg.Pythia8B.SignalPtCuts   = [   0.0,   0.0,   0.0,   0.0,   0.0, 0.5,  0.5 ] # no muon pT cut here - see trigger cuts below
topAlg.Pythia8B.SignalEtaCuts  = [ 102.5, 102.5, 102.5, 102.5, 102.5, 2.6,  2.6 ] # no muon eta cut here - see trigger cuts below
# Select the required method in the userselections file.
topAlg.Pythia8B.UserSelection = 'BDJPSIKSTAR_TRANS'
# Pass the requested physical parameters to the algorithm.
topAlg.Pythia8B.UserSelectionVariables = [ 1.0, 1.2, 1.0, 0.570, 0.211, 0.0, 0.658, 0.0 ,0.510, 0.0, 3.01, -2.86, 0.0 ]
# Meaning of each double in the array - userVars[x]:
# [0] 0(flat)/1(angle)
# [1] prob_limit
# [2] tag mode
# [3] A0^2        0.570 +/- 0.008  PDG2013
# [4] Al^2        0.211            PDG2013
# [5] As^2        0.0
# [6] GammaS      0.658 +/- 0.003  PDG2013 /added/
# [7] DeltaGamma  0.0
# [8] DeltaM      0.510 +/- 0.004  PDG2013
# [9] phiS        0.0
#[10] delta_p     3.01 +/- 0.14    PDG2013
#[11] delta_l    -2.86 +/- 0.11    PDG2013
#[12] delta_s     0.0
# Latest LHCb paper on Bd is Phys. Rev. D 88, 052002 (2013)

topAlg.Pythia8B.NHadronizationLoops = 4

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStatePtCut  = [ 3.5 ]
topAlg.Pythia8B.TriggerStateEtaCut = 2.6
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]
