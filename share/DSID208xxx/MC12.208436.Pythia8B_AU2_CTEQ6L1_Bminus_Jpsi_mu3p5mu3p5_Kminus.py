##############################################################
# Python snippet to generate EvtGen user decay file on the fly
# B- -> J/psi(mu3.5mu3.5) K- 
##############################################################
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B-\n")
f.write("1.0000  myJ/psi   K-             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

evgenConfig.description = "Exclusive Bminus->Jpsi_mu3p5mu3p5_Kminus production"
evgenConfig.keywords = ["exclusiveBtoJpsi","Bminus","Jpsi","dimuons"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py") 
include("MC12JobOptions/BSignalFilter.py")

#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#to synchronise this with the exclusive pythia decays (file above)
#we explicitly set the following options
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut = 7.0
topAlg.Pythia8B.AntiQuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 2.6
topAlg.Pythia8B.AntiQuarkEtaCut = 102.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

topAlg.Pythia8B.NHadronizationLoops = 1

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.Pythia8B.SignalPDGCodes = [-521]
topAlg.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"
topAlg.BSignalFilter.LVL1MuonCutOn = True
topAlg.BSignalFilter.LVL2MuonCutOn = True
topAlg.BSignalFilter.LVL1MuonCutPT = 3500 
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT = 3500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

topAlg.BSignalFilter.B_PDGCode = -521
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT = 500.0
topAlg.BSignalFilter.Cuts_Final_hadrons_eta = 2.6
