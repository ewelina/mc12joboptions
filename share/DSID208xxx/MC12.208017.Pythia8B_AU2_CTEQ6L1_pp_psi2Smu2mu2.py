##############################################################
# Job options fragment for pp->J/psi(mu4mu4)X  
##############################################################
include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
evgenConfig.description = "Inclusive pp->psi(2S)(mu2mu2) production"
evgenConfig.keywords = ["charmonium","dimuons","inclusive"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8B_Charmonium_Common.py") 
include("MC12JobOptions/Pythia8B_Photos.py")

topAlg.Pythia8B.Commands += ['9910441:m0 = 4.0']
topAlg.Pythia8B.Commands += ['9910441:mMin = 4.0']
topAlg.Pythia8B.Commands += ['9910441:mMax = 4.0']
topAlg.Pythia8B.Commands += ['9910441:0:products = 100443 21']

topAlg.Pythia8B.Commands += ['9900443:m0 = 4.0']
topAlg.Pythia8B.Commands += ['9900443:mMin = 4.0']
topAlg.Pythia8B.Commands += ['9900443:mMax = 4.0']
topAlg.Pythia8B.Commands += ['9900443:0:products = 100443 21']

topAlg.Pythia8B.Commands += ['9900441:m0 = 4.0']
topAlg.Pythia8B.Commands += ['9900441:mMin = 4.0']
topAlg.Pythia8B.Commands += ['9900441:mMax = 4.0']
topAlg.Pythia8B.Commands += ['9900441:0:products = 100443 21']

topAlg.Pythia8B.Commands += ['100443:onMode = off']
topAlg.Pythia8B.Commands += ['100443:1:onMode = on']
topAlg.Pythia8B.SignalPDGCodes = [100443,-13,13]

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [2.0]
topAlg.Pythia8B.TriggerStateEtaCut = 3.1
topAlg.Pythia8B.MinimumCountPerCut = [2]
