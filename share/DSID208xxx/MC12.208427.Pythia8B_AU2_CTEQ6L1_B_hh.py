######################################################
# Job options for Pythia8B_i generation of Bds->h3p5h3p5   
######################################################

evgenConfig.description = "Exclusive Bds->hh production"
evgenConfig.keywords = ["ExclusiveB","Bs","Bd","peaking"]
evgenConfig.minevents = 2000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

#include("MC12JobOptions/BSignalFilter.py")
# Instead, manually create two separate instances of
# BSignalFilter, for B0 and B0s decays

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
BSignalFilter511 = BSignalFilter("BSignalFilter511")
topAlg += BSignalFilter511
StreamEVGEN.AcceptAlgs += ["BSignalFilter511"]

from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
BSignalFilter531 = BSignalFilter("BSignalFilter531")
topAlg += BSignalFilter531
StreamEVGEN.AcceptAlgs += ["BSignalFilter531"]

# the file MC12JobOptions/Pythia8B_exclusiveB_Common.py includes also the options commented below:
#topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
#topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
#topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#topAlg.Pythia8B.SelectBQuarks = True
#topAlg.Pythia8B.SelectCQuarks = False
#include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 7.']

topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 7.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.VetoDoubleBEvents = True

# pi+ pi-
topAlg.Pythia8B.Commands += ['531:addChannel = 2 0.00073 0 -211 211']
# K+ K-
topAlg.Pythia8B.Commands += ['531:addChannel = 2 0.0245 0 -321 321']
# pi+ K-
topAlg.Pythia8B.Commands += ['531:addChannel = 2 0.0027 0 -321 211']
# K+ pi-
topAlg.Pythia8B.Commands += ['531:addChannel = 2 0.0027 0 -211 321']

# pi+ pi-
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.00510 0 -211 211']
# K+ K-
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.00012 0 -321 321']
# pi+ K-
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.0098 0 -321 211']
# K+ pi-
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.0098 0 -211 321']

# Just require a b-quark - it automatically makes Pythia8B to reject events with
# undecayed 521, 541 etc, while signal channel filtering is not really needed
# since only signal decays are switched on.

topAlg.Pythia8B.SignalPDGCodes = [5] 
topAlg.Pythia8B.NHadronizationLoops = 1

#topAlg.Pythia8B.TriggerPDGCode = 13
#topAlg.Pythia8B.TriggerStatePtCut = [3.5]
#topAlg.Pythia8B.TriggerStateEtaCut = 2.7
#topAlg.Pythia8B.MinimumCountPerCut = [2]

# Filtering B0 hadron daughters
topAlg.BSignalFilter511.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter511.Cuts_Final_hadrons_pT = 3500. 
topAlg.BSignalFilter511.Cuts_Final_hadrons_eta = 2.6
topAlg.BSignalFilter511.B_PDGCode = 511

# Filtering B0s hadron daughters
topAlg.BSignalFilter531.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter531.Cuts_Final_hadrons_pT = 3500.
topAlg.BSignalFilter531.Cuts_Final_hadrons_eta = 2.6
topAlg.BSignalFilter531.B_PDGCode = 531
