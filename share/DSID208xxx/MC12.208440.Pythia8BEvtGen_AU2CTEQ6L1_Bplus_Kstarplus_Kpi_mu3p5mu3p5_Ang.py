##################################################################
# Job options for EvtGen generation of B+ -> K*+(K0Pi+)mu3.5mu3.5.
##################################################################
f = open("BU_KSTARPLUS_MUMU_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")
f.write("Alias my_K*+ K*+\n")
f.write("Decay B+\n")
f.write("1.0000   my_K*+ mu+ mu-   PHOTOS BTOSLLBALL;\n")
f.write("Enddecay\n")
f.write("Decay my_K*+\n")
f.write("1.0000  K0 pi+   VSS;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()
##############################################################

evgenConfig.description = "Signal B+->K*+(Kpi)mu3p5mu3p5 production"
evgenConfig.keywords    = [ "exclusiveB", "Bplus", "K*+", "dimuons" ]
evgenConfig.minevents   = 200

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py")
include("MC12JobOptions/BSignalFilter.py")
#include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")
#Cannot close B-decays => extract all other parts from the exclusive decays config above.
topAlg.Pythia8B.Commands += ['HardQCD:all = on']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.Commands       += [ 'PhaseSpace:pTHatMin = 7.' ]
topAlg.Pythia8B.QuarkPtCut      = 0.0
topAlg.Pythia8B.AntiQuarkPtCut  = 7.0
topAlg.Pythia8B.QuarkEtaCut     = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 2.6
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.Pythia8B.SignalPDGCodes = [ 521 ]
topAlg.EvtInclusiveDecay.userDecayFile = "BU_KSTARPLUS_MUMU_USER.DEC"

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.BSignalFilter.LVL1MuonCutOn  = True
topAlg.BSignalFilter.LVL2MuonCutOn  = True
topAlg.BSignalFilter.LVL1MuonCutPT  = 3500
topAlg.BSignalFilter.LVL1MuonCutEta = 2.6
topAlg.BSignalFilter.LVL2MuonCutPT  = 3500
topAlg.BSignalFilter.LVL2MuonCutEta = 2.6

topAlg.BSignalFilter.B_PDGCode                 = 521
topAlg.BSignalFilter.Cuts_Final_hadrons_switch = True
topAlg.BSignalFilter.Cuts_Final_hadrons_pT     = 500.0
topAlg.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
