##########################################################################
# Job options fragment for bb->J/psi(mu6.5mu6.5)X for trigger LS1 studies.
##########################################################################

evgenConfig.description = "Inclusive bb->J/psi(mu6.5mu6.5)X production"
evgenConfig.keywords    = [ "inclusiveBtoJpsi", "dimuons" ]
evgenConfig.minevents   = 100

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_Photos.py")
include("MC12JobOptions/Pythia8B_inclusiveBJpsi_Common.py")

topAlg.Pythia8B.Commands += [ 'PhaseSpace:pTHatMin = 8.' ]
topAlg.Pythia8B.Commands += [ '443:onMode = off' ]
topAlg.Pythia8B.Commands += [ '443:onIfMatch = -13 13' ]
topAlg.Pythia8B.SignalPDGCodes = [ 443, -13,13 ]

topAlg.Pythia8B.TriggerPDGCode     = 13
topAlg.Pythia8B.TriggerStateEtaCut = 2.7
topAlg.Pythia8B.TriggerStatePtCut  = [ 6.5 ]
topAlg.Pythia8B.MinimumCountPerCut = [ 2 ]

topAlg.Pythia8B.QuarkPtCut                = 0.
topAlg.Pythia8B.AntiQuarkPtCut            = 6.
topAlg.Pythia8B.QuarkEtaCut               = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut           = 3.
topAlg.Pythia8B.RequireBothQuarksPassCuts = True
topAlg.Pythia8B.NHadronizationLoops       = 2
