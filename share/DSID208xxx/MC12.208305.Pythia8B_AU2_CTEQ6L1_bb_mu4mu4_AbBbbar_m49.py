evgenConfig.description = "bb->mu4mu4 production, only b->cbar->mu-, bbar->mu+"
evgenConfig.keywords = ["inclusive","bbbar","dimuons"]
evgenConfig.minevents = 5000

f = open("AbBbbar.DEC","w")
f.write("""
Alias Myanti-D0 anti-D0 
Alias MyD- D- 
Alias MyD_s- D_s- 
Alias Myanti-Lambda_c- anti-Lambda_c- 
Alias MyD*- D*- 
Alias Myanti-D_2*0 anti-D_2*0 
Alias Myanti-D'_10 anti-D'_10 
Alias MyD_2*- D_2*- 
Alias MyD(2S)- D(2S)- 
Alias Myanti-D_0*0 anti-D_0*0 
Alias Myanti-Lambda_c(2593)- anti-Lambda_c(2593)- 
Alias Myanti-D_10 anti-D_10 
Alias MyD_0*- D_0*- 
Alias MyD*(2S)- D*(2S)- 
Alias MyD_s0*- D_s0*- 
Alias MyD_s*- D_s*- 
Alias Myanti-D*(2S)0 anti-D*(2S)0 
Alias Myanti-D*0 anti-D*0 
Alias MyD_s1- D_s1- 
Alias MyD_s2*- D_s2*- 
Alias MyD'_1- D'_1- 
Alias Myanti-Xi_c- anti-Xi_c- 
Alias Myanti-Sigma_c- anti-Sigma_c- 
Alias Myanti-Sigma_c*0 anti-Sigma_c*0 
Alias Myanti-Sigma_c-- anti-Sigma_c-- 
Alias Myanti-Xi'_c0 anti-Xi'_c0 
Alias Myanti-Xi_c0 anti-Xi_c0 
Alias Myanti-D(2S)0 anti-D(2S)0 
Alias Myanti-Xi_c*0 anti-Xi_c*0 
Alias Myanti-Xi_c*- anti-Xi_c*- 
Alias Myanti-Lambda_c(2625)- anti-Lambda_c(2625)- 
Alias Myanti-Xi'_c- anti-Xi'_c- 
Alias Myanti-Omega_c0 anti-Omega_c0 
Alias Myanti-Sigma_c0 anti-Sigma_c0 
Alias MyD_1- D_1- 
Alias Myanti-Sigma_c*- anti-Sigma_c*- 
Alias Myanti-Sigma_c*-- anti-Sigma_c*-- 

Decay anti-B0
  0.000024000 MyD_s- pi+ PHSP; #[Reconstructed PDG2011]
  0.000021000 MyD_s*- pi+ SVS; #[Reconstructed PDG2011]
  0.000016 rho+ MyD_s- SVS;
  0.000041000 MyD_s*- rho+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0; #[Reconstructed PDG2011]
  0.000211000 MyD- D+ PHSP; #[Reconstructed PDG2011]
  0.000305 D*+ MyD- SVS;
  0.000610000 MyD*- D+ SVS; #[Reconstructed PDG2011]
  0.000820000 D*+ MyD*- SVV_HELAMP 0.47 0.0 0.96 0.0 0.56 0.0; #[Reconstructed PDG2011]
  0.007200000 D+ MyD_s- PHSP; #[Reconstructed PDG2011]
  0.008000000 D*+ MyD_s- SVS; #[Reconstructed PDG2011]
  0.007400000 MyD_s*- D+ SVS; #[Reconstructed PDG2011]
  0.017700000 D*+ MyD_s*- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
  0.0006 D'_1+ MyD_s- SVS;
  0.0012 D'_1+ MyD_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 D_1+ MyD_s- SVS;
  0.0024 D_1+ MyD_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 D_2*+ MyD_s- STS;
  0.0040 D_2*+ MyD_s*- PHSP;
  0.0018 MyD_s- D+ pi0 PHSP;
  0.0037 MyD_s- D0 pi+ PHSP;
  0.0018 MyD_s*- D+ pi0 PHSP;
  0.0037 MyD_s*- D0 pi+ PHSP;
  0.0030 MyD_s- D+ pi+ pi- PHSP;
  0.0022 MyD_s- D+ pi0 pi0 PHSP;
  0.0022 MyD_s- D0 pi+ pi0 PHSP;
  0.0030 MyD_s*- D+ pi+ pi- PHSP;
  0.0022 MyD_s*- D+ pi0 pi0 PHSP;
  0.0022 MyD_s*- D0 pi+ pi0 PHSP;
  0.001700000 D+ Myanti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.004600000 D+ Myanti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.003100000 D*+ Myanti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.011800000 D*+ Myanti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.0015 D+ MyD- anti-K0 PHSP;
  0.0018 D*+ MyD- anti-K0 PHSP;
  0.0047 D+ MyD*- anti-K0 PHSP;
  0.007800000 D*+ MyD*- anti-K0 PHSP; #[Reconstructed PDG2011]
  0.0005 D0 Myanti-D0 anti-K0 PHSP;
  0.000120000 D*0 Myanti-D0 anti-K0 PHSP; #[Reconstructed PDG2011]
  0.0015 D0 Myanti-D*0 anti-K0 PHSP;
  0.0015 D*0 Myanti-D*0 anti-K0 PHSP;
  0.0025 D+ Myanti-D0 K*- PHSP;
  0.0025 D*+ Myanti-D0 K*- PHSP;
  0.0025 D+ Myanti-D*0 K*- PHSP;
  0.0050 D*+ Myanti-D*0 K*- PHSP;
  0.0025 D+ MyD- anti-K*0 PHSP;
  0.0025 D*+ MyD- anti-K*0 PHSP;
  0.0025 D+ MyD*- anti-K*0 PHSP;
  0.0050 D*+ MyD*- anti-K*0 PHSP;
  0.0005 Myanti-D0 D0 anti-K*0 PHSP;
  0.0005 Myanti-D0 D*0 anti-K*0 PHSP;
  0.0005 Myanti-D*0 D0 anti-K*0 PHSP;
  0.0010 Myanti-D*0 D*0 anti-K*0 PHSP;
  0.00001 anti-K*0 Myanti-D0 SVS;
  0.00001 Myanti-D*0 anti-K*0 SVV_HELAMP 1. 0. 1. 0. 1. 0.;
  0.0016 MyD_s0*- D+ PHSP;
  0.0015 D*+ MyD_s0*- SVS;
  0.003500000 MyD_s1- D+ SVS; #[Reconstructed PDG2011]
  0.009300000 D*+ MyD_s1- SVV_HELAMP 0.4904 0. 0.7204 0. 0.4904 0.; #[Reconstructed PDG2011]
  0.000006000 Myanti-D0 K- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.002700000 Myanti-D*0 pi- pi- pi+ pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000170000 anti-K0 Myanti-D0 D0 pi0 PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.000011000 Lambda0 anti-Lambda0 Myanti-D0 PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay B-
  0.000016000 MyD_s- pi0 PHSP; #[Reconstructed PDG2011]
  0.000020 MyD_s*- pi0 SVS;
  0.000028 rho0 MyD_s- SVS;
  0.000028 MyD_s*- rho0 SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.010000000 D0 MyD_s- PHSP; #[Reconstructed PDG2011]
  0.008200000 D*0 MyD_s- SVS; #[Reconstructed PDG2011]
  0.007600000 MyD_s*- D0 SVS; #[Reconstructed PDG2011]
  0.017100000 MyD_s*- D*0 SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0; #[Reconstructed PDG2011]
  0.0006 D'_10 MyD_s- SVS;
  0.0012 D'_10 MyD_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0012 D_10 MyD_s- SVS;
  0.0024 D_10 MyD_s*- SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
  0.0042 D_2*0 MyD_s- STS;
  0.0040 D_2*0 MyD_s*- PHSP;
  0.0036 MyD_s- D+ pi- PHSP;
  0.0018 MyD_s- D0 pi0 PHSP;
  0.0037 MyD_s*- D+ pi- PHSP;
  0.0018 MyD_s*- D0 pi0 PHSP;
  0.0033 MyD_s- D+ pi- pi0 PHSP;
  0.0033 MyD_s- D0 pi- pi+ PHSP;
  0.0008 MyD_s- D0 pi0 pi0 PHSP;
  0.0033 MyD_s*- D+ pi- pi0 PHSP;
  0.0033 MyD_s*- D0 pi- pi+ PHSP;
  0.0008 MyD_s*- D0 pi0 pi0 PHSP;
  0.0017 D0 MyD- anti-K0 PHSP;
  0.0052 D0 MyD*- anti-K0 PHSP;
  0.0031 D*0 MyD- anti-K0 PHSP;
  0.007800000 D*0 MyD*- anti-K0 PHSP; #[Reconstructed PDG2011]
  0.002100000 D0 Myanti-D0 K- PHSP; #[Reconstructed PDG2011]
  0.0018 D*0 Myanti-D0 K- PHSP;
  0.004700000 D0 Myanti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.005300000 D*0 Myanti-D*0 K- PHSP; #[Reconstructed PDG2011]
  0.0005 MyD- D+ K- PHSP;
  0.0005 MyD*- D+ K- PHSP;
  0.001500000 MyD- D*+ K- PHSP; #[Reconstructed PDG2011]
  0.0015 MyD*- D*+ K- PHSP;
  0.0025 D0 MyD- anti-K*0 PHSP;
  0.0025 D*0 MyD- anti-K*0 PHSP;
  0.0025 D0 MyD*- anti-K*0 PHSP;
  0.0050 D*0 MyD*- anti-K*0 PHSP;
  0.0025 D0 Myanti-D0 K*- PHSP;
  0.0025 D*0 Myanti-D0 K*- PHSP;
  0.0025 D0 Myanti-D*0 K*- PHSP;
  0.0050 D*0 Myanti-D*0 K*- PHSP;
  0.0005 MyD- D+ K*- PHSP;
  0.0005 MyD*- D+ K*- PHSP;
  0.0005 MyD- D*+ K*- PHSP;
  0.0010 MyD*- D*+ K*- PHSP;
  0.000380000 MyD- D0 PHSP; #[Reconstructed PDG2011]
  0.000390000 MyD*- D0 SVS; #[Reconstructed PDG2011]
  0.000630000 D*0 MyD- SVS; #[Reconstructed PDG2011]
  0.000810000 D*0 MyD*- SVV_HELAMP 0.47 0.0 0.96 0.0 0.56 0.0; #[Reconstructed PDG2011]
  0.0000005 MyD- pi0 PHSP;
  0.0000005 MyD*- pi0 SVS;
  0.000011 MyD- anti-K0 PHSP;
  0.000006 MyD*- anti-K0 SVS;
  0.00075 D0 MyD_s0*- PHSP;
  0.0009 D*0 MyD_s0*- SVS;
  0.003100000 MyD_s1- D0 SVS; #[Reconstructed PDG2011]
  0.012000000 D*0 MyD_s1- SVV_HELAMP 0.4904 0.0 0.7204 0.0 0.4904 0.0; #[Reconstructed PDG2011]
Enddecay

Decay anti-B_s0
  0.010400000 MyD_s- D_s+ PHSP; #[Reconstructed PDG2011]
  0.0099 D_s*+ MyD_s- SVS;
  0.0099 MyD_s*- D_s+ SVS;
  0.0197 MyD_s*- D_s*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0096 MyD_s- D+ K0 PHSP;
  0.0096 MyD_s- Myanti-D0 K+ PHSP;
  0.0096 MyD_s*- D+ K0 PHSP;
  0.0096 MyD_s*- D0 K+ PHSP;
  0.0024 MyD_s- D+ pi0 K0 PHSP;
  0.0048 MyD_s- D0 pi+ K0 PHSP;
  0.0048 MyD_s- D+ pi- K+ PHSP;
  0.0024 MyD_s- D0 pi0 K+ PHSP;
  0.0024 MyD_s*- D+ pi0 K0 PHSP;
  0.0048 MyD_s*- D0 pi+ K0 PHSP;
  0.0048 MyD_s*- D+ pi- K+ PHSP;
  0.0024 MyD_s*- D0 pi0 K+ PHSP;
  0.0150 D_s*+ Myanti-D*0 K- PHSP;
  0.0150 D_s*+ MyD*- anti-K0 PHSP;
  0.0050 D_s*+ Myanti-D0 K- PHSP;
  0.0050 D_s*+ MyD- anti-K0 PHSP;
  0.0050 D_s+ Myanti-D*0 K- PHSP;
  0.0050 D_s+ MyD*- anti-K0 PHSP;
  0.0020 D_s+ Myanti-D0 K- PHSP;
  0.0020 D_s+ MyD- anti-K0 PHSP;
  0.0030 D_s*+ Myanti-D*0 K*- PHSP;
  0.0030 D_s*+ MyD*- anti-K*0 PHSP;
  0.0050 D_s*+ Myanti-D0 K*- PHSP;
  0.0050 D_s*+ MyD- anti-K*0 PHSP;
  0.0025 D_s+ Myanti-D*0 K*- PHSP;
  0.0025 D_s+ MyD*- anti-K*0 PHSP;
  0.0025 D_s+ Myanti-D0 K*- PHSP;
  0.0025 D_s+ MyD- anti-K*0 PHSP;
  0.0017 MyD_s- D+ PHSP;
  0.0017 D*+ MyD_s- SVS;
  0.0017 MyD_s*- D+ SVS;
  0.0017 MyD_s*- D*+ SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
  0.0007 MyD- D+ K0 PHSP;
  0.0007 MyD- Myanti-D0 K+ PHSP;
  0.0007 MyD*- D+ K0 PHSP;
  0.0007 MyD*- D0 K+ PHSP;
  0.0003 MyD- D+ pi0 K0 PHSP;
  0.0007 MyD- D0 pi+ K0 PHSP;
  0.0003 MyD- D+ pi- K+ PHSP;
  0.0007 MyD- D0 pi0 K+ PHSP;
  0.0003 MyD*- D+ pi0 K0 PHSP;
  0.0007 MyD*- D0 pi+ K0 PHSP;
  0.0003 MyD*- D+ pi- K+ PHSP;
  0.0007 MyD*- D0 pi0 K+ PHSP;
  0.000150000 MyD_s- K+ PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay Lambda_b0
  0.02200 Lambda_c+ MyD_s- PHSP;
  0.04400 Lambda_c+ MyD_s*- PHSP;
  0.00080 Lambda0 Myanti-D0 PHSP;
Enddecay

Decay B0
  0.050100000 D*- mu+ nu_mu PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.021700000 D- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0054 D_1- mu+ nu_mu PHOTOS ISGW2;
  0.0020 D_0*- mu+ nu_mu PHOTOS ISGW2;
  0.0050 D'_1- mu+ nu_mu PHOTOS ISGW2;
  0.0022 D_2*- mu+ nu_mu PHOTOS ISGW2;
  0.0003 D*- pi0 mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.004900000 anti-D*0 pi- mu+ nu_mu PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0010 D- pi0 mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.0000 anti-D0 pi- mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.000134000 pi- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000247000 rho- mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000000 D(2S)- mu+ nu_mu PHOTOS ISGW2;
  0.000000 D*(2S)- mu+ nu_mu PHOTOS ISGW2;
  #0.001892 Xu- mu+ nu_mu VUB 4.8 1.29 0.22 20 0.30 0.55 1.20 0.61 1.26 0.85 1.34 1.08 1.41 1.21 1.48 1.30 1.55 1.30 1.61 1.33 1.67 1.36 1.73 1.39 1.79 1.33 1.84 1.42 1.90 1.39 1.95 1.39 2.00 1.37 2.50 1.30 3.00 0.74 3.50 0.99 4.00 1.09 4.50 1.00; #TODO -- temporary workaround
  0.000000450 K0 mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  0.000001050 K*0 mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  #0.0000025 Xsd mu+ mu- PHOTOS BTOXSLL 4.8 0.2 0.0 0.41;
Enddecay

Decay B+
  0.056800000 anti-D*0 mu+ nu_mu PHOTOS HQET 0.77 1.33 0.92; #[Reconstructed PDG2011]
  0.022300000 anti-D0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.0040 anti-D_10 mu+ nu_mu PHOTOS ISGW2;
  0.0024 anti-D_0*0 mu+ nu_mu PHOTOS ISGW2;
  0.0007 anti-D'_10 mu+ nu_mu PHOTOS ISGW2;
  0.0018 anti-D_2*0 mu+ nu_mu PHOTOS ISGW2;
  0.006100000 D*- pi+ mu+ nu_mu PHOTOS GOITY_ROBERTS; #[Reconstructed PDG2011]
  0.0003 anti-D*0 pi0 mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.0000 D- pi+ mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.0010 anti-D0 pi0 mu+ nu_mu PHOTOS GOITY_ROBERTS;
  0.000077000 pi0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000037000 eta mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000128000 rho0 mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000115000 omega mu+ nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000270 eta' mu+ nu_mu PHOTOS ISGW2;
  0.000000 anti-D(2S)0 mu+ nu_mu PHOTOS ISGW2;
  0.000000 anti-D*(2S)0 mu+ nu_mu PHOTOS ISGW2;
  0.001948 Xu0 mu+ nu_mu VUB 4.8 1.29 0.22 20 0.30 0.54 1.20 0.95 1.26 0.78 1.34 0.98 1.41 0.91 1.48 1.23 1.55 1.36 1.61 1.39 1.67 1.38 1.73 1.43 1.79 1.41 1.84 1.42 1.90 1.45 1.95 1.40 2.00 1.42 2.50 1.31 3.00 1.36 3.50 1.15 4.00 1.01 4.50 1.51;
  0.000000520 K+ mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  0.000001160 K*+ mu+ mu- PHOTOS BTOSLLBALL; #[Reconstructed PDG2011]
  #0.0000025 Xsu mu+ mu- PHOTOS BTOXSLL 4.8 0.2 0.0 0.41;
  0.000000 mu+ nu_mu PHOTOS SLN;
Enddecay

Decay B_s0
  0.0210 D_s- mu+ nu_mu PHOTOS ISGW2;
  0.0490 D_s*- mu+ nu_mu PHOTOS ISGW2;
  0.0040 D_s1- mu+ nu_mu PHOTOS ISGW2;
  0.0040 D_s0*- mu+ nu_mu PHOTOS ISGW2;
  0.0070 D'_s1- mu+ nu_mu PHOTOS ISGW2;
  0.0070 D_s2*- mu+ nu_mu PHOTOS ISGW2;
  0.000200 K- mu+ nu_mu PHOTOS ISGW2;
  0.000300 K*- mu+ nu_mu PHOTOS ISGW2;
  0.000300 K_1- mu+ nu_mu PHOTOS ISGW2;
  0.000200 K'_1- mu+ nu_mu PHOTOS ISGW2;
  0.0000000035 mu+ mu- PHSP;
  0.0000023 phi mu+ mu- BTOSLLALI;
Enddecay

Decay anti-Lambda_b0
  0.050000000 anti-Lambda_c- mu+ nu_mu PHSP; #[Reconstructed PDG2011]
  0.006300000 anti-Lambda_c(2593)- mu+ nu_mu PHSP; #[Reconstructed PDG2011]
  0.011000000 anti-Lambda_c(2625)- mu+ nu_mu PHSP; #[Reconstructed PDG2011]
  0.00000 anti-Sigma_c0 pi- mu+ nu_mu PHSP;
  0.00000 anti-Sigma_c- pi0 mu+ nu_mu PHSP;
  0.00000 anti-Sigma_c-- pi+ mu+ nu_mu PHSP;
  0.00000 anti-Sigma_c*0 pi- mu+ nu_mu PHSP;
  0.00000 anti-Sigma_c*- pi0 mu+ nu_mu PHSP;
  0.00000 anti-Sigma_c*-- pi+ mu+ nu_mu PHSP;
  0.056000000 anti-Lambda_c- pi- pi+ mu+ nu_mu PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay MyD*-
  0.6770 Myanti-D0 pi- VSS;
  0.3070 MyD- pi0 VSS;
  0.0160 MyD- gamma VSP_PWAVE;
Enddecay

Decay Myanti-D*0
  0.6190 Myanti-D0 pi0 VSS;
  0.3810 Myanti-D0 gamma VSP_PWAVE;
Enddecay

Decay MyD_s*-
  0.942000000 MyD_s- gamma VSP_PWAVE; #[Reconstructed PDG2011]
  0.058000000 MyD_s- pi0 VSS; #[Reconstructed PDG2011]
Enddecay

Decay MyD_0*-
  0.3333 MyD- pi0 PHSP;
  0.6667 Myanti-D0 pi- PHSP;
Enddecay

Decay Myanti-D_0*0
  0.3333 Myanti-D0 pi0 PHSP;
  0.6667 MyD- pi+ PHSP;
Enddecay

Decay MyD_1-
  0.3333 MyD*- pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 Myanti-D*0 pi- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay Myanti-D_10
  0.3333 Myanti-D*0 pi0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.6667 MyD*- pi+ VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay

Decay MyD'_1-
  0.3333 MyD*- pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 Myanti-D*0 pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay Myanti-D'_10
  0.3333 Myanti-D*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.6667 MyD*- pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay

Decay MyD_2*-
  0.1030 MyD*- pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 Myanti-D*0 pi- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 MyD- pi0 TSS;
  0.4590 Myanti-D0 pi- TSS;
Enddecay

Decay Myanti-D_2*0
  0.1030 Myanti-D*0 pi0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2090 MyD*- pi+ TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.2290 Myanti-D0 pi0 TSS;
  0.4590 MyD- pi+ TSS;
Enddecay

Decay MyD_s0*-
  1.000 MyD_s- pi0 PHSP;
Enddecay

Decay D'_s1-
  0.5000 MyD*- anti-K0 VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.5000 Myanti-D*0 K- VVS_PWAVE 0.0 0.0 0.0 0.0 1.0 0.0;
  0.0000 gamma MyD_s*- PHSP;
  0.0000 gamma MyD_s- PHSP;
Enddecay

Decay MyD_s1-
  0.80000 MyD_s*- pi0 PARTWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.20000 MyD_s- gamma VSP_PWAVE;
Enddecay

Decay MyD_s2*-
  0.0500 MyD*- anti-K0 TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.0500 Myanti-D*0 K- TVS_PWAVE 0.0 0.0 1.0 0.0 0.0 0.0;
  0.4300 MyD- anti-K0 TSS;
  0.4700 Myanti-D0 K- TSS;
Enddecay

Decay Myanti-D(2S)0
  0.3333 Myanti-D*0 pi0 SVS;
  0.6667 MyD*- pi+ SVS;
Enddecay

Decay MyD(2S)-
  0.3333 MyD*- pi0 SVS;
  0.6667 Myanti-D*0 pi- SVS;
Enddecay

Decay Myanti-D*(2S)0
  0.1667 Myanti-D*0 pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 MyD*- pi+ VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 Myanti-D0 pi0 VSS;
  0.3333 MyD- pi+ VSS;
Enddecay

Decay D*(2S)-
  0.1667 MyD*- pi0 VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.3333 Myanti-D*0 pi- VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
  0.1667 MyD- pi0 VSS;
  0.3333 Myanti-D0 pi- VSS;
Enddecay

Decay Myanti-Sigma_c0
  1.0000 Myanti-Lambda_c- pi+ PHSP;
Enddecay

Decay Myanti-Sigma_c-
  1.0000 Myanti-Lambda_c- pi0 PHSP;
Enddecay

Decay Myanti-Sigma_c--
  1.0000 Myanti-Lambda_c- pi- PHSP;
Enddecay

Decay Myanti-Xi'_c-
  0.8200 gamma Myanti-Xi_c- PHSP;
Enddecay

Decay Myanti-Xi'_c0
  1.0000 gamma Myanti-Xi_c0 PHSP;
Enddecay

Decay Myanti-Sigma_c*0
  1.0000 Myanti-Lambda_c- pi+ PHSP;
Enddecay

Decay Myanti-Sigma_c*-
  1.0000 Myanti-Lambda_c- pi0 PHSP;
Enddecay

Decay Myanti-Sigma_c*--
  1.0000 Myanti-Lambda_c- pi- PHSP;
Enddecay

Decay Myanti-Lambda_c(2593)-
  0.096585366 Myanti-Sigma_c-- pi+ PHSP; #[Reconstructed PDG2011]
  0.096585366 Myanti-Sigma_c0 pi- PHSP; #[Reconstructed PDG2011]
  0.190000000 Myanti-Lambda_c- pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.096585366 Myanti-Sigma_c- pi0 PHSP; #[Reconstructed PDG2011]
  0.036219512 Myanti-Lambda_c- pi0 pi0 PHSP; #[Reconstructed PDG2011]
  0.004024390 Myanti-Lambda_c- gamma PHSP; #[Reconstructed PDG2011]
  0.240000000 Myanti-Sigma_c*-- pi+ PHSP; #[New mode added] #[Reconstructed PDG2011]
  0.240000000 Myanti-Sigma_c*0 pi- PHSP; #[New mode added] #[Reconstructed PDG2011]
Enddecay

Decay Myanti-Lambda_c(2625)-
  0.670000000 Myanti-Lambda_c- pi- pi+ PHSP; #[Reconstructed PDG2011]
  0.320294118 Myanti-Lambda_c- pi0 PHSP; #[Reconstructed PDG2011]
  0.009705882 Myanti-Lambda_c- gamma PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD-
  0.055000000 K*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.094000000 K0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002773020 K_10 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002927076 K_2*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.003312218 pi0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002002736 eta mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000385142 eta' mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002500000 rho0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002156793 omega mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.039000000 K+ pi- mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.001078397 K0 pi0 mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000382000 mu- anti-nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
Enddecay

Decay Myanti-D0
  0.019800000 K*+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.033100000 K+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000818960 K_1+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001380270 K_2*+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002370000 pi+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002024397 rho+ mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001012198 K0 pi+ mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000552108 K+ pi0 mu- anti-nu_mu PHOTOS PHSP; #[Reconstructed PDG2011]
  0.000000000 mu- mu+ PHSP; #[Reconstructed PDG2011]
  #
  0.021700000 K*+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.035500000 K+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000760000 K_1+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.001380270 K_2*+ e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.038900000 K+ pi- PHSP; #[Reconstructed PDG2011]
Enddecay

Decay MyD_s-
  0.018309605 phi mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.022845082 eta mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.008186726 eta' mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 K0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 K*0 mu- anti-nu_mu PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.005800000 mu- anti-nu_mu PHOTOS SLN; #[Reconstructed PDG2011]
  #
  0.024900000 phi e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.026700000 eta e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.009900000 eta' e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002058115 K0 e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.000762265 K*0 e- anti-nu_e PHOTOS ISGW2; #[Reconstructed PDG2011]
  0.002300000 omega pi- SVS; #[Reconstructed PDG2011]
  0.000200000 rho0 pi- SVS; #[Reconstructed PDG2011]
  0.000304906 rho- pi0 SVS; #[Reconstructed PDG2011]
Enddecay

Decay Myanti-Lambda_c-
  0.020000000 mu- anti-nu_mu anti-Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 mu- anti-nu_mu anti-Sigma0 PYTHIA 42;
  0.00500 mu- anti-nu_mu anti-Sigma*0 PYTHIA 42;
  0.00300 mu- anti-nu_mu anti-n0 PYTHIA 42;
  0.00200 mu- anti-nu_mu anti-Delta0 PYTHIA 42;
  0.00600 mu- anti-nu_mu anti-p- pi+ PYTHIA 42;
  0.00600 mu- anti-nu_mu anti-n0 pi0 PYTHIA 42;
  #
  0.021000000 e- anti-nu_e anti-Lambda0 PYTHIA 42; #[Reconstructed PDG2011]
  0.00500 e- anti-nu_e anti-Sigma0 PYTHIA 42;
  0.00500 e- anti-nu_e anti-Sigma*0 PYTHIA 42;
  0.00300 e- anti-nu_e anti-n0 PYTHIA 42;
  0.00200 e- anti-nu_e anti-Delta0 PYTHIA 42;
  0.00600 e- anti-nu_e anti-p- pi+ PYTHIA 42;
  0.00600 e- anti-nu_e anti-n0 pi0 PYTHIA 42;
  0.008600000 anti-Delta-- K+ PYTHIA 0; #[Reconstructed PDG2011]
  0.02500 anti-Delta-- K*+ PYTHIA 0;
  0.023000000 anti-p- K0 PYTHIA 0; #[Reconstructed PDG2011]
  0.016000000 anti-p- K*0 PYTHIA 0; #[Reconstructed PDG2011]
  0.00500 anti-Delta- K0 PYTHIA 0;
  0.010700000 anti-Lambda0 pi- PYTHIA 0; #[Reconstructed PDG2011]
Enddecay

End
""")

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_EvtGenAfterburner.py") 
include("MC12JobOptions/MultiMuonFilter.py")
include("MC12JobOptions/DiLeptonMassFilter.py")

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
#topAlg.Pythia8B.Commands += ['HardQCD:gg2bbbar = on']
#topAlg.Pythia8B.Commands += ['HardQCD:qqbar2bbbar = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 9.']
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quark cuts
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.QuarkPtCut = 5.0
topAlg.Pythia8B.AntiQuarkPtCut = 5.0
topAlg.Pythia8B.QuarkEtaCut = 3.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
topAlg.Pythia8B.NHadronizationLoops = 10
topAlg.Pythia8B.NDecayLoops = 1

# List of B-species
include("Pythia8B_i/BPDGCodes.py")

topAlg.EvtInclusiveDecay.userDecayFile = "AbBbbar.DEC"
topAlg.EvtInclusiveDecay.allowAllKnownDecays = False

topAlg.EvtInclusiveDecay.maxNRepeatedDecays = 150

topAlg.EvtInclusiveDecay.applyUserSelection = True
topAlg.EvtInclusiveDecay.userSelRequireOppositeSignedMu = True
topAlg.EvtInclusiveDecay.userSelMu1MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu2MinPt = 3500.
topAlg.EvtInclusiveDecay.userSelMu1MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMu2MaxEta = 3.0
topAlg.EvtInclusiveDecay.userSelMinDimuMass = 4000.
topAlg.EvtInclusiveDecay.userSelMaxDimuMass = 10000.

# Final state selections
topAlg.Pythia8B.TriggerPDGCode = 0
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 3.0
topAlg.MultiMuonFilter.NMuons = 2
topAlg.DiLeptonMassFilter.MinPt = 3500.
topAlg.DiLeptonMassFilter.MaxEta = 3.0
topAlg.DiLeptonMassFilter.MinMass = 4000.
topAlg.DiLeptonMassFilter.MaxMass = 10000.
topAlg.DiLeptonMassFilter.AllowSameCharge = False

