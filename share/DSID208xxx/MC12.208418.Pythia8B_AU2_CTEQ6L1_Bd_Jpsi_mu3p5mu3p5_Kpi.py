########################################################################
# Job options for Pythia8B_i generation of Bd -> J/psi(mu3p5mu3p5) K Pi
########################################################################
evgenConfig.description = "Signal Bd->J/psi(mu3p5mu3p5)K+Pi-)"
evgenConfig.keywords = ["exclusiveB","Bd","dimuons"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8B_exclusiveB_Common.py")

topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.AntiQuarkPtCut = 8.0
topAlg.Pythia8B.QuarkEtaCut = 102.5
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

topAlg.Pythia8B.Commands += ['511:onIfMatch = 443 321 -211']
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:onIfMatch = -13 13']
topAlg.Pythia8B.SignalPDGCodes = [511,443,-13,13,321,-211]

topAlg.Pythia8B.NHadronizationLoops = 1

topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [2]

