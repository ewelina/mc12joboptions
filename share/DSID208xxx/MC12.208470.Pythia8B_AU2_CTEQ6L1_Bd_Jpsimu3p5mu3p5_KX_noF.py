########################################################################
# Job options for Pythia8B_i generation of B0/Bs/Lambda_b->J/psi(mumu)hh
########################################################################
evgenConfig.description = "Background B0->J/psi(mumu)_KX"
evgenConfig.keywords = ["BtoJpsi","dimuons","pentaquarks"]
evgenConfig.minevents = 1000

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']
#
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.'] 

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.VetoDoubleCEvents = False
#
topAlg.Pythia8B.QuarkPtCut = 0.0
topAlg.Pythia8B.QuarkEtaCut = 1000.
topAlg.Pythia8B.AntiQuarkPtCut = 8.0
topAlg.Pythia8B.AntiQuarkEtaCut = 3.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = True

#
# J/psi:
#
topAlg.Pythia8B.Commands += ['443:m0 = 3.096916']  # PDG 2015
topAlg.Pythia8B.Commands += ['443:mWidth = 0.0000929'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['443:onMode = off']
topAlg.Pythia8B.Commands += ['443:2:onMode = on']

#
# Lambda_b:
#
topAlg.Pythia8B.Commands += ['5122:m0 = 5.61951']  # PDG 2015
topAlg.Pythia8B.Commands += ['5122:tau0 = 0.4395'] # PDG 2015
#
# B_d:
#
topAlg.Pythia8B.Commands += ['511:m0 = 5.27961']  # PDG 2015
topAlg.Pythia8B.Commands += ['511:tau0 = 0.4557'] # PDG 2015
#
# B_d decays:
#
topAlg.Pythia8B.Commands += ['511:onMode = 3']
#
# channels 850-869 have onMode = 2 in Pythia8 default
#
topAlg.Pythia8B.Commands += ['511:850:onMode = 0']
topAlg.Pythia8B.Commands += ['511:851:onMode = 0']
topAlg.Pythia8B.Commands += ['511:852:onMode = 0']
topAlg.Pythia8B.Commands += ['511:853:onMode = 0']
topAlg.Pythia8B.Commands += ['511:854:onMode = 0']
topAlg.Pythia8B.Commands += ['511:855:onMode = 0']
topAlg.Pythia8B.Commands += ['511:856:onMode = 0']
topAlg.Pythia8B.Commands += ['511:857:onMode = 0']
topAlg.Pythia8B.Commands += ['511:858:onMode = 0']
topAlg.Pythia8B.Commands += ['511:859:onMode = 0']
topAlg.Pythia8B.Commands += ['511:860:onMode = 0']
topAlg.Pythia8B.Commands += ['511:861:onMode = 0']
topAlg.Pythia8B.Commands += ['511:862:onMode = 0']
topAlg.Pythia8B.Commands += ['511:863:onMode = 0']
topAlg.Pythia8B.Commands += ['511:864:onMode = 0']
topAlg.Pythia8B.Commands += ['511:865:onMode = 0']
topAlg.Pythia8B.Commands += ['511:866:onMode = 0']
topAlg.Pythia8B.Commands += ['511:867:onMode = 0']
topAlg.Pythia8B.Commands += ['511:868:onMode = 0']
topAlg.Pythia8B.Commands += ['511:869:onMode = 0']
#
# channels 870-889 have onMode = 3 in Pythia8 default
#
#topAlg.Pythia8B.Commands += ['511:870:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:871:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:872:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:873:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:874:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:875:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:876:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:877:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:878:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:879:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:880:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:881:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:882:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:883:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:884:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:885:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:886:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:887:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:888:onMode = 0']
#topAlg.Pythia8B.Commands += ['511:889:onMode = 0']
#
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.20 0  443 321 -211']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 200313']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.21 0  443 315']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.20 0  443 10311']
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 10315']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.23 0  443 30313']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.03 0  443 317']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.08 0  443 9020311']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 319']
#
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 211 -211']
#
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.01 0  443 225']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.05 0  443 100113']
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 30113']
#
# topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.02 0  443 321 -321']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.50 0  -44123 321']
#
topAlg.Pythia8B.Commands += ['511:addChannel = 2 0.05 0  -10044123 321']
#

#
# K*(1410)0 -> K+ pi- # PDG 2016
#
topAlg.Pythia8B.Commands += ['200313:new = K*(1410)0 K*(1410)bar0 3 0 0 1.414 0.232 1.450 2.183 0.']
topAlg.Pythia8B.Commands += ['200313:addChannel = 1 1. 0 321 -211']

#
# K*_2(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['315:m0 = 1.4324']  # PDG 2016
topAlg.Pythia8B.Commands += ['315:mWidth = 0.109'] # PDG 2016
#
topAlg.Pythia8B.Commands += ['315:mMin = 1.450']
topAlg.Pythia8B.Commands += ['315:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['315:onMode = 3']
topAlg.Pythia8B.Commands += ['315:0:onMode = 1']

#
# K*_0(1430)0 -> K+ pi-
#
topAlg.Pythia8B.Commands += ['10311:m0 = 1.425']  # PDG 2015
topAlg.Pythia8B.Commands += ['10311:mWidth = 0.270'] # PDG 2015
#
topAlg.Pythia8B.Commands += ['10311:mMin = 1.450']
topAlg.Pythia8B.Commands += ['10311:mMax = 2.183']
#
topAlg.Pythia8B.Commands += ['10311:onMode = 3']
topAlg.Pythia8B.Commands += ['10311:0:onMode = 1']
#

#
# K*_2(1980)0 -> K+ pi- # PDFG 2016 (following BELLE)
#
topAlg.Pythia8B.Commands += ['10315:new = K*_2(1980)0 K*_2(1980)bar0 5 0 0 1.973 0.373 1.450 2.183 0.']
topAlg.Pythia8B.Commands += ['10315:addChannel = 1 1. 0 321 -211']

#
# X(4200)+ -> J/psi pi+
#
topAlg.Pythia8B.Commands += ['44123:new = X(4200)+ X(4200)- 3 3 0 4.196 0.370 3.2365 4.7859 0.']
topAlg.Pythia8B.Commands += ['44123:addChannel = 1 1. 0 443 211']

#
# X(4430)+ -> J/psi pi+
#
topAlg.Pythia8B.Commands += ['10044123:new = X(4430)+ X(4430)- 3 3 0 4.478 0.181 3.2365 4.7859 0.']
topAlg.Pythia8B.Commands += ['10044123:addChannel = 1 1. 0 443 211']

#
# B_u:
#
topAlg.Pythia8B.Commands += ['521:m0 = 5.27929']  # PDG 2015
topAlg.Pythia8B.Commands += ['521:tau0 = 0.4911'] # PDG 2015
#

#
# B_s:
#
topAlg.Pythia8B.Commands += ['531:m0 = 5.36679']  # PDG 2015
topAlg.Pythia8B.Commands += ['531:tau0 = 0.4527'] # PDG 2015
#


topAlg.Pythia8B.NHadronizationLoops = 2


topAlg.Pythia8B.TriggerPDGCode = 13
topAlg.Pythia8B.TriggerStatePtCut = [3.5]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.TriggerOppositeCharges = True
topAlg.Pythia8B.MinimumCountPerCut = [2]

#include("MC12JobOptions/ParentsTracksFilter.py")
##include("Generators/GeneratorFilters/share/ParentsTracksFilter.py")
#topAlg.ParentsTracksFilter.PDGParent  = [511]
#topAlg.ParentsTracksFilter.PtMinParent =  11000.
#topAlg.ParentsTracksFilter.EtaRangeParent = 2.3
#topAlg.ParentsTracksFilter.PtMinLeptons = 3500.
#topAlg.ParentsTracksFilter.EtaRangeLeptons = 2.5
#topAlg.ParentsTracksFilter.PtMinHadrons = 1600.
#topAlg.ParentsTracksFilter.EtaRangeHadrons = 2.7
#topAlg.ParentsTracksFilter.NumMinTracks = 4
#topAlg.ParentsTracksFilter.NumMaxTracks = 4
#topAlg.ParentsTracksFilter.NumMinLeptons = 2
#topAlg.ParentsTracksFilter.NumMaxLeptons = 2
#topAlg.ParentsTracksFilter.NumMinOthers = 0
#topAlg.ParentsTracksFilter.NumMaxOthers = 0
