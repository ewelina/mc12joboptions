#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     1.04000000E+03   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     1.05000000E+03   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05365999E+01   # W+
        25     1.26000000E+02   # h
        35     2.00401316E+03   # H
        36     2.00000000E+03   # A
        37     2.00160786E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.25753424E+03   # ~d_L
   2000001     2.25729389E+03   # ~d_R
   1000002     2.25699716E+03   # ~u_L
   2000002     2.25712491E+03   # ~u_R
   1000003     2.25753424E+03   # ~s_L
   2000003     2.25729389E+03   # ~s_R
   1000004     2.25699716E+03   # ~c_L
   2000004     2.25712491E+03   # ~c_R
   1000005     2.25646509E+03   # ~b_1
   2000005     2.25836568E+03   # ~b_2
   1000006     2.28232736E+03   # ~t_1
   2000006     2.45312085E+03   # ~t_2
   1000011     2.50016618E+03   # ~e_L
   2000011     2.50015256E+03   # ~e_R
   1000012     2.49968123E+03   # ~nu_eL
   1000013     2.50016618E+03   # ~mu_L
   2000013     2.50015256E+03   # ~mu_R
   1000014     2.49968123E+03   # ~nu_muL
   1000015     2.49959956E+03   # ~tau_1
   2000015     2.50072033E+03   # ~tau_2
   1000016     2.49968123E+03   # ~nu_tauL
   1000021     2.31205156E+03   # ~g
   1000022     1.05289822E+03   # ~chi_10
   1000023    -1.10655191E+03   # ~chi_20
   1000025     1.14077828E+03   # ~chi_30
   1000035     2.55465166E+03   # ~chi_40
   1000024     1.10233116E+03   # ~chi_1+
   1000037     2.55464804E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     7.49495350E-01   # N_11
  1  2    -3.43381248E-02   # N_12
  1  3     4.69794302E-01   # N_13
  1  4    -4.65156884E-01   # N_14
  2  1     3.97823175E-03   # N_21
  2  2    -4.26284331E-03   # N_22
  2  3    -7.06933184E-01   # N_23
  2  4    -7.07256301E-01   # N_24
  3  1     6.61995650E-01   # N_31
  3  2     4.13595320E-02   # N_32
  3  3    -5.27555287E-01   # N_33
  3  4     5.30788628E-01   # N_34
  4  1     1.62897701E-03   # N_41
  4  2    -9.98544997E-01   # N_42
  4  3    -3.49886584E-02   # N_43
  4  4     4.10003628E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -4.94341137E-02   # U_11
  1  2     9.98777387E-01   # U_12
  2  1     9.98777387E-01   # U_21
  2  2     4.94341137E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -5.79379388E-02   # V_11
  1  2     9.98320187E-01   # V_12
  2  1     9.98320187E-01   # V_21
  2  2     5.79379388E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.99985497E-01   # cos(theta_t)
  1  2    -5.38570234E-03   # sin(theta_t)
  2  1     5.38570234E-03   # -sin(theta_t)
  2  2     9.99985497E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     6.60884190E-01   # cos(theta_b)
  1  2     7.50487900E-01   # sin(theta_b)
  2  1    -7.50487900E-01   # -sin(theta_b)
  2  2     6.60884190E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     7.02797552E-01   # cos(theta_tau)
  1  2     7.11389908E-01   # sin(theta_tau)
  2  1    -7.11389908E-01   # -sin(theta_tau)
  2  2     7.02797552E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89922009E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     1.05000000E+03   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.52018682E+02   # vev(Q)              
         4     3.58337227E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53427231E-01   # gprime(Q) DRbar
     2     6.30106167E-01   # g(Q) DRbar
     3     1.08795704E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.03989761E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.73376084E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.79968067E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     1.04000000E+03   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     1.71982612E+06   # M^2_Hd              
        22    -9.05091136E+05   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.38351723E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.35081554E-01   # gluino decays
#          BR         NDA      ID1       ID2
     4.95505224E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.95505224E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99831330E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99831330E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.05197525E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.05197525E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.02883812E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.02883812E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.95505224E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.95505224E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99831330E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99831330E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.05197525E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.05197525E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.02883812E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.02883812E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     4.79901561E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     4.79901561E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     5.12745330E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     5.12745330E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.03465014E-04    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     2.00485068E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     2.17006144E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.90537464E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     2.98157323E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
    -5.70093089E-03    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     5.00049845E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.43970392E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.36064288E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.50518538E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.70725471E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -1.42521183E-03    2     1000006        25   # BR(~t_2 -> ~t_1    h )
     1.12224372E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     6.02449739E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     7.50551739E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005     8.01872597E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
     4.68550602E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
     7.24344911E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
     1.86882044E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     9.33732390E-01    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     1.25359708E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.64903742E-02    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     6.52441905E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     1.46864548E-02    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.68170729E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
#
#         PDG            Width
DECAY   1000002     2.23539004E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     2.64657898E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     2.76239576E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     5.84592808E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     1.50473054E-01    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
#
#         PDG            Width
DECAY   2000002     2.83581044E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     5.85711477E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     1.55665822E-05    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     4.14272957E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     2.18165105E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     7.37861825E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     5.73205695E-04    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     1.49268488E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     1.12296482E-01    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
#
#         PDG            Width
DECAY   2000001     7.09059768E-01   # sdown_R decays
#          BR         NDA      ID1       ID2
     5.85706794E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     1.55666348E-05    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     4.14277640E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
#
#         PDG            Width
DECAY   1000004     2.23539004E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     2.64657898E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     2.76239576E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     5.84592808E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     1.50473054E-01    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
#
#         PDG            Width
DECAY   2000004     2.83581044E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     5.85711477E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     1.55665822E-05    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     4.14272957E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     2.18165105E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     7.37861825E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     5.73205695E-04    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     1.49268488E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     1.12296482E-01    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
#
#         PDG            Width
DECAY   2000003     7.09059768E-01   # sstrange_R decays
#          BR         NDA      ID1       ID2
     5.85706794E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     1.55666348E-05    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     4.14277640E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
#
#         PDG            Width
DECAY   1000011     2.08152591E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     4.78454606E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     1.26578660E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     5.06486016E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     1.50467197E-02    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     8.13800362E+00   # selectron_R decays
#          BR         NDA      ID1       ID2
     5.80461183E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     1.56250243E-05    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     4.19523192E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     2.08152591E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     4.78454606E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     1.26578660E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     5.06486016E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     1.50467197E-02    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     8.13800362E+00   # smuon_R decays
#          BR         NDA      ID1       ID2
     5.80461183E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     1.56250243E-05    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     4.19523192E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     5.19427052E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     5.66562511E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     7.89440495E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     4.25156060E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     7.47036029E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     2.16278844E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     5.05659871E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     5.50074598E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.27823135E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     4.48067586E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     5.79585331E-04    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     2.09824762E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     6.58294568E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     1.28281439E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     3.21080924E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     2.04962262E-02    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     2.09824762E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     6.58294568E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     1.28281439E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     3.21080924E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     2.04962262E-02    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     2.10864028E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     6.55050093E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     1.27649190E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     3.19498443E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     2.53238149E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.78509109E-04   # chargino1+ decays
#          BR         NDA      ID1       ID2
     5.15468380E-03    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.31848296E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.31848296E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.10616667E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.10616667E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09915390E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     4.10120980E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     3.54211124E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     3.53363769E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     3.54211124E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     3.53363769E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.13274675E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
    -2.92330933E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
     1.58687116E-02    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     2.26394208E-02    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     4.44312892E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     4.44312892E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     4.44045505E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     4.36991239E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     4.36991239E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.19674110E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.17327983E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.96823785E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     7.77829875E-02    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.91713241E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     1.14469905E-01    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.95242041E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     3.03848793E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     9.24749526E-11    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     1.14584478E-06   # neutralino1 decays
#          BR         NDA      ID1       ID2
     5.27376765E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.63005647E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     9.61758736E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     2.16544331E-04   # neutralino2 decays
#          BR         NDA      ID1       ID2
     7.42471528E-04    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.69030082E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.65713176E-04    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     4.02828686E-03    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.17905426E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.52593234E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.17905426E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.52593234E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.41392845E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.48153479E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.48153479E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.45142125E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     6.95051362E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     6.95051362E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     6.95051362E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     2.31971608E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     2.31971608E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     2.31971608E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     2.31971608E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     7.73239024E-07    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     7.73239024E-07    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     7.73239024E-07    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     7.73239024E-07    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     3.28973258E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     3.28973258E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.45166490E-04   # neutralino3 decays
#          BR         NDA      ID1       ID2
     3.73018091E-04    2     1000022        22   # BR(~chi_30 -> ~chi_10 gam)
     1.77491222E-05    2     1000023        22   # BR(~chi_30 -> ~chi_20 gam)
#          BR         NDA      ID1       ID2
     5.47213418E-03    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.26288352E-03    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.58907875E-04    2     1000039        25   # BR(~chi_30 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.04462832E-04    3     1000022        -2         2   # BR(~chi_30 -> ~chi_10 ub      u)
     1.08688485E-04    3     1000022        -1         1   # BR(~chi_30 -> ~chi_10 db      d)
     1.04462832E-04    3     1000022        -4         4   # BR(~chi_30 -> ~chi_10 cb      c)
     1.08688485E-04    3     1000022        -3         3   # BR(~chi_30 -> ~chi_10 sb      s)
     1.74847940E-04    3     1000022        -5         5   # BR(~chi_30 -> ~chi_10 bb      b)
     1.57261165E-05    3     1000022       -11        11   # BR(~chi_30 -> ~chi_10 e+      e-)
     1.57261165E-05    3     1000022       -13        13   # BR(~chi_30 -> ~chi_10 mu+     mu-)
     2.58256162E-05    3     1000022       -15        15   # BR(~chi_30 -> ~chi_10 tau+    tau-)
     5.65796208E-05    3     1000022       -12        12   # BR(~chi_30 -> ~chi_10 nu_eb   nu_e)
     5.65796208E-05    3     1000022       -14        14   # BR(~chi_30 -> ~chi_10 nu_mub  nu_mu)
     5.65796208E-05    3     1000022       -16        16   # BR(~chi_30 -> ~chi_10 nu_taub nu_tau)
     2.11732479E-02    3     1000023        -2         2   # BR(~chi_30 -> ~chi_20 ub      u)
     2.74026903E-02    3     1000023        -1         1   # BR(~chi_30 -> ~chi_20 db      d)
     2.11732479E-02    3     1000023        -4         4   # BR(~chi_30 -> ~chi_20 cb      c)
     2.74026903E-02    3     1000023        -3         3   # BR(~chi_30 -> ~chi_20 sb      s)
     2.28852371E-02    3     1000023        -5         5   # BR(~chi_30 -> ~chi_20 bb      b)
     6.25219978E-03    3     1000023       -11        11   # BR(~chi_30 -> ~chi_20 e+      e-)
     6.25219978E-03    3     1000023       -13        13   # BR(~chi_30 -> ~chi_20 mu+     mu-)
     6.14619312E-03    3     1000023       -15        15   # BR(~chi_30 -> ~chi_20 tau+    tau-)
     1.24814329E-02    3     1000023       -12        12   # BR(~chi_30 -> ~chi_20 nu_eb   nu_e)
     1.24814329E-02    3     1000023       -14        14   # BR(~chi_30 -> ~chi_20 nu_mub  nu_mu)
     1.24814329E-02    3     1000023       -16        16   # BR(~chi_30 -> ~chi_20 nu_taub nu_tau)
     1.35451871E-01    3     1000024        -2         1   # BR(~chi_30 -> ~chi_1+ ub      d)
     1.35451871E-01    3    -1000024        -1         2   # BR(~chi_30 -> ~chi_1- db      u)
     1.35451871E-01    3     1000024        -4         3   # BR(~chi_30 -> ~chi_1+ cb      s)
     1.35451871E-01    3    -1000024        -3         4   # BR(~chi_30 -> ~chi_1- sb      c)
     4.51501546E-02    3     1000024       -12        11   # BR(~chi_30 -> ~chi_1+ nu_eb   e-)
     4.51501546E-02    3    -1000024        12       -11   # BR(~chi_30 -> ~chi_1- nu_e    e+)
     4.51501546E-02    3     1000024       -14        13   # BR(~chi_30 -> ~chi_1+ nu_mub  mu-)
     4.51501546E-02    3    -1000024        14       -13   # BR(~chi_30 -> ~chi_1- nu_mu   mu+)
     4.46735155E-02    3     1000024       -16        15   # BR(~chi_30 -> ~chi_1+ nu_taub tau-)
     4.46735155E-02    3    -1000024        16       -15   # BR(~chi_30 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000035     4.10074662E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     5.06114221E-04    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.95489993E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     7.14811652E-04    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.91865158E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.91865158E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     7.86652929E-02    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     1.19415273E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.15819750E-01    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.77092202E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     1.77092202E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     2.63557487E-08    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     2.63557487E-08    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     1.76736117E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     1.76736117E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     6.58224016E-09    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     6.58224016E-09    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     1.77092202E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     1.77092202E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     2.63557487E-08    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     2.63557487E-08    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     1.76736117E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     1.76736117E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     6.58224016E-09    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     6.58224016E-09    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.59676573E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.59676573E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
     9.77344728E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
     9.77344728E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     1.41085024E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     1.41085024E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.18047446E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.18047446E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     7.31961241E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     7.31961241E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.18047446E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.18047446E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     7.31961241E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     7.31961241E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.09558221E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.09558221E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.08362124E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.08362124E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.22717293E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.22717293E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.22717293E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.22717293E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.22717293E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.22717293E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     7.24705528E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.31375032E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     3.22713557E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     8.99850339E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     1.39122554E-12    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.22840149E-03   # h decays
#          BR         NDA      ID1       ID2
     6.02398310E-01    2           5        -5   # BR(h -> b       bb     )
     6.20390450E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.19591172E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.64619766E-04    2           3        -3   # BR(h -> s       sb     )
     2.00686058E-02    2           4        -4   # BR(h -> c       cb     )
     6.65827728E-02    2          21        21   # BR(h -> g       g      )
     2.30013298E-03    2          22        22   # BR(h -> gam     gam    )
     1.62247927E-03    2          22        23   # BR(h -> Z       gam    )
     2.16290154E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80142892E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     3.77701805E+01   # H decays
#          BR         NDA      ID1       ID2
     1.45678743E-03    2           5        -5   # BR(H -> b       bb     )
     2.46781109E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.72463099E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.11707917E-06    2           3        -3   # BR(H -> s       sb     )
     1.00690504E-05    2           4        -4   # BR(H -> c       cb     )
     9.96286046E-01    2           6        -6   # BR(H -> t       tb     )
     7.94571198E-04    2          21        21   # BR(H -> g       g      )
     2.68169926E-06    2          22        22   # BR(H -> gam     gam    )
     1.16010668E-06    2          23        22   # BR(H -> Z       gam    )
     2.55115498E-04    2          24       -24   # BR(H -> W+      W-     )
     1.27209694E-04    2          23        23   # BR(H -> Z       Z      )
     8.17589009E-04    2          25        25   # BR(H -> h       h      )
     1.07965934E-23    2          36        36   # BR(H -> A       A      )
     2.97358581E-11    2          23        36   # BR(H -> Z       A      )
     5.60433431E-12    2          24       -37   # BR(H -> W+      H-     )
     5.60433431E-12    2         -24        37   # BR(H -> W-      H+     )
#
#         PDG            Width
DECAY        36     3.82346405E+01   # A decays
#          BR         NDA      ID1       ID2
     1.45786185E-03    2           5        -5   # BR(A -> b       bb     )
     2.43920599E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.62347431E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.13688059E-06    2           3        -3   # BR(A -> s       sb     )
     9.96269927E-06    2           4        -4   # BR(A -> c       cb     )
     9.97089513E-01    2           6        -6   # BR(A -> t       tb     )
     9.43764167E-04    2          21        21   # BR(A -> g       g      )
     3.25655673E-06    2          22        22   # BR(A -> gam     gam    )
     1.35121298E-06    2          23        22   # BR(A -> Z       gam    )
     2.48370699E-04    2          23        25   # BR(A -> Z       h      )
#
#         PDG            Width
DECAY        37     3.74451199E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.33018121E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.49263831E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.81237686E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.48132642E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.45748124E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.08742381E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.99482001E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.54580407E-04    2          24        25   # BR(H+ -> W+      h      )
     3.77381360E-13    2          24        36   # BR(H+ -> W+      A      )
