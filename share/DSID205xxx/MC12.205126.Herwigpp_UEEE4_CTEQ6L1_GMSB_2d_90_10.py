## Herwig++ job option file for SUSY GMSB processes

## Setup Herwig++
include ( 'MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py' )

# define spectrum file name
slha_file = 'susy_gmsb_90_250_3_10_1_1_spheno.slha'

# Add Herwig++ parameters for this process
include ( 'MC12JobOptions/Herwigpp_SUSYConfig.py' )
cmds = buildHerwigppCommands(['all'], slha_file) 

## Define metadata
evgenConfig.description = 'H-shaped GMSB signal grid for 13 TeV production'
evgenConfig.keywords    = ['SUSY','GMSB','13TeV']
evgenConfig.contact     = ['oliver.ricken@cern.ch', 'bertrand.martindl@cern.ch']

# print checks
log.info('*** Begin Herwig++ commands ***')
log.info(cmds)
log.info('*** End Herwig++ commands ***')


# Set the command vector
topAlg.Herwigpp.Commands += cmds.splitlines() 

# clean up
del cmds

# dump Herwig++ configuration to file
topAlg.Herwigpp.InFileDump = 'herwigpp.config'
