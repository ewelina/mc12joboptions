#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.88000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.50000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05334567E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402201E+03   # H
        36     2.00000000E+03   # A
        37     2.00208935E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.25753876E+03   # ~d_L
   2000001     2.25729766E+03   # ~d_R
   1000002     2.25700025E+03   # ~u_L
   2000002     2.25712885E+03   # ~u_R
   1000003     2.25753876E+03   # ~s_L
   2000003     2.25729766E+03   # ~s_R
   1000004     2.25700025E+03   # ~c_L
   2000004     2.25712885E+03   # ~c_R
   1000005     2.25716448E+03   # ~b_1
   2000005     2.25767516E+03   # ~b_2
   1000006     2.29301200E+03   # ~t_1
   2000006     2.46969732E+03   # ~t_2
   1000011     2.50016690E+03   # ~e_L
   2000011     2.50015241E+03   # ~e_R
   1000012     2.49968066E+03   # ~nu_eL
   1000013     2.50016690E+03   # ~mu_L
   2000013     2.50015241E+03   # ~mu_R
   1000014     2.49968066E+03   # ~nu_muL
   1000015     2.50002679E+03   # ~tau_1
   2000015     2.50029378E+03   # ~tau_2
   1000016     2.49968066E+03   # ~nu_tauL
   1000021     2.31214313E+03   # ~g
   1000022     2.35714867E+02   # ~chi_10
   1000023    -2.67316405E+02   # ~chi_20
   1000025     3.30646472E+02   # ~chi_30
   1000035     2.55534896E+03   # ~chi_40
   1000024     2.64523987E+02   # ~chi_1+
   1000037     2.55534810E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     5.49989018E-01   # N_11
  1  2    -2.80144984E-02   # N_12
  1  3     5.97543723E-01   # N_13
  1  4    -5.82811090E-01   # N_14
  2  1     1.54967821E-02   # N_21
  2  2    -5.58604553E-03   # N_22
  2  3    -7.05538089E-01   # N_23
  2  4    -7.08480523E-01   # N_24
  3  1     8.35027824E-01   # N_31
  3  2     1.93705753E-02   # N_32
  3  3    -3.80460579E-01   # N_33
  3  4     3.96992521E-01   # N_34
  4  1     6.81142960E-04   # N_41
  4  2    -9.99404205E-01   # N_42
  4  3    -2.01804835E-02   # N_43
  4  4     2.79914155E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.85247378E-02   # U_11
  1  2     9.99593087E-01   # U_12
  2  1     9.99593087E-01   # U_21
  2  2     2.85247378E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.95772618E-02   # V_11
  1  2     9.99216513E-01   # V_12
  2  1     9.99216513E-01   # V_21
  2  2     3.95772618E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.96369624E-01   # cos(theta_t)
  1  2    -8.51326751E-02   # sin(theta_t)
  2  1     8.51326751E-02   # -sin(theta_t)
  2  2     9.96369624E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     5.13754579E-01   # cos(theta_b)
  1  2     8.57937196E-01   # sin(theta_b)
  2  1    -8.57937196E-01   # -sin(theta_b)
  2  2     5.13754579E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.87648001E-01   # cos(theta_tau)
  1  2     7.26044232E-01   # sin(theta_tau)
  2  1    -7.26044232E-01   # -sin(theta_tau)
  2  2     6.87648001E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89945310E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.50000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51712660E+02   # vev(Q)              
         4     3.86322823E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53679595E-01   # gprime(Q) DRbar
     2     6.31710251E-01   # g(Q) DRbar
     3     1.08794888E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.04266529E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74498781E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80031545E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.88000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.86484610E+06   # M^2_Hd              
        22     6.83668549E+04   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39070619E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.37754912E-01   # gluino decays
#          BR         NDA      ID1       ID2
     4.95431762E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.95431762E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99763737E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99763737E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.05132800E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.05132800E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.02807785E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.02807785E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.95431762E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.95431762E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99763737E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99763737E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.05132800E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.05132800E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.02807785E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.02807785E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.02275635E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.02275635E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.90936646E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     4.90936646E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.03110547E-04    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     3.47941140E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.05127392E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.86803236E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.91470715E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.65986574E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.57342258E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     1.93575386E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.37719562E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     1.04158078E-01    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.65021253E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -4.80592100E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -4.51673899E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
     1.98654198E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
     3.16141024E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005    -8.51527486E+00   # sbottom1 decays
#          BR         NDA      ID1       ID2
    -5.04242744E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -1.20677558E-03    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
    -1.02820766E-01    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.15445182E+00    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     4.35310710E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.99430245E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     3.22270105E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
     2.01171607E-03    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.95671711E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
#
#         PDG            Width
DECAY   1000002     3.26872939E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     1.43674016E-01    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.86096832E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     7.75763798E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     8.03760895E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
#
#         PDG            Width
DECAY   2000002     4.63249439E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     3.06856820E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     2.42152680E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     6.92901028E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.12102889E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     4.61329699E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     1.93076929E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     4.93002575E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.37369575E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
#
#         PDG            Width
DECAY   2000001     1.15819937E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     3.06856120E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     2.42152362E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     6.92901728E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
#
#         PDG            Width
DECAY   1000004     3.26872939E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     1.43674016E-01    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.86096832E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     7.75763798E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     8.03760895E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
#
#         PDG            Width
DECAY   2000004     4.63249439E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     3.06856820E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     2.42152680E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     6.92901028E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.12102889E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     4.61329699E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     1.93076929E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     4.93002575E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.37369575E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
#
#         PDG            Width
DECAY   2000003     1.15819937E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     3.06856120E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     2.42152362E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     6.92901728E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
#
#         PDG            Width
DECAY   1000011     3.05075935E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     2.50370326E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     3.03594210E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     7.44423337E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     5.17597771E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.20760693E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     3.06177927E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     2.41833955E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     6.93580239E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.05075935E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     2.50370326E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     3.03594210E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     7.44423337E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     5.17597771E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.20760693E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     3.06177927E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     2.41833955E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     6.93580239E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.85799225E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     3.04751703E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     6.99692596E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     6.90523873E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     4.01042213E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.43086323E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.31618539E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     2.83998288E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.80406437E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     7.14194315E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     3.33275855E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.05596886E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     3.59946298E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     6.45448724E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     6.29463104E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.94514895E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.05596886E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     3.59946298E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     6.45448724E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     6.29463104E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.94514895E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.07170839E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     3.58101923E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     6.42141426E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     6.26237716E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.50182193E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     1.44956051E-05   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.52553552E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34017033E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34017033E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11339448E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11339448E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.09251783E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.43526838E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.27526611E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.26434462E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     4.27526611E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.26434462E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.58182908E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.44397227E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
    -4.66177279E-04    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     4.83809986E-02    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     5.47510564E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     5.47510564E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     5.47209270E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     5.38496783E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     5.38496783E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.55474695E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.83009080E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.75087597E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.13206396E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.71193232E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     5.77806361E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.73992051E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.01617057E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.03516272E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     8.01831102E-03    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.01203840E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     2.28695091E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     3.63550735E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.24291644E-11    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     3.68208814E-10   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.94523814E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     4.98646376E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     6.82981042E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     1.85095582E-05   # neutralino2 decays
#          BR         NDA      ID1       ID2
     2.02637732E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     9.88986703E-09    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     1.02398737E-06    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     1.49610663E-05    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.20528122E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.55938183E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.20528122E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.55938183E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.26121848E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.55568763E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.55568763E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.48601272E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.09702175E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.09702175E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.09702175E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     3.44331458E-06    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     3.44331458E-06    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     3.44331458E-06    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     3.44331458E-06    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     1.14777251E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     1.14777251E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     1.14777251E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     1.14777251E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     1.36339114E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     1.36339114E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     1.66517121E-05   # neutralino3 decays
#          BR         NDA      ID1       ID2
     9.99790552E-01    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     1.48815766E-04    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     5.96132229E-05    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.01936798E-06    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.43460744E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     2.68968152E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.70909822E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.70962164E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.70896862E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.70896862E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.12663946E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     8.76189941E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     7.16665907E-03    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     4.45915944E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     8.90229147E-03    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.15024128E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     5.71774640E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     2.75091432E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     2.02442720E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.02759564E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.02759564E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.13801479E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.13801479E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.53148735E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.53148735E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.13219736E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.13219736E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.38147114E-09    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.38147114E-09    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.13801479E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.13801479E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.53148735E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.53148735E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.13219736E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.13219736E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.38147114E-09    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.38147114E-09    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.79844438E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.79844438E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
    -9.27051965E-04    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
    -9.27051965E-04    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     2.97235471E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     2.97235471E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.69001241E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.69001241E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.56873540E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.56873540E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.69001241E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.69001241E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.56873540E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.56873540E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.27555357E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.27555357E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.41289000E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.41289000E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.74128202E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.74128202E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.74128202E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.74128202E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.74128202E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.74128202E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     8.68460310E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.76745669E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.61861968E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.19471932E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     6.57656838E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21916161E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01417578E-01    2           5        -5   # BR(h -> b       bb     )
     6.21792376E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20087393E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65669080E-04    2           3        -3   # BR(h -> s       sb     )
     2.01119282E-02    2           4        -4   # BR(h -> c       cb     )
     6.67237339E-02    2          21        21   # BR(h -> g       g      )
     2.31792958E-03    2          22        22   # BR(h -> gam     gam    )
     1.62585531E-03    2          22        23   # BR(h -> Z       gam    )
     2.16862343E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80756375E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.00814647E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38788675E-03    2           5        -5   # BR(H -> b       bb     )
     2.32544332E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.22130790E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05263372E-06    2           3        -3   # BR(H -> s       sb     )
     9.48911860E-06    2           4        -4   # BR(H -> c       cb     )
     9.38904705E-01    2           6        -6   # BR(H -> t       tb     )
     7.48890263E-04    2          21        21   # BR(H -> g       g      )
     2.63449708E-06    2          22        22   # BR(H -> gam     gam    )
     1.09234001E-06    2          23        22   # BR(H -> Z       gam    )
     2.46280311E-04    2          24       -24   # BR(H -> W+      W-     )
     1.22804064E-04    2          23        23   # BR(H -> Z       Z      )
     7.76446139E-04    2          25        25   # BR(H -> h       h      )
     1.07311831E-23    2          36        36   # BR(H -> A       A      )
     2.83295021E-11    2          23        36   # BR(H -> Z       A      )
     1.76903215E-12    2          24       -37   # BR(H -> W+      H-     )
     1.76903215E-12    2         -24        37   # BR(H -> W-      H+     )
     1.10613990E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     1.18915250E-03    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     6.99171908E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     5.84932261E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.99786380E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     3.40116079E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     3.52919821E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.05807472E+01   # A decays
#          BR         NDA      ID1       ID2
     1.38858101E-03    2           5        -5   # BR(A -> b       bb     )
     2.29818745E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12492284E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07115378E-06    2           3        -3   # BR(A -> s       sb     )
     9.38672280E-06    2           4        -4   # BR(A -> c       cb     )
     9.39444482E-01    2           6        -6   # BR(A -> t       tb     )
     8.89202050E-04    2          21        21   # BR(A -> g       g      )
     2.59405274E-06    2          22        22   # BR(A -> gam     gam    )
     1.27209774E-06    2          23        22   # BR(A -> Z       gam    )
     2.39727902E-04    2          23        25   # BR(A -> Z       h      )
     9.60730183E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.81283396E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     2.90622850E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     2.05223828E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     4.90434934E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     6.22169010E-03    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.46656739E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97621789E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.22690241E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34794962E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.30085007E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41580810E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.14052671E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02426932E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.41458919E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.45781027E-04    2          24        25   # BR(H+ -> W+      h      )
     1.31653426E-12    2          24        36   # BR(H+ -> W+      A      )
     1.74960430E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     2.09067294E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.03420289E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
