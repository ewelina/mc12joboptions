#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_WW_Common.py')

# Override defaults
PowhegConfig.vdecaymodeWm = 13
PowhegConfig.vdecaymodeWp = -15

if evgenConfig.minevents > 0:
    PowhegConfig.nEvents = 7 * evgenConfig.minevents
else:
    PowhegConfig.nEvents = 7 * 5000

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WW production with CT10 (ME) and CTEQ6L1+A14 (shower)'
evgenConfig.keywords    = [ 'SM', 'ww' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]
evgenConfig.generators += [ 'Pythia8' ]

#--------------------------------------------------------------
# Pythia8 showering with main31 and CT10
#--------------------------------------------------------------
#include('MC12JobOptions/Pythia8_AU2_CT10_Common.py')
include('MC12JobOptions/Pythia8_A14_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_Powheg_Main31.py')

#from Pythia8_i.Pythia8_iConf import Pythia8_i
#topAlg += Pythia8_i("Pythia8")
topAlg.Pythia8.UserModes += ['Main31:pTHard = 0']

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter("Multi2LLeptonFilter")

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.MinPt = 10000.
Multi2LLeptonFilter.MaxEta = 5.0
Multi2LLeptonFilter.NLeptons = 2
Multi2LLeptonFilter.IncludeHadTaus = 0

StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
