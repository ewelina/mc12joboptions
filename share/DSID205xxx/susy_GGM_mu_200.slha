#
#                              ======================
#                              | THE SUSYHIT OUTPUT |
#                              ======================
#
#
#              ------------------------------------------------------
#              |     This is the output of the SUSY-HIT package     |
#              |  created by A.Djouadi, M.Muehlleitner and M.Spira. |
#              |  In case of problems with SUSY-HIT email to        |
#              |           margarete.muehlleitner@kit.edu           |
#              |           michael.spira@psi.ch                     |
#              |           abdelhak.djouadi@cern.ch                 |
#              ------------------------------------------------------
#
#              ------------------------------------------------------
#              |  SUSY Les Houches Accord - MSSM Spectrum + Decays  |
#              |              based on the decay programs           |
#              |                                                    |
#              |                     SDECAY 1.4                     |
#              |                                                    |
#              |  Authors: M.Muhlleitner, A.Djouadi and Y.Mambrini  |
#              |  Ref.:    Comput.Phys.Commun.168(2005)46           |
#              |           [hep-ph/0311167]                         |
#              |                                                    |
#              |                     HDECAY 3.4                     |
#              |                                                    |
#              |  By: A.Djouadi,J.Kalinowski,M.Muhlleitner,M.Spira  |
#              |  Ref.:    Comput.Phys.Commun.108(1998)56           |
#              |           [hep-ph/9704448]                         |
#              |                                                    |
#              |                                                    |
#              |  If not stated otherwise all DRbar couplings and   |
#              |  soft SUSY breaking masses are given at the scale  |
#              |  Q=  0.91187600E+02
#              |                                                    |
#              ------------------------------------------------------
#
#
BLOCK DCINFO  # Decay Program information
     1   SDECAY/HDECAY # decay calculator
     2   1.4  /3.4    # version number
#
BLOCK SPINFO  # Spectrum calculator information
     1   SuSpect     # RGE +Spectrum calculator            
     2   2.43         # version number                     
#
BLOCK MODSEL  # Model selection
     1     2   # GMSB                                              
#
BLOCK SMINPUTS  # Standard Model inputs
         1     1.27932904E+02   # alpha_em^-1(M_Z)^MSbar
         2     1.16639000E-05   # G_F [GeV^-2]
         3     1.17200000E-01   # alpha_S(M_Z)^MSbar
         4     9.11870000E+01   # M_Z pole mass
         5     4.25000000E+00   # mb(mb)^MSbar
         6     1.72900000E+02   # mt pole mass
         7     1.77700000E+00   # mtau pole mass
#
BLOCK MINPAR  # Input parameters - minimal models
#
BLOCK EXTPAR  # Input parameters - non-minimal models
         0     9.11876000E+01   # EWSB                
         1     2.67000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        11     0.00000000E+00   # A_t                 
        12     0.00000000E+00   # A_b                 
        13     0.00000000E+00   # A_tau               
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        23     2.00000000E+02   # mu(EWSB)            
        25     1.50000000E+00   # tanbeta(in)         
        26     2.00000000E+03   # MA_pole             
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
BLOCK MASS  # Mass Spectrum
# PDG code           mass       particle
        24     8.05332685E+01   # W+
        25     1.26000000E+02   # h
        35     2.00402169E+03   # H
        36     2.00000000E+03   # A
        37     2.00209348E+03   # H+
         5     4.87877839E+00   # b-quark pole mass calculated from mb(mb)_Msbar
   1000001     2.25753915E+03   # ~d_L
   2000001     2.25729788E+03   # ~d_R
   1000002     2.25700030E+03   # ~u_L
   2000002     2.25712907E+03   # ~u_R
   1000003     2.25753915E+03   # ~s_L
   2000003     2.25729788E+03   # ~s_R
   1000004     2.25700030E+03   # ~c_L
   2000004     2.25712907E+03   # ~c_R
   1000005     2.25720336E+03   # ~b_1
   2000005     2.25763690E+03   # ~b_2
   1000006     2.29331838E+03   # ~t_1
   2000006     2.47010807E+03   # ~t_2
   1000011     2.50016705E+03   # ~e_L
   2000011     2.50015241E+03   # ~e_R
   1000012     2.49968051E+03   # ~nu_eL
   1000013     2.50016705E+03   # ~mu_L
   2000013     2.50015241E+03   # ~mu_R
   1000014     2.49968051E+03   # ~nu_muL
   1000015     2.50005348E+03   # ~tau_1
   2000015     2.50026724E+03   # ~tau_2
   1000016     2.49968051E+03   # ~nu_tauL
   1000021     2.31214843E+03   # ~g
   1000022     1.90231621E+02   # ~chi_10
   1000023    -2.14279846E+02   # ~chi_20
   1000025     3.01105558E+02   # ~chi_30
   1000035     2.55528566E+03   # ~chi_40
   1000024     2.11524415E+02   # ~chi_1+
   1000037     2.55528484E+03   # ~chi_2+
   1000039     1.00000000E-09   # ~gravitino
#
BLOCK NMIX  # Neutralino Mixing Matrix
  1  1     4.37245153E-01   # N_11
  1  2    -2.96004741E-02   # N_12
  1  3     6.43026430E-01   # N_13
  1  4    -6.28058516E-01   # N_14
  2  1     1.79208182E-02   # N_21
  2  2    -5.71681986E-03   # N_22
  2  3    -7.04951372E-01   # N_23
  2  4    -7.09006153E-01   # N_24
  3  1     8.99163547E-01   # N_31
  3  2     1.52420932E-02   # N_32
  3  3    -2.98626388E-01   # N_33
  3  4     3.19522886E-01   # N_34
  4  1     6.60397557E-04   # N_41
  4  2    -9.99429241E-01   # N_42
  4  3    -1.95666662E-02   # N_43
  4  4     2.75300009E-02   # N_44
#
BLOCK UMIX  # Chargino Mixing Matrix U
  1  1    -2.76572049E-02   # U_11
  1  2     9.99617466E-01   # U_12
  2  1     9.99617466E-01   # U_21
  2  2     2.76572049E-02   # U_22
#
BLOCK VMIX  # Chargino Mixing Matrix V
  1  1    -3.89254767E-02   # V_11
  1  2     9.99242116E-01   # V_12
  2  1     9.99242116E-01   # V_21
  2  2     3.89254767E-02   # V_22
#
BLOCK STOPMIX  # Stop Mixing Matrix
  1  1     9.95974752E-01   # cos(theta_t)
  1  2    -8.96342199E-02   # sin(theta_t)
  2  1     8.96342199E-02   # -sin(theta_t)
  2  2     9.95974752E-01   # cos(theta_t)
#
BLOCK SBOTMIX  # Sbottom Mixing Matrix
  1  1     4.70909281E-01   # cos(theta_b)
  1  2     8.82181642E-01   # sin(theta_b)
  2  1    -8.82181642E-01   # -sin(theta_b)
  2  2     4.70909281E-01   # cos(theta_b)
#
BLOCK STAUMIX  # Stau Mixing Matrix
  1  1     6.82464426E-01   # cos(theta_tau)
  1  2     7.30918810E-01   # sin(theta_tau)
  2  1    -7.30918810E-01   # -sin(theta_tau)
  2  2     6.82464426E-01   # cos(theta_tau)
#
BLOCK ALPHA  # Higgs mixing
          -5.89946596E-01   # Mixing angle in the neutral Higgs boson sector
#
BLOCK HMIX Q=  9.11876000E+01  # DRbar Higgs Parameters
         1     2.00000000E+02   # mu(Q)               
         2     1.49999979E+00   # tanbeta(Q)          
         3     2.51687029E+02   # vev(Q)              
         4     3.87911897E+06   # MA^2(Q)             
#
BLOCK GAUGE Q=  9.11876000E+01  # The gauge couplings
     1     3.53718101E-01   # gprime(Q) DRbar
     2     6.31969940E-01   # g(Q) DRbar
     3     1.08794842E+00   # g3(Q) DRbar
#
BLOCK AU Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_u(Q) DRbar
  2  2     0.00000000E+00   # A_c(Q) DRbar
  3  3     0.00000000E+00   # A_t(Q) DRbar
#
BLOCK AD Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_d(Q) DRbar
  2  2     0.00000000E+00   # A_s(Q) DRbar
  3  3     0.00000000E+00   # A_b(Q) DRbar
#
BLOCK AE Q=  9.11876000E+01  # The trilinear couplings
  1  1     0.00000000E+00   # A_e(Q) DRbar
  2  2     0.00000000E+00   # A_mu(Q) DRbar
  3  3     0.00000000E+00   # A_tau(Q) DRbar
#
BLOCK Yu Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_u(Q) DRbar
  2  2     0.00000000E+00   # y_c(Q) DRbar
  3  3     1.04285447E+00   # y_t(Q) DRbar
#
BLOCK Yd Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_d(Q) DRbar
  2  2     0.00000000E+00   # y_s(Q) DRbar
  3  3     2.74578696E-02   # y_b(Q) DRbar
#
BLOCK Ye Q=  9.11876000E+01  # The Yukawa couplings
  1  1     0.00000000E+00   # y_e(Q) DRbar
  2  2     0.00000000E+00   # y_mu(Q) DRbar
  3  3     1.80036491E-02   # y_tau(Q) DRbar
#
BLOCK MSOFT Q=  9.11876000E+01  # The soft SUSY breaking masses at the scale Q
         1     2.67000000E+02   # M_1                 
         2     2.50000000E+03   # M_2                 
         3     2.50000000E+03   # M_3                 
        14     0.00000000E+00   # A_u                 
        15     0.00000000E+00   # A_d                 
        16     0.00000000E+00   # A_e                 
        21     2.88988887E+06   # M^2_Hd              
        22     8.93393283E+04   # M^2_Hu              
        31     2.50000000E+03   # M_eL                
        32     2.50000000E+03   # M_muL               
        33     2.50000000E+03   # M_tauL              
        34     2.50000000E+03   # M_eR                
        35     2.50000000E+03   # M_muR               
        36     2.50000000E+03   # M_tauR              
        41     2.50000000E+03   # M_q1L               
        42     2.50000000E+03   # M_q2L               
        43     2.50000000E+03   # M_q3L               
        44     2.50000000E+03   # M_uR                
        45     2.50000000E+03   # M_cR                
        46     2.50000000E+03   # M_tR                
        47     2.50000000E+03   # M_dR                
        48     2.50000000E+03   # M_sR                
        49     2.50000000E+03   # M_bR                
#
#
#
#                             =================
#                             |The decay table|
#                             =================
#
# - The QCD corrections to the decays gluino -> squark  + quark
#                                     squark -> gaugino + quark_prime
#                                     squark -> squark_prime + Higgs
#                                     squark -> gluino  + quark
#   are included.
#
# - The multi-body decays for the inos, stops and sbottoms are included.
#
# - The loop induced decays for the gluino, neutralinos and stops
#   are included.
#
# - The SUSY decays of the top quark are included.
#
# - Possible decays of the NLSP in GMSB models are included.
#
#
#         PDG            Width
DECAY         6     1.39185798E+00   # top decays
#          BR         NDA      ID1       ID2
     1.00000000E+00    2           5        24   # BR(t ->  b    W+)
#
#         PDG            Width
DECAY   1000021     7.37907733E-01   # gluino decays
#          BR         NDA      ID1       ID2
     4.95415067E-02    2     1000001        -1   # BR(~g -> ~d_L  db)
     4.95415067E-02    2    -1000001         1   # BR(~g -> ~d_L* d )
     4.99749562E-02    2     2000001        -1   # BR(~g -> ~d_R  db)
     4.99749562E-02    2    -2000001         1   # BR(~g -> ~d_R* d )
     5.05121044E-02    2     1000002        -2   # BR(~g -> ~u_L  ub)
     5.05121044E-02    2    -1000002         2   # BR(~g -> ~u_L* u )
     5.02793238E-02    2     2000002        -2   # BR(~g -> ~u_R  ub)
     5.02793238E-02    2    -2000002         2   # BR(~g -> ~u_R* u )
     4.95415067E-02    2     1000003        -3   # BR(~g -> ~s_L  sb)
     4.95415067E-02    2    -1000003         3   # BR(~g -> ~s_L* s )
     4.99749562E-02    2     2000003        -3   # BR(~g -> ~s_R  sb)
     4.99749562E-02    2    -2000003         3   # BR(~g -> ~s_R* s )
     5.05121044E-02    2     1000004        -4   # BR(~g -> ~c_L  cb)
     5.05121044E-02    2    -1000004         4   # BR(~g -> ~c_L* c )
     5.02793238E-02    2     2000004        -4   # BR(~g -> ~c_R  cb)
     5.02793238E-02    2    -2000004         4   # BR(~g -> ~c_R* c )
     5.22531197E-02    2     1000005        -5   # BR(~g -> ~b_1  bb)
     5.22531197E-02    2    -1000005         5   # BR(~g -> ~b_1* b )
     4.70795530E-02    2     2000005        -5   # BR(~g -> ~b_2  bb)
     4.70795530E-02    2    -2000005         5   # BR(~g -> ~b_2* b )
     1.03090376E-04    2     1000039        21   # BR(~g -> ~G g)
#
#         PDG            Width
DECAY   1000006     3.52198638E+01   # stop1 decays
#          BR         NDA      ID1       ID2
     3.59907046E-01    2     1000022         6   # BR(~t_1 -> ~chi_10 t )
     4.87080698E-01    2     1000023         6   # BR(~t_1 -> ~chi_20 t )
     1.34987600E-01    2     1000025         6   # BR(~t_1 -> ~chi_30 t )
     1.80246557E-02    2     1000024         5   # BR(~t_1 -> ~chi_1+ b )
#
#         PDG            Width
DECAY   2000006     7.62773841E+01   # stop2 decays
#          BR         NDA      ID1       ID2
     2.10889604E-01    2     1000022         6   # BR(~t_2 -> ~chi_10 t )
     2.38230532E-01    2     1000023         6   # BR(~t_2 -> ~chi_20 t )
     8.65920341E-02    2     1000025         6   # BR(~t_2 -> ~chi_30 t )
     4.64845523E-01    2     1000024         5   # BR(~t_2 -> ~chi_1+ b )
    -3.92977700E-04    2     1000006        25   # BR(~t_2 -> ~t_1    h )
    -6.53034093E-05    2     1000006        23   # BR(~t_2 -> ~t_1    Z )
    -1.59592214E-05    2     1000005        24   # BR(~t_2 -> ~b_1    W+)
    -8.34529196E-05    2     2000005        24   # BR(~t_2 -> ~b_2    W+)
#
#         PDG            Width
DECAY   1000005    -1.54412106E+01   # sbottom1 decays
#          BR         NDA      ID1       ID2
    -1.77968613E-02    2     1000022         5   # BR(~b_1 -> ~chi_10 b )
    -7.82081748E-04    2     1000023         5   # BR(~b_1 -> ~chi_20 b )
    -7.59782020E-02    2     1000025         5   # BR(~b_1 -> ~chi_30 b )
     1.09455715E+00    2    -1000024         6   # BR(~b_1 -> ~chi_1- t )
#
#         PDG            Width
DECAY   2000005     5.08268657E+01   # sbottom2 decays
#          BR         NDA      ID1       ID2
     1.50070781E-03    2     1000022         5   # BR(~b_2 -> ~chi_10 b )
     2.48596991E-04    2     1000023         5   # BR(~b_2 -> ~chi_20 b )
    -7.44274575E-04    2     1000025         5   # BR(~b_2 -> ~chi_30 b )
     9.98994970E-01    2    -1000024         6   # BR(~b_2 -> ~chi_1- t )
#
#         PDG            Width
DECAY   1000002     3.27553813E-01   # sup_L decays
#          BR         NDA      ID1       ID2
     7.01114747E-02    2     1000022         2   # BR(~u_L -> ~chi_10 u)
     1.45657841E-04    2     1000023         2   # BR(~u_L -> ~chi_20 u)
     8.51345952E-01    2     1000025         2   # BR(~u_L -> ~chi_30 u)
     7.83969158E-02    2     1000024         1   # BR(~u_L -> ~chi_1+ d)
#
#         PDG            Width
DECAY   2000002     4.65637182E+00   # sup_R decays
#          BR         NDA      ID1       ID2
     1.94396814E-01    2     1000022         2   # BR(~u_R -> ~chi_10 u)
     3.25372604E-04    2     1000023         2   # BR(~u_R -> ~chi_20 u)
     8.05277813E-01    2     1000025         2   # BR(~u_R -> ~chi_30 u)
#
#         PDG            Width
DECAY   1000001     3.12490414E-01   # sdown_L decays
#          BR         NDA      ID1       ID2
     3.36314262E-01    2     1000022         1   # BR(~d_L -> ~chi_10 d)
     2.22549819E-03    2     1000023         1   # BR(~d_L -> ~chi_20 d)
     6.19966722E-01    2     1000025         1   # BR(~d_L -> ~chi_30 d)
     4.14935172E-02    2    -1000024         2   # BR(~d_L -> ~chi_1- u)
#
#         PDG            Width
DECAY   2000001     1.16416820E+00   # sdown_R decays
#          BR         NDA      ID1       ID2
     1.94396296E-01    2     1000022         1   # BR(~d_R -> ~chi_10 d)
     3.25371927E-04    2     1000023         1   # BR(~d_R -> ~chi_20 d)
     8.05278332E-01    2     1000025         1   # BR(~d_R -> ~chi_30 d)
#
#         PDG            Width
DECAY   1000004     3.27553813E-01   # scharm_L decays
#          BR         NDA      ID1       ID2
     7.01114747E-02    2     1000022         4   # BR(~c_L -> ~chi_10 c)
     1.45657841E-04    2     1000023         4   # BR(~c_L -> ~chi_20 c)
     8.51345952E-01    2     1000025         4   # BR(~c_L -> ~chi_30 c)
     7.83969158E-02    2     1000024         3   # BR(~c_L -> ~chi_1+ s)
#
#         PDG            Width
DECAY   2000004     4.65637182E+00   # scharm_R decays
#          BR         NDA      ID1       ID2
     1.94396814E-01    2     1000022         4   # BR(~c_R -> ~chi_10 c)
     3.25372604E-04    2     1000023         4   # BR(~c_R -> ~chi_20 c)
     8.05277813E-01    2     1000025         4   # BR(~c_R -> ~chi_30 c)
#
#         PDG            Width
DECAY   1000003     3.12490414E-01   # sstrange_L decays
#          BR         NDA      ID1       ID2
     3.36314262E-01    2     1000022         3   # BR(~s_L -> ~chi_10 s)
     2.22549819E-03    2     1000023         3   # BR(~s_L -> ~chi_20 s)
     6.19966722E-01    2     1000025         3   # BR(~s_L -> ~chi_30 s)
     4.14935172E-02    2    -1000024         4   # BR(~s_L -> ~chi_1- c)
#
#         PDG            Width
DECAY   2000003     1.16416820E+00   # sstrange_R decays
#          BR         NDA      ID1       ID2
     1.94396296E-01    2     1000022         3   # BR(~s_R -> ~chi_10 s)
     3.25371927E-04    2     1000023         3   # BR(~s_R -> ~chi_20 s)
     8.05278332E-01    2     1000025         3   # BR(~s_R -> ~chi_30 s)
#
#         PDG            Width
DECAY   1000011     3.06303740E+00   # selectron_L decays
#          BR         NDA      ID1       ID2
     1.48341803E-01    2     1000022        11   # BR(~e_L -> ~chi_10 e-)
     5.94543608E-05    2     1000023        11   # BR(~e_L -> ~chi_20 e-)
     8.46708654E-01    2     1000025        11   # BR(~e_L -> ~chi_30 e-)
     4.89008825E-03    2    -1000024        12   # BR(~e_L -> ~chi_1- nu_e)
#
#         PDG            Width
DECAY   2000011     1.21289946E+01   # selectron_R decays
#          BR         NDA      ID1       ID2
     1.93920262E-01    2     1000022        11   # BR(~e_R -> ~chi_10 e-)
     3.24734302E-04    2     1000023        11   # BR(~e_R -> ~chi_20 e-)
     8.05755004E-01    2     1000025        11   # BR(~e_R -> ~chi_30 e-)
#
#         PDG            Width
DECAY   1000013     3.06303740E+00   # smuon_L decays
#          BR         NDA      ID1       ID2
     1.48341803E-01    2     1000022        13   # BR(~mu_L -> ~chi_10 mu-)
     5.94543608E-05    2     1000023        13   # BR(~mu_L -> ~chi_20 mu-)
     8.46708654E-01    2     1000025        13   # BR(~mu_L -> ~chi_30 mu-)
     4.89008825E-03    2    -1000024        14   # BR(~mu_L -> ~chi_1- nu_mu)
#
#         PDG            Width
DECAY   2000013     1.21289946E+01   # smuon_R decays
#          BR         NDA      ID1       ID2
     1.93920262E-01    2     1000022        13   # BR(~mu_R -> ~chi_10 mu-)
     3.24734302E-04    2     1000023        13   # BR(~mu_R -> ~chi_20 mu-)
     8.05755004E-01    2     1000025        13   # BR(~mu_R -> ~chi_30 mu-)
#
#         PDG            Width
DECAY   1000015     7.95569422E+00   # stau_1 decays
#          BR         NDA      ID1       ID2
     1.94129539E-01    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
     7.18575435E-04    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
     8.01260646E-01    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
     3.87710660E-03    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
     1.41336658E-05    2     1000039        15   # BR(~tau_1 -> ~G       tau-)
#
#         PDG            Width
DECAY   2000015     7.28408030E+00   # stau_2 decays
#          BR         NDA      ID1       ID2
     1.75060407E-01    2     1000022        15   # BR(~tau_2 -> ~chi_10  tau-)
     1.94844920E-03    2     1000023        15   # BR(~tau_2 -> ~chi_20  tau-)
     8.22989506E-01    2     1000025        15   # BR(~tau_2 -> ~chi_30  tau-)
     1.63758153E-06    2    -1000024        16   # BR(~tau_2 -> ~chi_1-  nu_tau)
#
#         PDG            Width
DECAY   1000012     3.06784559E+00   # snu_eL decays
#          BR         NDA      ID1       ID2
     2.40793407E-01    2     1000022        12   # BR(~nu_eL -> ~chi_10 nu_e)
     7.90944534E-04    2     1000023        12   # BR(~nu_eL -> ~chi_20 nu_e)
     7.48746241E-01    2     1000025        12   # BR(~nu_eL -> ~chi_30 nu_e)
     9.66940707E-03    2     1000024        11   # BR(~nu_eL -> ~chi_1+ e-)
#
#         PDG            Width
DECAY   1000014     3.06784559E+00   # snu_muL decays
#          BR         NDA      ID1       ID2
     2.40793407E-01    2     1000022        14   # BR(~nu_muL -> ~chi_10 nu_mu)
     7.90944534E-04    2     1000023        14   # BR(~nu_muL -> ~chi_20 nu_mu)
     7.48746241E-01    2     1000025        14   # BR(~nu_muL -> ~chi_30 nu_mu)
     9.66940707E-03    2     1000024        13   # BR(~nu_muL -> ~chi_1+ mu-)
#
#         PDG            Width
DECAY   1000016     3.08371702E+00   # snu_tauL decays
#          BR         NDA      ID1       ID2
     2.39554079E-01    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
     7.86873660E-04    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
     7.44892555E-01    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
     1.47664920E-02    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#
#         PDG            Width
DECAY   1000024     3.62188864E-06   # chargino1+ decays
#          BR         NDA      ID1       ID2
     3.66181117E-05    2     1000039        24   # BR(~chi_1+ -> ~G       W+)
#           BR         NDA      ID1       ID2       ID3
     3.34580577E-01    3     1000022         2        -1   # BR(~chi_1+ -> ~chi_10 u    db)
     3.34580577E-01    3     1000022         4        -3   # BR(~chi_1+ -> ~chi_10 c    sb)
     1.11527193E-01    3     1000022       -11        12   # BR(~chi_1+ -> ~chi_10 e+   nu_e)
     1.11527193E-01    3     1000022       -13        14   # BR(~chi_1+ -> ~chi_10 mu+  nu_mu)
     1.07747841E-01    3     1000022       -15        16   # BR(~chi_1+ -> ~chi_10 tau+ nu_tau)
#
#         PDG            Width
DECAY   1000037     3.39658371E+01   # chargino2+ decays
#          BR         NDA      ID1       ID2
     4.32614265E-02    2     1000002        -1   # BR(~chi_2+ -> ~u_L     db)
     4.31506830E-02    2    -1000001         2   # BR(~chi_2+ -> ~d_L*    u )
     4.32614265E-02    2     1000004        -3   # BR(~chi_2+ -> ~c_L     sb)
     4.31506830E-02    2    -1000003         4   # BR(~chi_2+ -> ~s_L*    c )
     4.62143848E-02    2     1000006        -5   # BR(~chi_2+ -> ~t_1     bb)
     1.91167658E-05    2     2000006        -5   # BR(~chi_2+ -> ~t_2     bb)
    -6.41270185E-03    2    -1000005         6   # BR(~chi_2+ -> ~b_1*    t )
     5.49414936E-02    2    -2000005         6   # BR(~chi_2+ -> ~b_2*    t )
     5.53001320E-04    2     1000012       -11   # BR(~chi_2+ -> ~nu_eL   e+  )
     5.53001320E-04    2     1000014       -13   # BR(~chi_2+ -> ~nu_muL  mu+ )
     5.52697212E-04    2     1000016       -15   # BR(~chi_2+ -> ~nu_tau1 tau+)
     5.43879067E-04    2    -1000011        12   # BR(~chi_2+ -> ~e_L+    nu_e)
     5.43879067E-04    2    -1000013        14   # BR(~chi_2+ -> ~mu_L+   nu_mu)
     2.53920174E-04    2    -1000015        16   # BR(~chi_2+ -> ~tau_1+  nu_tau)
     2.89945605E-04    2    -2000015        16   # BR(~chi_2+ -> ~tau_2+  nu_tau)
     1.71782850E-01    2     1000024        23   # BR(~chi_2+ -> ~chi_1+  Z )
     1.30308359E-01    2     1000022        24   # BR(~chi_2+ -> ~chi_10  W+)
     1.68073330E-01    2     1000023        24   # BR(~chi_2+ -> ~chi_20  W+)
     3.73999077E-02    2     1000025        24   # BR(~chi_2+ -> ~chi_30  W+)
     1.70621631E-01    2     1000024        25   # BR(~chi_2+ -> ~chi_1+  h )
     1.26600894E-02    2     1000024        35   # BR(~chi_2+ -> ~chi_1+  H )
     1.28563237E-02    2     1000024        36   # BR(~chi_2+ -> ~chi_1+  A )
     1.11057429E-02    2     1000022        37   # BR(~chi_2+ -> ~chi_10  H+)
     1.26036807E-02    2     1000023        37   # BR(~chi_2+ -> ~chi_20  H+)
     1.70757289E-03    2     1000025        37   # BR(~chi_2+ -> ~chi_30  H+)
     3.67653942E-06    2     1000039        24   # BR(~chi_2+ -> ~G       W+)
     4.08617673E-11    2     1000039        37   # BR(~chi_2+ -> ~G       H+)
#
#         PDG            Width
DECAY   1000022     8.38370518E-11   # neutralino1 decays
#          BR         NDA      ID1       ID2
     4.61007420E-01    2     1000039        22   # BR(~chi_10 -> ~G        gam)
     5.34416583E-01    2     1000039        23   # BR(~chi_10 -> ~G        Z)
     4.57599672E-03    2     1000039        25   # BR(~chi_10 -> ~G        h)
#
#         PDG            Width
DECAY   1000023     5.27292819E-06   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.71859058E-03    2     1000022        22   # BR(~chi_20 -> ~chi_10 gam)
#          BR         NDA      ID1       ID2
     1.62766641E-08    2     1000039        22   # BR(~chi_20 -> ~G        gam)
     8.85746355E-07    2     1000039        23   # BR(~chi_20 -> ~G        Z)
     8.70142017E-06    2     1000039        25   # BR(~chi_20 -> ~G        h)
#           BR         NDA      ID1       ID2       ID3
     1.22679842E-01    3     1000022        -2         2   # BR(~chi_20 -> ~chi_10 ub      u)
     1.58714057E-01    3     1000022        -1         1   # BR(~chi_20 -> ~chi_10 db      d)
     1.22679842E-01    3     1000022        -4         4   # BR(~chi_20 -> ~chi_10 cb      c)
     1.58714057E-01    3     1000022        -3         3   # BR(~chi_20 -> ~chi_10 sb      s)
     1.09340933E-01    3     1000022        -5         5   # BR(~chi_20 -> ~chi_10 bb      b)
     3.61864389E-02    3     1000022       -11        11   # BR(~chi_20 -> ~chi_10 e+      e-)
     3.61864389E-02    3     1000022       -13        13   # BR(~chi_20 -> ~chi_10 mu+     mu-)
     3.50385182E-02    3     1000022       -15        15   # BR(~chi_20 -> ~chi_10 tau+    tau-)
     7.22235539E-02    3     1000022       -12        12   # BR(~chi_20 -> ~chi_10 nu_eb   nu_e)
     7.22235539E-02    3     1000022       -14        14   # BR(~chi_20 -> ~chi_10 nu_mub  nu_mu)
     7.22235539E-02    3     1000022       -16        16   # BR(~chi_20 -> ~chi_10 nu_taub nu_tau)
     1.12847017E-05    3     1000024        -2         1   # BR(~chi_20 -> ~chi_1+ ub      d)
     1.12847017E-05    3    -1000024        -1         2   # BR(~chi_20 -> ~chi_1- db      u)
     1.12847017E-05    3     1000024        -4         3   # BR(~chi_20 -> ~chi_1+ cb      s)
     1.12847017E-05    3    -1000024        -3         4   # BR(~chi_20 -> ~chi_1- sb      c)
     3.76157087E-06    3     1000024       -12        11   # BR(~chi_20 -> ~chi_1+ nu_eb   e-)
     3.76157087E-06    3    -1000024        12       -11   # BR(~chi_20 -> ~chi_1- nu_e    e+)
     3.76157087E-06    3     1000024       -14        13   # BR(~chi_20 -> ~chi_1+ nu_mub  mu-)
     3.76157087E-06    3    -1000024        14       -13   # BR(~chi_20 -> ~chi_1- nu_mu   mu+)
     4.16288107E-07    3     1000024       -16        15   # BR(~chi_20 -> ~chi_1+ nu_taub tau-)
     4.16288107E-07    3    -1000024        16       -15   # BR(~chi_20 -> ~chi_1- nu_tau  tau+)
#
#         PDG            Width
DECAY   1000025     3.69720348E-02   # neutralino3 decays
#          BR         NDA      ID1       ID2
     4.77945757E-03    2     1000022        23   # BR(~chi_30 -> ~chi_10   Z )
     4.97610240E-01    2     1000024       -24   # BR(~chi_30 -> ~chi_1+   W-)
     4.97610240E-01    2    -1000024        24   # BR(~chi_30 -> ~chi_1-   W+)
     4.83539239E-08    2     1000039        22   # BR(~chi_30 -> ~G        gam)
     1.43969864E-08    2     1000039        23   # BR(~chi_30 -> ~G        Z)
     1.76235548E-10    2     1000039        25   # BR(~chi_30 -> ~G        h)
#
#         PDG            Width
DECAY   1000035     3.39591430E+01   # neutralino4 decays
#          BR         NDA      ID1       ID2
     3.36275989E-03    2     1000022        23   # BR(~chi_40 -> ~chi_10   Z )
     1.67358430E-01    2     1000023        23   # BR(~chi_40 -> ~chi_20   Z )
     1.29038961E-03    2     1000025        23   # BR(~chi_40 -> ~chi_30   Z )
     1.67697019E-01    2     1000024       -24   # BR(~chi_40 -> ~chi_1+   W-)
     1.67697019E-01    2    -1000024        24   # BR(~chi_40 -> ~chi_1-   W+)
     1.29340024E-01    2     1000022        25   # BR(~chi_40 -> ~chi_10   h )
     9.92081912E-04    2     1000022        35   # BR(~chi_40 -> ~chi_10   H )
     1.01455959E-02    2     1000022        36   # BR(~chi_40 -> ~chi_10   A )
     4.77988916E-03    2     1000023        25   # BR(~chi_40 -> ~chi_20   h )
     1.14047900E-02    2     1000023        35   # BR(~chi_40 -> ~chi_20   H )
     1.12571784E-03    2     1000023        36   # BR(~chi_40 -> ~chi_20   A )
     3.68046796E-02    2     1000025        25   # BR(~chi_40 -> ~chi_30   h )
     1.53115520E-04    2     1000025        35   # BR(~chi_40 -> ~chi_30   H )
     1.56364990E-03    2     1000025        36   # BR(~chi_40 -> ~chi_30   A )
     1.27792649E-02    2     1000024       -37   # BR(~chi_40 -> ~chi_1+   H-)
     1.27792649E-02    2    -1000024        37   # BR(~chi_40 -> ~chi_1-   H+)
     2.16349963E-02    2     1000002        -2   # BR(~chi_40 -> ~u_L      ub)
     2.16349963E-02    2    -1000002         2   # BR(~chi_40 -> ~u_L*     u )
     5.25816612E-09    2     2000002        -2   # BR(~chi_40 -> ~u_R      ub)
     5.25816612E-09    2    -2000002         2   # BR(~chi_40 -> ~u_R*     u )
     2.15757309E-02    2     1000001        -1   # BR(~chi_40 -> ~d_L      db)
     2.15757309E-02    2    -1000001         1   # BR(~chi_40 -> ~d_L*     d )
     1.31320975E-09    2     2000001        -1   # BR(~chi_40 -> ~d_R      db)
     1.31320975E-09    2    -2000001         1   # BR(~chi_40 -> ~d_R*     d )
     2.16349963E-02    2     1000004        -4   # BR(~chi_40 -> ~c_L      cb)
     2.16349963E-02    2    -1000004         4   # BR(~chi_40 -> ~c_L*     c )
     5.25816612E-09    2     2000004        -4   # BR(~chi_40 -> ~c_R      cb)
     5.25816612E-09    2    -2000004         4   # BR(~chi_40 -> ~c_R*     c )
     2.15757309E-02    2     1000003        -3   # BR(~chi_40 -> ~s_L      sb)
     2.15757309E-02    2    -1000003         3   # BR(~chi_40 -> ~s_L*     s )
     1.31320975E-09    2     2000003        -3   # BR(~chi_40 -> ~s_R      sb)
     1.31320975E-09    2    -2000003         3   # BR(~chi_40 -> ~s_R*     s )
     1.81498653E-02    2     1000006        -6   # BR(~chi_40 -> ~t_1      tb)
     1.81498653E-02    2    -1000006         6   # BR(~chi_40 -> ~t_1*     t )
    -4.69335657E-03    2     1000005        -5   # BR(~chi_40 -> ~b_1      bb)
    -4.69335657E-03    2    -1000005         5   # BR(~chi_40 -> ~b_1*     b )
     3.38297675E-02    2     2000005        -5   # BR(~chi_40 -> ~b_2      bb)
     3.38297675E-02    2    -2000005         5   # BR(~chi_40 -> ~b_2*     b )
     2.71697553E-04    2     1000011       -11   # BR(~chi_40 -> ~e_L-     e+)
     2.71697553E-04    2    -1000011        11   # BR(~chi_40 -> ~e_L+     e-)
     1.48841389E-10    2     2000011       -11   # BR(~chi_40 -> ~e_R-     e+)
     1.48841389E-10    2    -2000011        11   # BR(~chi_40 -> ~e_R+     e-)
     2.71697553E-04    2     1000013       -13   # BR(~chi_40 -> ~mu_L-    mu+)
     2.71697553E-04    2    -1000013        13   # BR(~chi_40 -> ~mu_L+    mu-)
     1.48841389E-10    2     2000013       -13   # BR(~chi_40 -> ~mu_R-    mu+)
     1.48841389E-10    2    -2000013        13   # BR(~chi_40 -> ~mu_R+    mu-)
     1.26782663E-04    2     1000015       -15   # BR(~chi_40 -> ~tau_1-   tau+)
     1.26782663E-04    2    -1000015        15   # BR(~chi_40 -> ~tau_1+   tau-)
     1.44756440E-04    2     2000015       -15   # BR(~chi_40 -> ~tau_2-   tau+)
     1.44756440E-04    2    -2000015        15   # BR(~chi_40 -> ~tau_2+   tau-)
     2.76871420E-04    2     1000012       -12   # BR(~chi_40 -> ~nu_eL    nu_eb)
     2.76871420E-04    2    -1000012        12   # BR(~chi_40 -> ~nu_eL*   nu_e )
     2.76871420E-04    2     1000014       -14   # BR(~chi_40 -> ~nu_muL   nu_mub)
     2.76871420E-04    2    -1000014        14   # BR(~chi_40 -> ~nu_muL*  nu_mu )
     2.76871420E-04    2     1000016       -16   # BR(~chi_40 -> ~nu_tau1  nu_taub)
     2.76871420E-04    2    -1000016        16   # BR(~chi_40 -> ~nu_tau1* nu_tau )
     8.77951346E-07    2     1000039        22   # BR(~chi_40 -> ~G        gam)
     2.79902854E-06    2     1000039        23   # BR(~chi_40 -> ~G        Z)
     2.62967947E-10    2     1000039        25   # BR(~chi_40 -> ~G        h)
     4.04196541E-11    2     1000039        35   # BR(~chi_40 -> ~G        H)
     4.23901343E-14    2     1000039        36   # BR(~chi_40 -> ~G        A)
#
#         PDG            Width
DECAY        25     4.21859784E-03   # h decays
#          BR         NDA      ID1       ID2
     6.01354487E-01    2           5        -5   # BR(h -> b       bb     )
     6.21877861E-02    2         -15        15   # BR(h -> tau+    tau-   )
     2.20117651E-04    2         -13        13   # BR(h -> mu+     mu-    )
     4.65733068E-04    2           3        -3   # BR(h -> s       sb     )
     2.01145813E-02    2           4        -4   # BR(h -> c       cb     )
     6.67320470E-02    2          21        21   # BR(h -> g       g      )
     2.32257123E-03    2          22        22   # BR(h -> gam     gam    )
     1.62606197E-03    2          22        23   # BR(h -> Z       gam    )
     2.16897226E-01    2          24       -24   # BR(h -> W+      W-     )
     2.80793893E-02    2          23        23   # BR(h -> Z       Z      )
#
#         PDG            Width
DECAY        35     4.01053025E+01   # H decays
#          BR         NDA      ID1       ID2
     1.38801578E-03    2           5        -5   # BR(H -> b       bb     )
     2.32405674E-04    2         -15        15   # BR(H -> tau+    tau-   )
     8.21640584E-07    2         -13        13   # BR(H -> mu+     mu-    )
     1.05200606E-06    2           3        -3   # BR(H -> s       sb     )
     9.48351357E-06    2           4        -4   # BR(H -> c       cb     )
     9.38350105E-01    2           6        -6   # BR(H -> t       tb     )
     7.48440061E-04    2          21        21   # BR(H -> g       g      )
     2.63020784E-06    2          22        22   # BR(H -> gam     gam    )
     1.09163855E-06    2          23        22   # BR(H -> Z       gam    )
     2.46459800E-04    2          24       -24   # BR(H -> W+      W-     )
     1.22893558E-04    2          23        23   # BR(H -> Z       Z      )
     7.76120177E-04    2          25        25   # BR(H -> h       h      )
     1.05820333E-23    2          36        36   # BR(H -> A       A      )
     2.83012883E-11    2          23        36   # BR(H -> Z       A      )
     1.74772298E-12    2          24       -37   # BR(H -> W+      H-     )
     1.74772298E-12    2         -24        37   # BR(H -> W-      H+     )
     1.14263051E-04    2     1000024  -1000024   # BR(H -> ~chi_1+ ~chi_1-)
     9.43973997E-04    2     1000022   1000022   # BR(H -> ~chi_10 ~chi_10)
     8.84363374E-05    2     1000023   1000023   # BR(H -> ~chi_20 ~chi_20)
     3.90922601E-04    2     1000025   1000025   # BR(H -> ~chi_30 ~chi_30)
     1.34194863E-02    2     1000022   1000023   # BR(H -> ~chi_10 ~chi_20)
     8.70459635E-04    2     1000022   1000025   # BR(H -> ~chi_10 ~chi_30)
     4.22929392E-02    2     1000023   1000025   # BR(H -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        36     4.06026089E+01   # A decays
#          BR         NDA      ID1       ID2
     1.38878116E-03    2           5        -5   # BR(A -> b       bb     )
     2.29695004E-04    2         -15        15   # BR(A -> tau+    tau-   )
     8.12054814E-07    2         -13        13   # BR(A -> mu+     mu-    )
     1.07057704E-06    2           3        -3   # BR(A -> s       sb     )
     9.38166870E-06    2           4        -4   # BR(A -> c       cb     )
     9.38938657E-01    2           6        -6   # BR(A -> t       tb     )
     8.88723277E-04    2          21        21   # BR(A -> g       g      )
     2.61280768E-06    2          22        22   # BR(A -> gam     gam    )
     1.27135314E-06    2          23        22   # BR(A -> Z       gam    )
     2.39916152E-04    2          23        25   # BR(A -> Z       h      )
     9.32761966E-04    2     1000024  -1000024   # BR(A -> ~chi_1+ ~chi_1-)
     2.20256549E-02    2     1000022   1000022   # BR(A -> ~chi_10 ~chi_10)
     3.55112906E-06    2     1000023   1000023   # BR(A -> ~chi_20 ~chi_20)
     1.54897362E-02    2     1000025   1000025   # BR(A -> ~chi_30 ~chi_30)
     2.71701050E-04    2     1000022   1000023   # BR(A -> ~chi_10 ~chi_20)
     1.78401217E-02    2     1000022   1000025   # BR(A -> ~chi_10 ~chi_30)
     1.73555209E-03    2     1000023   1000025   # BR(A -> ~chi_20 ~chi_30)
#
#         PDG            Width
DECAY        37     3.97856708E+01   # H+ decays
#          BR         NDA      ID1       ID2
     2.22761224E-06    2           4        -5   # BR(H+ -> c       bb     )
     2.34656809E-04    2         -15        16   # BR(H+ -> tau+    nu_tau )
     8.29596584E-07    2         -13        14   # BR(H+ -> mu+     nu_mu  )
     1.41626793E-08    2           2        -5   # BR(H+ -> u       bb     )
     5.13750058E-08    2           2        -3   # BR(H+ -> u       sb     )
     1.02366635E-05    2           4        -3   # BR(H+ -> c       sb     )
     9.40905993E-01    2           6        -5   # BR(H+ -> t       bb     )
     2.45962770E-04    2          24        25   # BR(H+ -> W+      h      )
     1.32880988E-12    2          24        36   # BR(H+ -> W+      A      )
     1.11443358E-02    2     1000024   1000022   # BR(H+ -> ~chi_1+ ~chi_10)
     1.99946767E-04    2     1000024   1000023   # BR(H+ -> ~chi_1+ ~chi_20)
     4.72557454E-02    2     1000024   1000025   # BR(H+ -> ~chi_1+ ~chi_30)
