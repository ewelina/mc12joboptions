#--------------------------------------------------------------
# Powheg tt setup starting from ATLAS defaults
#--------------------------------------------------------------
include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.topdecaymode = 22200

PowhegConfig.withdamp = 172.5

if evgenConfig.minevents > 0:
    PowhegConfig.nEvents = 4 * evgenConfig.minevents
else:
    PowhegConfig.nEvents = 4 * 5000

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

#--------------------------------------------------------------
# Pythia6 (Perugia2011) showering
#--------------------------------------------------------------
include('MC12JobOptions/PowhegPythia_Perugia2011C_Common.py')
include('MC12JobOptions/Pythia_Tauola.py')
include('MC12JobOptions/Pythia_Photos.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011c tune'
evgenConfig.keywords    = [ 'top', 'ttbar', 'leptonic' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]
evgenConfig.generators += [ 'Powheg', 'Pythia' ]
#evgenConfig.minevents = 10000

# ... Filter
from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
topAlg += MultiElecMuTauFilter("Multi2LLeptonFilter")

Multi2LLeptonFilter = topAlg.Multi2LLeptonFilter
Multi2LLeptonFilter.MinPt = 10000.
Multi2LLeptonFilter.MaxEta = 5.0
Multi2LLeptonFilter.NLeptons = 2
Multi2LLeptonFilter.IncludeHadTaus = 0

StreamEVGEN.RequireAlgs += [ "Multi2LLeptonFilter" ]
