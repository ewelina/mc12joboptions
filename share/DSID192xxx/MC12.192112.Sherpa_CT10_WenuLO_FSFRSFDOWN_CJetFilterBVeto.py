include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa W+/W- -> enu + 0,1,2j@NLO + 3,4,5j@LO."
evgenConfig.keywords = ["W" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.inputconfcheck = "Sherpa_CT10_WenuLO_FSFRSFDOWN"

evgenConfig.process="""
(run){
  %INIT_ONLY 1
  %scales, tags for scale variations
  FSF:=0.25; RSF:=0.25; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  %tags for process setup
  NJET:=4; LJET:=0; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=Internal;
}(run)

(processes){
  Process 93 93 -> 11 -12 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01;
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;

  Process 93 93 -> -11 12 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01;
  SCALES LOOSE_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2} {6,7,8};
  End process;
}(processes)

(selector){
  Mass 11 -12 2.0 E_CMS
  Mass -11 12 2.0 E_CMS
}(selector)
"""
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
HeavyFlavorBHadronFilter = HeavyFlavorHadronFilter(name="HeavyFlavorBHadronFilter")
HeavyFlavorBHadronFilter.RequestBottom=True
HeavyFlavorBHadronFilter.RequestCharm=False
HeavyFlavorBHadronFilter.Request_cQuark=False
HeavyFlavorBHadronFilter.Request_bQuark=False
HeavyFlavorBHadronFilter.RequestSpecificPDGID=False
HeavyFlavorBHadronFilter.RequireTruthJet=False
HeavyFlavorBHadronFilter.BottomPtMin=0*GeV
HeavyFlavorBHadronFilter.BottomEtaMax=4.0

HeavyFlavorCHadronFilter = HeavyFlavorBHadronFilter.clone('HeavyFlavorCHadronFilter')
HeavyFlavorCHadronFilter.RequestBottom=False
HeavyFlavorCHadronFilter.RequestCharm=True
HeavyFlavorCHadronFilter.CharmPtMin=0*GeV
HeavyFlavorCHadronFilter.CharmEtaMax=4.0
HeavyFlavorCHadronFilter.RequireTruthJet=True
HeavyFlavorCHadronFilter.DeltaRFromTruth=0.5
HeavyFlavorCHadronFilter.JetPtMin=15.0*GeV
HeavyFlavorCHadronFilter.JetEtaMax=3.0
HeavyFlavorCHadronFilter.TruthContainerName="AntiKt4TruthJets"

topAlg += HeavyFlavorBHadronFilter
topAlg += HeavyFlavorCHadronFilter
StreamEVGEN.RequireAlgs += [ "HeavyFlavorCHadronFilter" ]
StreamEVGEN.VetoAlgs += [ "HeavyFlavorBHadronFilter" ]
