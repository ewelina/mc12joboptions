evgenConfig.description = "W'->WZ with mass = 3000 GeV - produced with Pythia8 - showered with Herwig++"
evgenConfig.generators = ["Lhef","Herwigpp"]
evgenConfig.inputfilecheck = 'Wprime_WZqqqq'

include ( "MC12JobOptions/Herwigpp_UEEE3_CTEQ6L1_LHEF_Common.py")

cmds = """
create ThePEG::ParticleData W'+
setup W'+ 34 W'+ 3000.0 0.0 0.0 0.0 3 0 3 0
create ThePEG::ParticleData W'-
setup W'- -34 W'- 3000.0 0.0 0.0 0.0 -3 0 3 0
makeanti W'+ W'-
"""
topAlg.Herwigpp.Commands += cmds.splitlines()
