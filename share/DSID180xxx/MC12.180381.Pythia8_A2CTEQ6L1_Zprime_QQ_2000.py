
evgenConfig.description = "Zprime(2000)->qq production with the A2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW", "Zprime"]

include("MC12JobOptions/Pythia8_A2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on", # create Z bosons
                            "PhaseSpace:mHatMin = 500.", # lower invariant mass
                            "Zprime:gmZmode = 3",
                            "32:m0 = 2000", # set Z mass
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny = 6 5 4 3 2 1"] # switch on Z->bb decays

