evgenConfig.description = "Quantum black holes (M_th = 4.5 TeV) decaying to two opposite sign muons."
evgenConfig.contact = ["gingrich@ualberta.ca", "marc.bret.87@gmail.com"]
evgenConfig.keywords = ["exotics", "blackholes", "QBH", "dilepton"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "QBH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
