evgenConfig.description = "CI->ee production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CIee", "electrons", "ee"]
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
 
 
topAlg.Pythia8.Commands += ["ContactInteractions:QCffbar2eebar = on"] # process
topAlg.Pythia8.Commands += ["ContactInteractions:Lambda = 7000"] # mass scale
topAlg.Pythia8.Commands += ["ContactInteractions:etaLR = -1"] # interference
topAlg.Pythia8.Commands += ["PhaseSpace:mHatMin = 1200"] # min invariant mass
