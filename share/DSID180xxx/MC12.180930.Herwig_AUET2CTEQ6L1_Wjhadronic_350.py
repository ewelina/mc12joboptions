evgenConfig.description = "Herwig W->had pT > 350 GeV"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["Whad"]
evgenConfig.contact  = ["francesco.delorenzi@cern.ch"]
	
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
# ... Herwig
topAlg.Herwig.HerwigCommand += ["iproc 12100",
	                         "ptmin 350",
	                         "modbos 1 1"]
#... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
