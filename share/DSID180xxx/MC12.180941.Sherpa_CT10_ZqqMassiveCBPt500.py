include( "MC12JobOptions/Sherpa_CT10_Common.py" )
evgenConfig.description = "Zqq + jets, with c and b-quarks treated as massive"
evgenConfig.keywords = [ "Z", "hadronic" ]
evgenConfig.contact  = ["ayana@cern.ch"]
evgenConfig.inputconfcheck = "ZqqMassiveCBPt500"
evgenConfig.minevents = 200
evgenConfig.process="""
(run){
  MASSIVE[4]=1
  MASSIVE[5]=1 
  ME_SIGNAL_GENERATOR=Comix
}(run)

(processes){
  Process 93  93 -> 23[a] 93 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  93 -> 23[a] 93 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  93 -> 23[a] 93 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5  -5 -> 23[a] 93 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5  -5 -> 23[a] 93 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5  -5 -> 23[a] 93 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 -4 -> 23[a] 93 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 -4 -> 23[a] 93 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 4 -4 -> 23[a] 93 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 -5 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 -5 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 5 -5 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 5 -> 23[a] 5 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 5 -> 23[a] 5 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 5 -> 23[a] 5 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 93 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 93{3}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 93{3}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 93{3}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  5 -5 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  5 -5 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  5 -5 -> 23[a] 4 -4 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -4 -> 23[a] 5 -5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  5 -> 23[a] 5 4 -4 93{1}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  5 -> 23[a] 5 4 -4 93{1}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  5 -> 23[a] 5 4 -4 93{1}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 5 -5 93{1}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 5 -5 93{1}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93  4 -> 23[a] 4 5 -5 93{1}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 4 -4 93{1}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 4 -4 93{1}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -5 -> 23[a] -5 4 -4 93{1}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 5 -5 93{1}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 5 -5 93{1}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process 93 -4 -> 23[a] -4 5 -5 93{1}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4  5 -> 23[a] 4 5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4  5 -> 23[a] 4 5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4  5 -> 23[a] 4 5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4  5 -> 23[a] -4 5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4  5 -> 23[a] -4 5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4  5 -> 23[a] -4 5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -5 -> 23[a] 4 -5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -5 -> 23[a] 4 -5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process  4 -5 -> 23[a] 4 -5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> 23[a] -4 -5 93{2}
  Decay 23[a] -> 94 94
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> 23[a] -4 -5 93{2}
  Decay 23[a] -> 4 -4
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

  Process -4 -5 -> 23[a] -4 -5 93{2}
  Decay 23[a] -> 5 -5
  Order_EW 2; Max_N_Quarks 6;
  CKKW sqr(20.0/E_CMS)
  Integration_Error 0.05 {5,6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  Cut_Core 1
  End process

}(processes)

(selector){
  DecayMass 23 2.0 E_CMS
  Decay(PPerp(p[0]+p[1])) 23 500.0 E_CMS
  PT 93 1.0 E_CMS
  PT 4 1.0 E_CMS
  PT -4 1.0 E_CMS
  PT 5 1.0 E_CMS
  PT -5 1.0 E_CMS
}(selector)
"""

