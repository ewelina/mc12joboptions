
evgenConfig.description = "W*->QQ production AU2 MSTW2008LO M=200 SinX0"
evgenConfig.keywords = ["electroweak","W*","quarks"]
evgenConfig.inputfilecheck = "Wstar"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_CalcHep.py")
include("MC12JobOptions/Pythia8_Photos.py")


