evgenConfig.contact = ["istin@cern.ch"]
evgenConfig.description = "Comphep Pythia8 E6 isosinglet pair production-dilepton channel m400"
evgenConfig.keywords = ["E6","isosinglet","pair Production","Dibosons","Dilepton"]
evgenConfig.inputfilecheck ="E6_400_WZ_jjll"
evgenConfig.minevents  = 5000

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_CompHep.py")
include("MC12JobOptions/Pythia8_Photos.py")

#Z->ll W->jj decay modes only
topAlg.Pythia8.Commands += [
			    "23:onmode = OFF",
			    "23:onifany = 11 -11 13 -13",
			    "24:onmode = OFF",
			    "24:onifany = 1 2 3 4 5  -1 -2 -3 -4 -5",
			    "-24:onmode= OFF",
			    "-24:onifany = 1 2 3 4 5 -1 -2 -3 -4 -5"
			   ]
include ( "MC12JobOptions/MultiLeptonFilter.py" )
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)
