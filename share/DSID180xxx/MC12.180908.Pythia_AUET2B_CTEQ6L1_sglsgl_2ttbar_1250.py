evgenConfig.description = "process qqbar and gg -> sgluon sgluon with sgluon -> ttbar, m(sgluon)=1250GeV"
evgenConfig.keywords = ['Exotics','sgluons','4top']

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )

ecm4sgluon=open('./sgluonecm.dat', 'w')
print >> ecm4sgluon, runArgs.ecmEnergy
ecm4sgluon.close()

topAlg.Pythia.addParticle = True
topAlg.Pythia.PythiaCommand += ["pyinit user pythiasgluon",
                                "pydat2 pmas 5100021 1 1250",
				"pydat3 mdme 5065 1 0",
				"pydat3 mdme 5066 1 1",
				"pydat3 mdme 5067 1 0"]


