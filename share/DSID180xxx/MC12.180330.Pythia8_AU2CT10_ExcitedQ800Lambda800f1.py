
evgenConfig.description = "Excited quark 800 GeV, with the AU2 CT10 tune"
evgenConfig.keywords = ["excitedquark"]
evgenConfig.contact = ["Oliver Endner"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += ["ExcitedFermion:dg2dStar = on",
                           "ExcitedFermion:ug2uStar = on",
                           "4000001:m0 = 800", # d* mass
                           "4000002:m0 = 800", # u* mass
                           "ExcitedFermion:Lambda = 800.0",
                           "ExcitedFermion:coupF = 1.0", # SU(2) coupling
                           "ExcitedFermion:coupFprime = 1.0", # U(1) coupling
                           "ExcitedFermion:coupFcol = 1.0", # SU(3) coupling
                           "4000001:mayDecay = on", # d* -> d g, d gam, d Z0, d W-
                           "4000002:mayDecay = on"] # u* -> u g, u gam, u Z0, u W+
  