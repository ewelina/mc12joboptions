evgenConfig.description    = "Madgraph+Pythia8, W/Z+H production with a Higgs boson decaying to prompt lepton-jets, WH 2step-model mzd=200MeV mH=140GeV"
evgenConfig.keywords       = ["nonSMhiggs","VH","lepton-jet","Higgs physics","lepton production","exotics"]
evgenConfig.inputfilecheck = 'HtoLJets'

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

