evgenConfig.description = "G*->ee production with no lepton filter and AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "G*", "RS", "leptons"]
evgenConfig.contact = ["Daniel Hayden"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ['ExtraDimensionsG*:gg2G* = on']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:ffbar2G* = on']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:SMinBulk = off']
topAlg.Pythia8.Commands += ['ExtraDimensionsG*:kappaMG = 0.54']

topAlg.Pythia8.Commands += ['5100039:m0 = 1500.']
topAlg.Pythia8.Commands += ['5100039:onMode = off']
topAlg.Pythia8.Commands += ['5100039:onIfAny = 11']
topAlg.Pythia8.Commands += ['5100039:mMin = 50.']

topAlg.Pythia8.Commands += ['PhaseSpace:mHatMin = 50.']
