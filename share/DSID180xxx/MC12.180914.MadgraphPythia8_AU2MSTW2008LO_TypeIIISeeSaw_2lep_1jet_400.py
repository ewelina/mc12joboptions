evgenConfig.inputfilecheck = 'TypeIIISeeSaw'
evgenConfig.description = "Type III SeeSaw Model Mass: 400 GeV Vu=0.063 Ve=0.055"
evgenConfig.keywords = ["TypeIIISeeSaw"] 
	

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py");
include("MC12JobOptions/Pythia8_MadGraph.py")


#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]


include('MC12JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 5000  #5 GeV
MultiLeptonFilter.Etacut      = 3
MultiLeptonFilter.NLeptons = 2

include("MC12JobOptions/JetFilterAkt6.py")
JetFilter = topAlg.QCDTruthJetFilter
JetFilter.MinPt = 10000 #10 GeV


