evgenConfig.description = "W->taumu, tau->mugamma production, with lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "W", "tau", "leptons", "photon", "mugamma", "LFV"]
evgenConfig.contact = ['hod_@_cern.ch']
evgenConfig.generators += ["Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15", # switch on W->taunu decays
                            "15:oneChannel = true 1.0 0 13 22",   # tau- decays to mu- + gamma
                            "-15:oneChannel = true 1.0 0 -13 22"  # tau+ decays to mu+ + gamma
]
