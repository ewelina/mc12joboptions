evgenConfig.description = "LSTC Wgamma 400 point"
evgenConfig.keywords = ["LSTC", "Wgamma", "400"]
evgenConfig.contact = ["louis.helary@cern.ch"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

MaT=400.
MW=80.399
MrhoT=MaT/1.1
MPiT=MrhoT-MW


topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                       "pysubs msub 194 0",
                       "pysubs msub 361 0",
                       "pysubs msub 362 0",
                       "pysubs msub 363 0",
                       "pysubs msub 364 0",
                       "pysubs msub 365 0",
                       "pysubs msub 366 0",
                       "pysubs msub 368 0",
                       "pysubs msub 370 0",
                       "pysubs msub 371 0",
                       "pysubs msub 372 0",
                       "pysubs msub 373 0",
                       "pysubs msub 374 0",
                       "pysubs msub 375 0",
                       "pysubs msub 376 0",
                       "pysubs msub 367 0",
                       "pysubs msub 377 0",
                       "pysubs msub 378 1",
                       "pysubs msub 379 0",
                       "pysubs msub 380 0",
                       "pysubs ckin 1 "+str(MaT-40),
                       "pysubs ckin 2 "+str(MaT+40)]

topAlg.Pythia.PythiaCommand += ["pydat3 mdme 190 1 0",
                         "pydat3 mdme 191 1 0",
                         "pydat3 mdme 192 1 0",
                         "pydat3 mdme 193 1 -1",
                         "pydat3 mdme 194 1 0",
                         "pydat3 mdme 195 1 0",
                         "pydat3 mdme 196 1 0",
                         "pydat3 mdme 197 1 -1",
                         "pydat3 mdme 198 1 0",
                         "pydat3 mdme 199 1 0",
                         "pydat3 mdme 200 1 0",
                         "pydat3 mdme 201 1 -1",
                         "pydat3 mdme 202 1 -1",
                         "pydat3 mdme 203 1 -1",
                         "pydat3 mdme 204 1 -1",
                         "pydat3 mdme 205 1 -1",
                         "pydat3 mdme 206 1 1",
                         "pydat3 mdme 207 1 1",
                         "pydat3 mdme 208 1 1"]

topAlg.Pythia.PythiaCommand += ["pydat2 pmas 3000111 1 "+str(MPiT),
                        "pydat2 pmas 3000211 1 "+str(MPiT),
                        "pydat2 pmas 3000221 1 "+str(MPiT*2),
                        "pydat2 pmas 3000115 1 "+str(MaT),
                        "pydat2 pmas 3000215 1 "+str(MaT),
                        "pydat2 pmas 3000113 1 "+str(MrhoT),
                        "pydat2 pmas 3000213 1 "+str(MrhoT),
                        "pydat2 pmas 3000223 1 "+str(MrhoT)]

topAlg.Pythia.PythiaCommand += ["pypars mstp 42 0"]
topAlg.Pythia.PythiaCommand += ["pypars mstp 32 4"]
topAlg.Pythia.PythiaCommand += ["pytcsm rtcm 47 1.",
                        "pytcsm rtcm 12 "+str(MrhoT),
                        "pytcsm rtcm 13 "+str(MrhoT),
                        "pytcsm rtcm 48 "+str(MrhoT),
                        "pytcsm rtcm 49 "+str(MrhoT),
                        "pytcsm rtcm 50 "+str(MrhoT),
                        "pytcsm rtcm 51 "+str(MrhoT),
                        "pytcsm rtcm 2 0.5",
                        "pytcsm rtcm 3 0.3333333"]

topAlg.Pythia.PythiaCommand += ["pydat1 parj 90 20000",
                         "pydat3 mdcy 15 1 0"]

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

include("MC12JobOptions/PhotonFilter.py")
PhotonFilter = topAlg.PhotonFilter
PhotonFilter.Ptcut = 30000
PhotonFilter.Etacut = 3
PhotonFilter.NPhotons = 1

include("MC12JobOptions/MultiLeptonFilter.py")
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 15000
MultiLeptonFilter.Etacut = 3
MultiLeptonFilter.NLeptons = 1

