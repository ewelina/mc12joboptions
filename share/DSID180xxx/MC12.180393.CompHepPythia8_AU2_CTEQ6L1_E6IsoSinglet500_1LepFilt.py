evgenConfig.contact = ["istin@cern.ch"]
evgenConfig.description = "Comphep Pythia8 E6 isosinglet pair production m500"
evgenConfig.keywords = ["E6","isosinglet","pair Production","Dibosons","Higgs"]
evgenConfig.inputfilecheck ="E6IsoSinglet500"
evgenConfig.minevents  = 5000

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_CompHep.py")

#leave all decay modes on:

include ( "MC12JobOptions/MultiLeptonFilter.py" )
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 1
topAlg.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)
