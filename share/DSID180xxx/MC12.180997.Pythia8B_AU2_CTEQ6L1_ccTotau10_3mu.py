##############################################################
# Job options fragment for cc->X+tau10->3mu
# Noam Tal Hod hod@cern.ch
##############################################################
evgenConfig.contact = ['hod_@_cern.ch']
evgenConfig.generators += ["Pythia8B"]

include("MC12JobOptions/Pythia8B_AU2_CTEQ6L1_Common.py")

evgenConfig.description = "cc->X+tau10->3mu production"
evgenConfig.keywords = ["charm","mu","tau","LFV"]
evgenConfig.minevents = 200

topAlg.Pythia8B.Commands += ['HardQCD:all = on'] 
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 10.']    
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Force tau decays to 3mu
topAlg.Pythia8B.Commands += ["15:oneChannel = true 1.0 0 13 13 -13"]
topAlg.Pythia8B.Commands += ["-15:oneChannel = true 1.0 0 -13 -13 13"]

topAlg.Pythia8B.SelectBQuarks = False
topAlg.Pythia8B.SelectCQuarks = True
topAlg.Pythia8B.QuarkPtCut = 10.0
topAlg.Pythia8B.AntiQuarkPtCut = 10.0
topAlg.Pythia8B.QuarkEtaCut = 4.5
topAlg.Pythia8B.AntiQuarkEtaCut = 4.5
topAlg.Pythia8B.RequireBothQuarksPassCuts = False
topAlg.Pythia8B.VetoDoubleCEvents = True

topAlg.Pythia8B.NHadronizationLoops = 2

include("Pythia8B_i/BPDGCodes.py")

topAlg.Pythia8B.TriggerPDGCode = 15
topAlg.Pythia8B.TriggerStatePtCut = [10.0]
topAlg.Pythia8B.TriggerStateEtaCut = 2.5
topAlg.Pythia8B.MinimumCountPerCut = [1]
