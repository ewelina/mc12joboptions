include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include ("MC12JobOptions/Pythia8_MadGraph.py")
evgenConfig.generators += ["MadGraph", "Pythia8"]
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]
    
                                                                                                                                                                                               
include ( "MC12JobOptions/Pythia8_Photos.py")


evgenConfig.inputfilecheck = 'UVcomplete_DM'
evgenConfig.description = "mono-Z DM UV-complete toy model mchi10 meta200"
