include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

evgenConfig.description = "Vector-like leptons, m=140 GeV "
evgenConfig.keywords = ["vector","lepton","VLL","trilepton"]
evgenConfig.inputfilecheck = 'VectorLepton'
