evgenConfig.description = "RS1 quantum black holes (n = 1, M_th = 1000 GeV) decaying to one e+ and one e-."
evgenConfig.contact = ["gingrich@ualberta.ca"]
evgenConfig.keywords = ["exotics", "blackholes", "e+", "e-"]
evgenConfig.generators += ["QBH"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
