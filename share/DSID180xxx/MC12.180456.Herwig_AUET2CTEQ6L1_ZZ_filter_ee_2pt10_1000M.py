evgenConfig.description = "Herwig ZZ all filtered for one lepton"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["diboson", "semileptonic"]
evgenConfig.contact  = ["daniel.hayden@cern.ch"]
evgenConfig.minevents = 50

include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc 12810",
                                "modbos 1 2",
                                "taudec TAUOLA" ]
# note that 10000 is added to the herwig process number
# ... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Jimmy_Photos.py" )

# Add the filters:
# Dilepton MassRange filter
from GeneratorFilters.GeneratorFiltersConf import MassRangeFilter
topAlg += MassRangeFilter()

MassRangeFilter = topAlg.MassRangeFilter
MassRangeFilter.EtaCut  = 2.8
MassRangeFilter.EtaCut2 = 2.8
MassRangeFilter.PartId  = 11
MassRangeFilter.PartId2 = 11
MassRangeFilter.PtCut = 10000.
MassRangeFilter.PtCut2 = 10000.
MassRangeFilter.InvMassMin = 1000000.

StreamEVGEN.RequireAlgs += [ "MassRangeFilter" ]
