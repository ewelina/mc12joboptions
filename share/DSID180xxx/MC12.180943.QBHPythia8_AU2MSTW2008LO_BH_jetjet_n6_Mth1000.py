evgenConfig.description = "Quantum black holes (n = 6, M_th = 1000 GeV) decaying to two partons."
evgenConfig.contact = ["gingrich@ualberta.ca"]
evgenConfig.keywords = ["exotics", "blackholes", "QBH", "dijets"]
evgenConfig.generators += ["QBH"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
