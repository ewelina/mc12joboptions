evgenConfig.description = "Herwig Z->had  200 < pT < 350 GeV"
evgenConfig.generators = ["Herwig"]
evgenConfig.keywords = ["Zhad"]
evgenConfig.contact  = ["francesco.delorenzi@cern.ch"]
	
include ( "MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
# ... Herwig
topAlg.Herwig.HerwigCommand += ["iproc 12150",
	                         "ptmin 200",
				 "ptmax 350", 
	                         "modbos 1 1"]
#... Tauola
include ( "MC12JobOptions/Jimmy_Tauola.py" )
include ( "MC12JobOptions/Jimmy_Photos.py" )
