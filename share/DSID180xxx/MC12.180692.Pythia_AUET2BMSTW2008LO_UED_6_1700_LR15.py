evgenConfig.description = "UED diphoton plus MET - LR=15 1/R=1700 GeV"
evgenConfig.keywords = ["Exotics", "UED", "photons", "MET"]
evgenConfig.contact = ["helenka@mail.cern.ch"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia_Photos.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                         "pypued iued 1 1",      # turn ON UED =1 
                         "pypued iued 2 1",      # turn ON =1 gravity mediated decays
                         "pypued iued 3 5",      # number of KK quark flavors = 5
                         "pypued iued 4 6",      # number or large extra dimensions = 6 for grav.med.deca.
                         "pypued iued 5 1",      # Lambda*R is fixed to the RUED(4) value
                         "pypued iued 6 1",      # KK masses radiative corrections turned ON
                         "pysubs msub 311 1",    # all
                         "pysubs msub 312 1",    # UED
                         "pysubs msub 313 1",    # production
                         "pysubs msub 314 1",    # processes
                         "pysubs msub 315 1",    # are
                         "pysubs msub 316 1",    # turned
                         "pysubs msub 317 1",    # ON
                         "pysubs msub 318 1",    #
                         "pysubs msub 319 1",    #
                         "pypued rued 1 1700",    # 1/R = 300 GeV
                         "pypued rued 2 5000.",  # M_D = 5000 GeV for grav.med.dec.
                         "pypued rued 3 20000.", # Lambda = 20000 GeV if IUED(5)=0
                         "pypued rued 4 15.",    # Lambda*R = 20 GeV by default
                         "pydat1 parj 90 20000.",  ## Turn off FSR
                         "pydat3 mdcy 15 1 0"]     ## Turn off tau decays
