include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_MadGraph.py")

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

evgenConfig.description = "SM + MWT ZH -> vvjj, M(A)=300 GeV, LHEF produced with MadGraph 4.4.52"
evgenConfig.keywords = ["MWT Dibosons Multileptons"]
evgenConfig.inputfilecheck = 'MWT'
