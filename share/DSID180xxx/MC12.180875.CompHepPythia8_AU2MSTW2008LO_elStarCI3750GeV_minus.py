evgenConfig.description = " negative excited electron m_e*=3750GeV, Lambda=5TeV (e e* -> e e q q)"
evgenConfig.keywords = ["exotics","compositeness","ExcitedElectron"]
evgenConfig.inputfilecheck = "elStar" 

include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include ( "MC12JobOptions/Pythia8_CompHep.py" )
evgenConfig.minevents = 5000
