
evgenConfig.description = "Monophoton sample with mNeutralino 100"
evgenConfig.keywords = [ "Gravitino", "Neutralino", "Monophoton" ]
evgenConfig.inputfilecheck = "Neutral100_Monophot"
evgenConfig.generators = [ "CalcHep" ]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/DirectPhotonFilter.py")

topAlg.Pythia.PythiaCommand += [
    "pyinit user lhef",
    "pysubs msel 0",
    "pydat2 pmas 1000022 1 100.0",
    "pydat2 pmas 1000039 1 0.0",
    "pyinit win 8000.0",
    "pymssm imss 1 1",
    "pymssm imss 11 1",
    "pymssm rmss 29 1.0e12",
    "pydat3 mdme 2163 1 0",
    "pyinit pylisti 12",
    "pypars mstp 51 21000",
    "pypars mstp 53 21000",
    "pypars mstp 55 21000"]

topAlg.DirectPhotonFilter.Ptcut = 35000.0
topAlg.DirectPhotonFilter.NPhotons = 1
topAlg.DirectPhotonFilter.AllowSUSYDecay = True
