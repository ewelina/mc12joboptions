evgenConfig.description = "High pT Hadronic Z pT >350 "
evgenConfig.keywords = ["Zhad"] 

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")	 

topAlg.Pythia8.Commands += ["PartonLevel:FSR = on"] # turn on FSR (no Photons interface to Pythia8)
topAlg.Pythia8.Commands += ["PhaseSpace:pTHatMin = 350."]
topAlg.Pythia8.Commands += ["WeakBosonAndParton:qqbar2gmZg = on"]
topAlg.Pythia8.Commands += ["WeakBosonAndParton:qg2gmZq = on"]
topAlg.Pythia8.Commands += ["23:onMode = off"] # switch off all Z decays
topAlg.Pythia8.Commands += ["23:onIfAny = 1 2 3 4 5"] # switch on Z->had


