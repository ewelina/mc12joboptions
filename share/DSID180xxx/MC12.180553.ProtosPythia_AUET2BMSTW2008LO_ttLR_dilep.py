evgenConfig.description = "Protos+Pythia6 for same sign tops ttLR"

evgenConfig.keywords = ["tt", "top"]

evgenConfig.inputfilecheck = "ttLR"

evgenConfig.generators = ["Protos", "Pythia"]

include("MC12JobOptions/Pythia_AUET2B_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia_Tauola.py")
include("MC12JobOptions/Pythia_Photos.py")

topAlg.Pythia.PythiaCommand += ["pyinit user protos"]

