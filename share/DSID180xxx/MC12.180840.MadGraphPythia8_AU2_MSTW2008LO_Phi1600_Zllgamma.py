include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_MadGraph.py")
include ( "MC12JobOptions/Pythia8_Photos.py" ) 
topAlg.Pythia8.Commands += [ "Init:showAllParticleData = on",
                	  "Next:numberShowLHA = 10",
		          "Next:numberShowEvent = 10"
                          "15:onMode = off" 
]

include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 25000.
topAlg.PhotonFilter.Etacut    = 3.0

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.NLeptons = 2
topAlg.MultiLeptonFilter.Etacut = 3.0
topAlg.MultiLeptonFilter.Ptcut = 6.*GeV


evgenConfig.description = "Scalar resonance -> Z(ll)Gamma mll>60GeV; photon and dilepton filters -MadGraph+Pythia8 M=1600"
evgenConfig.keywords = ["Exotic", "Scalar", "ZGamma", "1600"]
evgenConfig.generators += ["Pythia8", "MadGraph"]
evgenConfig.contact = ["dartyin@cern.ch"]

evgenConfig.inputfilecheck = 'Zllgamma'

