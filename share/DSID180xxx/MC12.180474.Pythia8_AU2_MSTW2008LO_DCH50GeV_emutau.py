###############################################################
#
# Job options file
# Pythia8 Doubly Charged Higgs lepotn mode
# contact: Kenji Hamano
#==============================================================
evgenConfig.description = "Pythia8 DCH production JO example with the AU2 MSTW2008 tune"
evgenConfig.keywords = ["Exotics", "DCH", "H++", "SSDilepton"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += [
    "9900042:m0 = 50.", # H++_R mass [GeV]
    "9900041:m0 = 50.", # H++_L mass [GeV]         
    "LeftRightSymmmetry:ffbar2HLHL=on", # double HL pair production
    "LeftRightSymmmetry:ffbar2HRHR=on", # double HR pair production  
    
    # couplings to leptons
    "LeftRightSymmmetry:coupHee=0.1",
    "LeftRightSymmmetry:coupHmue=0.1",
    "LeftRightSymmmetry:coupHmumu=0.1",
    "LeftRightSymmmetry:coupHtaue=0.1",
    "LeftRightSymmmetry:coupHtaumu=0.1",
    "LeftRightSymmmetry:coupHtautau=0.1",
    ]


