include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Electroweak production of 2 neutrinos and 2 jets, merged with up to one additional QCD jet in ME+PS."
evgenConfig.keywords = [ ]
evgenConfig.contact  = ["frank.siegert@cern.ch"]
evgenConfig.minevents = 500

evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 -> 91 91 93 93 93{1}
  Order_EW 4;
  CKKW sqr(15/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {6}
  Integration_Error 0.05 {5,6}
  End process;
}(processes)

(selector){
  NJetFinder 2 15.0 0.0 0.4 -1
}(selector)
"""
