include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "lnnn + up to 3 jets with ME+PS."
evgenConfig.keywords = [ ]
evgenConfig.contact  = ["frank.siegert@cern.ch", "christian.schillo@cern.ch"]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.02
}(run)

(processes){
  Process 93 93 -> 90 91 91 91 93{3};
  Integration_Error 0.05 {6,7}
  Order_EW 4;
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)
"""
