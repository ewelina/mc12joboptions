evgenConfig.description = "W->taumu, tau->mugamma production, NO lepton filter and AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "W", "tau", "leptons"]
evgenConfig.contact = ['Olga.Igonkina_@_cern.ch']

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2W = on", # create W bosons
                            "24:onMode = off", # switch off all W decays
                            "24:onIfAny = 15", # switch on W->tau,nu decays
                            #"15:onMode = off",
                            "15:oneChannel = true 1.0 0 13 22",
                            "-15:oneChannel = true 1.0 0 -13 22"
]

