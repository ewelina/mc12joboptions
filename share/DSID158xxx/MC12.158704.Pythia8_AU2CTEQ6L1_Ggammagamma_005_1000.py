evgenConfig.description = "RS Graviton, m = 1000 GeV, kappaMG = 0.271."
evgenConfig.keywords = ["Exotics", "G*"]


include ("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include ("MC12JobOptions/Pythia8_Photos.py")


#CTEQ6L1 (LO)
topAlg.Pythia8.Commands += ["PDF:pSet= 8"]        


topAlg.Pythia8.Commands += ["ExtraDimensionsG*:gg2G* = on"]
topAlg.Pythia8.Commands += ["ExtraDimensionsG*:ffbar2G* = on"]
topAlg.Pythia8.Commands += ["5100039:m0 = 1000." ]
topAlg.Pythia8.Commands += ["5100039:onMode = off"]
topAlg.Pythia8.Commands += ["5100039:onIfAny = 22"]
topAlg.Pythia8.Commands += ["ExtraDimensionsG*:kappaMG = 0.271"]






