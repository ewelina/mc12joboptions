evgenConfig.description = "Leptoquark (M = 700 GeV) to di-electron jet channel"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]
evgenConfig.contact = ['vikas.bansal@cern.ch']

# Define Pythia8 Tune and PDF
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

# Include Photos
include("MC12JobOptions/Pythia8_Photos.py")

# turn on Leptoquark pair production
topAlg.Pythia8.Commands += ["LeptoQuark:gg2LQLQbar = on"]
topAlg.Pythia8.Commands += ["LeptoQuark:qqbar2LQLQbar = on"]

# Leptoquark Yukawa coupling lambda^2/(4 pi) = k alpha_em
topAlg.Pythia8.Commands += ["LeptoQuark:kCoup = 0.01"]

# Leptoquark decay products
topAlg.Pythia8.Commands += ["42:0:products = 2 11"]

# Leptoquark mass
topAlg.Pythia8.Commands += ["42:m0 = 700"]

# Leptoquark mass Kinematic cuts
topAlg.Pythia8.Commands += ["42:mMin = 500"]
topAlg.Pythia8.Commands += ["42:mMax = 900"]
