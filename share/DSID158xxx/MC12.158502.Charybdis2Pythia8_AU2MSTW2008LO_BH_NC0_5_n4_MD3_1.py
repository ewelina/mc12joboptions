evgenConfig.description = "Noncommutative black holes: sqrt(theta)*M_D=0.5, n=4, M_D=3.1TeV"
evgenConfig.contact = ["gingrich@ualberta.ca"]
evgenConfig.keywords = ["exotics", "blackholes", "noncommutative"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py" )

topAlg.Pythia8.Commands += [
  "SLHA:readFrom=1",
  "SLHA:minMassSM=0.0"
]
