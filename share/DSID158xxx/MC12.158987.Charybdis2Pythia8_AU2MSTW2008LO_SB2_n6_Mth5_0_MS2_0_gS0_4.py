#--------------------------------------------------------------
# MC12 common configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py")

#--------------------------------------------------------------
# Configuration for Charybdis2
#--------------------------------------------------------------
evgenConfig.description = "Charybdis2 + Pythia8 with the AU2 MSTW2008LO tune, SB2_n6_Mth5_0_MS2_0_gS0_4"
evgenConfig.keywords = ["charybdis2"]
evgenConfig.generators += ["Lhef","Charybdis2"]
evgenConfig.inputfilecheck = "Charybdis2"
