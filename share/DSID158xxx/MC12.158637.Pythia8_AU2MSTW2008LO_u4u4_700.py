evgenConfig.description = "Fourth generation t't'bar production with mass(t')=700 and t'->qW"
evgenConfig.keywords = ["exotics", "4thgen" ,"tprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# turn on the 4th generation pair processes
topAlg.Pythia8.Commands += ["FourthTop:gg2tPrimetPrimebar = on"]
topAlg.Pythia8.Commands += ["FourthTop:qqbar2tPrimetPrimebar = on"]

# set mass 
topAlg.Pythia8.Commands += ["8:m0=700"]

# set width
topAlg.Pythia8.Commands += ["8:mWidth=3.40517"]
topAlg.Pythia8.Commands += ["8:doForceWidth = on"]

# set CKM4 matrix elements
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimed = 0.707"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimes = 0.707"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimeb = 1"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimebPrime = 0"]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]

