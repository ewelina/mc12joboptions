include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ADD->mumu production"
evgenConfig.keywords = [ "ADD" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> e+ e-
  #
  Process 93 93 -> 13 -13 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 13 -13 120.0 300.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3          
  M_S   = 3500      
  M_CUT = 3500
  KK_CONVENTION = 5   

  MASS[39] = 100. 
  MASS[40] = 100.   

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   
 
 
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
