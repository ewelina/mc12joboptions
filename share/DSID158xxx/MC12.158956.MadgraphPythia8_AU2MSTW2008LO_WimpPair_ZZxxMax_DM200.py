include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

evgenConfig.description = "mono z leptonic : zzxxmax DM 200 GeV"
evgenConfig.keywords = ["MonoZ", "leptonic"]
evgenConfig.contact  = ["andrew.james.nelson@cern.ch"]


topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2",
                            ]
evgenConfig.inputfilecheck = 'WimpPair'
