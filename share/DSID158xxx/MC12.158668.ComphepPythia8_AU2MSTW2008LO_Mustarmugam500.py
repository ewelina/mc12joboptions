include ( "MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include ( "MC12JobOptions/Pythia8_CompHep.py" )

include ( "MC12JobOptions/PhotonFilter.py" ) 
topAlg.PhotonFilter.Ptcut = 15000.
topAlg.PhotonFilter.Etacut = 2.7
topAlg.PhotonFilter.NPhotons = 1

include ( "MC12JobOptions/MultiLeptonFilter.py" )
topAlg.MultiLeptonFilter.Ptcut = 15000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2

evgenConfig.inputfilecheck = "Mustar"
evgenConfig.minevents = 5000
evgenConfig.description = "excited muon m*=500GeV, Lambda=5TeV (mu mu* -> mu mu gamma)"
evgenConfig.keywords = ["exotics","compositeness","ExcitedMuon"]
