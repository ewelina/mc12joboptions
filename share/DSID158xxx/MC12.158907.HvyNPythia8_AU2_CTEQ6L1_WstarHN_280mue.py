evgenConfig.description = 'HvyN+Pythia8 heavy neutrinos, mN=280, channel=mue with CTEQ6L1 PDF'
evgenConfig.keywords = ['heavynu']
evgenConfig.generators += ['HvyN']
evgenConfig.inputfilecheck = 'HN280mue'
include('MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py')
include('MC12JobOptions/Pythia8_LHEF.py')