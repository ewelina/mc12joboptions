

evgenConfig.generators = ["MadGraph", "Pythia8"]
evgenConfig.description = "125GeV Higgs to displaced leptonjets; mixed final state; 2 dark photons + MET"
evgenConfig.keywords = ["leptonjets", "higgs"]
evgenConfig.inputfilecheck = 'MadGraph.*H125toLJ_2zd_mixed_MET'

## Config for Pythia8 tune AU2 with CTEQ6L1
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

## Configure Pythia8 to read input events from an LHEF file
include("MC12JobOptions/Pythia8_LHEF.py")

## Photos
include("MC12JobOptions/Pythia8_Photos.py")

