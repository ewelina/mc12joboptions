evgenConfig.description = "WIMP Model generation with MadGraph/Pythia6 in MC12"
evgenConfig.keywords = ["WimpPair"]
evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.contact = ["jgramlin@cern.ch", "david.berge@cern.ch"]

include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
			  	 "pyinit pylisti -1", # Changes listing of particles in log file
                          	 "pyinit pylistf 1", # Changes listing of particles in log file
                                 "pyinit dumpr 1 2",
                                 "pydat1 parj 90 20000.", # Turn off FSR for photons (use Photos instead, below)
                                 "pydat3 mdcy 15 1 0", # Turn off tau decays (use Tauol instead, below)
				]            


evgenConfig.inputfilecheck = 'group.phys-gener.madgraph.158424.WimpPair_D5_DM1000.TXT.mc12_v1'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0 
      showerkt=T
      qcut=200
      imss(21)=24
      imss(22)=24  
    """
    
phojf.write(phojinp)
phojf.close()
