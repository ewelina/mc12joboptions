include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ADD->ee production"
evgenConfig.keywords = [ "ADD" ]

evgenConfig.process="""
(processes){
  #
  # jet jet -> e+ e-
  #
  Process 93 93 -> 11 -11 93{1};
  CKKW sqr(20.0/E_CMS);
  End process;
}(processes)

(selector){
  Mass 11 -11 300.0 600.0
}(selector)

(model){
  MODEL = ADD

  N_ED  = 3          
  M_S   = 50000      
  M_CUT = 50000
  KK_CONVENTION = 5   

  MASS[39] = 100. 
  MASS[40] = 100.   

}(model)

(me){
  ME_SIGNAL_GENERATOR = Amegic
}(me)


"""
   
 
 
evgenConfig.minevents = 5000
evgenConfig.weighting = 0
