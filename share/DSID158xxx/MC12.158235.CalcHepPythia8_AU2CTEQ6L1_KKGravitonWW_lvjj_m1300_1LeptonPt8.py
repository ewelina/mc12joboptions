
###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Bulk RS Graviton->WW->lvjj with AU2 CTEQ6L1 tune and 1 lepton filter"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["Lhef", "Pythia8"]
evgenConfig.inputfilecheck = 'group.phys-gener.CalcHep.158235.KKGravitonWW_lvjj_m1300.TXT'

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += ["ParticleDecays:sophisticatedTau = 0"]

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 1
