
# author: Attilio Picazio

#Wprime Mass (in GeV)
M_Wprime = 2400.0  

evgenConfig.description = "Wprime "+str(M_Wprime)+" GeV, with the MSTW2008LO PDF and A2 tunee"
evgenConfig.keywords = ["Wprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2Wprime = on",
                           "34:m0 = "+str(M_Wprime), # Wprime mass
                           "Wprime:coup2WZ = 1.", #Wprime Coupling to WZ
                           "34:onMode = off", #turn off all Wprime decays
                           "34:onIfMatch = 24 23", #Wprime->WZ
                           "24:onMode = off", #turn off all W decays
	                   "24:onIfAny = 1 2 3 4 5 6 ", #W->qq
	                   "23:onMode = off", #turn off all Z decays
	                   "23:onIfAny = 1 2 3 4 5 6" #Z->qq
                           ]

  