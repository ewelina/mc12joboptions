



  


evgenConfig.description = "CI->mumu production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CImumu", "muons", "mumu"]

 
 

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
 
 

topAlg.Pythia8.Commands += [
                            "ContactInteractions:QCffbar2mumubar = on", # process
                            "ContactInteractions:Lambda = 7000", # mass scale
                            "ContactInteractions:etaLL = -1", # interference
                            "PhaseSpace:mHatMin = 600", # min invariant mass
                            "PhaseSpace:mHatMax = 1200" # max invariant mass
                            ]       

 

 
 
