#################################
##
##       LHE file generated with pythia6 stand-alone with parameter resmass = mass of rho_T
##       
##       msub(370)=1   ! W_L Z_L
## 
##       pmas(pycomp(3000111),1) = resmass-80. ! pi_tc0
##       pmas(pycomp(3000211),1) = resmass-80. ! pi_tc+
##       pmas(pycomp(3000113),1) = resmass ! rho_tc0
##       pmas(pycomp(3000213),1) = resmass ! rho_tc+
##       pmas(pycomp(3000223),1) = resmass ! omega_tc
##       pmas(pycomp(3000115),1) = resmass*1.1 ! a_tc0 
##       pmas(pycomp(3000215),1) = resmass*1.1 ! a_tc+ 
##       pmas(pycomp(3000221),1) = 5000. ! pi'_tc0
##       pmas(pycomp(3000331),1) = 5000. ! eta_tc0
## c
##       RTCM(2) = 1.  ! Q_U
##       RTCM(3)= 1./3. ! sin(Chi)
##       RTCM(12)=resmass ! 
##       RTCM(13)=resmass ! 
##       RTCM(48)=resmass ! 
##       RTCM(49)=resmass ! 
##       RTCM(50)=5000. ! vector technimeson decay parameter
##       RTCM(51)=5000. ! axial mass parameter
##       ITCM(1) = 4  ! N_TC
## c.. leptonic (e and mu only) decay of W and Z
##       do i=174,209
##          mdme(i,1)=min(mdme(i,1),0)
##       end do
##       mdme(182,1)=1
##       mdme(184,1)=1
##       mdme(206,1)=1
##       mdme(207,1)=1
##
## c.. use LHAPDF
##       mstp(52)=2
##       mstp(51)=21000   ! MSTW2008lo central value
#################################
#
#  choose the pdf
# ---------------
# include("MC12JobOptions/Pythia8_AU2_CT10_Common.py") 
# include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py") 
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include ("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]
## ... Tauola
# include ( "MC12JobOptions/Pythia8_Tauola.py" )
## ... Photos
# include ( "MC12JobOptions/Pythia8_Photos.py" )

evgenConfig.description = "LSTC WZ -> lvll "
evgenConfig.keywords = ["LSTC Dibosons multileptons"]
evgenConfig.inputfilecheck = 'wz_lvll'
