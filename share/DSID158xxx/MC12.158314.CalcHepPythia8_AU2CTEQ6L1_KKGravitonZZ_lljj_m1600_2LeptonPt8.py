
###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Bulk RS Graviton->ZZ->lljj with AU2 CTEQ6L1 tune and 2 lepton filter"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["Lhef", "Pythia8"]
evgenConfig.inputfilecheck = 'group.phys-gener.CalcHep.158314.KKGravitonZZ_lljj_m1600.TXT'

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += ["ParticleDecays:sophisticatedTau = 0"]

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
