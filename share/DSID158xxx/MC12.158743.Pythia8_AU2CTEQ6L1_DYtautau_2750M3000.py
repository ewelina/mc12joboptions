## Pythia8 DYtautau_2750M3000

evgenConfig.description = "Pythia8 DYtautau with mass cut 2750<M<3000GeV without lepton filter and AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "Z/gamma*", "leptons", "tau"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # create Z bosons
                            "PhaseSpace:mHatMin = 2750.", # lower invariant mass
                            "PhaseSpace:mHatMax = 3000.", # upper invariant mass
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 15"] # switch on Z->tautau decays
