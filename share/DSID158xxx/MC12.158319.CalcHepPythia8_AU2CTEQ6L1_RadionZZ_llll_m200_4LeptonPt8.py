
###############################################################
#
# Job options file
#
#-----------------------------------------------------------------------------
evgenConfig.description = "Radion->ZZ->llll with AU2 CTEQ6L1 tune and 4 lepton filter"
evgenConfig.keywords = ["Exotics", "Graviton" ,"WarpedED"]
evgenConfig.generators = ["Lhef", "Pythia8"]
evgenConfig.inputfilecheck = 'group.phys-gener.CalcHep.158319.RadionZZ_llll_m200.TXT'
evgenConfig.minevents=9500

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

topAlg.Pythia8.Commands += ["ParticleDecays:sophisticatedTau = 0"]

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 4
