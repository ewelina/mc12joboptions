include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z to mu mu gamma production with up to three jets in ME+PS and pT_gamma>70 GeV."
evgenConfig.keywords = [ "EW", "muon", "gamma" ]
evgenConfig.inputconfcheck = "mumugammaPt70"
evgenConfig.minevents = 2000
evgenConfig.contact  = [ "frank.siegert@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
}(run)

(processes){
  Process 93 93 ->  13 -13 22 93{3}
  Order_EW 3
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {5,6}
  End process;
}(processes)

(selector){
  Mass 90 90 40 7000
  PT 22  70 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
