
evgenConfig.description = "MadGraph5+Pythia8 for SM 4top"

evgenConfig.keywords = ["SM,4top"]

evgenConfig.inputfilecheck = "4top"

evgenConfig.contact = ["Emmanuel Busato"]

evgenConfig.generators = ["MadGraph", "Pythia8"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")

include("MC12JobOptions/Pythia8_Photos.py")


