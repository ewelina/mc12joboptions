
evgenConfig.inputfilecheck = 'TypeIII'
evgenConfig.description = "Type III SeeSaw Model Mass: 120 GeV Vu=0.063 Ve=0.055"
evgenConfig.keywords = ["TypeIIISeeSaw"] 

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")

#print out some pythia info
topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 10",
                            "Next:numberShowEvent = 10"]

include('MC12JobOptions/MultiLeptonFilter.py')
MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut       = 4000
MultiLeptonFilter.Etacut      = 3
MultiLeptonFilter.NLeptons = 2
