# Author Margar Simonyan (at SPAMNOTcern.ch)

evgenConfig.description = "LQ3 ->top + tau^+, M_LQ = 400 GeV, Pythia6"
evgenConfig.keywords    = [ "exotics", "leptoquark" ]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand +=  ["pysubs msel 0"          ,
                             #   "pysubs msub 162 1"      ,     # qg    -> lLQ
                                 "pysubs msub 163 1"      ,     # gg    -> LQLQ
                                 "pysubs msub 164 1"      ,     # qqbar -> LQLQ
                                 "pydat2 pmas 42 1 400."  ,
                                 "pysubs ckin 41 300.0"   ,
                                 "pysubs ckin 42 500.0"   ,
                                 "pysubs ckin 43 300.0"   ,
                                 "pysubs ckin 44 500.0"   ,
                                 "pydat3 brat 539 1."     ,
                                 "pydat3 kfdp 539 1 6"    ,
                                 "pydat3 kfdp 539 2 -15"  ,
                                 "pydat1 paru 151 0.01"
                                 ]

topAlg.Pythia.PythiaCommand += ["pyinit dumpr 1 20"]

include ( "MC12JobOptions/Pythia_Tauola.py" )
include ( "MC12JobOptions/Pythia_Photos.py" )

