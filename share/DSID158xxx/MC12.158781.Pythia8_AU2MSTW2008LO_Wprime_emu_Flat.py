#--------------------------------------------------------------
# Author 1: S. Ask (Cambrdige U.), 21 May 2012
# Author 2: D. Hayden (RHUL.), 3rd August 2012
# Modifications for Wprime->e,mu flat sample by Nikos Tsirintanis, Mihail Chizhov
# Based on Generators/Pythia8_i/share/jobOptions.pythia8.py
#--------------------------------------------------------------

evgenConfig.description = "Wprime->e,mu + nu production with AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Wprime", "leptons"]


include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

#Wprime -> e,mu flat
topAlg.Pythia8.Commands += ['PDF:pSet= 5'] #MSTW2008
topAlg.Pythia8.Commands +=['NewGaugeBoson:ffbar2Wprime = on'] # Create Wprime
topAlg.Pythia8.Commands +=['PhaseSpace:mHatMin = 0.0'] # lower invariant mass
topAlg.Pythia8.Commands +=['PhaseSpace:mHatMax = 4000.0'] # higher invariant mass
topAlg.Pythia8.Commands +=['34:m0 =1000.0']# initial mass, does not matter
topAlg.Pythia8.Commands +=['34:onMode = off']# no decays except...
topAlg.Pythia8.Commands +=['34:onIfAny = 11,12']#e
topAlg.Pythia8.Commands +=['34:onIfAny = 13,14']#mu

topAlg.Pythia8.UserHook = "WprimeFlat" # apply userhooks
