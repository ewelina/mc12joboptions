evgenConfig.description = 'HvyN+HERWIG+Jimmy heavy neutrinos, mass=200, channel=mumu with CTEQ6L1 PDF'
evgenConfig.keywords = ['heavynu']
evgenConfig.inputfilecheck = 'HN200mumu'
include('MC12JobOptions/AlpgenJimmy_AUET2_CTEQ6L1_Common.py')
include('MC12JobOptions/Jimmy_Tauola.py')
include('MC12JobOptions/Jimmy_Photos.py')
