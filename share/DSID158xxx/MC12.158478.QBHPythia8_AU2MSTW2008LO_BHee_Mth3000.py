evgenConfig.description = "Quantum black holes (M_th = 3000 GeV) decaying to one electron and one positron."
evgenConfig.contact = ["gingrich@ualberta.ca", "areinsch@uoregon.edu"]
evgenConfig.keywords = ["exotics", "blackholes", "electron", "positron"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py" )
