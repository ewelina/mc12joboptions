






evgenConfig.description = "CI->ee production with MSTW2008LO tune"
evgenConfig.keywords = ["Contact Interaction", "CIee", "electrons", "ee"]

 
 

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
 
 

topAlg.Pythia8.Commands += [
                            "ContactInteractions:QCffbar2eebar = on", # process
                            "ContactInteractions:Lambda = 14000", # mass scale
                            "ContactInteractions:etaLL = -1", # interference
                            "PhaseSpace:mHatMin = 300", # min invariant mass
                            "PhaseSpace:mHatMax = 600" # max invariant mass
                            ]       

 

 
 
