include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                            "Next:numberShowLHA = 2",
                            "Next:numberShowEvent = 2",
                            ]

evgenConfig.description = "mono w leptonic decay : D52 DM 100 GeV"
evgenConfig.keywords = ["MonoW", "leptonic"]
evgenConfig.inputfilecheck = 'wlep'
