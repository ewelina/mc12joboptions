evgenConfig.description = "Z*->ee template production  AU2 MSTW2008LO tune"
evgenConfig.keywords = ["electroweak", "Z*", "leptons"]
#
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_CalcHep.py")
include("MC12JobOptions/Pythia8_Photos.py")
#
evgenConfig.inputfilecheck = 'ZStar'

