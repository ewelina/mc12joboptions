evgenConfig.description = "Fourth generation b'b'bar production with mass(b')=450 and b'->qW"
evgenConfig.keywords = ["Exotics", "FourGeneration" ,"bprime"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# turn on the 4th generation pair processes
topAlg.Pythia8.Commands += ["FourthBottom:gg2bPrimebPrimebar = on"]
topAlg.Pythia8.Commands += ["FourthBottom:qqbar2bPrimebPrimebar = on"]

# set mass 
topAlg.Pythia8.Commands += ["7:m0=450"]

# set width
topAlg.Pythia8.Commands += ["7:mWidth=0.91357"]
topAlg.Pythia8.Commands += ["7:doForceWidth = on"]

# set CKM4 matrix elements
topAlg.Pythia8.Commands += ["FourthGeneration:VubPrime = 1"]
topAlg.Pythia8.Commands += ["FourthGeneration:VcbPrime = 1"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtbPrime = 1"]
topAlg.Pythia8.Commands += ["FourthGeneration:VtPrimebPrime = 0"]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
topAlg += MultiLeptonFilter()

MultiLeptonFilter = topAlg.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10.*GeV
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 1

StreamEVGEN.RequireAlgs += [ "MultiLeptonFilter" ]

