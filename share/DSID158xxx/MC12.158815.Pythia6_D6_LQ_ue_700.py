evgenConfig.description = "Pythia6 Leptoquark (M = 700 GeV) to di-electron jet channel"
evgenConfig.keywords = [ "Exotics", "Leptoquark" ]
evgenConfig.contact = ['vikas.bansal@cern.ch']

include("MC12JobOptions/Pythia_D6_Common.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0"]

#    gg -> LQLQ
topAlg.Pythia.PythiaCommand += ["pysubs msub 163 1"]

#    qqbar -> LQLQ
topAlg.Pythia.PythiaCommand += ["pysubs msub 164 1"]

#  LQ mass
topAlg.Pythia.PythiaCommand += ["pydat2 pmas 42 1 700."]

#  To avoid problems in MC generation
topAlg.Pythia.PythiaCommand += ["pysubs ckin 41 500.0",
                                "pysubs ckin 42 900.0",
                                "pysubs ckin 43 500.0",
                                "pysubs ckin 44 900.0",]

#  Branching fraction for the decay described below
topAlg.Pythia.PythiaCommand += ["pydat3 brat 539 1."]

#  This is relevant to both the decay AND production mechanism for single LQ
topAlg.Pythia.PythiaCommand += ["pydat3 kfdp 539 1 2",
                                "pydat3 kfdp 539 2 11",]

#  Coupling: lambda=sqrt(4pi*alpha_em)
topAlg.Pythia.PythiaCommand += ["pydat1 paru 151 0.01"]

# ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )
#
# ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )
