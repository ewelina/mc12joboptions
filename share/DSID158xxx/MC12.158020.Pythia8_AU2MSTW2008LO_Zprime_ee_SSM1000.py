##################################
###     Job Options File       ###
### Prepared by:               ###
### Elizabeth Castaneda-Miranda###
### Daniel Hayden              ###
###     SSM Zprime* -> ee      ###
##################################

evgenConfig.description = "Z'->ee production"
evgenConfig.keywords = ["HeavyBoson", "Zprime", "leptons"]

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# Zprime resonance mass (in GeV)
ZprimeMass = 1000
# Minimum mass for Drell-Yan production (in GeV)
ckin1 = 500

topAlg.Pythia8.Commands += ["NewGaugeBoson:ffbar2gmZZprime = on",    # create Z' bosons
                            "Zprime:gmZmode = 0",                    # Z',Z,g with interference
                            "PhaseSpace:mHatMin = "+str(ckin1), # lower invariant mass
                            "32:onMode = off", # switch off all Z decays
                            "32:onIfAny  = 11", # switch on Z->ee decays
                            "32:m0 = "+str(ZprimeMass)]
