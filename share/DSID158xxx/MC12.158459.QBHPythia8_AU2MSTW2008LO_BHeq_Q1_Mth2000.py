evgenConfig.description = "Quantum black holes (M_th = 2000 GeV, charge: 1/3) decaying to one electron and one jet."
evgenConfig.contact = ["areinsch@uoregon.edu", "mshamim@cern.ch", "gingrich@ualberta.ca"]
evgenConfig.keywords = ["exotics", "blackholes", "e", "jets"]
evgenConfig.generators += ["Lhef"]
evgenConfig.inputfilecheck = "BH"

include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py" )
include("MC12JobOptions/Pythia8_LHEF.py")
include("MC12JobOptions/Pythia8_Photos.py" )
