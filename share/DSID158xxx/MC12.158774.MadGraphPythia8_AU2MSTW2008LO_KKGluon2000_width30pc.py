#--------------------------------------------------------------
# MC12 common configuration
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
include("MC12JobOptions/Pythia8_Photos.py")



#--------------------------------------------------------------
# Configuration for BlackMax
#--------------------------------------------------------------
evgenConfig.description = "MadGraph5+Pythia8 mstw2008LO  -  KKGluonTTBar  -  pp -> o1 -> ttbar ALLchannels  -  where o1 is the colour octet state in the topBSM model  -  Couplings according to Randall & Lillie  -  GAMMA/M=30%  -  M=2000 GeV"
evgenConfig.keywords = ["MadGraph", "Pythia", "mstw2008LO", "KKGluon"]
evgenConfig.inputfilecheck = "KKGluonTTbar"
