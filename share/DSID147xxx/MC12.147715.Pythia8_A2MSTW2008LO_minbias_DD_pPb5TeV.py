# Pythia shifted to pPb
# A .Olszewski December 2012

evgenConfig.description = "Double diffractive events, with the A2 MSTW2008LO tune, shifted to pPb"
evgenConfig.keywords = ["QCD", "minbias"]

include("MC12JobOptions/Pythia8_A2_MSTW2008LO_Common.py")
#include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.McEventKey = "PYTHIA_EVENT"
topAlg.Pythia8.Commands += ["SoftQCD:doubleDiffractive = on"]

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
boost=topAlg.BoostEvent
boost.BetaZ=-0.43448
boost.McInputKey="PYTHIA_EVENT"
boost.McOutputKey="GEN_EVENT"

if "BoostEvent" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["BoostEvent"]

evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")

from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV

StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
