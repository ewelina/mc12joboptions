## PYTHIA W->mu,nu

evgenConfig.description = "W->mu,nu production with no lepton filter and AUET2B CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Photos.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                                "pysubs msub 2 1"] # Create W bosons

include("MC12JobOptions/Pythia_Decay_Wmunu.py")
