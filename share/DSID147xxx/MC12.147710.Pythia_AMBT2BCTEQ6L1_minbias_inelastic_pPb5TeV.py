# Pythia shifted to pPb
# A .Olszewski November 2012

evgenConfig.description = "Inelastic minimum bias events, with PYTHIA6 shifted to pPb"
evgenConfig.keywords = ["QCD", "minbias", "pileup"]

include("MC12JobOptions/Pythia_AMBT2B_CTEQ6L1_Common.py")

topAlg.Pythia.McEventKey = "PYTHIA_EVENT"
topAlg.Pythia.PythiaCommand += \
    ["pysubs msel 0",
     # ND
     "pysubs msub 11 1",
     "pysubs msub 12 1",
     "pysubs msub 13 1",
     "pysubs msub 28 1",
     "pysubs msub 53 1",
     "pysubs msub 68 1",
     "pysubs msub 95 1",
     # SD
     "pysubs msub 92 1",
     "pysubs msub 93 1",
     # DD
     "pysubs msub 94 1"]

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
boost=topAlg.BoostEvent
boost.BetaZ=-0.43448
boost.McInputKey="PYTHIA_EVENT"
boost.McOutputKey="GEN_EVENT"

if "BoostEvent" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["BoostEvent"]

evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")

from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV

StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
