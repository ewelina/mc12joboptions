## POWHEG+PYTHIA W->tau,nu

evgenConfig.description = "POWHEG+PYTHIA W->tau,nu production with the AUET2B CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplustaunu"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

## Specify W decay mode
include("MC12JobOptions/Pythia_Decay_Wtaunu.py")
