include("MC12JobOptions/Sherpa_CT10_Common.py")
evgenConfig.description = "VBF-W production, aTGC"
evgenConfig.keywords = [ "Sherpa", "VBF", "EW"]
evgenConfig.contact  = [ "jhenders@cern.ch" ]
evgenConfig.minevents = 500
evgenConfig.process="""
(run){
EW_TCHAN_MODE=1
}(run)

(processes){
!positron
Process 93 93  -> 11 -12 93 93 93{1}
Order_EW 4
Min_N_TChannels 1
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

!electron
Process 93 93  -> -11 12 93 93 93{1}
Order_EW 4
Min_N_TChannels 1
Integration_Error 5.e-2
CKKW  sqr(15/E_CMS)

End process;

}(processes)

(selector){
Mass -11 12 10 E_CMS
Mass 11 -12 10 E_CMS
NJetFinder 2  15.0  0.0  0.4 1
}(selector)

(me){ 
ME_SIGNAL_GENERATOR = Amegic
}(me)

(model){ 
ACTIVE[6]=0 
MODEL=SM+AGC

KAPPA_GAMMA=1.0
KAPPA_Z=1.0
G1_GAMMA=1.0 
G1_Z=1.0

LAMBDA_GAMMA=-0.01
LAMBDA_Z=-0.01
 
UNITARIZATION_SCALE=100000 
UNITARIZATION_N=2
 
}(model)

"""

topAlg.Sherpa_i.ResetWeight = 0
#evgenConfig.inputconfcheck = ''

try:
    from JetRec.JetGetters import *
    antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
    antikt4alg = antikt4.jetAlgorithmHandle()
    antikt4alg.OutputLevel = INFO
except Exception, e:
    pass


from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=[ "VBFMjjIntervalFilter" ]

