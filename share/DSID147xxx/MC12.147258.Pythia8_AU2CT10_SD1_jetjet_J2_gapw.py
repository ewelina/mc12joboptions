evgenConfig.description = "PYTHIA 8 SD dijet production with CT10 pdf and AU2 CT10 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 100

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")


## no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = off",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = off"]

#
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

## Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 30.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 3.5 # below this value the filtering is applied

###MC12 AUT_C0 tune, parameters of double exponential fit for gap distribution
topAlg.GapJetFilter.c0 = 0.482301
topAlg.GapJetFilter.c1 = 0.394178
topAlg.GapJetFilter.c2 = -4.80907
topAlg.GapJetFilter.c3 = 0.00524406
topAlg.GapJetFilter.c4 = 2.72373
topAlg.GapJetFilter.c5 = -1.33205
topAlg.GapJetFilter.c6 = -0.000287871
topAlg.GapJetFilter.c7 = -1.553

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]

