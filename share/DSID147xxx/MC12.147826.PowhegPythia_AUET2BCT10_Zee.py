## POWHEG+PYTHIA Z->ee

evgenConfig.description = "POWHEG+PYTHIA Z->ee production with no lepton filter and AUET2B CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zee"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Photos.py")

## Specify Z decay mode
include("MC12JobOptions/Pythia_Decay_Zee.py")
