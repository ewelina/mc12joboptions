## POWHEG+Pythia8 Wplus->enu

evgenConfig.description = "POWHEG+Pythia8 Wplus->enu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wplusenu"

ver =  os.popen("cmt show versions External/Pythia8").read()
if 'Pythia8-01' in ver:
    include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
else:
    include("MC12JobOptions/Pythia82_AU2_CT10_Common.py")

include("MC12JobOptions/Pythia8_Photos.py")
