include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Wm_taunu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Wmin->taunu production with central lepton pt>10 GeV filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "W", "leptonic", "tau"]

# compensate filter efficiency
evt_multiplier = 15

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.7
