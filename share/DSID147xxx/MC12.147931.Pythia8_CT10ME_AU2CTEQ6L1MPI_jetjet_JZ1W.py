evgenConfig.description = "Dijet truth jet slice JZ1W, with the AU2 CTEQ6L1 MPI tune and CT10 matrix element PDF"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_CT10ME_AU2CTEQ6L1MPI_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ1W.py")
evgenConfig.minevents = 500
