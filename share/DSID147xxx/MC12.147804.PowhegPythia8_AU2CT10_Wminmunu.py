## POWHEG+Pythia8 Wmin->munu

evgenConfig.description = "POWHEG+Pythia8 Wmin->munu production without lepton filter and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminmunu"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
