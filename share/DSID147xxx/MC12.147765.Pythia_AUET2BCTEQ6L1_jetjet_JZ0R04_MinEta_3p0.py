evgenConfig.description = "Dijet truth jet slice JZ1 at eta>3.0 (per. B), with Pythia 6 AUET2B and R=0.4"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

# use min bias ND for this slice
topAlg.Pythia.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           ]


include("MC12JobOptions/JetFilter_JZ0R04.py")
include("MC12JobOptions/JetFilterFragment_MinEta_3p0.py")
evgenConfig.minevents = 5000
include("MC12JobOptions/PutAlgsInSequence.py")
