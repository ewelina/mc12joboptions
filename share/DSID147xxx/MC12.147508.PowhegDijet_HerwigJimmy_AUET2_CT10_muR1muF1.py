###############################################################
#
# Job options file for POWHEG with Herwig/Jimmy
# J. Robinson, 14.04.2013 <james.robinson@cern.ch>
#
#==============================================================

#--------------------------------------------------------------
# Configuration for Evgen Job Transforms
#--------------------------------------------------------------
evgenConfig.description    = "POWHEG+fHerwig/Jimmy dijet production with bornktmin = 5 GeV, bornsuppfact = 250 GeV, muR=muF=1 and AUET2 CT10 tune"
evgenConfig.keywords       = [ "QCD", "dijet", "jets" ]
evgenConfig.contact        = [ "james.robinson@cern.ch" ]
evgenConfig.generators    += [ "Powheg", "Herwig" ]
evgenConfig.inputfilecheck = "Powheg_Dijet_r2169_muR1muF1"
evgenConfig.weighting      = 0      # to avoid failure with high weights - TODO: remove this if fix is implemented
evgenConfig.minevents      = 48000  # allow some of the 50k events to fail TestHepMC 

## Herwig/Jimmy Showering
include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")
