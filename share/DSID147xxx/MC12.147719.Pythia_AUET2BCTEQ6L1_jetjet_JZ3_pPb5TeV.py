evgenConfig.description = "Dijet truth jet slice JZ3, with Pythia 6 AUET2B moved to pPb 5TeV frame"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

topAlg.Pythia.PythiaCommand += [
                                  "pysubs msel 0",
                                  "pysubs ckin 3 100.",
                                  "pysubs msub 11 1",
                                  "pysubs msub 12 1",
                                  "pysubs msub 13 1",
                                  "pysubs msub 68 1",
                                  "pysubs msub 28 1",
                                  "pysubs msub 53 1"]


include("MC12JobOptions/JetFilter_JZ3.py")
evgenConfig.minevents = 2000
include("PutAlgsInSequence.py")


topAlg.Pythia.McEventKey = "PYTHIA_EVENT"
topAlg.QCDTruthJetFilter.McEventKey = "PYTHIA_EVENT"

if hasattr(topSequence,'INav4MomTruthEventBuilder'):
    truthParticleBuilder = getattr(topSequence,'INav4MomTruthEventBuilder')
    truthParticleBuilder.CnvTool.McEvents = "PYTHIA_EVENT"

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
boost=topAlg.BoostEvent
boost.BetaZ=-0.43448
boost.McInputKey="PYTHIA_EVENT"
boost.McOutputKey="GEN_EVENT"

if "BoostEvent" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["BoostEvent"]
    
evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")
    
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV
    
StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
