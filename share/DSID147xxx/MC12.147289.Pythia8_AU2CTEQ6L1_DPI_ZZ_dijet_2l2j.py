evgenConfig.description = "DPI production of ZZ->llll/llvv/vvvv and TwoJets. Dilepton filter of pT>10 GeV. Dijet filter of pT>15 GeV, dijet mass>100 Gev."
evgenConfig.keywords = ["electroweak", "ZZ", "leptons", "l", "TwoJets", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakDoubleBoson:ffbar2gmZgmZ = on", # create ZZ
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:TwoJets = on", # TwoJets
                            "PhaseSpace:sameForSecond = off", # turn off the default option 
                            "PhaseSpace:pTHatMinSecond = 5.", # pTMin on the second hard process
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 11", # switch on Z->e,e decays
                            "23:onIfAny = 12", # switch on Z->enu,enu decays
                            "23:onIfAny = 13", # switch on Z->u,u decays
                            "23:onIfAny = 14", # switch on Z->unu,unu decays
                            "23:onIfAny = 15", # switch on Z->t,t decays
                            "23:onIfAny = 16"] # switch on Z->tnu,tnu decays

# 2-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10.*GeV
topAlg.MultiLeptonFilter.Etacut = 10.
topAlg.MultiLeptonFilter.NLeptons = 2

# 2-jet filter
include("MC12JobOptions/VBFForwardJetsFilter.py")
topAlg.VBFForwardJetsFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.VBFForwardJetsFilter.NJets = 2
topAlg.VBFForwardJetsFilter.JetMinPt = 15.*GeV
topAlg.VBFForwardJetsFilter.JetMaxEta = 10.
topAlg.VBFForwardJetsFilter.Jet1MinPt = 15.*GeV
topAlg.VBFForwardJetsFilter.Jet1MaxEta = 10.
topAlg.VBFForwardJetsFilter.Jet2MinPt = 15.*GeV
topAlg.VBFForwardJetsFilter.Jet2MaxEta = 10.
topAlg.VBFForwardJetsFilter.UseOppositeSignEtaJet1Jet2 = 0
topAlg.VBFForwardJetsFilter.MassJJ = 100.*GeV
topAlg.VBFForwardJetsFilter.DeltaEtaJJ = 0
topAlg.VBFForwardJetsFilter.UseLeadingJJ = 0
