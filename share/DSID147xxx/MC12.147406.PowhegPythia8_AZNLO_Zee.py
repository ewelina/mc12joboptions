## POWHEG+Pythia8 Z->ee

include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_ee"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Z->ee production without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "e"]

include("MC12JobOptions/PowhegControl_postInclude.py")
