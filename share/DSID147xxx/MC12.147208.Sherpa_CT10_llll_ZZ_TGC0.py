include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Sherpa 1.4.0 diboson llll, TGC sample, up to 1 jet with ME+PS."
evgenConfig.keywords = ["diboson", "llll", "ZZ","TGC"]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "andrew.james.nelson@cern.ch" ]

evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ERROR=0.02
  ME_SIGNAL_GENERATOR=Amegic
  MASSIVE[15]=1
}(run)

(model){
  MODEL         = SM+AGC

  F4_GAMMA=0.1
  F5_GAMMA=0.0
  F4_Z=0.0
  F5_Z=0.0

  UNITARIZATION_SCALE = 10000.0
  UNITARIZATION_N = 0
}(model)

(processes){
  Process 93 93 -> 90 90 90 90 93{1};
  Integration_Error 0.05 {6,7}
  Order_EW 4
  CKKW sqr(20/E_CMS)
  Scales LOOSE_METS{MU_F2}{MU_R2} {7}
  End process;
}(processes)

(selector){
  Mass  11 -11  4.0  E_CMS
  Mass  13 -13  4.0  E_CMS
  Mass  15 -15  4.0  E_CMS
  "PT" 90 5.0,E_CMS:5.0,E_CMS:5.0,E_CMS:5.0,E_CMS [PT_UP]
}(selector)
"""
