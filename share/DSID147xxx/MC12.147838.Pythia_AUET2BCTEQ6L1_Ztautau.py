## PYTHIA Z->tautau

evgenConfig.description = "Z->tautau production with the AUET2B CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "tau"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                                "pysubs msub 1 1",        # Create Z bosons
                                "pysubs ckin 1 60.0"]     # Lower invariant mass

include("MC12JobOptions/Pythia_Decay_Ztautau.py")
