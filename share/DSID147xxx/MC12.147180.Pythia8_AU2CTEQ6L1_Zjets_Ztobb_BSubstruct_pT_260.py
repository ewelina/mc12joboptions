evgenConfig.description = "High pT Z->bb production with AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["Z", "bottom", "hadronic", "boosted"]
evgenConfig.contact = [ "luke.lambourne@cern.ch" ]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

topAlg.Pythia8.Commands += ["WeakBosonAndParton:qqbar2gmZg = on", # create Z bosons
                            "WeakBosonAndParton:qg2gmZq = on", # create Z bosons
                            "PhaseSpace:mHatMin = 60.", # lower invariant mass
			    "PhaseSpace:pTHatMin = 130.", #lower ptHat cut
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 5"] # switch on Z->bb decays

include("MC12JobOptions/BSubstructFilter.py")
topAlg.BSubstruct.pTMin = 260000.  # MeV
