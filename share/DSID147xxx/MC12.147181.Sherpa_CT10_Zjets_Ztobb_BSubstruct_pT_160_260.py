evgenConfig.description = "High pT Z/gamma* -> bb + 1 or 2 jets."
evgenConfig.keywords = ["Z", "bottom", "hadronic", "boosted"]
evgenConfig.contact = [ "luke.lambourne@cern.ch" ]

include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.process="""
(processes){
  Process 93 93 -> 5 -5 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;
}(processes)

(selector){
  Mass 5 -5 60 E_CMS
  PT2  5 -5 70 E_CMS
}(selector)

(run){
  MASSIVE[5]=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "Ztobb_160_260"

include("MC12JobOptions/BSubstructFilter.py")
topAlg.BSubstruct.pTMin = 160000.  # MeV
topAlg.BSubstruct.pTMax = 260000.
