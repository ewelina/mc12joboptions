include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z/gamma* -> mm + up to 5 jets using Sherpa's built-in MENLOPS prescription."
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch" ]

evgenConfig.process="""
(processes){
  Process 93 93 -> 13 -13 93{5}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  NLO_QCD_Part BVIRS {2};
  ME_Generator Amegic {2};
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 13 -13 40 E_CMS
}(selector)

(run){
  NLO_Mode 3
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0
