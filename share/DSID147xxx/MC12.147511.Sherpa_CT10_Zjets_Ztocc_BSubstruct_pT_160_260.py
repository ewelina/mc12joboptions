## Sherpa boosted Z->cc
## Author: Luke Lambourne (luke.lambourne@cern.ch)

evgenConfig.description = "High pT Z/gamma* -> cc + 1 or 2 jets."
evgenConfig.keywords = ["Z", "charm", "hadronic", "boosted"]
evgenConfig.contact = [ "luke.lambourne@cern.ch" ]

include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.process="""
(processes){
  Process 93 93 -> 4 -4 93 93{1}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Cut_Core 1
  Integration_Error 0.05 {4}
  End process;
}(processes)

(selector){
  Mass 4 -4 60 E_CMS
  PT2  4 -4 70 E_CMS
}(selector)

(run){
  MASSIVE[4]=1
  MASSIVE[5]=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0

include("MC12JobOptions/Zbb_160_260_Filter.py")
evgenConfig.minevents = 500
evgenConfig.inputconfcheck = "147511.Ztocc_160_260"
