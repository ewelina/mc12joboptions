evgenConfig.description = "Dijet truth jet slice JZ0, with Pythia 6 AUET2B moved and R=0.4, min 1 muon 3.5 GeV pt cut"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

# use min bias ND for this slice
topAlg.Pythia.PythiaCommand += [  "pysubs msel 0",
                           # ND
                           "pysubs msub 11 1",
                           "pysubs msub 12 1",
                           "pysubs msub 13 1",
                           "pysubs msub 28 1",
                           "pysubs msub 53 1",
                           "pysubs msub 68 1",
                           "pysubs msub 95 1",
                           ]

#--------------------------------------------------------------
# Filter
#--------------------------------------------------------------
include("MC12JobOptions/JetFilter_JZ0R04.py")

include ( 'MC12JobOptions/MultiMuonFilter.py' )
topAlg.MultiMuonFilter.Ptcut = 3500.
topAlg.MultiMuonFilter.Etacut = 10.0
topAlg.MultiMuonFilter.NMuons = 1

evgenConfig.minevents = 5000
include("PutAlgsInSequence.py")
