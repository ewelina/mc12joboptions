include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "WZjj->lllnjj Order_EW=6"
evgenConfig.keywords = ["VBS", "Vector-Boson Scattering", "Diboson"]
evgenConfig.contact  = ["philipp.anger@cern.ch"]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
  ME_SIGNAL_GENERATOR=Comix;
}(run)

(model){
  MASS[5]    = 4.2;
  WIDTH[6]   = 1.523;
  MASS[11]   = 0.000510997;
  MASS[13]   = 0.105658389;
  MASS[15]   = 1.77705;
  ACTIVE[25] = 1;
  MASS[25]   = 126.0;
  WIDTH[25]  = 0.00418;
  # GF scheme:
  GF                  = 1.16639E-5;
  M_W                 = 80.399;
  M_Z                 = 91.1876;
  VEV                 = 246.2184581;
  SIN2THETAW          = 0.2226265156;
  LAMBDA              = 0.5237570374;
  1/ALPHAQED(default) = 132.3466351;
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 4000;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 4000;
}(beam)

(processes){
  Process 93 93 -> 93 93 90 90 90 91;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6};
  Order_EW 6;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  NJetFinder 2 15. 0. 0.4 -1;
  Mass 11 -11 0.1 E_CMS;
  Mass 13 -13 0.1 E_CMS;
  "PT" 90 5.0,E_CMS:5.0,E_CMS [PT_UP];
}(selector)
"""

evgenConfig.inputconfcheck = 'lllnjj_WZjj_EW6'
