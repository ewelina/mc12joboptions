## POWHEG+PYTHIA Z->mumu

evgenConfig.description = "POWHEG+PYTHIA Z->mumu production with no lepton filter and AUET2B CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Zmumu"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Photos.py")

## Specify Z decay mode
include("MC12JobOptions/Pythia_Decay_Zmumu.py")
