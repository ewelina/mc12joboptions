## POWHEG+Pythia8 WZ
evgenConfig.description = "POWHEG+Pythia8 WZ production and AU2 CT10 tune, cut on all SFOS pair mll>5GeV, at least one SFOS with  mll>30GeV"
evgenConfig.keywords = ["EW", "diboson", "leptonic"]
evgenConfig.inputfilecheck = "Powheg_CT10.*.WZ_Wm13Z13_gklwz_0_0_0p05_mll5p0d0"
evgenConfig.minevents  = 5000
evgenConfig.contact = ["Alexander Oh <alexander.oh@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

