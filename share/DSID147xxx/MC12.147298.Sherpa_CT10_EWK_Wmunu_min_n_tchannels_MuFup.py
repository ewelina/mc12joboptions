include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-W production, with W -> mu nu. Min_N_Tchannels option enabled. Factorization scale up"
evgenConfig.keywords = [ "Sherpa", "QCD", "W", "jets",  "Sherpa", "VBF", "Min_N_Tchannels", "MuFup" , "Systematics", "Factorization", "ScaleUp" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch", "rking@cern.ch" ]
evgenConfig.minevents = 5000

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 13 -14 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Scales METS{4.0*MU_F2}{MU_R2}
  Integration_Error 0.05
  Min_N_TChannels 1
  End process;

  Process 93 93 -> -13 14 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Scales METS{4.0*MU_F2}{MU_R2}
  Integration_Error 0.05
  Min_N_TChannels 1
  End process;
}(processes)

(selector){
  Mass 13 -14 10 E_CMS
  Mass -13 14 10 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

(model){
  ACTIVE[6]=0
}(model)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '147298.Sherpa_CT10_EWK_Wmunu_min_n_tchannels_MuFup_8TeV.TXT.mc12_v3'
