evgenConfig.description = "PYTHIA 8 ND dijet production with CT10 pdf and AU2 CT10 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")

topAlg.Pythia8.Commands += \
     ["HardQCD:all = on",
     "SoftQCD:singleDiffractive = off",
     "SoftQCD:doubleDiffractive = off",
     "PhaseSpace:pTHatMin = 17.",
     "PhaseSpace:pTHatMax = 35."]

#
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 4.0 # below this value the filtering is applied

###MC12 AUT_C0 tune, parameters of double exponential fit for gap distribution
topAlg.GapJetFilter.c0 = 0.571986
topAlg.GapJetFilter.c1 = 0.812387
topAlg.GapJetFilter.c2 = -8.38949
topAlg.GapJetFilter.c3 = 0.0378457
topAlg.GapJetFilter.c4 = 0.0589355
topAlg.GapJetFilter.c5 = -2.66747
topAlg.GapJetFilter.c6 = 0.000485016
topAlg.GapJetFilter.c7 = -2.66127

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]
