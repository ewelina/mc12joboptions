include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-W production, with W -> mu nu. Min_N_Tchannels option enabled. Alternative parton shower scheme."
evgenConfig.keywords = [ "Sherpa", "QCD", "W", "jets",  "Sherpa", "VBF", "Min_N_Tchannels" , "Systematics", "PartonShower" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch", "rking@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.weighting = 0

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 13 -14 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  CSS_KIN_SCHEME 1
  End process;

  Process 93 93 -> -13 14 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  CSS_KIN_SCHEME 1
  End process;
}(processes)

(selector){
  Mass 13 -14 10 E_CMS
  Mass -13 14 10 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)

(model){
  ACTIVE[6]=0
}(model)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '147305.Sherpa_CT10_Wmunu2JetsEW1JetQCD15GeVM40_min_n_tchannels_PartonShower1'


try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass

from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]
