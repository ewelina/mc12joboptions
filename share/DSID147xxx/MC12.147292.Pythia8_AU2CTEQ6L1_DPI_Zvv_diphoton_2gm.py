evgenConfig.description = "DPI production of Z->vv and di-photon. Diphoton filter of pT>10 GeV."
evgenConfig.keywords = ["electroweak", "Z", "Photon", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate Z bosons
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:TwoPhotons = on", # prompt photon
                            "PhaseSpace:sameForSecond = off", # turn off the default option 
                            "PhaseSpace:pTHatMinSecond = 5.", # pTMin on the second hard process
                            "23:mMin = 10", # Z mass down to 10 GeV
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 12", # switch on Z->enu,enu decays
                            "23:onIfAny = 14", # switch on Z->unu,unu decays
                            "23:onIfAny = 16"] # switch on Z->tnu,tnu decays

# photon filter
include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 10.*GeV
topAlg.PhotonFilter.Etacut    = 10.
topAlg.PhotonFilter.NPhotons  = 2
