evgenConfig.description = "Dijet truth jet slice JZ4W, with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 250."]

include("MC12JobOptions/JetFilter_JZ4W.py")
evgenConfig.minevents = 500
