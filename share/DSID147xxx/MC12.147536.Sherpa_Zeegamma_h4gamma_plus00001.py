include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "e e gamma production with aTGC H4_GAMMA 0.0001"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "Hulin.Wang@cern.ch" ]


evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  ERROR=0.05
}(run)

(model){
  MODEL = SM+AGC
  H4_GAMMA = 0.0001
  UNITARIZATION_SCALE=2000000
  UNITARIZATION_N=4
}(model)

(processes){
  Process 93 93 ->  11 -11 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 3  
  End process;
}(processes)
(selector){
  Mass 90 90 40 7000
  PT 22  80 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
