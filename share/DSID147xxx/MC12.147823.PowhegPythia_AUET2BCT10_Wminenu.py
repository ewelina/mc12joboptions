## POWHEG+PYTHIA W->e,nu

evgenConfig.description = "POWHEG+PYTHIA W->e,nu production with the AUET2B CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "e"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminenu"

include("MC12JobOptions/PowhegPythia_AUET2B_CT10_Common.py")
include("MC12JobOptions/Pythia_Photos.py")

## Specify W decay mode
include("MC12JobOptions/Pythia_Decay_Wenu.py")
