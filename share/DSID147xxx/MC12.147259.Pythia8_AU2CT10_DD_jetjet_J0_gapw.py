evgenConfig.description = "PYTHIA 8 DD dijet production with CT10 pdf and AU2 CT10 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")


## no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = off",
     "SoftQCD:singleDiffractive = off",
     "SoftQCD:doubleDiffractive = on"]

#
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

## Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = 20.0
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 4.5 # below this value the filtering is applied

###MC12 AUT_C0 tune, parameters of double exponential fit for gap distribution
topAlg.GapJetFilter.c0 = 0.284872
topAlg.GapJetFilter.c1 = 0.0854596
topAlg.GapJetFilter.c2 = -3.98323
topAlg.GapJetFilter.c3 = 0.00954016
topAlg.GapJetFilter.c4 = 2.60674
topAlg.GapJetFilter.c5 = -1.01018
topAlg.GapJetFilter.c6 = -0.000615121
topAlg.GapJetFilter.c7 = -1.17151

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]

from PerfMonComps.PerfMonFlags import jobproperties as perfmonjp
perfmonjp.PerfMonFlags.doMonitoring = False # to enable monitoring
perfmonjp.PerfMonFlags.doFastMon = False    # to only enable a lightweight monitoring
