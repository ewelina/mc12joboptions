evgenConfig.description = "PYTHIA 8 DD single jet slice JZ1W production with CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ1W.py")

from PerfMonComps.PerfMonFlags import jobproperties as perfmonjp
perfmonjp.PerfMonFlags.doMonitoring = False # to enable monitoring
perfmonjp.PerfMonFlags.doFastMon = False    # to only enable a lightweight monitoring

