evgenConfig.description = "DPI production of Z/Gm*->ll and prompt photon. Lepton filet of pT>10 GeV. Photon filter of pT>10 GeV"
evgenConfig.keywords = ["electroweak", "Z", "Photon", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate Z bosons
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:PhotonAndJet = on", # prompt photon
                            "PhaseSpace:sameForSecond = off", # turn off the default option 
                            "PhaseSpace:pTHatMinSecond = 5.", # pTMin on the second hard process
                            "23:mMin = 0.25", # Z/Gm* mass down to 250 MeV
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 11", # switch on Z->e,e decays
                            "23:onIfAny = 13", # switch on Z->u,u decays
                            "23:onIfAny = 15"] # switch on Z->t,t decays

# 1-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut    = 10.*GeV
topAlg.MultiLeptonFilter.Etacut   = 10.
topAlg.MultiLeptonFilter.NLeptons = 1

# photon filter
include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 10.*GeV
topAlg.PhotonFilter.Etacut    = 10.
topAlg.PhotonFilter.NPhotons  = 1
