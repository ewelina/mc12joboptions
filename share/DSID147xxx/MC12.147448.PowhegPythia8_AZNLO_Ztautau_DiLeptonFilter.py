include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_tautau"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production with two central lepton pt>10 GeV filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "tau"]

# compensate filter efficiency
evt_multiplier = 90

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 10000.
topAlg.MultiLeptonFilter.Etacut = 2.7
topAlg.MultiLeptonFilter.NLeptons = 2
