evgenConfig.description = "HERWIG++2.6 SD dijet production in EE3 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 5000

include ('MC12JobOptions/Herwigpp_Diffractive_CTEQ6L1_H12007.py')

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
cd /Herwig/Partons
set QCDExtractor:SecondPDF PomeronFlux
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
set /Herwig/Cuts/JetKtCut:MinKT 17*GeV
set /Herwig/Cuts/JetKtCut:MaxKT 35*GeV
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

## Add the filter to the correct sequence!
from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

#Jet kinematic cuts
if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 6.0 # below this value the filtering is applied

###parameters of double exponential fit for gap distribution
topAlg.GapJetFilter.c0 = 0.919287
topAlg.GapJetFilter.c1 = -0.319976
topAlg.GapJetFilter.c2 = -4.76212
topAlg.GapJetFilter.c3 = 0.0107555
topAlg.GapJetFilter.c4 = 2.50518
topAlg.GapJetFilter.c5 = -1.03081
topAlg.GapJetFilter.c6 = -0.00259374
topAlg.GapJetFilter.c7 = -1.99435

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]

