evgenConfig.description = "Dijet truth jet slice JZ7, with the AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
topAlg.Pythia8.Commands += \
    ["HardQCD:all = on",
     "PhaseSpace:pTHatMin = 1400."]

include("MC12JobOptions/JetFilter_JZ7.py")
evgenConfig.minevents = 5000
