evgenConfig.description = "PYTHIA 8 ND dijet production with CT10 pdf and AU2 CT10 tune with dijet and gap filter"
evgenConfig.keywords = ["QCD", "jets", "gaps"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 200

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")

## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
     ["HardQCD:all = on",
     "SoftQCD:singleDiffractive = off",
     "SoftQCD:doubleDiffractive = off",
     "PhaseSpace:pTHatMin = 140.",
     "PhaseSpace:pTHatMax = 280."]

#
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import GapJetFilter

if not hasattr(topAlg, "GapJetFilter"):
    topAlg += GapJetFilter()
topAlg.GapJetFilter.JetContainer = "AntiKt4TruthJets"
topAlg.GapJetFilter.MinPt1 = 12.0
topAlg.GapJetFilter.MinPt2 = 12.0
topAlg.GapJetFilter.MaxPt1 = runArgs.ecmEnergy
topAlg.GapJetFilter.MaxPt2 = runArgs.ecmEnergy

#Particle kinematic cuts for gap distribution
topAlg.GapJetFilter.MinPtparticle = 0.0
topAlg.GapJetFilter.MaxEtaparticle = 4.9
topAlg.GapJetFilter.gapf = 4.0 # below this value the filtering is applied

###MC12 AUT_C0 tune
topAlg.GapJetFilter.c0 = 0.76134
topAlg.GapJetFilter.c1 = 0.670652
topAlg.GapJetFilter.c2 = -8.56632
topAlg.GapJetFilter.c3 = 0.0237387
topAlg.GapJetFilter.c4 = 0.677585
topAlg.GapJetFilter.c5 = -2.60486
topAlg.GapJetFilter.c6 = 0.0002597
topAlg.GapJetFilter.c7 = -2.69922

if "GapJetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["GapJetFilter"]
