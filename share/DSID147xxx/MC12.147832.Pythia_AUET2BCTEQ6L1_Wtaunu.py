## PYTHIA W->tau,nu

evgenConfig.description = "W->tau,nu production with the AUET2B CTEQ6L1 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "tau"]

include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_Photos.py")
include("MC12JobOptions/Pythia_Tauola.py")

topAlg.Pythia.PythiaCommand += ["pysubs msel 0",
                                "pysubs msub 2 1"] # Create W bosons

include("MC12JobOptions/Pythia_Decay_Wtaunu.py")
