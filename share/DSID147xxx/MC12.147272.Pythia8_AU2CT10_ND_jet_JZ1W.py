evgenConfig.description = "PYTHIA 8 ND single jet slice JZ1W production with CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 500

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:minBias = on"]

include("MC12JobOptions/JetFilter_JZ1W.py")


