evgenConfig.description = "QED exclusive dimuon production"
evgenConfig.keywords = ["QED", "muons", "exclusive"]
evgenConfig.minevents = 5000
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

include("MC12JobOptions/Herwigpp_QED_Common.py")

## Add to commands
cmds = """
##################################################
#Cuts
##################################################
cd /Herwig/Cuts
set QCDCuts:ScaleMin 0.0*GeV
set QCDCuts:X1Min 0
set QCDCuts:X2Min 0
set QCDCuts:X1Max 1.
set QCDCuts:X2Max 1.
erase QCDCuts:MultiCuts 0
set LeptonKtCut:MinKT 15*GeV
set QCDCuts:MHatMin 30*GeV
##################################################
# Selected the hard process
##################################################
cd /Herwig/MatrixElements

# fermion-antifermion 
insert SimpleQCD:MatrixElements 0 /Herwig/MatrixElements/MEgg2ff
set /Herwig/MatrixElements/MEgg2ff:Process Muon
"""

## Set commands
topAlg.Herwigpp.Commands += cmds.splitlines()
