include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W -> mu nu + up to 4 jets using Sherpa's built-in MENLOPS prescription. MPI variations (type-2)"
evgenConfig.keywords = [ "Sherpa", "QCD", "W", "jets", "Systematics", "MPI" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch", "rking@cern.ch" ]
evgenConfig.minevents = 5000

evgenConfig.process="""
(processes){
  Process 93 93 -> 90 -91 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  NLO_QCD_Part BVIRS {2};
  ME_Generator Amegic {2};
  Integration_Error 0.05 {6,7,8}
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(mi){
  SIGMA_ND_FACTOR 0.31
}(mi)

(selector){
  Mass 90 -91 1.7 E_CMS
}(selector)

(run){
  NLO_Mode 3
  MASSIVE[11]=1
  MASSIVE[15]=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0
evgenConfig.inputconfcheck = '147318.Sherpa_CT10_Wmunu_mpi2'
