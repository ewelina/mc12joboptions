# Pythia shifted to pPb
# A .Olszewski November 2012

evgenConfig.description = "Double-diffractive inelastic events, with PYTHIA6 shifted to pPb"
evgenConfig.keywords = ["QCD", "minbias", "diffraction"]

include("MC12JobOptions/Pythia_AMBT2B_CTEQ6L1_Common.py")

topAlg.Pythia.McEventKey = "PYTHIA_EVENT"
topAlg.Pythia.PythiaCommand += \
    ["pysubs msel 0",
     "pysubs msub 94 1"]

from BoostAfterburner.BoostAfterburnerConf import BoostEvent
topAlg+=BoostEvent()
boost=topAlg.BoostEvent
boost.BetaZ=-0.43448
boost.McInputKey="PYTHIA_EVENT"
boost.McOutputKey="GEN_EVENT"

if "BoostEvent" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["BoostEvent"]
    
evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")

from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV

StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
