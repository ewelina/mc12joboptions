include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "ZZjj(j)->llnnjj(j) Order_EW=6"
evgenConfig.keywords = ["VBS", "Vector-Boson Scattering", "Diboson"]
evgenConfig.contact  = ["philipp.anger@cern.ch"]
evgenConfig.minevents = 100

evgenConfig.process="""
(run){
  ME_SIGNAL_GENERATOR=Comix;
  PARTICLE_CONTAINER 901 leptons 11 13 15;
  PARTICLE_CONTAINER 902 antileptons -11 -13 -15;
  PARTICLE_CONTAINER 903 antineutrinos -12 -14 -16;
  PARTICLE_CONTAINER 904 neutrinos 12 14 16;
  PARTICLE_CONTAINER 9923[m:-1] Zgamma 22 23;
}(run)

(model){
  MASS[5]    = 4.2;         # ATLAS default, bottom is massive
  WIDTH[6]   = 1.523;       # ATLAS default
  MASS[11]   = 0.000510997; # mass should not matter because they are massless
  MASS[13]   = 0.105658389; # mass should not matter because they are massless
  MASS[15]   = 1.77705;     # mass should not matter because they are massless
  ACTIVE[25] = 1;           # switch on the Higgs boson
  MASS[25]   = 126.0;       # mass of the Higgs boson in GeV
  WIDTH[25]  = 0.00418;     # width of the Higgs for the mass above
                            # taken from CERN yellow report twiki
  # GF scheme:
  #EW_SCHEME=3;  # this has running alpha_weak and does not work for us
  GF                  = 1.16639E-5;
  M_W                 = 80.399;
  M_Z                 = 91.1876;
  VEV                 = 246.2184581;
  SIN2THETAW          = 0.2226265156;
  LAMBDA              = 0.5237570374; # depends om MH: 2x(MH/VEV)^2
  1/ALPHAQED(default) = 132.3466351;  # default means no running coupling
}(model)

(beam){
  BEAM_1 = 2212; BEAM_ENERGY_1 = 4000;
  BEAM_2 = 2212; BEAM_ENERGY_2 = 4000;
}(beam)

(processes){
  Process 93 93 -> 93 93 9923[a] 9923[b] 93{1};
  Decay 9923[a] -> 901 902;
  Decay 9923[b] -> 903 904;
  Scales LOOSE_METS{MU_F2}{MU_R2} {6,7};
  Order_EW 6;
  CKKW sqr(20/E_CMS);
  Integration_Error 0.05;
  End process;
}(processes)

(selector){
  PT 90 5 E_CMS;
  NJetFinder 2 15. 0. 0.4 -1;
  Mass 11 -11 0.1 E_CMS;
  Mass 13 -13 0.1 E_CMS;
}(selector)
"""

evgenConfig.inputconfcheck = 'llnnjj_ZZjj_EW6'
