evgenConfig.description = "HERWIG++2.6 SD single jet production JZ1W in EE3 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 1000

include('MC12JobOptions/Herwigpp_Diffractive_CTEQ6L1_H12007.py')

cmds = """\
insert /Herwig/MatrixElements/SimpleQCD:MatrixElements[0] /Herwig/MatrixElements/MEQCD2to2
set /Herwig/Cuts/JetKtCut:MinKT 10.0*GeV
set /Herwig/UnderlyingEvent/MPIHandler:IdenticalToUE 0
cd /Herwig/Partons
set QCDExtractor:SecondPDF PomeronFlux
"""

topAlg.Herwigpp.Commands += cmds.splitlines()

include("MC12JobOptions/JetFilter_JZ1W.py")



