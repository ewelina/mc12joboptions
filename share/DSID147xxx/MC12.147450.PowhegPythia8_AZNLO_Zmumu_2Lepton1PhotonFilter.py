include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_mumu"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0


evgenConfig.description = "POWHEG+Pythia8 Z->ee production with di-lepton (pT > 8 GeV, |eta|<2.6) and QED FSR photon (pT > 10 GeV, |eta|<2.5) filters and AZNLO CT10 tune"
evgenConfig.keywords = ["electroweak", "Z", "leptons", "mu", "dilepton filter", "photon filter"]

# compensate filter efficiency
evt_multiplier = 40

include("MC12JobOptions/PowhegControl_postInclude.py")

include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut = 8000.
topAlg.MultiLeptonFilter.Etacut = 2.6
topAlg.MultiLeptonFilter.NLeptons = 2

include("MC12JobOptions/DirectPhotonFilter.py")
topAlg.DirectPhotonFilter.Ptcut = 10000.
topAlg.DirectPhotonFilter.Etacut = 2.5
	
