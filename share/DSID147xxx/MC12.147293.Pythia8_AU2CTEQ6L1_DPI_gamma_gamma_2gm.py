evgenConfig.description = "DPI production of prompt photon and prompt photon. Diphoton filter of pT>15 GeV."
evgenConfig.keywords = ["electroweak", "Z", "Photon", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "PromptPhoton:gg2ggamma = on",
                            "PhaseSpace:pTHatMin = 5.",
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:PhotonAndJet = on", # prompt photon
                            "PhaseSpace:sameForSecond = off", # turn off the default option 
                            "PhaseSpace:pTHatMinSecond = 5."] # pTMin on the second hard process

# photon filter
include("MC12JobOptions/PhotonFilter.py")
topAlg.PhotonFilter.Ptcut     = 15.*GeV
topAlg.PhotonFilter.Etacut    = 10.
topAlg.PhotonFilter.NPhotons  = 2
