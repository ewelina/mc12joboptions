evgenConfig.description = "Double Parton Interaction production of Z/Gm*->ll/vv and Z/Gm*->ll/vv. Dilepton filter of pT>10 GeV."
evgenConfig.keywords = ["electroweak", "Z", "leptons", "l", "DPI"]
evgenConfig.contact = ["Lulu Liu <Lulu.Liu@cern.ch>"]

include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

topAlg.Pythia8.Commands += ["WeakSingleBoson:ffbar2gmZ = on", # generate Z bosons
                            "SecondHard:generate = on", # second hard process
                            "SecondHard:SingleGmZ = on", # another Z
                            "23:mMin = 0.25", # Z/Gm* mass down to 250 MeV
                            "23:onMode = off", # switch off all Z decays
                            "23:onIfAny = 11", # switch on Z->ee decays
                            "23:onIfAny = 12", # switch on Z->enu,enu decays
                            "23:onIfAny = 13", # switch on Z->uu decays
                            "23:onIfAny = 14", # switch on Z->unu,unu decays
                            "23:onIfAny = 15", # switch on Z->tt decays
                            "23:onIfAny = 16"] # switch on Z->tnu,tnu decays

# 2-lepton filter
include("MC12JobOptions/MultiLeptonFilter.py")
topAlg.MultiLeptonFilter.Ptcut    = 10.*GeV
topAlg.MultiLeptonFilter.Etacut   = 10.
topAlg.MultiLeptonFilter.NLeptons = 2
