## POWHEG+Pythia8 Wminmunu_pn

evgenConfig.description = "POWHEG+Pythia8 Wminmunu for proton-neutron beam, no lepton filter, AU2 CT10 tune"
evgenConfig.keywords = ["electroweak", "W", "leptons", "mu", "proton-neutron beam"]
evgenConfig.inputfilecheck = "Powheg_CT10.*Wminmunu_pn"

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")
