include("MC12JobOptions/PowhegControl_preInclude.py")

postGenerator="Pythia8Photos"
postGeneratorTune="AZNLO_CTEQ6L1"
process="Z_tautau"

def powheg_override():
    # needed for AZNLO tune
    PowhegConfig.ptsqmin = 4.0

evgenConfig.description = "POWHEG+Pythia8 Z->tautau production without lepton filter and AZNLO CT10 tune"
evgenConfig.keywords = ["EW", "Z", "leptonic", "tau"]

include("MC12JobOptions/PowhegControl_postInclude.py")
