include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "W -> mu nu + up to 4 jets using Sherpa's built-in MENLOPS prescription. muF up"
evgenConfig.keywords = [ "Sherpa", "QCD", "W", "jets", "Systematics", "Factorization", "ScaleUp" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch", "rking@cern.ch" ]
evgenConfig.minevents = 200
evgenConfig.weighting = 0

evgenConfig.process="""
(processes){
  Process 93 93 -> 90 -91 93{4}
  Order_EW 2
  CKKW sqr(20/E_CMS)
  Scales METS{4.0*MU_F2}{MU_R2}
  Scales LOOSE_METS{4.0*MU_F2}{MU_R2} {6,7,8}
  NLO_QCD_Part BVIRS {2};
  ME_Generator Amegic {2};
  Integration_Error 0.05 {6,7,8}
  Enhance_Factor 2.0 {3}
  Enhance_Factor 5.0 {4,5,6,7,8}
  End process;
}(processes)

(selector){
  Mass 90 -91 1.7 E_CMS
}(selector)

(run){
  NLO_Mode 3
  MASSIVE[11]=1
  MASSIVE[15]=1
}(run)
"""

topAlg.Sherpa_i.ResetWeight = 0
evgenConfig.inputconfcheck = '147315.Sherpa_CT10_Wmunu_MuFup_MjjFilt_8TeV.TXT.mc12_v3'


try:    
  from JetRec.JetGetters import *
  antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets',useInteractingOnly=True,includeMuons=False)
  antikt4alg = antikt4.jetAlgorithmHandle()
  antikt4alg.OutputLevel = INFO
except Exception, e:    
  pass

from GeneratorFilters.GeneratorFiltersConf import VBFMjjIntervalFilter
topAlg += VBFMjjIntervalFilter()
topAlg.VBFMjjIntervalFilter.TruthJetContainerName="AntiKt4TruthJets"
topAlg.VBFMjjIntervalFilter.OutputLevel = INFO

StreamEVGEN.RequireAlgs +=  [ "VBFMjjIntervalFilter" ]
