include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "VBF-Z production, with Z/gamma* -> mm. Min_N_Tchannels option enabled. Alternative parton shower scheme."
evgenConfig.keywords = [  "Sherpa", "VBF", "Min_N_Tchannels" , "Systematics", "PartonShower" ]
evgenConfig.contact = [ "frank.siegert@cern.ch", "christian.schillo@cern.ch", "kiran.joshi@cern.ch" ]
evgenConfig.minevents = 5000

evgenConfig.process="""
(run){
  EW_TCHAN_MODE=1
}(run)

(processes){
  Process 93 93 -> 13 -13 93 93 93{1}
  Order_EW 4
  CKKW sqr(15/E_CMS)
  Integration_Error 0.05
  Min_N_TChannels 1
  CSS_KIN_SCHEME 1
  End process;
}(processes)

(selector){
  Mass 13 -13 40 E_CMS
  NJetFinder 2 15.0 0.0 0.4 1
}(selector)
"""

topAlg.Sherpa_i.ResetWeight = 0

evgenConfig.inputconfcheck = '147336.Sherpa_CT10_Ztomm2JetsEW1JetQCD15GeVM40_min_n_tchannels_PartonShower1'
