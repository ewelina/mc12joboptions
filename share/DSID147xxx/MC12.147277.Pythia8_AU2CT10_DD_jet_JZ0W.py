evgenConfig.description = "PYTHIA 8 DD single jet slice JZ0W production with CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["QCD", "jets"]
evgenConfig.contact = ["Marek Tasevsky <Marek.Tasevsky@cern.ch>"]
evgenConfig.minevents = 5000

include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
## Run with a min bias process: no pThat cut possible
topAlg.Pythia8.Commands += \
    ["SoftQCD:doubleDiffractive = on"]

include("MC12JobOptions/JetFilter_JZ0W.py")
# increase low-pt edge 
topAlg.QCDTruthJetFilter.MinPt = 10.*GeV


