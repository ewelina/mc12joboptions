include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "nu nu gamma production"
evgenConfig.keywords = [ ]
evgenConfig.contact  = [ "Hulin.Wang@cern.ch" ]


evgenConfig.process="""
(run){
  ACTIVE[25]=0
  ME_SIGNAL_GENERATOR=Amegic
  ERROR=0.05
}(run)

(model){
  MODEL         = SM
}(model)

(processes){
  Process 93 93 ->  91 91 22 93{1}
  CKKW sqr(20/E_CMS)
  Order_EW 3
  End process;
}(processes)
(selector){
  PT 22  80 7000
  DeltaR 22 90 0.1 1000
  DeltaR 22 93 0.1 1000
}(selector)
"""
