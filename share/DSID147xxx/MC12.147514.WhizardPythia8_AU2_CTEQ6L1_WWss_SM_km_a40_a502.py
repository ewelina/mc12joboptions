############################################################################################################
# Whizard lhef showered with Pythia8
# Ulrike Schnoor <ulrike.schnoor@cern.ch>
# Generators/MC12JobOptions/tags/MC12JobOptions-00-05-70/share/MC12.xxxxxx.LHEFPythia8_AU2CTEQ6L1_example.py
############################################################################################################                                                                        
evgenConfig.description = "Whizard+Pythia8 production JO example with the AU2 CTEQ6L1 tune"
evgenConfig.keywords = ["EW"]
evgenConfig.inputfilecheck = "whizard.147514"
include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia8_LHEF.py")

evgenConfig.minevents = 5000
evgenConfig.contact = ["Ulrike Schnoor <ulrike.schnoor@physik.tu-dresden.de>"]

evgenConfig.generators = ["Whizard", "Pythia8" ]
