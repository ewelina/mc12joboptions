include('MC12JobOptions/MadGraphControl_SM_TT_directBWN.py')


if not hasattr(topAlg, "PtmissAndOrLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import PtmissAndOrLeptonFilter
    topAlg += PtmissAndOrLeptonFilter()

if "PtmissAndOrLeptonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["PtmissAndOrLeptonFilter"]

topAlg.PtmissAndOrLeptonFilter.PtmissANDLepton = False
topAlg.PtmissAndOrLeptonFilter.PtminElectron = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MaxEtaElectron = 2.8
topAlg.PtmissAndOrLeptonFilter.PtminMuon = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MaxEtaMuon = 2.8
topAlg.PtmissAndOrLeptonFilter.PtminLostTrack = 5*GeV
topAlg.PtmissAndOrLeptonFilter.MinEtaLost = 100.
topAlg.PtmissAndOrLeptonFilter.PtminMissing = 80*GeV


include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 150*GeV
topAlg.METFilter.MissingEtCalcOption = 1
