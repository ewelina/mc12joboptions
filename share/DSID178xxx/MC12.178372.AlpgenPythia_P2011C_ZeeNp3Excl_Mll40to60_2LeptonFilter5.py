include('MC12JobOptions/AlpgenPythia_SUSY_P2011C_DY.py')
evgenConfig.minevents = 1000
evgenConfig.inputconfcheck = "ZeeNp3Excl_Mll40to60"

include("MC12JobOptions/MultiElecMuTauFilter.py")
MultiElecMuTauFilter = topAlg.MultiElecMuTauFilter
topAlg.MultiElecMuTauFilter.NLeptons  = 2
topAlg.MultiElecMuTauFilter.MinPt = 5000.
topAlg.MultiElecMuTauFilter.MaxEta = 2.8
topAlg.MultiElecMuTauFilter.IncludeHadTaus = 0
