
evgenConfig.description = "POWHEG+Pythia8 ZZ_4tau production mll>4GeV with dimuon filter pt>5GeV using CT10 pdf and AU2 CT10 tune"
evgenConfig.keywords = ["electroweak"  ,"ZZ", "leptons" ]
evgenConfig.inputfilecheck = "Powheg_CT10.*126942"
evgenConfig.minevents = 5000
evgenConfig.contact = ["Oldrich Kepka <oldrich.kepka@cern.ch>"]

include("MC12JobOptions/PowhegPythia8_AU2_CT10_Common.py")
include("MC12JobOptions/Pythia8_Photos.py")

# 2-lepton veto
if not hasattr(topAlg, "MultiLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter()
    
if "MultiLeptonFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs += ["MultiLeptonFilter"]
    
topAlg.MultiLeptonFilter.Ptcut = 5.*GeV
topAlg.MultiLeptonFilter.Etacut = 10
topAlg.MultiLeptonFilter.NLeptons = 2

# 2-lepton with tau
include("MC12JobOptions/MultiElecMuTauFilter.py")
topAlg.MultiElecMuTauFilter.MinPt = 5.*GeV
topAlg.MultiElecMuTauFilter.MaxEta = 10
topAlg.MultiElecMuTauFilter.NLeptons = 2
topAlg.MultiElecMuTauFilter.MinVisPtHadTau = 15.*GeV
