include('MC12JobOptions/MadGraphControl_SM_TT_directBWN.py')


if not hasattr(topAlg, "LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    topAlg += LeptonFilter()

if "LeptonFilter" not in StreamEVGEN.AcceptAlgs:
    StreamEVGEN.AcceptAlgs += ["LeptonFilter"]

topAlg.LeptonFilter.Ptcut = 5*GeV
topAlg.LeptonFilter.Etacut = 2.8


include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.AcceptAlgs:
    StreamEVGEN.AcceptAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 80*GeV
topAlg.METFilter.MissingEtCalcOption = 1
