include("MC12JobOptions/Sherpa_CT10_Common.py")

evgenConfig.description = "Z(->qq)Z(->vv) with 0,1j@NLO + 2,3j@LO."
evgenConfig.keywords = ["diboson", "neutrino", "2jet" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch" ]
evgenConfig.minevents = 2000
evgenConfig.inputconfcheck = "Sherpa_CT10_ZZ"

evgenConfig.process="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  CORE_SCALE VAR{Abs2(p[2]+p[3])/4.0}

  %tags for process setup
  NJET:=3; LJET:=2,3; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops;
  EXCLUSIVE_CLUSTER_MODE=1

  % decay setup
  HARD_DECAYS=1
  STABLE[23]=0
  WIDTH[23]=0
}(run)

(processes){
  Process 93 93 -> 23 23 93{NJET};
  Order_EW 2; CKKW sqr(QCUT/E_CMS);
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.05 {3,4,5,6,7};
  End process;
}(processes)
"""

topAlg.Sherpa_i.Parameters += [ "WIDTH[23]=0" ]

from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
decaysfilter = DecaysFinalStateFilter()
topAlg += decaysfilter

if "DecaysFinalStateFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["DecaysFinalStateFilter"]

topAlg.DecaysFinalStateFilter.PDGAllowedParents = [ 23 ]
topAlg.DecaysFinalStateFilter.NQuarks = 2
topAlg.DecaysFinalStateFilter.NNeutrinos = 2
