MODEL = 'regge'
CASE = 'sbottom'
MASS = 800

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic sbottom 800GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
