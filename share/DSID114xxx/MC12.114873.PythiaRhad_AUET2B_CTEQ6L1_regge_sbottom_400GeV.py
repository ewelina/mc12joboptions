MODEL = 'regge'
CASE = 'sbottom'
MASS = 400

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic sbottom 400GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
