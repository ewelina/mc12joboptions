MODEL = 'regge'
CASE = 'stop'
MASS = 700

include("MC12JobOptions/PythiaRhad_Common.py")
evgenConfig.description += " stable generic stop 700GeV"
evgenConfig.specialConfig = "MODEL={model};CASE={case};MASS={mass};preInclude=SimulationJobOptions/preInclude.Rhadrons.py;".format(model = MODEL, case = CASE, mass = MASS)
