#! /usr/bin/env python

"""\
Check that ATLAS production job options files have filenames and contents that
conform with ATLAS production rules.

Usage: %prog <jo1.py> <jo2.py> ...

TODO:
 * Add a check against the allowed generator names... good algorithm for that?
"""

## Define compiled regex objects for compliance checks
import re, os
RE_JONAME = re.compile(r"^MC12\.[\dx]{6}\.\w+\.py") # update for each MCXX campaign
RE_EVGENCONFIG = re.compile(r"import\s+.*evgenConfig")
RE_REDEFINITION = re.compile(r"(.*?)\s*=\s*\1\s*\(")
RE_INPUTBASE = re.compile(r"evgenConfig\.input(file|conf)base\s*=")
RE_DESCRIPTION = re.compile(r"evgenConfig\.description\s*=")
RE_EFFICIENCY = re.compile(r"evgenConfig\.efficiency\s*=")
RE_AFSPATH = re.compile(r"/afs")
RE_IMPORTALGSEQUENCE = re.compile(r"from AthenaCommon.AlgSequence import AlgSequence")
RE_SETTOPALG = re.compile(r"topAlg\s*=")
RE_UNIXGENJOB = re.compile(r"import AthenaCommon.AtlasUnixGeneratorJob")
RE_SVCMGR = re.compile(r"from AthenaCommon.AppMgr import ServiceMgr as svcMgr")
RE_CONFIGURABLE = re.compile(r"from AthenaCommon.Configurable import Configurable")
RE_OUTPUTLEVEL = re.compile(r"svcMgr.MessageSvc.OutputLevel\s*=")
RE_FRAGMENT = re.compile(r"include.*\(.*_Fragment\.py")
RE_BUILDHERWIG = re.compile(r"setup\sMSSM/Model\s.*slha")
RE_LHEF=re.compile(r"evgenConfig\.generators.*Lhef")

## Parse the command line arguments
import optparse
op = optparse.OptionParser(usage=__doc__)
opts, args = op.parse_args()

## The args are a list of JO files to test for compliance
for jopath in args:
    jo = os.path.basename(jopath)

    ## First check the filename compliance
    if not RE_JONAME.match(jo):
        print "Bad JO name:", jo

    if len(jo.split('.')[2])>60:
        print "Physics field more than 60 characters", jo

    ## Check that the file exists! It's ok if not, to test proposed JO names
    if not os.path.exists(jopath):
        print "JO file {f} does not exist".format(f=jopath)
        continue

    ## Next check the file contents
    with open(jopath) as jofile:
        description_ok = False
        for line in jofile:
            if RE_IMPORTALGSEQUENCE.search(line):
                print "JOs should not re-import AlgSequence: ", line
            if RE_EVGENCONFIG.search(line):
                print "JOs should not re-import evgenConfig: ", line
            if RE_UNIXGENJOB.search(line):
                print "JOs should not re-import AtlasUnixGeneratorJob: ", line
            if RE_SVCMGR.search(line):
                print "JOs should not re-import svcMgr: ", line
            if RE_CONFIGURABLE.search(line):
                print "JOs should not re-import Configurable: ", line
            if RE_REDEFINITION.search(line):
                print "Avoid assigning a class instance to an object with the class name: ", line
            if RE_INPUTBASE.search(line):
                print "evgenConfig.input*base is no longer used: ", line
            if RE_EFFICIENCY.search(line):
                print "evgenConfig.efficiency is no longer used: ", line
            if RE_AFSPATH.search(line):
                print "JOs should not attempt to access /afs paths: ", line
            if RE_SETTOPALG.search(line):
                print "JOs should not redefine topAlg: ", line
            if RE_OUTPUTLEVEL.search(line):
                print "JOs should not try to control the output level: ", line
            if RE_FRAGMENT.search(line):
                print "*_Fragment include files should not appear in JOs: ", line
            if RE_BUILDHERWIG.search(line):
                print "Should use buildHerwigppCommands() instead of old way"
            if RE_LHEF.search(line):
                print "Use of generic \"Lhef\" in evgenConfig\.generators field is discouraged. Use specific generator name."
                
            if not description_ok and RE_DESCRIPTION.search(line):
                description_ok = True

        if not description_ok:
            print "evgenConfig.description must be provided"
