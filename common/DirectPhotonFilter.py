## Default configuration of the direct photon filter
## original version by L. Carminati
## Apr. 2014 modified version for double-sided binning (J. Ocariz)

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
if not hasattr(topAlg, "DirectPhotonFilter"):
    topAlg += DirectPhotonFilter()

## topAlg.DirectPhotonFilter.Ptcut = 10000.		// old-fashion property
##                                                      // now use Ptmin and Ptmax instead

topAlg.DirectPhotonFilter.Ptmin = 10000.		
topAlg.DirectPhotonFilter.Ptmax = 100000000.		
topAlg.DirectPhotonFilter.Etacut =  2.7
topAlg.DirectPhotonFilter.NPhotons = 1
if "DirectPhotonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["DirectPhotonFilter"]
