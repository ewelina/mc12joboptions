from JetRec.JetGetters import *
antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets_IncAll',useInteractingOnly=False,includeMuons=True)
antikt4alg = antikt4.jetAlgorithmHandle()

from GeneratorFilters.GeneratorFiltersConf import DiBjetFilter
topAlg += DiBjetFilter()

if "DiBjetFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["DiBjetFilter"]

topAlg.DiBjetFilter.TruthContainerName="AntiKt4TruthJets_IncAll"
topAlg.DiBjetFilter.AcceptSomeLightEvents=True
