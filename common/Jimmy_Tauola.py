## TAUOLA config for PYTHIA

## Disable native tau decays
topAlg.Herwig.HerwigCommand += ["taudec TAUOLA"]

## Enable TAUOLA
include("MC12JobOptions/Tauola_Fragment.py")
