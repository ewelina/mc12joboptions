## Tauola++ tau decay config for Pythia8

from Tauolapp_i.Tauolapp_iConf import TauolaAlg
topAlg += TauolaAlg("Tauolapp")
evgenConfig.generators += ["Tauolapp"]
