## POWHEG+PYTHIA config for the Perugia 2011 (CTEQ5L PDF) UE tune
include("MC12JobOptions/Pythia_Perugia2011_Common.py")

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
