from Hydjet_i.Hydjet_iConf import Hydjet
topAlg += Hydjet()

## evgenConfig setup -- use reduced number of events for HI
evgenConfig.generators += ["Hydjet"]
evgenConfig.minevents = 250

## Set default HYDJET random number seeds
## Not actually used in Hydjet interface
# TODO: Isn't this reset anyway in EvgenJobTransforms/share/Generate_randomseeds.py?
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()
svcMgr.AtRndmGenSvc.Seeds = \
    ["HYDJET 999999991 999999992", "HYDJET_INIT 999999993 999999994"]
## NB. Seed specification via steering params is done in the JOs

## Extra stream persistency (same as for HIJING)
## 2101 == EventInfo, 133273 == MCTruth (HepMC), 54790518 == HijingEventParams
from AthenaPoolCnvSvc.WriteAthenaPool import AthenaPoolOutputStream
_evgenstream = AthenaPoolOutputStream("StreamEVGEN")
_evgenstream.ItemList = ["2101#*","133273#GEN_EVENT", "54790518#*"]
del _evgenstream
