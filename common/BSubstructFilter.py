from JetRec.JetGetters import *
from JetRec.JetRecFlags import jetFlags
jetFlags.doJVF = False
jetFlags.inputFileType='GEN'

fatJetGetter = make_StandardJetGetter('CamKt',1.2,'Truth')
fatAlg = fatJetGetter.jetAlgorithmHandle()

if "BSubstruct" not in topAlg:
    from GeneratorFilters.GeneratorFiltersConf import BSubstruct
    topAlg += BSubstruct()
if "BSubstruct" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += [ "BSubstruct" ]
    
#Default configurations (Update in your JOs!)
topAlg.BSubstruct.JetKey = fatJetGetter.outputKey()
topAlg.BSubstruct.filterB = False #this bit turns off the b-tagging to leave a cut on fat jet pT
topAlg.BSubstruct.filterC = False
topAlg.BSubstruct.pTMin = 160000.  # MeV
topAlg.BSubstruct.pTMax = -1.

