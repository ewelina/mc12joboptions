## Truth jet filter config for JZ5
include("MC12JobOptions/JetFilter_JZX_Fragment.py")
topAlg.QCDTruthJetFilter.MinPt = 1000.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 1500.*GeV
