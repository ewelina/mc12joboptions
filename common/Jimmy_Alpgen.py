## Common config for Alpgen+HERWIG+JIMMY
evgenConfig.generators = ["Alpgen", "Herwig" , "Jimmy"]
topAlg.Herwig.HerwigCommand += ["iproc alpgen"]
