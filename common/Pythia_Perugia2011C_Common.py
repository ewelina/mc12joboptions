## PYTHIA 6 config with Perugia2011-C tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_356"
evgenConfig.tune = "Perugia2011C"
