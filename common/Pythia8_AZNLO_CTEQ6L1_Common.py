## Config for Py8 tune AZNLO with CTEQ6L1
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = cteq6ll.LHpdf",
    "BeamRemnants:primordialKThard = 1.74948",
    "SpaceShower:alphaSorder = 2",
    "SpaceShower:alphaSvalue = 0.118",
    "SpaceShower:pT0Ref = 1.923589",
    "MultipartonInteractions:pT0Ref = 2.002887"
    ]

evgenConfig.tune = "AZNLO CTEQ6L1"

# needs Pythia8 Main31 matching
include('MC12JobOptions/Pythia8_Powheg_Main31.py')

topAlg.Pythia8.UserModes += ['Main31:NFinal = 1',
                             'Main31:pTHard = 0',
                             'Main31:pTdef = 2'
                             ]
