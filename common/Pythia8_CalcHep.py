## Enable CalcHep LHEF reading in Pythia8
include("MC12JobOptions/Pythia8_LHEF.py")
evgenConfig.generators = ["CalcHep", "Pythia8"]
