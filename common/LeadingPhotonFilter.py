## Default configuration of the leading photon filter
from GeneratorFilters.GeneratorFiltersConf import LeadingPhotonFilter
if not hasattr(topAlg, "LeadingPhotonFilter"):
    topAlg += LeadingPhotonFilter()
topAlg.LeadingPhotonFilter.PtMin  = 0.0
topAlg.LeadingPhotonFilter.PtMax  = 10000000.0
topAlg.LeadingPhotonFilter.EtaCut = 100
if "LeadingPhotonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["LeadingPhotonFilter"]
