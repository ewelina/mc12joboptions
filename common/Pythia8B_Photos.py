## Disable native QED FSR
assert hasattr(topAlg, "Pythia8B")
topAlg.Pythia8B.Commands += ["TimeShower:QEDshowerByL = off"]

## Enable Photos++
include("MC12JobOptions/Photospp_Fragment.py")

