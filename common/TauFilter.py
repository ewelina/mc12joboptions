##Instantiate the tau filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "TauFilter"):
	from GeneratorFilters.GeneratorFiltersConf import TauFilter
	topAlg += TauFilter()
## Add this filter to the algs required to be successful for streaming
if "TauFilter" not in StreamEVGEN.RequireAlgs:
	StreamEVGEN.RequireAlgs += ["TauFilter"]
