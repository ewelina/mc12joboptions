## Truth four-jets above 50 GeV filter
include("MC12JobOptions/MultiJetFilterAkt4.py")
topAlg.QCDTruthMultiJetFilter.Njet = 4
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 50.*GeV
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = -1.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = float(runArgs.ecmEnergy)*GeV
topAlg.QCDTruthMultiJetFilter.MaxEta = 999.
topAlg.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
topAlg.QCDTruthMultiJetFilter.DoShape = False

