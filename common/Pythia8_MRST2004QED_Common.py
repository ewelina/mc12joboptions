## Config for Py8 with MRST2004QED
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = MRST2004qed.LHgrid"
]
evgenConfig.tune = "MRST2004qed"
