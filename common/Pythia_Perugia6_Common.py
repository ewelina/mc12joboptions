## PYTHIA 6 config with Perugia6 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_326"
evgenConfig.tune = "Perugia6"
