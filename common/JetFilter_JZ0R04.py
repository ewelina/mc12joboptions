## Truth jet filter config for JZ0 with R=0.4
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 0.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 20.*GeV
