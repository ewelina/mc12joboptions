## PYTHIA 6 config with D6 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_108"
evgenConfig.tune = "D6"
