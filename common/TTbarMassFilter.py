## Instantiate the TTbarMassFilter, including adding it to the stream requirement
## Filter passed if: TopPairMassLowThreshold < m_tt < TopPairMassHighThreshold
## Configuration of the filter cuts is left to the specific JO
## neil.james.cooper-smith@cern.ch

if not hasattr(topAlg, "TTbarMassFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TTbarMassFilter
    topAlg += TTbarMassFilter()

## Add this filter to the algs required to be successful for streaming
if "TTbarMassFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["TTbarMassFilter"]

## Default cut params
topAlg.TTbarMassFilter.TopPairMassLowThreshold  = 0.
topAlg.TTbarMassFilter.TopPairMassHighThreshold = 10000000.
