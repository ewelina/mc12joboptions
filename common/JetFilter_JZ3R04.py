## Truth jet filter config for JZ3 with R=0.4
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 200.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 500.*GeV
