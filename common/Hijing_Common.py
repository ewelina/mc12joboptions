from Hijing_i.Hijing_iConf import Hijing
topAlg += Hijing()

## evgenConfig setup -- use reduced number of events for HI
evgenConfig.generators += ["Hijing"]

## Set default HIJING random number seeds
# TODO: Isn't this reset anyway in EvgenJobTransforms/share/Generate_randomseeds.py?
if not hasattr(svcMgr, 'AtRndmGenSvc'):
    from AthenaServices.AthenaServicesConf import AtRndmGenSvc
    svcMgr += AtRndmGenSvc()
svcMgr.AtRndmGenSvc.Seeds = \
    ["HIJING 327213897 979111713", "HIJING_INIT 31452781 78713307"]

## Extra stream persistency
## (default configuration writes out too much, as Hijing produced its own McEventCollection)
StreamEVGEN.ItemList.remove("McEventCollection#*")
StreamEVGEN.ItemList += ["McEventCollection#GEN_EVENT"]
StreamEVGEN.ItemList += ["HijingEventParams#*"]
