if not hasattr(topAlg, "XtoVVDecayFilter"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilter
    topAlg += XtoVVDecayFilter()

## Add this filter to the algs required to be successful for streaming
if "XtoVVDecayFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilter"]

## Default cut params
topAlg.XtoVVDecayFilter.PDGGrandParent = 25                  
topAlg.XtoVVDecayFilter.PDGParent = 24                       
topAlg.XtoVVDecayFilter.StatusParent = 22                    
topAlg.XtoVVDecayFilter.PDGChild1 = [11,12,13,14,15,16]      
topAlg.XtoVVDecayFilter.PDGChild2 = [1,2,3,4,5,6]            
