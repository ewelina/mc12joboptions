## Common job option for gamma gamma processes in Herwig++
## MPI tune is not relevant as the pdf_gammagamma_cmds() function disables MPI
# TODO: Why a common include rather than a specific JO?
## Contact: Pavel Ruzicka
include("MC12JobOptions/Herwigpp_Base_Fragment.py")
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.pdf_gammagamma_cmds()
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds
