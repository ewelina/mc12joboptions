## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "FourLeptonInvMassFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import FourLeptonInvMassFilter
    topAlg += FourLeptonInvMassFilter()

## Add this filter to the algs required to be successful for streaming
if "FourLeptonInvMassFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["FourLeptonInvMassFilter"]

## Default cut params
