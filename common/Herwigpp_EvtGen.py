## Run EvtGen afterburner on top of Herwig++
assert hasattr(topAlg, "Herwigpp")
include("MC12JobOptions/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['HerwigppInclusiveP8.pdt']
topAlg.EvtInclusiveDecay.pdtFile = "HerwigppInclusiveP8.pdt"

# FHerwig has problems with omega b* (5334), so not present in the base EvtGen fragment.  Add it here.
topAlg.EvtInclusiveDecay.whiteList+=[-5334, 5334]

