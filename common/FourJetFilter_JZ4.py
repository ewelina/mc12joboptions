## Truth four jet filter config for JZ4
include("MC12JobOptions/FourJetFilter_JZX_Fragment.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 300.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 500.*GeV
