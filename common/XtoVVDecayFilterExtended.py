if not hasattr(topAlg, "XtoVVDecayFilterExtended"):
    from GeneratorFilters.GeneratorFiltersConf import XtoVVDecayFilterExtended
    topAlg += XtoVVDecayFilterExtended()

## Add this filter to the algs required to be successful for streaming
if "XtoVVDecayFilterExtended" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["XtoVVDecayFilterExtended"]

## Default cut params
topAlg.XtoVVDecayFilterExtended.PDGGrandParent = 25                  
topAlg.XtoVVDecayFilterExtended.PDGParent = 24                       
topAlg.XtoVVDecayFilterExtended.StatusParent = 22                    
topAlg.XtoVVDecayFilterExtended.PDGChild1 = [11,12,13,14,15,16]      
topAlg.XtoVVDecayFilterExtended.PDGChild2 = [1,2,3,4,5,6]            
