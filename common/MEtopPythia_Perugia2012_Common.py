## Configuration for MEtop+PYTHIA using the Perugia2012 tune
include("MC12JobOptions/Pythia_Perugia2012_Common.py")
evgenConfig.generators += [ "MEtop", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

