## PYTHIA 6 config with AUET2B-CTEQ6L1 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110003"
evgenConfig.tune = "AUET2B CTEQ6L1"
