## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "MultiLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter()

## Add this filter to the algs required to be successful for streaming
if "MultiLeptonFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs += ["MultiLeptonFilter"]

## Default cut params
topAlg.MultiLeptonFilter.Ptcut = 10000.
topAlg.MultiLeptonFilter.Etacut = 2.8
topAlg.MultiLeptonFilter.NLeptons = 2
