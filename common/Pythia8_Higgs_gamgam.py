## Configure Pythia 8 to decay Higgs to gamgam exclusively

## Contact: David.Hall@cern.ch, Tatsuya.Masubuchi@cern.ch

assert hasattr(topAlg, "Pythia8")

topAlg.Pythia8.Commands += [
                            '25:onMode = off',#decay of Higgs
                            '25:onIfMatch = 22 22',
                            ]
