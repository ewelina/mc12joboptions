## Truth multi-jet filter setup for anti-kT R=0.4 truth jets
from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

include("MC12JobOptions/MultiJetFilter_Fragment.py")
topAlg.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
