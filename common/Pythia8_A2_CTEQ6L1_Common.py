## Config for Py8 tune A2 with CTEQ6L1
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = cteq6ll.LHpdf",
    "MultipartonInteractions:bProfile = 4",
    "MultipartonInteractions:a1 = 0.06",
    "MultipartonInteractions:pT0Ref = 2.18",
    "MultipartonInteractions:ecmPow = 0.22",
    "BeamRemnants:reconnectRange = 1.55",
    "SpaceShower:rapidityOrder=0"]
evgenConfig.tune = "A2 CTEQ6L1"
