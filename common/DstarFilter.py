## Instantiate the muon filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "HeavyFlavorHadronFilter"):
    from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
    topAlg += HeavyFlavorHadronFilter()

## Add this filter to the algs required to be successful for streaming
if "HeavyFlavorHadronFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["HeavyFlavorHadronFilter"]

## Default cut params
topAlg.HeavyFlavorHadronFilter.RequestCharm=FALSE
topAlg.HeavyFlavorHadronFilter.RequestBottom=FALSE
topAlg.HeavyFlavorHadronFilter.Request_cQuark=FALSE
topAlg.HeavyFlavorHadronFilter.Request_bQuark=FALSE
topAlg.HeavyFlavorHadronFilter.RequestSpecificPDGID=TRUE
topAlg.HeavyFlavorHadronFilter.PDGID=413
topAlg.HeavyFlavorHadronFilter.PDGAntiParticleToo=TRUE
topAlg.HeavyFlavorHadronFilter.RequireTruthJet=FALSE
topAlg.HeavyFlavorHadronFilter.PDGEtaMax=3.0
topAlg.HeavyFlavorHadronFilter.PDGPtMin=4.0*GeV
