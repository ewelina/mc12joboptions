## Common config for adding the EvtGen decayer to PYTHIA6

assert hasattr(topAlg, "Pythia")
evgenConfig.generators += [ "EvtGen" ]
evgenConfig.auxfiles += [ 'inclusive.dec', 'inclusive.pdt' ]

## This code sets up EvtGen for inclusive decays
from HepMCTools.HepMCToolsConf import TranslateParticleCodes
topAlg += TranslateParticleCodes()
topAlg.TranslateParticleCodes.OutputLevel = 3
topAlg.TranslateParticleCodes.translateToPDTFrom = "PYTHIA"
topAlg.TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
topAlg.TranslateParticleCodes.test = [0,10000000]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()
topAlg.EvtInclusiveDecay.OutputLevel = 3
topAlg.EvtInclusiveDecay.pdtFile = "inclusive.pdt"
topAlg.EvtInclusiveDecay.decayFile = "inclusive.dec"

## The following is NEEDED with the default parameters of EvtInclusiveDecay
topAlg.Pythia.PythiaCommand += [
    "pydat1 parj 90 20000",   # prevent photon emission from leptons
    "pydat1 mstj 26 0"        # turn off B mixing (done by EvtGen)
    ]

# TODO: Doesn't PHOTOS need to be enabled via Pythia_Photos.py?
