## Instantiate the multi muon filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "MultiMuonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
    topAlg += MultiMuonFilter()

## Add this filter to the algs required to be successful for streaming
if "MultiMuonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["MultiMuonFilter"]

## Default cut params
topAlg.MultiMuonFilter.Ptcut = 8000.
topAlg.MultiMuonFilter.Etacut = 2.7
topAlg.MultiMuonFilter.NMuons = 2
