## Default configuration of the photon filter
from GeneratorFilters.GeneratorFiltersConf import PhotonFilter
if not hasattr(topAlg, "PhotonFilter"):
    topAlg += PhotonFilter()
topAlg.PhotonFilter.Ptcut     = 10000.
topAlg.PhotonFilter.Etacut    = 2.7
topAlg.PhotonFilter.NPhotons  = 1
if "PhotonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  ["PhotonFilter"]
