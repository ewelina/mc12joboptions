## PYTHIA 6 config with Z2 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_343"
evgenConfig.tune = "Z2"
