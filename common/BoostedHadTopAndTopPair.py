if not hasattr(topAlg, "BoostedHadTopAndTopPair"):
    from GeneratorFilters.GeneratorFiltersConf import BoostedHadTopAndTopPair
    topAlg += BoostedHadTopAndTopPair("BoostedHadTopAndTopPair")

## Add this filter to the algs required to be successful for streaming
if "BoostedHadTopAndTopPair" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["BoostedHadTopAndTopPair"]

## Choose to cut on the pT of the top on the list for m_cutPtOf = 0,
## or to cut on the pT of the top decay products (b, q, q'bar) on the list for m_cutPtOf = 1
topAlg.BoostedHadTopAndTopPair.cutPtOf    = 1

## Default cut params (in MeV)
topAlg.BoostedHadTopAndTopPair.tHadPtMin    =       0.0
topAlg.BoostedHadTopAndTopPair.tHadPtMax    = 3000000.0
topAlg.BoostedHadTopAndTopPair.tPairPtMin   =       0.0
topAlg.BoostedHadTopAndTopPair.tPairPtMax   = 3000000.0

