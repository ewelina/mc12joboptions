## Common config for Alpgen+PYTHIA
assert hasattr(topAlg, "Pythia")
topAlg.Pythia.PythiaCommand += ["pyinit user alpgen", "pypars mstp 143 1"]
evgenConfig.generators = ["Alpgen", "Pythia"]
