## Common config for Alpgen+HERWIG+JIMMY with the CTEQ66 PDF
include("MC12JobOptions/Jimmy_AUET2_CTEQ66_Common.py")
include("MC12JobOptions/Jimmy_Alpgen.py")
