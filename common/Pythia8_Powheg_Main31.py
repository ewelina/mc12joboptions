## Configure Pythia 8 to shower PoWHEG input using Main31 shower veto
include("MC12JobOptions/Pythia8_Powheg.py")
topAlg.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2',
                             'TimeShower:pTmaxMatch = 2'  ]
topAlg.Pythia8.UserHook = 'Main31'