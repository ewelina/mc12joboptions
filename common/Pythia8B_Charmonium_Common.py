##############################################################
# Pythia8B_Charmonium_Common.py
#
# Common job options for direct charmonium production using
# Pythia8B.
##############################################################

# Hard process
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 1.'] # Equivalent of CKIN3
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Quarkonia production mode
topAlg.Pythia8B.Commands += ['Charmonium:all = on']
topAlg.Pythia8B.Commands += ['PhaseSpace:pTHatMinDiverge = 0.5']
topAlg.Pythia8B.SuppressSmallPT = True
topAlg.Pythia8B.pT0timesMPI = 1.
topAlg.Pythia8B.numberAlphaS = 3.
topAlg.Pythia8B.useSameAlphaSasMPI = False
topAlg.Pythia8B.SelectBQuarks = False
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = False
topAlg.Pythia8B.VetoDoubleCEvents = False


# Number of repeat-hadronization loops
topAlg.Pythia8B.NHadronizationLoops = 1

# List of B-species - for counting purposes (no effect on generation)
include("Pythia8B_i/BPDGCodes.py")

