## Truth four-jet filter common config for all JZx and JZxW
include("MC12JobOptions/MultiJetFilterAkt6.py")
topAlg.QCDTruthMultiJetFilter.Njet = 4
topAlg.QCDTruthMultiJetFilter.NjetMinPt = 10.*GeV
