## POWHEG+PYTHIA config for the AUET2B CT10 UE tune
include("MC12JobOptions/Pythia_AUET2B_CT10_Common.py")
evgenConfig.generators = ["Powheg", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
