## Truth jet filter config for JZ1
include("MC12JobOptions/JetFilter_JZX_Fragment.py")
topAlg.QCDTruthJetFilter.MinPt = 20.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 80.*GeV
