## Setup truth multi-particle filter to search for DPS in events generated with Jimmy 

from GeneratorFilters.GeneratorFiltersConf import MultiParticleFilter
if "MultiParticleFilter" not in topAlg:
    topAlg += MultiParticleFilter()
if "MultiParticleFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["MultiParticleFilter"]


topAlg.MultiParticleFilter.Np = 4
topAlg.MultiParticleFilter.ptMinParticle = 15.*GeV
topAlg.MultiParticleFilter.ptMaxParticle = 14000.*GeV
topAlg.MultiParticleFilter.etaRangeParticle = 999.
topAlg.MultiParticleFilter.particlePDG = [1,2,3,4,5,6,7,8,21]
topAlg.MultiParticleFilter.particleStatus = [123,124]
