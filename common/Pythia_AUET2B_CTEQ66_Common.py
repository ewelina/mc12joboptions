## PYTHIA 6 config with AUET2B-CTEQ66 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110005"
evgenConfig.tune = "AUET2B CTEQ66"
