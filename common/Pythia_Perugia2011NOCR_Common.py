## PYTHIA 6 config with Perugia2011-noCR tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_354"
evgenConfig.tune = "Perugia2011NOCR"
