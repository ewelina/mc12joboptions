## Instantiate the multi electron filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "MultiElectronFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
    topAlg += MultiElectronFilter()

## Add this filter to the algs required to be successful for streaming
if "MultiElectronFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["MultiElectronFilter"]

## Default cut params
topAlg.MultiElectronFilter.Ptcut = 8000.
topAlg.MultiElectronFilter.Etacut = 2.7
topAlg.MultiElectronFilter.NElectrons = 2
