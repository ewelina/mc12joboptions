## PYTHIA config for the AUET2B-LO** UE tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110002"
evgenConfig.tune = "AUET2B LO**"
