## Instantiate the dileptonmass filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "DiLeptonMassFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import DiLeptonMassFilter
    topAlg += DiLeptonMassFilter()

## Add this filter to the algs required to be successful for streaming
if "DiLeptonMassFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["DiLeptonMassFilter"]

## Default cut params
