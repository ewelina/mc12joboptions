## Truth jet filter config for JZ2 with R=0.4
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 80.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 200.*GeV
