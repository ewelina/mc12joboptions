## Truth jet filter config for JZ1 with R=0.4
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 20.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 80.*GeV
