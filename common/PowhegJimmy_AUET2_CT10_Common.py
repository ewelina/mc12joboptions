## POWHEG+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable LHEF mode
include("MC12JobOptions/Jimmy_AUET2_CT10_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
