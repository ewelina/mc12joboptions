include("MC12JobOptions/Pythia8_Base_Fragment.py")
topAlg.Pythia8.Commands += [
    "Tune:ee = 7",
    "Tune:pp = 14",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = NNPDF23_lo_as_0130_qed",
#    "PDF:pSet=LHAPDF6:NNPDF23_lo_as_0130_qed",
    "MultipartonInteractions:bProfile = 2",
    "MultipartonInteractions:pT0Ref = 2.45",
    "MultipartonInteractions:ecmPow = 0.21",
    "MultipartonInteractions:coreRadius = 0.55",
    "MultipartonInteractions:coreFraction = 0.9",
    "Diffraction:PomFlux = 4",
    "Diffraction:PomFluxEpsilon = 0.07",
    "Diffraction:PomFluxAlphaPrime = 0.25",
    "BeamRemnants:reconnectRange  = 1.8"]


