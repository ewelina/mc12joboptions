## Common job option for diffraction in Herwig++
## MPI tune is not relevant as the pdf_pomeron_cmds() function disables MPI
## Contact: Oldrich Kepka
include("MC12JobOptions/Herwigpp_Base_Fragment.py")
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(runArgs.ecmEnergy) + hw.base_cmds() + hw.lo_pdf_cmds("cteq6ll.LHpdf") + hw.pdf_pomeron_cmds("Pomeron2007", "2007")
topAlg.Herwigpp.Commands = cmds.splitlines()
del cmds

evgenConfig.tune = "NoneDiffractive" 
