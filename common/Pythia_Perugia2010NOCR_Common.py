## PYTHIA 6 config with Perugia2010-NOCR tune: no CR, less MPI
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_334"
evgenConfig.tune = "Perugia2010NOCR"
