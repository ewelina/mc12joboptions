# MC11 FSR syst. setup for ATLAS CTEQ6L1 [20110003] tune
#
# - PARP(72): multiplicative factor of the lam_QCD in running alpha_s used in FSR
#             central param. setting is motivated by ATLAS FSR QCD jet shapes,
#             variations correspond to *1/2 and *1.5 central value
# - PARJ(82): FSR low-pt cutoff

Pythia.PygiveCommand += [ "PARP(72)=0.2635", "PARJ(82)=1.66" ]
