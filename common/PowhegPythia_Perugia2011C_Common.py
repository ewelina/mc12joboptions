## POWHEG+PYTHIA config for the Perugia 2011C (CTEQ6L1 PDF) UE tune
include("MC12JobOptions/Pythia_Perugia2011C_Common.py")

# Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
evgenConfig.generators = ["Powheg", "Pythia"]

