###############################################################
# Job options file for Evgen MadGraph PYTHIA8 Leptosusy Generation
# J.A. Benitez 2013-04-01, 
#==============================================================

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
include ( "MC12JobOptions/Pythia8_MadGraph.py" )

#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
topAlg.Pythia8.Commands += ["2000011:mayDecay = no"]
topAlg.Pythia8.Commands += ["2000013:mayDecay = no"]
topAlg.Pythia8.Commands += ["1000015:mayDecay = no"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = 'MadGraph LeptoSusy S=%sGeV G=%sGeV' % (squarkmass, gluinomass)
evgenConfig.keywords = ["SUSY", "LeptoSusy", "longlived"]
evgenConfig.contact = ["benitez@cern.ch"]
evgenConfig.specialConfig = 'GMSBStau=300*GeV;GMSBSlepton=300*GeV;GMSBIndex=2;preInclude=SimulationJobOptions/preInclude.GMSB.py'
evgenConfig.inputfilecheck = 'group.phys-gener.madgraph.%s.LS_S%s_G%s.TXT.mc12_v1' % (runArgs.runNumber, squarkmass, gluinomass)
