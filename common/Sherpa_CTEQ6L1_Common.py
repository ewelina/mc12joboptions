## Sherpa config with CTEQ6L1 PDF
include("MC12JobOptions/Sherpa_Base_Fragment.py")
topAlg.Sherpa_i.Parameters += [
    "PDF_SET=cteq6l1",
    "K_PERP_MEAN_1=0.573",
    "K_PERP_MEAN_2=0.573",
    "K_PERP_SIGMA_1=0.898",
    "K_PERP_SIGMA_2=0.898",
    "PROFILE_PARAMETERS=0.884 0.828",
    "RESCALE_EXPONENT=0.244",
    "SCALE_MIN=2.65",
    "SIGMA_ND_FACTOR=0.788"
    ]
evgenConfig.tune = "CTEQ6L1"
