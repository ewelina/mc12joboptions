## Truth multi-jet filter setup for anti-kT R=0.6 truth jets

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()

include("MC12JobOptions/MultiJetFilter_Fragment.py")
topAlg.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt6TruthJets"
