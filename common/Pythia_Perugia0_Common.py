## PYTHIA 6 config with Perugia0 tune (CTEQ5L PDF)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_320"
evgenConfig.tune = "Perugia0"
