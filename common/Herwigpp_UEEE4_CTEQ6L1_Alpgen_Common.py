# Common job options for Alpgen+Herwig++ with CTEQ6L1 PDF and UEEE4 tune
# Contact: m.k.bugge@fys.uio.no

include('MC12JobOptions/Herwigpp_UEEE4_CTEQ6L1_Common.py')
include('MC12JobOptions/Herwigpp_Alpgen.py')
