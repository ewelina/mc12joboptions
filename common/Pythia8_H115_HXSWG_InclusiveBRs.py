## Configure Pythia 8 to decay Higgs with BRs of a 115 GeV SM Higgs boson,
## following recommendations of Higgs Cross Section Working Group (CERN Report 3, 2013 update).

## Might need to update the number when HXSec group update official BR ##

## Contact: 

assert hasattr(topAlg, "Pythia8")
topAlg.Pythia8.Commands += [
    "25:onMode = off",
    "25:addChannel = 1 0.00e00 100 1 -1",   # H->ddbar
    "25:addChannel = 1 0.00e00 100 2 -2",   # H->uubar
    "25:addChannel = 1 3.00e-4 100 3 -3",   # H->ssbar
    "25:addChannel = 1 3.55e-2 100 4 -4",   # H->ccbar
    "25:addChannel = 1 7.03e-1 100 5 -5",   # H->bbbar
    "25:addChannel = 1 0.00e00 100 6 -6",   # H->ttbar
    "25:addChannel = 1 4.93e-9 100 11 -11", # H->ee (not sure but almost 0)
    "25:addChannel = 1 2.63e-4 100 13 -13", # H->mumu
    "25:addChannel = 1 7.58e-2 100 15 -15", # H->tautau
    "25:addChannel = 1 8.76e-2 100 21  21", # H->gluon gluon
    "25:addChannel = 1 2.11e-3 100 22  22", # H->gamma gamma
    "25:addChannel = 1 7.10e-4 100 22  23", # H->gamma Z
    "25:addChannel = 1 8.64e-3 100 23  23", # H->ZZ
    "25:addChannel = 1 8.59e-2 100 24 -24", # H->WW
    ]
