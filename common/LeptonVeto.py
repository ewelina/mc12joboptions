## Instantiate the lepton filter, including adding it to the stream veto
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "LeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    topAlg += LeptonFilter()

## Add this filter to the algs required to be successful for streaming
if "LeptonFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs += ["LeptonFilter"]

## Default cut params
topAlg.LeptonFilter.Ptcut = 10000.
topAlg.LeptonFilter.Etacut = 2.8
