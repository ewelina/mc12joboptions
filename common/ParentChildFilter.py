if not hasattr(topAlg, "ParentChildFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
    topAlg += ParentChildFilter()

## Add this filter to the algs required to be successful for streaming
if "ParentChildFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ParentChildFilter"]

## Default cut params
topAlg.ParentChildFilter.PDGParent  = [23]
topAlg.ParentChildFilter.PtMinParent =  0.0
topAlg.ParentChildFilter.EtaRangeParent = 1000.
topAlg.ParentChildFilter.PDGChild = [13]
topAlg.ParentChildFilter.PtMinChild = 1000.
topAlg.ParentChildFilter.EtaRangeChild = 1000.
