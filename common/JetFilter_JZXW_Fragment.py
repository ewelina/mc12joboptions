## Extra chunk of config for JZxW slices, to be included in each JetFilter_JZxW.py file
## *after* the appropriate *_JZx.py fragment has been included.

## Use pT shaping in the truth jet filter
assert hasattr(topAlg, "QCDTruthJetFilter")
topAlg.QCDTruthJetFilter.DoShape = True

## Configure CountHepMC to disable the default UseEventWeight setting
## NB. CountHepMC will be later pushed to the end of the alg sequence
from TruthExamples.TruthExamplesConf import CountHepMC
topAlg += CountHepMC()
topAlg.CountHepMC.UseEventWeight = False
