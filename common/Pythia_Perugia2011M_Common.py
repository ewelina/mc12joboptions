## PYTHIA 6 config with Perugia2011-M tune (LO** PDF)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_355"
evgenConfig.tune = "Perugia2011M"
