## Instantiate the ParticleFilter, including adding it to the stream requirement
## Filter passed if a particle in truth chain has specific kinematics
## Configuration of the filter cuts is left to the specific JO
## mcasolin@cern.ch
	
if not hasattr(topAlg, "HiggsFilter"):
    from GeneratorFilters.GeneratorFiltersConf import HiggsFilter
    topAlg += HiggsFilter()
	
## Add this filter to the algs required to be successful for streaming
if "HiggsFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["HiggsFilter"]
	
## Default cut params
topAlg.HiggsFilter.Ptcut = 1.
