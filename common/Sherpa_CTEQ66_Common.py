## Sherpa config with CTEQ66 PDF
include("MC12JobOptions/Sherpa_Base_Fragment.py")
evgenConfig.tune = "CTEQ66"
## Nothing else to be specified: CTEQ66 is the default PDF (for Sherpa 1.3.0)!
