# dijet production with pythia

from Pyquench_i.Pyquench_iConf import Pyquench
topAlg += Pyquench()
evgenConfig.generators += ["Pyquen"]
topAlg.Pyquench.nuclearA = 208
topAlg.Pyquench.minBiasYN = False
topAlg.Pyquench.bimpact = 2
topAlg.Pyquench.sqrtSNN = 2760
topAlg.Pyquench.TuneIndex=129
topAlg.Pyquench.PythiaCommand += ["pyqpar T 0.5",
                                  "pyqpar tau 0.5",
                                  "pyqpar nfu 4",
                                  "pyqpar ienglu 0",
                                  "pyqpar ianglu 1"]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------

evgenConfig.minevents = 10
evgenLog.info("TestHepMC.EnergyDifference=10^7GeV")
from TruthExamples.TruthExamplesConf import TestHepMC
TestHepMC.EnergyDifference = 10000000*GeV
TestHepMC.EffFailThreshold=1e-9

