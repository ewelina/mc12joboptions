## Min bias truth jet filter for min bias sample preparation (low slice)

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()
a6alg = make_StandardJetGetter('AntiKt', 0.6, 'Truth').jetAlgorithmHandle()
evgenConfig.saveJets = True

include("MC12JobOptions/JetFilter_Fragment.py")
topAlg.QCDTruthJetFilter.MaxPt = 35.*GeV
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt6TruthJets"
topAlg.QCDTruthJetFilter.DoShape = False
