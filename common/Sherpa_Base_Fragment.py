## Base config for Sherpa
from Sherpa_i.Sherpa_iConf import Sherpa_i
topAlg += Sherpa_i()
evgenConfig.generators = ["Sherpa"]
evgenConfig.auxfiles = []

## Tell Sherpa to read its run card sections from the jO
topAlg.Sherpa_i.Parameters += [ 'RUNDATA=%s' % runArgs.jobConfig[0] ]
## Tell Sherpa to write logs into a separate file
## (need for production, looping job detection, Wolfgang Ehrenfeld)
topAlg.Sherpa_i.Parameters += [ 'LOG_FILE=sherpa.log' ]

## General ATLAS parameters
topAlg.Sherpa_i.Parameters += [
    "MASS[6]=172.5",
    "MASS[23]=91.1876",
    "MASS[24]=80.399",
    "WIDTH[23]=2.4952",
    "WIDTH[24]=2.085",
    "SIN2THETAW=0.23113",
    "MAX_PROPER_LIFETIME=10.0",
    "MI_HANDLER=Amisic"
    ]
