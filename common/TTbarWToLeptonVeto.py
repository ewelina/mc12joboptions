## Instantiate the TTbarWToLeptonFilter, including adding it to the stream requirement
## Filter passed if top, antitop and t->W->e/mu/tau in truth chain
## Configuration of the filter cuts is left to the specific JO
## Filter used to veto events with leptonic top decay  
## christoph.wasicki@cern.ch

if not hasattr(topAlg, "TTbarWToLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TTbarWToLeptonFilter
    topAlg += TTbarWToLeptonFilter()

## Default cut params
topAlg.TTbarWToLeptonFilter.Ptcut = 1.

## Add this filter to the algs required to be successful for streaming
if "TTbarWToLeptonFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs += ["TTbarWToLeptonFilter"]



