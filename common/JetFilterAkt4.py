## Truth jet filter setup for anti-kT R=0.4 truth jets

from JetRec.JetGetters import *
a4alg = make_StandardJetGetter('AntiKt', 0.4, 'Truth').jetAlgorithmHandle()

include("MC12JobOptions/JetFilter_Fragment.py")
topAlg.QCDTruthJetFilter.TruthJetContainer = "AntiKt4TruthJets"
