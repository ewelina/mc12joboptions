## POWHEG+PYTHIA config for the Perugia 2012 radHi (with alphaS(pT/2)) (CTEQ6L1 PDF) UE tune
include("MC12JobOptions/Pythia_Perugia2012radHi_Common.py")

# Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
evgenConfig.generators = ["Powheg", "Pythia"]
