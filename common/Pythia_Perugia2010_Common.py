## PYTHIA 6 config with Perugia2010 tune (CTEQ5L PDF)
## reference: Perugia2010 arXiv:1005.3457v1 [hep-ph]
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_327"
evgenConfig.tune = "Perugia2010"
