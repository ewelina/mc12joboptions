## Truth four jet filter config for JZ5
include("MC12JobOptions/FourJetFilter_JZX_Fragment.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 500.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 7000.*GeV
