## Truth jet filter config for JZ0 with R=0.4 and MinPt=4GeV
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 4.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 20.*GeV
