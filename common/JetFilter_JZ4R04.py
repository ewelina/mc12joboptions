## Truth jet filter config for JZ4 with R=0.4
include("MC12JobOptions/JetFilterAkt4.py")
topAlg.QCDTruthJetFilter.MinPt = 500.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 1000.*GeV
