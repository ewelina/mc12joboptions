## Herwig++ config for the CTEQ6L1 UE-EE-3 tune series with an LO ME PDF
include("MC12JobOptions/Herwigpp_Base_Fragment.py")

## Construct command set
from Herwigpp_i import config as hw
cmds = hw.energy_cmds(int(runArgs.ecmEnergy)) + hw.base_cmds() \
    + hw.lo_pdf_cmds("cteq6ll.LHpdf") \
    + hw.ue_tune_cmds("CTEQ6L1-UE-EE-%d-3" % int(runArgs.ecmEnergy))
topAlg.Herwigpp.Commands += cmds.splitlines()
del cmds

evgenConfig.tune = "CTEQ6L1-UE-EE-3"
