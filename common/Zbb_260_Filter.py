include("MC12JobOptions/BSubstruct_Filter_Fragment.py")

#this bit turns off the b-tagging to leave a cut on fat jet pT
filter.filterB = False
filter.filterC = False

#pT cuts in MeV
filter.pTMin = 260000.

StreamEVGEN.RequireAlgs += [ "BSubstruct" ]
