## Disable native tau decays
topAlg.Herwig.HerwigCommand += ["taudec TAUOLA"]

## TAUOLA config 
include("MC12JobOptions/Tauola_LeptonicDecay_Fragment.py" )
