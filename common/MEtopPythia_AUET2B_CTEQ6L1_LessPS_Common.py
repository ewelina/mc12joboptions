## Configuration for MEtop+PYTHIA 6 using the AUET2B_CTEQ6L1 LessPS tune
include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")
include("MC12JobOptions/Pythia_CTEQ6L1_LessPS_Common.py")
evgenConfig.generators = [ "MEtop", "Pythia"]

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]

