###############################################################
#
# Job options file for Evgen Single Monopole Generation
# W. Taylor, 2014-01-23
#==============================================================

PDG = 10000000 + int(float(charge)*100.0)

loE = (float(mass) + 10.)*1000.
hiE  = (float(mass) + 3000.)*1000.

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/ParticleGenerator_Common.py" )

# For VERBOSE output from ParticleGenerator.
topAlg.ParticleGenerator.OutputLevel = 1

topAlg.ParticleGenerator.orders = [
 "PDGcode: sequence -"+str(PDG)+" "+str(PDG),
 "energy: flat %s %s" % (loE,hiE),
 "eta: flat -3.0 3.0",
 "phi: flat -3.14159 3.14159"
 ]

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Single Qball generation for Mass=%s, Charge=%s in MC12" % (mass,charge)
evgenConfig.keywords = ["exotics", "Qball", "Single"]
evgenConfig.contact = ["wtaylor@cern.ch"]

evgenConfig.specialConfig = 'MASS=%s;CHARGE=%s;preInclude=SimulationJobOptions/preInclude.Qball.py' % (mass,charge)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with Qball PDGid and mass
#--------------------------------------------------------------
ALINE1="M %s                         %s.E+03      +0.0E+00 -0.0E+00 QBall           +" % (PDG,mass)
ALINE2="W %s                         0.E+00        +0.0E+00 -0.0E+00 QBall           +" % (PDG)

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2
