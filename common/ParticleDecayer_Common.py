## Common setup for ParticleDecayer
from ParticleDecayer.ParticleDecayerConf import ParticleDecayer
topAlg += ParticleDecayer()
topAlg.ParticleDecayer.McEventCollection = "GEN_EVENT"
evgenConfig.generators += ["ParticleDecayer"]
