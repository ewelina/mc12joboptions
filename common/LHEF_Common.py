## Should be included by any JOs using external events from LHEF files.
# TODO: Why not specify the actual generator used, e.g. MC@NLO, MadGraph, POWHEG, etc.?
evgenConfig.generators += ["LhaExt"]
