## Truth four jet filter config for JZ3
include("MC12JobOptions/FourJetFilter_JZX_Fragment.py")
topAlg.QCDTruthMultiJetFilter.MinLeadJetPt = 150.*GeV
topAlg.QCDTruthMultiJetFilter.MaxLeadJetPt = 300.*GeV
