## Instantiate the TTbarWToLeptonFilter, including adding it to the stream requirement
## Filter passed if top, antitop and t->W->e/mu/tau in truth chain
## Configuration of the filter cuts is left to the specific JO
## christoph.wasicki@cern.ch

if not hasattr(topAlg, "TTbarBoostCatFilter"):
    from GeneratorFilters.GeneratorFiltersConf import TTbarBoostCatFilter
    topAlg += TTbarBoostCatFilter()

## Add this filter to the algs required to be successful for streaming
if "TTbarBoostCatFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["TTbarBoostCatFilter"]

## Default cut params
topAlg.TTbarBoostCatFilter.Ptcut = 1.

topAlg.TTbarBoostCatFilter.TopPtcutLowedge = -1.
topAlg.TTbarBoostCatFilter.TopPtcutHighedge = -1.
topAlg.TTbarBoostCatFilter.WPtcutLowedge = -1.
topAlg.TTbarBoostCatFilter.WPtcutHighedge = -1.
topAlg.TTbarBoostCatFilter.TTMasscutLowedge = -1.
topAlg.TTbarBoostCatFilter.TTMasscutHighedge = -1.
topAlg.TTbarBoostCatFilter.LepsPtcutLowedge = -1.
topAlg.TTbarBoostCatFilter.LepsPtcutHighedge = -1.
topAlg.TTbarBoostCatFilter.NusPtcutLowedge = -1.
topAlg.TTbarBoostCatFilter.NusPtcutHighedge = -1.
#topAlg.TTbarBoostCatFilter.LepPtcutLowedge = 150000.
topAlg.TTbarBoostCatFilter.LepPtcutLowedge = -1.
topAlg.TTbarBoostCatFilter.LepPtcutHighedge = -1.
