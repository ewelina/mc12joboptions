## PYTHIA 6 config with AMBT1 tune (LO* PDF)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_340"
evgenConfig.tune = "AMBT1 LO*"
