include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.VetoAlgs:
    StreamEVGEN.VetoAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 150*GeV
topAlg.METFilter.MissingEtCalcOption = 1
