## POWHEG+PYTHIA config for the AUET2B-LO** UE tune
include("MC12JobOptions/Pythia_AUET2B_LOxx_Common.py")

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
