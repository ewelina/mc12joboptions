try:
    from JetRec.JetGetters import *
    antikt4=make_StandardJetGetter('AntiKt',0.4,'Truth',disable=False,outputCollectionName='AntiKt4TruthJets_IncAll',useInteractingOnly=False,includeMuons=True)
    antikt4alg = antikt4.jetAlgorithmHandle()
    antikt8=make_StandardJetGetter('AntiKt',0.8,'Truth',disable=False,outputCollectionName='AntiKt8TruthJets_IncAll',useInteractingOnly=False,includeMuons=True)
except Exception, e:    
    pass

from GeneratorFilters.GeneratorFiltersConf import Boosted2DijetFilter
topAlg += Boosted2DijetFilter()

topAlg.Boosted2DijetFilter.TruthContainerName="AntiKt4TruthJets_IncAll"
topAlg.Boosted2DijetFilter.TruthContainerName_largeR="AntiKt8TruthJets_IncAll"


