## PYTHIA 6 config with AUET2B-CT10 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110006"
evgenConfig.tune = "AUET2B CT10"
