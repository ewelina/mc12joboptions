########################################################
# OpenBJpsiDecays.py
# Opens all B->chi_c1 decays
########################################################

# B0
topAlg.Pythia8B.Commands += ['511:onIfAny = 20443']
# B+/-
topAlg.Pythia8B.Commands += ['521:onIfAny = 20443']
# Bs
topAlg.Pythia8B.Commands += ['531:onIfAny = 20443']
# Bc
topAlg.Pythia8B.Commands += ['541:onIfAny = 20443']
# LambdaB
topAlg.Pythia8B.Commands += ['5122:onIfAny = 20443']
# Xb+/-
topAlg.Pythia8B.Commands += ['5132:onIfAny = 20443']
# Xb
topAlg.Pythia8B.Commands += ['5232:onIfAny = 20443']
# Omega_b+/-
topAlg.Pythia8B.Commands += ['5332:onIfAny = 20443']

