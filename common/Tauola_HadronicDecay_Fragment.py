## Tauola config for hadronic decay (dmode = 2)
from Tauola_i.Tauola_iConf import Tauola
topAlg += Tauola()
topAlg.Tauola.TauolaCommand = \
    ["tauola polar 1",
     "tauola radcor 1",
     "tauola phox 0.01",
     "tauola dmode 2",
     "tauola jak1 0",
     "tauola jak2 0"]
evgenConfig.generators += [ "Tauola" ]
