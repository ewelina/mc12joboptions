include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METFilter
    topAlg += METFilter()

if "METFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs +=  [ "METFilter" ]

topAlg.METFilter.MissingEtCut = 60*GeV
topAlg.METFilter.MissingEtCalcOption = 1
