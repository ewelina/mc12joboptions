##  ParticleGenerator common to build a user-defined ET spectrum in [0-3] TeV
##  smooth ET spectrum optimized for MV calibration
##  author: Bruno Lenzi <Bruno.Lenzi@cern.ch>, July 2013
##	    J. Ocariz (Aug. 2014) : increase the range in the very-low ET region for tau studies

from AthenaPython import PyAthena
from AthenaPython.PyAthena import StatusCode
import os, numpy as np

def FD(x, a, r):
  "FD(x, a, r) -> Evaluate Fermi-Dirac function at point x with mu = a and kT = r" 
  return 1./(np.exp( (x/a - 1.) * r ) + 1)


class EvgenHistoAlg( PyAthena.Alg ):
  "To create and add to HistogramDataSvc a user-defined histogram for single electron/photon samples"
  def __init__(self, name = 'EvgenHistoAlg', histoName = 'evgen', **kw):
    kw['histoName'] = histoName
    super(EvgenHistoAlg, self).__init__(name, **kw)
  
  def initialize(self):
    self.msg.info("initializing [%s]", self.name())
    
    # Retrieve HistogramDataSvc, book and fill histogram
    self.hsvc = PyAthena.py_svc('HistogramDataSvc', iface='IHistogramSvc')
    if not self.hsvc:
      self.msg.error('could not retrieve HistogramDataSvc')
      return StatusCode.Failure
    
    numberOfBins, xLowLimit, xHighLimit = 2999, 1.0e3, 3e6
    histoLocation = os.path.join('/stat/particlegenerator', self.histoName)
    self.histo = self.hsvc.book(histoLocation,  # Full path, including histogram ID
      "egamma Et distribution", # Histogram title
      numberOfBins,             # Number of x-axis bins
      xLowLimit,                # x-axis lower limit
      xHighLimit                # x-axis upper limit
    )
    
    # The histogram: 
    # Increase (-Fermi-Dirac function) from 0 to ~1 GeV then flat
    # Decrease (Fermi-Dirac function) from ~80-120 GeV then flat until 3 TeV
    # Densities/GeV: 0.02 @ 3 GeV, 0.03 @ 10-80 GeV, 0.015 @ 100 GeV, 0.00015 @ 150 GeV - 3 TeV
    x0, x1, r0, r1 = 0.5e3, 100e3, 1, 20
    X =  np.linspace(xLowLimit, xHighLimit, numberOfBins + 1)
    Y =  -FD(X, x0, r0) + FD(X, x1, r1) + 0.005
    _ = map(self.histo.fill, X, Y)
    return StatusCode.Success
  
  def execute(self):
    return StatusCode.Success
  
  def finalize(self):
    self.msg.info("finalizing [%s]", self.name())
    return StatusCode.Success

if __name__ == '__main__':
  from AthenaCommon.AlgSequence import AthSequencer, AlgSequence
  seq = AthSequencer("AthFilterSeq")
  job = AlgSequence()
  evgenH = EvgenHistoAlg()
  job += evgenH
