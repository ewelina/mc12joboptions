## Base config for HERWIG 6.5 + Jimmy 4.31 using the AUET2 LO** tune
from Herwig_i.Herwig_iConf import Herwig
topAlg += Herwig()

include("MC12JobOptions/Jimmy_Base_Fragment.py")
include("MC12JobOptions/Jimmy_AUET2B_LOxx_Fragment.py")
