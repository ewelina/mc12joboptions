include( "ParticleBuilderOptions/MissingEtTruth_jobOptions.py" )
METPartTruth.TruthCollectionName="GEN_EVENT"
topAlg.METAlg+=METPartTruth

if not hasattr(topAlg, "METRhadFilter"):
    from GeneratorFilters.GeneratorFiltersConf import METRhadFilter
    topAlg += METRhadFilter()

if "METRhadFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += [ "METRhadFilter" ]

topAlg.METRhadFilter.MissingEtCut = 160*GeV
topAlg.METRhadFilter.MissingEtCalcOption = 1
