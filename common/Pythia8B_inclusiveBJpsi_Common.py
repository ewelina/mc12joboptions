##############################################################
# Pythia8B_inclusiveBJpsi_Common.py
#
# Common job options for inclusive b->J/psi production using
# Pythia8B.
##############################################################

# Hard process
topAlg.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
topAlg.Pythia8B.Commands += ['ParticleDecays:mixB = off']
topAlg.Pythia8B.Commands += ['HadronLevel:all = off']

# Event selection
topAlg.Pythia8B.SelectBQuarks = True
topAlg.Pythia8B.SelectCQuarks = False
topAlg.Pythia8B.VetoDoubleBEvents = True
topAlg.Pythia8B.UserSelection = 'BJPSIINCLUSIVE'

# Close B decays and open antiB decays
include ("Pythia8B_i/CloseBDecays.py")

# Open inclusive B->J/psi decays
include ("Pythia8B_i/OpenBJpsiDecays.py")

# List of B-species
include("Pythia8B_i/BPDGCodes.py")
