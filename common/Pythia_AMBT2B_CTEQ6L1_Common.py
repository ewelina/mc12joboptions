## PYTHIA 6 config with AMBT2B tune (CTEQ6L1 PDF)
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "ATLAS_20110103"
evgenConfig.tune = "AMBT2B CTEQ6L1"
