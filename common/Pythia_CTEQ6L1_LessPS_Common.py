# MC12 PartonShower (ISR+FSR) syst. setup for ATLAS CTEQ6L1 [20110003] tune
# uses jet gap fraction [ATL-COM-PHYS-2011-1740] for ISR parameter settings
# uses jet shapes in ttbar events (G. Compostella, https://indico.cern.ch/conferenceDisplay.py?confId=196737)
# for FSR parameter settings
#
# - PARP(67): controls high-pt ISR branchings phase-space;
#             ISR branchings with pTevol > m_dip/2 * PARP(67) are power suppressed
#             by a factor (m_dip/(2pTevol))**2
# - PARP(64): multiplicative factor of the mom. scale^2 in running alpha_s used in ISR
# - PARP(72): multiplicative factor of the lam_QCD in running alpha_s used in FSR
# - PARJ(82): FSR low-pt cutoff

topAlg.Pythia.PygiveCommand += [ "PARP(67)=0.75", "PARP(64)=4.08", "PARP(72)=0.150", "PARJ(82)=1.66" ]
