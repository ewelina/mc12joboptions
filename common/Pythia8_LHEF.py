## Configure Pythia8 to read input events from an LHEF file
assert hasattr(topAlg, "Pythia8")
topAlg.Pythia8.LHEFile = "events.lhe"
topAlg.Pythia8.CollisionEnergy = int(runArgs.ecmEnergy)
