## PYTHIA 6 config with Z1 tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_341"
evgenConfig.tune = "Z1"
