## Truth jet filter config for JZ4
include("MC12JobOptions/JetFilter_JZX_Fragment.py")
topAlg.QCDTruthJetFilter.MinPt = 500.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 1000.*GeV
