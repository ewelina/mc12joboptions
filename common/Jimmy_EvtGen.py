## Run EvtGen afterburner on top of Herwig/Jimmy
assert hasattr(topAlg, "Herwig")
include("MC12JobOptions/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8.pdt']
topAlg.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"

