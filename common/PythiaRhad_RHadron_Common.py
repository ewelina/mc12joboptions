## R-hadron evgen config

include("MC12JobOptions/PythiaRhad_Common.py")

if CASE == 'gluino':
    topAlg.PythiaRhad.RunGluinoHadrons = True
    topAlg.PythiaRhad.RunStopHadrons = False
    topAlg.PythiaRhad.RunSbottomHadrons = False
    if 'GBALLPROB' in globals():
        topAlg.PythiaRhad.GluinoBallProbability = GBALLPROB
elif CASE == 'stop':
    topAlg.PythiaRhad.RunGluinoHadrons = False
    topAlg.PythiaRhad.RunStopHadrons = True
    topAlg.PythiaRhad.RunSbottomHadrons = False
elif CASE == 'sbottom':
    topAlg.PythiaRhad.RunGluinoHadrons = False
    topAlg.PythiaRhad.RunStopHadrons = False
    topAlg.PythiaRhad.RunSbottomHadrons = True

# Gluino case
if topAlg.PythiaRhad.RunGluinoHadrons:
    topAlg.PythiaRhad.PythiaCommand += [
	"pymssm rmss 12 4000.0",		# stop1 mass
	"pymssm rmss 3 "+str(MASS)+".0"]	# gluino pole mass
    msub = 244 # gg -> ~g~g process
    if 'PROC' in globals() and PROC == 'ffbar':
        msub = 243 # Turn on ffbar -> ~g~g
    topAlg.PythiaRhad.PythiaCommand += ["pysubs msub %d 1" % msub]

# Stop/sbottom case
elif topAlg.PythiaRhad.RunStopHadrons or topAlg.PythiaRhad.RunSbottomHadrons:
    job.PythiaRhad.PythiaCommand += [
	"pymssm rmss 12 "+str(MASS)+".0",	# stop1 mass
	"pymssm rmss 3 4000.0",			# gluino pole mass
	"pysubs msub 261 1",			# Turn on ffbar -> stop1stop1bar
	"pysubs msub 264 1"]			# Turn on gg -> stop1stop1bar

# TODO: What's this?
# LocalWords: REGGE
