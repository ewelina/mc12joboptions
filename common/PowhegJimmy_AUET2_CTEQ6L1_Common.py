## POWHEG+HERWIG+JIMMY config -- just include a standard HERWIG+JIMMY tune and enable LHEF mode
# TODO: Should we be using an NLO PDF tune with POWHEG cf. PYTHIA Z pT tuning study?
include("MC12JobOptions/Jimmy_AUET2_CTEQ6L1_Common.py")
topAlg.Herwig.HerwigCommand += ["iproc lhef"]
