## PYTHIA 6 config with Z2star tune ; Z2 + PARP(82)=1.927 + PARP(90)=0.225
## p16 http://www.phys.ufl.edu/~rfield/cdf/QCD@LHC2011_RickField_8-25-11.pdf 
include("MC12JobOptions/Pythia_Base_Fragment.py")

# tune setting:
# usually:
# Pythia.Tune_Name="PYTUNE_343"

# but for param overwriting set it like this:
#----------------------------------------------------------------------
# use only recommended ATLAS parameter settings (i.e. the ones necessary for succsfull && consistent simulation within Athena)
Pythia.Tune_Name="ATLAS_-1"
# this sets: 
# mstp(128)  =1,      // fix junk output for documentary particles       
#this->pydat1().mstu(21)   =1;      // error handling switch
#this->pypars().mstp(81)   =21;     // run PYEVNW with PYEVNT
#this->pydat1().mstj(22)=2;         // stable particles convention
# all other ATLAS defaults e.g. the ATLAS default tune are switched off
#----------------------------------------------------------------------
topAlg.Pythia.Direct_call_to_pytune=343
topAlg.Pythia.PygiveCommand+=[ "PARP(82)=1.927", "PARP(90)=0.225" ]
evgenConfig.tune = "Z2star"

