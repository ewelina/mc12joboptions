## Truth jet filter config for JZ2
include("MC12JobOptions/JetFilter_JZX_Fragment.py")
topAlg.QCDTruthJetFilter.MinPt = 80.*GeV
topAlg.QCDTruthJetFilter.MaxPt = 200.*GeV
