## Config for Py8 tune A2 with CTEQ6L1
include("MC12JobOptions/Pythia8_Base_Fragment.py")

topAlg.Pythia8.Commands += [
    "Tune:pp = 5",
    "PDF:useLHAPDF = on",
    "PDF:LHAPDFset = cteq6ll.LHpdf"]
evgenConfig.tune = "4C"
