## Configuration for POWHEG with PYTHIA 6
include("MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py")

## Read external Les Houches event file
topAlg.Pythia.PythiaCommand += ["pyinit user lhef"]
