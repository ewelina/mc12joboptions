## Instantiate the lepton filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "MultiObjectsFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MultiObjectsFilter
    topAlg += MultiObjectsFilter()

## Add this filter to the algs required to be successful for streaming
if "MultiObjectsFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["MultiObjectsFilter"]

## Default cut params
