## TAUOLA config for PYTHIA

## Disable native tau decays
assert hasattr(topAlg, "Pythia")
topAlg.Pythia.PythiaCommand += ["pydat3 mdcy 15 1 0"]

## Enable TAUOLA
include("MC12JobOptions/Tauola_Fragment.py")
