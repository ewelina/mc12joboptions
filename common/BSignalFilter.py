## Instantiate the BSignal filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "BSignalFilter"):
    from GeneratorFilters.GeneratorFiltersConf import BSignalFilter
    topAlg += BSignalFilter()

## Add this filter to the algs required to be successful for streaming
if "BSignalFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["BSignalFilter"]
