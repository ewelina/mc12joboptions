if not hasattr(topAlg, "DecayModeFilter"):
    from GeneratorFilters.GeneratorFiltersConf import DecayModeFilter
    topAlg += DecayModeFilter()

if "DecayModeFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += [ "DecayModeFilter" ]

topAlg.DecayModeFilter.modeID = "D"
