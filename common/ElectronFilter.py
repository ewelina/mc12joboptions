## Instantiate the electron filter, including adding it to the stream requirement
## Configuration of the filter cuts is left to the specific JO
if not hasattr(topAlg, "ElectronFilter"):
    from GeneratorFilters.GeneratorFiltersConf import ElectronFilter
    topAlg += ElectronFilter()

## Add this filter to the algs required to be successful for streaming
if "ElectronFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["ElectronFilter"]

## Default cut params
topAlg.ElectronFilter.Ptcut = 8000.
topAlg.ElectronFilter.Etacut = 2.7
