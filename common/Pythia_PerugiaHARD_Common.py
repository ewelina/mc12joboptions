## PYTHIA 6 config with Perugia HARD tune
include("MC12JobOptions/Pythia_Base_Fragment.py")
topAlg.Pythia.Tune_Name = "PYTUNE_321"
evgenConfig.tune = "PerugiaHARD"
