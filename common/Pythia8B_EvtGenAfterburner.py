## Common config for adding the EvtGen decayer to Pythia8

assert hasattr(topAlg, "Pythia8B")
evgenConfig.generators += ["EvtGen"]
#
# For now, run photos inside EvtGen.  inclusive.dec does that, while inclusiveP8.dec turns
# Photos off so we can run it in afterburner mode.  The two .dec files are indentical otherwise.
#
#evgenConfig.auxfiles += ['inclusiveP8.dec', 'inclusiveP8.pdt']
evgenConfig.auxfiles += ['inclusive.dec', 'inclusiveP8.pdt']

# This code sets up EvtGen for inclusive decays

# We believe this code is no longer needed with the new particle data tables
#from HepMCTools.HepMCToolsConf import TranslateParticleCodes
#topAlg += TranslateParticleCodes()
#topAlg.TranslateParticleCodes.OutputLevel = INFO
#topAlg.TranslateParticleCodes.translateToPDTFrom = "PYTHIA"
#topAlg.TranslateParticleCodes.translateFromPDTTo = "EVTGEN"
#topAlg.TranslateParticleCodes.test = [0,10000000]

from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
topAlg += EvtInclusiveDecay()
topAlg.EvtInclusiveDecay.OutputLevel = INFO
topAlg.EvtInclusiveDecay.pdtFile = "inclusiveP8.pdt"
topAlg.EvtInclusiveDecay.decayFile = "inclusive.dec"

## Disable B-mixing since EvtGen will do this
topAlg.Pythia8B.Commands += ["ParticleDecays:mixB = off"]

## Turn off afterburner since we are running photos in EvtGen for now
## Use Photos++ for QED FSR from leptons
###include("MC12JobOptions/Pythia8_Photos.py")
###topAlg.Photospp.DelayInitialisation=True
