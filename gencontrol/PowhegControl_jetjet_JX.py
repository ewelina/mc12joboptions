##--------------------------------------------------------------
##
## Job options file for POWHEG OTF setup
## Author: James Robinson 23.10.2013 <james.robinson@cern.ch>
## Slicing added by Zach Marshall 8.11.2013 <zach.marshall@cern.ch>
##
##--------------------------------------------------------------

run_kt_filter = {
  184150 : [   5,0] ,
  184151 : [   5,1] ,
  184152 : [  30,2] ,
  184153 : [  80,3] ,
  184154 : [ 250,4] ,
  184155 : [ 600,5] ,
  184156 : [ 900,6] ,
  184157 : [1500,7] ,
  184160 : [   5,0] ,
  184161 : [   5,1] ,
  184162 : [  30,2] ,
  184163 : [  80,3] ,
  184164 : [ 250,4] ,
  184165 : [ 600,5] ,
  184166 : [ 900,6] ,
  184167 : [1500,7] ,
  187490 : [   5,0] ,
  187491 : [   5,1] ,
  187492 : [  30,2] ,
  187493 : [  80,3] ,
  187494 : [ 250,4] ,
  187495 : [ 600,5] ,
  187496 : [ 900,6] ,
  187497 : [1500,7] }

import sys
if not 'runArgs' in dir():
    print 'Please run inside of a job transform!'
    sys.exit(1)
if not hasattr(runArgs,'runNumber'):
    evgenLog.error('No run number found, please fix your command line.')
    sys.exit(1)
if not runArgs.runNumber in run_kt_filter:
    evgenLog.error('Run number '+str(runArgs.runNumber)+' not found in dictionary: '+str(run_kt_filter))
    sys.exit(1)

# This includes the setting of random seeds and c.o.m. energy
include('PowhegControl/PowhegControl_Dijet_Common.py')
PowhegConfig.bornktmin = run_kt_filter[runArgs.runNumber][0]
PowhegConfig.pdf = 10800
PowhegConfig.fixspikes = True
PowhegConfig.withnegweights = 0 #1 

# This is not a total guess
PowhegConfig.nEvents=30000

PowhegConfig.ncall2    = 20000

print PowhegConfig

PowhegConfig.generateRunCard()
PowhegConfig.generateEvents()

##--------------------------------------------------------------
## EVGEN transform
##--------------------------------------------------------------
evgenConfig.keywords       = [ "QCD", "dijet", "jets","JZ%i"%(run_kt_filter[runArgs.runNumber][1]) ]
evgenConfig.contact        = [ "james.robinson@cern.ch" , 'ZLMarshall@lbl.gov' ]

evgenConfig.weighting      = 0      # to avoid failure with high weights - TODO: remove this if fix is implemented
evgenConfig.minevents      = 500   # allow some of the 5k events to fail TestHepMC
if runArgs.runNumber%10 in [5,4]: # 7 only necessary here for 7 TeV running
    evgenConfig.minevents      = 200   # allow some of the 5k events to fail TestHepMC
if runArgs.runNumber%10 in [3,6,7]: # 6 only necessary here for 7 TeV running
    evgenConfig.minevents      = 100   # allow some of the 5k events to fail TestHepMC

if 184150<=runArgs.runNumber and runArgs.runNumber<=184157:
    evgenConfig.description    = "POWHEG+Pythia8 dijet production with bornktmin = %i GeV, muR=muF=1 and AU2 CT10 tune"%(run_kt_filter[runArgs.runNumber][0])
    evgenConfig.generators    += ["Powheg","Pythia8" ]

    ##--------------------------------------------------------------
    ## Pythia8 showering with new, main31-style shower
    ##--------------------------------------------------------------
    include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
    # -- New release (18.X.X)
    include("MC12JobOptions/Pythia8_LHEF.py")
    topAlg.Pythia8.Commands += [ "SpaceShower:pTmaxMatch = 2"
                               , "TimeShower:pTmaxMatch = 2"
                               ]
    topAlg.Pythia8.UserHook = "Main31"
elif 187490<=runArgs.runNumber and runArgs.runNumber<=187497:
    evgenConfig.description    = "POWHEG+Pythia8 dijet production with bornktmin = %i GeV, bornsuppfact = 250 GeV, muR=muF=1 and AU2 CT10 tune"%(run_kt_filter[runArgs.runNumber][0])
    evgenConfig.generators    += ["Powheg","Pythia8" ]

    ##--------------------------------------------------------------
    ## Pythia8 showering with new, main31-style shower
    ##--------------------------------------------------------------
    include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
    # -- New release (18.X.X)
    include("MC12JobOptions/Pythia8_LHEF.py")
    topAlg.Pythia8.Commands += [ "SpaceShower:pTmaxMatch = 2"
                               , "TimeShower:pTmaxMatch = 2"
                               ]
    topAlg.Pythia8.UserHook = "Main31"
elif 184160<=runArgs.runNumber and runArgs.runNumber<=184167:
    evgenConfig.description    = "POWHEG+fHerwig dijet production with bornktmin = %i GeV, bornsuppfact = 250 GeV, muR=muF=1 and AU2 CT10 tune"%(run_kt_filter[runArgs.runNumber][0])
    evgenConfig.generators    += [ "Powheg","Herwig" ]

    ##--------------------------------------------------------------
    ## Herwig/Jimmy Showering
    ##--------------------------------------------------------------
    include("MC12JobOptions/PowhegJimmy_AUET2_CT10_Common.py")

# Include jet filter
include('MC12JobOptions/JetFilter_JZ%i.py'%(run_kt_filter[runArgs.runNumber][1]))

