include("MC12JobOptions/FourLeptonMassVeto.py")
topAlg.FourLeptonMassFilter.MinPt = 5000.
topAlg.FourLeptonMassFilter.MaxEta = 3.
topAlg.FourLeptonMassFilter.MinMass1 = 40000.      #was 60000.
topAlg.FourLeptonMassFilter.MaxMass1 = 14000000.
topAlg.FourLeptonMassFilter.MinMass2 = 10000.      #was 12000.
topAlg.FourLeptonMassFilter.MaxMass2 = 14000000.
topAlg.FourLeptonMassFilter.AllowElecMu = True
topAlg.FourLeptonMassFilter.AllowSameCharge = True
