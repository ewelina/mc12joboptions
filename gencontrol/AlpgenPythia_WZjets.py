# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]

# Run numbers currently reserved: 147001 - 147151
# W+jets samples obo L. Mijovic
if run_num>=147001 and run_num<=147048:
    log.info('Recognized W+jets run number.  Will generate for run '+str(run_num))
    process = 'wjet'
    jets = [(run_num-147001)%8,1] # Exclusive matching to the last number of partons
    #     if jets[0]>5: jets[1]=0 # TK use same schema like for Zjets
    # Run # 5, Np5 incl., Run # 6, Np5 excl.  Run # 7, Np6 incl.

    if (run_num-147001)>=24: UseHerwig=False # Move to Pythia for half the samples
    #               10 min  30 min  55 min  9 hrs    10 hrs
    #               20 hrs     ???      ???
    event_numbers=[1000000,5000000,15000000,30000000,10000000,
                   60000000,10000000,10000000]
    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    
    # 0:5000  1:5000  2:5000  3:5000  4:5000 

    events_athena = 5000
    if jets[0]==2: events_athena=1000 
    if jets[0]==3: events_athena=1000 
    if jets[0]==4: events_athena=200 
    if jets[0]==5: events_athena=100 # Should work

    
    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]==5: jets[1]=0 # 5jet incl

    W_mode = int((run_num-147001)/8)%3+1
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau)\n '%(W_mode)
    special2 = special1

# Z+jets samples - thanks to O. Rosenthal; modified by B. Di Micco
if run_num>=147073 and run_num<=147136:
    log.info('Recognized Z+jets run number.  Will generate for run '+str(run_num))
    process = 'zjet'
    ZJET=True
    run_min=147073
    jets = [(run_num-run_min)%8,1] # Exclusive matching to the last number of partons

    if (run_num-run_min)>=32: UseHerwig=False # Move to Pythia for half the samples
#    event_numbers=[1000000,5000000,10000000,30000000,1000000,
#                   20000000,10000000,10000000]

    event_numbers=[1000000,5000000,15000000,30000000,15000000,
                   60000000,10000000,10000000]

    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    
    events_athena = 5000
    if jets[0]==2: events_athena=1000
    if jets[0]==3: events_athena=1000
    if jets[0]==4: events_athena=200
    if jets[0]==5: events_athena=100


    Z_mode = int((run_num-147073)/8)%4+1

    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""
    if jets[0]==5: jets[1]=0

    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """
#    if not UseHerwig: special1 += """xclu   0.26          ! ???
#ipclu 1         ! ???
#"""
    special2 = special1

# Z+jets samples - thanks to O. Rosenthal; modified by B. Di Micco
if (run_num>=181680 and run_num<=181693) or (run_num>=189110 and run_num<=189123) or (run_num>=189566 and run_num<=189581):
    #181680-181693: Z->ee/mumu Alpgen+Pythia,  mumu starats at 181688
    #189110-189123: Z->ee/mumu Alpgen+Herwig,  mumu starats at 189131
    #189566-189581: Z->tautau, Alpgen+Pythia 189566-189573, else Alpgen+Herwig
    log.info('Recognized Z+jets run number.  Will generate for run '+str(run_num))
    process = 'zjet'
    ZJET=True
    run_min=181680
    if run_num >=189110 and run_num<189566 : run_min=189110
    elif run_num>=189566 and run_num<=189573 : run_min=189566
    elif run_num>=189574 and run_num<=189581 : run_min=189574
    jets = [(run_num-run_min)%8,1] # Exclusive matching to the last number of partons

    UseHerwig=False
    if run_min == 189110 or run_min == 189574 : UseHerwig=True
#    event_numbers=[1000000,5000000,10000000,30000000,1000000,
#                   20000000,10000000,10000000]

    event_numbers=[1000000,5000000,15000000,30000000,30000000,
                   60000000/2,10000000,10000000]

    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    
    events_athena = 5000
    if jets[0]==2: events_athena=1000
    if jets[0]==3: events_athena=1000
    if jets[0]==4: events_athena=100
    if jets[0]==5: events_athena=20


    Z_mode = int((run_num-run_min)/8)%4+1

    if run_min>=189566 : Z_mode = 3

    special1 = """mllmin  100           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll
"""
    if jets[0]==5: jets[1]=0

    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """
#    if not UseHerwig: special1 += """xclu   0.26          ! ???
#ipclu 1         ! ???
#"""
    special2 = special1


# Wc+jets samples - thanks to O. Rosenthal
if run_num>=200000 and run_num<=200064:
    log.info('Recognized Wc+jets run number.  Will generate for run '+str(run_num))
    process = 'wcjet'
    jets = [(run_num-200000)%8,1] # Exclusive matching to the last number of partons
    if jets[0]==4: jets[1]=0

    if (run_num-200000)>=32: UseHerwig=False # Move to Pythia for half the samples

    #               10 min  30 min  55 min  9 hrs    10 hrs
    #               20 hrs     ???      ???
#    event_numbers=[5000000,5000000,30000000,12000000,
#                   10000000,10000000,10000000,1000000]

    event_numbers=[1000000,6000000,10000000,25000000,
                   25000000,10000000,10000000,1000000]
    ncwarm = [2,3,3,4,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]

    events_athena = 5000
    if jets[0]==2: events_athena=1000
    if jets[0]==3: events_athena=500
    if jets[0]==4: events_athena=200
#    if jets[0]==5: events_athena=100 # Should work
#    if jets[0]==6: events_athena=50
#    if jets[0]==7: events_athena=200

    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
#    if jets[0]==7: jets[0]=5

    W_mode = int((run_num-200000)/8)%4+1
    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all leptons)\n '%(W_mode)
    special1 +="""
mc      0           ! quark c mass
ptcmin  10.            ! quark c pt min
 """
    special2 = special1

# Wcc+jets samples - thanks to O. Rosenthal
if run_num>=200100 and run_num<=200164:
    log.info('Recognized Wcc+jets run number.  Will generate for run '+str(run_num))
    process = 'wqq'
    jets = [(run_num-200100)%8,1] # Exclusive matching to the last number of partons
    if jets[0]==3: jets[1]=0

    if (run_num-200100)>=32: UseHerwig=False # Move to Pythia for half the samples

    #               10 min  30 min  55 min  9 hrs    10 hrs
    #               20 hrs     ???      ???
    event_numbers=[5000000,15000000,30000000,
                   50000000,1000000,1000000,10000000,10000000]
    ncwarm = [3,3,4,4,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]

    events_athena = 1000
    if jets[0]==1: events_athena=500
    if jets[0]==2: events_athena=200
    if jets[0]==3: events_athena=100
#   if jets[0]==3: events_athena=200
#    if jets[0]==4: events_athena=100

    W_mode = int((run_num-200100)/8)%4+1
    special1 = """iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4: all leptons)
ihvy 4        ! Select W+cc
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptcmin  0.            ! quark c pt min
drcmin  0.            ! deltar c
 """%(W_mode)
    special2 = special1

# Wbb+jets samples - thanks to O. Roseg reinnthal


if run_num>=200200 and run_num<=200264:
    log.info('Recognized Wbb+jets run number.  Will generate for run '+str(run_num))
    process = 'wqq'
    jets = [(run_num-200200)%8,1] # Exclusive matching to the last number of partons
    if jets[0]==3: jets[1]=0

    if (run_num-200200)>=32: UseHerwig=False # Move to Pythia for half the samples

    #               10 min  30 min  55 min  9 hrs    10 hrs
    #               20 hrs     ???      ???
    event_numbers=[5000000,15000000,30000000,
                   50000000,1000000,1000000,10000000,10000000]

    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]

    # 0:5000  1:5000  2:5000  3:5000  4:5000 

    events_athena = 1000
    if jets[0]==1: events_athena=500
    if jets[0]==2: events_athena=200
    if jets[0]==3: events_athena=100
#    if jets[0]==4: events_athena=750
#    if jets[0]==5: events_athena=200 # Should work
#    if jets[0]==6: events_athena=50
#    if jets[0]==7: events_athena=100

    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]==7: jets[0]=5

    W_mode = int((run_num-200200)/8)%4+1
    special1 = """iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau, 4:all leptons)
ihvy 5        ! Select W+bb
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptbmin  0.            ! quark b pt min
drbmin  0.            ! deltar b
 """%(W_mode)
    special2 = special1

# Zbb+jets samples - thanks to O. Rosenthal
if run_num>=200300 and run_num<=200364:
    log.info('Recognized Zbb+jets run number.  Will generate for run '+str(run_num))
    process = 'zqq'
    jets = [(run_num-200300)%8,1] # Exclusive matching to the last number of partons
    if jets[0]==3: jets[1]=0

    if (run_num-200300)>=32: UseHerwig=False # Move to Pythia for half the samples
    event_numbers=[5000000,15000000,30000000,
                   50000000,1000000,1000000,10000000,10000000]    
    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    
    events_athena = 1000
    if jets[0]==1: events_athena=500
    if jets[0]==2: events_athena=200
    if jets[0]==3: events_athena=100
#    if jets[0]==3: events_athena=1000
#    if jets[0]==4: events_athena=500
#    if jets[0]==5: events_athena=100
#    if jets[0]==6: events_athena=50
#    if jets[0]==7: events_athena=100

    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]==7: jets[0]=5

    Z_mode = int((run_num-200300)/8)%4+1
    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll 
ihvy    5             ! Select Z+bb
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptbmin  0.            ! quark b pt min
drbmin  0.            ! deltar b
"""
    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """

    special2 = special1

# Zcc+jets samples - thanks to O. Rosenthal
if run_num>=200400 and run_num<=200464:
    log.info('Recognized Zcc+jets run number.  Will generate for run '+str(run_num))
    process = 'zqq'
    jets = [(run_num-200400)%8,1] # Exclusive matching to the last number of partons
    if jets[0]==3: jets[1]=0

    if (run_num-200400)>=32: UseHerwig=False # Move to Pythia for half the samples
    event_numbers=[5000000,15000000,30000000,
                   50000000,1000000,1000000,10000000,10000000]        
    ncwarm = [2,2,3,3,4,4,4,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    
    events_athena = 1000
    if jets[0]==2: events_athena=500
    if jets[0]==2: events_athena=200
    if jets[0]==3: events_athena=100
#    if jets[0]==4: events_athena=500
#    if jets[0]==5: events_athena=100
#    if jets[0]==6: events_athena=50
#    if jets[0]==7: events_athena=100

    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]==7: jets[0]=5

    Z_mode = int((run_num-200400)/8)%4+1
    special1 = """mllmin  60           ! Minimum M_ll
mllmax  2000          ! Maximum M_ll 
ihvy    4             ! Select Z+cc
mc      1.4           ! quark c mass
mb      4.7           ! quark b mass
ptcmin  0.            ! quark c pt min
drcmin  0.            ! deltar c
"""
    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """

    special2 = special1
    

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
        log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
        inputgridfile=runArgs.inputGenConfFile
            

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3,inputgridfile=inputgridfile)

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include ( "MC12JobOptions/AlpgenPythia_Perugia2011C_Common.py" )
    include ( "MC12JobOptions/Pythia_Tauola.py" )
    include ( "MC12JobOptions/Pythia_Photos.py" )


    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch','thorsten.kuhl@cern.ch' ]
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
