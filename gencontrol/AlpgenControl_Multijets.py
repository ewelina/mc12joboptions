# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenMultijetGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
inputgridfile=None
nwarm = [ 4 , 1000000 ]

# Run numbers currently reserved: 168000 - 168100
# Njet samples obo M. Stoebe
UseJetFilter = False
if (run_num>=168000 and run_num<168100):
    inclusive=False
    log.info('Recognized Njet run number.  Will generate for run '+str(run_num))
    process = 'Njet'
    UseJetFilter = True
    UseHerwig = True
    jSlice = (run_num-168050)%10
    num_parton = int((run_num-168050)/10)+2
    pT_range = ['0-20','20-80','80-200', '200-500', '500-1000', '1000-1500', '1500-2000', '2000+']
    if (run_num-168000)<50: 
        UseHerwig=False # Move to Pythia for half the samples
        jSlice = (run_num-168000)%10
        num_parton = int((run_num-168000)/10)+2
    #special RunNumber for Np4 inclusive production
    # Np4+: JZ1=(168008,168058),JZ2=(168009,168059),JZ3=(168018,168058),JZ4=(168019,168058),JZ5=(168028,168058),JZ6=(168029,168058),JZ7=(168038,168058)    
    if run_num==168008 or run_num==168058:
        num_parton=4
        jSlice=1
        inclusive=True
    if run_num==168009 or run_num==168059:
        num_parton=4
        jSlice=2
        inclusive=True
    if run_num==168018 or run_num==168068:
        num_parton=4
        jSlice=3
        inclusive=True
    if run_num==168019 or run_num==168069:
        num_parton=4
        jSlice=4
        inclusive=True
    if run_num==168028 or run_num==168078:
        num_parton=4
        jSlice=5
        inclusive=True
    if run_num==168029 or run_num==168079:
        num_parton=4
        jSlice=6
        inclusive=True
    if run_num==168038 or run_num==168088:
        num_parton=4
        jSlice=7
        inclusive=True
    log.info('Will generate Njet process 2 -> '+str(num_parton)+'. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice])
    if num_parton > 6:
        log.info('Process for num_partons = '+str(num_parton)+' not defined. Will generate Njet process 2 -> 6+ instead. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice]+' GeV')
        num_parton = 6
    jets = [num_parton,1] # Exclusive matching to the last number of partons
    if jets[0] == 6: 
        jets[1]=0
    if inclusive:
        jets[1]=0
    warm_numbers=[500000,1000000,8000000,20000000,15000000]
    ncwarm=[3,3,4,5,5]
    nwarm = [ ncwarm[jets[0]-2] , warm_numbers[(jets[0]-2)] ]
    event_numbers=[10000000,400000000,8000000,20000000,15000000]
    ev_athena=[5000,5000,200,100,50]
    # Add a special case for number of Alpgen and Athena events in 7 TeV runs (Tuning was done only for J1-J3!!)
    if ecm == 3500.:
        event_numbers=[10000000,400000000,64000000,24000000,6000000]
        ev_athena=[5000,5000,2000,500,50]
    
    events_alpgen = event_numbers[(jets[0]-2)]
    #if num_parton==4 and jSlice==5:
    #    events_alpgen=events_alpgen*2
    #if jets[0] == 4 and jSlice == 5:
    #    events_athena=100
    events_athena=ev_athena[(jets[0]-2)]
    if num_parton>=4 and (jSlice==5 or jSlice ==6):
        events_alpgen=events_alpgen/1.6
        events_athena=events_athena/2
    if num_parton>=4 and jSlice==7:  
        events_athena=events_athena/2
    if num_parton==4 and jSlice==5:
        events_athena=events_athena/2
        events_alpgen=events_alpgen*2 #/1.6
        if inclusive:
            events_alpgen=events_alpgen*1.6*3
    if inclusive:
        events_alpgen=events_alpgen/1.6
    # Add a special case for number of Alpgen and Athena events in 7 TeV runs - now specifically for each NPx and JZx.
    if ecm == 3500.:
        # Number of events for NP4
        if num_parton==4 and jSlice==1:
            events_alpgen=events_alpgen*0.75
        if num_parton==4 and jSlice==2:
            events_alpgen=events_alpgen*1.5
            events_athena=events_athena*0.1
        if num_parton==4 and jSlice==3:
            events_alpgen=events_alpgen*2
            events_athena=events_athena*0.025
        if num_parton==4 and jSlice>=4:
            events_athena=events_athena*0.025
        # Number of events for NP5
        if num_parton==5 and jSlice==3:
            events_alpgen=events_alpgen*4
            events_athena=events_athena*0.1
        if num_parton==5 and jSlice>=4:
            events_athena=events_athena*0.1
        # Number of events for NP6
        if num_parton==6 and jSlice==1:
            events_alpgen=events_alpgen*1.2

    et_range_max = [100,120,230,550,1050,1600,2100,10000]
    et_range_min = [0,10,60,180,450,950,1450,1950]

    #events_alpgen=1
    #events_athena=1   

    special1 = """ptj1max %d
    ptj1min %d
    """%(et_range_max[jSlice], et_range_min[jSlice])
    special2 = special1

# Run numbers currently reserved: 168100 - 168200
# bb-bar samples obo M. Stoebe
if run_num>=168100 and run_num<168200:
    log.info('Recognized bb-bar run number.  Will generate for run '+str(run_num))
    process = '2Q'
    UseJetFilter = True
    UseHerwig = True
    jSlice = (run_num-168150)%10
    num_parton = int((run_num-168150)/10)
    pT_range = ['0-20','20-80','80-200', '200-500', '500-1000', '1000-1500', '1500-2000', '2000+']
    if (run_num-168100)<50: 
        UseHerwig=False # Move to Pythia for half the samples
        jSlice = (run_num-168100)%10
        num_parton = int((run_num-168100)/10)
    log.info('Will generate 2Q process 2 -> bb-bar + '+str(num_parton)+'. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice])
    if num_parton > 4:
        log.info('Process for num_partons = bb-bar + '+str(num_parton)+' not defined. Will generate Njet process 2 -> bb-bar + 4+ jets instead. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice]+' GeV')
        num_parton = 4
    jets = [num_parton,1] # Exclusive matching to the last number of partons
    if jets[0] == 4: 
        jets[1]=0
    warm_numbers=[500000,1000000,3000000,5000000,5000000]
    ncwarm=[3,3,4,4,4]
    nwarm = [ ncwarm[jets[0]] , warm_numbers[(jets[0])] ]
    event_numbers=[5000000,30000000,30000000,40000000,40000000]
    events_alpgen = event_numbers[(jets[0])]
 
    ev_athena=[5000,1000,500,100,50]
    events_athena=ev_athena[(jets[0])] 
 
    et_range_max = [100,120,230,550,1050,1600,2100,10000]
    et_range_min = [0,10,60,180,450,950,1450,1950]
 
    special1 = """ptj1max %d
    ptj1min %d
    ihvy 5
    """%(et_range_max[jSlice], et_range_min[jSlice])
    special2 = special1

# Run numbers currently reserved: 168200 - 168999
# cc-bar samples obo M. Stoebe
if run_num>=168201 and run_num<168300:
    log.info('Recognized cc-bar run number.  Will generate for run '+str(run_num))
    process = '2Q'
    UseJetFilter = True
    UseHerwig = True
    jSlice = (run_num-168250)%10
    num_parton = int((run_num-168250)/10)
    pT_range = ['0-20','20-80','80-200', '200-500', '500-1000', '1000-1500', '1500-2000', '2000+']
    if (run_num-168200)<50: 
        UseHerwig=False # Move to Pythia for half the samples
        jSlice = (run_num-168200)%10
        num_parton = int((run_num-168200)/10)
    log.info('Will generate 2Q process 2 -> cc-bar + '+str(num_parton)+' jet. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice])
    if num_parton > 4:
        log.info('Process for num_partons = cc-bar + '+str(num_parton)+' not defined. Will generate Njet process 2 -> cc-bar + 4+ jets instead. For pT range JZ'+str(jSlice)+' = '+pT_range[jSlice]+' GeV')
        num_parton = 4
    jets = [num_parton,1] # Exclusive matching to the last number of partons
    if jets[0] == 4: 
        jets[1]=0
    warm_numbers=[500000,1000000,5000000,5000000,5000000]
    ncwarm=[3,3,4,4,4]
    nwarm = [ ncwarm[jets[0]] , warm_numbers[(jets[0])] ]
    event_numbers=[5000000,30000000,50000000,40000000,40000000]
    events_alpgen = event_numbers[(jets[0])]
 
    ev_athena=[5000,1000,500,100,50]
    events_athena=ev_athena[(jets[0])] 
 
    et_range_max = [100,120,230,550,1050,1600,2100,10000]
    et_range_min = [0,10,60,180,450,950,1450,1950]
 
    special1 = """ptj1max %d
    ptj1min %d
    ihvy 4
    """%(et_range_max[jSlice], et_range_min[jSlice])
    special2 = special1


# Run numbers for DPS in four jets request: 185612 - 185618
# Contact - Orel Gueta
if (run_num>=185612 and run_num<=185618):
    log.info('Recognized Njet run number.  Will generate for run '+str(run_num))
    process = 'Njet'
    UseJetFilter = True
    UseHerwig = True
    if run_num==185612:
        num_parton=2
        jSlice=1
    if run_num==185613:
        num_parton=2
        jSlice=2
    if run_num==185614:
        num_parton=3
        jSlice=1
    if run_num==185615:
        num_parton=3
        jSlice=2
    if run_num==185616:
        num_parton=4
        jSlice=2
    if run_num==185617:
        num_parton=5
        jSlice=2
    if run_num==185618:
        num_parton=6
        jSlice=2

    pT_range = ['0-20','20-80','80+'] # The first slice (0-20) is never used, 80+ means minimum 80, no maximum.
    if jSlice==2:
        jzxText = 'JZ2x'
    else:
        jzxText = 'JZ'+str(jSlice)

    log.info('Will generate Njet process 2 -> '+str(num_parton)+'. For pT range '+jzxText+' = '+pT_range[jSlice])
    if num_parton > 6:
        log.info('Process for num_partons = '+str(num_parton)+' not defined. Will generate Njet process 2 -> 6+ instead. For pT range '+jzxText+' = '+pT_range[jSlice]+' GeV')
        num_parton = 6
    jets = [num_parton,1] # Exclusive matching to the last number of partons
    if jets[0] == 6: 
        jets[1]=0
    warm_numbers=[500000,1000000,8000000,20000000,15000000]
    ncwarm=[3,3,4,5,5]
    nwarm = [ ncwarm[jets[0]-2] , warm_numbers[(jets[0]-2)] ]
    event_numbers=[50000000,400000000,8000000,20000000,15000000]
    ev_athena=[5000,5000,200,100,50]
    # Add a special case for number of Alpgen and Athena events in 7 TeV runs 
    if ecm == 3500.:
        event_numbers=[50000000,600000000,150000000,75000000,20000000]
        ev_athena=[5000,2000,200,500,50]
    
    events_alpgen = event_numbers[(jets[0]-2)]
    events_athena=ev_athena[(jets[0]-2)]
    # Add a special case for number of Alpgen and Athena events in 7 TeV runs - now specifically for each NPx and JZx.
    if ecm == 3500.:
        # Number of events for NP4
        if num_parton==4 and jSlice==2:
            events_alpgen=events_alpgen*1.5
            events_athena=events_athena*0.1

    et_range_max = [100,120,14000]
    et_range_min = [0,10,60]

    special1 = """ptj1max %d
    ptj1min %d
    """%(et_range_max[jSlice], et_range_min[jSlice])
    special2 = special1

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

if hasattr(runArgs,'inputGenConfFile'):
    log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
    inputgridfile=runArgs.inputGenConfFile
    
SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2, special3=special3, inputgridfile=inputgridfile )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

if UseJetFilter:
  filterString = 'MC12JobOptions/JetFilter_JZ'+str(jSlice)+'.py'
  include(filterString)

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_mutlijets'
evgenConfig.contact = ['zach.marshall@cern.ch','Michael.Stoebe@cern.ch']
evgenConfig.process = 'alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.keywords = ["SM","Multijet","QCD","jets"]
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
