# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
jets = 0

############################################################################

# Zcc by Marc Hohlfeld
FileCheck = '.*'

if run_num>=166990 and run_num<=166993:
    
    log.info('Recognized Zcc+jets Filtered run number.  Will generate for run '+str(run_num))
    process = 'zqq'

    jets = [(run_num-166990)%4,1] # Exclusive matching to the last number of partons
    if jets[0]>2: jets[1]=0
    
    event_numbers_warm=[500000, 500000, 3000000, 5000000]
    event_numbers=[4000000, 12000000, 50000000, 20000000]

    
    ncwarm = [3,3,3,4]
    nwarm = [ ncwarm[jets[0]] , event_numbers_warm[jets[0]] ]
    events_alpgen = event_numbers[jets[0]]
    if jets[0] == 0:
        events_athena = 5000
    elif jets[0] == 1:
        events_athena = 5000
    elif jets[0] == 2:
        events_athena = 2000
    elif jets[0] == 3:
        events_athena = 100
                            
                
    # Select Z->nunu decays
    Z_mode=4

    special1 = """mllmin  30.0         ! Minimum M_ll
mllmax  2000         ! Maximum M_ll
ihvy    4            ! Select Z+cc
mc      1.5            ! quark c mass
ptbmin  0.0001            ! quark b pt min
ptcmin  0.0001            ! quark c pt min
drjmin  0.4          ! deltar jets
drcmin  0.0001            ! deltar c
"""
    
    
    special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) 
izdecmode %i          ! Z decay mode (4: nunu)\n """%(Z_mode)

    special2 = special1

    special3 = "502    0.4        ! min RCLUS value for parton-jet matching\n"

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 


if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
    log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
    inputgridfile=runArgs.inputGenConfFile


from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2, special3=special3, inputgridfile=inputgridfile )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')


# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.keywords = ['SUSY', 'Znunu+ccbar']
evgenConfig.contact = ['mark.hohlfeld@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.inputconfcheck = FileCheck
evgenConfig.minevents = events_athena
