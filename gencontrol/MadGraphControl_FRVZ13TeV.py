from MadGraphControl.MadGraphUtils import *

# MC12.203979.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zdDisplacedmH125.py
# MC12.203980.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zdDisplacedmH125.py
# MC12.203981.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zdDisplacedmH600.py
# MC12.203982.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zdDisplacedmH600.py
# MC12.203983.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zdPromptmH125.py
# MC12.203984.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zdPromptmH125.py
# MC12.203985.MadGraphPythia8_AU2CTEQ6L1_FRVZ2zdPromptmH600.py
# MC12.203986.MadGraphPythia8_AU2CTEQ6L1_FRVZ4zdPromptmH600.py

# Variables that depend on run number: which process and which Higgs mass
process = { 
    2:  'generate g g > h > nd2 nd2, (nd2 > nd1 zd, zd > f- f+), (nd2 > nd1 zd, zd > f- f+)',
    4:  'generate g g > h > nd2 nd2, (nd2 > nd1 hd1, (hd1 > zd zd, zd > f- f+)), (nd2 > nd1 hd1, (hd1 > zd zd, zd > f- f+))' 
    }
modelcode='usrmodv4_lj_Run2'

if runArgs.runNumber==203979:
    mH=125
    nGamma=2
    LJ='Displaced'
    avgtau = 39.81
elif runArgs.runNumber==203980:
    mH=125
    nGamma=4
    LJ='Displaced'
    avgtau = 71.8
elif runArgs.runNumber==203981:
    mH=600
    nGamma=2
    LJ='Displaced'
    avgtau = 15.0
elif runArgs.runNumber==203982:
    mH=600
    nGamma=4
    LJ='Displaced'
    avgtau = 25.4
elif runArgs.runNumber==203983:
    mH=125
    nGamma=2
    LJ='Prompt'
elif runArgs.runNumber==203984:
    mH=125
    nGamma=4
    LJ='Prompt'
elif runArgs.runNumber==203985:
    mH=600
    nGamma=2
    LJ='Prompt'
elif runArgs.runNumber==203986:
    mH=600
    nGamma=4
    LJ='Prompt'
else:
    raise RuntimeError('Bad runNumber')

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 7500.

rcard='MadGraph_LJ_FRVZ_runcard_13TeV.dat'
pcard='MadGraph_%sLJ_FRVZ_paramcard_mH%s.dat' % (nGamma, mH)

# initialise random number generator/sequence
import random
random.seed(runArgs.randomSeed)
# lifetime function
def lifetime(avgtau = 21):
    import math
    t = random.random()
    return -1.0 * avgtau * math.log(t)

# basename for madgraph LHEF file
rname = 'run_lj'+str(runArgs.runNumber)

# do not run MadGraph if config only is requested
if not opts.config_only:

    # writing proc card for MG
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
import model_v4 %s
define p = g u c d s u~ c~ d~ s~
define f- = me- mmu- pi-
define f+ = me+ mmu+ pi+
%s
output -f
""" % (modelcode, process[nGamma]))
    fcard.close()


    # getting run cards
    from PyJobTransformsCore.trfutil import get_files
    get_files( rcard, keepDir=False, errorIfNotFound=True )
    get_files( pcard, keepDir=False, errorIfNotFound=True )

    # generating events in MG
    process_dir = new_process()

    generate(run_card_loc=rcard,param_card_loc=pcard,mode=0,njobs=1,run_name=rname,proc_dir=process_dir)

    # replacing lifetime of dark photon, manually
    unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz'])
    unzip1.wait()

    oldlhe = open(process_dir+'/Events/'+rname+'/unweighted_events.lhe','r')
    newlhe = open(process_dir+'/Events/'+rname+'/unweighted_events2.lhe','w')
    init = True
    for line in oldlhe:
        if init==True:
            newlhe.write(line)
            if '</init>' in line:
                init = False
        else:
            if '3000001' in line:
                if LJ=='Displaced':
                   part1 = line[:-7]
                   part2 = "%.11E" % (lifetime(avgtau))
                   part3 = line[-5:]
                   newlhe.write(part1+part2+part3)
                else:
                   newlhe.write(line)
            elif '-3000011' in line:
                part1 = '      -11'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '3000011' in line:
                part1 = '       11'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '-3000013' in line:
                part1 = '      -13'
                part2 = line[10:]
                newlhe.write(part1+part2)
            elif '3000013' in line:
                part1 = '       13'
                part2 = line[10:]
                newlhe.write(part1+part2)
            else:
                newlhe.write(line)

    oldlhe.close()
    newlhe.close()

    zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+rname+'/unweighted_events2.lhe'])
    zip1.wait()
    shutil.move(process_dir+'/Events/'+rname+'/unweighted_events2.lhe.gz',process_dir+'/Events/'+rname+'/unweighted_events.lhe.gz')
    os.remove(process_dir+'/Events/'+rname+'/unweighted_events.lhe')

    arrange_output(run_name=rname,proc_dir=process_dir,outputDS=rname+'._00001.events.tar.gz',skip_events=False)

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
include("MC12JobOptions/Pythia8_MadGraph.py")
topAlg.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

from TruthExamples.TruthExamplesConf import TestHepMC
topAlg += TestHepMC()
topAlg.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
topAlg.TestHepMC.MaxVtxDisp = 100000000 #in mm
#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph"]
evgenConfig.description = 'FRVZ process Higgs -> %sgamma_d + X with mH=%sGeV, %s LJs' % (nGamma,mH,LJ)
evgenConfig.keywords = ["exotics", "nonSMhiggs"]
evgenConfig.inputfilecheck = rname
runArgs.inputGeneratorFile=rname+'._00001.events.tar.gz'

