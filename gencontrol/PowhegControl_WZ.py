from PowhegControl.PowhegUtils import PowhegConfig_base

###############################################################################
#
#  WZ
#
###############################################################################

class AtlasPowhegConfig_WZ(PowhegConfig_base) :
  # These are user configurable - put generic properties in PowhegConfig_base

  diagCKM = None

  # anomalous couplings
  delg1_z = None
  delg1_g = None
  lambda_z = None
  lambda_g = None
  delk_g = None
  delk_z = None
  tevscale = None

  # other parameters
  mllmin = 10
  zerowidth = 0
  withinterference = 1
  dronly = 0

  bornonly = 0
  wllmin = None

  # integration parameters
  ncall1  = 1500000
  itmx1   = 4
  ncall2  = 1000000
  itmx2   = 5
  nubound = 1000000
  foldcsi = 1
  foldy   = 1
  foldphi = 1
  
  # Set process-dependent paths in the constructor
  def __init__(self,runArgs=None) :
    PowhegConfig_base.__init__(self,runArgs)
    self._powheg_executable += '/WZ/pwhg_main'

  def generateRunCard(self) :
    self.generateRunCardSharedOptions()

    with open( str(self.TestArea)+'/powheg.input','a') as f :
      f.write( 'vdecaymodeW '+str(self.vdecaymodeW)+'           ! PDG code for charged decay product of the W boson (11:e-;-11:e+;...)\n' )
      f.write( 'vdecaymodeZ '+str(self.vdecaymodeZ)+'           ! PDG code for lepton Z boson (11:e-; 12: ve; ...)\n' )

      if self.diagCKM is not None:
        f.write( '                                                ! CKM matrix\n' )
        f.write( 'diagCKM '+str(self.diagCKM)+'                   ! if 1 (true) then diagonal CKM (default 0)\n' )

      if self.mllmin:
        f.write( 'mllmin '+str(self.mllmin)+'                     ! default 0.1 GeV this is minimum invar mass for Z leptons\n' )
      if self.zerowidth:
        f.write( 'zerowidth '+str(self.zerowidth)+'               ! if 1 (true) use zero width approximatrion (default 0)\n' )
      if self.withinterference:
        f.write( 'withinterference '+str(self.withinterference)+' ! if 1 (true) include interference for like flavour charged leptons\n' )
      if self.dronly:
        f.write( 'dronly '+str(self.dronly)+'                     ! if 1 (true) only include double resonant diagrams (default 0)\n' )
      if self.bornonly:
        f.write( 'bornonly   '+str(self.bornonly)+'               ! (default 0) if 1 do Born only\n' )
      if self.wllmin:
        f.write( 'wllmin  '+str(self.wllmin)+'                    ! default 1d-5 GeV this is min invar mass for W leptons\n' )

      f.write( '                                                ! anom couplings\n' )
      if self.delg1_z:
          f.write( 'delg1_z    '+str(self.delg1_z)+'                ! Delta_g1(Z)\n' )
      if self.delg1_g:
          f.write( 'delg1_g    '+str(self.delg1_g)+'                ! Delta_g1(Gamma)\n' )
      if self.lambda_z:
          f.write( 'lambda_z   '+str(self.lambda_z)+'               ! Lambda(Z)\n' )
      if self.lambda_g:
          f.write( 'lambda_g   '+str(self.lambda_g)+'               ! Lambda(gamma)\n' )
      if self.delk_z:
          f.write( 'delk_z     '+str(self.delk_z)+'                 ! Delta_K(Z)\n' )
      if self.delg1_g:
          f.write( 'delk_g     '+str(self.delk_g)+'                 ! Delta_K(Gamma)\n' )
      if self.tevscale:
        f.write( 'tevscale   '+str(self.tevscale)+'               ! Form-factor scale, in TeV\n' )
