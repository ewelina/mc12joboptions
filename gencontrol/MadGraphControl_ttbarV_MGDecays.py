from MadGraphControl.MadGraphUtils import *

### Default parameters
removeMomentumBalanceWarning=False
minevents=5000
nevents=40000

gridpackMode=False
nJobs=1
cluster_type=None
cluster_queue=None

mllcut=-999.9
dRllcut=0.4
dRjlcut=0.4
pTlcut=-999.9
etalcut=2.5

maxjetflavor=4

#lhapdfid=10042
lhapdfid=-1
    
ickkw=1
xqcut = 20
qcut = 30

cut_decays='T'

doShower=True


applyLeptonFilter=True
filt_Nlep=1
filt_lepPt=7000.
filt_lepEta=3.0



### DSIDs ###
ttbarW_Np0Excl         = [117487]
ttbarW_Np1Incl         = [117488]

ttbarZ_Np0Excl         = [117489]
ttbarZ_Zll_Np1Incl     = [117490]
ttbarZ_Znunuqq_Np1Incl = [117491]



ttbarW_Np0Excl_3lep    = [185878]
ttbarW_Np1Incl_3lep    = [185879]






fcard = open('proc_card_mg5.dat','w')

### ttW
if runArgs.runNumber in ttbarW_Np0Excl:
    fcard.write("""
    import model sm  
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b
    define q~ = u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = w+ w-
    define l = l+ l-
    define vlall = vl vl~
    define all = q q~ l vlall
    generate p p > t t~ v, t > b all all, t~ > b~ all all, v > all all
    output -f
    """)
    fcard.close()
    iexcfile=1
    ickkw=1
    name="ttbarW_Np0Excl_MGDecays_1lepfilt"
    cut_decays='F'
    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='short'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0


elif runArgs.runNumber in ttbarW_Np1Incl:
    fcard.write("""
    import model sm  
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b
    define q~ = u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = w+ w-
    define l = l+ l-
    define vlall = vl vl~
    define all = q q~ l vlall
    generate p p > t t~ v j, t > b all all, t~ > b~ all all, v > all all
    output -f
    """)
    fcard.close()
    iexcfile=0
    ickkw=1
    name="ttbarW_Np1Incl_MGDecays_1lepfilt"
    cut_decays='F'
    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='short'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0

    

    
### ttZ
elif runArgs.runNumber in ttbarZ_Np0Excl:
    fcard.write("""
    import model sm-no_masses
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b
    define q~ = u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = w+ w-
    define l = l+ l-
    define vlall = vl vl~
    define all_nolep = q q~ vlall
    define all = q q~ l vlall
    generate p p > t t~ z, t > b all all, t~ > b~ all all, z > all_nolep all_nolep @1
    add process p p > t t~ l+ l- $ h, t > b all all, t~ > b~ all all @2
    output -f
    """)
    fcard.close()
    iexcfile=1
    ickkw=1
    name="ttbarZ_Np0Excl_MGDecay_1lepfilt"
    cut_decays='F'
    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='short'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0
    removeMomentumBalanceWarning=True


elif runArgs.runNumber in ttbarZ_Zll_Np1Incl:
    fcard.write("""
    import model sm-no_masses
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b
    define q~ = u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = w+ w-
    define l = l+ l-
    define vlall = vl vl~
    define all_nolep = q q~ vlall
    define all = q q~ l vlall
    generate p p > t t~ l+ l- j $ h, t > b all all, t~ > b~ all all @1
    output -f
    """)
    fcard.close()   
    iexcfile=0
    ickkw=1
    name="ttbarZ_Zll_Np1Incl_MGDecays_1lepfilt"
    cut_decays='F'
    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='short'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0
    minevents=2000
    nevents=10000
    removeMomentumBalanceWarning=True

elif runArgs.runNumber in ttbarZ_Znunuqq_Np1Incl:
    fcard.write("""
    import model sm-no_masses
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define q = u c d s b
    define q~ = u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define v = w+ w-
    define l = l+ l-
    define vlall = vl vl~
    define all_nolep = q q~ vlall
    define all = q q~ l vlall
    generate p p > t t~ z j $ h, t > b all all, t~ > b~ all all, z > all_nolep all_nolep @1
    output -f
    """)
    fcard.close()   
    iexcfile=0
    ickkw=1
    name="ttbarZ_Znunuqq_Np1Incl_MGDecays_1lepfilt"
    cut_decays='F'
    gridpackMode=True
    nJobs=20
    cluster_type='pbs'
    cluster_queue='short'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0
    minevents=2000
    nevents=10000    
    removeMomentumBalanceWarning=True

### ttW 3lep filter
elif runArgs.runNumber in ttbarW_Np0Excl_3lep:
    fcard.write("""
    import model sm
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define lall = l- l+
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define vlall = vl vl~
    define v = w+ w-
    generate p p > t t~ v, v > lall vlall, t > b l+ vl, t~ > b~ l- vl~
    output -f
    """)
    fcard.close()   
    iexcfile=1
    ickkw=1
    name="ttbarW_Np0ex_MGDec_3lep"
    cut_decays='F'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    dRjjcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0
    
    filt_Nlep=3
    filt_lepPt=7000.
    filt_lepEta=2.7



elif runArgs.runNumber in ttbarW_Np1Incl_3lep:
    fcard.write("""
    import model sm
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define lall = l- l+
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define vlall = vl vl~
    define v = w+ w-
    generate p p > t t~ v j, v > lall vlall, t > b l+ vl, t~ > b~ l- vl~
    output -f
    """)
    fcard.close()   
    iexcfile=0
    ickkw=1
    name="ttbarW_Np1in_MGDec_3lep"
    cut_decays='F'
    maxjetflavor=5
    mllcut=5.0
    dRllcut=0.0
    dRjjcut=0.0
    pTlcut=1.0
    etalcut=5.0
    dRjlcut=0.0
    
    filt_Nlep=3
    filt_lepPt=7000.
    filt_lepEta=2.7



beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RunTimeError("No center of mass energy found.")




# Grab the run card and move it into place
#runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
#runcard.wait()
if os.access(os.environ['MADPATH']+'/Template/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/run_card.dat','run_card.SM.dat')
elif os.access(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat','run_card.SM.dat')
else:
    raise RunTimeError("Cannot find Template run_card.dat!")
    


if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw' in line:
            newcard.write('%i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ickkw==1 and ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ickkw==1 and ' xptj ' in line:
            newcard.write('   %f      = xptj ! minimum pt for at least one jet \n'%(xqcut))
        elif ickkw==1 and ' = maxjetflavor' in line:
            newcard.write(' %i = maxjetflavor \n'%(maxjetflavor))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = cut_decays' in line:
            newcard.write(' %s = cut_decays \n'%(cut_decays))
        elif ' = mmll ' in line and mllcut>0:
            newcard.write(' %f = mmll    ! min invariant mass of l+l- (same flavour) lepton pair \n'%(mllcut))
        elif ' = drll ' in line and dRllcut!=0.4:
            newcard.write(' %f = drll    ! min distance between leptons \n'%(dRllcut))
        elif ' = ptl ' in line and pTlcut>0:
            newcard.write(' %f = ptl       ! minimum pt for the charged leptons \n'%(pTlcut))
        elif ' = etal ' in line and etalcut!=2.5:
            newcard.write(' %f = etal    ! max rap for the charged leptons \n'%(etalcut))
        elif ' = drjl ' in line and dRjlcut!=0.4:
            newcard.write(' %f = drjl    ! min distance between jet and lepton \n'%(dRjlcut))
        elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()



    
stringy=''
   

procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
procCard.wait()

runCard = subprocess.Popen(['cat','run_card.dat'])
runCard.wait()


runName='Test'     




if not hasattr(runArgs, "inputGenConfFile"):
    ## Generate process ##
    if gridpackMode:
        print 'Generating process for gridpack'
    else:
        print 'Generating process and events'

    process_dir = new_process()
    generate(run_card_loc='run_card.dat',param_card_loc=None,mode=1,njobs=nJobs,proc_dir=process_dir,run_name=runName,grid_pack=gridpackMode,cluster_type=cluster_type,cluster_queue=cluster_queue)
    
else:
    seed=runArgs.randomSeed

    if gridpackMode and hasattr(runArgs, "inputGenConfFile"):
        ## Generate events from gridpack ##
        print 'Generating events using gripack mode'
        
        gridpack_dir='madevent/'
        generate_from_gridpack(run_name=runName,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=seed)
        process_dir=gridpack_dir
        


if not gridpackMode or (gridpackMode and hasattr(runArgs, "inputGenConfFile")):


        
    stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
    
    skip_events=0
    if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
    arrange_output(run_name=runName,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)
    
    if doShower:

            
        ## Pass to Pythia ##
        print 'Pass generated events to Pythia'
            
        #--------------------------------------------------------------
        # General MC12 configuration
        #--------------------------------------------------------------
        include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
        topAlg.Pythia.PythiaCommand += [ "pyinit user madgraph",
                                         "pypars mstp 143 1",
                                         "pystat 1 3 4 5",
                                         "pyinit dumpr 1 5",
                                         "pyinit pylistf 1"
                                         ]
        
        
        include ( "MC12JobOptions/Pythia_Tauola.py" )
        include ( "MC12JobOptions/Pythia_Photos.py" )
        
        
        evgenConfig.generators += ["MadGraph", "Pythia"]
        evgenConfig.description = 'MadGraph_'+str(name) #Move to DSID jobOptions
        evgenConfig.keywords = ["SM","ttbarV"]
        evgenConfig.contact  = ["mcfayden@cern.ch"]         
        evgenConfig.inputfilecheck = stringy
        #evgenConfig.minevents = minevents #Move to DSID jobOptions
        runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
        
    
        
        # Additional bit for ME/PS matching
        if ickkw==1:
            if iexcfile>=0:
                #Generating separate jet multiplicity samples
                phojf=open('./pythia_card.dat', 'w')
                phojinp = """
                !...Matching parameters...
                IEXCFILE=%i
                showerkt=T
                qcut=%i
                imss(21)=24
                imss(22)=24
                """%(iexcfile,qcut)
            else:
                #Not separating jet multiplicity samples
                phojf=open('./pythia_card.dat', 'w')
                phojinp = """
                !...Matching parameters...
                showerkt=T
                qcut=%i
                imss(21)=24
                imss(22)=24
                """%(qcut)
        
            phojf.write(phojinp)
            phojf.close()
    
    
            

        if applyLeptonFilter:
            # ... Filter
            from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
            topAlg += MultiLeptonFilter("MultiLeptonFilter")
            MultiLeptonFilter = topAlg.MultiLeptonFilter
            MultiLeptonFilter.Ptcut = filt_lepPt
            MultiLeptonFilter.Etacut = filt_lepEta
            MultiLeptonFilter.NLeptons = filt_Nlep
            StreamEVGEN.RequireAlgs  = [ "MultiLeptonFilter" ]


        if removeMomentumBalanceWarning:
            from TruthExamples.TruthExamplesConf import TestHepMC
            topAlg += TestHepMC()
            topAlg.TestHepMC.EnergyDifference = 10000.
            
