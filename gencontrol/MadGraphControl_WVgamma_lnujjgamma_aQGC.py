from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
rcard='run_card.wva.dat'

if not hasattr(runArgs,'runNumber'):
  raise RunTimeError("No run number found.")

#--------------------------------------------------------------
# MG5 Proc card
# WWy: use DSID 185982-185991
# WZy: use DSID 207106-207115
#--------------------------------------------------------------
if ((runArgs.runNumber==185982) or (runArgs.runNumber==185983) or (runArgs.runNumber==185984) or (runArgs.runNumber==185985)):
    fcard.write("""
    import model SM_LM0123_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ w+ a NP=1, w+ > j j @1
    add process p p > e- ve~ w+ a j NP=1, w+ > j j @2
    add process p p > e+ ve w- a NP=1, w- > j j @3
    add process p p > e+ ve w- a j NP=1, w- > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    if (runArgs.runNumber==185982):
        name="WWgamma_elnujjgamma_aqgc_LM0"
    elif (runArgs.runNumber==185983):
        name="WWgamma_elnujjgamma_aqgc_LM1"
    elif (runArgs.runNumber==185984):
        name="WWgamma_elnujjgamma_aqgc_LM2"
    elif (runArgs.runNumber==185985):
        name="WWgamma_elnujjgamma_aqgc_LM3"

elif (runArgs.runNumber==185986):
    fcard.write("""
    import model SM_LT012_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ w+ a NP=1, w+ > j j @1
    add process p p > e- ve~ w+ a j NP=1, w+ > j j @2
    add process p p > e+ ve w- a NP=1, w- > j j @3
    add process p p > e+ ve w- a j NP=1, w- > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_elnujjgamma_aqgc_LT0"        
        

elif ((runArgs.runNumber==185987) or (runArgs.runNumber==185988) or (runArgs.runNumber==185989) or (runArgs.runNumber==185990)):
    fcard.write("""
    import model SM_LM0123_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ w+ a NP=1, w+ > j j @1
    add process p p > mu- vm~ w+ a j NP=1, w+ > j j @2
    add process p p > mu+ vm w- a NP=1, w- > j j @3
    add process p p > mu+ vm w- a j NP=1, w- > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    if (runArgs.runNumber==185987):
        name="WWgamma_munujjgamma_aqgc_LM0"
    elif (runArgs.runNumber==185988):
        name="WWgamma_munujjgamma_aqgc_LM1"
    elif (runArgs.runNumber==185989):
        name="WWgamma_munujjgamma_aqgc_LM2"
    elif (runArgs.runNumber==185990):
        name="WWgamma_munujjgamma_aqgc_LM3"
        
        
elif (runArgs.runNumber==185991):
    fcard.write("""
    import model SM_LT012_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ w+ a NP=1, w+ > j j @1
    add process p p > mu- vm~ w+ a j NP=1, w+ > j j @2
    add process p p > mu+ vm w- a NP=1, w- > j j @3
    add process p p > mu+ vm w- a j NP=1, w- > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_munujjgamma_aqgc_LT0"

elif ((runArgs.runNumber==207106) or (runArgs.runNumber==207107) or (runArgs.runNumber==207108) or (runArgs.runNumber==207109)):
    fcard.write("""
    import model SM_LM0123_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ z a NP=1, z > j j @1
    add process p p > e- ve~ z a j NP=1, z > j j @2
    add process p p > e+ ve z a NP=1, z > j j @3
    add process p p > e+ ve z a j NP=1, z > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    if (runArgs.runNumber==207106):
        name="WZgamma_elnujjgamma_aqgc_LM0"
    elif (runArgs.runNumber==207107):
        name="WZgamma_elnujjgamma_aqgc_LM1"
    elif (runArgs.runNumber==207108):
        name="WZgamma_elnujjgamma_aqgc_LM2"
    elif (runArgs.runNumber==207109):
        name="WZgamma_elnujjgamma_aqgc_LM3"
        
elif (runArgs.runNumber==207110):
    fcard.write("""
    import model SM_LT012_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ z a NP=1, z > j j @1
    add process p p > e- ve~ z a j NP=1, z > j j @2
    add process p p > e+ ve z a NP=1, z > j j @3
    add process p p > e+ ve z a j NP=1, z > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WZgamma_elnujjgamma_aqgc_LT0"
    
elif ((runArgs.runNumber==207111) or (runArgs.runNumber==207112) or (runArgs.runNumber==207113) or (runArgs.runNumber==207114)):
    fcard.write("""
    import model SM_LM0123_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ z a NP=1, z > j j @1
    add process p p > mu- vm~ z a j NP=1, z > j j @2
    add process p p > mu+ vm z a NP=1, z > j j @3
    add process p p > mu+ vm z a j NP=1, z > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    if (runArgs.runNumber==207111):
        name="WZgamma_munujjgamma_aqgc_LM0"
    elif (runArgs.runNumber==207112):
        name="WZgamma_munujjgamma_aqgc_LM1"
    elif (runArgs.runNumber==207113):
        name="WZgamma_munujjgamma_aqgc_LM2"
    elif (runArgs.runNumber==207114):
        name="WZgamma_munujjgamma_aqgc_LM3"
        
elif (runArgs.runNumber==207115):
    fcard.write("""
    import model SM_LT012_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ z a NP=1, z > j j @1
    add process p p > mu- vm~ z a j NP=1, z > j j @2
    add process p p > mu+ vm z a NP=1, z > j j @3
    add process p p > mu+ vm z a j NP=1, z > j j @4
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WZgamma_munujjgamma_aqgc_LT0"
    
else:
    raise RunTimeError("No data set ID found")

#pcard='MadGraph_paramcard_%s.dat' %name

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy=runArgs.ecmEnergy/2.0

else:
    raise RunTimeError("No center of mass energy found.")

# getting run cards
from PyJobTransformsCore.trfutil import get_files
get_files( rcard, keepDir=False, errorIfNotFound=True )

# Grab the parameter and the run card and move it into place
paramcard = subprocess.Popen(['get_files','-data','MadGraph_WVA_aqgc_param_card.dat'])
paramcard.wait()

if not os.access('MadGraph_WVA_aqgc_param_card.dat',os.R_OK):
    raise RunTimeError('Could not get param card: MadGraph_WVA_aqgc_param_card.dat')
elif os.access('param_card.dat',os.R_OK):
    raise RunTimeError('Old param card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('MadGraph_WVA_aqgc_param_card.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ((runArgs.runNumber==185982 ) or (runArgs.runNumber==185987) or (runArgs.runNumber==207106) or (runArgs.runNumber==207111)):
            if '# FM0' in line:
                newcard.write('    3 80.0e-11 # FM0 \n')
            else:
                newcard.write(line)
        if ((runArgs.runNumber==185983) or (runArgs.runNumber==185988) or (runArgs.runNumber==207107) or (runArgs.runNumber==207112)):
            if '# FM1' in line:
                newcard.write('    4 130.0e-11 # FM1 \n')
            else:
                newcard.write(line)
        if ((runArgs.runNumber==185984) or (runArgs.runNumber==185989) or (runArgs.runNumber==207108) or (runArgs.runNumber==207113)):
            if '# FM2' in line:
                newcard.write('    5 40.0e-11 # FM2 \n')
            else:
                newcard.write(line)        
        if ((runArgs.runNumber==185985) or (runArgs.runNumber==185990) or (runArgs.runNumber==207109) or (runArgs.runNumber==207114)):
            if '# FM3' in line:
                newcard.write('    6 65.0e-11 # FM3 \n')
            else:
                newcard.write(line)
        if ((runArgs.runNumber==185986) or (runArgs.runNumber==185991) or (runArgs.runNumber==207110) or (runArgs.runNumber==207115)):
            if '# FT0' in line:
                newcard.write('   11 25.0e-11 # FT0 \n')
            else:
                newcard.write(line)              
    oldcard.close()
    newcard.close()

print "Modified parameter.dat:"
pcard = open('param_card.dat','r')
print pcard.read()
pcard.close()


process_dir = new_process()

generate(run_card_loc=rcard,param_card_loc='param_card.dat',mode=0,njobs=1,run_name='WVAaQGC',proc_dir=process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name = 'WVAaQGC', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)


#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_Perugia2012_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = "MadGraph+Pythia6 production for " + str(name) + "with up to 1 additional hard jets (ME+PS) with the Perugia2012 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "electroweak", "SM"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
       IEXCFILE=0
       QCUT=25
"""
phojf.write(phojinp) 
phojf.close()
