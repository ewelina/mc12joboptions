from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
rcard='run_card.wva.dat'
	
if not hasattr(runArgs,'runNumber'):
  raise RunTimeError("No run number found.")

#--------------------------------------------------------------
# MG5 Proc card
#--------------------------------------------------------------
if (runArgs.runNumber==185965):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ ve w- a, w- > j j @1
    add process p p > e+ ve w- a j, w- > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpWmgamma_epnujjgamma"

elif (runArgs.runNumber==185966):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ w+ a, w+ > j j @1
    add process p p > e- ve~ w+ a j, w+ > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmWpgamma_emnujjgamma"

elif (runArgs.runNumber==185967):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ vm w- a, w- > j j @1
    add process p p > mu+ vm w- a j, w- > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpWmgamma_mupnujjgamma"

elif (runArgs.runNumber==185968):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ w+ a, w+ > j j @1
    add process p p > mu- vm~ w+ a j, w+ > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmWpgamma_mumnujjgamma"

elif (runArgs.runNumber==185969):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > ta+ vt w- a, w- > j j @1
    add process p p > ta+ vt w- a j, w- > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpWmgamma_taupnugamma"

elif (runArgs.runNumber==185970):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > ta- vt~ w+ a, w+ > j j @1
    add process p p > ta- vt~ w+ a j, w+ > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmWpgamma_taumnugamma"

elif (runArgs.runNumber==185971):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e+ ve z a, z > j j @1
    add process p p > e+ ve z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpZgamma_epnujjgamma"

elif (runArgs.runNumber==185972):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ z a, z > j j @1
    add process p p > e- ve~ z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmZgamma_emnujjgamma"

elif (runArgs.runNumber==185973):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu+ vm z a, z > j j @1
    add process p p > mu+ vm z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpZgamma_mupnujjgamma"

elif (runArgs.runNumber==185974):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > mu- vm~ z a, z > j j @1
    add process p p > mu- vm~ z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmZgamma_mumnujjgamma"

elif (runArgs.runNumber==185975):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > ta+ vt z a, z > j j @1
    add process p p > ta+ vt z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WpZgamma_taupnugamma"

elif (runArgs.runNumber==185976):
    fcard.write("""
    import model sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > ta- vt~ z a, z > j j @1
    add process p p > ta- vt~ z a j, z > j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WmZgamma_taumnugamma"

else:
    raise RunTimeError("No data set ID found")

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy=runArgs.ecmEnergy/2.0

else:
    raise RunTimeError("No center of mass energy found.")

# getting run cards
from PyJobTransformsCore.trfutil import get_files
get_files( rcard, keepDir=False, errorIfNotFound=True )

# generating events in MG
process_dir = new_process()

generate(run_card_loc=rcard,param_card_loc=None,mode=0,njobs=1,run_name='WVA',proc_dir=process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name = 'WVA', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)


#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_Perugia2012_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = "MadGraph+Pythia6 production for " + str(name) + "with up to 1 additional hard jets (ME+PS) with the Perugia2012 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "electroweak", "SM"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
       IEXCFILE=0
       QCUT=25
"""
phojf.write(phojinp) 
phojf.close()
