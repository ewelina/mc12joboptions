# -*- coding: utf-8 -*-
############################################################
# Generation of TFCNV samples with MadGraph5+Pythia8
# Emmanuelle Dubreuil emmanuelle.dubreuil@cern.ch
# Model : http://arxiv.org/abs/1305.2427
############################################################

### Runs
#    * 182926-182929: uu->tt - Higgs mass: 125,250,500,750 GeV
#    * 182930-182932: uc->tt - Higgs mass:     250,500,750 GeV
#    * 182933-182935: cc->tt - Higgs mass:     250,500,750 GeV

from MadGraphControl.MadGraphUtils import *
#from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

### Set generation card
if runArgs.runNumber>=182926 and runArgs.runNumber<=182929: # uu > tt
    procname='TFCNC'
    modelname='uutt'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model TFCNC_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate u u > t t
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=182930 and runArgs.runNumber<=182932: # uc > tt
    procname='TFCNC'
    modelname='uctt'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model TFCNC_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate u c > t t
    add process c u > t t
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=182933 and runArgs.runNumber<=182935: # cc > tt
    procname='TFCNC'
    modelname='cctt'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model TFCNC_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate c c > t t
    output -f
    """)
    fcard.close()
else:
    print 'ERROR: run number not in run range'
    
process_dir = new_process()

print 'INFO: Events will be generated for model '+modelname

### Modified parameters w.r.t default run_card.SM.dat
nevents = 5000

### Choice of PDF set 
doMSTW08PDFSet = True 
#doMSTW08PDFSet = False
pdfSet='lhapdf' #default set to 'cteq6l1'
pdfNumber=21000 # for lhaid 

###################
# Masses and width
###################

# default masses
###################
MH = 0

# default widths
###################
WH = 0

# in the following, only the relevant masses and widths are changed, the other ones remain at their default values

if runArgs.runNumber>=182926 and runArgs.runNumber<=182929: # uu > tt
    # setting the width and mass of the resonance
    if   runArgs.runNumber==182926:
        MH = 125     # Higgs mass of 125 GeV
        WH = 0.00565 # Higgs width
    elif runArgs.runNumber==182927:
        MH = 250     # Higgs mass of 250 GeV
        WH = 4.199   # Higgs width
    elif runArgs.runNumber==182928:
        MH = 500     # Higgs mass of 500 GeV
        WH = 64.1    # Higgs width
    elif runArgs.runNumber==182929:
        MH = 750     # Higgs mass of 750 GeV
        WH = 225     # Higgs width
    else:
        print 'ERROR: run number not in run range'
    print 'INFO: Using value MH='+str(MH)
    print 'INFO: Using value WH='+str(WH)

elif runArgs.runNumber>=182930 and runArgs.runNumber<=182932: # uc > tt
    # setting the width and mass of the resonance
    if   runArgs.runNumber==182930:
        MH = 250     # Higgs mass of 250 GeV
        WH = 4.199   # Higgs width
    elif runArgs.runNumber==182931:
        MH = 500     # Higgs mass of 500 GeV
        WH = 64.1    # Higgs width
    elif runArgs.runNumber==182932:
        MH = 750     # Higgs mass of 750 GeV
        WH = 225     # Higgs width
    else:
        print 'ERROR: run number not in run range'
    print 'INFO: Using value MH='+str(MH)
    print 'INFO: Using value WH='+str(WH)
    
elif runArgs.runNumber>=182933 and runArgs.runNumber<=182935: # cc > tt
    # setting the width and mass of the resonance
    if   runArgs.runNumber==182933:
        MH = 250     # Higgs mass of 250 GeV
        WH = 4.199   # Higgs width
    elif runArgs.runNumber==182934:
        MH = 500     # Higgs mass of 500 GeV
        WH = 64.1    # Higgs width
    elif runArgs.runNumber==182935:
        MH = 750     # Higgs mass of 750 GeV
        WH = 225.1   # Higgs width
    else:
        print 'ERROR: run number not in run range'
    print 'INFO: Using value MH='+str(MH)
    print 'INFO: Using value WH='+str(WH)

else:
    print 'ERROR: run number not in run range'

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run_card.SM.dat'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if   ' iseed ' in line:
            newcard.write('     %i     = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('     %i     = ebeam1  ! beam 1 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('     %i     = ebeam2  ! beam 2 energy in GeV \n'%(runArgs.ecmEnergy/2))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
            
# Grab parameter card and move it into place
paramcard = subprocess.Popen(['get_files','-data','param_card.TFCNC.dat'])
paramcard.wait()
if not os.access('param_card.TFCNC.dat',os.R_OK):
    print 'ERROR: Could not get param_card.TFCNC.dat'
else:
    oldcard = open('param_card.TFCNC.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if modelname=='uutt':
            if 'KTuH' in line:
                newcard.write('    1 0.100000e+00 # KTuH \n')
            elif 'KTcH' in line:
                newcard.write('    2 0.000000e+00 # KTcH \n')
            elif 'MH' in line:
                newcard.write('   25 %f # MH'%(MH))
            elif 'WH' in line:
                newcard.write('DECAY  25 %f # WH'%(WH))
            else:
                newcard.write(line)
        elif modelname=='uctt':
            if 'KTuH' in line:
                newcard.write('    1 0.100000e+00 # KTuH \n')
            elif 'KTcH' in line:
                newcard.write('    2 0.100000e+00 # KTcH \n')
            elif 'MH' in line:
                newcard.write('   25 %f # MH'%(MH))
            elif 'WH' in line:
                newcard.write('DECAY  25 %f # WH'%(WH))
            else:
                newcard.write(line)
        elif modelname=='cctt':
            if 'KTuH' in line:
                newcard.write('    1 0.000000e+00 # KTuH \n')
            elif 'KTcH' in line:
                newcard.write('    2 0.100000e+00 # KTcH \n')
            elif 'MH' in line:
                newcard.write('   25 %f # MH'%(MH))
            elif 'WH' in line:
                newcard.write('DECAY  25 %f # WH'%(WH))
            else:
                newcard.write(line)
        else:
            print 'ERROR : no valid modelname'
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

if  modelname=="uutt" or modelname=="uctt" or modelname=="cctt":
  stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+modelname+'_MH'+str(MH)+'.TXT.mc12_v1'
else:
  print 'ERROR: model '+modelname+' is unknown'

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)



### MC 12 configuration with PYTHIA 8 

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

evgenConfig.keywords = ["fcnc", "top", "higgs", "exotic"]

evgenConfig.inputfilecheck = "TFCNC"

evgenConfig.description = "TFCNC"

evgenConfig.generators = ["MadGraph", "Pythia8"]

if doMSTW08PDFSet:
    include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
else:
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_MadGraph.py")

topAlg.Pythia8.Commands += ["Init:showAllParticleData = on", "Next:numberShowLHA = 10", "Next:numberShowEvent = 10"]
topAlg.Pythia8.Commands += ["24:offIfAny = 2 4"] 
