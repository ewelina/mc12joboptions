include("MC12JobOptions/MultiObjectsFilter.py")
topAlg.MultiObjectsFilter.PtCut = 6000.
topAlg.MultiObjectsFilter.EtaCut = 3.0 
topAlg.MultiObjectsFilter.UseEle = True
topAlg.MultiObjectsFilter.UseMuo = True
topAlg.MultiObjectsFilter.UseJet = False
topAlg.MultiObjectsFilter.UsePho = False 
topAlg.MultiObjectsFilter.UseSumPt = False
topAlg.MultiObjectsFilter.PtCutEach = [10000.,6000.]
