# Generator transform pre-include
#  Gets us ready for on-the-fly Powheg generation

# process definition
#   examples: Z, Wplus, Wminus, WpWm, WZ, ZZ, tt, ... 
#   see PowhegControl_postInclude.py for documentation
process = None

# override PDF to be used in powheg
#   examples: integer number from LHAPDF numbering scheme or string from LHAPDF naming scheme
#   see PowhegControl_postInclude.py for documentation
pdf = None

# specify hadronisation generator
#  main generators are Pythia6, Pythia8, Jimmy, Herwigpp
#  post processors can be added, eg Tauola, Photos
#  example: 'Pythia6TauolaPhotos
postGenerator = 'NotSpecified'
#  override tune in hadronisation generator
postGeneratorTune = None

# Event multipliers for getting more events out of powheg to feed through athena (esp. for filters)
evt_multiplier = 1.2
