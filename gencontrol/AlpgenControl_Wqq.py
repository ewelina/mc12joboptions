# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]

ForceLeptonicTau=False

# Contact: Zofia Czyczula / Soshi Tsuno
if run_num>=169544 and run_num<=169549:

    log.info('Recognized W+jets Filtered run number.  Will generate for run '+str(run_num))
    process = 'wjet'

    event_numbers=[1000000,2000000,5000000]
    events_athena = 5000
    
    run_min = 169544

    if (run_num-run_min)>=3: UseHerwig=False # Move to Pythia for half the samples

    jets = [(run_num-run_min)%3,1] 

    if jets[0]==0: events_athena=5000
    if jets[0]==1: events_athena=5000
    if jets[0]==2: events_athena=5000

    nwarm = [ jets[0]+2 , 1000000 ]
    events_alpgen = event_numbers[jets[0]]
    if jets[0]>2: jets[0]=2
    if jets[0]==2: jets[1]=0

    special1 = """ih2 1
iwdecmod 5
ptlmin 0.0
ptjmin 15.0
etalmax 10.0
etajmax 6.0
metmin 0.0
drlmin 0.0
drjmin 0.4
ilep 0
"""

    if not UseHerwig: special1 += """xclu   0.26          ! ???
ipclu 1         ! ???
xlclu 0.26      ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

    special2 = special1

    special3 = "502    0.4        ! min RCLUS value for parton-jet matching\n"

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2, special3=special3 )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()

if ForceLeptonicTau:
   include("MC12JobOptions/Tauola_LeptonicDecay_Fragment.py")
   # tauola multiplicative factor for AMI MetaData
   topAlg.Herwig.CrossSectionScaleFactor=0.1393

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['biagio.di.micco@cern.ch','antonio.salvucci@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.keywords = ['W','hadronic']
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena

