# soft muon filter
if not hasattr(topAlg, "SoftLeptonFilter"):
    from GeneratorFilters.GeneratorFiltersConf import SoftLeptonFilter
    topAlg += SoftLeptonFilter()
if "SoftLeptonFilter" not in StreamEVGEN.RequireAlgs:
    StreamEVGEN.RequireAlgs += ["SoftLeptonFilter"]
topAlg.SoftLeptonFilter.Ptcut = 2000.
topAlg.SoftLeptonFilter.Etacut = 2.7
topAlg.SoftLeptonFilter.LeptonType = 2 #muon

# filter for central W lepton
include("MC12JobOptions/LeptonFilter.py")
topAlg.LeptonFilter.Ptcut = 15000.
topAlg.LeptonFilter.Etacut = 2.7
