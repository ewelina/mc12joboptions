print "EAS: MadGraphControl_bAbb.py"


from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

procname = "bHbbb"

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = p b b~
define j = p
generate p p > h, h > b b~ @0
add process p p > h j, h > b b~ @1
add process p p > h j j, h > b b~ @2
add process p p > h j j j, h > b b~ @3
output -f
    """)



fcard.close()

ickkw=1
xqcut = 20
qcut = max(xqcut*1.4, xqcut+10)
maxjetflavor = 5
iexcfile=0

mH = 0
wH = 0

# dictionary format:
# dataset id : (higgs mass, higgs width)
signal_points = { 
    181120 : (250, 1.68682),
    181121 : (280, 1.92318),
    181122 : (310, 2.50849),
    181123 : (350, 3.23507),
    169093 : (380, 3.65990),
    181124 : (400, 3.93561),
    181125 : (450, 4.71906),
    181126 : (500, 5.59454),
    181127 : (550, 6.84368),
    181128 : (600, 8.11044),
    181129 : (650, 9.26688),
    181130 : (700, 10.3760),
    181131 : (800, 12.5049),
    }

try:
    mH, wH = signal_points[runArgs.runNumber]
except KeyError:
    raise RunTimeError("Unknown runNumber %d" % runArgs.runNumber)

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RunTimeError("No center of mass energy found.")

process_dir = new_process()

runcard = subprocess.Popen(['get_files','-data','run_card.bH.dat'])
#runcard = subprocess.Popen(['cp', 'template/run_card.bH.dat', 'run_card.bH.dat'])
runcard.wait()

if not os.access('run_card.bH.dat',os.R_OK):
    raise RunTimeError('Could not get run card: run_card.bH.dat')
else:
    oldcard = open('run_card.bH.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(50000))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = xptj ' in line:
            newcard.write('   %f      = xptj ! minimum pt for at least one jet \n'%(0.0))
        elif ' = ptj ' in line:
            newcard.write('   %f      = ptj       ! minimum pt for the jets \n'%(xqcut))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif '= maxjetflavor' in line:
            newcard.write('   %i      = maxjetflavor \n'%(int(maxjetflavor)))
        elif ' = ickkw' in line:
            newcard.write(' %i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n' % int(ickkw))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


paramcard = subprocess.Popen(['get_files','-data','param_card.bH.dat'])
#paramcard = subprocess.Popen(['cp','template/param_card.bH.dat','param_card.bH.dat'])
paramcard.wait()

if not os.access('param_card.bH.dat',os.R_OK):
    raise RunTimeError('Could not get param card: param_card.bH.dat')
else:
    oldcard = open('param_card.bH.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if '# MH' in line:
            newcard.write('   25 %e # MH \n'%(mH))
        elif 'DECAY  25' in line:
            newcard.write('DECAY  25 %e # WH \n'%(wH))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()


generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)


stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(procname)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]


include ( "MC12JobOptions/Tauola_Fragment.py" )
include ( "MC12JobOptions/Photos_Fragment.py" )


evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = 'MadGraph_'+str(procname)
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

if ickkw==1:
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
    !...Matching parameters...
    IEXCFILE=%i
    showerkt=T
    qcut=%i
    imss(21)=24
    imss(22)=24
    """%(iexcfile,qcut)

    phojf.write(phojinp)
    phojf.close()
