from PowhegControl.PowhegUtils import PowhegConfig_base

###############################################################################
#
#  W
#
###############################################################################

class AtlasPowhegConfig_W(PowhegConfig_base) :
  # These are user configurable - put generic properties in PowhegConfig_base

  # SM parameters    
  Zmass   = 91.1876
  Zwidth  = 2.4952
  sthw2   = 0.23113
  alphaem = 0.00781653
  Wmass   = 80.399
  Wwidth  = 2.085
 
  CKM_Vud = 0.97428
  CKM_Vus = 0.2253
  CKM_Vub = 0.00347
  CKM_Vcd = 0.2252
  CKM_Vcs = 0.97345
  CKM_Vcb = 0.0410
  CKM_Vtd = 0.00862
  CKM_Vts = 0.0403
  CKM_Vtb = 0.999152
  
  # mass window
  mass_low  = 2.5
  mass_high = 8000.

  # other parameters
  runningscale = 1
  running_width = 1
  bornonly  = 0
  smartsig  = 1
  withsubtr = 1
  withdamp = 1
  ptsqmin = 0.8
  testplots = 1
  hfact  = -1
  testsuda = 0
  radregion = -1
  iupperisr = 1
  iupperfsr = 2

  withnegweights = 1

  # integration parameters
  ncall1   = 120000
  ncall2   = 250000
  nubound  = 20000 
  xupbound = 2
  itmx1    = 5
  itmx2    = 5
  ixmax    = 1
  iymax    = 1
  foldx    = 1
  foldy    = 1
  foldphi  = 1

  # Set process-dependent paths in the constructor
  def __init__(self,runArgs=None) :
    PowhegConfig_base.__init__(self,runArgs)
    self._powheg_executable += '/W/pwhg_main'

  def generateRunCard(self) :
    self.generateRunCardSharedOptions()

    with open( str(self.TestArea)+'/powheg.input','a') as f :
      f.write( '!SM parameters\n\n')
      f.write( 'Zmass '+str(self.Zmass)+' ! Z mass in GeV\n')
      f.write( 'Zwidth '+str(self.Zwidth)+' ! Z width in GeV\n')
      f.write( 'sthw2 '+str(self.sthw2)+' ! sin**2 theta w\n')
      f.write( 'alphaem '+str(self.alphaem)+' ! em coupling 1/127\n')
      f.write( 'Wmass '+str(self.Wmass)+' ! W mass in GeV\n')
      f.write( 'Wwidth '+str(self.Wwidth)+' ! W width in GeV\n\n')

      f.write( 'CKM_Vud '+str(self.CKM_Vud)+'\n')
      f.write( 'CKM_Vus '+str(self.CKM_Vus)+'\n')
      f.write( 'CKM_Vub '+str(self.CKM_Vub)+'\n')
      f.write( 'CKM_Vcd '+str(self.CKM_Vcd)+'\n')
      f.write( 'CKM_Vcs '+str(self.CKM_Vcs)+'\n')
      f.write( 'CKM_Vcb '+str(self.CKM_Vcb)+'\n')
      f.write( 'CKM_Vtd '+str(self.CKM_Vtd)+'\n')
      f.write( 'CKM_Vts '+str(self.CKM_Vts)+'\n')
      f.write( 'CKM_Vtb '+str(self.CKM_Vtb)+'\n')

      f.write( 'mass_low '+str(self.mass_low)+' ! M V > mass low\n')
      f.write( 'mass_high '+str(self.mass_high)+' ! M V < mass high\n')
      
      f.write( 'runningscale '+str(self.runningscale)+' ! choice for ren and fac scales in Bbar integration (0: fixed scale M Z, 1: running scale inv mass Z, 2: running scale transverse mass Z\n')
      f.write( 'running_width '+str(self.running_width)+'\n')

      f.write( 'idvecbos '+str(self.idvecbos)+'         ! PDG code for vector boson to be produced (Z:23, W:24, ..)\n' )
      f.write( 'vdecaymode ' +str(self.vdecaymode)+'    ! PDG code for first decay product of the vector boson\n' )
      f.write( '                                        !   (11:e-; 12:ve; 13:mu-; 14:vmu; 15:tau; 16:vtau)\n' )
      f.write( 'bornonly '+str(self.bornonly)+'         ! (default 0) if 1 do Born only\n' )
      f.write( 'smartsig '+str(self.smartsig)+'         ! (default 1) remember equal amplitudes (0 do not remember)\n' )
      f.write( 'withsubtr '+str(self.withsubtr)+'       ! (default 1) subtract real counterterms (0 do not subtract)\n' )
      f.write( 'withdamp '+str(self.withdamp)+'         ! (default 0, do not use) use Born-zero damping factor\n' )
      f.write( 'ptsqmin '+str(self.ptsqmin)+'           ! (default 0.8 GeV) minimum pt for generation of radiation\n' )
      f.write( 'testplots '+str(self.testplots)+'       ! (default 0, do not) do NLO and PWHG distributions\n' )
      f.write( 'hfact '+str(self.hfact)+'               ! (default no dumping factor) dump factor for high-pt radiation: > 0 dumpfac=h**2/(pt2+h**2)\n' )
      f.write( 'testsuda '+str(self.testsuda)+'         ! (default 0, do not test) test Sudakov form factor\n' )
      f.write( 'radregion '+str(self.radregion)+'       ! (default all regions) only generate radiation in the selected singular region\n' )
      f.write( 'iupperisr '+str(self.iupperisr)+'       ! (default 1) choice of ISR upper bounding functional form\n' )
      f.write( 'iupperfsr  '+str(self.iupperfsr )+'     ! (default 2) choice of FSR upper bounding functional form\n' )
