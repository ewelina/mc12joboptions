from MadGraphControl.MadGraphUtils import *
#from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM

#mglog = Logging.logging.getLogger('MadGraphUtils')

fcard = open('proc_card_mg5.dat','w')

doPSVars = False

#Default matching parameters
removeMomentumBalanceWarning=False
nevents=40000

ickkw = 1
xqcut = 20
qcut = 30
 
#DSID lists (extensions include systematics samples)

ttbarW_Np0Excl = [119353, 189961] # add 189961, Yuan-Pao June 25, 2014
ttbarW_Np0Excl.extend([176895, 176896, 176897, 176898, 176899, 176900, 176901, 176902]) # systematics samples

ttbarW_Np1Incl = [119354]

ttbarW_Np1Excl = [174830, 189962] # add 189962, Yuan-Pao June 25, 2014
ttbarW_Np1Excl.extend([176903, 176904, 176905, 176906, 176907, 176908, 176909, 176910]) # systematics samples

ttbarW_Np2Incl = [174831, 189963] # add 189963, Yuan-Pao June 25 2014
ttbarW_Np2Incl.extend([176911, 176912, 176913, 176914, 176915, 176916, 176917, 176918]) # systematics samples

ttbarZ_Np0Excl = [119355]
ttbarZ_Np0Excl.extend([176919, 176920, 176921, 176922, 176923, 176924, 176925, 176926]) # systematics samples

ttbarZ_Np1Incl = [119356]

ttbarZ_Np1Excl = [174832]
ttbarZ_Np1Excl.extend([176927, 176928, 176929, 176930, 176931, 176932, 176933, 176934]) # systematics samples

ttbarZ_Np2Incl = [174833]
ttbarZ_Np2Incl.extend([176935, 176936, 176937, 176938, 176939, 176940, 176941, 176942]) # systematics samples

ttbarV_filter =  [189961, 189962, 189963] # added by Yuan-Pao, June 25, 2014

#Scale variation lists
systDict={}

systDict['scalefactUP']   = [176895, 176903, 176911, 176919, 176927, 176935]
systDict['scalefactDOWN'] = [176896, 176904, 176912, 176920, 176928, 176936]
systDict['alpsfactUP']    = [176897, 176905, 176913, 176921, 176929, 176937]
systDict['alpsfactDOWN']  = [176898, 176906, 176914, 176922, 176930, 176938]
systDict['MoreFSR']       = [176899, 176907, 176915, 176923, 176931, 176939]
systDict['LessFSR']       = [176900, 176908, 176916, 176924, 176932, 176940]
systDict['xqcutUP']       = [176901, 176909, 176917, 176925, 176933, 176941]
systDict['xqcutDOWN']     = [176902, 176910, 176918, 176926, 176934, 176942]

if runArgs.runNumber in ttbarW_Np0Excl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ W+ @1
    add process p p > t t~ W- @2
    output -f
    """)
    fcard.close()
    iexcfile=1
    name="ttbarW"
    
elif runArgs.runNumber in ttbarW_Np1Incl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ W+ j @1
    add process p p > t t~ W- j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="ttbarWj"

elif runArgs.runNumber in ttbarW_Np1Excl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ W+ j @1
    add process p p > t t~ W- j @2
    output -f
    """)
    fcard.close()
    iexcfile=1
    name="ttbarWjExcl"
    if runArgs.runNumber==189962:
        nevents = 60000

elif runArgs.runNumber in ttbarW_Np2Incl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ W+ j j @1
    add process p p > t t~ W- j j @2
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="ttbarWjjIncl"
    if runArgs.runNumber==189963:
        nevents = 60000

elif runArgs.runNumber in ttbarZ_Np0Excl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ Z @1
    output -f
    """)
    fcard.close()
    iexcfile=1
    name="ttbarZ"
    
elif runArgs.runNumber in ttbarZ_Np1Incl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ Z j @1
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="ttbarZj"
    
elif runArgs.runNumber in ttbarZ_Np1Excl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ Z j @1
    output -f
    """)
    fcard.close()
    iexcfile=1
    name="ttbarZjExcl"
    
elif runArgs.runNumber in ttbarZ_Np2Incl:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ Z j j @1
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="ttbarZjjIncl"

elif runArgs.runNumber==119583:
    fcard.write("""
    import model sm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+
    define l- = e- mu-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ W+ W- @1
    output -f
    """)
    fcard.close()
    iexcfile=1
    name="ttbarWW"

beamEnergy = 4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RunTimeError("No center of mass energy found.")

systName = None
for k,v in systDict.iteritems():
    if runArgs.runNumber in v:
        systName = k
        name = name + "_" + k
print "\nSYSTEMATIC VARIATION:", systName, "\n"

scaleUP = 2.0
scaleDOWN = 0.5

alpsUP = 2.0
alpsDOWN  =0.5

if systName == 'xqcutUP':
    xqcut = 25
if systName == 'xqcutDOWN':
    xqcut = 15

if systName == 'MoreFSR':
    doPSVars = True
    PSVar = "MoreFSR"
if systName == 'LessFSR':
    doPSVars = True
    PSVar = "LessFSR"
  
# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n' % (xqcut))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n' % (nevents))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n' % (runArgs.randomSeed))
        elif ' = xptj ' in line:
            newcard.write('   %f      = xptj ! minimum pt for at least one jet \n' % (xqcut))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n' % (int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n' % (int(beamEnergy)))
        elif systName == 'scalefactUP' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n' % (scaleUP))
        elif systName == 'scalefactDOWN' and ' scalefact ' in line:
            newcard.write(' %f        = scalefact        ! scale factor for event-by-event scales \n' % (scaleDOWN))
        elif systName == 'alpsfactUP' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n' % (alpsUP))
        elif systName == 'alpsfactDOWN' and ' alpsfact ' in line:
            newcard.write(' %f        = alpsfact         ! scale factor for QCD emission vx \n' % (alpsDOWN))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

#print "proc card:"
#print fcard
print "Modified run_card.dat:"
rcard = open('run_card.dat','r')
print rcard.read()
rcard.close()

process_dir = new_process()
generate(run_card_loc = 'run_card.dat', param_card_loc = None, mode = 0, njobs = 1, run_name = 'Test', proc_dir = process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber) + '.MadGraph_' + str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events = runArgs.skipEvents
arrange_output(run_name = 'Test', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

if systName == 'alpsfactUP' or systName == 'alpsfactDOWN':

    print "\nChanging Pythia parameters to reflect change in alpsfact in MadGraph"
    if systName == 'alpsfactUP':
        #PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsUP * alpsUP)), "PARP(72)=%f" % (0.25 * (1.0 / alpsUP))]
        PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsUP * alpsUP))]
    if systName == 'alpsfactDOWN':
        #PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsDOWN * alpsDOWN)), "PARP(72)=%f" % (0.25 * (1.0 / alpsDOWN))]
        PythiaParameterList = [ "PARP(64)=%f" % (1.0 * (alpsDOWN * alpsDOWN))]

    print "New parameter values: ", PythiaParameterList
    topAlg.Pythia.PygiveCommand += PythiaParameterList

# Parton shower variations

if doPSVars:
    print "\nIncluding additional ISR/FSR/PS variation:", PSVar
    #include ( str("MC12JobOptions/Pythia_CTEQ6L1_"+PSVar+"_Common.py") )
    if PSVar == 'MoreFSR':
        PythiaParameterList = [ "PARP(72)=0.7905", "PARJ(82)=0.5" ]
    elif PSVar == 'LessFSR':
        PythiaParameterList = [ "PARP(72)=0.2635", "PARJ(82)=1.66" ]
    print "New parameter values: ", PythiaParameterList
    topAlg.Pythia.PygiveCommand += PythiaParameterList
        
include ( "MC12JobOptions/Tauola_Fragment.py" )
include ( "MC12JobOptions/Photos_Fragment.py" )

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = 'MadGraph_' + str(name)
#evgenConfig.keywords = ["SM","ttbarV"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile = stringy + '._00001.events.tar.gz'

# Additional bit for ME/PS matching
if ickkw == 1:
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
    !...Matching parameters...
    IEXCFILE=%i
    showerkt=T
    qcut=%i
    imss(21)=24
    imss(22)=24
    """ % (iexcfile, qcut)
    
    phojf.write(phojinp)
    phojf.close()

# filter
if runArgs.runNumber in ttbarV_filter:
#    print "\nfilter works oh ya!"
    nJobs=8
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    topAlg += MultiLeptonFilter("MultiLeptonFilter")
    Multi1TLeptonFilter = topAlg.MultiLeptonFilter
    Multi1TLeptonFilter.Ptcut = 7000.
    Multi1TLeptonFilter.Etacut = 3.0
    Multi1TLeptonFilter.NLeptons = 2 #was 1, Yuan-Pao June 23, 2014
    StreamEVGEN.RequireAlgs  = [ "MultiLeptonFilter" ]

    if removeMomentumBalanceWarning:
        from TruthExamples.TruthExamplesConf import TestHepMC
        topAlg += TestHepMC()
        topAlg.TestHepMC.EnergyDifference = 1e10
