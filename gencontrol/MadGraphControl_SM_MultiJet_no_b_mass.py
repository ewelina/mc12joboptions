from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
generate p p > j j @1
add process p p > j j j @2
add process p p > j j j j @3
output -f
""") 
# Only four jets possible if we use gcc 4.3
# Only five jets possible in MadGraph as of 1.5.2
fcard.close()

process_dir = new_process()

maxjetflavor = 5

# looser jet min cuts and the same xqcut for all slices.

## from previous production -------------------------->
if runArgs.runNumber==185505: 
    jetmin = 10 
    xqcut = 20 
    slice = 1
    nevents = 50000 # Works for 5000 events
elif runArgs.runNumber==185506: 
    jetmin = 30
    xqcut = 20
    slice = 2
    nevents = 1000000 # Works for 3000 events
## from previous production --------------------------<
elif runArgs.runNumber==185608: 
    jetmin = 100 
    xqcut = 20 
    slice = 3
    nevents = 1000000 # Works for 5K events
elif runArgs.runNumber==185609: 
    jetmin = 250 
    xqcut = 20 
    slice = 4
    nevents = 1000000 # Works for 4K events
elif runArgs.runNumber==185610: 
    jetmin = 500 
    xqcut = 20 
    slice = 5
    nevents = 1000000 # Works for 1.5K events
elif runArgs.runNumber==185611: 
    jetmin = 1000 
    xqcut = 20 
    slice = 6
    nevents = 1000000 # Works for 4K events

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    beamEnergy = 4000.

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' xptj ' in line:
            newcard.write('   %i      = xptj ! minimum pt for at least one jet \n'%(jetmin))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif 'maxjetflavor' in line:
            newcard.write('   %i      = maxjetflavor \n'%(int(maxjetflavor)))
        else:
            newcard.write(line)
        
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc=None,mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_MultiJet_'+str(jetmin)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
from AthenaCommon.AlgSequence import AlgSequence
topAlg = AlgSequence("TopAlg")
include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
#--------------------------------------------------------------
# Algorithms Private Options
#--------------------------------------------------------------
Pythia = topAlg.Pythia
Pythia.PythiaCommand = ["pyinit user madgraph"]
#--------------------------------------------------------------
Pythia.PythiaCommand +=  [
            "pystat 1 3 4 5",
            "pyinit dumpr 1 5",
            "pyinit pylistf 1",
            ]

# ... Tauola
include ( "MC12JobOptions/Pythia_Tauola.py" )
# ... Photos
include ( "MC12JobOptions/Pythia_Photos.py" )

# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.generators += ["MadGraph", "Pythia"]
#evgenConfig.description = 'MadGraph_MultiJets_'+str(jetmin)
evgenConfig.keywords = ["SM","MultiJet"]

evgenConfig.inputfilecheck = stringy
#evgenConfig.minevents = 20
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

# Additional bit for ME/PS matching
phojf=open('./pythia_card.dat', 'w')
phojinp = """
      !...Matching parameters...
      IEXCFILE=0
      showerkt=T
      imss(21)=24
      imss(22)=24  
    """

phojf.write(phojinp)
phojf.close()


if slice < 6:
    filterString = 'MC12JobOptions/JetFilter_JZ'+str(slice)+'.py'
elif slice == 6:
    filterString = 'MC12JobOptions/JetFilter_JZ'+str(slice)+'i.py'
else:
    sys.exit("Exiting ==> Slice > 6 wasn't supposed to be here.")

include(filterString)
