### Generation of W'->tb samples with MadGraph+Pythia ###

### Runs
#    * 110921-110931: W'R, nominal, for each mass point 
#    * 110932-110942: W'R, less PS, for each mass point 
#    * 110943-110953: W'R, more PS, for each mass point 
#    * 110954-110964: W'L, nominal, for each mass point 
#    * 110965-110975: W'L, less PS, for each mass point 
#    * 110976-110986: W'L, more PS, for each mass point 
#    * 110987-110997: W'L + single-top s-channel, nominal, for each mass point
#
### Special runs with varied g'/g:
#    * 110843: Wprime_g3_SM_right_tb_leptonic_M500
#    * 110844: Wprime_g3_SM_right_tb_leptonic_M1500
#    * 110845: Wprime_g3_SM_right_tb_leptonic_M2500
#    * 110846: Wprime_g5_SM_right_tb_leptonic_M500
#    * 110847: Wprime_g5_SM_right_tb_leptonic_M1500
#    * 110848: Wprime_g5_SM_right_tb_leptonic_M2500
#    * 110849: Wprime_g3_SM_left_tb_leptonic_M500
#    * 110850: Wprime_g3_SM_left_tb_leptonic_M1500
#    * 110851: Wprime_g3_SM_left_tb_leptonic_M2500
#    * 110852: Wprime_g5_SM_left_tb_leptonic_M500
#    * 110853: Wprime_g5_SM_left_tb_leptonic_M1500
#    * 110854: Wprime_g5_SM_left_tb_leptonic_M2500
### New AF2 samples:
#    * 110732: Wprime_g2_SM_right_tb_leptonic_M1750
#    * 110733: Wprime_g2_SM_right_tb_leptonic_M2000
#    * 110734: Wprime_g2_SM_right_tb_leptonic_M2250
#    * 110735: Wprime_g3_SM_right_tb_leptonic_M2000
#    * 110736: Wprime_g3_SM_right_tb_leptonic_M2250
#    * 110737: Wprime_g4_SM_right_tb_leptonic_M2500
#    * 110738: Wprime_g4_SM_right_tb_leptonic_M2750
#    * 110739: Wprime_g5_SM_right_tb_leptonic_M2750
#    * 110740: Wprime_g5_SM_right_tb_leptonic_M3000
#    * 110741: Wprime_g2_SM_left_tb_leptonic_M1750
#    * 110742: Wprime_g2_SM_left_tb_leptonic_M2000
#    * 110743: Wprime_g2_SM_left_tb_leptonic_M2250
#    * 110744: Wprime_g3_SM_left_tb_leptonic_M2000
#    * 110745: Wprime_g3_SM_left_tb_leptonic_M2250
#    * 110746: Wprime_g4_SM_left_tb_leptonic_M2500
#    * 110747: Wprime_g4_SM_left_tb_leptonic_M2750
#    * 110748: Wprime_g5_SM_left_tb_leptonic_M2750
#    * 110749: Wprime_g5_SM_left_tb_leptonic_M3000

from MadGraphControl.MadGraphUtils import *

### Set generation card
if (runArgs.runNumber>=110921 and runArgs.runNumber<=110986) or (runArgs.runNumber>=110843 and runArgs.runNumber<=110854) or (runArgs.runNumber>=110732 and runArgs.runNumber<=110749): # W' only
    procname='Wprime'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model WEff_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t b~ QED=0 NP=2, (t > b W+, W+ > l+ vl)
    add process p p > t~ b QED=0 NP=2, (t~ > b~ W-, W- > l- vl~)
    output -f
    """)
    fcard.close()
elif runArgs.runNumber>=110987 and runArgs.runNumber<=110997: # W'L+ST s-chan
    procname='WprimeSTs'
    fcard = open('proc_card_mg5.dat','w')
    fcard.write("""
    import model WEff_UFO
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > t b~ QED=2 NP=2, (t > b W+, W+ > l+ vl)
    add process p p > t~ b QED=2 NP=2, (t~ > b~ W-, W- > l- vl~)
    output -f
    """)
    fcard.close()
else:
    print 'ERROR: run number not in run range'
    
process_dir = new_process()

### Modified parameters w.r.t default run_card.SM.dat
nevents = 6000
#nevents = 20000
ptl = 0
xqcut = 0
bwcutoff = 25

### Choice of PDF set 
#doMSTW08PDFSet = True 
doMSTW08PDFSet = False
pdfSet='lhapdf' #default set to 'cteq6l1'
pdfNumber=21000 # for lhaid 

# W masses and width (NLO - 7 and 8 TeV)
if runArgs.runNumber in [ 110921+(i*11) for i in range(7) ]:
    MWp = 500
    WWpL = 17
    WWpR = 12
    gSF = 1
elif runArgs.runNumber in [ 110922+(i*11) for i in range(7) ]:
    MWp = 750
    WWpL = 26
    WWpR = 19
    gSF = 1
elif runArgs.runNumber in [ 110923+(i*11) for i in range(7) ]:
    MWp = 1000
    WWpL = 34
    WWpR = 26
    gSF = 1
elif runArgs.runNumber in [ 110924+(i*11) for i in range(7) ]:
    MWp = 1250
    WWpL = 43
    WWpR = 32
    gSF = 1
elif runArgs.runNumber in [ 110925+(i*11) for i in range(7) ]:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 1
elif runArgs.runNumber in [ 110926+(i*11) for i in range(7) ]:
    MWp = 1750
    WWpL = 60
    WWpR = 46
    gSF = 1
elif runArgs.runNumber in [ 110927+(i*11) for i in range(7) ]:
    MWp = 2000
    WWpL = 69
    WWpR = 52
    gSF = 1
elif runArgs.runNumber in [ 110928+(i*11) for i in range(7) ]:
    MWp = 2250
    WWpL = 78
    WWpR = 59
    gSF = 1
elif runArgs.runNumber in [ 110929+(i*11) for i in range(7) ]:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 1
elif runArgs.runNumber in [ 110930+(i*11) for i in range(7) ]:
    MWp = 2750
    WWpL = 95
    WWpR = 72
    gSF = 1
elif runArgs.runNumber in [ 110931+(i*11) for i in range(7) ]:
    MWp = 3000
    WWpL = 104
    WWpR = 78
    gSF = 1

# Special runs with g' different than 1
if runArgs.runNumber==110843 or runArgs.runNumber==110849:
    MWp = 500
    WWpL = 17
    WWpR = 12
    gSF = 3
elif runArgs.runNumber==110844 or runArgs.runNumber==110850:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 3
elif runArgs.runNumber==110845 or runArgs.runNumber==110851:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 3
elif runArgs.runNumber==110846 or runArgs.runNumber==110852:
    MWp = 500
    WWpL = 17
    WWpR = 12
    gSF = 5
elif runArgs.runNumber==110847 or runArgs.runNumber==110853:
    MWp = 1500
    WWpL = 52
    WWpR = 39
    gSF = 5
elif runArgs.runNumber==110848 or runArgs.runNumber==110854:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 5
elif runArgs.runNumber==110732 or runArgs.runNumber==110741:
    MWp = 1750
    WWpL = 60
    WWpR = 46
    gSF = 2
elif runArgs.runNumber==110733 or runArgs.runNumber==110742:
    MWp = 2000
    WWpL = 69
    WWpR = 52
    gSF = 2
elif runArgs.runNumber==110734 or runArgs.runNumber==110743:
    MWp = 2250
    WWpL = 78
    WWpR = 59
    gSF = 2
elif runArgs.runNumber==110735 or runArgs.runNumber==110744:
    MWp = 2000
    WWpL = 69
    WWpR = 52
    gSF = 3
elif runArgs.runNumber==110736 or runArgs.runNumber==110745:
    MWp = 2250
    WWpL = 78
    WWpR = 59
    gSF = 3
elif runArgs.runNumber==110737 or runArgs.runNumber==110746:
    MWp = 2500
    WWpL = 86
    WWpR = 65
    gSF = 4
elif runArgs.runNumber==110738 or runArgs.runNumber==110747:
    MWp = 2750
    WWpL = 95
    WWpR = 72
    gSF = 4
elif runArgs.runNumber==110739 or runArgs.runNumber==110748:
    MWp = 2750
    WWpL = 95
    WWpR = 72
    gSF = 5
elif runArgs.runNumber==110740 or runArgs.runNumber==110749:
    MWp = 3000
    WWpL = 104
    WWpR = 78
    gSF = 5

# L/R couplings
gSM = (6.483972e-01)

if (runArgs.runNumber>=110921 and runArgs.runNumber<=110953) or (runArgs.runNumber>=110843 and runArgs.runNumber<=110848) or (runArgs.runNumber>=110732 and runArgs.runNumber<=110740):
    chirname='right'
    gL = 0
    gR = gSM*gSF
    WWp = WWpR*(gR/gSM)**2
elif (runArgs.runNumber>=110954 and runArgs.runNumber<=110997) or (runArgs.runNumber>=110849 and runArgs.runNumber<=110854) or (runArgs.runNumber>=110741 and runArgs.runNumber<=110749):
    chirname='left'
    gL = gSM*gSF
    gR = 0
    WWp = WWpL*(gL/gSM)**2

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.SM.dat'])
runcard.wait()
if not os.access('run_card.SM.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' xqcut  ' in line:
            newcard.write('%i   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ptl ' in line:
            newcard.write('   %i      = ptl ! minimum pt for the charged leptons \n'%(ptl))
        elif ' etal ' in line:
            newcard.write(' 1d2  = etal    ! max rap for the charged leptons \n')
        elif ' ebeam1 ' in line:
            newcard.write('     %i     = ebeam1  ! beam 1 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' ebeam2 ' in line:
            newcard.write('     %i     = ebeam2  ! beam 2 energy in GeV \n'%(runArgs.ecmEnergy/2))
        elif ' bwcutoff' in line:
            newcard.write('     %i     = bwcutoff \n'%(bwcutoff))                   
        elif ' = pdlabel ' in line and doMSTW08PDFSet:
            newcard.write('   \'%s\'      = pdlabel ! PDF set\n   %d      = lhaid       ! PDF number used ONLY for LHAPDF\n' % (str(pdfSet), int(pdfNumber)))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
            
# Grab parameter card and move it into place
paramcard = subprocess.Popen(['get_files','-data','param_card.Wprime_tb.dat'])
paramcard.wait()
if not os.access('param_card.Wprime_tb.dat',os.R_OK):
    print 'ERROR: Could not get run card'
else:
    oldcard = open('param_card.Wprime_tb.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if ' MWp' in line:
            newcard.write('    34 %f # MWp \n'%(MWp))
        elif ' WWp' in line:
            newcard.write('DECAY  34 %f # WWp \n'%(WWp))
        elif ' gL' in line:
            newcard.write('    1 %f # gL \n'%(gL))
        elif ' gR' in line:
            newcard.write('    2 %f # gR \n'%(gR))
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()

generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=0,njobs=1,run_name='Test',proc_dir=process_dir)

stringy = 'group.phys-gener.MadGraph.'+str(runArgs.runNumber)+'.'+procname+'_'+chirname+'_M'+str(MWp)+'.TXT.mc12_v1'
#stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+procname+'_'+chirname+'_'+str(MWp)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name='Test',proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)


### MC 12 configuration with PYTHIA 8 

runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

evgenConfig.description = "MadGraph5+Pythia8 for Wprime"

evgenConfig.keywords = ["Wprime", "top", "schan"]

evgenConfig.inputfilecheck = "Wprime"

evgenConfig.generators = ["MadGraph", "Pythia8"]

if doMSTW08PDFSet:
    include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
else:
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")

include("MC12JobOptions/Pythia8_LHEF.py")


