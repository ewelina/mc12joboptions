from MadGraphControl.MadGraphUtils import *
from MadGraphControl.SetupMadGraphEventsForSM import setupMadGraphEventsForSM


fcard = open('proc_card_mg5.dat','w')

ickkw=0
xqcut = 15
qcut = 20
nJobs=8
nevents=10000
mmll=0.2
gridpackMode=True

if runArgs.runNumber==118522:
    fcard.write("""
    import model sm-full
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define l = l+ l-
    define v = vl vl~
    generate p p > Z l+ l- l v / h , Z > vl vl~ @1
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="ZZWStar_llnunulnu"
    mmll=0.2

else:
    print "ERROR: Run number not found!"

beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy=runArgs.ecmEnergy/2.0
else:
    raise RunTimeError("No center of mass energy found.")


lhapdfid=-1

# Grab the run card and move it into place
runcard = subprocess.Popen(['get_files','-data','run_card.triboson.dat'])
runcard.wait()
if not os.access('run_card.triboson.dat',os.R_OK):
    print 'ERROR: Could not get run card'
elif os.access('run_card.dat',os.R_OK):
    print 'ERROR: Old run card in the current directory.  Dont want to clobber it.  Please move it first.'
else:
    oldcard = open('run_card.triboson.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw ' in line:
            newcard.write('%i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ickkw==1 and ' xqcut ' in line:
            newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))
        elif ickkw==1 and ' xptj ' in line:
            newcard.write('   %f      = xptj ! minimum pt for at least one jet \n'%(xqcut))
        elif ' nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = mmll ' in line:
            newcard.write(' %f = mmll    ! min invariant mass of l+l- (same flavour) lepton pair \n' % (mmll))
        elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
        else:
            newcard.write(line)
    oldcard.close()
    newcard.close()
    
stringy=''
   

procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
procCard.wait()

runCard = subprocess.Popen(['cat','run_card.dat'])
runCard.wait()


runName='Test'     

if not hasattr(runArgs, "inputGenConfFile"):
    ## Generate process ##
    if gridpackMode:
        print 'Generating process for gridpack'
    else:
        print 'Generating process and events'

    process_dir = new_process()
    generate(run_card_loc='run_card.dat',param_card_loc=None,mode=1,njobs=nJobs,proc_dir=process_dir,run_name=runName,grid_pack=gridpackMode)
    
else:
    seed=runArgs.randomSeed

    if gridpackMode and hasattr(runArgs, "inputGenConfFile"):
        ## Generate events from gridpack ##
        print 'Generating events using gridpack mode'
        
        gridpack_dir='madevent/'
        generate_from_gridpack(run_name=runName,gridpack_dir=gridpack_dir,nevents=nevents,random_seed=seed)
        process_dir=gridpack_dir
        

if not gridpackMode or (gridpackMode and hasattr(runArgs, "inputGenConfFile")):

    ## Pass to Pythia ##
    print 'Pass generated events to Pythia'
    stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)

    skip_events=0
    if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
    arrange_output(run_name=runName,proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)
    

    #--------------------------------------------------------------
    # General MC12 configuration
    #--------------------------------------------------------------
    include ( "MC12JobOptions/Pythia_AUET2B_CTEQ6L1_Common.py" )
    topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]
    
    #--------------------------------------------------------------
    topAlg.Pythia.PythiaCommand +=  [
                    "pystat 1 3 4 5",
                    "pyinit dumpr 1 5",
                    "pyinit pylistf 1",
                    ]    
    
#    include ( "MC12JobOptions/Tauola_Fragment.py" )
    include ( "MC12JobOptions/Photos_Fragment.py" )
        
    evgenConfig.generators += ["MadGraph", "Pythia"]
    evgenConfig.description = 'MadGraph_'+str(name)
    evgenConfig.keywords = ["triboson","ZZW"]
    evgenConfig.inputfilecheck = stringy
    evgenConfig.minevents = 5000
    runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
    
   
    # Additional bit for ME/PS matching
    if ickkw==1:
        phojf=open('./pythia_card.dat', 'w')
        phojinp = """
        !...Matching parameters...
        IEXCFILE=%i
        showerkt=T
        qcut=%i
        imss(21)=24
        imss(22)=24
        """%(iexcfile,qcut)
        
        phojf.write(phojinp)
        phojf.close()
