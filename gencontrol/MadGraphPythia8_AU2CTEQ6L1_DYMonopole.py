###############################################################
#
# Job options file for Evgen MadGraph Pythia8 Monopole Generation
# W. Taylor, 2014-01-23
#==============================================================

#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include( "MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py" )
include( "MC12JobOptions/Pythia8_MadGraph.py" )

#--------------------------------------------------------------
# Configuration for EvgenJobTransforms
#--------------------------------------------------------------
evgenConfig.description = "Drell-Yan magnetic monopole generation for Mass=%s, Gcharge=%s with MadGraph+Pythia8 and the AU2 CTEQ6L1 tune in MC12" % (mass,gcharge)
evgenConfig.keywords = ["exotics", "Monopole", "Drell-Yan"]
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["wtaylor@cern.ch"]

evgenConfig.specialConfig = 'MASS=%s;GCHARGE=%s;preInclude=SimulationJobOptions/preInclude.Monopole.py' % (mass,gcharge)

#--------------------------------------------------------------
# Edit PDGTABLE.MeV with monopole mass
#--------------------------------------------------------------
ALINE1="M 4110000                          %s.E+03       +0.0E+00 -0.0E+00 Monopole        0" % (mass)
ALINE2="W 4110000                          0.E+00         +0.0E+00 -0.0E+00 Monopole        0"

import os
import sys

pdgmod = os.path.isfile('PDGTABLE.MeV')
if pdgmod is True:
    os.remove('PDGTABLE.MeV')
os.system('get_files -data PDGTABLE.MeV')
f=open('PDGTABLE.MeV','a')
f.writelines(str(ALINE1))
f.writelines('\n')
f.writelines(str(ALINE2))
f.writelines('\n')
f.close()

del ALINE1
del ALINE2
