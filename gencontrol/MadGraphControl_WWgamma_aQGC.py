from MadGraphControl.MadGraphUtils import *

fcard = open('proc_card_mg5.dat','w')
rcard='run_card.wwa.dat'

if not hasattr(runArgs,'runNumber'):
  raise RunTimeError("No run number found.")

#--------------------------------------------------------------
# MG5 Proc card
#--------------------------------------------------------------
if ((runArgs.runNumber==185977) or (runArgs.runNumber==185978) or (runArgs.runNumber==185979) or (runArgs.runNumber==185980)):
    fcard.write("""
    import model SM_LM0123_UFO
    
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ e+ ve a NP=1 @1
    add process p p > mu- vm~ mu+ vm a NP=1 @2
    add process p p > e- ve~ mu+ vm a NP=1 @3
    add process p p > mu- vm~ e+ ve a NP=1 @4
        
    output -f
    """)
    fcard.close()
    iexcfile=0
    if (runArgs.runNumber==185977):
        name="WWgamma_lnulnugamma_lightleptons_aqgc_LM0"
    elif (runArgs.runNumber==185978):
        name="WWgamma_lnulnugamma_lightleptons_aqgc_LM1"
    elif (runArgs.runNumber==185979):
        name="WWgamma_lnulnugamma_lightleptons_aqgc_LM2"
    elif (runArgs.runNumber==185980):
        name="WWgamma_lnulnugamma_lightleptons_aqgc_LM3"
        
elif (runArgs.runNumber==185981):
    fcard.write("""
    import model SM_LT012_UFO
     
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s b u~ c~ d~ s~ b~
    generate p p > e- ve~ e+ ve a NP=1 @1
    add process p p > mu- vm~ mu+ vm a NP=1 @2
    add process p p > e- ve~ mu+ vm a NP=1 @3
    add process p p > mu- vm~ e+ ve a NP=1 @4
     
    output -f
    """)
    fcard.close()
    iexcfile=0
    name="WWgamma_lnulnugamma_lightleptons_aqgc_LT0"

else:
    raise RunTimeError("No data set ID found")


beamEnergy=4000
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy=runArgs.ecmEnergy/2.0

else:
    raise RunTimeError("No center of mass energy found.")

# getting run cards
from PyJobTransformsCore.trfutil import get_files
get_files( rcard, keepDir=False, errorIfNotFound=True )

# Grab the parameter and the run card and move it into place
paramcard = subprocess.Popen(['get_files','-data','MadGraph_WVA_aqgc_param_card.dat'])
paramcard.wait()

if not os.access('MadGraph_WVA_aqgc_param_card.dat',os.R_OK):
    raise RunTimeError('Could not get param card: MadGraph_WVA_aqgc_param_card.dat')
elif os.access('param_card.dat',os.R_OK):
    raise RunTimeError('Old param card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('MadGraph_WVA_aqgc_param_card.dat','r')
    newcard = open('param_card.dat','w')
    for line in oldcard:
        if (runArgs.runNumber==185977):
            if '# FM0' in line:
                newcard.write('    3 80.0e-11 # FM0 \n')
            else:
                newcard.write(line)
        if (runArgs.runNumber==185978):
            if '# FM1' in line:
                newcard.write('    4 130.0e-11 # FM1 \n')
            else:
                newcard.write(line)
        if (runArgs.runNumber==185979):
            if '# FM2' in line:
                newcard.write('    5 40.0e-11 # FM2 \n')
            else:
                newcard.write(line)        
        if (runArgs.runNumber==185980):
            if '# FM3' in line:
                newcard.write('    6 65.0e-11 # FM3 \n')
            else:
                newcard.write(line)
        if (runArgs.runNumber==185981):
            if '# FT0' in line:
                newcard.write('   11 25.0e-11 # FT0 \n')
            else:
                newcard.write(line)              
    oldcard.close()
    newcard.close()

print "Modified parameter.dat:"
pcard = open('param_card.dat','r')
print pcard.read()
pcard.close()


process_dir = new_process()

generate(run_card_loc=rcard,param_card_loc='param_card.dat',mode=0,njobs=1,run_name='WWAaQGC',proc_dir=process_dir)

stringy = 'madgraph.' + str(runArgs.runNumber)+'.MadGraph_'+str(name)

skip_events = 0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
arrange_output(run_name = 'WWAaQGC', proc_dir = process_dir, outputDS = stringy + '._00001.events.tar.gz', skip_events = skip_events)


#--------------------------------------------------------------
# General MC12 configuration
#--------------------------------------------------------------
include ( "MC12JobOptions/Pythia_Perugia2012_Common.py" )
topAlg.Pythia.PythiaCommand = ["pyinit user madgraph"]

#--------------------------------------------------------------
topAlg.Pythia.PythiaCommand +=  [
                "pystat 1 3 4 5",
                "pyinit dumpr 1 5",
                "pyinit pylistf 1",
                ]

evgenConfig.generators += ["MadGraph", "Pythia"]
evgenConfig.description = "MadGraph+Pythia6 production for " + str(name) + "with up to 1 additional hard jets (ME+PS) with the Perugia2012 CTEQ6L1 tune"
evgenConfig.keywords = ["triboson", "electroweak", "SM"]
evgenConfig.inputfilecheck = stringy
evgenConfig.minevents = 5000
runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'

phojf=open('./pythia_card.dat', 'w')
phojinp = """
!...Matching parameters...
       IEXCFILE=0
       QCUT=25
"""
phojf.write(phojinp) 
phojf.close()

