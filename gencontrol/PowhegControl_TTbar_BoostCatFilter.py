include("MC12JobOptions/PowhegControl_preInclude.py")

run_num=int(runArgs.runNumber)

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia6 ttbar production with Perugia 2011c tune'
evgenConfig.keywords    = [ 'top', 'ttbar', 'leptonic' ]
evgenConfig.contact     = [ 'james.robinson@cern.ch' ]

postGenerator="Pythia6TauolaPhotos"
process="tt_all"

# compensate filter efficiency
# number of events need to be changed in the top job
factor = { 189541: 2., 189542: 8., 189543: 13., 189544: 35., 189545: 35., 189546: 500., 189547: 1200. }
evt_multiplier = factor[run_num]

include("MC12JobOptions/PowhegControl_postInclude.py")

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include("MC12JobOptions/TTbarBoostCatFilter.py")
low = -1.
high = -1.
if run_num==189541:
	low = -1.
	high = 60000.
if run_num==189542:
	low = 60000.
	high = 90000.
if run_num==189543:
	low = 90000.
	high = 125000.
if run_num==189544:
	low = 125000.
	high = 150000.
if run_num==189545:
	low = 150000.
	high = 250000.
if run_num==189546:
	low = 250000.
	high = 350000.
if run_num==189547:
        low = 350000.
        high = -1.

topAlg.TTbarBoostCatFilter.LepPtcutLowedge = low
topAlg.TTbarBoostCatFilter.LepPtcutHighedge = high
