# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
TTBARV=False

# ttbar+W->lnu
if run_num>=174233 and run_num<=174237:
    event_numbers=[500000,4000000,6000000,12000000]
    jets = [ run_num-174233 ,1]
    # Np0,Np1,Np2,Np3+,Np2+
    if run_num==174236: jets = [3,0]
    if run_num==174237: jets = [2,0]
    events_athena_nJet=[5000,5000,1000,500,1000]
    events_athena=events_athena_nJet[ jets[0] ]
    ttbarV_mode=1 # corresponds to ttbarW
    special1 = """iwdecmode 4          ! W decay mode
"""
    TTBARV=True

# ttbar+W->qqbar
if run_num>=174238 and run_num<=174242:
    event_numbers=[500000,4000000,6000000,12000000]
    jets = [ run_num-174238 ,1]
    # Np0,Np1,Np2,Np3+
    if run_num==174241: jets = [3,0]
    if run_num==174242: jets = [2,0]
    events_athena_nJet=[5000,5000,1000,500,1000]
    events_athena=events_athena_nJet[ jets[0] ]
    ttbarV_mode=1 # corresponds to ttbarW
    special1 = """iwdecmode 5          ! W decay mode
"""
    TTBARV=True

# ttbar+Z->nunu
if run_num>=174243 and run_num<=174247:
    event_numbers=[1400000,6000000,6000000,12000000]
    jets = [ run_num-174243 ,1]
    # Np0,Np1,Np2,Np3+,Np2+
    if run_num==174246: jets = [3,0]
    if run_num==174247: jets = [2,0]
    events_athena_nJet=[5000,5000,1000,500,1000]
    events_athena=events_athena_nJet[ jets[0] ]
    ttbarV_mode=2 # corresponds to ttbarZ
    special1 = """ilep 1       ! Use neutrinos in the final state (not leptons) 
mllmin 0.0 ! Make sure there is no mll cut
mllmax 8000.0 ! Make sure there is no mll cut
etmiss_min 0.0 ! Make sure there is no met cut
izdecmode 4          ! Z decay mode (all flavours)
"""
    TTBARV=True

# ttbar+Z->ll
if run_num>=174248 and run_num<=174252:
    event_numbers=[3500000,20000000,20000000,30000000]
    jets = [ run_num-174248 ,1]
    # Np0,Np1,Np2,Np3+,Np2+
    if run_num==174251: jets = [3,0]
    if run_num==174252: jets = [2,0]
    events_athena_nJet=[5000,5000,1000,500,1000]
    events_athena=events_athena_nJet[ jets[0] ]
    ttbarV_mode=2 # corresponds to ttbarZ
    special1 = """ilep 0       ! Use charged leptons in the final state
mllmin 5.0 ! Set min mll cut
mllmax 8000.0 ! Make sure there is no upper mll cut
izdecmode 4          ! Z decay mode (all flavours)
"""
    TTBARV=True

# ttbar+Z->qqbar - this is currently not possible it is thought

if TTBARV:
    log.info('Recognised ttbar+V run number. Will generate for run '+str(run_num))

    nwarm = jets[0]+2
    events_alpgen = event_numbers[jets[0]]

    nwarm = [ jets[0]+2 , 4000000 ]
    if ttbarV_mode==1:
        process = 'wqq'
        special1 += """ihvy 6          ! Set top as heavy flavour quark
"""
    if ttbarV_mode==2:
        process = 'zqq'
        special1 += """ihvy 6          ! Set top as heavy flavour quark
"""


if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3 )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()


# Fill up the evgenConfig
evgenConfig.description = 'Alpgen autoconfig ttbar+V'
evgenConfig.contact = ['zach.marshall@cern.ch','christopher.young@cern.ch']
evgenConfig.keywords = ['SUSY', 'ttbar']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.minevents = events_athena
