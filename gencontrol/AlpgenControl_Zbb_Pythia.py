# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=False
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
jets = 0

############################################################################

# Zbb 4LepM - B. Di Micco, A. Salvucci
UseM4lFilter = False
UseM4lVeto = False
Use3lFilter = False
FileCheck = '.*'

if run_num>=181420 and run_num<=181439:
    
    log.info('Recognized Zbb+jets Filtered run number.  Will generate for run '+str(run_num))
    process = 'zqq'

    event_numbers=[250000000,120000000,66000000,30000000]
    events_athena = 5000
    Z_mode=0

    ## Zbb Z-> nunu
    #if run_num>=167060 and run_num<=167063:
    #   run_min=167060
    #   jets = [(run_num-run_min)%5,1]
    #   
    #   if ecm < 5000:
    #       event_numbers[2]=20000000
    #   if jets[0]==0:
    #       events_athena=5000
    #   if jets[0]==1:
    #       events_athena=5000
    #   if jets[0]==2:
    #       events_athena=500
    #   if jets[0]==3:
    #       events_athena=200
    #   Z_mode=4
    #        
    ##Zbb no filter (for 3p, please use a grid input file)
    #if run_num>=167100 and run_num<=167108:
    #    run_min = 167100
    #    jets = [(run_num-run_min)%5,1] 
    #
    #    if jets[0]==0: events_athena=5000
    #    if jets[0]==1: events_athena=5000
    #    if jets[0]==2: events_athena=500 
    #    if jets[0]==3: events_athena=200
    #    if ecm < 5000: event_numbers[2]=20000000 
 
    #Zbb 4LepM, 3p are without filter
    if run_num>=181420 and run_num<=181429:
        UseM4lFilter = True
        run_min = 181420
        jets = [(run_num-run_min)%5,1] 
        if jets[0]==0: events_athena=500
        if jets[0]==1: events_athena=200
        if jets[0]==2: events_athena=20
        
    #Zbb Veto4LepM+Pass3Lep
    if run_num>=181430 and run_num<=181439:
        UseM4lVeto = True
        Use3lFilter = True
        run_min = 181430
        jets = [(run_num-run_min)%5,1] 
        if jets[0]==0: events_athena=2000
        if jets[0]==1: events_athena=2000 
        if jets[0]==2: events_athena=500

    
        
    nwarm = [ jets[0]+2 , 1000000 ]
    events_alpgen = event_numbers[jets[0]]
    # Ok, just kidding, we aren't doing 7 partons, we're doing 5 inclusive
    if jets[0]>3: jets[0]=3

    # Z_mode = 1(ee) 2(mumu) 3(tautau) 4 nunu
    if Z_mode==0: Z_mode = int((run_num-run_min)/5)%2+1

    if jets[0]==3 and Z_mode<4:
         jets[1]=0
         FileCheck='AlpgenJimmy.*Zbb.*Np3'

    
    special1 = """mllmin  30           ! Minimum M_ll
mllmax  2000         ! Maximum M_ll
ihvy    5            ! Select Z+bb
mc      0            ! quark b mass
ptbmin  0            ! quark b pt min
ptcmin  0            ! quark c pt min
drjmin  0.4          ! deltar jets
drbmin  0            ! deltar b
"""

    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) 
izdecmode %i          ! Z decay mode (4: nunu)\n """%(Z_mode)

    special2 = special1

    special3 = "502    0.4        ! min RCLUS value for parton-jet matching\n"

if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

inputgridfile = None
if hasattr(runArgs,'inputGenConfFile'):
    log.info('Passing input grid file to AlpGenUtils ' +str(runArgs.inputGenConfFile))
    inputgridfile=runArgs.inputGenConfFile


from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2, special3=special3, inputgridfile=inputgridfile )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

if Use3lFilter: include("MC12JobOptions/AlpgenControl_3lFilter.py")

if UseM4lFilter: include("MC12JobOptions/AlpgenControl_M4lFilter_LowerCuts.py")

if UseM4lVeto: include("MC12JobOptions/AlpgenControl_M4lVeto_LowerCuts.py")

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.keywords = ['Higgs', 'Z','e','mu','bb']
evgenConfig.contact = ['biagio.di.micco@cern.ch','antonio.salvucci@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.inputconfcheck = FileCheck
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
