from PowhegControl.PowhegUtils import PowhegConfig_base

###############################################################################
#
#  WW
#
###############################################################################

class AtlasPowhegConfig_WW(PowhegConfig_base) :
  # These are user configurable - put generic properties in PowhegConfig_base

  # other parameters
  tevscale = None

  # integration parameters
  ncall1  = 300000
  itmx1   = 3
  ncall2  = 1000000
  itmx2   = 5
  nubound = 500000 

  # Set process-dependent paths in the constructor
  def __init__(self,runArgs=None) :
    PowhegConfig_base.__init__(self,runArgs)
    self._powheg_executable += '/WW/pwhg_main'

  def generateRunCard(self) :
    self.generateRunCardSharedOptions()

    with open( str(self.TestArea)+'/powheg.input','a') as f :
      f.write( 'vdecaymodeWp '+str(self.vdecaymodeWp)+' ! PDG code for charged decay product of the vector boson (1:dbar; 3: sbar; 7: dbar or sbar; -11:e+; -13:mu+; -15:tau+)\n' )
      f.write( 'vdecaymodeWm '+str(self.vdecaymodeWm)+' ! PDG code for charged decay product of the vector boson (1:d; 3: s; 7: d or s; 11:e-; 13:mu-; 15:tau-)\n' )

      if self.tevscale:
        f.write( 'tevscale '+str(self.tevscale)+'\n' )
