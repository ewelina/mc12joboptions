from MadGraphControl.MadGraphUtils import *


# Nevents
minevents=5000
nevents=40000
lhe_version=2

# Gridpack and parallel running options
gridpackMode=False
nJobs=1
cluster_type=None
cluster_queue=None

# Parameters to override MG5 defaults
ptjcut=20
ptbcut=20
mllcut=0
mmjjcut=0
dRjjcut=0.
dRllcut=0.
dRjlcut=0.4
pTlcut=0.
etalcut=10
etabcut=6
etajcut=6

maxjetflavor=5
asrwgtflavor=5

#lhapdfid=10042
#Internal PDF options below - but should always LHAPDF where possible
lhapdfid=-1
pdflabel='cteq6l1' 

# Matching/merging options
ickkw=0
cut_decays='F'

dokTMerging=False
doMLMMatching=False
doCutBasedMerging=False
nJetMax=-1
ktdurham=-1
dparameter=-1

# Post-showering options
removeMomentumBalanceWarning=False
applyBFilter=False
runRivet=None


#DSIDs
Zee_jets_5fl_Incl = [187521,187526]
Zee_jets_5fl_Np0  = [187522,187527]
Zee_jets_5fl_Np1  = [187523,187528]
Zee_jets_5fl_Np2  = [187524,187529]
Zee_jets_5fl_Np3  = [187525,187530]

Wenu_jets_5fl_Incl = [187531,187536]
Wenu_jets_5fl_Np0  = [187532,187537]
Wenu_jets_5fl_Np1  = [187533,187538]
Wenu_jets_5fl_Np2  = [187534,187539]
Wenu_jets_5fl_Np3  = [187535,187540]


EvtGenList=[187526,
            187527,
            187528,
            187529,
            187530,
            187536,
            187537,            
            187538,
            187539,    
            187540]


fcard = open('proc_card_mg5.dat','w')
# Zee 5fl Inclusive Pythia8
if runArgs.runNumber in Zee_jets_5fl_Incl:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > e- e+ @0
    output -f
    """)
    fcard.close()

    name="Zee_jets_5fl_Incl_smnobmass_Pythia8"
    mllcut=10
    #runRivet='Zjets'
    evgenConfig.keywords+=['Z','electron','jets','drellYan']

#Zee Np0 5fl Pythia8 KTMerging
elif runArgs.runNumber in Zee_jets_5fl_Np0:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > e- e+ @0
    output -f
    """)
    fcard.close()
  
    name="Zee_jets_5fl_Np0Np3_smnobmass_Pythia8"

    mllcut=10
    
    process="pp>e-e+"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','electron','jets','drellYan']

#Zee Np1 5fl Pythia8 KTMerging
elif runArgs.runNumber in Zee_jets_5fl_Np1:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > e- e+ j @1
    output -f
    """)
    fcard.close()
  
    name="Zee_jets_5fl_Np0Np3_smnobmass_Pythia8"

    mllcut=10
    
    process="pp>e-e+"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','electron','jets','drellYan']

#Zee Np2 5fl Pythia8 KTMerging
elif runArgs.runNumber in Zee_jets_5fl_Np2:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > e- e+ j j @2
    output -f
    """)
    fcard.close()
  
    name="Zee_jets_5fl_Np2_smnobmass_Pythia8"

    mllcut=10
    
    process="pp>e-e+"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','electron','jets','drellYan']

#Zee Np3 5fl Pythia8 KTMerging
elif runArgs.runNumber in Zee_jets_5fl_Np3:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    generate p p > e- e+ j j j @3
    output -f
    """)
    fcard.close()
  
    name="Zee_jets_5fl_Np3_smnobmass_Pythia8"

    mllcut=10
    
    process="pp>e-e+"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['Z','electron','jets','drellYan']

# Wenu 5fl Inclusive Pythia8
elif runArgs.runNumber in Wenu_jets_5fl_Incl:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define epm = e+ e-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define veall = ve ve~
    generate p p > epm veall @0
    output -f
    """)
    fcard.close()

    name="Wenu_jets_5fl_Incl_smnobmass_Pythia8"
    evgenConfig.keywords+=['W','electron','jets']
       
#Wenu Np0 5fl Pythia8 KTMerging
elif runArgs.runNumber in Wenu_jets_5fl_Np0:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define epm = e+ e-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define veall = ve ve~
    generate p p > epm veall @0
    output -f
    """)
    fcard.close()
  
    name="Wenu_jets_5fl_Np_smnobmass_Pythia8"

    process="pp>LEPTONS,NEUTRINOS"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['W','electron','jets']

#Wenu Np1 5fl Pythia8 KTMerging
elif runArgs.runNumber in Wenu_jets_5fl_Np1:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define epm = e+ e-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define veall = ve ve~
    generate p p > epm veall j @1
    output -f
    """)
    fcard.close()
  
    name="Wenu_jets_5fl_Np1_smnobmass_Pythia8"

    process="pp>LEPTONS,NEUTRINOS"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['W','electron','jets']

#Wenu Np2 5fl Pythia8 KTMerging
elif runArgs.runNumber in Wenu_jets_5fl_Np2:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define epm = e+ e-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define veall = ve ve~
    generate p p > epm veall j j @2
    output -f
    """)
    fcard.close()
  
    name="Wenu_jets_5fl_Np2_smnobmass_Pythia8"

    process="pp>LEPTONS,NEUTRINOS"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['W','electron','jets']

#Wenu Np3 5fl Pythia8 KTMerging
elif runArgs.runNumber in Wenu_jets_5fl_Np3:
    fcard.write("""
    import model sm-no_b_mass
    define p = g u c d s b u~ c~ d~ s~ b~
    define j = g u c d s b u~ c~ d~ s~ b~
    define l+ = e+ mu+ ta+
    define l- = e- mu- ta-
    define epm = e+ e-
    define vl = ve vm vt
    define vl~ = ve~ vm~ vt~
    define veall = ve ve~
    generate p p > epm veall j j j @3
    output -f
    """)
    fcard.close()
  
    name="Wenu_jets_5fl_Np3_smnobmass_Pythia8"

    process="pp>LEPTONS,NEUTRINOS"
    dokTMerging=True
    nJetMax=3
    ktdurham=30
    dparameter=0.4
    evgenConfig.keywords+=['W','electron','jets']

else:
    raise RuntimeError("DSID %i not recognised."%(int(runArgs.runNumber)))



beamEnergy=0
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


# Grab the run card and move it into place
if os.access(os.environ['MADPATH']+'/Template/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/Cards/run_card.dat','run_card.SM.dat')
elif os.access(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat',os.R_OK):
    shutil.copy(os.environ['MADPATH']+'/Template/LO/Cards/run_card.dat','run_card.SM.dat')
else:
    raise RuntimeError('Cannot find Template run_card.dat!')
    

    
if not os.access('run_card.SM.dat',os.R_OK):
    raise RuntimeError('Cannot read fetched run_card.dat!')
elif os.access('run_card.dat',os.R_OK):
    raise RuntimeError('Old run card in the current directory.  Dont want to clobber it.  Please move it first.')
else:
    oldcard = open('run_card.SM.dat','r')
    newcard = open('run_card.dat','w')
    for line in oldcard:
        if ' = ickkw' in line and ickkw!=0:
            newcard.write(' %i        = ickkw            ! 0 no matching, 1 MLM, 2 CKKW matching \n'%(ickkw))
        elif ' = xqcut ' in line and ickkw==1 and xqcut>0:
                newcard.write('%f   = xqcut   ! minimum kt jet measure between partons \n'%(xqcut))               
        elif ' = ptj ' in line and ptjcut!=20:
            newcard.write(' %f  = ptj       ! minimum pt for the jets \n'%(ptjcut))
        elif ' = mmjj ' in line and mmjjcut>0:
            newcard.write(' %f = mmjj    ! min invariant mass of a jet pair\n'%(mmjjcut))
        elif ' = drjj ' in line and dRjjcut!=0.4:
            newcard.write(' %f = drjj    ! min distance between jets \n'%(dRjjcut))
        elif ' = ptb ' in line and ptbcut>0.:
            newcard.write(' %f  = ptb       ! minimum pt for the b \n'%(ptbcut))
        elif ' = maxjetflavor' in line and maxjetflavor!=4:
            newcard.write(' %i = maxjetflavor \n'%(maxjetflavor))
        elif ' = asrwgtflavor ' in line and asrwgtflavor!=5: 
            newcard.write(' %i        = asrwgtflavor     ! highest quark flavor for a_s reweight\n'%(asrwgtflavor))
        elif ' = nevents ' in line:
            newcard.write('  %i       = nevents ! Number of unweighted events requested \n'%(nevents))
        elif ' = iseed ' in line:
            newcard.write('   %i      = iseed   ! rnd seed (0=assigned automatically=default)) \n'%(runArgs.randomSeed))
        elif ' = ebeam1 ' in line:
            newcard.write('   %i      = ebeam1  ! beam 1 energy in GeV \n'%(int(beamEnergy)))
        elif ' = ebeam2 ' in line:
            newcard.write('   %i      = ebeam2  ! beam 2 energy in GeV \n'%(int(beamEnergy)))
        elif ' = cut_decays' in line and cut_decays!='T':
            newcard.write(' %s = cut_decays \n'%(cut_decays))
        elif ' = mmll ' in line and mllcut>0:
            newcard.write(' %f = mmll    ! min invariant mass of l+l- (same flavour) lepton pair \n'%(mllcut))
        elif ' = drll ' in line and dRllcut!=0.4:
            newcard.write(' %f = drll    ! min distance between leptons \n'%(dRllcut))
        elif ' = ptl ' in line and pTlcut!=10:
            newcard.write(' %f = ptl       ! minimum pt for the charged leptons \n'%(pTlcut))
        elif ' = etal ' in line and etalcut!=2.5:
            newcard.write(' %f = etal    ! max rap for the charged leptons \n'%(etalcut))
        elif ' = drjl ' in line and dRjlcut!=0.4:
            newcard.write(' %f = drjl    ! min distance between jet and lepton \n'%(dRjlcut))
        elif ' = etab ' in line and etabcut!=-1:
            newcard.write(' %f = etab    ! max rap for the b \n'%(etabcut))
        elif ' = etaj ' in line and etajcut!=5:
           newcard.write(' %f = etaj    ! max rap for the jets \n'%(etajcut))
        elif ' =  ktdurham' in line and ktdurham!=-1:
            newcard.write(' %i =  ktdurham\n'%(ktdurham))
        elif ' =  dparameter' in line and dparameter!=0.4:
            newcard.write(' %f =  dparameter\n'%(dparameter))
        elif ' = lhe_version' in line:
            newcard.write(' %i.0      = lhe_version       ! Change the way clustering information pass to shower.\n'%lhe_version)
        elif lhapdfid>0 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'lhapdf\'    = pdlabel     ! PDF set\n')
        elif lhapdfid>0 and '= lhaid ' in line:
            newcard.write('   %i       = lhaid       ! PDF number used ONLY for LHAPDF\n'%(lhapdfid))  
        elif lhapdfid==-1 and '= pdlabel     ! PDF set' in line:
            newcard.write('   \'%s\'    = pdlabel     ! PDF set\n'%(pdflabel))
        else:
            newcard.write(line)
            

    oldcard.close()
    newcard.close()


   
stringy=''
   
#Print out cards for debugging
procCard = subprocess.Popen(['cat','proc_card_mg5.dat'])
procCard.wait()

runCard = subprocess.Popen(['cat','run_card.dat'])
runCard.wait()



# Check if gridpack is provided, if not go to create new process 
if not hasattr(runArgs, "inputGenConfFile"):
    ## Generate process 
    process_dir = new_process()
    generate(run_card_loc='run_card.dat',param_card_loc=None,mode=1,njobs=nJobs,proc_dir=process_dir,grid_pack=gridpackMode,cluster_type=cluster_type,cluster_queue=cluster_queue)
    
else:

    if gridpackMode and hasattr(runArgs, "inputGenConfFile"):
        ## Gridpack found - now generate events from gridpack 
        print 'Generating events using gripack mode'
        seed=runArgs.randomSeed
        gridpack_dir='madevent/'
        generate_from_gridpack(gridpack_dir=gridpack_dir,nevents=nevents,random_seed=seed)
        process_dir=gridpack_dir
        


if not gridpackMode or (gridpackMode and hasattr(runArgs, "inputGenConfFile")):

    ## Pass generated events to Pythia8 (if not generating gridpack)
    print 'Pass generated events to Pythia8'
    
    stringy = 'madgraph.'+str(runArgs.runNumber)+'.MadGraph_'+str(name)
    
    skip_events=0
    if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents
    arrange_output(proc_dir=process_dir,outputDS=stringy+'._00001.events.tar.gz',skip_events=skip_events)
    
    
    #Choose PDF
    include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
    #include("MC12JobOptions/Pythia8_AU2_CT10_Common.py")
    #include("MC12JobOptions/Pythia8_AU2_CTEQ6L1_Common.py")
    #include("MC12JobOptions/Pythia8_AU2_MSTW2008LO_Common.py")
    
    include ("MC12JobOptions/Pythia8_MadGraph.py")

    topAlg.Pythia8.Commands += ["Init:showAllParticleData = on",
                                "Next:numberShowLHA = 10",
                                "Next:numberShowEvent = 10",
                                "Beams:frameType = 4"]


    #Merging settings
    if dokTMerging:

        topAlg.Pythia8.Commands += ["Merging:doKTMerging = on",
                                    "Merging:ktType = 1",
                                    "Merging:nJetMax = %i"%nJetMax,
                                    "Merging:Process = %s"%process,
                                    "Merging:TMS = %f"%ktdurham]

    elif doCutBasedMerging:

        topAlg.Pythia8.Commands += ["Merging:doCutBasedMerging = on",
                                    #"Merging:enforceCutOnLHE = off",
                                    "Merging:QijMS = %f"%mmjjcut,
                                    "Merging:pTiMS = %f"%ptjcut, 
                                    "Merging:dRijMS = %f"%dRjjcut,
                                    "Merging:nJetMax = %i"%nJetMax,
                                    "Merging:Process = %s"%process]


    elif doMLMMatching:

        topAlg.Pythia8.Commands += ["JetMatching:merge = on",
                                    "JetMatching:setMad = on",
                                    "JetMatching:scheme = 1",
                                    #"JetMatching:doShowerKt = on", #doesn't work in 17.2.13.9
                                    "JetMatching:qCut = %f"%qcut,
                                    "JetMatching:nQmatch = %i"%maxjetflavor,
                                    #"JetMatching:qCutME = %f"%xqcut, #doesn't work in 17.2.13.9
                                    "JetMatching:exclusive = %i"%iexcfile ]

                                    #JetMatching:setMad = on means the following are automatically set:
                                    #JetMatching:doMerge = ickkw,
                                    #JetMatching:qCut = xqcut,
                                    #JetMatching:nQmatch = maxjetflavor,
                                    #JetMatching:clFact = alpsfact.

    else:
        print "Inclusive run - no merging"


    print "Pythia8.Commands: ",topAlg.Pythia8.Commands

    
    ## Photos not recommended expect for special cases
    # include ( "MC12JobOptions/Pythia8_Photos.py" )
    
    evgenConfig.generators += ["MadGraph", "Pythia8"]
    evgenConfig.description = 'MadGraph_'+str(name)
    evgenConfig.keywords += ["SM","validation"]
    evgenConfig.inputfilecheck = stringy
    evgenConfig.minevents = minevents
    runArgs.inputGeneratorFile=stringy+'._00001.events.tar.gz'
    


    ##Post-showering options

    #Run EvtGen
    if runArgs.runNumber in EvtGenList:
        include("MC12JobOptions/Pythia8_EvtGen.py")



    #Loosen TestHepMC mom balance threshold (sometimes required for massless b's in ME)
    if removeMomentumBalanceWarning:
        from TruthExamples.TruthExamplesConf import TestHepMC
        topAlg += TestHepMC()
        topAlg.TestHepMC.EnergyDifference = 10000

        
    #Apply B-filter a la Sherpa
    if applyBFilter:      
        from GeneratorFilters.GeneratorFiltersConf import HeavyFlavorHadronFilter
        topAlg += HeavyFlavorHadronFilter()
        HeavyFlavorHadronFilter = topAlg.HeavyFlavorHadronFilter
        HeavyFlavorHadronFilter.RequestBottom=True
        HeavyFlavorHadronFilter.RequestCharm=False
        HeavyFlavorHadronFilter.Request_cQuark=False
        HeavyFlavorHadronFilter.Request_bQuark=False
        HeavyFlavorHadronFilter.RequestSpecificPDGID=False
        HeavyFlavorHadronFilter.RequireTruthJet=False
        HeavyFlavorHadronFilter.BottomPtMin=0*GeV
        HeavyFlavorHadronFilter.BottomEtaMax=4.0
        StreamEVGEN.RequireAlgs += [ "HeavyFlavorHadronFilter" ]
        
        

    if runRivet:

        from Rivet_i.Rivet_iConf import Rivet_i
        topAlg += Rivet_i()
        
        if 'Zbb' in runRivet:
            topAlg.Rivet_i.Analyses += ['ATLAS_ZB','CMS_2013_I1256943' ]
        if 'Zjets' in runRivet:
            topAlg.Rivet_i.Analyses += ['ATLAS_2011_I945498']
            #['ATLAS_2013_I1230812_EL']
        if 'Wjets' in runRivet:
            topAlg.Rivet_i.Analyses += ['ATLAS_2012_I1083318']
            
        topAlg.Rivet_i.RunName = ""
        topAlg.Rivet_i.HistoFile = "%i.aida"%runArgs.runNumber
        topAlg.Rivet_i.CrossSection = 1.0
        
        import os
        topAlg.Rivet_i.AnalysisPath = os.environ['PWD']
        
        from AthenaCommon.AppMgr import ServiceMgr as svcMgr
        from GaudiSvc.GaudiSvcConf import THistSvc
        svcMgr += THistSvc()
        svcMgr.THistSvc.Output = ["Rivet DATAFILE='Rivet.root' OPT='RECREATE'"]
        

        
