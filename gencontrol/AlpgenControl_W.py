# Set up for generic alpgen generation
#  This is the main job option to call

import sys

UseHerwig=True
rand_seed=1
ecm = 3500.

from AthenaCommon import Logging

log = Logging.logging.getLogger('AlpGenGenericGeneration')

if not hasattr(runArgs,'runNumber'):
    log.fatal('No run number found.  Generation will fail.  No idea how to decode this.')
    raise RunTimeError('No run number found.')

if not hasattr(runArgs,'randomSeed'):
    log.error('Random seed not set.  Will use 1.')
else:
    rand_seed=runArgs.randomSeed

if not hasattr(runArgs,'skipEvents'):
    log.info('No skip events argument found.  Good!')
else:
    log.fatal('Skip events argument found.  No idea how to deal with that yet.  Bailing out.')
    raise RunTimeError('Skip events argument found.')

if not hasattr(runArgs,'ecmEnergy'):
    log.warning('No center of mass energy found.  Will use the default of 7 TeV.')
else:
    ecm = runArgs.ecmEnergy / 2.

# Set up run based on run number
run_num=int(runArgs.runNumber)
process=''
special1=None
special2=None
special3=None
nwarm = [ 4 , 1000000 ]
UseDilFilter=False
UseLooseDilFilter=False
ForceLeptonicTau=False
ZJET=False
WJET=False
 

# VBF filtered + Atau filtered samples Z B. Di Micco, K. Nakamura
VBFFilter=False
WVBFFilter=False
ATauFilter=False
TruthJetFilter=False

if run_num>=169550 and run_num<=(169550+29) :
    WJET=True
    WVBFFilter=True
    run_min=169550
        
    W_mode = int((run_num-run_min)/10)%4+1
    event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]

    jets = [(run_num-run_min)%10,1] # Exclusive matching to the last number of partons

#    events_athena = 5000
    events_athena = 500
    if jets[0]==3: events_athena=200
    if jets[0]==4: events_athena=200
    if jets[0]==5: events_athena=20

    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau)\n '%(W_mode)
    special2 = special1


if run_num>=169804 and run_num<=169806 :
    UseHerwig=False
    WJET=True
    TruthJetFilter=True
    run_min=169804
        
    W_mode = (run_num-run_min)+1
#    event_numbers=[2600000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]
    event_numbers=[260000,11000000,34000000,73000000,50000000,15000000,10000000,10000000,10000000]

    jets = [0,1] # Exclusive matching to the last number of partons

    events_athena = 5000

    special1 = 'iwdecmode %i          ! W decay mode (1: el, 2: mu, 3: tau)\n '%(W_mode)
    special2 = special1



if ZJET:
    log.info('Recognized Z+jets run number.  Will generate for run '+str(run_num))
    process = 'zjet'
    if jets[0]==5: jets[1]=0

    nwarm = jets[0]+2
    events_alpgen = event_numbers[jets[0]]

    # Handle special cases for more than five partons.  Run # 6, Np5 excl.  Run # 7, Np6 incl.
    if jets[0]==6: 
        jets=[5,1]
    elif jets[0]==7:
        jets=[6,1]
    elif jets[0]>7:
        jets=[5,1]

    nwarm = [ jets[0]+2 , 1000000 ]
    if Z_mode<4: special1 += """ilep 0        ! Use leptons in the final state (not neutrinos)
izdecmode %i          ! Z decay mode (1: ee, 2: mumu, 3: tautau)\n """%(Z_mode)
    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """
    if not UseHerwig: special1 += """xclu   0.26          ! ???
ipclu 1         ! ???
"""
    special2 = special1


if WJET:
    log.info('Recognized W+jets run number.  Will generate for run '+str(run_num))
    process = 'wjet'
    if jets[0]==5: jets[1]=0

    nwarm = jets[0]+2
    events_alpgen = event_numbers[jets[0]]

    # Handle special cases for more than five partons.  Run # 6, Np5 excl.  Run # 7, Np6 incl.
    if jets[0]==6: 
        jets=[5,1]
    elif jets[0]==7:
        jets=[6,1]
    elif jets[0]>7:
        jets=[5,1]

    nwarm = [ jets[0]+2 , 1000000 ]
#    if W_mode<4: special1 += """ilep 0        ! Use lepton in the final state 
#iwdecmode %i          ! W decay mode (1: ev, 2: muv, 3: tauv)\n """%(W_mode)
#    else: special1 += """ilep 1       ! Use neutrinos in the final state (not leptons) \n """
    if not UseHerwig: special1 += """xclu   0.26          ! ???
ipclu 1         ! ???
"""
    special2 = special1






if process=='':
    log.fatal('Unknown run number!  Bailing out!')
    raise RunTimeError('Unknown run number.')

if not UseHerwig:  
        special1 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 
        special2 += """xlclu   0.26          ! lambda for alpha_s in CKKW scale evaluation
lpclu 1         ! nloop for alphas in CKKW scale evaluation
""" 

if hasattr(runArgs,'cutAthenaEvents'):
    events_athena = events_athena/runArgs.cutAthenaEvents
    log.warning('Reducing number of Athena events by '+str(runArgs.cutAthenaEvents)+' to '+str(events_athena)+' .  Later you should fix this.')
if hasattr(runArgs,'raiseAlpgenEvents'):
    events_alpgen = events_alpgen*runArgs.raiseAlpgenEvents
    log.warning('Increasing number of AlpGen events by '+str(runArgs.raiseAlpgenEvents)+' to '+str(events_alpgen)+' .  Later you should fix this.')

from AlpGenControl.AlpGenUtils import SimpleRun

log.info('Running AlpGen on process '+process+' with ECM='+str(ecm)+' with '+str(nwarm)+' warm up rounds')
log.info('Will generate '+str(events_alpgen)+' events with jet params '+str(jets)+' and random seed '+str(rand_seed))
if special1 is not None:
    log.info('Special settings for mode 1:')
    log.info(special1)
if special2 is not None:
    log.info('Special settings for mode 2:')
    log.info(special2)
if special3 is not None:
    log.info('Special settings for pythia:')
    log.info(special3)

SimpleRun( proc_name=process , events=events_alpgen , nwarm=nwarm , energy=ecm , jets=jets , seed = rand_seed , special1=special1 , special2=special2 , special3=special3 )

if hasattr(runArgs,'inputGeneratorFile'):
    log.info('About to clobber old input generator file argument, '+str(runArgs.inputGeneratorFile))
runArgs.inputGeneratorFile='alpgen.XXXXXX.TXT.v1'

if UseHerwig:
    log.info('Passing events on to Herwig for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenHerwig.py') 
else:
    log.info('Passing events on to Pythia for generation of '+str(events_athena)+' events in athena')
    include('AlpGenControl/MC12.AlpGenPythia.py')

    # Additional bit for ME/PS matching
    phojf=open('./pythia_card.dat', 'w')
    phojinp = """
      !...Matching parameters...
      IEXCFILE=%i
      showerkt=T
      qcut=%i
      imss(21)=24
      imss(22)=24  
    """%(jets[1],15)
    # For the moment, hard-coded jet cut (in AlpGenUtils.py)

    phojf.write(phojinp)
    phojf.close()


if ForceLeptonicTau:
   include("MC12JobOptions/Tauola_LeptonicDecay_Fragment.py")
   # tauola multiplicative factor for AMI MetaData
   topAlg.Herwig.CrossSectionScaleFactor=0.1393

if UseDilFilter: include("AlpGenControl/MC.GenAlpGenDilFilter.py")
if VBFFilter: include("MC12JobOptions/AlpgenControl_VBFFilter.py")
if WVBFFilter: include("MC12JobOptions/AlpgenControl_WVBFFilter.py")
if ATauFilter: include("MC12JobOptions/AlpgenControl_ATauFilter.py")
if UseLooseDilFilter: include("MC12JobOptions/AlpgenControl_LooseDilFilter.py")
if TruthJetFilter: include ( "MC12JobOptions/AlpgenControl_CentralJetFilter.py" )

# Fill up the evgenConfig
evgenConfig.description = 'alpgen_autoconfig'
evgenConfig.contact = ['zach.marshall@cern.ch','Koji.Nakamura@cern.ch']
evgenConfig.process = 'auto_alpgen_'+process+'_'+`jets[0]`+'p'
evgenConfig.inputfilecheck = 'alpgen.XXXXXX.TXT.v1'
evgenConfig.keywords = ['W','leptonic']
#evgenConfig.efficiency = 0.90000
evgenConfig.minevents = events_athena
